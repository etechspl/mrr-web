import { AreaService } from './core/services/Area/area.service';
import { HolidayService } from './core/services/Holiday/holiday.service';
import { DesignationService } from './core/services/designation.service';

import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
 
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthenticationModule } from './core/auth/authentication.module';
import { NgxPermissionsModule } from 'ngx-permissions';
//import { FakeApiService } from './fake-api/fake-api.service';

import { LayoutModule } from './content/layout/layout.module';
import { PartialsModule } from './content/partials/partials.module';
import { CoreModule } from './core/core.module';
import { AclService } from './core/services/acl.service';
import { LayoutConfigService } from './core/services/layout-config.service';
import { MenuConfigService } from './core/services/menu-config.service';
import { PageConfigService } from './core/services/page-config.service';
import { UserService } from './core/services/user.service';
import { UtilsService } from './core/services/utils.service';
import { ClassInitService } from './core/services/class-init.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GestureConfig, MatProgressSpinnerModule, MatToolbarModule, MatButtonModule, MatDatepickerModule, MatRadioModule, MatSnackBarModule, MatTooltipModule, MatTableModule, MatTable, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatDialogModule, MatGridListModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';

import { MessengerService } from './core/services/messenger.service';
import { ClipboardService } from './core/services/clipboard.sevice';

import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { LayoutConfigStorageService } from './core/services/layout-config-storage.service';
import { LogsService } from './core/services/logs.service';
import { QuickSearchService } from './core/services/quick-search.service';
import { SubheaderService } from './core/services/layout/subheader.service';
import { HeaderService } from './core/services/layout/header.service';
import { MenuHorizontalService } from './core/services/layout/menu-horizontal.service';
import { MenuAsideService } from './core/services/layout/menu-aside.service';
import { LayoutRefService } from './core/services/layout/layout-ref.service';
import { SplashScreenService } from './core/services/splash-screen.service';

import 'hammerjs';

//Praveen Kumar 18-09-2018------------------
import { StateService } from "./core/services/state.service";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MonthYearService } from './core/services/month-year.service';
import { DistrictService } from "./core/services/district.service";
import { UserDetailsService } from './core/services/user-details.service';
import { URLService } from './core/services/URL/url.service';
import { TourProgramService } from './core/services/TourProgram/tour-program.service';
import { StockiestService } from "./core/services/Stockiest/stockiest.service";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { STPService } from './core/services/stp.service';
import { StpApprovalService } from './core/services/stp-approval.service';
import { DailyallowanceService } from './content/pages/_core/services/dailyallowance.service';
import { InterceptService } from './content/pages/_core/utils/intercept.service';
import { FileHandlingService } from './core/services/FileHandling/file-handling.service';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { InobxService } from './core/services/inbox/inobx.service';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { BroadcastMessageService } from './core/services/BroadcastMessage/broadcast-message.service';

import { ExportAsModule } from 'ngx-export-as';
import { LeaveApprovalService } from './core/services/leave-approval.service';
import { DoctorChemistService } from './core/services/DoctorChemist/doctor-chemist.service';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};

@NgModule({
	declarations: [AppComponent],
	imports: [
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		BrowserAnimationsModule,
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		LayoutModule,
		PartialsModule,
		CoreModule,
		OverlayModule,
		AuthenticationModule,
		NgxPermissionsModule.forRoot(),
		NgbModule.forRoot(),
		TranslateModule.forRoot(),
		MatProgressSpinnerModule,
		MatToolbarModule,
		FormsModule,
		MatFormFieldModule,
		ReactiveFormsModule,
		FormsModule,
		MatButtonModule,
		MatDatepickerModule,
		MatRadioModule,
		MatSnackBarModule,
		AmChartsModule,
		FormlyModule,
		FormlyBootstrapModule,
		MatTooltipModule,
		MatTableModule,
		MatPaginatorModule,
		MatSortModule,
		MatDialogModule,
		MatGridListModule,
		ExportAsModule,
		LeafletModule.forRoot(),
		ToastrModule.forRoot({
			timeOut: 5000,
			positionClass: 'toast-top-right',
			preventDuplicates: true,
		})
	],

	schemas: [CUSTOM_ELEMENTS_SCHEMA],

	providers: [
		DoctorChemistService,
		AclService,
		LayoutConfigService,
		LayoutConfigStorageService,
		LayoutRefService,
		MenuConfigService,
		PageConfigService,
		UserService,
		UtilsService,
		ClassInitService,
		MessengerService,
		ClipboardService,
		LogsService,
		QuickSearchService,
		SplashScreenService,
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		},

		// template services
		SubheaderService,
		HeaderService,
		MenuHorizontalService,
		MenuAsideService,
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: GestureConfig
		},
		StateService,
		MonthYearService,
		DistrictService,
		DesignationService,
		UserDetailsService,
		URLService,
		TourProgramService,
		AreaService,
		StockiestService,
		DailyallowanceService,
		STPService,
		StpApprovalService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		},
		
		FileHandlingService,
		InobxService,
		HolidayService,
		BroadcastMessageService,
		LeaveApprovalService
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
