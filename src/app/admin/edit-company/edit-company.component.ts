import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExpenseHeadService } from '../../core/services/ExpensesHead/expense-head.service';
import { AdminServiceService } from '../admin-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'm-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.scss']
})
export class EditCompanyComponent implements OnInit {
  
  companyEditForm: FormGroup;
  previousData: any;
  disableButtons = false;
  
  constructor(
    private toast: ToastrService,
    public dialogRef: MatDialogRef<EditCompanyComponent>,
    private service: AdminServiceService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.previousData = data;
   }

  ngOnInit() {
    console.table(this.previousData);
    this.companyEditForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      maxUser: [null, Validators.compose([Validators.required])],
      status: [null, Validators.compose([Validators.required])]
    });
    this.companyEditForm.patchValue({
      name : this.previousData.name,
      maxUser: this.previousData.maxUser,
      status: this.previousData.status
    });
  }
  updateCompany() {
    let query ={"id": ''+this.previousData.id};
    let data = {
      'status': this.companyEditForm.controls.status.value,
      'maxUser': this.companyEditForm.controls.maxUser.value
    };
    this.disableButtons = true;
    this.service.updateCompanydata(query, data).subscribe(res =>{
      if(res.count){
        this.toast.success('data updated successfully');
        console.log(`update  company response`, res);
        this.closeModel(true);
      }else{
        this.toast.error('Something went wrong !');
      }
    });
  }
  closeModel(obj: Boolean){
    this.service.updatedOrNot.emit(obj);
    this.dialogRef.close();
  }

}
