import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import {MatDialogModule, MatSelectModule, MatInputModule, MatButtonModule} from '@angular/material';
// import { NativeDateAdapter, DateAdapter, MatDateFormats, MAT_DATE_FORMATS, MatTooltipModule } from "@angular/material";
import {MatTooltipModule} from '@angular/material';
import { CompaniesComponent } from './companies/companies.component';
import { AdminServiceService } from './admin-service.service';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditCompanyComponent } from './edit-company/edit-company.component';

const routes: Routes = [
    {
        path: '',
        component: CompaniesComponent
    }
]
@NgModule({
	imports: [
		MatButtonModule,
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		RouterModule.forChild(routes),
		MatDialogModule,
		MatSelectModule,
		MatInputModule,
		MatTooltipModule,
		
	],
	declarations: [
        CompaniesComponent,
        EditCompanyComponent
	],
	exports: [],
	providers: [
		AdminServiceService,
		DatePipe,
	],
	entryComponents: [EditCompanyComponent,]
})
export class AdminModule {}
