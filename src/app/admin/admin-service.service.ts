import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../core/auth/authentication.service';
import { URLService } from '../../app/core/services/URL/url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  @Output() updatedOrNot: EventEmitter<any> = new EventEmitter();
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  constructor(
    private urlService: URLService,
    private authservice: AuthenticationService,
    private http: HttpClient,
  ) {}
  getCompaniesList(): Observable<any> {
    return this.http.get(this.urlService.API_ENDPOINT + '/CompanyMasters');
  }
  getCompaniesData(): Observable<any> {
    let params={};
    return this.http.get(this.urlService.API_ENDPOINT_USERINFO + '/getCompanyCreationDetails?params='+ JSON.stringify(params));
  }
  updateCompanydata(query, data): Observable<any>{
    return this.http.post(this.urlService.API_ENDPOINT + '/CompanyMasters/update?where='+ JSON.stringify(query),data);
  }
  getCompaniesDCR(onj: any){
    //  api should be edited
    // return this.http.post(this.urlService.API_ENDPOINT + '/CompanyMasters/update?where='+ JSON.stringify(query),data);
  }
}
