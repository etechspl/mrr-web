import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminServiceService } from '../admin-service.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
declare var $;
import { EditCompanyComponent } from '../edit-company/edit-company.component';
import { DatePipe } from '@angular/common'


@Component({
  selector: 'm-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

  @ViewChild("companyDataTable") dataTable;
  companyDataTable: any;
  companyDTOptions: any;
  dialogRef: any = null;
  dialogConfig = new MatDialogConfig();
  constructor(
    private datePipe: DatePipe,
    private dialog: MatDialog,
    private service : AdminServiceService
  ) { }

  ngOnInit() {
    this.getAllCompanies();
    this.service.updatedOrNot.subscribe(res=>{
      (res)?this.getAllCompanies() : '';
    });
  }
  getAllCompanies() {
    let months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
    this.service.getCompaniesData().subscribe(res => {
      this.companyDTOptions = {
      "pagingType": 'full_numbers',
        "paging": true,
        "ordering": true,
        "info": true,
        "scrollY": 300,
        "scrollX": true,
        "fixedColumns": true,
        destroy: true,
        data: res,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excel',
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {
            extend: 'csv',
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              
            }
          },
          {
            extend: 'copy',
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              
            }
          }
        ],
        columns: [
          {
            title: 'Company',
            data: 'companyName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Admin Contact',
            data: 'adminContact',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Admin Email',
            data: 'adminEmail',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Created At',
            data: 'createdAt',
            render: function (data) {
              if (data === '') {
                return '---'
              } else if(data) {
                let date = new Date(data);
                let year = date.getFullYear();
                let monthIndex = date.getMonth();
                let day = date.getDate();
                return day+'-'+ months[monthIndex]+'-'+year
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Updated At',
            data: 'updatedAt',
            render: function (data) {
              if (data === '') {
                return '---'
              } else if(data) {
                let date = new Date(data);
                let year = date.getFullYear();
                let monthIndex = date.getMonth();
                let day = date.getDate();
                return day+'-'+ months[monthIndex]+'-'+year
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Last Reporting date',
            data: 'lastReportingDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else if(data) {
                let date = new Date(data);
                let year = date.getFullYear();
                let monthIndex = date.getMonth();
                let day = date.getDate();
                return day+'-'+ months[monthIndex]+'-'+year
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Finalized Users',
            data: 'finaliseUser',
            render: function (data) {
              if (data ) {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Max Users',
            data: 'maxUser',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Active Users',
            data: 'activeUsers',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'In-active Users',
            data: 'inActiveUsers',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Division Exist',
            data: 'isDivisionExist',
            render: function (data) {
              if (data===true) {
                return 'Yes'
              } else if(data === false ) {
                return 'No'
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Status',
            data: 'status',
            render: function (data) {
              if(data){
                return "Active"
              } else{
                return "In-active"
              }
            },
            defaultContent: '---'
          },
          {
            title: "Get DCR",
            data: "status",
            render: function (data){
                return "<button id='getDCR' class='btn btn-warning btn-sm'>&nbsp;DCR</button>"
            }
          },
          {
            title: "Edit Company",
            data: "status",
            render: function (data){
                return "<button id='editButton'  matTooltip='Edit' class='btn btn-warning btn-sm'><i id='edit' class='la la-edit'></i>&nbsp;Edit</button>"
            }
          },
          {
            title: "Change Status",
            data: "status",
            render: function (data){
              if(data ===''){
                return"---";
              }else {
                if (data == true) {
                  return "<button id='deactivate' class='btn btn-warning btn-sm'><i id='deact' class='fa fa-lock'></i>&nbsp;&nbsp;Deactivate</button>"
                } else if (data == false) {
                  return "<button id='activate' class='btn btn-warning btn-sm'><i id='act' class='fa fa-unlock'></i>&nbsp;&nbsp;&nbsp;&nbsp;Activate</button>"
                }
              }
            }
          }
        ],
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          $('td', row).unbind('click');
          $('td button', row).bind('click', (event) => {
            if(event.target.id === "editButton" || event.target.id === "edit"){
              self.editCompany(data);
            } else if(event.target.id === "deactivate"|| event.target.id === "deact"){
              self.changeCompanyStatus(data);
            } else if(event.target.id === "activate"|| event.target.id === "act") {
              self.changeCompanyStatus(data);
            }else if(event.target.id === "getDCR") {
              self.getCompanyDCR(data);
            }
          });
        }
        }
        this.companyDataTable = $(this.dataTable.nativeElement);
        this.companyDataTable.DataTable(this.companyDTOptions);
    },
    
    );
  }
  
  changeCompanyStatus(obj: any){
    let query ={"id": ''+obj.id};
    let data = {'status': !obj.status};
    this.service.updateCompanydata(query, data).subscribe(res =>{
      this.getAllCompanies();
    })
  }
  editCompany(object: any){
    this.dialogConfig.data =  object;
    if(this.dialogRef === null){
      this.dialogRef = this.dialog.open(EditCompanyComponent, this.dialogConfig);
    }
    this.dialogRef.afterClosed().subscribe(()=> {
      this.dialogRef = null;
    });
  }

  getCompanyDCR(obj: any) {
    console.log('getting dcr',obj);
    this.service.getCompaniesDCR(obj);
  }
}
