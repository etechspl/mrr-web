import { ChangeDetectionStrategy, Component, HostBinding, Inject, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { ClassInitService } from '../../../../core/services/class-init.service';
import { LayoutConfigService } from '../../../../core/services/layout-config.service';
import * as objectPath from 'object-path';
import { DOCUMENT } from '@angular/common';
import { FileHandlingService } from '../../../../core/services/FileHandling/file-handling.service';

@Component({
	selector: 'm-brand',
	templateUrl: './brand.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class BrandComponent implements OnInit {
	@HostBinding('class') classes = 'm-stack__item m-brand';
	@Input() menuAsideLeftSkin: any = '';
	@Input() menuAsideMinimizeDefault: any = false;
	@Input() menuAsideMinimizToggle: any = false;
	@Input() menuAsideDisplay: any = false;
	@Input() menuHeaderDisplay: any = true;
	@Input() headerLogo: any = '';
	currentUser = JSON.parse(sessionStorage.currentUser);

	constructor(
		private classInitService: ClassInitService,
		private layoutConfigService: LayoutConfigService,
		private _fileHandlingService: FileHandlingService,
		private _changeDetectorRef: ChangeDetectorRef,
		@Inject(DOCUMENT) private document: Document
	) {
		// subscribe to class update event
		this.classInitService.onClassesUpdated$.subscribe(classes => {
			this.classes = 'm-stack__item m-brand ' + classes.brand.join(' ');
		});

		this.layoutConfigService.onLayoutConfigUpdated$.subscribe(model => {			

			this.menuAsideLeftSkin = objectPath.get(model, 'config.aside.left.skin');

			this.menuAsideMinimizeDefault = objectPath.get(model, 'config.aside.left.minimize.default');

			this.menuAsideMinimizToggle = objectPath.get(model, 'config.aside.left.minimize.toggle');

			this.menuAsideDisplay = objectPath.get(model, 'config.menu.aside.display');

			this.menuHeaderDisplay = objectPath.get(model, 'config.menu.header.display');

			// const headerLogo = objectPath.get(model, 'config.header.self.logo');
			// if (typeof headerLogo === 'object') {
			// 	this.headerLogo = './assets/demo/default/media/img/logo/mrlogofinalPNG.png'//objectPath.get(headerLogo, this.menuAsideLeftSkin);
			// 	//console.log(this.headerLogo);
				
			// } else {
			// 	this.headerLogo = headerLogo;
			// }

			this._fileHandlingService.getDocument("CompanyLogo",this.currentUser.company.logo.modifiedFileName).subscribe(res => {
				//console.log(res);
				this.createImageFromBlob(res);
			  }, err => {
				console.log(err);
		  
			  })
		});
	}
	createImageFromBlob(image: Blob) {		
		let reader = new FileReader();		
		reader.addEventListener("load", () => {
		  this.headerLogo = reader.result;
		  (!this._changeDetectorRef['destroyed'])?this._changeDetectorRef.detectChanges():null;

		}, false);
	
		if (image) {
		  reader.readAsDataURL(image);
		}
		// this._changeDetectorRef.detectChanges();
		(!this._changeDetectorRef['destroyed'])?this._changeDetectorRef.detectChanges():null;

	  }
	ngOnInit(): void {}

	/**
	 * Toggle class topbar show/hide
	 * @param event
	 */
	clickTopbarToggle(event: Event): void {
		this.document.body.classList.toggle('m-topbar--on');
	}
}
