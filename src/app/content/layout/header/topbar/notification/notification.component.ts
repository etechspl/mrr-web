import {
	Component,
	OnInit,
	HostBinding,
	HostListener,
	Input,
	ChangeDetectionStrategy, ChangeDetectorRef, ViewChild
} from '@angular/core';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
declare var $;
@Component({
	selector: 'm-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);

	@HostBinding('class')
	// tslint:disable-next-line:max-line-length
	classes = 'm-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width';

	@HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';
	@HostBinding('attr.m-dropdown-persistent') attrDropdownPersisten = 'true';

	@Input() animateShake: any;
	@Input() animateBlink: any;
	dataTable: any;
	dtOptions: any;
	@ViewChild('dataTable') table;
	birthdayToday = [];
	anniversaryToday = [];
	showProcessing: boolean = false;
	notificationCount=0;
	birthdayNotificationCount=0;
	anniNotificationCount=0;

	showBirthdays: boolean = false;
	showAnniversaries: boolean = false;
	constructor(
		private _userDetailsService: UserDetailsService,
		private _changeDetectorRef: ChangeDetectorRef
	) {
		// animate icon shake and dot blink
		setInterval(() => {
			this.animateShake = 'm-animate-shake';
			this.animateBlink = 'm-animate-blink';
		}, 3000);
		setInterval(() => (this.animateShake = this.animateBlink = ''), 6000);
	}

	ngOnInit(): void {
		this.showProcessing = true;
		let param = {
			companyId: this.currentUser.companyId,
			status: true,
			rl: this.currentUser.userInfo[0].rL,
			userId: this.currentUser.userInfo[0].userId
		}
		this._userDetailsService.getUsersBirthdayAndAnniversaryDetails(param).subscribe((res) => {
			console.log("------------------------------------------------------------------------res",res)
				this.showProcessing = false;	

			res.data.forEach(element => {
				if(element.type == 'RMP'){
					
					this.notificationCount+=1;
					if(element.description=="Happy Birthday"){
					  this.birthdayNotificationCount+=1;
					  var obj={};
					  obj["name"]=element.title;
					  obj["state"]=element.area;
					  obj["mobile"]=element.mobile;
					  obj["mrrname"]=element.mrrname;
					  obj["districts"]=element.districts;
					  obj['type'] = this.currentUser.company.lables.doctorLabel;
					  this.birthdayToday.push(obj);
					}else if(element.description=="Happy Anniversary"){
						this.anniNotificationCount+=1;
						var objAnni={}
						objAnni['type'] = this.currentUser.company.lables.doctorLabel;
						objAnni["state"]=element.area;
						objAnni["mobile"]=element.mobile;
						objAnni["name"]=element.title;
						obj["mrrname"]=element.mrrname;
						obj["districts"]=element.districts;
				    	this.anniversaryToday.push(objAnni)
					}
					
					

				}else if(element.type == 'Drug'){
					this.notificationCount+=1;
					if(element.description=="Happy Birthday"){
						this.birthdayNotificationCount+=1;
						var obj={}
						obj["name"]=element.title;
						obj["state"]=element.area;
						obj["mrrname"]=element.mrrname;
						obj["districts"]=element.districts;
						obj["mobile"]=element.mobile;
						obj['type'] = this.currentUser.company.lables.vendorLabel;
						this.birthdayToday.push(obj);
					  }else if(element.description=="Happy Anniversary"){
						  this.anniNotificationCount+=1;
						  var objAnni={}
						  objAnni["name"]=element.title;
						  objAnni["mobile"]=element.mobile;
						  objAnni["state"]=element.area;
						  obj["mrrname"]=element.mrrname;
						obj["districts"]=element.districts;
						  objAnni['type'] = this.currentUser.company.lables.vendorLabel;
						  this.anniversaryToday.push(objAnni)
					  }
					
					

				}else if(element.type == 'Stockist'){
					this.notificationCount+=1;
					if(element.description=="Happy Birthday"){
						this.birthdayNotificationCount+=1;
						var obj={};
						obj["name"]=element.title;
						obj["state"]=element.area;
						obj["mrrname"]=element.mrrname;
						obj["districts"]=element.districts;
						obj["mobile"]=element.mobile;
						obj['type'] = this.currentUser.company.lables.stockistLabel;
						this.birthdayToday.push(obj);
					  }else if(element.description=="Happy Anniversary"){
						var objAnni={}
						objAnni["name"]=element.title;
						objAnni["state"]=element.area;
						objAnni["mobile"]=element.mobile;
						obj["mrrname"]=element.mrrname;
						obj["districts"]=element.districts;
						objAnni['type'] = this.currentUser.company.lables.stockistLabel;
						  this.anniNotificationCount+=1;
						  this.anniversaryToday.push(objAnni);
					  }
					
					
				}	
				else if(element.type == 'User'){					
					this.notificationCount+=1;
					if(element.description=="Happy Birthday"){
						this.birthdayNotificationCount+=1;
						var obj={};
						obj["name"]=element.title;
						obj["state"]=element.state;
						obj["mobile"]=element.mobile;
						obj["hq"]=element.hq;
						obj['type'] = "Employee";
						this.birthdayToday.push(obj);
					  }else if(element.description=="Happy Anniversary"){
						var objAnni={}
						objAnni["name"]=element.title;
						objAnni["state"]=element.state;
						objAnni["mobile"]=element.mobile;
						objAnni["hq"]=element.hq;
						objAnni['type'] = "Employee";
						this.anniNotificationCount+=1;
						this.anniversaryToday.push(objAnni)
					  }
				}				
			});
			
			// res.anniversaryToday.forEach(element => {
			// 	if(element.type == 'RMP'){
			// 		element['type'] = this.currentUser.company.lables.doctorLabel
			// 	}else if(element.type == 'Drug'){
			// 		element['type'] = this.currentUser.company.lables.vendorLabel
			// 	}else if(element.type == 'Stockist'){
			// 		element['type'] = this.currentUser.company.lables.stockistLabel
			// 	}				
			// });
			// this.birthdayToday = res.birthdayToday;
			// this.anniversaryToday = res.anniversaryToday;
			!this._changeDetectorRef["destroyed"]
				? this._changeDetectorRef.detectChanges()
				: null;				
				if(!sessionStorage.getItem("notificationCount")){
					setTimeout(() => {
						sessionStorage.setItem("notificationCount","true");
						document.getElementById('m_topbar_notification_icon').click();
					}, 1000);
				}
				
		})
	}
	tabChange(event) {
		document.getElementById('m_topbar_notification_icon').click();
	}
}
