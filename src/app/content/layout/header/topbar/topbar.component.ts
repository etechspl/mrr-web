import {
	Component,
	OnInit,
	HostBinding,
	Input,
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
} from "@angular/core";
import { LayoutConfigService } from "../../../../core/services/layout-config.service";
import * as objectPath from "object-path";
import { Router } from "@angular/router";
import { BroadcastMessageService } from "../../../../core/services/BroadcastMessage/broadcast-message.service";
import Swal from "sweetalert2";
@Component({
	selector: "m-topbar",
	templateUrl: "./topbar.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopbarComponent implements OnInit, AfterViewInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	@HostBinding("id") id = "m_header_nav";
	@HostBinding("class")
	classes = "m-stack__item m-stack__item--fluid m-header-head";

	@Input() searchType: any;
	broadcastMessage = [];

	constructor(
		private layoutConfigService: LayoutConfigService,
		private router: Router,
		private _broadcastMessageService: BroadcastMessageService,
		private _changeDetectorRef: ChangeDetectorRef
	) {
		this.layoutConfigService.onLayoutConfigUpdated$.subscribe((model) => {
			const config = model.config;
			this.searchType = objectPath.get(config, "header.search.type");
		});
	}

	ngOnInit(): void {
		// Anjana 29-08-2019
		this._broadcastMessageService
			.getUserBroadcastMessage(
				this.currentUser.companyId,
				this.currentUser.id
			)
			.subscribe((res) => {
				if (JSON.parse(JSON.stringify(res)).length > 0) {
					this.broadcastMessage = JSON.parse(JSON.stringify(res));
				}
				!this._changeDetectorRef["destroyed"]
					? this._changeDetectorRef.detectChanges()
					: null;
			});
	}

	showUserBroadcastMessages() {
		let swal_html = ``;
		this.broadcastMessage.forEach((data, index) => {
			swal_html += `<tr>
				<td  style="border-top: 1px solid #8e8282;    border: 1px solid; text-align: left !important;">${data.subject}</td>
				<td  style="border-top: 1px solid #8e8282;    border: 1px solid; text-align: left !important;">${data.message}</td>
				</tr>`;

			if (this.broadcastMessage.length - 1 == index) {
				(Swal as any).fire({
					title: "Broadcast Messages",
					width: "800px",
					html: `<div style="
					color: black;
					font-weight: 400;
					text-align: left !important;
					max-height:170px;
					overflow-y:auto;
					background:#e2e6ef;
					border-radius:6px;
					border:1px solid #8e8282;">
				<table class="table">
				<tr>
				<td  style="border-top: 1px solid #8e8282;font-size:larger;line-height:0;      border: 1px solid;   padding: 1.5rem;   text-align: center;
				background: #16a184;text-align: left !important;
				color: rgb(255, 255, 255);">SUBJECT</td>
				<td  style="border-top: 1px solid #8e8282;font-size:larger;line-height:0;     border: 1px solid;    padding: 1.5rem;   text-align: center;
				background: #16a184;text-align: left !important;
				color: rgb(255, 255, 255);">MESSAGES</td>
				</tr>
				${swal_html}
				</table>
				</div>`,
				});
			}
		});
		!this._changeDetectorRef["destroyed"]
		? this._changeDetectorRef.detectChanges()
		: null;
	}

	makeRandomColor() {
		return "#" + Math.random().toString(16).substr(2, 6);
	}

	ngAfterViewInit(): void {}
}
