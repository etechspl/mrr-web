import { AuthenticationService } from '../../../../../core/auth/authentication.service';
import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Input, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { FileHandlingService } from '../../../../../core/services/FileHandling/file-handling.service';


@Component({
	selector: 'm-user-profile',
	templateUrl: './user-profile.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserProfileComponent implements OnInit {
	@HostBinding('class')
	// tslint:disable-next-line:max-line-length
	classes = 'm-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light';

	@HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';

	@Input() avatar: string = './assets/app/media/img/users/default-user.png';
	@Input() avatarBg: SafeStyle = '';

	@ViewChild('mProfileDropdown') mProfileDropdown: ElementRef;

	currentUser = JSON.parse(sessionStorage.currentUser);
	constructor(
		private router: Router,
		private authService: AuthenticationService,
		private sanitizer: DomSanitizer,
		private _fileHandlingService: FileHandlingService,
		private _changeDetectorRef: ChangeDetectorRef
	) { }

	ngOnInit(): void {
		if (!this.avatarBg) {
			this.avatarBg = this.sanitizer.bypassSecurityTrustStyle('url(./assets/app/media/img/misc/user_profile_bg.jpg)');
		}

		this._fileHandlingService.getDocument("UserProfile", this.currentUser.image.modifiedFileName).subscribe(res => {

			this.createImageFromBlob(res);

		}, err => {

			console.log(err);



		});

	}
	createImageFromBlob(image: Blob) {
		let reader = new FileReader();
		reader.addEventListener("load", () => {
			this.avatar = reader.result.toString();
			this._changeDetectorRef.detectChanges();
		}, false);
		if (image) {
			reader.readAsDataURL(image);
		}
	}
	public logout() {
		this.authService.logout(true);
		//Praveen Kumar
		this.router.navigate(['login']);
	}
}
