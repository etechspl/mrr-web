import { DoughnutChartComponent } from './../../../partials/content/widgets/charts/doughnut-chart/doughnut-chart.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layout/layout.module';
import { PartialsModule } from '../../../partials/partials.module';
import { ListTimelineModule } from '../../../partials/layout/quick-sidebar/list-timeline/list-timeline.module';
import { WidgetChartsModule } from '../../../partials/content/widgets/charts/widget-charts.module';
import { MeyerDashboardComponent } from './meyer-dashboard/meyer-dashboard.component';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


// Material
import {
	
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule,
	MatSlideToggleModule
} from '@angular/material';
import { CalenderUtilityComponent } from './calender-utility/calender-utility.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { DemoUtilsModule } from '../demo-utils/module';
import { CalenderComponent } from './calender/calender.component';
import { ERPDashboardComponent } from './erpdashboard/erpdashboard.component';
import { ERPGraphsComponent } from './erpgraphs/erpgraphs.component';
import { MraskplusComponent } from './mraskplus/mraskplus.component';
import { ErpgraphsHqwiseComponent } from './erpgraphs-hqwise/erpgraphs-hqwise.component';
import { ErpdashboardHqwiseComponent } from './erpdashboard-hqwise/erpdashboard-hqwise.component';
import { MonthComponent } from '../filters/month/month.component';
@NgModule({
	imports: [
		FormsModule,
		MatDialogModule,
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		ReactiveFormsModule,
		CommonModule,
		LayoutModule,
		PartialsModule,
		ListTimelineModule,
		WidgetChartsModule,
		MatSlideToggleModule,
		RouterModule.forChild([
			{
				path: '',
				component: DashboardComponent
			},
			{
				path: 'ERPDashBoard',
				component: ERPDashboardComponent
			},
			{
				path: 'ERPDashBoardHQwise',
				component: ErpdashboardHqwiseComponent
			},
			{
				path: 'mraskplus',
				component: MraskplusComponent
			}
		]),
			CalendarModule.forRoot({
				provide: DateAdapter,
				useFactory: adapterFactory
			  }),
			  DemoUtilsModule
			  
	],
	providers: [],
	declarations: [DashboardComponent,  MeyerDashboardComponent, CalenderUtilityComponent, CalenderComponent, ERPDashboardComponent, ERPGraphsComponent, MraskplusComponent, ErpgraphsHqwiseComponent,ErpdashboardHqwiseComponent],
	schemas : [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {
	

}
