import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ERPGraphsComponent } from './erpgraphs.component';

describe('ERPGraphsComponent', () => {
  let component: ERPGraphsComponent;
  let fixture: ComponentFixture<ERPGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ERPGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ERPGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
