import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_material from "@amcharts/amcharts4/themes/material";
import { ERPServiceService } from '../../../../../core/services/erpservice.service';
import { EventEmitter } from 'protractor';
import { MatTableDataSource } from '@angular/material';
import { AmChartsService } from '@amcharts/amcharts3-angular';
import { MatSlideToggleChange } from "@angular/material";
declare var $;
import * as XLSX from 'xlsx';
import { timeout } from 'rxjs/operators';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
am4core.useTheme(am4themes_animated);
// am4core.useTheme(am4themes_material);

@Component({
  selector: 'm-erpgraphs',
  templateUrl: './erpgraphs.component.html',
  styleUrls: ['./erpgraphs.component.scss']
})

export class ERPGraphsComponent implements OnInit {
  hq: HTMLElement;
  @ViewChild("StatewiseDataTable") StatewiseDataTable: ElementRef;
  @ViewChild("HqwiseDataTable") HqwiseDataTable: ElementRef;
  @ViewChild("PartywiseDataTable") PartywiseDataTable: ElementRef;
  @ViewChild("StatewiseProductDataTable") StatewiseProductDataTable: ElementRef;
  @ViewChild("HqwiseProductDataTable") HqwiseProductDataTable: ElementRef;
  @ViewChild('hqTable') hqTableRef: ElementRef;
  @ViewChild('partyTable') partyTableRef: ElementRef;

  showStateWiseDiv = false;
  showHQWiseDiv = false;
  showPartyWiseDiv = false;
  headerStateName: any;
  headerHqName: any;
  stateData: any;
  headerSaleType: any;
  isShowProcessingState: boolean = false;
  isShowProcessingHq: boolean = false;
  isShowProcessingParty: boolean = false;
  showPrimary : boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);

  REQUESTS = [];

  dataSource: MatTableDataSource<any>;
  dataSourceHq: MatTableDataSource<any>;
  dataSourceHq1: MatTableDataSource<any>;
  dataSourceParty: MatTableDataSource<any>;
  dataSourceParty1 : MatTableDataSource<any>;
  productDatasource: MatTableDataSource<any>;
  columnsToDisplay = ['stateName', 'amount'];
  columnsToDisplay1 = ['stateName', 'ProductName', 'amount'];
  columnsToDisplayHq = ['Headquater', 'stateName', 'amount'];
  columnsToDisplayHq1 = ['Headquater', 'ProductName', 'amount'];
  columnsToDisplayParty = ['PartyName', 'hqName', 'amount'];
  columnsToDisplayParty1 = ['PartyName', 'ProductName', 'amount'];
  StatewiseDTOptions: any;
  StateDataTable: any;
  HqwiseDTOptions: any;
  HQDataTable: any;
  PartywiseDTOptions: any;
  PartyDataTable: any;
  chartParty: any;
  @Input() data: any;
  fromDate: any;
  toDate: any;
  month: any;
  year: any;
  hqGraphDispose: any;
  partyGraphDispose: any;
  primarySale: any = 0;
  collection: any = 0;
  outstanding: any = 0;
  salesReturn: any = 0;
  selectedItem: any;
  totalState: number;
  totalHq: number;
  totalParty: number;
  totalAmount: any = 0;
  totalAmountHq: any = 0;
  TotalAmountParty: any = 0;
  isToggledProduct = false;

  // @Output() parentData = new EventEmitter();


  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  exportAsConfig3: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element
  }

  exportAsConfig2: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId3', // the id of html/table element
  }

  exportAsConfig1: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element
  }

  constructor(
    private ERPServiceService: ERPServiceService,
    private AmCharts: AmChartsService,
    private changeDetectorRef: ChangeDetectorRef,
    private exportAsService: ExportAsService,
    private _toastrService: ToastrService
  ) { }

  ngOnInit() {
   

    // create chart instance here---
    this.data.forEach(element => {
     
      if (element.type == "Primary") {
        this.primarySale = element.data;
      } else if (element.type == "Collection") {
        this.collection = element.data;
      } else if (element.type == "Outstanding") {
        this.outstanding = element.data;
      } else if (element.type == "SalesReturn") {
        console.log("Hello SalesReturn");
        this.salesReturn = element.data;

       
      }
    });
    let date = new Date().getDate();
    this.month = new Date().getMonth() + 1;
    this.year = new Date().getFullYear();
    this.fromDate = "01" + "-" + this.month + "-" + this.year;
    this.toDate = date + "-" + this.month + "-" + this.year;
    this.getStateWiseData("Primary", 'table');
    this.changeDetectorRef.detectChanges();
  }

  getTotalSalesCount(data) {
    let chart = am4core.create('ERPGraph', am4charts.XYChart)
    // Add data
    data.forEach(item => {
      chart.data.push({
        "type": item.type,
        "data": item.data
      })
    });
    // Create axes

    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "type";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;

    categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
      if (target.dataItem && target.dataItem.index) {
        return dy + 25;
      }
      return dy;
    });

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "data";
    series.dataFields.categoryX = "type";
    series.name = "Sales Data";
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fillOpacity = .8;
    series.columns.template.width = am4core.percent(50);
    let columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 1;
    // add events in it--
    series.columns.template.events.on("hit", function (ev) {
      this.getStateWiseData(ev.target.dataItem.categories.categoryX, "graph");
    }, this);
  }

  typeData = true;
  cardData: any;
  cardType: any;
  getStateWiseData(data, type) {
    this.cardData = data;
    this.cardType = type;
    this.productStatus = false;
    this.isToggledProduct = false;
    let state = [];
    let params1 = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, }
    this.ERPServiceService.getERPStateData(params1).subscribe((stateDataResult) => {
      this.stateData = stateDataResult;

      this.stateData.forEach(element => {
        state.push(parseInt(element.code));
      });
      let stateIds = state.toString();
      this.selectedItem = data;
      this.headerSaleType = data;
      this.showStateWiseDiv = false;
      this.showHQWiseDiv = false;
      this.showPartyWiseDiv = false;
      this.isShowProcessingState = true;
      // Create chart instance
      let params = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.employeeCode,
        stateCode: stateIds,
        APIEndPoint: this.currentUser.company.APIEndPoint
      };
      if (data == "Primary") {
        params['fromDate'] = this.fromDate;
        params['toDate'] = this.toDate;
        params['stateIds'] = "";
      } else if (data == "Collection") {
        params['month'] = this.month;
        params['year'] = this.year;
      } else if (data == "Outstanding") {
        params['month'] = this.month;
        params['year'] = this.year;
      } else if (data == "SalesReturn") {
        params['fromDate'] = this.fromDate;
        params['toDate'] = this.toDate;
        params['stateIds'] = "";
      }

      if (type == "graph") {
        params["type"] = data
      } else {
        params["type"] = data
      }


      this.ERPServiceService.getStateWiseDetails(params).subscribe((res) => {

        if (res == null || res == undefined || res == '' || res == 'null') {
          this.isShowProcessingState = false;
          this.isShowProcessingHq = false;
          this.isShowProcessingParty = false;
          this._toastrService.info('No Record Found....');
        } else {
          let dataProvider = [];
          let result = [];
          this.totalState = 0;

          res.forEach(element => {
            

          


            if (data == "Collection") {
              dataProvider.push({
                "State": element.StateName,
                "Sales": element.AMOUNT,
                "code": element.StateCode
              })
              result.push({
                "StateName": element.StateName,
                "amount": element.AMOUNT,
                "code": element.StateCode,
                "saleType": data
              });
              this.totalState = this.totalState + parseFloat(element.AMOUNT);
            } else if (data == "SalesReturn") {
              dataProvider.push({
                "State": element.StateName,
                "Sales": element.Amount,
                "code": element.StateCode
              })
              result.push({
                "StateName": element.StateName,
                "amount": element.Amount,
                "code": element.StateCode,
                "saleType": data
              });
              this.totalState = this.totalState + parseFloat(element.Amount);
            } else {
              dataProvider.push({
                "State": element.StateName,
                "Sales": element.amount,
                "code": element.StateCode
              })
              result.push({
                "StateName": element.StateName,
                "amount": element.amount,
                "code": element.StateCode,
                "saleType": data
              });
              this.totalState = this.totalState + parseFloat(element.amount);
            }
          });
          this.dataSource = new MatTableDataSource(result);
          this.makeGraph(dataProvider, params);
          //const PrintTableFunction = this.PrintTableFunction.bind(this);
          // this.StatewiseDTOptions = {
          //   "pagingType": 'full_numbers',
          //   "paging": true,
          //   "ordering": true,
          //   "info": true,
          //   "scrollY": 300,
          //   "scrollX": true,
          //   "fixedColumns": true,
          //   "columnDefs": [
          //     { "width": "300px", "targets": 0 },
          //     { "width": "250px", "targets": 1 }],
          //   destroy: true,
          //   data: this.dataSource,
          //   responsive: true,
          //   dom: 'Bfrtip',
          //   buttons: [
          //     {
          //       extend: 'excel',
          //       exportOptions: {
          //         columns: ':visible'
          //       }
          //     },
          //     {
          //       extend: 'csv',
          //       exportOptions: {
          //         columns: ':visible'
          //       }
          //     },
          //     {
          //       extend: 'copy',
          //       exportOptions: {
          //         columns: ':visible'
          //       }
          //     }
          //   ],
          //   columns: [
          //     {
          //       title: 'State',
          //       data: 'StateName',
          //       render: function (data) {
          //         if (data === '') {
          //           return '---'
          //         } else {
          //           return data
          //         }
          //       },
          //       defaultContent: '---'
          //     },
          //     {
          //       title: 'Amount',
          //       data: 'amount',
          //       render: function (data) {
          //         if (data === '') {
          //           return data
          //         } else {
          //           return "<button id='viewHQSale' class='btn btn-warning btn-sm'><i class='fa fa-rupee-sign'></i>&nbsp;&nbsp;" + data + "</button>"
          //         }
          //       },
          //       defaultContent: '---'
          //     }
          //   ],
          //   rowCallback: (row: Node, data: any[] | Object, index: number) => {
          //     const self = this;
          //     $('td', row).unbind('click');
          //     $('td button', row).bind('click', (event) => {
          //       let rowObject = JSON.parse(JSON.stringify(data));
          //       if (event.target.id === "viewHQSale") {
          //         self.getHQWiseData(rowObject, 'table', rowObject['saleType']);
          //       }
          //     });
          //   }
          // }
          // this.StateDataTable = $(this.StatewiseDataTable.nativeElement);
          // this.StateDataTable.DataTable(this.StatewiseDTOptions);
          // this.changeDetectorRef.detectChanges();
        }
      })

    })
  }

  makeGraph(dataProvider, params) {
    am4core.disposeAllCharts();
    let chart: any;
    chart = am4core.create("stateWiseGraph", am4charts.XYChart);
    chart.scrollbarX = new am4core.Scrollbar();
    chart.data = dataProvider;
    // Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "State";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.verticalCenter = "middle";
    categoryAxis.renderer.labels.template.rotation = -30;
    categoryAxis.tooltip.disabled = true;
    categoryAxis.renderer.minHeight = 110;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.minWidth = 50;

    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.sequencedInterpolation = true;
    series.dataFields.valueY = "Sales";
    series.dataFields.categoryX = "State";
    series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
    series.columns.template.strokeWidth = 0;

    series.tooltip.pointerOrientation = "vertical";
    categoryAxis.renderer.cellStartLocation = 0.2;
    categoryAxis.renderer.cellEndLocation = 0.8;
    series.columns.template.width = am4core.percent(40);
    let label = categoryAxis.renderer.labels.template;
    label.wrap = true;
    series.columns.template.column.cornerRadiusTopLeft = 10;
    series.columns.template.column.cornerRadiusTopRight = 10;
    series.columns.template.column.fillOpacity = 0.8;

    // on hover, make corner radiuses bigger
    let hoverState = series.columns.template.column.states.create("hover");
    hoverState.properties.cornerRadiusTopLeft = 0;
    hoverState.properties.cornerRadiusTopRight = 0;
    hoverState.properties.fillOpacity = 1;

    series.columns.template.adapter.add("fill", function (fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    });

    // Cursor
    chart.cursor = new am4charts.XYCursor();
    series.columns.template.events.on("hit", function (ev) {
      this.getHQWiseData(ev.target.dataItem['dataContext'], "graph", params["type"]);
    }, this);
    this.showStateWiseDiv = true;
    this.isShowProcessingState = false;
    this.showStateWiseDiv = true;
    this.changeDetectorRef.detectChanges();
  }

  getHQWiseData(data, type, salesType) {
    console.log('HQ data', data);
    console.log('HQ 2nd', data);
    console.log('HQ third', data);

    this.changeDetectorRef.detectChanges();
    this.showHQWiseDiv = false;
    this.showPartyWiseDiv = false;
    this.isShowProcessingHq = true;
    this.headerSaleType = salesType;
    // Create chart instance
    let params = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.employeeCode,
      APIEndPoint: this.currentUser.company.APIEndPoint,
      stateCode: data['code']
    };

    if (salesType == "Primary") {
      params['fromDate'] = this.fromDate;
      params['toDate'] = this.toDate;
    } else if (salesType == "Collection") {
      params['month'] = this.month;
      params['year'] = this.year;
    } else if (salesType == "SalesReturn") {
      params['fromDate'] = this.fromDate;
      params['toDate'] = this.toDate;
    }

    if (type == "graph") {
      params["type"] = salesType
    } else {
      params["type"] = salesType
    }

    this.ERPServiceService.getHQWiseDetails(params).subscribe((res) => {
      console.log("res : ", res);
      if (res.length > 0) {
        let result = [];
        this.totalHq = 0;
        if (this.hqGraphDispose) {
          this.hqGraphDispose.dispose();
          this.hqGraphDispose = undefined;
        }
        let chartHq = am4core.create("HqWiseGraph", am4charts.XYChart);
        this.hqGraphDispose = chartHq;
        chartHq.scrollbarX = new am4core.Scrollbar();
        res.forEach(element => {
          if (salesType == "Primary") {
            chartHq.data.push({
              "Headquarter": element.Headquater,
              "Sales": element.primSales,
              "HeadquaterCode": element.HeadquaterCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.Headquater,
              "amount": element.primSales,
              "HeadquaterCode": element.HeadquaterCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.primSales);
          } else if (salesType == "Outstanding") {
            chartHq.data.push({
              "Headquarter": element.headquarter,
              "Sales": element.amount,
              "HeadquaterCode": element.headquarterCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.headquarter,
              "amount": element.amount,
              "HeadquaterCode": element.headquarterCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.amount);
          } else if (salesType == "Collection") {
            chartHq.data.push({
              "Headquarter": element.HQ,
              "Sales": element.AMOUNT,
              "HeadquaterCode": element.HQCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQ,
              "amount": element.AMOUNT,
              "HeadquaterCode": element.HQCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.AMOUNT);
          } else if (salesType == "SalesReturn") {
            chartHq.data.push({
              "Headquarter": element.HQ,
              "Sales": element.Amount,
              "HeadquaterCode": element.HQCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQ,
              "amount": element.Amount,
              "HeadquaterCode": element.HQCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.Amount);
          }
        });
        this.headerStateName = result[0].stateName;
        this.dataSourceHq = new MatTableDataSource(result);
        //this.scroll(this.hq as HTMLElement);
        setTimeout(() => {
          this.hqTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        }, 500)
        // this.HqwiseDTOptions = {
        //   "pagingType": 'full_numbers',
        //   "paging": true,
        //   "ordering": true,
        //   "info": true,
        //   "scrollY": 300,
        //   "scrollX": true,
        //   //"fixedColumns": true,
        //   // "columnDefs": [
        //   //   { "width": "300px", "targets": 0 },
        //   //   { "width": "250px", "targets": 1 },
        //   //   { "width": "200px", "targets": 2 }],
        //   destroy: true,
        //   data: this.dataSourceHq,
        //   responsive: true,
        //   "bAutoWidth": true,
        //   dom: 'Bfrtip',
        //   buttons: [
        //     {
        //       extend: 'excel',
        //       exportOptions: {
        //         columns: ':visible'
        //       }
        //     },
        //     {
        //       extend: 'csv',
        //       exportOptions: {
        //         columns: ':visible'
        //       }
        //     },
        //     {
        //       extend: 'copy',
        //       exportOptions: {
        //         columns: ':visible'
        //       }
        //     }
        //   ],
        //   columns: [

        //     {
        //       title: 'Headquarter',
        //       data: 'Headquater',
        //       render: function (data) {
        //         if (data === '') {
        //           return '---'
        //         } else {
        //           return data
        //         }
        //       },
        //       defaultContent: '---'
        //     },
        //     {
        //       title: 'State',
        //       data: 'stateName',
        //       render: function (data) {
        //         if (data === '') {
        //           return '---'
        //         } else {
        //           return data
        //         }
        //       },
        //       defaultContent: '---'
        //     },
        //     {
        //       title: 'Amount',
        //       data: 'amount',
        //       render: function (data) {
        //         if (data === '') {
        //           return '---'
        //         } else {
        //           return "<button id='viewPartySale' class='btn btn-warning btn-sm'><i class='fa fa-rupee-sign'></i>&nbsp;&nbsp;" + data + "</button>"
        //         }
        //       },
        //       defaultContent: '---'
        //     }
        //   ],
        //   rowCallback: (row: Node, data: any[] | Object, index: number) => {
        //     const self = this;
        //     $('td', row).unbind('click');
        //     $('td button', row).bind('click', (event) => {
        //       let rowObject = JSON.parse(JSON.stringify(data));
        //       if (event.target.id === "viewPartySale") {
        //         self.getPartyWiseData(rowObject, 'table', rowObject['saleType']);
        //         this.changeDetectorRef.detectChanges();
        //       }
        //     });
        //   }
        // }
        // this.HQDataTable = $(this.HqwiseDataTable.nativeElement);
        // this.HQDataTable.DataTable(this.HqwiseDTOptions);
        // Create axes
        let categoryAxis = chartHq.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "Headquarter";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = -30;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        let valueAxis = chartHq.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        // Create series
        let series = chartHq.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "Sales";
        series.dataFields.categoryX = "Headquarter";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;

        series.tooltip.pointerOrientation = "vertical";
        categoryAxis.renderer.cellStartLocation = 0.2;
        categoryAxis.renderer.cellEndLocation = 0.8;
        series.columns.template.width = am4core.percent(40);
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;
        let label = categoryAxis.renderer.labels.template;
        label.wrap = true;
        // on hover, make corner radiuses bigger
        let hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", function (fill, target) {
          return chartHq.colors.getIndex(target.dataItem.index);
        });

        // Cursor
        chartHq.cursor = new am4charts.XYCursor();
        series.columns.template.events.on("hit", function (ev) {
          this.getPartyWiseData(ev.target.dataItem['dataContext'], "graph", salesType);
        }, this);
        this.showHQWiseDiv = true;
        this.isShowProcessingHq = false;
      }
      this.changeDetectorRef.detectChanges();
    })
  }

  getPartyWiseData(data, type, salesType) {
    // Create chart instance
    this.showPartyWiseDiv = false;
    this.isShowProcessingParty = true;
    this.headerSaleType = salesType;
    // Create chart instance
    let params = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.employeeCode,
      APIEndPoint: this.currentUser.company.APIEndPoint,
      headquaterCode: data['HeadquaterCode']
    };
    if (salesType == "Primary") {
      params['fromDate'] = this.fromDate;
      params['toDate'] = this.toDate;
    } else if (salesType == "Collection") {
      params['month'] = this.month;
      params['year'] = this.year;
    } else if (salesType == "SalesReturn") {
      params['fromDate'] = this.fromDate;
      params['toDate'] = this.toDate;
    }

    if (type == "graph") {
      params["type"] = salesType
    } else {
      params["type"] = salesType
    }
    this.ERPServiceService.getPartyWiseDetails(params).subscribe((res) => {
      if (res.length > 0) {
        if (this.partyGraphDispose) {
          this.partyGraphDispose.dispose();
          this.partyGraphDispose = undefined;
        }
        let chartParty = am4core.create("PartyWiseGraph", am4charts.XYChart);
        this.partyGraphDispose = chartParty;
        chartParty.scrollbarX = new am4core.Scrollbar();
        let result = [];
        this.totalParty = 0;
        res.forEach(element => {
          if (salesType == "Primary") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.primSales,
              "PartyCode": element.PartyCode
            })
            result.push({
              //"stateName":element.StateName,
              "hqName": element.Headquater,
              "PartyName": element.PartyName,
              "amount": element.primSales,
            });
            this.totalParty = this.totalParty + parseFloat(element.primSales);
          } else if (salesType == "Outstanding") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.amount,
              "PartyCode": element.PartyCode
            })
            result.push({
              "hqName": element.headquarter,
              "PartyName": element.PartyName,
              "amount": element.amount,
            });
            this.totalParty = this.totalParty + parseFloat(element.amount);
          } else if (salesType == "Collection") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.AMOUNT,
              "PartyCode": element.PartyCode
            })
            result.push({
              "hqName": element.HQ,
              "PartyName": element.PartyName,
              "amount": element.AMOUNT,
            });
            this.totalParty = this.totalParty + parseFloat(element.AMOUNT);
          } else if (salesType == "SalesReturn") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.Amount,
              "PartyCode": element.party_code
            })
            result.push({
              //"stateName":element.StateName,
              "hqName": element.HQ,
              "PartyName": element.PartyName,
              "amount": element.Amount,
            });
            this.totalParty = this.totalParty + parseFloat(element.Amount);
          }
        });
        this.headerHqName = result[0].hqName;
        this.dataSourceParty = new MatTableDataSource(result);
        // this.PartywiseDTOptions = {
        //   "pagingType": 'full_numbers',
        //   "paging": true,
        //   "ordering": true,
        //   "info": true,
        //   "scrollY": 300,
        //   "scrollX": true,
        //   "fixedColumns": true,
        //   "columnDefs": [
        //     { "width": "300px", "targets": 0 },
        //     { "width": "250px", "targets": 1 },
        //     { "width": "200px", "targets": 2 }],
        //   destroy: true,
        //   data: this.dataSourceParty,
        //   responsive: true,
        //   dom: 'Bfrtip',
        //   buttons: [
        //     {
        //       extend: 'excel',
        //       exportOptions: {
        //         columns: ':visible'
        //       }
        //     },
        //     {
        //       extend: 'csv',
        //       exportOptions: {
        //         columns: ':visible'
        //       }
        //     },
        //     {
        //       extend: 'copy',
        //       exportOptions: {
        //         columns: ':visible'
        //       }
        //     }
        //   ],
        //   columns: [

        //     {
        //       title: 'Stockist',
        //       data: 'PartyName',
        //       render: function (data) {
        //         if (data === '') {
        //           return '---'
        //         } else {
        //           return data
        //         }
        //       },
        //       defaultContent: '---'
        //     },
        //     {
        //       title: 'Headquarter',
        //       data: 'hqName',
        //       render: function (data) {
        //         if (data === '') {
        //           return '---'
        //         } else {
        //           return data
        //         }
        //       },
        //       defaultContent: '---'
        //     },
        //     {
        //       title: 'Amount',
        //       data: 'amount',
        //       render: function (data) {
        //         if (data === '') {
        //           return '---'
        //         } else {
        //           return "<button class='btn btn-warning btn-sm'><i class='fa fa-rupee-sign'></i>&nbsp;&nbsp;" + data + "</button>"
        //         }
        //       },
        //       defaultContent: '---'
        //     }
        //   ],
        //   rowCallback: (row: Node, data: any[] | Object, index: number) => {
        //     const self = this;
        //     $('td', row).unbind('click');
        //     $('td button', row).bind('click', (event) => {
        //       let rowObject = JSON.parse(JSON.stringify(data));
        //     });
        //   }

        // }
        // //this.changeDetectorRef.detectChanges();
        // this.PartyDataTable = $(this.PartywiseDataTable.nativeElement);
        // this.PartyDataTable.DataTable(this.PartywiseDTOptions);
        // Create axes
        let categoryAxis = chartParty.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "PartyName";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = -30;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        let valueAxis = chartParty.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        // Create series
        let series = chartParty.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "Sales";
        series.dataFields.categoryX = "PartyName";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;
        let label = categoryAxis.renderer.labels.template;
        label.wrap = true;
        series.tooltip.pointerOrientation = "vertical";
        categoryAxis.renderer.cellStartLocation = 0.2;
        categoryAxis.renderer.cellEndLocation = 0.8;
        series.columns.template.width = am4core.percent(40);
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;

        // on hover, make corner radiuses bigger
        let hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", function (fill, target) {
          return chartParty.colors.getIndex(target.dataItem.index);
        });

        // Cursor
        chartParty.cursor = new am4charts.XYCursor();
        series.columns.template.events.on("hit", function (ev) {
          //this.getHQWiseDetails(ev.target.dataItem['dataContext'], "graph",'');
        }, this);
        this.showPartyWiseDiv = true;
        this.isShowProcessingParty = false;
        setTimeout(() => {
          this.partyTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        }, 500)
        this.changeDetectorRef.detectChanges();
      }
    })
    this.changeDetectorRef.detectChanges();
  }

  // scroll(el: HTMLElement) {
  //   console.log('el', el);
  //   el.scrollIntoView();
  // }

  applyFilterState(eventState: Event) {
    const filterValueState = (eventState.target as HTMLInputElement).value;
    this.dataSource.filter = filterValueState.trim().toLowerCase();
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.productDatasource.filter = filterValue;
    this.dataSourceHq1.filter = filterValue;
  }

  applyFilterHq(eventHq: Event) {
    const filterValueHq = (eventHq.target as HTMLInputElement).value;
    this.dataSourceHq.filter = filterValueHq.trim().toLowerCase();
  }

  applyFilterParty(eventParty: Event) {
    const filterValueParty = (eventParty.target as HTMLInputElement).value;
    this.dataSourceParty.filter = filterValueParty.trim().toLowerCase();
  }
  exportToExcelState() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.StatewiseDataTable.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    let fileName = "State Wise " + this.selectedItem + " Data.xlsx";
    XLSX.writeFile(wb, fileName);
  }


  exporttoExcelStateProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Employee Attendance Report').subscribe(() => {
      // save started
    });
  }
  exporttoExcelHqProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'State Wise Report').subscribe(() => {
      // save started
    });
  }

  exporttoExcelHeadquarterProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig3, 'Headquarter Wise Report').subscribe(() => {
      // save started
    });
  }

  exportToExcelHq() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.HqwiseDataTable.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    let fileName = "Headquarter Wise " + this.selectedItem + " Data.xlsx";
    XLSX.writeFile(wb, fileName);
  }

  exporttoExcelPartyProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig2, 'Party Wise Report').subscribe(() => {
      // save started
    });
  }

  exportToExcelParty() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.PartywiseDataTable.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    let fileName = "Stockist Wise " + this.selectedItem + " Data.xlsx";
    XLSX.writeFile(wb, fileName);
  }


  isToggledState = false;
  colMdState = "col-6";

  // --------------------------------------GOURAV 30-03-2021 ---------------------------------------

  productStatus: boolean = false;

  toggleProduct(event: MatSlideToggleChange) {
    this.isToggledProduct = event.checked;
    if (event.checked) {
      this.getStateWiseProductReport(this.cardData, this.cardType);
      this.productStatus = true;
    } else {
      this.productStatus = false;
      this.getStateWiseData(this.cardData, this.cardType);
    }
  }


  getStateWiseProductReport(data, type) {
    this.totalAmount=0;
    let key = false;
    let state = [];
    let params1 = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, }
    this.ERPServiceService.getERPStateData(params1).subscribe((stateDataResult) => {
      this.stateData = stateDataResult;
      this.stateData.forEach(element => {
        state.push(parseInt(element.code));
      });
      let stateIds = state.toString();
      this.selectedItem = data;
      this.headerSaleType = data;
      this.showStateWiseDiv = false;
      this.showHQWiseDiv = false;
      this.showPartyWiseDiv = false;
      this.isShowProcessingState = true;
      // Create chart instance
      let params = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.employeeCode,
        stateCode: stateIds,
        APIEndPoint: this.currentUser.company.APIEndPoint
      };
      if (data == "Primary") {
        key=true;
        params['fromDate'] = this.fromDate;
        params['toDate'] = this.toDate;
        params['stateIds'] = "";
      } else if (data == "Collection") {
        key=false;
        params['month'] = this.month;
        params['year'] = this.year;
      } else if (data == "Outstanding") {
        key=false;
        params['month'] = this.month;
        params['year'] = this.year;
      } else if (data == "SalesReturn") {
        key=false;
        params['fromDate'] = this.fromDate;
        params['toDate'] = this.toDate;
        params['stateIds'] = "";
      }

      if (type == "graph") {
        params["type"] = data
      } else {
        params["type"] = data
      }
      this.ERPServiceService.getStateWiseProductDetails(params).subscribe((res) => {
        if (res == null || res == undefined || res == '' || res == 'null') {
          this.isShowProcessingState = false;
          this.isShowProcessingHq = false;
          this.isShowProcessingParty = false;
          this._toastrService.info('No Record Found....');
        } else {
          let dataProvider = [];
          let result = [];
          this.totalState = 0;
          res.forEach(element => {
            if (data == "Collection") {
              dataProvider.push({
                "State": element.StateName,
                "Sales": element.AMOUNT,
                "ProductName": element.ProductName,
                "code": element.StateCode
              })
              result.push({
                "StateName": element.StateName,
                "amount": element.AMOUNT,
                "code": element.StateCode,
                "ProductName": element.ProductName,
                "saleType": data
              });
              this.totalAmount = this.totalAmount + parseFloat(element.AMOUNT);
          
            } else if (data == "SalesReturn") {
              dataProvider.push({
                "State": element.StateName,
                "Sales": element.Amount,
                "ProductName": element.ProductName,
                "code": element.StateCode
              })
              result.push({
                "StateName": element.StateName,
                "amount": element.Amount,
                "code": element.StateCode,
                "ProductName": element.ProductName,
                "saleType": data
              });
              let val = element.creditvalue;
              let total = element.qty * val;
              this.totalAmount = this.totalAmount + total;
                
            } else if (data == "Outstanding") {
              dataProvider.push({
                "State": element.StateName,
                "Sales": element.amount,
                "ProductName": element.ProductName,
                "code": element.StateCode
              })
              result.push({
                "StateName": element.StateName,
                "amount": element.amount,
                "code": element.StateCode,
                "ProductName": element.ProductName,
                "saleType": data
              });
              
              this.totalAmount = this.totalAmount + parseFloat(element.amount);
              
            } else {
              this.showPrimary = true;
              dataProvider.push({
                "State": element.StateName,
                "Sales": element.amount,
                "ProductName": element.ProductName,
                "code": element.StateCode
              })
              result.push({
                "StateName": element.StateName,
                "amount": (element.qty*element.creditvalue),
                "code": element.StateCode,
                "ProductName": element.ProductName,
                "saleType": data
              });
              let total = element.qty * element.creditvalue;
              this.totalAmount = this.totalAmount + total;
            }
          });         
          this.productDatasource = new MatTableDataSource(result);
          this.makeGraph(dataProvider, params);
        }
      });
    });
  }

  getHQWiseProductData(req, type, selectedType) {
    let state = [];
    console.log(req);
    
    this.totalAmountHq = 0;
    this.changeDetectorRef.detectChanges();
    this.showHQWiseDiv = false;
    this.showPartyWiseDiv = false;
    this.isShowProcessingHq = true;
    this.headerSaleType = selectedType;
    // Create chart instance

    let params1 = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, }
    this.ERPServiceService.getERPStateData(params1).subscribe((stateDataResult) => {
      this.stateData = stateDataResult;
      this.stateData.forEach(element => {
        state.push(parseInt(element.code));
      });
      // let stateids = state.toString();
      let stateids = req.code;
      let params = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.employeeCode,
        APIEndPoint: this.currentUser.company.APIEndPoint,
        stateCode: stateids
      };
      if (selectedType == "Primary") {
        params['fromDate'] = this.fromDate;
        params['toDate'] = this.toDate;
      } else if (selectedType == "Collection") {
        params['month'] = this.month;
        params['year'] = this.year;
      } else if (selectedType == "SalesReturn") {
        params['fromDate'] = this.fromDate;
        params['toDate'] = this.toDate;
      }

      if (type == "graph") {
        params["type"] = selectedType
      } else {
        params["type"] = selectedType
      }

      this.ERPServiceService.getHQWiseProductDetails(params).subscribe((res) => {
        if (res.length > 0) {
          let result = [];
          this.totalHq = 0;
          if (this.hqGraphDispose) {
            this.hqGraphDispose.dispose();
            this.hqGraphDispose = undefined;
          }
          let chartHq = am4core.create("HqWiseGraph", am4charts.XYChart);
          this.hqGraphDispose = chartHq;
          chartHq.scrollbarX = new am4core.Scrollbar();
          res.forEach(element => {
            if (selectedType == "Primary") {
              chartHq.data.push({
                "Headquarter": element.Headquater,
                "Sales": element.primSales,
                "HeadquaterCode": element.HeadquaterCode,
                "ProductName": element.ProductName
              })
              result.push({
                "stateName": element.StateName,
                "Headquater": element.Headquater,
                "amount": element.primSales,
                "ProductName": element.ProductName,
                "HeadquaterCode": element.HeadquaterCode,
                "selectedType": selectedType
              });
              let val = element.creditvalue;
              let total = element.qty * val;
              this.totalAmountHq = this.totalAmountHq + total;
            } else if (selectedType == "Outstanding") {
              chartHq.data.push({
                "Headquarter": element.headquarter,
                "Sales": element.amount,
                "HeadquaterCode": element.headquarterCode,
                "ProductName": element.ProductName
              })
              result.push({
                "stateName": element.StateName,
                "Headquater": element.headquarter,
                "amount": element.amount,
                "HeadquaterCode": element.headquarterCode,
                "ProductName": element.ProductName,
                "selectedType": selectedType
              });
              this.totalAmountHq = this.totalAmountHq + parseFloat(element.amount);
            } else if (selectedType == "Collection") {
              chartHq.data.push({
                "Headquarter": element.HQ,
                "Sales": element.AMOUNT,
                "HeadquaterCode": element.HQCode,
                "ProductName": element.ProductName
              })
              result.push({
                "stateName": element.StateName,
                "Headquater": element.HQ,
                "amount": element.AMOUNT,
                "HeadquaterCode": element.HQCode,
                "ProductName": element.ProductName,
                "selectedType": selectedType
              });
              this.totalAmountHq = this.totalAmountHq + parseFloat(element.AMOUNT);
            } else if (selectedType == "SalesReturn") {
              chartHq.data.push({
                "Headquarter": element.HQ,
                "Sales": element.Amount,
                "HeadquaterCode": element.HQCode,
                "ProductName": element.ProductName
              })
              result.push({
                "stateName": element.StateName,
                "Headquater": element.HQ,
                "amount": element.Amount,
                "HeadquaterCode": element.HQCode,
                "ProductName": element.ProductName,
                "selectedType": selectedType
              });
              let val = element.creditvalue;
              let total = element.Amount * val;
              this.totalAmountHq = this.totalAmountHq + total;
            }
          });
          this.headerStateName = result[0].stateName;
          this.dataSourceHq1 = new MatTableDataSource(result);
          //this.scroll(this.hq as HTMLElement);
          setTimeout(() => {
            this.hqTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
          }, 500)

          let categoryAxis = chartHq.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "Headquarter";
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.renderer.minGridDistance = 30;
          categoryAxis.renderer.labels.template.horizontalCenter = "right";
          categoryAxis.renderer.labels.template.verticalCenter = "middle";
          categoryAxis.renderer.labels.template.rotation = -30;
          categoryAxis.tooltip.disabled = true;
          categoryAxis.renderer.minHeight = 110;

          let valueAxis = chartHq.yAxes.push(new am4charts.ValueAxis());
          valueAxis.renderer.minWidth = 50;

          // Create series
          let series = chartHq.series.push(new am4charts.ColumnSeries());
          series.sequencedInterpolation = true;
          series.dataFields.valueY = "Sales";
          series.dataFields.categoryX = "Headquarter";
          series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
          series.columns.template.strokeWidth = 0;

          series.tooltip.pointerOrientation = "vertical";
          categoryAxis.renderer.cellStartLocation = 0.2;
          categoryAxis.renderer.cellEndLocation = 0.8;
          series.columns.template.width = am4core.percent(40);
          series.columns.template.column.cornerRadiusTopLeft = 10;
          series.columns.template.column.cornerRadiusTopRight = 10;
          series.columns.template.column.fillOpacity = 0.8;
          let label = categoryAxis.renderer.labels.template;
          label.wrap = true;
          // on hover, make corner radiuses bigger
          let hoverState = series.columns.template.column.states.create("hover");
          hoverState.properties.cornerRadiusTopLeft = 0;
          hoverState.properties.cornerRadiusTopRight = 0;
          hoverState.properties.fillOpacity = 1;

          series.columns.template.adapter.add("fill", function (fill, target) {
            return chartHq.colors.getIndex(target.dataItem.index);
          });

          // Cursor
          chartHq.cursor = new am4charts.XYCursor();
          series.columns.template.events.on("hit", function (ev) {
            this.getPartyWiseData(ev.target.dataItem['dataContext'], "graph", selectedType);
          }, this);
          this.showHQWiseDiv = true;
          this.isShowProcessingHq = false;
        }
        this.changeDetectorRef.detectChanges();
      })
    });
  }

  getPartyWiseProductData(req, type, selectedType) {
    this.showPartyWiseDiv = false;
    this.isShowProcessingParty = true;
    this.headerSaleType = selectedType;
    // Create chart instance
    let params = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.employeeCode,
      APIEndPoint: this.currentUser.company.APIEndPoint,
      headquaterCode: req['HeadquaterCode']
    };
    if (selectedType == "Primary") {
      params['fromDate'] = this.fromDate;
      params['toDate'] = this.toDate;
    } else if (selectedType == "Collection") {
      params['month'] = this.month;
      params['year'] = this.year;
    } else if (selectedType == "SalesReturn") {
      params['fromDate'] = this.fromDate;
      params['toDate'] = this.toDate;
    }

    if (type == "graph") {
      params["type"] = selectedType
    } else {
      params["type"] = selectedType
    }
    this.ERPServiceService.getPartyWiseProductDetails(params).subscribe((res) => {
      if (res.length > 0) {
        if (this.partyGraphDispose) {
          this.partyGraphDispose.dispose();
          this.partyGraphDispose = undefined;
        }
        let chartParty = am4core.create("PartyWiseGraph", am4charts.XYChart);
        this.partyGraphDispose = chartParty;
        chartParty.scrollbarX = new am4core.Scrollbar();
        let result = [];
        this.totalParty = 0;
        res.forEach(element => {
          if (selectedType == "Primary") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.primSales,
              "PartyCode": element.PartyCode,
              "ProductName": element.ProductName
            })
            result.push({
              //"stateName":element.StateName,
              "hqName": element.Headquater,
              "PartyName": element.PartyName,
              "ProductName": element.ProductName,
              "amount": (element.creditvalue*element.qty)
            });
            
              let total = element.creditvalue * element.qty;
              this.totalParty = this.totalParty + total;
          } else if (selectedType == "Outstanding") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.amount,
              "ProductName": element.ProductName,
              "PartyCode": element.PartyCode
            })
            result.push({
              "hqName": element.headquarter,
              "PartyName": element.PartyName,
              "ProductName": element.ProductName,
              "amount": element.amount
            });
            this.totalParty = this.totalParty + parseFloat(element.amount);
          } else if (selectedType == "Collection") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.AMOUNT,
              "ProductName": element.ProductName,
              "PartyCode": element.PartyCode
            })
            result.push({
              "hqName": element.HQ,
              "PartyName": element.PartyName,
              "ProductName": element.ProductName,
              "amount": element.AMOUNT
            });
            let val = element.creditvalue;
            let total = element.AMOUNT * val;
            this.totalParty = this.totalParty + total;
          } else if (selectedType == "SalesReturn") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.Amount,
              "ProductName": element.ProductName,
              "PartyCode": element.party_code
            })
            result.push({
              "hqName": element.HQ,
              "PartyName": element.PartyName,
              "ProductName": element.ProductName,
              "amount": element.Amount
            });
            let val = element.creditvalue;
              let total = element.Amount * val;
              this.totalParty = this.totalParty + total;
          }
        });
        this.headerHqName = result[0].hqName;
        this.dataSourceParty1 = new MatTableDataSource(result);

        let categoryAxis = chartParty.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "PartyName";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = -30;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        let valueAxis = chartParty.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        // Create series
        let series = chartParty.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "Sales";
        series.dataFields.categoryX = "PartyName";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;
        let label = categoryAxis.renderer.labels.template;
        label.wrap = true;
        series.tooltip.pointerOrientation = "vertical";
        categoryAxis.renderer.cellStartLocation = 0.2;
        categoryAxis.renderer.cellEndLocation = 0.8;
        series.columns.template.width = am4core.percent(40);
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;

        // on hover, make corner radiuses bigger
        let hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", function (fill, target) {
          return chartParty.colors.getIndex(target.dataItem.index);
        });

        // Cursor
        chartParty.cursor = new am4charts.XYCursor();
        series.columns.template.events.on("hit", function (ev) {
          //this.getHQWiseDetails(ev.target.dataItem['dataContext'], "graph",'');
        }, this);
        this.showPartyWiseDiv = true;
        this.isShowProcessingParty = false;
        setTimeout(() => {
          this.partyTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        }, 500)
        this.changeDetectorRef.detectChanges();
      }
    })
    this.changeDetectorRef.detectChanges();
  }

  // --------------------------------------GOURAV 30-03-2021 ---------------------------------------
  toggleFilterState(event: MatSlideToggleChange) {
    this.isToggledState = event.checked;
    if (event.checked) {
      this.colMdState = "col-12";
    } else {
      this.colMdState = "col-6";
    }
  }
  isToggledHq = false;
  colMdHq = "col-6";
  toggleFilterHq(event: MatSlideToggleChange) {
    this.isToggledHq = event.checked;
    if (event.checked) {
      this.colMdHq = "col-12";
    } else {
      this.colMdHq = "col-6";
    }
  }
  isToggledParty = false;
  colMdParty = "col-6";
  toggleFilterParty(event: MatSlideToggleChange) {
    this.isToggledParty = event.checked;
    if (event.checked) {
      this.colMdParty = "col-12";
    } else {
      this.colMdParty = "col-6";
    }
  }


}
