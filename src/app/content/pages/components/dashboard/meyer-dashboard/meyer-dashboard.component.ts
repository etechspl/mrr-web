import { Component, OnInit } from '@angular/core';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'm-meyer-dashboard',
  templateUrl: './meyer-dashboard.component.html',
  styleUrls: ['./meyer-dashboard.component.scss']
})
export class MeyerDashboardComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);

  private chart: any;
  displayedColumns = ['sno', 'headquarter', 'target', 'primary', 'secondary'];
  dataSource;
  displayedColumnsForTrafficLight = ['employeeName', 'state', 'district', 'target', 'primary', 'secondary', 'drCallAvg', 'score'];
  dataSourceForTrafficLight;


  constructor(
    private AmCharts: AmChartsService
  ) { }

  ngOnInit() {


    this.dataSource = [{
      'sno': 1,
      'headquarter': 'ARARIA',
      'target': '705890',
      'primary': '550860',
      'secondary': '155030'
    }, {
      'sno': 2,
      'headquarter': 'AURANGABAD',
      'target': '896540',
      'primary': '796540',
      'secondary': '100000'
    }, {
      'sno': 3,
      'headquarter': 'Abhor',
      'target': '752960',
      'primary': '120000',
      'secondary': '632960'
    }, {
      'sno': 4,
      'headquarter': 'BUXAR',
      'target': '900000',
      'primary': '690000',
      'secondary': '210000'
    }, {
      'sno': 5,
      'headquarter': 'DARBHANGA',
      'target': '850000',
      'primary': '759850',
      'secondary': '90150'
    }, {
      'sno': 6,
      'headquarter': 'GAYA',
      'target': '950000',
      'primary': '852310',
      'secondary': '97690'
    }, {
      'sno': 7,
      'headquarter': 'KATIHAR',
      'target': '700000',
      'primary': '682000',
      'secondary': '180000'
    }, {
      'sno': 8,
      'headquarter': 'MADHUBANI',
      'target': '765000',
      'primary': '600000',
      'secondary': '165000'
    }, {
      'sno': 9,
      'headquarter': 'MUZAFFAPUR',
      'target': '823000',
      'primary': '650000',
      'secondary': '173000'
    }, {
      'sno': 10,
      'headquarter': 'SAMASTIPUR',
      'target': '682500',
      'primary': '450000',
      'secondary': '232500'
    },]
    this.chart = this.AmCharts.makeChart("targetAchievementGraphMeyer", {
      "type": "serial",
      "categoryField": "category",
      "startDuration": 1,
      "categoryAxis": {
        "gridPosition": "start"
      },
      "trendLines": [],
      "mouseWheelScrollEnabled": true,
      // "mouseWheelZoomEnabled": false,
      "chartScrollbar": {
        "autoGridCount": true,
        "hideResizeGrips": true,
        "scrollbarHeight": 20



      },
      "graphs": [{
        "balloonText": "[[title]] of [[category]]:[[value]]L",
        "balloonFunction": function (graphDataItem, graphs) {
          var value = graphDataItem.values.value * 100000;
          return graphs.title + " of " + graphDataItem.category + ": ₹" + value;
        },
        "fillAlphas": 1,
        "id": "AmGraph-1",
        "title": "Target",
        "type": "column",
        "valueField": "column-1",
        "labelText": "[[column-1]]",
        "fixedColumnWidth": 22,
        "colorField": "red"

      },
      {
        "balloonText": "[[title]] of [[category]]:[[value]]L",
        "balloonFunction": function (graphDataItem, graphs) {
          var value = graphDataItem.values.value * 100000;
          return graphs.title + " of " + graphDataItem.category + ": ₹" + value;
        },
        "fillAlphas": 1,
        "id": "AmGraph-2",
        "title": "Primary Sales",
        "type": "column",
        "valueField": "column-2",
        "labelText": "[[column-2]]",
        "fixedColumnWidth": 22
      },
      {
        "balloonText": "[[title]] of [[category]]:[[value]]L",
        "balloonFunction": function (graphDataItem, graphs) {
          var value = graphDataItem.values.value * 100000;
          return graphs.title + " of " + graphDataItem.category + ": ₹" + value;
        },

        "fillAlphas": 1,
        "id": "AmGraph-3",
        "title": "Secondary Sales",
        "type": "column",
        "valueField": "column-3",
        "labelText": "[[column-3]]",
        "fixedColumnWidth": 22
      }
      ],
      "guides": [],
      "valueAxes": [{
        "id": "ValueAxis-1",
        "title": "₹ in Lakhs",
        // "unit": "L",
        "labelFunction": function (value, valueText, valueAxis) {
          if (value < 100) {
            return "" + value + "L"
          } else {
            return "" + 1 + "Cr."
          }

        }
      }],
      "allLabels": [],
      "balloon": {},
      "legend": {
        "enabled": true,
        "useGraphSettings": true
      },
      "titles": [{
        "id": "Title-1",
        "size": 15,
        "text": "Sale Status February'2019 in Lakhs"
      }],
      "dataProvider": [{
        "category": "Bihar",
        "column-1": 60.23,
        "column-2": 30.20,
        "column-3": 10.57
      },
      {
        "category": "Haryana",
        "column-1": 80.15,
        "column-2": 60.35,
        "column-3": 20.50
      },
      {
        "category": "Himachal Pradesh",
        "column-1": 60.26,
        "column-2": 40.34,
        "column-3": 10.40
      },
      {
        "category": "Maharashtra",
        "column-1": 80.40,
        "column-2": 60.78,
        "column-3": 20.82
      },
      {
        "category": "Madhya Pradesh",
        "column-1": 80.68,
        "column-2": 70.58,
        "column-3": 10.74
      }, {
        "category": "Punjab",
        "column-1": 70.25,
        "column-2": 60.75,
        "column-3": 10.25
      },
      {
        "category": "Rajasthan",
        "column-1": 60.69,
        "column-2": 40.10,
        "column-3": 20.21
      },
      {
        "category": "Uttar Pradesh",
        "column-1": 80.03,
        "column-2": 70.72,
        "column-3": 20.00
      },
      ],
      "export": {
        "enabled": true
      }

    });

    this.chart = this.AmCharts.makeChart("targetAchievementHqGraphMeyer", {
      "type": "serial",
      "categoryField": "state",
      "startDuration": 1,
      "categoryAxis": {
        "gridPosition": "start"
      },
      "chartScrollbar": {
        "autoGridCount": true,
        "hideResizeGrips": true,
        "scrollbarHeight": 20,
        "maximum": 10,
        "minimum": 5

      },
      "trendLines": [],
      "graphs": [{
        "balloonText": "[[title]] of [[state]]:[[value]]",
        "bullet": "round",
        "id": "AmGraph-1",
        "title": "Target",
        "valueField": "column-1",
        "labelText": "[[column-1]]"

      }, {
        "balloonText": "[[title]] of [[state]]:[[value]]",
        "bullet": "round",
        "id": "AmGraph-2",
        "title": "Primary",
        "valueField": "column-2",
        "labelText": "[[column-2]]"
      }, {
        "balloonText": "[[title]] of [[state]]:[[value]]",
        "bullet": "round",
        "id": "AmGraph-3",
        "title": "Secondary",
        "valueField": "column-3",
        "labelText": "[[column-3]]"
      }],
      "guides": [],
      "valueAxes": [{
        "id": "ValueAxis-1",
        "title": "₹ in Lakhs",
        "unit": "L"
      }],
      "allLabels": [],
      "balloon": {},
      "legend": {
        "enabled": true,
        "useGraphSettings": true,
        "markerSize": 5,
        "verticalGap": 10
      },
      "export": {
        "enabled": true
      },
      "titles": [{
        "id": "Title-1",
        "size": 15,
        "text": ""
      }],
      "dataProvider": [{
        "state": "ARARIA",
        "column-1": 7.05,
        "column-2": 5.50,
        "column-3": 1.55
      }, {
        "state": "AURANGABAD",
        "column-1": 8.96,
        "column-2": 7.96,
        "column-3": 1.00
      }, {
        "state": "Abhor",
        "column-1": 7.5,
        "column-2": 5.20,
        "column-3": 1.32
      }, {
        "state": "BUXAR",
        "column-1": 9.00,
        "column-2": 6.90,
        "column-3": 2.10
      }, {
        "state": "DARBHANGA",
        "column-1": 8.50000,
        "column-2": 7.59,
        "column-3": 0.90
      }, {
        "state": "GAYA",
        "column-1": 9.50,
        "column-2": 8.52,
        "column-3": 1.00
      }, {
        "state": "KATIHAR",
        "column-1": 7.00,
        "column-2": 6.82,
        "column-3": 1.8
      }, {
        "state": "MADHUBANI",
        "column-1": 7.65,
        "column-2": 6.00,
        "column-3": 1.02
      }, {
        "state": "MUZAFFAPUR",
        "column-1": 8.23,
        "column-2": 6.50,
        "column-3": 1.73
      }, {
        "state": "SAMASTIPUR",
        "column-1": 6.82,
        "column-2": 4.50,
        "column-3": 2.00
      }],
      "listeners": [{
        "event": "clickGraphItem",
        "method": function (event) {
          alert("Getting Data...")
        }
      }]
    });


    this.dataSourceForTrafficLight = [{
      "employeeName": "Praveen Kumar",
      "state": "Delhi",
      "district": "South Delhi",
      "target": "896540",
      "primary": "796540",
      "secondary": "100000",
      "drCallAvg": "98.5",
      "score": "98",
      "rowColor": "A5D6A7"
    }, {
      "employeeName": "Anjana Rana",
      "state": "Punjab",
      "district": "Nangal",
      "target": "896540",
      "primary": "796540",
      "secondary": "100000",
      "drCallAvg": "97",
      "score": "96.5",
      "rowColor": "A5D6A7"
    }, {
      "employeeName": "Praveen Singh",
      "state": "Rajasthan",
      "district": "Pali",
      "target": "896540",
      "primary": "796540",
      "secondary": "100000",
      "drCallAvg": "93",
      "score": "90",
      "rowColor": "A5D6A7"
    }, {
      "employeeName": "Ravindra Singh",
      "state": "Haryana",
      "district": "Bhiwani",
      "target": "896540",
      "primary": "796540",
      "secondary": "100000",
      "drCallAvg": "83.5",
      "score": "84",
      "rowColor": "FFF9C4"
    }, {
      "employeeName": "Rahul Kumar",
      "state": "Punjab",
      "district": "Nangal",
      "target": "896540",
      "primary": "796540",
      "secondary": "100000",
      "drCallAvg": "85",
      "score": "81",
      "rowColor": "FFF9C4"
    }, {
      "employeeName": "Preeti",
      "state": "Haryana",
      "district": "Rohtak",
      "target": "896540",
      "primary": "796540",
      "secondary": "100000",
      "drCallAvg": "80",
      "score": "78",
      "rowColor": "ffcdd2"
    }, {
      "employeeName": "Chaman Deep Singh",
      "state": "Bihar",
      "district": "Buxar",
      "drCallAvg": "79.5",
      "target": "896540",
      "primary": "796540",
      "secondary": "100000",
      "score": "75.5",
      "rowColor": "ffcdd2"
    }]




    //----------------------Scattered Chart----------------------------
    this.chart = this.AmCharts.makeChart("scatteredChart", {
      "type": "xy",
      "startDuration": 1.5,
      "theme": "default",
      "chartScrollbar": {
        "enabled": true
      },
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "Division Name : <b>[[title]]</b></br> Total Visit ::<b>[[y]]</b><br>User Name:<b>[[TCName]]</b>",
          "bullet": "circle",
          "id": "AmGraph-1",
          "lineAlpha": 0,
          "lineColor": "#b0de09",
          "title": "Meyer",
          "valueField": "TCName",
          "xField": "x",
          "yField": "y"
        },
        {
          "balloonText": "Division Name : <b>[[title]]</b></br> Total Visit :<b>[[y]]</b><br>User Name:<b>[[TCName2]]</b>",
          "bullet": "round",
          "id": "AmGraph-2",
          "lineAlpha": 0,
          "lineColor": "#fcd202",
          "title": "Cellage",
          "valueField": "TCName2",
          "xField": "x2",
          "yField": "y2"
        },
        {
          "balloonText": "Division Name : <b>[[title]]</b></br> Total Visit :<b>[[y]]</b><br>User Name:<b>[[TCName]]</b>",
          "bullet": "round",
          "id": "AmGraph-3",
          "lineAlpha": 0,
          "lineColor": "#ff1a1a",
          "title": "Excel",
          "valueField": "TCName",
          "xField": "x3",
          "yField": "y3"
        },
        {
          "balloonText": "Division Name : <b>[[title]]</b></br> Total Visit :<b>[[y]]</b><br>User Name:<b>[[TCName]]</b>",
          "bullet": "round",
          "id": "AmGraph-4",
          "lineAlpha": 0,
          "lineColor": "#0066ff",
          "title": "Vita Life",
          "valueField": "TCName",
          "xField": "x4",
          "yField": "y4"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "axisAlpha": 0,
          "title": "Dr. Call Avg.",
        },
        {
          "id": "ValueAxis-2",
          "position": "bottom",
          "axisAlpha": 0,
          "title": "User"
        }
      ],
      "balloon": {},
      "legend": {
        "enabled": true,
        "align": "center",
        "backgroundAlpha": 0.31,
        "backgroundColor": "#60BB5D",
        "borderAlpha": 0.12,
        "fontSize": 12,
        "markerBorderAlpha": 0.26,
        "markerBorderColor": "#000000",
        "markerLabelGap": 19,
        "markerType": "circle",
        "rollOverGraphAlpha": 0
      },
      "titles": [],
      "dataProvider": [
        {
          "x": 1,
          "y": 6,
          "TCName": "ROOPENDRA VAISHNAV"
        },
        {
          "x": 2,
          "y": 62,
          "TCName": "Ravi Kumar Rathore"
        },
        {
          "x": 3,
          "y": 90,
          "TCName": "Samrath Malaviya"
        },
        {
          "x": 4,
          "y": 91,
          "TCName": "Rakesh Mishra"
        },
        {
          "x": 5,
          "y": 108,
          "TCName": "Roop Narayan Vyas"
        },
        {
          "x": 6,
          "y": 115,
          "TCName": "Shubham Rathore"
        },
        {
          "x": 7,
          "y": 134,
          "TCName": "Vinod Dangi"
        },
        {
          "x": 8,
          "y": 137,
          "z": "5b69f2d2ecc7514402f53bfb",
          "TCName": "Mahendra Dawar"
        },
        {
          "x": 9,
          "y": 153,
          "TCName": "Anil Nayak"
        },
        {
          "x": 10,
          "y": 156,
          "z": "5af88a037c4aca068638d0e3",
          "TCName": "MANISH DASHROE"
        },
        {
          "x": 11,
          "y": 161,
          "z": "5af87f6c7c4aca068638d001",
          "TCName": "SHABBIR HUSSAIN"
        },
        {
          "x": 12,
          "y": 167,
          "TCName": "SHAILENDRA SINGH RANAWAT"
        },
        {
          "x": 13,
          "y": 171,
          "TCName": "Mukesh Mawal"
        },
        {
          "x": 14,
          "y": 201,
          "z": "5af5b7f41497b83f74afaea3",
          "TCName": "Akhilesh Soni"
        },
        {
          "x": 15,
          "y": 210,
          "z": "5af5bf6e1497b83f74afaee9",
          "TCName": "AMOD TRIPATHI"
        },
        {
          "x": 16,
          "y": 217,
          "z": "5af888a27c4aca068638d0c9",
          "TCName": "Ranjeet Tawar"
        },
        {
          "x": 17,
          "y": 218,
          "z": "5af5b4d91497b83f74afae86",
          "TCName": "Mahesh Birgodiya"
        },
        {
          "x": 18,
          "y": 220,
          "z": "5af5b66a1497b83f74afae94",
          "TCName": "Rajesh Gujariya"
        },
        {
          "x": 19,
          "y": 220,
          "z": "5af886357c4aca068638d05b",
          "TCName": "Mahesh Dangi"
        },
        {
          "x": 20,
          "y": 222,
          "z": "5af887137c4aca068638d080",
          "TCName": "SANDEEP YADUWANSHI"
        },
        {
          "x": 21,
          "y": 223,
          "z": "5af878757c4aca068638cfea",
          "TCName": "Ravi Dangi"
        },
        {
          "x": 22,
          "y": 224,
          "z": "5af5be101497b83f74afaedb",
          "TCName": "CHANDRAPAL SINGH TOMER"
        },
        {
          "x": 23,
          "y": 226,
          "z": "5af5bd541497b83f74afaed7",
          "TCName": "PANKAJ TILHADIYA"
        },
        {
          "x": 24,
          "y": 231,
          "z": "5b9065ca82efd73147ad20cb",
          "TCName": "Aaftab khan"
        },
        {
          "x": 25,
          "y": 235,
          "z": "5af5b4301497b83f74afae7e",
          "TCName": "Ram Gopal meena"
        },
        {
          "x": 26,
          "y": 236,
          "z": "5af885157c4aca068638d02f",
          "TCName": "Rajesh Dangi"
        },
        {
          "x": 27,
          "y": 251,
          "z": "5af885757c4aca068638d03c",
          "TCName": "Nitesh Sen"
        },
        {
          "x": 28,
          "y": 269,
          "z": "5af8843b7c4aca068638d022",
          "TCName": "Deendayal Dangi"
        },
        {
          "x": 29,
          "y2": 295,
          "z": "5b19a3df0ca8cc446306c22c",
          "TCName": "Sourabh Soni"
        },
        {
          "x2": 30,
          "y2": 3,
          "z": "5c3f982bbd1509287e99214d",
          "TCName2": "Shailendra Singh Tomar"
        },
        {
          "x2": 31,
          "y2": 12,
          "z": "5c4e2726c64e322d17dd4d15",
          "TCName2": "Kanhaiya Lal Patidar"
        },
        {
          "x2": 32,
          "y2": 12,
          "z": "5c4f2f51c64e322d17ddd29a",
          "TCName2": "Ajaypal Yadav"
        },
        {
          "x2": 33,
          "y2": 15,
          "z": "5c3f99b7bd1509287e992399",
          "TCName2": "Kuldeep Joshi"
        },
        {
          "x2": 34,
          "y2": 17,
          "z": "5c4dfb0cc64e322d17dd34c1",
          "TCName2": "Sanjay Soni"
        },
        {
          "x2": 35,
          "y2": 22,
          "z": "5c44a3f4bd1509287e9c0ca3",
          "TCName2": "Govind Solanki"
        },
        {
          "x2": 36,
          "y2": 32,
          "z": "5b63ebd8387c7e278a077fe6",
          "TCName2": "Ajay Parmar"
        },
        {
          "x2": 37,
          "y2": 62,
          "z": "5b6114f9387c7e278a072a1e",
          "TCName2": "Ajaypal Yadav"
        },
        {
          "x2": 38,
          "y2": 69,
          "z": "5bce321710d4c836b0986aff",
          "TCName2": "Ashish Sharma"
        },
        {
          "x2": 39,
          "y2": 71,
          "z": "5b9c62eb2aad1655657dac2e",
          "TCName2": "Akash Banjara"
        },
        {
          "x2": 40,
          "y2": 72,
          "z": "5c3b76ec0d45223382498b55",
          "TCName2": "Rajkumar Pandey"
        },
        {
          "x2": 41,
          "y2": 74,
          "z": "5ba002b919c9192c9cc0f0ea",
          "TCName2": "Nageshwar Kumbhakar"
        },
        {
          "x2": 42,
          "y2": 74,
          "z": "5b1054eda75d9025acf753a4",
          "TCName2": "Dinesh Chouhan"
        },
        {
          "x2": 43,
          "y2": 87,
          "z": "5c40e336bd1509287e99e287",
          "TCName2": "Deepak"
        },
        {
          "x2": 44,
          "y2": 90,
          "z": "5c3f9910bd1509287e9921f0",
          "TCName2": "Rameshwar Chaubey"
        },
        {
          "x2": 45,
          "y2": 98,
          "z": "5b01e1e83e3618389129be68",
          "TCName2": "Amit Sharma"
        },
        {
          "x2": 46,
          "y2": 106,
          "z": "5abfdf5fa27682354b42a867",
          "TCName2": "Neeraj Choubey"
        },
        {
          "x2": 47,
          "y2": 121,
          "z": "5b01f1ff3e3618389129bf9a",
          "TCName2": "Ravikant Niranjan"
        },
        {
          "x2": 48,
          "y2": 121,
          "z": "5b196e690ca8cc446306c1cf",
          "TCName2": "Dinesh K Sahu"
        },
        {
          "x2": 49,
          "y2": 125,
          "z": "5be99fd8a721280133534007",
          "TCName2": "UMASHANKAR SAHU"
        },
        {
          "x2": 50,
          "y2": 127,
          "z": "5c3f721fbd1509287e9906f5",
          "TCName2": "Rahul Gautam"
        },
        {
          "x2": 51,
          "y2": 132,
          "z": "5b01efa13e3618389129bf71",
          "TCName2": "Santosh Khare"
        },
        {
          "x2": 52,
          "y2": 138,
          "z": "5b105333a75d9025acf7538e",
          "TCName2": "Sanjay Jat"
        },
        {
          "x2": 53,
          "y2": 140,
          "z": "5b9b530c2aad1655657d3897",
          "TCName2": "Deepak Thakur"
        },
        {
          "x2": 54,
          "y2": 161,
          "z": "5b105430a75d9025acf75399",
          "TCName2": "Salahuddin Mansuri"
        },
        {
          "x2": 55,
          "y2": 164,
          "z": "5b196b020ca8cc446306c1b7",
          "TCName2": "Digvijay Singh"
        },
        {
          "x2": 56,
          "y2": 167,
          "z": "5b1053c1a75d9025acf75393",
          "TCName2": "Sandeep Jat"
        },
        {
          "x2": 57,
          "y2": 170,
          "z": "5b196f1c0ca8cc446306c1d5",
          "TCName2": "Dileep Rai"
        },
        {
          "x2": 58,
          "y2": 174,
          "z": "5bc6269ae3ae7c5188a299f7",
          "TCName2": "Akhilesh Barholiya"
        },
        {
          "x2": 59,
          "y2": 176,
          "z": "5b196a5f0ca8cc446306c1b1",
          "TCName2": "Izhar Ahmad"
        },
        {
          "x2": 60,
          "y2": 179,
          "z": "5b63ec56387c7e278a077ff9",
          "TCName2": "Ram Babu"
        },
        {
          "x2": 61,
          "y2": 181,
          "z": "5b01f0ca3e3618389129bf7f",
          "TCName2": "Shailendra Gupta"
        },
        {
          "x2": 62,
          "y2": 185,
          "z": "5b1969a40ca8cc446306c1ac",
          "TCName2": "Ashish Tiwari"
        },
        {
          "x2": 63,
          "y2": 199,
          "z": "5bf1b9ab4165954f7f9b9eeb",
          "TCName2": "PAWAN YADAV"
        },
        {
          "x2": 64,
          "y2": 202,
          "z": "5b01eac13e3618389129bf2c",
          "TCName2": "Praveen Sharma"
        },
        {
          "x2": 65,
          "y2": 206,
          "z": "5be99ef2a721280133534002",
          "TCName2": "BHANU PRATAP AHIRWAR"
        },
        {
          "x2": 66,
          "y2": 207,
          "z": "5b770459ecc7514402f94c1f",
          "TCName2": "Rajendra kumar Namdev"
        },
        {
          "x2": 67,
          "y2": 222,
          "z": "5b7704ccecc7514402f94c27",
          "TCName2": "Tarun Sahu"
        },
        {
          "x2": 68,
          "y2": 223,
          "z": "5b4656a915141b47c27055d6",
          "TCName2": "Vikram Singh"
        },
        {
          "x2": 69,
          "y2": 224,
          "z": "5b01e6013e3618389129bea6",
          "TCName2": "Avinash Upadhya"
        },
        {
          "x2": 70,
          "y2": 225,
          "z": "5b01e2b43e3618389129be71",
          "TCName2": "Raghav Sharma"
        },
        {
          "x2": 71,
          "y2": 235,
          "z": "5b01ee4f3e3618389129bf63",
          "TCName2": "Ram Naresh Sharma"
        },
        {
          "x2": 72,
          "y2": 243,
          "z": "5b19682968f6133e943ae6df",
          "TCName2": "Anurag Pathak"
        },
        {
          "x2": 73,
          "y2": 243,
          "z": "5b01f1b33e3618389129bf93",
          "TCName2": "Manoj Singh Baghel"
        },
        {
          "x2": 74,
          "y2": 245,
          "z": "5b196c800ca8cc446306c1c4",
          "TCName2": "Anil Thakur"
        },
        {
          "x2": 75,
          "y2": 265,
          "z": "5b1970260ca8cc446306c1df",
          "TCName2": "Akash Sahu"
        },
        {
          "x2": 76,
          "y2": 273,
          "z": "5b19664768f6133e943ae6d7",
          "TCName2": "Anuraj Patel"
        },
        {
          "x2": 77,
          "y2": 278,
          "z": "5abfdfe7a27682354b42a870",
          "TCName2": "Ramprakash Sharma"
        },
        {
          "x2": 78,
          "y2": 282,
          "z": "5b7058a9ecc7514402f74c42",
          "TCName2": "Susheel Kumar Mehra"
        },
        {
          "x2": 79,
          "y2": 289,
          "z": "5b1972910ca8cc446306c1ec",
          "TCName2": "Pushpendra Tomar"
        },
        {
          "x2": 80,
          "y2": 327,
          "z": "5b1973db0ca8cc446306c1f1",
          "TCName2": "Hariom Prajapati"
        },
        {
          "x2": 81,
          "y2": 332,
          "z": "5b01f34d3e3618389129bfb1",
          "TCName2": "Shelendra Sharma"
        },
        {
          "x2": 82,
          "y2": 336,
          "z": "5b01ea1a3e3618389129bf15",
          "TCName2": "Abdhesh Singh Raghuwanshi"
        },
        {
          "x2": 83,
          "y2": 347,
          "z": "5b01e75c3e3618389129bed2",
          "TCName2": "Avid Khan"
        },
        {
          "x3": 84,
          "y3": 38,
          "z": "5afee726b9656b0a86b2a64e",
          "TCName": "Gajanan Bhakte"
        },
        {
          "x3": 85,
          "y3": 69,
          "z": "5bc8961e45252c33d8b8e631",
          "TCName": "SHARAD KUMAR VERMA"
        },
        {
          "x3": 86,
          "y3": 70,
          "z": "5bfeb9bb80581c5dbbb7dca5",
          "TCName": "Mukesh Gautam"
        },
        {
          "x3": 87,
          "y3": 173,
          "z": "5abfe23da27682354b42a897",
          "TCName": "Gajendra Singh"
        },
        {
          "x3": 88,
          "y3": 200,
          "z": "5bfb1af8a83ea160c3688ca9",
          "TCName": "Shyam Patel"
        },
        {
          "x3": 89,
          "y3": 206,
          "z": "5afee5c3b9656b0a86b2a617",
          "TCName": "Mukesh Baathri"
        },
        {
          "x3": 90,
          "y3": 213,
          "z": "5afee841b9656b0a86b2a6ab",
          "TCName": "Pawan Chandra Bhan"
        },
        {
          "x3": 91,
          "y3": 216,
          "z": "5abfe376a27682354b42a8a9",
          "TCName": "Nakul Jharbade*****"
        },
        {
          "x3": 92,
          "y3": 226,
          "z": "5afee921b9656b0a86b2a6e2",
          "TCName": "Yashwant Prajapati"
        },
        {
          "x3": 93,
          "y3": 228,
          "z": "5afee507b9656b0a86b2a5fe",
          "TCName": "Irshad Ansari"
        },
        {
          "x3": 94,
          "y3": 228,
          "z": "5afeeaddb9656b0a86b2a702",
          "TCName": "Rambharosh Chore"
        },
        {
          "x3": 95,
          "y3": 236,
          "z": "5b8e91db82efd73147ac4ade",
          "TCName": "Praveen Bhargav"
        },
        {
          "x3": 96,
          "y3": 242,
          "z": "5afee562b9656b0a86b2a609",
          "TCName": "Sunil Lodhi"
        },
        {
          "x3": 97,
          "y3": 257,
          "z": "5afee49db9656b0a86b2a5f5",
          "TCName": "Raman Noriya"
        },
        {
          "x3": 98,
          "y3": 261,
          "z": "5ade3c512791940a41d83dd5",
          "TCName": "Mukesh Kumar Prajapati"
        },
        {
          "x3": 99,
          "y3": 263,
          "z": "5bd130ee59e05b6075444fc4",
          "TCName": "Sharad Sahu"
        },
        {
          "x3": 100,
          "y3": 269,
          "z": "5ade3cb82791940a41d83dda",
          "TCName": "Praful Kumar Mahale"
        },
        {
          "x3": 101,
          "y3": 293,
          "z": "5b9f21b519c9192c9cc08bcb",
          "TCName": "THAKUR PRASAD SAHU"
        },
        {
          "x4": 102,
          "y4": 10,
          "z": "5ade4e332791940a41d83e3f",
          "TCName": "Ajay Jha"
        },
        {
          "x4": 103,
          "y4": 12,
          "z": "5ade4d302791940a41d83e2b",
          "TCName": "Prakash Shrivastava"
        },
        {
          "x4": 104,
          "y4": 30,
          "z": "5bfc633480581c5dbbb70adc",
          "TCName": "Kamlesh Verma"
        },
        {
          "x4": 105,
          "y4": 43,
          "z": "5af868087c4aca068638cec8",
          "TCName": "VIVEK SHUK`LA"
        },
        {
          "x4": 106,
          "y4": 48,
          "z": "5af868fd7c4aca068638cee5",
          "TCName": "DHIRENDRA DWIVEDI"
        },
        {
          "x4": 107,
          "y4": 49,
          "z": "5af9f18029904645efc67ff2",
          "TCName": "SHAHID ALI"
        },
        {
          "x4": 108,
          "y4": 65,
          "z": "5ade4cb32791940a41d83e17",
          "TCName": "Mohit Raj Mishra"
        },
        {
          "x4": 109,
          "y4": 74,
          "z": "5af869587c4aca068638cef1",
          "TCName": "SUNIL DUBEY"
        },
        {
          "x4": 110,
          "y4": 75,
          "z": "5b2bebb20775ea3d5fc42ebb",
          "TCName": "RAJESH KUMAR DWIVEDI"
        },
        {
          "x4": 111,
          "y4": 81,
          "z": "5af86f9a7c4aca068638cfad",
          "TCName": "PRAKASH MEHRA"
        },
        {
          "x4": 112,
          "y4": 82,
          "z": "5af86f497c4aca068638cf93",
          "TCName": "Devraj tiwari"
        },
        {
          "x4": 113,
          "y4": 87,
          "z": "5af9f28c29904645efc68026",
          "TCName": "SAMAR SHAH"
        },
        {
          "x4": 114,
          "y4": 90,
          "z": "5af86fe77c4aca068638cfc9",
          "TCName": "SANJEEV SINGH"
        },
        {
          "x4": 115,
          "y4": 113,
          "z": "5af86e5f7c4aca068638cf63",
          "TCName": "Rohit pandey"
        },
        {
          "x4": 116,
          "y4": 118,
          "z": "5af9f01229904645efc67fce",
          "TCName": "SATISH HARDAHA"
        },
        {
          "x4": 117,
          "y4": 127,
          "z": "5af9efc229904645efc67fc8",
          "TCName": "MANOHAR HARDAHA"
        },
        {
          "x4": 118,
          "y4": 197,
          "z": "5bfadb0442ac3d4b88b337d3",
          "TCName": "UMAKANT SINGROURE"
        }
      ]
    });
    //-----------------------------END---------------------------------


    
  }
  onChangeForMonthOnMonth(event) {
      console.log(event)
  }

}
