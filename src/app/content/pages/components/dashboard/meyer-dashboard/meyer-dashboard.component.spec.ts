import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeyerDashboardComponent } from './meyer-dashboard.component';

describe('MeyerDashboardComponent', () => {
  let component: MeyerDashboardComponent;
  let fixture: ComponentFixture<MeyerDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeyerDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeyerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
