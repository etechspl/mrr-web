import { AuthenticationService } from './../../../../../core/auth/authentication.service';
import { map } from 'rxjs/operators';

import { URLService } from './../../../../../core/services/URL/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DcrReportsService } from './../../../../../core/services/DCR/dcr-reports.service';
import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import {
  CalendarEvent
} from 'angular-calendar';
import {
  isSameMonth,
  isSameDay,
  startOfMonth,
  endOfMonth,
  startOfWeek,
  endOfWeek,
  startOfDay,
  endOfDay,
  format
} from 'date-fns';
import { Observable } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};
function getTimezoneOffsetString(date: Date): string {
  const timezoneOffset = date.getTimezoneOffset();
  const hoursOffset = String(
    Math.floor(Math.abs(timezoneOffset / 60))
  ).padStart(2, '0');
  const minutesOffset = String(Math.abs(timezoneOffset % 60)).padEnd(2, '0');
  const direction = timezoneOffset > 0 ? '-' : '+';
  return `T00:00:00${direction}${hoursOffset}${minutesOffset}`;
}
interface DCRPerformance {
  //id: number;
  // title: string;
  start: string;
  doctorCall: number;
  vendorCall: number;
  totalPob:number;
  totalDCR: number;
  workingDCR: number;
  drCallAvg: number;
  venCallAvg: number;
  listedDoctorCall: number;
  listedVendorCall: number;
  listedDoctorCallAvg: number;
  listedVendorCallAvg: number;
  incompleteDCR: number;
  productPob: number;
  events: any;
  cssClass: any;
  date: any;
}
@Component({
  selector: 'm-calender-utility',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calender-utility.component.html',
  styleUrls: ['./calender-utility.component.scss'],
  styles: [
    `
    ::ng-deep .odd-cell {
        background-color: #4AB21D !important;
      }
    ::ng-deep .odd-cell1 {
        background-color: yellow !important;
      } 
    ::ng-deep .odd-cell2 {
        background-color: red !important;
      } 
    `
  ]
})
export class CalenderUtilityComponent {
  events$: Observable<Array<CalendarEvent<{ film: DCRPerformance }>>>;
  constructor(
    private router: Router,
    private http: HttpClient,
    private _urlService: URLService,
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private authservice: AuthenticationService
  ) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  ngOnInit(): void {
    this.fetchEvents();
  }
  view: string = 'month';
  userId: string = '';
  viewDate: Date = new Date();

  activeDayIsOpen: boolean = false;

  currentUser = JSON.parse(sessionStorage.currentUser);

  getValue(val) {
    this.userId = val;
    this.fetchEvents();
  }

  fetchEvents(): void {
    const getStart: any = {
      month: startOfMonth,
      week: startOfWeek,
      day: startOfDay
    }[this.view];

    const getEnd: any = {
      month: endOfMonth,
      week: endOfWeek,
      day: endOfDay
    }[this.view];
    var params: any = {}
    // console.log("value is:"+this.userId);
    if (this.currentUser.userInfo[0].rL == 0 || this.currentUser.userInfo[0].rL > 1) {
      if (this.currentUser.userInfo[0].rL == 0 && (this.userId == "All" || this.userId == "" || this.userId == undefined)) {
        params = {
          fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
          toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
          rL: this.currentUser.userInfo[0].rL,
          //companyId:this.currentUser.companyId
          companyId: this.currentUser.companyId

        }
      } else if (this.currentUser.userInfo[0].rL == 0 && this.userId != "All" && (this.userId != "" || this.userId != undefined)) {
        params = {
          fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
          toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
          rL: 1,
          supervisorId: this.userId,
          //companyId:this.currentUser.companyId
          companyId: this.currentUser.companyId
        }
      } else {
        if (this.userId == 'Self') {
          params = {
            fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
            toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
            rL: 1,
            supervisorId: this.currentUser.id,
            //companyId:this.currentUser.companyId
            companyId: this.currentUser.companyId
          }
        } else {
          params = {
            fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
            toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
            rL: this.currentUser.userInfo[0].rL,
            supervisorId: this.currentUser.id,
            //companyId:this.currentUser.companyId
            companyId: this.currentUser.companyId,
            type: 'lowerWithMgr'
          }
        }

      }
    } else {

      params = {
        fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
        toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
        rL: this.currentUser.userInfo[0].rL,
        supervisorId: this.currentUser.id,
        //companyId:this.currentUser.companyId
        companyId: this.currentUser.companyId
      }


    }

    //console.log('DDATA',params)
    this.events$ = this.http
      .get(this._urlService.API_ENDPOINT_DCRMASTER + '/getDateWisePerformance?params=' + JSON.stringify(params), { headers: this.headers })
      .pipe(
        map((results: DCRPerformance[]) => {
          return results.map((film: DCRPerformance) => {
            return {

              title: "<table><tr><td>Total DCR:&nbsp;</td><td>" + film.totalDCR +
                "</td></tr><tr><td>Working DCR :&nbsp;</td><td>" + film.workingDCR +
                "</td></tr><tr><td>Incomplete DCR :&nbsp;</td><td>" + film.incompleteDCR +
                "</td></tr><tr><td>Total Doctor Visited:&nbsp;</td><td>" + film.doctorCall +
                "</td></tr><tr><td>Total Vendor Visited:&nbsp;</td><td>" + film.vendorCall +
                "</td></tr><tr><td>Doctor call Avg :&nbsp;</td><td>" + Math.round(film.drCallAvg) +
                "</td></tr><tr><td>Vendor Call Avg:&nbsp;</td><td>" + Math.round(film.venCallAvg) +
                "</td></tr><tr><td>Total POB:&nbsp;</td><td>" + film.productPob +
                "</td></tr><tr><td>Unique Doctor Visited:&nbsp;</td><td>" + film.listedDoctorCall +
                "</td></tr><tr><td>Unique Vendor Visited:&nbsp;</td><td>" + film.listedVendorCall +
                "</td></tr></table>",
              start: new Date(
                film.start + getTimezoneOffsetString(this.viewDate)
              ),
              color: colors.yellow,
              allDay: true,
              meta: {
                film
              }
            };
          });
        })
      );
    /* this.events$ = this._dcrReportsService.getDateWisePerformance(params).subscribe(
       res => {
         console.log("resonse :"+res);
         let array = res.map(item => {
           return {
             title: "aa",
             start: new Date(
               item.dcrDate + getTimezoneOffsetString(this.viewDate)
             ),
             color: colors.yellow,
             allDay: true,
             meta: {
               sub:7
             }
            // ...item
           };
         });
       });*/
    //console.log(this.events$)
  }

  dayClicked({ date, events }: { date: Date; events: Array<CalendarEvent<{ film: DCRPerformance }>>; }): void {
    if (isSameMonth(date, this.viewDate)) {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          "toAndFromDate": date,
          "userId": this.userId
        }
      };
      // this.router.navigate(['/reports/DCRReportDoctorWise'], navigationExtras);
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }


  beforeMonthViewRender({ body }: { body: [DCRPerformance] }): void {
    if (this.currentUser.userInfo[0].rL == 1 || this.userId == "Self" || ((this.currentUser.userInfo[0].rL == 0 || this.currentUser.userInfo[0].rL > 1) && this.userId !== "" && this.userId != "All")) {
      body.forEach(day => {
        var d = new Date();
        var diff = Math.abs(new Date().getTime() - new Date(day.date).getTime());
        var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
        if (day.events.length > 0) {
          day.cssClass = 'odd-cell';
        } else if (day.events.length == 0 && (diffDays <= this.currentUser.userInfo[0].lockingPeriod) && (isSameMonth(day.date, this.viewDate))) {
          day.cssClass = 'odd-cell1';
        } else if (day.events.length == 0 && (diffDays > this.currentUser.userInfo[0].lockingPeriod) && (isSameMonth(day.date, this.viewDate))) {
          day.cssClass = 'odd-cell2';
        }
      });
    }
  }
}

/*events: CalendarEvent[] = [
  {
    title: 'Has custom class',
    color: colors.yellow,
    start: new Date(),
    cssClass: 'my-custom-class',
    meta:{
      sub:7
   }
  },
  {
    title: 'Has custom class',
    color: colors.blue,
    start: new Date("2019-01-10"),
  //  cssClass: 'my-custom-class'
  meta:{
    sub:188
 }
  }
];*/