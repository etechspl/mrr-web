import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalenderUtilityComponent } from './calender-utility.component';

describe('CalenderUtilityComponent', () => {
  let component: CalenderUtilityComponent;
  let fixture: ComponentFixture<CalenderUtilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderUtilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalenderUtilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
