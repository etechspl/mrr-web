import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErpdashboardHqwiseComponent } from './erpdashboard-hqwise.component';

describe('ErpdashboardHqwiseComponent', () => {
  let component: ErpdashboardHqwiseComponent;
  let fixture: ComponentFixture<ErpdashboardHqwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErpdashboardHqwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErpdashboardHqwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
