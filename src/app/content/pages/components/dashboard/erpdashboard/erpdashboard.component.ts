import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { forkJoin } from 'rxjs';
import { delay, retryWhen, take } from 'rxjs/operators';
import { ERPServiceService } from '../../../../../core/services/erpservice.service';
import { ERPGraphsComponent } from '../erpgraphs/erpgraphs.component';

@Component({
  selector: 'm-erpdashboard',
  templateUrl: './erpdashboard.component.html',
  styleUrls: ['./erpdashboard.component.scss']
})

export class ERPDashboardComponent implements OnInit {
  @ViewChild("CallERPGraphsComponent") childData: ERPGraphsComponent;

  childSalesData = [];
  showPrimaryGraph = false;
  showCollectionGraph = false;
  showOutstandingGraph = false;
  showSalesReturnGraph=false;

  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(private ERPServiceService: ERPServiceService, private changeRef: ChangeDetectorRef) { }
  ngOnInit() {
    this.getERPData();
  }

  getERPData() {
    let data = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.employeeCode,
      APIEndPoint: this.currentUser.company.APIEndPoint,
    };   
    
    this.ERPServiceService.getPrimaryCount(data)
      .pipe(retryWhen((errors) => errors.pipe(delay(1000), take(10))))
      .subscribe(
        (res) => {
          
          this.childSalesData.push({
            type: "Primary",
            data: res.primarysale
          })
          this.childSalesData.push({
            type: "SalesReturn",
            data: res.salereturn
          })

          this.showPrimaryGraph = true; 
          this.changeRef.detectChanges();
        },
        (err) => {
          console.log(err);
        }
      );



    this.ERPServiceService.getCollectionCount(data)
      .pipe(retryWhen((errors) => errors.pipe(delay(1000), take(10))))
      .subscribe(
        (res) => {
          
          this.childSalesData.push({
            type: "Collection",
            data: res.amount
          }
          // , {
          //   type: "Target",
          //   data: 40000
          // }, {
          //   type: "Secondary",
          //   data: 567800
          // }
          )
          this.showCollectionGraph = true;
          this.changeRef.detectChanges();
        },
        (err) => {
          console.log(err);
        }
      );


      
    this.ERPServiceService.getOutstandingCount(data)
      .pipe(
        retryWhen(errors => errors.pipe(delay(1000), take(10)))
      )
      .subscribe(res => {

       // console.log("this.childSalesData",this.childSalesData);
       
       this.childSalesData.push({
        type: "Outstanding",
        data: res.outstandingamount
      })
      
        this.showOutstandingGraph = true;
        this.changeRef.detectChanges();
      }, err => {
        console.log(err)
      }
      );
     
      // this.ERPServiceService.getSalesReturnCount(data)
      // .pipe(retryWhen((errors) => errors.pipe(delay(1000), take(10))))
      // .subscribe(
      //   (res) => {
      //   // console.log("childSalesData+++++",this.childSalesData);

         
      //     this.childSalesData.push({
      //       type: "SalesReturn",
      //       data: res.amount
      //     })
      //     this.showSalesReturnGraph = true;
      //     this.changeRef.detectChanges();
      //   },
      //   (err) => {
          
      //     console.log(err);
      //   }
      // );


  }

}
