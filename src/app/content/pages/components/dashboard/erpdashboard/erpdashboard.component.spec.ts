import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ERPDashboardComponent } from './erpdashboard.component';

describe('ERPDashboardComponent', () => {
  let component: ERPDashboardComponent;
  let fixture: ComponentFixture<ERPDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ERPDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ERPDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
