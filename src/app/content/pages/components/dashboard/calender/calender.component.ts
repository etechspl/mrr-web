import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef, ChangeDetectorRef, Output, Input, SimpleChange } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, startOfMonth, endOfMonth, startOfWeek, endOfWeek, isSameDay, isSameMonth, addHours, format } from 'date-fns';
import { Subject, Observable } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, } from 'angular-calendar';
import { DesignationService } from '../../../../../core/services/Designation/designation.service'
import { UserDetailsService } from '../../../../../core/services/user-details.service'
import { DivisionService } from '../../../../../core/services/Division/division.service'
import { visitAll } from '@angular/compiler';
import * as moment from 'moment';
import { AuthenticationService } from '../../../../../core/auth/authentication.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { URLService } from '../../../../../core/services/URL/url.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { map } from 'rxjs/operators';
import { NavigationExtras, Router } from '@angular/router';
import { log } from 'util';
import { EventEmitter } from 'protractor';

interface DCRPerformance {
  start: string;
  doctorCall: number;
  vendorCall: number;
  totalDCR: number;
  totalPOB:number;
  workingDCR: number;
  drCallAvg: number;
  venCallAvg: number;
  listedDoctorCall: number;
  listedVendorCall: number;
  listedDoctorCallAvg: number;
  listedVendorCallAvg: number;
  incompleteDCR: number;
  productPob: number;
  events: any;
  cssClass: any;
  date: any;
  OnlyStCallAvg: number;
  OnlyVenCallAvg: number;
  listedOnlyStockistCall: number;
  listedOnlyStockistCallAvg: number;
  listedOnlyVendorCall: number;
  listedOnlyVendorCallAvg: number;
  onlyStockistCall: number;
  onlyVendorCall: number;
  otherDCR: number;

}

function getTimezoneOffsetString(date: Date): string {
  const timezoneOffset = date.getTimezoneOffset();
  const hoursOffset = String(
    Math.floor(Math.abs(timezoneOffset / 60))
  ).padStart(2, '0');
  const minutesOffset = String(Math.abs(timezoneOffset % 60)).padEnd(2, '0');
  const direction = timezoneOffset > 0 ? '-' : '+';
  return `T00:00:00${direction}${hoursOffset}${minutesOffset}`;
}

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'm-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalenderComponent implements OnInit {
  @Input() data : string;
  totalDCR = 0;
  totalPOB = 0;
  totalChemistsVisited;
  totalStockistsVisited = 0;
  DoctorCallAvg = 0;
  ChemistCallAvg = 0;
  noOfDays=0;
  StockistCallAvg = 0;
  TotalDoctorCallAvg=0;
  TotalChemistCallAvg = 0;
  TotalStockistCallAvg = 0;
  firstLoaddone = false;

  view: CalendarView = CalendarView.Month;
  // option = 'Month';
  option = 'Month';
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event$: CalendarEvent;
  };
  Events: CalendarEvent[];
  events$: Observable<Array<CalendarEvent<{ film: DCRPerformance }>>>;
  refresh: Subject<any> = new Subject();
  activeDayIsOpen: boolean = false;
  designations;
  divisionData = null;
  availEmp;
  selectedEmp;
  selec_Desig_Obj;
  showEmpty = true;
  selectedDivision = null;
  currentUser;
  selectedUserDetails: any;
  isGoheathy : boolean = false;


  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private designationService: DesignationService,
    private userDetailsService: UserDetailsService,
    private divisionService: DivisionService,
    private authservice: AuthenticationService,
    private http: HttpClient,
    private _urlService: URLService,
    private dcrReportsService: DcrReportsService,

  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  isAlenVision:boolean=false;
  
  ngOnInit() {
   
    this.currentUser = JSON.parse(sessionStorage.currentUser);

    if(this.currentUser.companyId === '6163030a26517a11b436cac6'){
      this.isGoheathy = true;
    }

    if(this.currentUser.companyId === '5f7fd2bb989fbc06fc363935'){
      this.isAlenVision = true;
    }
    this.designationService.getLowerLevelDesignations(this.currentUser.companyId, this.currentUser.userInfo[0].designationLevel)
      .subscribe(res => {
        this.designations = res;
        // this.selec_Desig_Obj = res[0];
        this.getDesignation(res[0]);
      });

    if (this.currentUser.company.isDivisionExist) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.divisionService.getDivisions(obj).subscribe(res => {
        if (res.length > 0) {
          res[0].divisionObj.sort(function (a, b) {
            var textA = a.divisionName.toUpperCase();
            var textB = b.divisionName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
          this.divisionData = res[0].divisionObj;
          // console.log('this.divisionData ',this.divisionData);
        }
      })

    }
    // this.getEmployee('All');
  }

  ngOnChanges(change:SimpleChange){

	}

  getDivisionVal(val) {
    this.selectedDivision = val;
  }

  getDesignation(val) {
    if (!val) {
      return;
    }
    this.showEmpty = true;
    this.selectedEmp = null;
    this.selec_Desig_Obj = val;
    this.userDetailsService.getUserInfoBasedOnDesignation(this.currentUser.companyId, val.designationLevel, [true])
      .subscribe(res => {
        this.availEmp = res;
		this.selectedEmp = res[0];

		this.getEmployee(res[0].userId);
        // if(!this.firstLoaddone){
        //   this.getEmployee(res[0].userId);
        //   this.firstLoaddone = true;
        // }
      });
    // if (this.selectedDivision) {
    //   let obj = {
    //     designationObject: val.designationLevel,
    //     companyId: this.currentUser.companyId,
    //     division: [this.selectedDivision],
    //     status: [true]

    //   };
    //   this.userDetailsService.getUserInfoBasedOnDesignationAndBasedOnDivision(obj).subscribe(res=>{
    //     this.availEmp = res;
    //   })
    // } else {
    //   this.userDetailsService.getUserInfoBasedOnDesignation(this.currentUser.companyId, val.designationLevel, [true])
    //     .subscribe(res => {
    //       this.availEmp = res;
    //     });
    // }
    (!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
  }

  getEmployee(val) {

    this.userDetailsService.getEmployee(this.currentUser.companyId,val).subscribe(userRes=>{
      
    this.selectedUserDetails=userRes[0];
    })

    this.selectedEmp = val;
    const getStart: any = {
      month: startOfMonth,
      week: startOfWeek,
      day: startOfDay
    }[this.view];

    const getEnd: any = {
      month: endOfMonth,
      week: endOfWeek,
      day: endOfDay
    }[this.view];
    let params = {
      fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
      toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
      rL: this.currentUser.userInfo[0].rL,
      supervisorId: val,
      companyId: this.currentUser.companyId
    }
    this.fetchEvents();
    this.option = 'Month';
    (!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
  }
  beforeMonthViewRender({ body }: { body: [DCRPerformance] }): void {
    
    
    body.forEach(day => {
      let firstDate = moment(new Date);
      let secondDate = moment(new Date(day.date));
      let diffInDays = (firstDate.diff(secondDate, 'days'));

      // console.log('day',day);
      // console.log('day.date',day.date);
      // console.log('new Date()',new Date());
      // console.log('diffInDays',diffInDays);
      // var d = new Date();
      // var diff = Math.abs(new Date().getTime() - new Date(day.date).getTime());
      // var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
      // console.log('diffDays',diffDays)
      // if(day.events.length){
      // if(day.events[0].meta.film.holidayDCR[0]){
      //   console.log('holidayDCR ',day.events[0].meta.film.holidayDCR);
      // }
      // if(day.events[0].meta.film.meetingDCR[0]){
      //   console.log('meetingDCR ',day.events[0].meta.film.meetingDCR);
      // }
      // if(day.events[0].meta.film.leaveDCR[0]){
      //   console.log('leaveDCR ',day.events[0].meta.film.leaveDCR);
      // }
      // console.log('day.events ',day.events[0]);
      // if(day.events[0].meta.film.transit[0]){
      //   console.log('TRANSIT ',day.events[0].meta.film.transit[0]);
      //   console.log('TRANSIT ',day.events[0].meta.film);
      // }
      // console.log('otherDCR ',day.events[0].meta.film);
      // }

      //console.log("dataD=>",day.events);
      

      if (this.selectedEmp !== 'All') {
        if (day.events.length == 0 && (diffInDays < this.selectedUserDetails.lockingPeriod) && diffInDays > 0 && (isSameMonth(day.date, this.viewDate))) {
          day.cssClass = 'odd-cell1';
        } else if (day.events.length == 0 && (diffInDays >= this.selectedUserDetails.lockingPeriod) && diffInDays > 0 && (isSameMonth(day.date, this.viewDate))) {
          day.cssClass = 'odd-cell2';
        } else if (day.events.length > 0) {
          if (day.events[0].meta.film.holidayDCR[0]) {
            day.cssClass = 'odd-cell3';
          } else if (day.events[0].meta.film.meetingDCR[0]) {
            day.cssClass = 'odd-cell4';
          } else if (day.events[0].meta.film.leaveDCR[0]) {
            day.cssClass = 'odd-cell5';
          } else if (day.events[0].meta.film.workingDCR) {
            day.cssClass = 'odd-cell6';
          } else if (day.events[0].meta.film.other[0]) {
            day.cssClass = 'odd-cell7';
          } else if (day.events[0].meta.film.transit[0]) {
            day.cssClass = 'odd-cell9';
          }
        } else {
          day.cssClass = 'odd-cell8';
        }
      } else {
        day.cssClass = 'odd-cell10';
      }
    });
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (this.selectedEmp !== 'All') {
      if (isSameMonth(date, this.viewDate)) {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            "toAndFromDate": date,
            "userId": [this.selectedEmp]
          }
        };


        if (
          (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
          events.length === 0 ||
          (events.length && events[0].meta.film.holidayDCR[0]) ||
          (events.length && events[0].meta.film.meetingDCR[0]) ||
          (events.length && events[0].meta.film.leaveDCR[0])
        ) {
          // this.activeDayIsOpen = false;
        } else if (
          (events[0].meta.film.doctorCall != 0) ||
          (events[0].meta.film.onlyVendorCall != 0) ||
          (events[0].meta.film.onlyStockistCall != 0)
        ) {
          this.router.navigate(['/reports/DCRReportDoctorWise'], navigationExtras);

          // this.activeDayIsOpen = true;
          // this.viewDate = date;
        }
        this.viewDate = date;
      }

    } else if(this.selectedEmp == 'All'){
    }
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    if (this.selectedEmp) {
      this.fetchEvents();
    }
  }

  fetchEvents(): void {
    const getStart: any = {
      month: startOfMonth,
      week: startOfWeek,
      day: startOfDay
    }[this.view];

    const getEnd: any = {
      month: endOfMonth,
      week: endOfWeek,
      day: endOfDay
    }[this.view];

    let params = {}

    if (this.currentUser.userInfo[0].rL == 0 || this.currentUser.userInfo[0].rL > 1) {
      if (this.currentUser.userInfo[0].rL == 0 && (this.selectedEmp == "All" || this.selectedEmp == "" || this.selectedEmp == undefined)) {
        params = {
          fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
          toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
          rL: this.currentUser.userInfo[0].rL,
          //companyId:this.currentUser.companyId
          companyId: this.currentUser.companyId

        }

      } else if (this.currentUser.userInfo[0].rL == 0 && this.selectedEmp != "All" && (this.selectedEmp != "" || this.selectedEmp != undefined)) {
        params = {
          fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
          toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
          rL: this.currentUser.userInfo[0].rL,
          supervisorId: this.selectedEmp,
          //companyId:this.currentUser.companyId
          companyId: this.currentUser.companyId
        }

      } else {
        if (this.selectedEmp == 'Self') {
          params = {
            fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
            toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
            rL: 1,
            supervisorId: this.currentUser.id,
            //companyId:this.currentUser.companyId
            companyId: this.currentUser.companyId
          }

        } else {
          params = {
            fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
            toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
            rL: 1,
            supervisorId: this.currentUser.id,
            //companyId:this.currentUser.companyId
            companyId: this.currentUser.companyId,
            type: 'lowerWithMgr'
          }
        }

      }
    }

    else {

      params = {
        fromDate: format(getStart(this.viewDate), 'YYYY-MM-DD'),
        toDate: format(getEnd(this.viewDate), 'YYYY-MM-DD'),
        rL: this.currentUser.userInfo[0].rL,
        supervisorId: this.currentUser.id,
        //companyId:this.currentUser.companyId
        companyId: this.currentUser.companyId
      }


    }

    this.showEmpty = false;

    this.totalDCR = 0;
    this.totalPOB = 0;
    this.totalStockistsVisited = 0;
    this.totalChemistsVisited = 0;
    this.ChemistCallAvg = 0;
    this.StockistCallAvg = 0;
    this.DoctorCallAvg = 0;
    this.TotalDoctorCallAvg=0;
    this.TotalChemistCallAvg = 0;
    this.TotalStockistCallAvg = 0;
    this.noOfDays=0;

    
    
    this.events$ = this.http
      .get(this._urlService.API_ENDPOINT_DCRMASTER + '/getDateWisePerformance?params=' + JSON.stringify(params), { headers: this.headers })
      .pipe(
        map((results: DCRPerformance[]) => {
          return results.map((film: DCRPerformance) => {

            this.totalDCR += parseInt(''+film.doctorCall);
            this.totalPOB += parseInt(''+film.productPob);
            this.totalStockistsVisited += parseInt('' + film.onlyStockistCall);
            this.totalChemistsVisited += parseInt('' + film.onlyVendorCall);
            this.ChemistCallAvg += parseInt(''+film.OnlyVenCallAvg);
            this.StockistCallAvg += parseInt(''+film.OnlyStCallAvg);
            this.DoctorCallAvg += parseInt(''+film.drCallAvg);

           //debugger
            //let noOfDays = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth() + 1, 0).getDate();

            // added condition on call avg by preeti, before that it was getting from no of days
            if(film.workingDCR==1){
            this.noOfDays += parseInt('' + film.workingDCR);
            this.TotalDoctorCallAvg = this.DoctorCallAvg / this.noOfDays; 
            this.TotalChemistCallAvg = this.ChemistCallAvg / this.noOfDays;
            this.TotalStockistCallAvg =  this.StockistCallAvg / this.noOfDays;
            }
            
           

            return {

              title: `
              <table><tr><td>Total DCR:&nbsp;</td><td> ${film.totalDCR}
              </td></tr><tr><td>Working DCR :&nbsp;</td><td> ${film.workingDCR}
              </td></tr><tr><td>Incomplete DCR :&nbsp;</td><td> ${film.incompleteDCR}
              </td></tr><tr><td>Total Doctor Visited:&nbsp;</td><td>${film.doctorCall}
              </td></tr><tr><td>Total Vendor Visited:&nbsp;</td><td>${film.vendorCall}
              </td></tr><tr><td>Total POB:&nbsp;</td><td>${film.productPob}
              </td></tr><tr><td>Doctor call Avg :&nbsp;</td><td>${Math.round(film.drCallAvg)}
              </td></tr><tr><td>Vendor Call Avg:&nbsp;</td><td>${Math.round(film.venCallAvg)}
              </td></tr><tr><td>Total POB:&nbsp;</td><td>${film.productPob}
              </td></tr><tr><td>Unique Doctor Visited:&nbsp;</td><td>${film.listedDoctorCall}
              </td></tr><tr><td>Unique Vendor Visited:&nbsp;</td><td>${film.listedVendorCall}
              </td></tr></table>
              `,
              start: new Date(
                film.start + getTimezoneOffsetString(this.viewDate)
              ),
              color: colors.yellow,
              allDay: true,
              meta: {
                film
              }
            };


            
          });
        })

        
      );

     
    
      
  }


  
}
