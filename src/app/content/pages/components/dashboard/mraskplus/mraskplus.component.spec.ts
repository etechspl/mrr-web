import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MraskplusComponent } from './mraskplus.component';

describe('MraskplusComponent', () => {
  let component: MraskplusComponent;
  let fixture: ComponentFixture<MraskplusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MraskplusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MraskplusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
