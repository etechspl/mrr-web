import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_material from "@amcharts/amcharts4/themes/material";
import { ErpservicehqwiseService } from '../../../../../core/services/erpservicehqwise.service';
import { EventEmitter } from 'protractor';
import { MatTableDataSource } from '@angular/material';
import { AmChartsService } from '@amcharts/amcharts3-angular';
import { MatSlideToggleChange } from "@angular/material";
declare var $;
import * as XLSX from 'xlsx';
import { timeout } from 'rxjs/operators';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'm-erpgraphs-hqwise',
  templateUrl: './erpgraphs-hqwise.component.html',
  styleUrls: ['./erpgraphs-hqwise.component.scss']
})
export class ErpgraphsHqwiseComponent implements OnInit {
  hq: HTMLElement;
  @ViewChild("StatewiseDataTable") StatewiseDataTable: ElementRef;
  @ViewChild("HqwiseDataTable") HqwiseDataTable: ElementRef;
  @ViewChild("PartywiseDataTable") PartywiseDataTable: ElementRef;
  @ViewChild("StatewiseProductDataTable") StatewiseProductDataTable: ElementRef;
  @ViewChild("HqwiseProductDataTable") HqwiseProductDataTable: ElementRef;
  @ViewChild('hqTable') hqTableRef: ElementRef;
  @ViewChild('partyTable') partyTableRef: ElementRef;

  showStateWiseDiv = false;
  showHQWiseDiv = false;
  showPartyWiseDiv = false;
  headerStateName: any;
  headerHqName: any;
  stateData: any;
  hqsData: any;
  headerSaleType: any;
  isShowProcessingState: boolean = false;
  isShowProcessingHq: boolean = false;
  isShowProcessingParty: boolean = false;
  showPrimary: boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);

  REQUESTS = [];

  dataSource: MatTableDataSource<any>;
  dataSourceHq: MatTableDataSource<any>;
  dataSourceHq1: MatTableDataSource<any>;
  dataSourceParty: MatTableDataSource<any>;
  dataSourceParty1: MatTableDataSource<any>;
  productDatasource: MatTableDataSource<any>;
  columnsToDisplay = ['stateName', 'amount'];
  columnsToDisplay1 = ['stateName', 'ProductName', 'amount', 'qty', 'itemRate', 'creditValue'];
  columnsToDisplayHq = ['Headquater', 'stateName', 'amount'];
  columnsToDisplayHq1 = ['Headquater', 'ProductName', 'amount', 'qty', 'itemRate', 'creditValue'];
  columnsToDisplayParty = ['PartyName', 'hqName', 'amount'];
  columnsToDisplayParty1 = ['PartyName', 'ProductName', 'amount', 'qty', 'itemRate', 'creditValue'];
  StatewiseDTOptions: any;
  StateDataTable: any;
  HqwiseDTOptions: any;
  HQDataTable: any;
  PartywiseDTOptions: any;
  PartyDataTable: any;
  chartParty: any;
  @Input() data: any;
  fromDate: any;
  toDate: any;
  month: any;
  year: any;
  hqGraphDispose: any;
  partyGraphDispose: any;
  primarySale: any = 0;
  collection: any = 0;
  outstanding: any = 0;
  salesReturn: any = 0;
  selectedItem: any;
  totalState: number;
  totalHq: number;
  totalParty: number;
  totalAmount: any = 0;
  totalAmountHq: any = 0;
  TotalAmountParty: any = 0;
  isToggledProduct = false;

  // @Output() parentData = new EventEmitter();


  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  exportAsConfig3: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element
  }

  exportAsConfig2: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId3', // the id of html/table element
  }

  exportAsConfig1: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element
  }

  constructor(
    private ERPServiceService: ErpservicehqwiseService,
    private AmCharts: AmChartsService,
    private changeDetectorRef: ChangeDetectorRef,
    private exportAsService: ExportAsService,
    private _toastrService: ToastrService
  ) { }

  ngOnInit() {
    // create chart instance here---
    console.log('this.data', this.data);
    this.data.forEach(element => {
      if (element.type == "Primary") {
        this.primarySale = element.data;
      } else if (element.type == "Collection") {
        this.collection = element.data;
      } else if (element.type == "Outstanding") {
        this.outstanding = element.data;
      } else if (element.type == "SalesReturn") {
        this.salesReturn = element.data;
      }
    });
    let date = new Date().getDate();
    this.month = new Date().getMonth() + 1;
    this.year = new Date().getFullYear();
    this.fromDate = "01" + "-" + this.month + "-" + this.year;
    this.toDate = date + "-" + this.month + "-" + this.year;
    this.getStateWiseData("Primary", 'table');
    this.changeDetectorRef.detectChanges();
  }

  getTotalSalesCount(data) {
    let chart = am4core.create('ERPGraph', am4charts.XYChart)
    // Add data
    data.forEach(item => {
      chart.data.push({
        "type": item.type,
        "data": item.data
      })
    });
    // Create axes

    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "type";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;

    categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
      if (target.dataItem && target.dataItem.index) {
        return dy + 25;
      }
      return dy;
    });

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "data";
    series.dataFields.categoryX = "type";
    series.name = "Sales Data";
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fillOpacity = .8;
    series.columns.template.width = am4core.percent(50);
    let columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 1;
    // add events in it--
    series.columns.template.events.on("hit", function (ev) {
      this.getStateWiseData(ev.target.dataItem.categories.categoryX, "graph");
    }, this);
  }

  typeData = true;
  cardData: any;
  cardType: any;
  getStateWiseData(data, type) {
    this.cardData = data;
    this.cardType = type;
    this.productStatus = false;
    this.isToggledProduct = false;
    //let state = [];
    //let params1 = { APIEndPoint: this.currentUser.company.APIEndPointHQWise, userId: this.currentUser.employeeCode, }
    //this.ERPServiceService.getERPStateData(params1).subscribe((stateDataResult) => {
    // this.stateData = stateDataResult;
    // this.stateData.forEach(element => {
    //   state.push(parseInt(element.code));
    // });
    // let stateIds = state.toString();
    this.selectedItem = data;
    this.headerSaleType = data;
    this.showStateWiseDiv = false;
    this.showHQWiseDiv = false;
    this.showPartyWiseDiv = false;
    this.isShowProcessingState = true;
    // Create chart instance
    let params = {
      //companyId: this.currentUser.companyId,
      //userId: this.currentUser.employeeCode,
      fromDate: this.fromDate,
      toDate: this.toDate,
      stateIDs: "",
      APIEndPoint: this.currentUser.company.APIEndPointHQWise
    };
    if (data == "Primary") {
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
      // params['stateIds'] = "";
      params['Flag'] = "PS";
    } else if (data == "Collection") {
      //params['month'] = this.month;
      //params['year'] = this.year;
      params['Flag'] = "CL";
    } else if (data == "Outstanding") {
      //params['month'] = this.month;
      //params['year'] = this.year;
      params['Flag'] = "OS";
    } else if (data == "SalesReturn") {
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
      //params['stateIds'] = "";
      params['Flag'] = "PS";
    }

    if (type == "graph") {
      params["type"] = data
    } else {
      params["type"] = data
    }


    this.ERPServiceService.getStateWiseDetails(params).subscribe((res) => {
      console.log('getStateWiseDetails', res);
      if (res == null || res == undefined || res == '' || res == 'null') {
        this.isShowProcessingState = false;
        this.isShowProcessingHq = false;
        this.isShowProcessingParty = false;
        this._toastrService.info('No Record Found....');
      } else {
        let dataProvider = [];
        let result = [];
        this.totalState = 0;
        res.forEach(element => {
          console.log("element: ", element);
          if (data == "Primary") {
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.primarysale,
              "code": element.StateCode
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.primarysale,
              "code": element.StateCode,
              "saleType": data
            });
            this.totalState = this.totalState + parseFloat(element.primarysale);
          } else if (data == "Collection") {
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.collectionamount,
              "code": element.StateCode
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.collectionamount,
              "code": element.StateCode,
              "saleType": data
            });
            this.totalState = this.totalState + parseFloat(element.collectionamount);
          } else if (data == "SalesReturn") {
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.salereturn,
              "code": element.StateCode
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.salereturn,
              "code": element.StateCode,
              "saleType": data
            });
            this.totalState = this.totalState + parseFloat(element.salereturn);
          } else if (data == "Outstanding") {
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.outstandingamount,
              "code": element.StateCode
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.outstandingamount,
              "code": element.StateCode,
              "saleType": data
            });
            this.totalState = this.totalState + parseFloat(element.outstandingamount);
          }
        });
        this.dataSource = new MatTableDataSource(result);
        this.makeGraph(dataProvider, params);
      }
    })

    //})
  }

  makeGraph(dataProvider, params) {
    am4core.disposeAllCharts();
    let chart: any;
    chart = am4core.create("stateWiseGraph", am4charts.XYChart);
    chart.scrollbarX = new am4core.Scrollbar();
    chart.data = dataProvider;
    // Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "State";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.verticalCenter = "middle";
    categoryAxis.renderer.labels.template.rotation = -30;
    categoryAxis.tooltip.disabled = true;
    categoryAxis.renderer.minHeight = 110;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.minWidth = 50;

    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.sequencedInterpolation = true;
    series.dataFields.valueY = "Sales";
    series.dataFields.categoryX = "State";
    series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
    series.columns.template.strokeWidth = 0;

    series.tooltip.pointerOrientation = "vertical";
    categoryAxis.renderer.cellStartLocation = 0.2;
    categoryAxis.renderer.cellEndLocation = 0.8;
    series.columns.template.width = am4core.percent(40);
    let label = categoryAxis.renderer.labels.template;
    label.wrap = true;
    series.columns.template.column.cornerRadiusTopLeft = 10;
    series.columns.template.column.cornerRadiusTopRight = 10;
    series.columns.template.column.fillOpacity = 0.8;

    // on hover, make corner radiuses bigger
    let hoverState = series.columns.template.column.states.create("hover");
    hoverState.properties.cornerRadiusTopLeft = 0;
    hoverState.properties.cornerRadiusTopRight = 0;
    hoverState.properties.fillOpacity = 1;

    series.columns.template.adapter.add("fill", function (fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    });

    // Cursor
    chart.cursor = new am4charts.XYCursor();
    series.columns.template.events.on("hit", function (ev) {
      this.getHQWiseData(ev.target.dataItem['dataContext'], "graph", params["type"]);
    }, this);
    this.showStateWiseDiv = true;
    this.isShowProcessingState = false;
    this.showStateWiseDiv = true;
    this.changeDetectorRef.detectChanges();
  }

  getHQWiseData(data, type, salesType) {

    this.changeDetectorRef.detectChanges();
    this.showHQWiseDiv = false;
    this.showPartyWiseDiv = false;
    this.isShowProcessingHq = true;
    this.headerSaleType = salesType;
    // Create chart instance
    let params = {
      userId: this.currentUser.employeeCode,
      fromDate: this.fromDate,
      toDate: this.toDate,
      APIEndPoint: this.currentUser.company.APIEndPointHQWise,
      stateIDs: data['code']
    };

    if (salesType == "Primary" || salesType == "SalesReturn") {
      params['Flag'] = "PS";
      //params['toDate'] = this.toDate;
    } else if (salesType == "Collection") {
      params['Flag'] = "CL";
      //params['month'] = this.month;
      //params['year'] = this.year;
    } else if (salesType == "Outstanding") {
      params['Flag'] = "OS";
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
    }

    if (type == "graph") {
      params["type"] = salesType
    } else {
      params["type"] = salesType
    }

    this.ERPServiceService.getHQWiseDetails(params).subscribe((res) => {
      console.log("res : ", res);
      if (res.length > 0) {
        let result = [];
        this.totalHq = 0;
        if (this.hqGraphDispose) {
          this.hqGraphDispose.dispose();
          this.hqGraphDispose = undefined;
        }
        let chartHq = am4core.create("HqWiseGraph", am4charts.XYChart);
        this.hqGraphDispose = chartHq;
        chartHq.scrollbarX = new am4core.Scrollbar();
        res.forEach(element => {
          if (salesType == "Primary") {
            chartHq.data.push({
              "Headquarter": element.HQName,
              "Sales": element.primarysale,
              "HeadquaterCode": element.HQCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.primarysale,
              "HeadquaterCode": element.HQCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.primarysale);
          } else if (salesType == "Outstanding") {
            chartHq.data.push({
              "Headquarter": element.HQName,
              "Sales": element.outstandingamount,
              "HeadquaterCode": element.HQCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.outstandingamount,
              "HeadquaterCode": element.HQCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.outstandingamount);
          } else if (salesType == "Collection") {
            chartHq.data.push({
              "Headquarter": element.HQName,
              "Sales": element.collectionamount,
              "HeadquaterCode": element.HQCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.collectionamount,
              "HeadquaterCode": element.HQCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.collectionamount);
          } else if (salesType == "SalesReturn") {
            chartHq.data.push({
              "Headquarter": element.HQName,
              "Sales": element.salereturn,
              "HeadquaterCode": element.HQCode
            })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.salereturn,
              "HeadquaterCode": element.HQCode,
              "saleType": salesType
            });
            this.totalHq = this.totalHq + parseFloat(element.salereturn);
          }
        });
        this.headerStateName = data['StateName'];
        this.dataSourceHq = new MatTableDataSource(result);
        //this.scroll(this.hq as HTMLElement);
        setTimeout(() => {
          this.hqTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        }, 500)

        // Create axes
        let categoryAxis = chartHq.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "Headquarter";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = -30;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        let valueAxis = chartHq.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        // Create series
        let series = chartHq.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "Sales";
        series.dataFields.categoryX = "Headquarter";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;

        series.tooltip.pointerOrientation = "vertical";
        categoryAxis.renderer.cellStartLocation = 0.2;
        categoryAxis.renderer.cellEndLocation = 0.8;
        series.columns.template.width = am4core.percent(40);
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;
        let label = categoryAxis.renderer.labels.template;
        label.wrap = true;
        // on hover, make corner radiuses bigger
        let hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", function (fill, target) {
          return chartHq.colors.getIndex(target.dataItem.index);
        });

        // Cursor
        chartHq.cursor = new am4charts.XYCursor();
        series.columns.template.events.on("hit", function (ev) {
          this.getPartyWiseData(ev.target.dataItem['dataContext'], "graph", salesType);
        }, this);
        this.showHQWiseDiv = true;
        this.isShowProcessingHq = false;
      }
      this.changeDetectorRef.detectChanges();
    })
  }

  getPartyWiseData(data, type, salesType) {
    // Create chart instance
    this.showPartyWiseDiv = false;
    this.isShowProcessingParty = true;
    this.headerSaleType = salesType;
    // Create chart instance
    let params = {
      //companyId: this.currentUser.companyId,
      //userId: this.currentUser.employeeCode,
      fromDate: this.fromDate,
      toDate: this.toDate,
      APIEndPoint: this.currentUser.company.APIEndPointHQWise,
      HQs: data['HeadquaterCode']
    };
    if (salesType == "Primary" || salesType == "SalesReturn") {
      params['Flag'] = "PS";
      //params['toDate'] = this.toDate;
    } else if (salesType == "Collection") {
      params['Flag'] = "CL";
      //params['month'] = this.month;
      //params['year'] = this.year;
    } else if (salesType == "Outstanding") {
      params['Flag'] = "OS";
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
    }

    if (type == "graph") {
      params["type"] = salesType
    } else {
      params["type"] = salesType
    }
    this.ERPServiceService.getPartyWiseDetails(params).subscribe((res) => {
      if (res.length > 0) {
        if (this.partyGraphDispose) {
          this.partyGraphDispose.dispose();
          this.partyGraphDispose = undefined;
        }
        let chartParty = am4core.create("PartyWiseGraph", am4charts.XYChart);
        this.partyGraphDispose = chartParty;
        chartParty.scrollbarX = new am4core.Scrollbar();
        let result = [];
        this.totalParty = 0;
        res.forEach(element => {
          if (salesType == "Primary") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.primarysale,
              "PartyCode": element.PartyCode
            })
            result.push({
              //"stateName":element.StateName,
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "amount": element.primarysale,
            });
            this.totalParty = this.totalParty + parseFloat(element.primarysale);
          } else if (salesType == "Outstanding") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.outstandingamount,
              "PartyCode": element.PartyCode
            })
            result.push({
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "amount": element.outstandingamount,
            });
            this.totalParty = this.totalParty + parseFloat(element.outstandingamount);
          } else if (salesType == "Collection") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.collectionamount,
              "PartyCode": element.PartyCode
            })
            result.push({
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "amount": element.collectionamount,
            });
            this.totalParty = this.totalParty + parseFloat(element.collectionamount);
          } else if (salesType == "SalesReturn") {
            chartParty.data.push({
              "PartyName": element.PartyName,
              "Sales": element.salereturn,
              "PartyCode": element.PartyCode
            })
            result.push({
              //"stateName":element.StateName,
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "amount": element.salereturn,
            });
            this.totalParty = this.totalParty + parseFloat(element.salereturn);
          }
        });
        this.headerHqName = data['Headquater'];
        this.dataSourceParty = new MatTableDataSource(result);

        // Create axes
        let categoryAxis = chartParty.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "PartyName";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = -30;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        let valueAxis = chartParty.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        // Create series
        let series = chartParty.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "Sales";
        series.dataFields.categoryX = "PartyName";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;
        let label = categoryAxis.renderer.labels.template;
        label.wrap = true;
        series.tooltip.pointerOrientation = "vertical";
        categoryAxis.renderer.cellStartLocation = 0.2;
        categoryAxis.renderer.cellEndLocation = 0.8;
        series.columns.template.width = am4core.percent(40);
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;

        // on hover, make corner radiuses bigger
        let hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", function (fill, target) {
          return chartParty.colors.getIndex(target.dataItem.index);
        });

        // Cursor
        chartParty.cursor = new am4charts.XYCursor();
        series.columns.template.events.on("hit", function (ev) {
          //this.getHQWiseDetails(ev.target.dataItem['dataContext'], "graph",'');
        }, this);
        this.showPartyWiseDiv = true;
        this.isShowProcessingParty = false;
        setTimeout(() => {
          this.partyTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        }, 500)
        this.changeDetectorRef.detectChanges();
      }
    })
    this.changeDetectorRef.detectChanges();
  }


  applyFilterState(eventState: Event) {
    const filterValueState = (eventState.target as HTMLInputElement).value;
    this.dataSource.filter = filterValueState.trim().toLowerCase();
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.productDatasource.filter = filterValue;
    this.dataSourceHq1.filter = filterValue;
  }

  applyFilterHq(eventHq: Event) {
    const filterValueHq = (eventHq.target as HTMLInputElement).value;
    this.dataSourceHq.filter = filterValueHq.trim().toLowerCase();
  }

  applyFilterParty(eventParty: Event) {
    const filterValueParty = (eventParty.target as HTMLInputElement).value;
    this.dataSourceParty.filter = filterValueParty.trim().toLowerCase();
  }
  exportToExcelState() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.StatewiseDataTable.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    let fileName = "State Wise " + this.selectedItem + " Data.xlsx";
    XLSX.writeFile(wb, fileName);
  }

  exporttoExcelStateProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Employee Attendance Report').subscribe(() => {
      // save started
    });
  }
  exporttoExcelHqProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'State Wise Report').subscribe(() => {
      // save started
    });
  }

  exporttoExcelHeadquarterProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig3, 'Headquarter Wise Report').subscribe(() => {
      // save started
    });
  }

  exportToExcelHq() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.HqwiseDataTable.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    let fileName = "Headquarter Wise " + this.selectedItem + " Data.xlsx";
    XLSX.writeFile(wb, fileName);
  }

  exporttoExcelPartyProduct() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig2, 'Party Wise Report').subscribe(() => {
      // save started
    });
  }

  exportToExcelParty() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.PartywiseDataTable.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    let fileName = "Stockist Wise " + this.selectedItem + " Data.xlsx";
    XLSX.writeFile(wb, fileName);
  }


  isToggledState = false;
  colMdState = "col-6";

  // --------------------------------------GOURAV 30-03-2021 ---------------------------------------

  productStatus: boolean = false;

  toggleProduct(event: MatSlideToggleChange) {
    this.isToggledProduct = event.checked;
    if (event.checked) {
      this.getStateWiseProductReport(this.cardData, this.cardType);
      this.productStatus = true;
    } else {
      this.productStatus = false;
      this.getStateWiseData(this.cardData, this.cardType);
    }
  }

  totalQty = 0.0;
  itemwiseTotalValue = 0.0;
  getStateWiseProductReport(data, type) {
    this.totalQty = 0.0;
    this.itemwiseTotalValue = 0.0;
    this.totalAmount = 0;
    let key = false;
    // let state = [];
    // let params1 = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, }
    // this.ERPServiceService.getERPStateData(params1).subscribe((stateDataResult) => {
    //   this.stateData = stateDataResult;
    //   this.stateData.forEach(element => {
    //     state.push(parseInt(element.code));
    //   });
    //   let stateIds = state.toString();
    this.selectedItem = data;
    this.headerSaleType = data;
    this.showStateWiseDiv = false;
    this.showHQWiseDiv = false;
    this.showPartyWiseDiv = false;
    this.isShowProcessingState = true;
    // Create chart instance
    let params = {
      //companyId: this.currentUser.companyId,
      //userId: this.currentUser.employeeCode,
      fromDate: this.fromDate,
      toDate: this.toDate,
      stateIDs: "",
      APIEndPoint: this.currentUser.company.APIEndPointHQWise
    };
    if (data == "Primary") {
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
      // params['stateIds'] = "";
      params['Flag'] = "PS";
    } else if (data == "Collection") {
      //params['month'] = this.month;
      //params['year'] = this.year;
      params['Flag'] = "CL";
    } else if (data == "Outstanding") {
      //params['month'] = this.month;
      //params['year'] = this.year;
      params['Flag'] = "OS";
    } else if (data == "SalesReturn") {
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
      //params['stateIds'] = "";
      params['Flag'] = "PS";
    }

    if (type == "graph") {
      params["type"] = data
    } else {
      params["type"] = data
    }
    this.ERPServiceService.getStateWiseProductDetails(params).subscribe((res) => {
      if (res == null || res == undefined || res == '' || res == 'null') {
        this.isShowProcessingState = false;
        this.isShowProcessingHq = false;
        this.isShowProcessingParty = false;
        this._toastrService.info('No Record Found....');
      } else {
        let dataProvider = [];
        let result = [];
        this.totalState = 0;
        res.forEach(element => {
          if (data == "Collection") {
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.collectionamount,
              //"ProductName": element.ProductName,
              "code": element.StateCode
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.collectionamount,
              "code": element.StateCode,
              //"ProductName": element.ProductName,
              "saleType": data
            });
            this.totalAmount = this.totalAmount + parseFloat(element.collectionamount);

          } else if (data == "SalesReturn") {
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.salereturnamount,
              "ProductName": element.productname,
              "code": element.StateCode,
              "qty": element.salereturnqty,
              "itemRate": element.itemrate
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.salereturnamount,
              "code": element.StateCode,
              "ProductName": element.productname,
              "saleType": data,
              "qty": element.salereturnqty,
              "itemRate": element.itemrate
            });
            //let val = element.itemrate;
            //let total = element.salereturnqty * val;
            this.totalQty = this.totalQty + parseFloat(element.salereturnqty);
            this.itemwiseTotalValue = this.itemwiseTotalValue + (element.itemrate * element.salereturnqty);
            this.totalAmount = this.totalAmount + parseFloat(element.salereturnamount);

          } else if (data == "Outstanding") {
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.outstandingamount,
              //"ProductName": element.ProductName,
              "code": element.StateCode
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.outstandingamount,
              "code": element.StateCode,
              //"ProductName": element.ProductName,
              "saleType": data
            });

            this.totalAmount = this.totalAmount + parseFloat(element.outstandingamount);

          } else if (data == "Primary") {
            this.showPrimary = true;
            dataProvider.push({
              "State": element.StateName,
              "Sales": element.primarysaleamount,
              "ProductName": element.productname,
              "code": element.StateCode,
              "qty": element.primarysaleqty,
              "itemRate": element.itemrate
            })
            result.push({
              "StateName": element.StateName,
              "amount": element.primarysaleamount,
              "code": element.StateCode,
              "ProductName": element.productname,
              "qty": element.primarysaleqty,
              "itemRate": element.itemrate,
              "saleType": data
            });
            //let total = element.primarysaleqty * element.itemrate;
            this.totalQty = this.totalQty + parseFloat(element.primarysaleqty);
            this.itemwiseTotalValue = this.itemwiseTotalValue + (element.itemrate * element.primarysaleqty);
            this.totalAmount = this.totalAmount + parseFloat(element.primarysaleamount);
          }
        });
        this.productDatasource = new MatTableDataSource(result);
        this.showStateWiseDiv = true;
        this.isShowProcessingState = false;
        this.changeDetectorRef.detectChanges();
        //this.makeGraph(dataProvider, params);
      }
    });
    //});
  }

  totalQtyHq = 0.0;
  itemwiseTotalValueHq = 0.0;
  getHQWiseProductData(req, type, selectedType) {
    this.totalQtyHq = 0.0;
    this.itemwiseTotalValueHq = 0.0;
    let hqIds = [];
    this.totalAmountHq = 0;
    this.changeDetectorRef.detectChanges();
    this.showHQWiseDiv = false;
    this.showPartyWiseDiv = false;
    this.isShowProcessingHq = true;
    this.headerSaleType = selectedType;
    // Create chart instance

    let params = {
      userId: this.currentUser.employeeCode,
      fromDate: this.fromDate,
      toDate: this.toDate,
      APIEndPoint: this.currentUser.company.APIEndPointHQWise,
      stateIDs: req.code.toString()
    };

    console.log('params', params);
    if (selectedType == "Primary" || selectedType == "SalesReturn") {
      params['Flag'] = "PS";
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
    } else if (selectedType == "Collection") {
      params['Flag'] = "CL";
      //params['month'] = this.month;
      //params['year'] = this.year;
    } else if (selectedType == "outstanding") {
      params['Flag'] = "OS";
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
    }

    if (type == "graph") {
      params["type"] = selectedType
    } else {
      params["type"] = selectedType
    }

    this.ERPServiceService.getHQWiseProductDetails(params).subscribe((res) => {
      if (res.length > 0) {
        let result = [];
        this.totalHq = 0;
        if (this.hqGraphDispose) {
          this.hqGraphDispose.dispose();
          this.hqGraphDispose = undefined;
        }
        //let chartHq = am4core.create("HqWiseGraph", am4charts.XYChart);
        //this.hqGraphDispose = chartHq;
        //chartHq.scrollbarX = new am4core.Scrollbar();
        res.forEach(element => {
          if (selectedType == "Primary") {
            // chartHq.data.push({
            //   "Headquarter": element.HQName,
            //   "Sales": element.primarysaleamount,
            //   "HeadquaterCode": element.HQCode,
            //   "ProductName": element.productname,
            //   "qty":element.primarysaleqty,
            //   "itemRate":element.itemrate
            // })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.primarysaleamount,
              "ProductName": element.productname,
              "HeadquaterCode": element.HQCode,
              "selectedType": selectedType,
              "qty": element.primarysaleqty,
              "itemRate": element.itemrate
            });
            //let val = element.itemrate;
            //let total = element.primarysaleqty * val;
            this.totalQtyHq = this.totalQtyHq + parseFloat(element.primarysaleqty);
            this.itemwiseTotalValueHq = this.itemwiseTotalValueHq + (element.itemrate * element.primarysaleqty);
            this.totalAmountHq = this.totalAmountHq + parseFloat(element.primarysaleamount);
          } else if (selectedType == "Outstanding") {
            // chartHq.data.push({
            //   "Headquarter": element.HQName,
            //   "Sales": element.outstandingamount,
            //   "HeadquaterCode": element.HQCode,
            //   "ProductName": element.productname
            // })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.outstandingamount,
              "HeadquaterCode": element.HQCode,
              "ProductName": element.productname,
              "selectedType": selectedType
            });
            this.totalAmountHq = this.totalAmountHq + parseFloat(element.outstandingamount);
          } else if (selectedType == "Collection") {
            // chartHq.data.push({
            //   "Headquarter": element.HQName,
            //   "Sales": element.collectionamount,
            //   "HeadquaterCode": element.HQCode,
            //   "ProductName": element.productname
            // })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.collectionamount,
              "HeadquaterCode": element.HQCode,
              "ProductName": element.productname,
              "selectedType": selectedType
            });
            this.totalAmountHq = this.totalAmountHq + parseFloat(element.collectionamount);
          } else if (selectedType == "SalesReturn") {
            // chartHq.data.push({
            //   "Headquarter": element.HQName,
            //   "Sales": element.salereturnamount,
            //   "HeadquaterCode": element.HQCode,
            //   "ProductName": element.productname,
            //   "qty":element.salereturnqty,
            //   "itemRate":element.itemrate
            // })
            result.push({
              "stateName": element.StateName,
              "Headquater": element.HQName,
              "amount": element.salereturnamount,
              "HeadquaterCode": element.HQCode,
              "ProductName": element.productname,
              "selectedType": selectedType,
              "qty": element.salereturnqty,
              "itemRate": element.itemrate
            });
            //let val = element.itemrate;
            //let total = element.salereturnqty * val;
            this.totalQtyHq = this.totalQtyHq + parseFloat(element.salereturnqty);
            this.itemwiseTotalValueHq = this.itemwiseTotalValueHq + (element.itemrate * element.salereturnqty);
            this.totalAmountHq = this.totalAmountHq + parseFloat(element.salereturnamount);
          }
        });
        this.headerStateName = req.StateName;
        this.dataSourceHq1 = new MatTableDataSource(result);
        //this.scroll(this.hq as HTMLElement);
        setTimeout(() => {
          this.hqTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        }, 500)

        // let categoryAxis = chartHq.xAxes.push(new am4charts.CategoryAxis());
        // categoryAxis.dataFields.category = "Headquarter";
        // categoryAxis.renderer.grid.template.location = 0;
        // categoryAxis.renderer.minGridDistance = 30;
        // categoryAxis.renderer.labels.template.horizontalCenter = "right";
        // categoryAxis.renderer.labels.template.verticalCenter = "middle";
        // categoryAxis.renderer.labels.template.rotation = -30;
        // categoryAxis.tooltip.disabled = true;
        // categoryAxis.renderer.minHeight = 110;

        // let valueAxis = chartHq.yAxes.push(new am4charts.ValueAxis());
        // valueAxis.renderer.minWidth = 50;

        // // Create series
        // let series = chartHq.series.push(new am4charts.ColumnSeries());
        // series.sequencedInterpolation = true;
        // series.dataFields.valueY = "Sales";
        // series.dataFields.categoryX = "Headquarter";
        // series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        // series.columns.template.strokeWidth = 0;

        // series.tooltip.pointerOrientation = "vertical";
        // categoryAxis.renderer.cellStartLocation = 0.2;
        // categoryAxis.renderer.cellEndLocation = 0.8;
        // series.columns.template.width = am4core.percent(40);
        // series.columns.template.column.cornerRadiusTopLeft = 10;
        // series.columns.template.column.cornerRadiusTopRight = 10;
        // series.columns.template.column.fillOpacity = 0.8;
        // let label = categoryAxis.renderer.labels.template;
        // label.wrap = true;
        // // on hover, make corner radiuses bigger
        // let hoverState = series.columns.template.column.states.create("hover");
        // hoverState.properties.cornerRadiusTopLeft = 0;
        // hoverState.properties.cornerRadiusTopRight = 0;
        // hoverState.properties.fillOpacity = 1;

        // series.columns.template.adapter.add("fill", function (fill, target) {
        //   return chartHq.colors.getIndex(target.dataItem.index);
        // });

        // // Cursor
        // chartHq.cursor = new am4charts.XYCursor();
        // series.columns.template.events.on("hit", function (ev) {
        //   this.getPartyWiseData(ev.target.dataItem['dataContext'], "graph", selectedType);
        // }, this);
        this.showHQWiseDiv = true;
        this.isShowProcessingHq = false;
      }
      this.changeDetectorRef.detectChanges();
    })

  }

  totalQtyParty = 0.0;
  itemwiseTotalValueParty = 0.0;
  getPartyWiseProductData(req, type, selectedType) {
    this.totalQtyParty = 0.0;
    this.itemwiseTotalValueParty = 0.0;
    this.showPartyWiseDiv = false;
    this.isShowProcessingParty = true;
    this.headerSaleType = selectedType;
    // Create chart instance
    let params = {
      //companyId: this.currentUser.companyId,
      //userId: this.currentUser.employeeCode,
      fromDate: this.fromDate,
      toDate: this.toDate,
      APIEndPoint: this.currentUser.company.APIEndPointHQWise,
      HQIDs: req['HeadquaterCode']
    };
    if (selectedType == "Primary" || selectedType == "SalesReturn") {
      params['Flag'] = "PS";
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
    } else if (selectedType == "Collection") {
      params['Flag'] = "CL";
      //params['month'] = this.month;
      //params['year'] = this.year;
    } else if (selectedType == "outstanding") {
      params['Flag'] = "OS";
      //params['fromDate'] = this.fromDate;
      //params['toDate'] = this.toDate;
    }

    if (type == "graph") {
      params["type"] = selectedType
    } else {
      params["type"] = selectedType
    }
    this.ERPServiceService.getPartyWiseProductDetails(params).subscribe((res) => {
      if (res.length > 0) {
        if (this.partyGraphDispose) {
          this.partyGraphDispose.dispose();
          this.partyGraphDispose = undefined;
        }
        //let chartParty = am4core.create("PartyWiseGraph", am4charts.XYChart);
        //this.partyGraphDispose = chartParty;
        //chartParty.scrollbarX = new am4core.Scrollbar();
        let result = [];
        this.totalParty = 0;
        res.forEach(element => {
          if (selectedType == "Primary") {
            // chartParty.data.push({
            //   "PartyName": element.PartyName,
            //   "Sales": element.primarysaleamount,
            //   "PartyCode": element.PartyCode,
            //   "ProductName": element.productname,
            //   "qty":element.primarysaleqty,
            //   "itemRate":element.itemrate
            // })
            result.push({
              //"stateName":element.StateName,
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "ProductName": element.productname,
              "amount": element.primarysaleamount,
              "qty": element.primarysaleqty,
              "itemRate": element.itemrate
            });

            //let total = element.itemrate * element.primarysaleqty;
            this.totalQtyParty = this.totalQtyParty + parseFloat(element.primarysaleqty);
            this.itemwiseTotalValueParty = this.itemwiseTotalValueParty + (element.itemrate * element.primarysaleqty);
            this.totalParty = this.totalParty + parseFloat(element.primarysaleamount);
          } else if (selectedType == "Outstanding") {
            // chartParty.data.push({
            //   "PartyName": element.PartyName,
            //   "Sales": element.outstandingamount,
            //   "ProductName": element.productname,
            //   "PartyCode": element.PartyCode
            // })
            result.push({
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "ProductName": element.productname,
              "amount": element.outstandingamount
            });
            this.totalParty = this.totalParty + parseFloat(element.outstandingamount);
          } else if (selectedType == "Collection") {
            // chartParty.data.push({
            //   "PartyName": element.PartyName,
            //   "Sales": element.collectionamount,
            //   "ProductName": element.productname,
            //   "PartyCode": element.PartyCode
            // })
            result.push({
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "ProductName": element.productname,
              "amount": element.collectionamount
            });
            this.totalParty = this.totalParty + parseFloat(element.collectionamount);
          } else if (selectedType == "SalesReturn") {
            // chartParty.data.push({
            //   "PartyName": element.PartyName,
            //   "Sales": element.salereturnamount,
            //   "ProductName": element.productname,
            //   "PartyCode": element.PartyCode,
            //   "qty":element.salereturnqty,
            //   "itemRate":element.itemrate
            // })
            result.push({
              "hqName": element.HQName,
              "PartyName": element.PartyName,
              "ProductName": element.productname,
              "amount": element.salereturnamount,
              "qty": element.salereturnqty,
              "itemRate": element.itemrate
            });
            //let val = element.itemrate;
            //let total = element.salereturnamount * val;
            this.totalQtyParty = this.totalQtyParty + parseFloat(element.salereturnqty);
            this.itemwiseTotalValueParty = this.itemwiseTotalValueParty + (element.itemrate * element.salereturnqty);
            this.totalParty = this.totalParty + parseFloat(element.salereturnamount);
          }
        });
        this.headerHqName = req['Headquarter'];
        this.dataSourceParty1 = new MatTableDataSource(result);

        // let categoryAxis = chartParty.xAxes.push(new am4charts.CategoryAxis());
        // categoryAxis.dataFields.category = "PartyName";
        // categoryAxis.renderer.grid.template.location = 0;
        // categoryAxis.renderer.minGridDistance = 30;
        // categoryAxis.renderer.labels.template.horizontalCenter = "right";
        // categoryAxis.renderer.labels.template.verticalCenter = "middle";
        // categoryAxis.renderer.labels.template.rotation = -30;
        // categoryAxis.tooltip.disabled = true;
        // categoryAxis.renderer.minHeight = 110;

        // let valueAxis = chartParty.yAxes.push(new am4charts.ValueAxis());
        // valueAxis.renderer.minWidth = 50;

        // // Create series
        // let series = chartParty.series.push(new am4charts.ColumnSeries());
        // series.sequencedInterpolation = true;
        // series.dataFields.valueY = "Sales";
        // series.dataFields.categoryX = "PartyName";
        // series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        // series.columns.template.strokeWidth = 0;
        // let label = categoryAxis.renderer.labels.template;
        // label.wrap = true;
        // series.tooltip.pointerOrientation = "vertical";
        // categoryAxis.renderer.cellStartLocation = 0.2;
        // categoryAxis.renderer.cellEndLocation = 0.8;
        // series.columns.template.width = am4core.percent(40);
        // series.columns.template.column.cornerRadiusTopLeft = 10;
        // series.columns.template.column.cornerRadiusTopRight = 10;
        // series.columns.template.column.fillOpacity = 0.8;

        // // on hover, make corner radiuses bigger
        // let hoverState = series.columns.template.column.states.create("hover");
        // hoverState.properties.cornerRadiusTopLeft = 0;
        // hoverState.properties.cornerRadiusTopRight = 0;
        // hoverState.properties.fillOpacity = 1;

        // series.columns.template.adapter.add("fill", function (fill, target) {
        //   return chartParty.colors.getIndex(target.dataItem.index);
        // });

        // // Cursor
        // chartParty.cursor = new am4charts.XYCursor();
        // series.columns.template.events.on("hit", function (ev) {
        //   //this.getHQWiseDetails(ev.target.dataItem['dataContext'], "graph",'');
        // }, this);
        this.showPartyWiseDiv = true;
        this.isShowProcessingParty = false;
        setTimeout(() => {
          this.partyTableRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        }, 500)
        this.changeDetectorRef.detectChanges();
      }
    })
    this.changeDetectorRef.detectChanges();
  }

  // --------------------------------------GOURAV 30-03-2021 ---------------------------------------
  toggleFilterState(event: MatSlideToggleChange) {
    this.isToggledState = event.checked;
    if (event.checked) {
      this.colMdState = "col-12";
    } else {
      this.colMdState = "col-6";
    }
  }
  isToggledHq = false;
  colMdHq = "col-6";
  toggleFilterHq(event: MatSlideToggleChange) {
    this.isToggledHq = event.checked;
    if (event.checked) {
      this.colMdHq = "col-12";
    } else {
      this.colMdHq = "col-6";
    }
  }
  isToggledParty = false;
  colMdParty = "col-6";
  toggleFilterParty(event: MatSlideToggleChange) {
    this.isToggledParty = event.checked;
    if (event.checked) {
      this.colMdParty = "col-12";
    } else {
      this.colMdParty = "col-6";
    }
  }


}
