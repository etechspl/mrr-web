import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErpgraphsHqwiseComponent } from './erpgraphs-hqwise.component';

describe('ErpgraphsHqwiseComponent', () => {
  let component: ErpgraphsHqwiseComponent;
  let fixture: ComponentFixture<ErpgraphsHqwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErpgraphsHqwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErpgraphsHqwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
