import { TodateComponent } from './../filters/todate/todate.component';
import {
	Input,
	EventEmitter,
	Output,
	SimpleChange,
} from "@angular/core";
import { retryWhen, delay, take } from "rxjs/operators";
import { LayoutConfigService } from "../../../../core/services/layout-config.service";
import { SubheaderService } from "../../../../core/services/layout/subheader.service";
import * as objectPath from "object-path";
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { TourProgramService } from "../../../../core/services/TourProgram/tour-program.service";
import { ProviderService } from '../../../../core/services/Provider/provider.service';

import { PrimaryAndSaleReturnService } from "../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service";
import { StateService } from "../../../../core/services/state.service";
// import { BroadcastMessageService } from "../../../../core/services/BroadcastMessage/broadcast-message.service";
import { ERPServiceService } from "../../../../core/services/erpservice.service";
import * as moment from "moment";
import { LeaveApprovalService } from "../../../../core/services/leave-approval.service";
import { CrmService } from "../../../../core/services/CRM/crm.service";

import { log } from 'console';
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import {
	Component,
	OnInit,
	ViewChild,
	ChangeDetectorRef,
	ChangeDetectionStrategy,
	ElementRef,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import * as _moment from "moment";
declare var $;
import Swal from "sweetalert2";
import { ActivatedRoute } from "@angular/router";
import { MatPaginator, MatSlideToggleChange, MatTableDataSource } from "@angular/material";
import { DivisionComponent } from '../filters/division/division.component';
import { DistrictComponent } from '../filters/district/district.component';
import { EmployeeComponent } from '../filters/employee/employee.component';
import { DcrReportsService } from '../../../../core/services/DCR/dcr-reports.service';
import { HierarchyService } from '../../../../core/services/hierarchy-service/hierarchy.service';
import { StateComponent } from '../filters/state/state.component';
import { TypeComponent } from '../filters/type/type.component';
import { values } from 'lodash';
import { DcrProviderVisitDetailsService } from '../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
import { MonthYearService } from "../../../../core/services/month-year.service";
export interface trafficLight {
	employeeName: String;
	state: String;
	district: String;
	target: String;
	primary: String;
	secondary: String;
	drCallAvg: String;
	score: String;
	rowColor: String;
}

const tableData: trafficLight[] = [
	{
		employeeName: "Praveen Kumar",
		state: "Delhi",
		district: "South Delhi",
		target: "896540",
		primary: "796540",
		secondary: "100000",
		drCallAvg: "98.5",
		score: "98",
		rowColor: "A5D6A7",
	},
	{
		employeeName: "Anjana Rana",
		state: "Punjab",
		district: "Nangal",
		target: "896540",
		primary: "796540",
		secondary: "100000",
		drCallAvg: "97",
		score: "96.5",
		rowColor: "A5D6A7",
	},
	{
		employeeName: "Praveen Singh",
		state: "Rajasthan",
		district: "Pali",
		target: "896540",
		primary: "796540",
		secondary: "100000",
		drCallAvg: "93",
		score: "90",
		rowColor: "A5D6A7",
	},
	{
		employeeName: "Ravindra Singh",
		state: "Haryana",
		district: "Bhiwani",
		target: "896540",
		primary: "796540",
		secondary: "100000",
		drCallAvg: "83.5",
		score: "84",
		rowColor: "FFF9C4",
	},
	{
		employeeName: "Rahul Kumar",
		state: "Punjab",
		district: "Nangal",
		target: "896540",
		primary: "796540",
		secondary: "100000",
		drCallAvg: "85",
		score: "81",
		rowColor: "FFF9C4",
	},
	{
		employeeName: "Preeti",
		state: "Haryana",
		district: "Rohtak",
		target: "896540",
		primary: "796540",
		secondary: "100000",
		drCallAvg: "80",
		score: "78",
		rowColor: "ffcdd2",
	},
	{
		employeeName: "Chaman Deep Singh",
		state: "Bihar",
		district: "Buxar",
		drCallAvg: "79.5",
		target: "896540",
		primary: "796540",
		secondary: "100000",
		score: "75.5",
		rowColor: "ffcdd2",
	},
];

@Component({
	selector: "m-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ['./dashboard.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	currencySymbol;
	//currencySymbol='₹'
	@ViewChild("callTypeComponent")
	callTypeComponent: TypeComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callStateComponent")
	callStateComponent: StateComponent;

	@ViewChild("MatPaginator")
	paginator: MatPaginator;
	@ViewChild("doctorPaginator") doctorPaginator: MatPaginator;

	displayedColumns = ["sno", "headquarter", "target", "primary", "secondary"];
	displayedColumnsForMonthOnMonthTrendTable = ["type", "value"];
	//displayedColumns = ['position', 'name', 'weight', 'symbol'];
	dataSource;
	dataSourceForMonthOnMonthTrendTable = [];
	dataSource1 = new MatTableDataSource(tableData);
	displayedColumns1: string[] = ['employeeName', 'state', 'district', 'target', 'primary', 'secondary', 'drCallAvg', 'score'];
	dataSource3;
	dataSources1: any;
	dataSource4;
	displayedColumns3: string[] = ['sno', 'Name', 'Pending', 'Date']
	displayedColumns4: string[] = ['sno', 'Name', 'designation']
	otherTypes = ['Drug', 'Stockist'];
	Mdate = true;
	Ddata: string;
	dcrStatusData;
	currentMonth = new Date().getMonth();
	currentYear = new Date().getFullYear();
	Date = new Date().getDate();


	lastDay = new Date(this.currentYear, this.currentMonth, 0).getDate();
	actualMonth = this.currentMonth + 1;
	currentDate = this.currentYear + "-" + this.actualMonth + "-" + this.Date;
	currentMonthStartDate = this.currentYear + "-" + this.actualMonth + "-01";
	currentMonthLastDate =
		this.currentYear + "-" + this.actualMonth + "-" + this.lastDay;
	currentYearStartDate = this.currentYear + "-01-01";
	currentYearLastDate = this.currentYear + "-12-31";
	monthsName = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];
	currentMonthName = this.monthsName[this.currentMonth];
	basicObject = {};
	// broadcastMessage = [];
	public config: any;
	private chart: any;
	public showERPDashboard = false;

	public totalActiveUser: number = 0;
	public totalPrimCount: number = 0;
	public totalCollecCount: number = 0;
	public totalOutCount: number = 0;

	public totalSubmittedDCR: number = 0;
	public totalPendingDCR: number = 0;
	public teamDoctorCallAvg: number = 0;
	public teamChemistCallAvg: number = 0;
	public drPOB: number = 0;
	public venPOB: number = 0;
	public combDocvenPOB: number = 0;
	public totalPOB: number = 0;
	public totalDayPob: number = 0;
	public tppendding: number = 0;
	public dcrgetRemenderCalls: number = 0;
	public stkPOB: number = 0;
	public totalLeaveCount: number = 0;

	public totalActiveCount: number = 0;
	public onLeave: number = 0;

	public sponsorshipApprovalCount: number = 0;
	@Input() leaveApproval: any;
	@Input() LeaveDetails: any;

	//-----Variable Used For Month On Month Trend Graph-------------------
	selectedValueForMonthOnMonthTrendGraph = "option2";
	last6MonthFromDate;
	toDate;
	public dataProviderForTotalProviderVisited = [];
	public dataProviderForTotalDoctorVisited = [];
	public dataProviderForTotalVendorVisited = [];
	public dataProviderForTotalStockistVisited = [];

	public dataProviderForTotalDistinctProviderVisited = [];
	public dataProviderForTotalDistinctDoctorVisited = [];
	public dataProviderForTotalDistinctVendorVisited = [];
	public dataProviderForTotalDistinctStockistVisited = [];

	public dataProviderForOverAllCallAvg = [];
	public dataProviderForDoctorCallAvg = [];
	public dataProviderForVendorCallAvg = [];
	public dataProviderForStockistCallAvg = [];

	public viewTitle = "";


	public showDashobad = false;
	public showMDSRDashobard = false;
	//----------------------------____END---------------------------------------------

	public stateNameForTarget = "";

	//----------Pk 09-05-22019--------------
	hqDataSource;
	public stateNameForTargetDemo = "Bihar";
	hqNameForTargetDemo = "";
	partWiseDataSource;
	partWiseSecondaryDataSource;
	showPartyData = false;
	getRemenderCallsTable = false;
	gettprecode = false;
	showERPDemoDashboard = false;
	showTargetDashboard = false;
	displayedColumnsForTrafficLight = [
		"employeeName",
		"state",
		"district",
		"target",
		"primary",
		"secondary",
		"drCallAvg",
		"score",
	];
	dashboardForm: FormGroup;
	tpFislter: FormGroup;

	dataSourceForTrafficLight;



	unlistedValidations = {
		unlistedDocCall: this.currentUser.company.validation.unlistedDocCall,
		unlistedVenCall: this.currentUser.company.validation.unlistedVenCall,
		unlistedDocAvg: this.currentUser.company.validation.unlistedDocAvg,
		unlistedVenAvg: this.currentUser.company.validation.unlistedVenAvg,
	};
	//----------PK END----------------------
	constructor(
		private router: Router,
		private toastr: ToastrService,
		private providerService: ProviderService,
		private layoutConfigService: LayoutConfigService,
		private _DCRReportService: DcrReportsService,
		private subheaderService: SubheaderService,
		private AmCharts: AmChartsService,
		private fb: FormBuilder,
		private _dcrService: DcrReportsService,
		private _tpService: TourProgramService,
		private _changeDetectorRef: ChangeDetectorRef,
		private _primaryAndSaleReturnService: PrimaryAndSaleReturnService,
		private _stateService: StateService,
		// private _broadcastMessageService: BroadcastMessageService,
		private ERPServiceService: ERPServiceService,
		private _LeaveApprovalService: LeaveApprovalService,

		private _CrmService: CrmService,
		private _hierarchyService: HierarchyService,
		private _dcrprovidervisitDetails: DcrProviderVisitDetailsService,
		public hierarchyService: HierarchyService,
		private monthYearService: MonthYearService

	) {
		this.dashboardForm = this.fb.group({
			type: new FormControl(null, Validators.required),
			stateInfo: new FormControl(null),
			districtInfo: new FormControl(null),
			employeeId: new FormControl(null),
			designation: new FormControl(null),
		}),
			this.tpFislter = this.fb.group({
				month: new FormControl(true),
				year: new FormControl(true),
			});
	}

	stokistAverage: any;

	showParty(hqName) {
		this.hqNameForTargetDemo = hqName;
		if (hqName == "DEORIYA") {
			this.partWiseDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "550860.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "155030.00",
				},
			];
		} else if (hqName == "GORAKHPUR") {
			this.partWiseDataSource = [
				{
					partyName: "NAVEEN PHARMA",
					partyAmount: "334546.80",
				},
				{
					partyName: "RAJ BANSHI MEDICO",
					partyAmount: "238962.00",
				},
				{
					partyName: "SAMEYA PHARMA",
					partyAmount: "143377.20",
				},
			];

			this.partWiseSecondaryDataSource = [
				{
					partyName: "NAVEEN PHARMA",
					partyAmount: "42000.00",
				},
				{
					partyName: "RAJ BANSHI MEDICO",
					partyAmount: "30000.00",
				},
				{
					partyName: "SAMEYA PHARMA",
					partyAmount: "18000.00",
				},
			];
		} else if (hqName == "PATNA") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "PURNEA") {
			this.partWiseDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "759850",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "90150.00",
				},
			];
		} else if (hqName == "East Delhi") {
			this.partWiseDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "550860.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "155030.00",
				},
			];
		} else if (hqName == "North Delhi") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "South Delhi") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "West Delhi") {
			this.partWiseDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "759850",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "90150.00",
				},
			];
		} else if (hqName == "Ambala") {
			this.partWiseDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "550860.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "155030.00",
				},
			];
		} else if (hqName == "Hisar") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "Jind") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "Sonipat") {
			this.partWiseDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "759850",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "90150.00",
				},
			];
		} else if (hqName == "Kangra") {
			this.partWiseDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "550860.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "155030.00",
				},
			];
		} else if (hqName == "Mandi") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "Shimla") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "Una") {
			this.partWiseDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "759850",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "ALIYA DISTRIBUTTORS",
					partyAmount: "90150.00",
				},
			];
		} else if (hqName == "Agra") {
			this.partWiseDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "550860.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "DAAS MEDICAL AGENCY",
					partyAmount: "155030.00",
				},
			];
		} else if (hqName == "Lucknow") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		} else if (hqName == "Kanpur") {
			this.partWiseDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "379500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "310500.00",
				},
			];
			this.partWiseSecondaryDataSource = [
				{
					partyName: "PRASAD DRUG DISTRIBUTORS",
					partyAmount: "115500.00",
				},
				{
					partyName: "SANJEEVNI MEDICALS",
					partyAmount: "94500.00",
				},
			];
		}

		this.showPartyData = true;
		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;
	}

	generateTargetGraphForDemo(stateId, stateName) {
		this.showPartyData = false;
		let hqDataProvider = [];
		this.stateNameForTargetDemo = stateName;
		if (stateId == "biharId") {
			this.hqDataSource = [
				{
					sno: 1,
					headquarter: "DEORIYA",
					target: "705890",
					primary: "550860",
					secondary: "155030",
				},
				{
					sno: 2,
					headquarter: "GORAKHPUR",
					target: "896540",
					primary: "796540",
					secondary: "100000",
				},
				{
					sno: 4,
					headquarter: "PATNA",
					target: "900000",
					primary: "690000",
					secondary: "210000",
				},
				{
					sno: 5,
					headquarter: "PURNEA",
					target: "850000",
					primary: "759850",
					secondary: "90150",
				},
				//, {
				// 	'sno': 6,
				// 	'headquarter': 'GAYA',
				// 	'target': '950000',
				// 	'primary': '852310',
				// 	'secondary': '97690'
				// }, {
				// 	'sno': 7,
				// 	'headquarter': 'KATIHAR',
				// 	'target': '700000',
				// 	'primary': '682000',
				// 	'secondary': '180000'
				// }, {
				// 	'sno': 8,
				// 	'headquarter': 'MADHUBANI',
				// 	'target': '765000',
				// 	'primary': '600000',
				// 	'secondary': '165000'
				// }, {
				// 	'sno': 9,
				// 	'headquarter': 'MUZAFFAPUR',
				// 	'target': '823000',
				// 	'primary': '650000',
				// 	'secondary': '173000'
				// }, {
				// 	'sno': 10,
				// 	'headquarter': 'SAMASTIPUR',
				// 	'target': '682500',
				// 	'primary': '450000',
				// 	'secondary': '232500'
				// }
			];

			hqDataProvider = [
				{
					category: "DEORIYA",
					"column-1": 7.05,
					"column-2": 5.5,
					"column-3": 1.55,
				},
				{
					category: "GORAKHPUR",
					"column-1": 8.9,
					"column-2": 7.9,
					"column-3": 1.0,
				},
				{
					category: "PATNA",
					"column-1": 9.0,
					"column-2": 6.9,
					"column-3": 2.1,
				},
				{
					category: "PURNEA",
					"column-1": 8.5,
					"column-2": 7.5,
					"column-3": 0.99,
				},
			];
		} else if (stateId == "delhiId") {
			this.hqDataSource = [
				{
					sno: 1,
					headquarter: "East Delhi",
					target: "705890",
					primary: "550860",
					secondary: "155030",
				},
				{
					sno: 2,
					headquarter: "North Delhi",
					target: "896540",
					primary: "796540",
					secondary: "100000",
				},
				{
					sno: 4,
					headquarter: "South Delhi",
					target: "900000",
					primary: "690000",
					secondary: "210000",
				},
				{
					sno: 5,
					headquarter: "West Delhi",
					target: "850000",
					primary: "759850",
					secondary: "90150",
				},
			];

			hqDataProvider = [
				{
					category: "East Delhi",
					"column-1": 7.05,
					"column-2": 5.5,
					"column-3": 1.55,
				},
				{
					category: "North Delhi",
					"column-1": 8.9,
					"column-2": 7.9,
					"column-3": 1.0,
				},
				{
					category: "South Delhi",
					"column-1": 9.0,
					"column-2": 6.9,
					"column-3": 2.1,
				},
				{
					category: "West Delhi",
					"column-1": 8.5,
					"column-2": 7.5,
					"column-3": 0.99,
				},
			];
		} else if (stateId == "haryanaId") {
			this.hqDataSource = [
				{
					sno: 1,
					headquarter: "Ambala",
					target: "705890",
					primary: "550860",
					secondary: "155030",
				},
				{
					sno: 2,
					headquarter: "Hisar",
					target: "896540",
					primary: "796540",
					secondary: "100000",
				},
				{
					sno: 4,
					headquarter: "Jind",
					target: "900000",
					primary: "690000",
					secondary: "210000",
				},
				{
					sno: 5,
					headquarter: "Sonipat",
					target: "850000",
					primary: "759850",
					secondary: "90150",
				},
			];

			hqDataProvider = [
				{
					category: "Ambala",
					"column-1": 7.05,
					"column-2": 5.5,
					"column-3": 1.55,
				},
				{
					category: "Hisar",
					"column-1": 8.9,
					"column-2": 7.9,
					"column-3": 1.0,
				},
				{
					category: "Jind",
					"column-1": 9.0,
					"column-2": 6.9,
					"column-3": 2.1,
				},
				{
					category: "Sonipat",
					"column-1": 8.5,
					"column-2": 7.5,
					"column-3": 0.99,
				},
			];
		} else if (stateId == "himachalPradeshId") {
			this.hqDataSource = [
				{
					sno: 1,
					headquarter: "Kangra",
					target: "705890",
					primary: "550860",
					secondary: "155030",
				},
				{
					sno: 2,
					headquarter: "Mandi",
					target: "896540",
					primary: "796540",
					secondary: "100000",
				},
				{
					sno: 4,
					headquarter: "Shimla",
					target: "900000",
					primary: "690000",
					secondary: "210000",
				},
				{
					sno: 5,
					headquarter: "Una",
					target: "850000",
					primary: "759850",
					secondary: "90150",
				},
			];

			hqDataProvider = [
				{
					category: "Kangra",
					"column-1": 7.05,
					"column-2": 5.5,
					"column-3": 1.55,
				},
				{
					category: "Mandi",
					"column-1": 8.9,
					"column-2": 7.9,
					"column-3": 1.0,
				},
				{
					category: "Shimla",
					"column-1": 9.0,
					"column-2": 6.9,
					"column-3": 2.1,
				},
				{
					category: "Una",
					"column-1": 8.5,
					"column-2": 7.5,
					"column-3": 0.99,
				},
			];
		} else if (stateId == "UttarPradeshId") {
			this.hqDataSource = [
				{
					sno: 1,
					headquarter: "Agra",
					target: "705890",
					primary: "550860",
					secondary: "155030",
				},
				{
					sno: 2,
					headquarter: "Lucknow",
					target: "896540",
					primary: "796540",
					secondary: "100000",
				},
				{
					sno: 4,
					headquarter: "Kanpur",
					target: "900000",
					primary: "690000",
					secondary: "210000",
				},
			];
			hqDataProvider = [
				{
					category: "Agra",
					"column-1": 7.05,
					"column-2": 5.5,
					"column-3": 1.55,
				},
				{
					category: "Lucknow",
					"column-1": 8.9,
					"column-2": 7.9,
					"column-3": 1.0,
				},
				{
					category: "Kanpur",
					"column-1": 9.0,
					"column-2": 6.9,
					"column-3": 2.1,
				},
			];
		}

		this.chart = this.AmCharts.makeChart("targetAchievementHqGraphDemo2", {
			type: "serial",
			categoryField: "category",
			"fontFamily": "Vardana",
			startDuration: 1,
			categoryAxis: {
				gridPosition: "start",
			},
			chartScrollbar: {
				autoGridCount: true,
				hideResizeGrips: true,
				scrollbarHeight: 20,
				maximum: 10,
				minimum: 5,
			},
			trendLines: [],
			graphs: [
				{
					balloonText: "[[title]] of [[category]]:[[value]]",
					bullet: "round",
					id: "AmGraph-1",
					title: "Target",
					valueField: "column-1",
					labelText: "[[column-1]]",
				},
				{
					balloonText: "[[title]] of [[category]]:[[value]]",
					bullet: "round",
					id: "AmGraph-2",
					title: "Primary",
					valueField: "column-2",
					labelText: "[[column-2]]",
				},
				{
					balloonText: "[[title]] of [[category]]:[[value]]",
					bullet: "round",
					id: "AmGraph-3",
					title: "Secondary",
					valueField: "column-3",
					labelText: "[[column-3]]",
				},
			],
			guides: [],
			valueAxes: [
				{
					id: "ValueAxis-1",
					// "title": " in Lakhs",
					title: ` Sales Amount in ${this.currencySymbol ? this.currencySymbol : "₹"
						}`,
					unit: "L",
				},
			],
			allLabels: [],
			balloon: {},
			legend: {
				enabled: true,
				useGraphSettings: true,
				markerSize: 5,
				verticalGap: 10,
			},
			export: {
				enabled: true,
			},
			titles: [
				{
					id: "Title-1",
					size: 15,
					text: "",
				},
			],
			dataProvider: hqDataProvider,
			listeners: [
				{
					event: "clickGraphItem",
					method: (event) => {
						alert("Getting Data...");
					},
				},
			],
		});
		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;
	}
	ngOnInit() {
		this.Ddata = 'Month';
		if (this.currentUser.country) {
			this.currencySymbol = this.currentUser.country.currencySymbol;
		} else {
			this.currencySymbol = "₹";
		}
		// <><><><><>*****   AAKASH 17-03-2020  *****<><><><>
		if (this.currentUser.userInfo[0].rL === 0) {
			let params = {
				companyId: this.currentUser.companyId,
				type: "dashboardCount",
			};
			this._LeaveApprovalService
				.getLeavesForApproval(params)
				.subscribe((res) => {
					this.leaveApproval = res;
					this.totalLeaveCount = res.length;
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				});

			this.callGetActive();
			this.callOnLeave();
		}

		// <><><><><>*****   AAKASH 17-03-2020  *****<><><><>

		//<> <><><><<><<<>>>>>*****Preeti Arora 8-03-2021---------
		if (this.currentUser.userInfo[0].rL === 0) {
			let obj = {
				companyId: this.currentUser.companyId,
				fromDate: this.currentMonthStartDate,
				toDate: this.currentMonthLastDate,
				designation: this.currentUser.userInfo[0].designationLevel
			}
			this._DCRReportService.getAllEmployeeCallAverage(obj).subscribe(res => {
				if (res.length > 0) {
					res.forEach(element => {
						this.drPOB += element.DoctorPOB;
						this.venPOB += element.VendorPOB;
						this.stkPOB += element.stockistPOB;
						this.totalPOB += element.TotalPOB;

					});
					this.combDocvenPOB = this.venPOB + this.drPOB;
				}
			});
		}

		//<><><><><><><><><>******************************************************

		//<><><><><><><><><>*****mahender*****************************************

		if (this.currentUser.userInfo[0].rL === 0) {
			let obj = {
				companyId: this.currentUser.companyId,
				designation: this.currentUser.userInfo[0].designationLevel
			}
			this._DCRReportService.getAlldayPobEmployeeCallAverage(obj).subscribe(res => {
				if (res.length > 0) {
					res.forEach(element => {
						this.drPOB += element.DoctorPOB;
						this.venPOB += element.VendorPOB;
						this.stkPOB += element.stockistPOB;
						this.totalDayPob += element.TotalPOB;

					});
					this.combDocvenPOB = this.venPOB + this.drPOB;
				}
			});
		}

		if (this.currentUser.userInfo[0].rL === 0) {
			let Obj = {
				companyId: this.currentUser.companyId,
			}
			this._tpService.getUserTpPendingdata(Obj).subscribe((res) => {
				if (res.length > 0) {
					this.tppendding = res.length;
				}

			})
		}



		if (this.currentUser.userInfo[0].rL === 0) {
			let Obj = {
				companyId: this.currentUser.companyId,

			}

			this._dcrprovidervisitDetails.getRemenderCalls(Obj).subscribe((res) => {
				if (res.length > 0) {
					this.dcrgetRemenderCalls = res.length;
				}

			})
		}
		this.onLoad(this.currentUser.companyId, this.currentUser.userInfo[0].designationLevel, this.currentUser.id);
		if (this.currentUser.company.lables.other) {
			this.currentUser.company.lables.otherType.forEach(i => {
				let a = Object.keys(i);
				this.otherTypes.push(i.key);
			});
		}
		//<><><><><><><><><>*****Mahender End*************************************

		if (this.currentUser.userInfo[0].rL == 2) {
			let userIds = [];
			let object = {
				supervisorId: this.currentUser.id,
				companyId: this.currentUser.companyId,
				type: "lower",
			};
			this._hierarchyService
				.getManagerHierarchy(object)
				.subscribe((res) => {
					res.forEach((user) => {
						userIds.push(user.userId);
					});
					let query = {
						userId: { inq: userIds },
					};
					this._CrmService
						.getRequestCountManagerHeirarchy(query)
						.subscribe((res) => {
							this.sponsorshipApprovalCount = res["count"];
							!this._changeDetectorRef["destroyed"]
								? this._changeDetectorRef.detectChanges()
								: null;
						});
				});
		} else if (this.currentUser.userInfo[0].rL == 0) {
			let query = {
				where: {
					companyId: this.currentUser.companyId,
				},
			};
			this._CrmService
				.getRequestDataManagerHeirarchy(query)
				.subscribe((res: any) => {
					let count = 0;
					res.forEach(element => {
						if (element.approvalStatusByAdmin === 'pending' && element.approvalStatusByMgr === "approved") {
							count += 1;
						}
					})
					this.sponsorshipApprovalCount = count;
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				});
		}
		//---------Preeti 12-02-2020--
		if (this.currentUser.company.isERPExist == true) {
			this.showERPDashboard = true;

			//this.getERPData();
		}

		//-----------------
		// Anjana 29-08-2019
		// this._broadcastMessageService
		// 	.getUserBroadcastMessage(
		// 		this.currentUser.companyId,
		// 		this.currentUser.id
		// 	)
		// 	.subscribe((res) => {
		// 		if (JSON.parse(JSON.stringify(res)).length > 0) {
		// 			this.broadcastMessage = JSON.parse(JSON.stringify(res));
		// 		}
		// 	});
		//--------PK 08-09-2019----------------
		if (this.currentUser.companyId != "5cd12d8c0e55b24ba8605772") {
			this.showDashobad = true;
			if (this.currentUser.id == "5cd47bc3a37aa250db0337e6") {
				//this.showERPDemoDashboard = true;
				this.showTargetDashboard = false;
				!this._changeDetectorRef["destroyed"]
					? this._changeDetectorRef.detectChanges()
					: null;
			} else {
				//	this.showERPDemoDashboard = false;
				this.showTargetDashboard = true;
				!this._changeDetectorRef["destroyed"]
					? this._changeDetectorRef.detectChanges()
					: null;
			}
		} else if (this.currentUser.companyId == "5cd12d8c0e55b24ba8605772") {
			this.showMDSRDashobard = true;
			!this._changeDetectorRef["destroyed"]
				? this._changeDetectorRef.detectChanges()
				: null;
		}
		//----------------END-------------------------------------------------
		let that = this;

		this.chart = this.AmCharts.makeChart("targetAchievementGraphDemo1", {
			type: "serial",
			categoryField: "category",
			"fontFamily": "Vardana",
			startDuration: 1,
			categoryAxis: {
				gridPosition: "start",
				labelRotation: 45,
			},
			trendLines: [],
			mouseWheelScrollEnabled: true,
			// "mouseWheelZoomEnabled": false,
			chartScrollbar: {
				autoGridCount: true,
				hideResizeGrips: true,
				scrollbarHeight: 20,
			},
			// (this.currencySymbol)?this.currencySymbol:'₹';
			graphs: [
				{
					balloonText: "[[title]] of [[category]]:[[value]]L",
					balloonFunction: (graphDataItem, graphs) => {
						var value = graphDataItem.values.value * 100000;
						// return graphs.title + ` of ` + graphDataItem.category + ": ₹" + value;
						return (
							graphs.title +
							` of ` +
							graphDataItem.category +
							`: ${this.currencySymbol ? this.currencySymbol : "₹"
							}` +
							value
						);
					},
					fillAlphas: 1,
					id: "AmGraph-1",
					title: "Target",
					type: "column",
					valueField: "column-1",
					labelText: "[[column-1]]",
					fixedColumnWidth: 22,
					colorField: "red",
				},
				{
					balloonText: "[[title]] of [[category]]:[[value]]L",
					balloonFunction: (graphDataItem, graphs) => {
						var value = graphDataItem.values.value * 100000;
						return (
							graphs.title +
							` of ` +
							graphDataItem.category +
							`: ${this.currencySymbol ? this.currencySymbol : "₹"
							}` +
							value
						);
					},
					fillAlphas: 1,
					id: "AmGraph-2",
					title: "Primary Sales",
					type: "column",
					valueField: "column-2",
					labelText: "[[column-2]]",
					fixedColumnWidth: 22,
				},
				{
					balloonText: "[[title]] of [[category]]:[[value]]L",
					balloonFunction: (graphDataItem, graphs) => {
						var value = graphDataItem.values.value * 100000;
						return (
							graphs.title +
							` of ` +
							graphDataItem.category +
							`: ${this.currencySymbol ? this.currencySymbol : "₹"
							}` +
							value
						);
					},

					fillAlphas: 1,
					id: "AmGraph-3",
					title: "Secondary Sales",
					type: "column",
					valueField: "column-3",
					labelText: "[[column-3]]",
					fixedColumnWidth: 22,
				},
			],
			guides: [],
			valueAxes: [
				{
					id: "ValueAxis-1",
					title: `Sales Amount in ${this.currencySymbol ? this.currencySymbol : "₹"
						}`,
					// "unit": "L",
					labelFunction: (value, valueText, valueAxis) => {
						if (value < 100) {
							return "" + value + "L";
						} else {
							return "" + 1 + "Cr.";
						}
					},
				},
			],
			allLabels: [],
			balloon: {},
			legend: {
				enabled: true,
				useGraphSettings: true,
			},
			titles: [
				{
					id: "Title-1",
					size: 20,
					text:
						"Sale Status of " +
						this.currentMonthName +
						"," +
						this.currentYear +
						" ",
				},
			],
			dataProvider: [
				{
					category: "Bihar",
					"column-1": 10,
					"column-2": 5,
					"column-3": 2,
					stateId: "biharId",
					stateName: "Bihar",
				},
				{
					category: "Delhi",
					"column-1": 15,
					"column-2": 10,
					"column-3": 8,
					stateId: "delhiId",
					stateName: "Delhi",
				},
				{
					category: "Haryana",
					"column-1": 7,
					"column-2": 5,
					"column-3": 4,
					stateId: "haryanaId",
					stateName: "Haryana",
				},
				{
					category: "Himacal Pradesh",
					"column-1": 11,
					"column-2": 9,
					"column-3": 1,
					stateId: "himachalPradeshId",
					stateName: "Himachal Pradesh",
				},
				{
					category: "Uttar Pradesh",
					"column-1": 14,
					"column-2": 10,
					"column-3": 7,
					stateId: "UttarPradeshId",
					stateName: "Uttar Pradesh",
				},
			],
			listeners: [
				{
					event: "clickGraphItem",
					method: (event) => {
						that.generateTargetGraphForDemo(
							event.item.dataContext.stateId,
							event.item.dataContext.stateName
						);
					},
				},
			],
			export: {
				enabled: true,
			},
		});
		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;

		this.chart = this.AmCharts.makeChart("targetAchievementHqGraphDemo2", {
			type: "serial",
			categoryField: "category",
			startDuration: 1,
			"fontFamily": "Vardana",
			categoryAxis: {
				gridPosition: "start",
				labelRotation: 45,
			},
			chartScrollbar: {
				autoGridCount: true,
				hideResizeGrips: true,
				scrollbarHeight: 20,
				maximum: 10,
				minimum: 5,
			},
			trendLines: [],
			graphs: [
				{
					balloonText: "[[title]] of [[category]]:[[value]]",
					bullet: "round",
					id: "AmGraph-1",
					title: "Target",
					valueField: "column-1",
					labelText: "[[column-1]]",
				},
				{
					balloonText: "[[title]] of [[category]]:[[value]]",
					bullet: "round",
					id: "AmGraph-2",
					title: "Primary",
					valueField: "column-2",
					labelText: "[[column-2]]",
				},
				{
					balloonText: "[[title]] of [[category]]:[[value]]",
					bullet: "round",
					id: "AmGraph-3",
					title: "Secondary",
					valueField: "column-3",
					labelText: "[[column-3]]",
				},
			],
			guides: [],
			valueAxes: [
				{
					id: "ValueAxis-1",
					title: ` Sales Amount in ${this.currencySymbol ? this.currencySymbol : "₹"
						}`,
					unit: "L",
				},
			],
			allLabels: [],
			balloon: {},
			legend: {
				enabled: true,
				useGraphSettings: true,
				markerSize: 5,
				verticalGap: 10,
			},
			export: {
				enabled: true,
			},
			titles: [
				{
					id: "Title-1",
					size: 20,
					text: "",
				},
			],
			dataProvider: [
				{
					category: "DEORIYA",
					"column-1": 7.05,
					"column-2": 5.5,
					"column-3": 1.55,
				},
				{
					category: "GORAKHPUR",
					"column-1": 8.9,
					"column-2": 7.9,
					"column-3": 1.0,
				},
				{
					category: "PATNA",
					"column-1": 9.0,
					"column-2": 6.9,
					"column-3": 2.1,
				},
				{
					category: "PURNEA",
					"column-1": 8.5,
					"column-2": 7.5,
					"column-3": 0.99,
				},
			],
			listeners: [
				{
					event: "clickGraphItem",
					method: (event) => {
						alert("Getting Data...");
					},
				},
			],
		});
		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;
		this.hqDataSource = [
			{
				sno: 1,
				headquarter: "DEORIYA",
				target: "705890",
				primary: "550860",
				secondary: "155030",
			},
			{
				sno: 2,
				headquarter: "GORAKHPUR",
				target: "896540",
				primary: "796540",
				secondary: "100000",
			},
			{
				sno: 4,
				headquarter: "PATNA",
				target: "900000",
				primary: "690000",
				secondary: "210000",
			},
			{
				sno: 5,
				headquarter: "PURNEA",
				target: "850000",
				primary: "759850",
				secondary: "90150",
			},
			// , {
			// 	'sno': 7,
			// 	'headquarter': 'KATIHAR',
			// 	'target': '700000',
			// 	'primary': '682000',
			// 	'secondary': '180000'
			// }, {
			// 	'sno': 8,
			// 	'headquarter': 'MADHUBANI',
			// 	'target': '765000',
			// 	'primary': '600000',
			// 	'secondary': '165000'
			// }, {
			// 	'sno': 9,
			// 	'headquarter': 'MUZAFFAPUR',
			// 	'target': '823000',
			// 	'primary': '650000',
			// 	'secondary': '173000'
			// }, {
			// 	'sno': 10,
			// 	'headquarter': 'SAMASTIPUR',
			// 	'target': '682500',
			// 	'primary': '450000',
			// 	'secondary': '232500'
			// }
		];
		this.dataSourceForTrafficLight = [
			{
				employeeName: "Praveen Kumar",
				state: "Delhi",
				district: "South Delhi",
				target: "896540",
				primary: "796540",
				secondary: "100000",
				drCallAvg: "98.5",
				score: "98",
				rowColor: "A5D6A7",
			},
			{
				employeeName: "Anjana Rana",
				state: "Punjab",
				district: "Nangal",
				target: "896540",
				primary: "796540",
				secondary: "100000",
				drCallAvg: "97",
				score: "96.5",
				rowColor: "A5D6A7",
			},
			{
				employeeName: "Praveen Singh",
				state: "Rajasthan",
				district: "Pali",
				target: "896540",
				primary: "796540",
				secondary: "100000",
				drCallAvg: "93",
				score: "90",
				rowColor: "A5D6A7",
			},
			{
				employeeName: "Ravindra Singh",
				state: "Haryana",
				district: "Bhiwani",
				target: "896540",
				primary: "796540",
				secondary: "100000",
				drCallAvg: "83.5",
				score: "84",
				rowColor: "FFF9C4",
			},
			{
				employeeName: "Rahul Kumar",
				state: "Punjab",
				district: "Nangal",
				target: "896540",
				primary: "796540",
				secondary: "100000",
				drCallAvg: "85",
				score: "81",
				rowColor: "FFF9C4",
			},
			{
				employeeName: "Preeti",
				state: "Haryana",
				district: "Rohtak",
				target: "896540",
				primary: "796540",
				secondary: "100000",
				drCallAvg: "80",
				score: "78",
				rowColor: "ffcdd2",
			},
			{
				employeeName: "Abhishek",
				state: "Haryana",
				district: "Rohtak",
				target: "120000",
				primary: "45000",
				secondary: "100000",
				drCallAvg: "80",
				score: "18",
				rowColor: "ffcdd2",
			},
			{
				employeeName: "Chaman Deep Singh",
				state: "Bihar",
				district: "Buxar",
				drCallAvg: "79.5",
				target: "896540",
				primary: "796540",
				secondary: "100000",
				score: "75.5",
				rowColor: "ffcdd2",
			},
		];
		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;

		let passingObjectForTargetGraphForState = {
			companyId: this.currentUser.companyId,
			type: "state",
			lookingFor: "all",
			month: this.actualMonth,
			year: this.currentYear,
			designationLevel: this.currentUser.userInfo[0].designationLevel,
			userId: this.currentUser.id,
			isDivisonExist: this.currentUser.company.isDivisionExist,
		};
		this._primaryAndSaleReturnService
			.getTargetVsAchievementData(passingObjectForTargetGraphForState)
			.subscribe(
				(res) => {
					let dynamicDataProvider = [];
					for (let i = 0; i < res.length; i++) {
						dynamicDataProvider.push({
							category: res[i].stateName,
							"column-1": res[i].target,
							"column-2": res[i].primary,
							"column-3": res[i].secondary,
							stateId: res[i].stateId,
							stateName: res[i].stateName,
						});
					}
					this.chart = this.AmCharts.makeChart(
						"targetAchievementGraph",
						{
							type: "serial",
							categoryField: "category",
							"fontFamily": "Vardana",
							startDuration: 1,
							categoryAxis: {
								gridPosition: "start",
								labelRotation: 45,
							},
							trendLines: [],
							mouseWheelScrollEnabled: false,
							// "mouseWheelZoomEnabled": false,
							chartScrollbar: {
								autoGridCount: true,
								hideResizeGrips: true,
								scrollbarHeight: 20,
							},
							graphs: [
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]L",
									balloonFunction: (
										graphDataItem,
										graphs
									) => {
										//var value = graphDataItem.values.value * 100000;
										let value = graphDataItem.values.value;
										return (
											graphs.title +
											` of ` +
											graphDataItem.category +
											`: ${this.currencySymbol
												? this.currencySymbol
												: "₹"
											}` +
											value
										);
									},
									fillAlphas: 1,
									id: "AmGraph-1",
									title: "Target",
									type: "column",
									valueField: "column-1",
									//labelText: "[[column-1]]", //hide top value of graph
									fixedColumnWidth: 22,
									colorField: "red",
								},
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]L",
									balloonFunction: (
										graphDataItem,
										graphs
									) => {
										//var value = graphDataItem.values.value * 100000;
										let value = graphDataItem.values.value;

										return (
											graphs.title +
											` of ` +
											graphDataItem.category +
											`: ${this.currencySymbol
												? this.currencySymbol
												: "₹"
											}` +
											value
										);
									},
									fillAlphas: 1,
									id: "AmGraph-2",
									title: "Primary Sales",
									type: "column",
									valueField: "column-2",
									//labelText: "[[column-2]]",
									fixedColumnWidth: 22,
								},
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]L",
									balloonFunction: (
										graphDataItem,
										graphs
									) => {
										//var value = graphDataItem.values.value * 100000;
										let value = graphDataItem.values.value;

										return (
											graphs.title +
											` of ` +
											graphDataItem.category +
											`: ${this.currencySymbol
												? this.currencySymbol
												: "₹"
											}` +
											value
										);
									},

									fillAlphas: 1,
									id: "AmGraph-3",
									title: "Secondary Sales",
									type: "column",
									valueField: "column-3",
									//	labelText: "[[column-3]]",
									fixedColumnWidth: 22,
								},
							],
							guides: [],
							valueAxes: [
								{
									id: "ValueAxis-1",
									title: `Sales Amount in ${this.currencySymbol
										? this.currencySymbol
										: "₹"
										} `,
									// "unit": "L",
									// "labelFunction": function (value, valueText, valueAxis) {
									// 	if (value < 9999999) {
									// 		return "" + value + "L"
									// 	} else {
									// 		return "" + 1 + "Cr."
									// 	}
									// }
								},
							],
							allLabels: [],
							balloon: {},
							legend: {
								enabled: true,
								useGraphSettings: true,
							},
							titles: [
								{
									id: "Title-1",
									size: 20,
									text:
										"Sale Status of " +
										this.currentMonthName +
										"," +
										this.currentYear +
										" ",
								},
							],
							dataProvider: dynamicDataProvider,
							listeners: [
								{
									event: "clickGraphItem",
									method: (event) => {
										that.generateTargetGraph(
											event.item.dataContext.stateId,
											event.item.dataContext.stateName
										);
									},
								},
							],
							export: {
								enabled: true,
							},
						}
					);
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				},
				(err) => {
					console.log(err);
				}
			);

		//----------Target Vs Achievement Graph Headquarter Wise---------------------------
		this._stateService
			.getOnlyFirstState(this.currentUser.companyId)
			.subscribe(
				(res) => {
					this.stateNameForTarget = res.stateName;
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;

					this.generateTargetGraph(res.id, res.stateName);
				},
				(err) => {
					console.log(err);
				}
			);
		//-----------------------END-------------------------------------------

		this._dcrService
			.getBasicDetails(
				this.currentUser.companyId,
				this.currentMonth + 1,
				this.currentYear,
				this.currentUser.userInfo[0].designationLevel,
				this.currentUser.userInfo[0].userId
			)
			.subscribe(
				(res) => {
					if (this.currentUser.userInfo[0].designationLevel != 1) {
						this.totalActiveUser = res[0].TeamsDCRDetails.totalUser;

						this.totalSubmittedDCR =
							res[0].TeamsDCRDetails.totalSubmittedDCR;

						this.totalPendingDCR =
							res[0].TeamsDCRDetails.totalPendingDCR;

						this.teamDoctorCallAvg =
							res[0].TeamsCallAvg.doctorCallAverage;

						this.teamChemistCallAvg =
							res[0].TeamsCallAvg.vendorCallAverage;

						this.stokistAverage = (parseFloat(res[0].TeamsCallAvg.doctorCallAverage) + parseFloat(res[0].TeamsCallAvg.vendorCallAverage)).toFixed(2);
					} else {
						this.totalActiveUser = res[0].TeamsDCRDetails.totalUser;

						this.totalSubmittedDCR =
							res[0].SelfDcrDetail.totalSubmittedDCR;

						this.totalPendingDCR =
							res[0].SelfDcrDetail.totalPendingDCR;

						this.teamDoctorCallAvg =
							res[0].SelfCallAvg.doctorCallAverage;

						this.teamChemistCallAvg =
							res[0].TeamsCallAvg.vendorCallAverage;

						this.stokistAverage =
							(((parseFloat(res[0].TeamsCallAvg.doctorCallAverage) + parseFloat(res[0].TeamsCallAvg.vendorCallAverage)) / 2)).toFixed(2);
					}
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				},
				(err) => {
					console.log(err);
				}
			);

		//---Doctor Vendor Avg. State or Employee Wise--------------
		this._dcrService
			.getCallAvgStateorEmp(
				this.currentUser.companyId,
				this.currentMonthStartDate,
				this.currentMonthLastDate,
				this.currentUser.userInfo[0].designationLevel,
				this.currentUser.userInfo[0].userId,
				this.unlistedValidations
			)
			.subscribe(
				(res) => {
					let data = [];

					for (let i = 0; i < res.length; i++) {
						if (
							this.currentUser.userInfo[0].designationLevel == 0
						) {
							data.push({
								category: res[i].stateName,
								"column-1": res[i].docCallAvg,
								"column-2": res[i].venCallAvg,
							});
						} else if (
							this.currentUser.userInfo[0].designationLevel >= 2
						) {
							data.push({
								category: res[i].empName,
								"column-1": res[i].docCallAvg,
								"column-2": res[i].venCallAvg,
							});
						}
					}

					this.chart = this.AmCharts.makeChart("chartdiv", {
						type: "serial",
						categoryField: "category",
						"fontFamily": "Vardana",
						startDuration: 1,
						categoryAxis: {
							gridPosition: "start",
							labelRotation: 45,
						},
						chartScrollbar: {
							enabled: true,
						},
						trendLines: [],
						graphs: [
							{
								balloonText:
									"[[title]] of [[category]]:[[value]]",
								fillAlphas: 1,
								id: "AmGraph-1",
								title:
									this.currentUser.company.lables
										.doctorLabel + " Avg.",
								type: "column",
								valueField: "column-1",
								labelText: "[[column-1]]",
								fixedColumnWidth: 22,
							},
							{
								balloonText:
									"[[title]] of [[category]]:[[value]]",
								fillAlphas: 1,
								id: "AmGraph-2",
								title:
									this.currentUser.company.lables
										.vendorLabel + " Avg.",
								type: "column",
								valueField: "column-2",
								labelText: "[[column-2]]",
								fixedColumnWidth: 22,
							},
						],
						guides: [],
						valueAxes: [
							{
								id: "ValueAxis-1",
								title: "Average",
							},
						],
						allLabels: [],
						balloon: {},
						legend: {
							enabled: true,
							useGraphSettings: true,
						},
						titles: [
							{
								id: "Title-1",
								size: 20,
								text:
									this.currentUser.company.lables
										.doctorLabel +
									" and " +
									this.currentUser.company.lables
										.vendorLabel +
									" Call Avg " +
									this.currentMonthName +
									"'" +
									this.currentYear,
							},
						],
						dataProvider: data,
						export: {
							enabled: true,
						},
					});
				},
				(err) => {
					console.log(err);
				}
			);
		//---Doctor Vendor Avg. State or Employee Wise End--------------

		//----------Doctor Vendor Avg. Month Wise------------------

		this._dcrService
			.getDrVenAvgMonthWise(
				this.currentUser.companyId,
				this.currentYearStartDate,
				this.currentYearLastDate,
				this.currentUser.userInfo[0].designationLevel,
				this.currentUser.userInfo[0].userId,
				this.unlistedValidations
			)
			.subscribe(
				(res) => {
					let dataProvider = [];
					let category;

					for (let i = 0; i < res.length; i++) {
						if (i == 0) {
							category = "Jan";
						} else if (i == 1) {
							category = "Feb";
						} else if (i == 2) {
							category = "March";
						} else if (i == 3) {
							category = "April";
						} else if (i == 4) {
							category = "May";
						} else if (i == 5) {
							category = "June";
						} else if (i == 6) {
							category = "July";
						} else if (i == 7) {
							category = "Aug";
						} else if (i == 8) {
							category = "Sept";
						} else if (i == 9) {
							category = "Oct";
						} else if (i == 10) {
							category = "Nov";
						} else if (i == 11) {
							category = "Dec";
						}

						dataProvider.push({
							category: category,
							"column-1": res[i].docCallAvg,
							"column-2": res[i].venCallAvg,
						});
					}

					this.chart = this.AmCharts.makeChart(
						"chartDivAvgMonthWise",
						{
							type: "serial",
							categoryField: "category",
							"fontFamily": "Vardana",
							startDuration: 1,
							categoryAxis: {
								gridPosition: "start",
								labelRotation: 45,
							},
							trendLines: [],
							graphs: [
								{
									balloonText:
										"[[title]] of [[category]] : [[value]]",
									fillAlphas: 1,
									id: "AmGraph-1",
									title: "Doctor Avg.",
									type: "column",
									valueField: "column-1",
									labelText: "[[column-1]]",
									fixedColumnWidth: 22,
								},
								{
									balloonText:
										"[[title]] of [[category]] : [[value]]",
									fillAlphas: 1,
									id: "AmGraph-2",
									title: "Chemist Avg.",
									type: "column",
									valueField: "column-2",
									labelText: "[[column-2]]",
									fixedColumnWidth: 22,
								},
							],
							guides: [],
							valueAxes: [
								{
									id: "ValueAxis-1",
									title: "Average",
								},
							],
							allLabels: [],
							balloon: {},
							legend: {
								enabled: true,
								useGraphSettings: true,
							},

							titles: [
								{
									id: "Title-1",
									size: 20,
									text: "",
								},
							],
							dataProvider: dataProvider,
							export: {
								enabled: true,
							},
						}
					);
				},
				(err) => {
					console.log(err);
				}
			);
		//----------Doctor Vendor Avg. Month Wise END------------------

		//------------Pie Chart-------------------
		this._tpService
			.getTotalSbmiitedAndApprovedTPStatus(
				this.currentMonth + 1,
				this.currentYear,
				this.currentUser.companyId,
				[true],
				this.currentUser.userInfo[0].designationLevel,
				this.currentUser.userInfo[0].userId
			)
			.subscribe(
				(res) => {
					let totalActiveUser = res[0]["totalActiveUser"];
					let totalUserSubmittedTP = res[0]["totalUserSubmittedTP"];
					let totalUserApprovedTP = res[0]["totalUserApprovedTP"];
					let totalUserNotApprovedTP =
						res[0]["totalUserNotApprovedTP"];
					let totalUserNotSubmittedTP =
						totalActiveUser - totalUserSubmittedTP;
					this.chart = this.AmCharts.makeChart("chartdiv6", {
						type: "pie",
						theme: "dark",
						balloonText:
							"[[title]] : <br><span style='font-size:14px'><b>  [[value]]</span>",
						labelText: "[[value]]",
						depth3D: 0,
						innerRadius: "50%",
						titleField: "category",
						valueField: "column-1",
						backgroundColor: "#E7D4D4",
						//"colors": ["#FF0F00", "#FF6600", "#FF9E01"],
						fontSize: 13,
						allLabels: [],
						balloon: {},
						legend: {
							enabled: true,
							align: "center",
							fontSize: 15,
							//   "labelText": "[[title]] :",
							valueText: "",
							markerLabelGap: 2,
							markerSize: 10,
							markerType: "circle",
							spacing: 10,
							switchType: "v",
							valueAlign: "left",
							verticalGap: 10,
						},
						titles: [
							{
								id: "Title-1",
								size: 15,
								text: "Total User : " + totalActiveUser,
							},
						],
						dataProvider: [
							{
								category: "Not Submitted TP",
								"column-1": totalUserNotSubmittedTP,
							},
							{
								category: "Submitted TP",
								"column-1": totalUserSubmittedTP,
							},
							{
								category: "Approved TP",
								"column-1": totalUserApprovedTP,
							},
							{
								category: "Not Approved",
								"column-1": totalUserNotApprovedTP,
							},
						],
						export: {
							enabled: true,
						},
					});
				},
				(err) => {
					console.log(err);
				}
			);

		this._dcrService
			.getTotalSubmmitedAndPendingDCR(
				this.currentUser.companyId,
				this.currentMonthStartDate,
				this.currentMonthLastDate,
				[true],
				this.currentUser.userInfo[0].designationLevel,
				this.currentUser.userInfo[0].userId
			)
			.subscribe(
				(res) => {
					let totalSubmittedDCR = res["totalSubmittedDCR"];
					let totalPendingDCR = res["totalPendingDCR"];
					this.chart = this.AmCharts.makeChart("dcrStatus", {
						type: "pie",
						theme: "dark",
						balloonText:
							"[[title]] : <br><span style='font-size:14px'><b>  [[value]]</span>",
						labelText: "[[value]]",
						depth3D: 0,
						innerRadius: "50%",
						titleField: "category",
						valueField: "column-1",
						backgroundColor: "#E7D4D4",
						//"colors": ["#FF0F00", "#FF6600", "#FF9E01"],
						fontSize: 13,
						allLabels: [],
						balloon: {},
						legend: {
							enabled: true,
							align: "center",
							fontSize: 15,
							//   "labelText": "[[title]] :",
							valueText: "",
							markerLabelGap: 2,
							markerSize: 10,
							markerType: "circle",
							spacing: 10,
							switchType: "v",
							valueAlign: "left",
							verticalGap: 10,
						},
						titles: [
							{
								id: "Title-1",
								size: 15,
								text: "Total User : " + res["totalUser"],
							},
						],
						dataProvider: [
							{
								category: "Total DCR Submitted",
								"column-1": totalSubmittedDCR,
							},
							{
								category: "Total Pending DCR",
								"column-1": totalPendingDCR,
							},
						],
						export: {
							enabled: true,
						},
					});
				},
				(err) => {
					console.log(err);
				}
			);
		//-----------------Pie Chart End----------------------

		//---------------Month On Month Progress--------------------------

		if (this.actualMonth === 1) {
			this.last6MonthFromDate = this.currentYear - 1 + "-08-01";
			this.toDate = this.currentYear + "-01-31";
		} else if (this.actualMonth === 2) {
			if (
				(0 == this.currentYear % 4 && 0 != this.currentYear % 100) ||
				0 == this.currentYear % 400
			) {
				this.last6MonthFromDate = this.currentYear - 1 + "-09-01";
				this.toDate = this.currentYear + "-02-29";
			} else {
				this.last6MonthFromDate = this.currentYear - 1 + "-09-01";
				this.toDate = this.currentYear + "-02-28";
			}
		} else if (this.actualMonth === 3) {
			this.last6MonthFromDate = this.currentYear - 1 + "-10-01";
			this.toDate = this.currentYear + "-03-31";
		} else if (this.actualMonth === 4) {
			this.last6MonthFromDate = this.currentYear - 1 + "-11-01";
			this.toDate = this.currentYear + "-04-30";
		} else if (this.actualMonth === 5) {
			this.last6MonthFromDate = this.currentYear - 1 + "-12-01";
			this.toDate = this.currentYear + "-05-31";
		} else if (this.actualMonth === 6) {
			this.last6MonthFromDate = this.currentYear + "-01-01";
			this.toDate = this.currentYear + "-06-30";
		} else if (this.actualMonth === 7) {
			this.last6MonthFromDate = this.currentYear + "-02-01";
			this.toDate = this.currentYear + "-07-31";
		} else if (this.actualMonth === 8) {
			this.last6MonthFromDate = this.currentYear + "-03-01";
			this.toDate = this.currentYear + "-08-31";
		} else if (this.actualMonth === 9) {
			this.last6MonthFromDate = this.currentYear + "-04-01";
			this.toDate = this.currentYear + "-09-30";
		} else if (this.actualMonth === 10) {
			this.last6MonthFromDate = this.currentYear + "-05-01";
			this.toDate = this.currentYear + "-10-31";
		} else if (this.actualMonth === 11) {
			this.last6MonthFromDate = this.currentYear + "-06-01";
			this.toDate = this.currentYear + "-11-30";
		} else if (this.actualMonth === 12) {
			this.last6MonthFromDate = this.currentYear + "-07-01";
			this.toDate = this.currentYear + "-12-31";
		}

		let passingMonthOnMonthObj = {
			companyId: this.currentUser.companyId,
			userId: this.currentUser.userInfo[0].userId,
			designationLevel: this.currentUser.userInfo[0].designationLevel,
			fromDate: this.last6MonthFromDate,
			toDate: this.toDate,
			unlistedValidations: this.unlistedValidations,
		};
		this._dcrService
			.getMOnthOnMonthProgress(passingMonthOnMonthObj)
			.subscribe(
				(res) => {
					let dataProvider = [];
					//this.viewTitle = "Total Visited Providers (" + this.currentUser.company.lables.doctorLabel + ", " + this.currentUser.company.lables.vendorLabel + " and " + this.currentUser.company.lables.stockistLabel + ")";
					this.viewTitle =
						"Total Visited " +
						this.currentUser.company.lables.doctorLabel;

					for (let i in res) {
						this.dataSourceForMonthOnMonthTrendTable.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalDocVenStockCall,
						});

						dataProvider.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalDocVenStockCall,
						});

						this.dataProviderForTotalProviderVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalDocVenStockCall,
						});

						this.dataProviderForTotalDoctorVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalDoctorCall,
						});

						this.dataProviderForTotalVendorVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalVendorCall,
						});

						this.dataProviderForTotalStockistVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalStockistCall,
						});

						this.dataProviderForTotalDistinctProviderVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].TotalUniqueDocVenStockCall,
						});

						this.dataProviderForTotalDistinctDoctorVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalUniqueDoctorCall,
						});

						this.dataProviderForTotalDistinctVendorVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalUniqueVendorCall,
						});

						this.dataProviderForTotalDistinctStockistVisited.push({
							type: res[i].monthNameWithYear,
							value: res[i].totalUniqueStockistCall,
						});

						this.dataProviderForOverAllCallAvg.push({
							type: res[i].monthNameWithYear,
							value: res[i].overAllAvg,
						});

						this.dataProviderForDoctorCallAvg.push({
							type: res[i].monthNameWithYear,
							value: res[i].drCallAvg,
						});

						this.dataProviderForVendorCallAvg.push({
							type: res[i].monthNameWithYear,
							value: res[i].venCallAvg,
						});

						this.dataProviderForStockistCallAvg.push({
							type: res[i].monthNameWithYear,
							value: res[i].stockCallAvg,
						});
					}

					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;

					let chart = this.AmCharts.makeChart(
						"monthOnMonthProgressDiv",
						{
							type: "serial",
							theme: "light",
							dataProvider: dataProvider,
							"fontFamily": "Vardana",
							graphs: [
								{
									bulletSize: 14,
									bullet: "round",
									valueField: "value",
									lineColor: "#3C7ABD",
									//"balloonText": "<div style='margin:10px; text-align:left;'><span style='font-size:13px'>[[category]]</span><br><span style='font-size:18px'>Value:[[value]]%</span>",
									balloonText:
										"[[category]] : <br><span style='font-size:14px'><b>  [[value]]</span>",
								},
							],
							categoryAxis: {
								autoWrap: true,
								labelRotation: 0,
							},
							valueAxes: [
								{
									title: "value",
									minimum: 0,
								},
							],
							titles: [
								{
									id: "Title-1",
									size: 20,
									text: this.viewTitle,
								},
							],
							marginTop: 40,
							marginRight: 30,
							marginLeft: 60,
							marginBottom: 40,
							chartCursor: {
								graphBulletSize: 1.5,
								zoomable: false,
								valueZoomable: true,
								cursorAlpha: 0,
								valueLineEnabled: true,
								valueLineBalloonEnabled: true,
								valueLineAlpha: 0.2,
							},
							autoMargins: false,
							categoryField: "type",
							export: {
								enabled: true,
							},
						}
					);
				},
				(err) => {
					console.log(err);
				}
			);

		//------------------------END-------------------------------------

		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;
	}
	callGetActive() {
		let date = _moment(new Date(this.currentDate)).format("YYYY-MM-DD");
		let whereObj = {
			where: {
				companyId: this.currentUser.companyId,
				dcrDate: date,
				workingStatus: { neq: ["Leave", "Holiday"] }
			}
		}



		this._DCRReportService.getActiveUsersList(whereObj).subscribe(res => {
			this.totalActiveCount = res.length;
			!this._changeDetectorRef["destroyed"]
				? this._changeDetectorRef.detectChanges()
				: null;
		})
	};
	callOnLeave() {
		let date = _moment(new Date(this.currentDate)).format("YYYY-MM-DD");
		let onLeaveObj = {
			where: {
				companyId: this.currentUser.companyId,
				dcrDate: date,
				workingStatus: "Leave"
			}
		}

		this._DCRReportService.getActiveUsers(onLeaveObj).subscribe(res => {

			this.onLeave = res.length;
			!this._changeDetectorRef["destroyed"]
				? this._changeDetectorRef.detectChanges()
				: null;
		})

	};


	ngAfterViewInit() {
		this.dataSource1.paginator = this.paginator;
		// Commenting below line since datasource3 is not a MatTable - Tarun - 2-5-2022
		// this.dataSource3.paginator = this.paginator;
		// this.dataSource4.paginator = this.paginator;
	}

	onChangeForMonthOnMonth(event) {
		let localDataProvvider = [];
		if (event.value == "option1") {
			this.viewTitle =
				"Total Visited Providers (" +
				this.currentUser.company.lables.doctorLabel +
				", " +
				this.currentUser.company.lables.vendorLabel +
				" and " +
				this.currentUser.company.lables.stockistLabel +
				")";
			localDataProvvider = this.dataProviderForTotalProviderVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalProviderVisited;
		} else if (event.value == "option2") {
			this.viewTitle =
				"Total Visited " + this.currentUser.company.lables.doctorLabel;
			localDataProvvider = this.dataProviderForTotalDoctorVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalDoctorVisited;
		} else if (event.value == "option3") {
			this.viewTitle =
				"Total Visited " + this.currentUser.company.lables.vendorLabel;
			localDataProvvider = this.dataProviderForTotalVendorVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalVendorVisited;
		} else if (event.value == "option4") {
			this.viewTitle =
				"Total Visited " +
				this.currentUser.company.lables.stockistLabel;
			localDataProvvider = this.dataProviderForTotalStockistVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalStockistVisited;
		} else if (event.value == "option5") {
			this.viewTitle =
				"Distinct Providers Visited (" +
				this.currentUser.company.lables.doctorLabel +
				", " +
				this.currentUser.company.lables.vendorLabel +
				" and " +
				this.currentUser.company.lables.stockistLabel +
				")";
			localDataProvvider = this.dataProviderForTotalDistinctVendorVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalDistinctVendorVisited;
		} else if (event.value == "option6") {
			this.viewTitle =
				"Total Distinct Visited " +
				this.currentUser.company.lables.doctorLabel;
			localDataProvvider = this.dataProviderForTotalDistinctDoctorVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalDistinctDoctorVisited;
		} else if (event.value == "option7") {
			this.viewTitle =
				"Total Distinct Visited " +
				this.currentUser.company.lables.vendorLabel;
			localDataProvvider = this.dataProviderForTotalDistinctVendorVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalDistinctVendorVisited;
		} else if (event.value == "option8") {
			this.viewTitle =
				"Total Distinct Visited " +
				this.currentUser.company.lables.stockistLabel;
			localDataProvvider = this
				.dataProviderForTotalDistinctStockistVisited;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForTotalDistinctStockistVisited;
		} else if (event.value == "option9") {
			this.viewTitle =
				this.currentUser.company.lables.doctorLabel + " Call Average";
			localDataProvvider = this.dataProviderForDoctorCallAvg;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForDoctorCallAvg;
		} else if (event.value == "option10") {
			this.viewTitle =
				this.currentUser.company.lables.vendorLabel + " Call Average";
			localDataProvvider = this.dataProviderForVendorCallAvg;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForVendorCallAvg;
		} else if (event.value == "option11") {
			this.viewTitle =
				this.currentUser.company.lables.stockistLabel + " Call Average";
			localDataProvvider = this.dataProviderForStockistCallAvg;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForStockistCallAvg;
		} else if (event.value == "option12") {
			this.viewTitle =
				"OverAll Call Avg.(" +
				this.currentUser.company.lables.doctorLabel +
				", " +
				this.currentUser.company.lables.vendorLabel +
				" and " +
				this.currentUser.company.lables.stockistLabel +
				")";
			localDataProvvider = this.dataProviderForOverAllCallAvg;
			this.dataSourceForMonthOnMonthTrendTable = this.dataProviderForOverAllCallAvg;
		}
		let chart = this.AmCharts.makeChart("monthOnMonthProgressDiv", {
			type: "serial",
			theme: "light",
			dataProvider: localDataProvvider,
			"fontFamily": "Vardana",
			graphs: [
				{
					bulletSize: 14,
					bullet: "round",
					valueField: "value",
					lineColor: "#3C7ABD",
					//"balloonText": "<div style='margin:10px; text-align:left;'><span style='font-size:13px'>[[category]]</span><br><span style='font-size:18px'>Value:[[value]]%</span>",
					balloonText:
						"[[category]] : <br><span style='font-size:14px'><b>  [[value]]</span>",
				},
			],
			categoryAxis: {
				autoWrap: true,
				labelRotation: 0,
			},
			valueAxes: [
				{
					title: "value",
					minimum: 0,
				},
			],
			titles: [
				{
					id: "Title-1",
					size: 20,
					text: this.viewTitle,
				},
			],
			marginTop: 40,
			marginRight: 30,
			marginLeft: 60,
			marginBottom: 40,
			chartCursor: {
				graphBulletSize: 1.5,
				zoomable: false,
				valueZoomable: true,
				cursorAlpha: 0,
				valueLineEnabled: true,
				valueLineBalloonEnabled: true,
				valueLineAlpha: 0.2,
			},
			autoMargins: false,
			categoryField: "type",

			export: {
				enabled: true,
			},
		});

		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;
	}

	generateTargetGraph(stateId, stateName) {
		this.stateNameForTarget = stateName;
		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;

		let passingObjectForTargetGraphForHq = {
			companyId: this.currentUser.companyId,
			type: "headquarter",
			lookingFor: "all",
			stateId: stateId,
			month: this.actualMonth,
			year: this.currentYear,
		};
		this._primaryAndSaleReturnService
			.getTargetVsAchievementData(passingObjectForTargetGraphForHq)
			.subscribe(
				(res) => {
					let dynamicDataProvider = [];
					let dataSourceForTable = [];
					for (let i = 0; i < res.length; i++) {
						dynamicDataProvider.push({
							category: res[i].districtName,
							"column-1": res[i].target,
							"column-2": res[i].primary,
							"column-3": res[i].secondary,
						});

						dataSourceForTable.push({
							districtName: res[i].districtName,
							districtTarget: res[i].target,
							districtPrimary: res[i].primary,
							districtSecondary: res[i].secondary,
						});
					}

					//-----Code For Target Table-------------
					this.dataSource = dataSourceForTable;
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
					//-----------END-------------------------

					this.chart = this.AmCharts.makeChart(
						"targetAchievementHqGraph",
						{
							type: "serial",
							categoryField: "category",
							"fontFamily": "Vardana",
							startDuration: 1,
							categoryAxis: {
								gridPosition: "start",
								labelRotation: 45,
							},
							chartScrollbar: {
								autoGridCount: true,
								hideResizeGrips: true,
								scrollbarHeight: 20,
								maximum: 10,
								minimum: 5,
							},
							trendLines: [],
							graphs: [
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]",
									bullet: "round",
									id: "AmGraph-1",
									title: "Target",
									valueField: "column-1",
									labelText: "[[column-1]]",
								},
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]",
									bullet: "round",
									id: "AmGraph-2",
									title: "Primary",
									valueField: "column-2",
									labelText: "[[column-2]]",
								},
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]",
									bullet: "round",
									id: "AmGraph-3",
									title: "Secondary",
									valueField: "column-3",
									labelText: "[[column-3]]",
								},
							],
							guides: [],
							valueAxes: [
								{
									id: "ValueAxis-1",
									title: `Sales Amount in ${this.currencySymbol
										? this.currencySymbol
										: "₹"
										}`,
									//"unit": "L"
								},
							],
							allLabels: [],
							balloon: {},
							legend: {
								enabled: true,
								useGraphSettings: true,
								markerSize: 5,
								verticalGap: 10,
							},
							export: {
								enabled: true,
							},
							titles: [
								{
									id: "Title-1",
									size: 20,
									text: "",
								},
							],
							dataProvider: dynamicDataProvider,
							listeners: [
								{
									event: "clickGraphItem",
									method: (event) => {
										//alert("Getting Data...");
									},
								},
							],
						}
					);
				},
				(err) => {
					console.log(err);
				}
			);

		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;
	}

	//--------------------get ERP Data--by Preeti Arora 11-02-2020--------------
	// getStateWiseSale(data) {
	// 	let month = parseInt(moment(new Date(), "YYYY/MM/DD").format("M"));
	// 	let year = parseInt(moment(new Date(), "YYYY/MM/DD").format("YYYY"));

	// 	let lastDay = new Date(year, month, 0).getDate();
	// 	let fromDate = "01" + "-" + month + "-" + year;
	// 	let toDate = lastDay + "-" + month + "-" + year;
	// 	let params = {
	// 		fromDate: fromDate,
	// 		toDate: toDate,
	// 		userId: this.currentUser.userInfo[0].employeeCode,
	// 		stateId: this.currentUser.userInfo[0].ERPStateCode,
	// 		hqId: this.currentUser.userInfo[0].ERPHeadQtrCode,
	// 		companyId: this.currentUser.companyId,
	// 		collection: data,
	// 		APIEndPoint: this.currentUser.company.APIEndPoint,
	// 	};
	// 	this.router.navigate(["reports/ERPStateWiseReport"], {
	// 		queryParams: params,
	// 	});
	// }
	getERPDashboard() {
		this.router.navigate(["/ERPDashBoard"]);
	}
	// getERPData() {
	// 	let data = {
	// 		companyId: this.currentUser.companyId,
	// 		userId: this.currentUser.employeeCode,
	// 		APIEndPoint: this.currentUser.company.APIEndPoint,
	// 	};
	// 	this.ERPServiceService.getPrimaryCount(data)
	// 		.pipe(retryWhen((errors) => errors.pipe(delay(1000), take(0))))
	// 		.subscribe(
	// 			(res) => {
	// 				this.totalPrimCount = res.prm_sale;
	// 			},
	// 			(err) => {
	// 				console.log(err);
	// 			}
	// 		);
	// 	this.ERPServiceService.getCollectionCount(data)
	// 		.pipe(retryWhen((errors) => errors.pipe(delay(1000), take(10))))
	// 		.subscribe(
	// 			(res) => {
	// 				this.totalCollecCount = res.collection;
	// 			},
	// 			(err) => {
	// 				console.log(err);
	// 			}
	// 		);
	// 	this.ERPServiceService.getOutstandingCount(data)
	// 		.pipe(
	// 			retryWhen(errors => errors.pipe(delay(1000), take(10)))
	// 		)
	// 		.subscribe(res => {
	// 			this.totalOutCount = res.outstanding_sales;
	// 		}, err => {
	// 			console.log(err)
	// 		});
	// }
	//------------- Preeti Arora (11-02-2020) End-----------------------

	goToLeaveApproval() {
		if (this.totalLeaveCount > 0)
			this.router.navigate(["reports/leaveApproval"]);
		else {
			(Swal as any).fire({
				title: "No Leave approval requests !!",
				type: "info",
			});
		}
	}

	goToEmployeeCallLogin() {
		this.router.navigate(["reports/EmployeeLoginDetails"]);
	}

	goToEmpLeave() {

		if (this.onLeave > 0)
			this.router.navigate(["reports/employeeLeaveDetails"]);
		else {
			(Swal as any).fire({
				title: "No Leave approval requests !!",
				type: "info",
			});
		}
	}


	goToEmployeeCallAverage() {
		this.router.navigate(["reports/EmploeeWiseCallAverage"]);
	}

	goToSponsorshipApproval() {
		if (this.sponsorshipApprovalCount > 0)
			this.router.navigate(["reports/sponsorshipApproval"]);
		else {
			(Swal as any).fire({
				title: "No Sponsorship approval requests !!",
				type: "info",
			});
		}
	}

	toggleCalenderEvent_1(event) {
		this.Ddata = event
	}


	/*------------------------------------GOURAV start ---------------------------------------------------------------*/
	showHq = false;
	showEmp = false;
	showEmployee = false;
	hq = false;
	showState = false;
	reportesVal = [];
	visible = true;

	trafficLightForm = new FormGroup({
		mainSelection: new FormControl(),
		employeeSelect: new FormControl(),
		districtSelect: new FormControl(),
		stateSelect: new FormControl()
	});

	getSelectedTypeForEmployeePerformance(value) {
		if (value == 'state') {
			this.showState = true;
			this.showHq = false;
			this.showEmp = false;
		} else if (value == 'hq') {
			this.showHq = true;
			this.showState = false;
			this.showEmp = false;
		} else if (value == 'employee') {
			this.showEmp = true;
			this.showHq = false;
			this.showState = false;
		} else if (value == 'all') {
			this.showEmp = false;
			this.showHq = false;
			this.showState = false;
			this.reportesVal = this.dataSourceForTrafficLight;
		}
	}

	generateTrafficLightReport() {
		const form = this.trafficLightForm.value;
		const dispData = this.dataSourceForTrafficLight.filter((value) => {

			if (form.mainSelection == "state") {
				return value.state == form.stateSelect;
			}
			else if (form.mainSelection == "hq") {
				return value.district == form.districtSelect;
			}
			else if (form.mainSelection == "employee") {
				return value.employeeName == form.employeeSelect;
			}
		});

		this.dataSource1 = new MatTableDataSource(dispData);
		this._changeDetectorRef.detectChanges();
		if (form.mainSelection == "all") {
			this.dataSource1 = new MatTableDataSource(tableData);
		}
	}




	/*------------------------------------GOURAV Ending------------------------------------------------------------*/
	/*------------------------------------Mahender Start------------------------------------------------------------*/

	async getUserattendence(value) {
		const d = new Date();
		let month = d.getMonth();
		let year = d.getFullYear();

		let Obj = {
			companyId: this.currentUser.companyId,
			reminder: value,

		}

		this._dcrprovidervisitDetails.getRemenderCalls(Obj).subscribe((res) => {
			if (res.length > 0) {

				this.getRemenderCallsTable = true;
				this.gettprecode = false;
				this.dataSource3 = res;
				console.log("hello", res);
				this._changeDetectorRef.detectChanges();
			} else {
				this.toastr.info("No Record Found.");
				this.getRemenderCallsTable = false;
			}

		})
	}
	onLoad(companyId, designationLevel, loggedUserId) {
		this.providerService.apprAndDelAreaDocVenCount(companyId, this.currentUser.company.validation.approvalUpto, designationLevel, loggedUserId).subscribe(res => {			
			this.dataSources1 = res[0];
			(!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
		}, err => {
			console.log(err)
		});
	}
	getUserTpPending() {
		let Obj = {
			companyId: this.currentUser.companyId,
		}
		console.log("Obj", Obj)
		this._tpService.getUserTpPendingdata(Obj).subscribe((res) => {
			if (res.length > 0) {

				this.gettprecode = true;
				this.getRemenderCallsTable = false;
				this.dataSource4 = res;
				this._changeDetectorRef.detectChanges();
			} else {
				this.toastr.info("No Record Found.");
				this.gettprecode = false;
			}

		})
	}

	lastDatepob() {
		this.router.navigate(["reports/EmploeeWisedayCallAverage"]);
	}
	lastMonthtarget() {
		this.router.navigate(["reports/Targetvsachievement"]);
	}

	TourProgramtarget() {
		this.router.navigate(["master/TourProgramtargetComponent"]);
	}
	ChemistNotification() {
		this.router.navigate(["master/ChemistNotificationComponent"]);
	}
	AreaNotification() {
		this.router.navigate(["master/AreaNotificationComponent"]);
	}
	DoctorNotification() {
		this.router.navigate(["master/DoctorNotificationComponent"]);
	}



	/*------------------------------------Mahender Ending------------------------------------------------------------*/

}
