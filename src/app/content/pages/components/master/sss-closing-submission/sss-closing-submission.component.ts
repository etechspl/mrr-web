import { StockiestService } from './../../../../../core/services/Stockiest/stockiest.service';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { SelectionModel } from '@angular/cdk/collections';
import { MappedStockistComponent } from './../../filters/mapped-stockist/mapped-stockist.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { TypeComponent } from '../../filters/type/type.component';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { UserService } from '../../../../../core/services/user.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'm-sss-closing-submission',
  templateUrl: './sss-closing-submission.component.html',
  styleUrls: ['./sss-closing-submission.component.scss']
})
export class SssClosingSubmissionComponent implements OnInit {
  showState = false;
  showDistrict = false;
  showEmployee = false;
  isShowButton = true;
  isShowSSSClosingForm = false;
  isShowSingleUserSSS = false;
  showMappedStockist = false;
  showHQWiseStockist = false;
  public hideColumn: boolean = true;
  presentDate;
  providerCode;
  userCode;
  triggerData: boolean = true;

  exampleDatabase: StockiestService | null;
  constructor(private _toastr: ToastrService, private fb: FormBuilder,
    private monthYearService: MonthYearService, private snackBar: MatSnackBar,
    private _providers: ProviderService, private _user: UserDetailsService,
    private stockiestService: StockiestService, private _changeDetectorRef: ChangeDetectorRef) { }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    console.log(this.currentUser);

    if (this.currentUser.companyId == "5fbb617728dda403fc650b9b") {
      this.hideColumn = true;
      console.log("Synaptrics", this.hideColumn);
    }
    else {

      this.hideColumn = false;

      console.log("Medens", this.hideColumn);
    }


    if (this.currentUser.userInfo[0].rL === 1) {
      this.sssClosingForm.removeControl('stateId');
      this.sssClosingForm.removeControl('districtId');
      this.sssClosingForm.removeControl('userId');
      this.sssClosingForm.removeControl('divisionId');

      if (this.currentUser.company.validation.getAllStockistHqWise == true) {
        this.getStockistHqWise(this.currentUser.id);
        this.getStateValue(this.currentUser.userInfo[0].stateId);
        this.getDistrictValue(this.currentUser.userInfo[0].districtId);
      } else {
        this.getStockists(this.currentUser.id);
      }

    } else if (this.currentUser.userInfo[0].rL === 0) {
      this.showState = true;
      this.showDistrict = true;
      this.showEmployee = true;
    } else if (this.currentUser.userInfo[0].rL === 2) {
      // this.showEmployee = false;
      // this.sssClosingForm.removeControl('userId');
      // this.sssClosingForm.removeControl('districtId');
      // this.sssClosingForm.removeControl('stateId');
      // this.sssClosingForm.removeControl('divisionId'); 
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }

  }

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMappedStockistComponent") callMappedStockistComponent: MappedStockistComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  check: boolean;
  selection = new SelectionModel(true, []);
  displayedColumns = [];
  displayedColumnsForEdit = [];
  index: number;
  id: number;
  dataSource;

  currentUser = JSON.parse(sessionStorage.currentUser);
  sssClosingForm = new FormGroup({
    stateId: new FormControl(null, Validators.required),
    districtId: new FormControl(null, Validators.required),
    userId: new FormControl(null),
    employeeCode: new FormControl(null),
    stockistId: new FormControl(null, Validators.required),
    month: new FormControl(null, Validators.required),
    year: new FormControl(null, Validators.required),
    divisionId: new FormControl(null),
  });

  myform = new FormGroup({
    productName: new FormControl(Validators.required),
    totalPrimarySubmitUser: new FormControl(null, Validators.required),
    totalPrimarySale: new FormControl(),
    totalRetuenSubmitUser: new FormControl(null, Validators.required),
    totalRetuenSale: new FormControl(null, Validators.required),
    primarySaleData: new FormControl(),

    returnSaleData: new FormControl(),
    breakageQty:new FormControl(),
    expiryQty:new FormControl(),
    batchRecallQty:new FormControl(),
    scheme: new FormControl(),

    closing: new FormControl(null, Validators.required),
    resultList: new FormArray([])
  });


  productList = [];





  createForm(productArr) {

    this.productList = productArr;
    this.myform = this.fb.group({
      productName: [[Validators.required, Validators.minLength(4)]],
      totalPrimarySubmitUser: [],
      totalPrimarySale: [],
      totalRetuenSubmitUser: [],
      totalRetuenSale: [],
      primarySaleData: [],
      returnSaleData: [],
      scheme: [],
      closing: [],
      resultList: new FormArray([])
    });

    const productFGs = this.productList.map(product => {
      return this.fb.group({
        productName: [product.productName],
        productId: [product.productId],
        totalPrimarySubmitUser: [product.totalPrimarySubmitUser],
        totalPrimarySale: [product.totalPrimarySale],
        totalRetuenSubmitUser: [product.totalRetuenSubmitUser],
        totalRetuenSale: [product.totalRetuenSale],
        closing: [product.closing],
        scheme: [product.scheme],
        primarySaleData: [product.primarySaleData],
        returnSaleData: [product.returnSaleData],
      })
    });
    const productFormArray: FormArray = this.fb.array(productFGs);
    this.myform.setControl('resultList', productFormArray);
  }

  getMgrReportType(val) {
    this.callEmployeeComponent.setBlank();
    this.callMappedStockistComponent.setBlank();
    if (val == "Self") {
      if (this.currentUser.company.validation.getAllStockistHqWise == true) {
        this.getStockistHqWise(this.currentUser.id);
        this.getStateValue(this.currentUser.userInfo[0].stateId);
        this.getDistrictValue(this.currentUser.userInfo[0].districtId);
      } else {
        this.getStockists(this.currentUser.id);
      }
      this.showEmployee = false;
      this.sssClosingForm.patchValue({ userId: null });
    } else {
      this.showState = true;
      this.showEmployee = true;
      this.showDistrict = true;
      // if(this.currentUser.company.isDivisionExist){
      // }
    }
  }



  getStateValue(val) {
    this.isShowSSSClosingForm = false;
    this.sssClosingForm.patchValue({ stateId: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);

  }

  getDistrictValue(val) {
    let districtArr = [];
    this.isShowSSSClosingForm = false;
    this.sssClosingForm.patchValue({ districtId: val });
    districtArr.push(val);
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr);
  
  }

  getStockists(val) {
    let userArr = [];
    this.isShowSSSClosingForm = false;
    this.sssClosingForm.patchValue({ userId: val });
    userArr.push(val);
    
   
    //-----getting hq wise stockist and mapped stockist-----------
    this.callMappedStockistComponent.getMappedStockists(this.currentUser.companyId, userArr)

  }
  getStockistHqWise(val) {
    let userArr = [];
    this.isShowSSSClosingForm = false;
    this.sssClosingForm.patchValue({ userId: val });
    userArr.push(val);
    var divisionId = null;
    if (this.currentUser.userInfo[0].rL === 1) {
      this.callMappedStockistComponent.getMappedStockists(this.currentUser.companyId, userArr)
      
    } else {
      var obj = {
        userId: userArr,
        companyId: this.currentUser.companyId,
        districtId: this.currentUser.userInfo[0].districtId,
        divisionId: null
      }
      if (this.currentUser.company.isDivisionExist == true) {
        obj.divisionId = this.currentUser.userInfo[0].divisionId
      } else {
        this.sssClosingForm.removeControl('divisionId');
      }
      this.callMappedStockistComponent.getHQWiseStockist(obj)
    }
  }
  setStockistValue(val) {
    this.isShowSSSClosingForm = false;
    this.sssClosingForm.patchValue({ stockistId: val });
    
  }

  getMonth(val) {
    this.isShowSSSClosingForm = false;
    this.sssClosingForm.patchValue({ month: val })
    
  }

  getYear(val) {
    this.isShowSSSClosingForm = false;
    this.sssClosingForm.patchValue({ year: val })
}

  getEmployee(val):Promise<any>{
    return new Promise((resolve, reject)=>{
      let obj ={
        companyId: this.currentUser.companyId,
          id: this.sssClosingForm.value.userId
      }
      this._user.getUserCode(obj).subscribe(res => {  
          this.userCode = res[0].employeeCode;
          resolve(this.userCode);
        }, err => {
          reject(err);
          //alert("Oooops! \nNo Record Found");
        });
    })
  }
  
  getFirstAndLastDate(month, year) {
    var date = new Date(`${month}-1-${year}`),
        y = date.getFullYear(),
        m = date.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);
    const result = [firstDay, lastDay];
    return result;
}



  
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  //single user sss submit work start
  sinleUserSSS = new FormGroup({
    productName: new FormControl(Validators.required),
    netRate: new FormControl(null, Validators.required),
    splRate: new FormControl(),
    opening: new FormControl(null, Validators.required),
    scheme: new FormControl(null),
    primarySale: new FormControl(null, Validators.required),
    returnSale: new FormControl(null, Validators.required),
    breakageQty: new FormControl(null, Validators.required),
    expiryQty: new FormControl(null, Validators.required),
    batchRecallQty: new FormControl(null, Validators.required),
    resultList: new FormArray([])
  });


  productListForSingle = [];
  singleUserSSSFormMethod(productArr) {
    // const data = Object.keys(productArr).sort().reduce((obj,key)=>{
    //   obj[key]= productArr[key];
    //   return obj;
    // })

    // console.log('Gourav',data);
    /*--------------------------GOURAV starting--------------------------------------------*/
    /*--------------------------Sorting data based on product Name--------------------------------------------*/
    let resultObj = [];
    let sortedData
    let filterData = productArr.map((x)=>{
      return x.productName;
    })
    sortedData = filterData.sort();
    
    
    for(let i=0;i<productArr.length;i+=1){
      productArr.forEach(element => {
        if(sortedData[i] == element.productName){
          //console.log(resultObj);
          resultObj.push(element);
        }
      });
    }

    /*--------------------------GOURAV Ending--------------------------------------------*/
    
    // this.productListForSingle = productArr;
    this.productListForSingle = resultObj;
    console.log('DZ',this.productListForSingle);
    
    //console.log("this.productListForSingle : ",this.productListForSingle);
    this.sinleUserSSS = this.fb.group({
      productName: [[Validators.required]],
      netRate: [],
      splRate: [],
      opening: [],
      primarySale: [],
      breakage: [],
      scheme: [],
      saleable: [],
      other: [],
      returnSale: [],
      breakageQty:[],
      expiryQty:[],
      batchRecallQty:[],
      closing: [],
      resultList: new FormArray([])
    });

    const productFGs = this.productListForSingle.map(product => {

      
      return this.fb.group({
        productName: [product.productName],
        productId: [product.productId],
        netRate: [product.netRate],
        splRate: [product.splRate],
        opening: [product.opening],
        scheme: [product.scheme],

        primarySale: [product.primarySale],
        breakage: [product.breakage],
        saleable: [product.saleable],
        other: [product.other],
      returnSale: [product.returnSale],
      breakageQty:[product.breakageQty],
      expiryQty:[product.expiryQty],
      batchRecallQty:[product.batchRecallQty],
        closing: [product.closing]
      })
    });
    
    const productFormArray: FormArray = this.fb.array(productFGs);
    this.sinleUserSSS.setControl('resultList', productFormArray);
    console.log("product+++++++++",productFormArray);
  }

  valuechange(sssInfo) {
  }

  async getSubmittedSSSClosing() {

      let stateId;
      if (this.showState) {
        stateId = this.sssClosingForm.value.stateId;
      } else {
        stateId = this.currentUser.userInfo[0].stateId;
      }
      this.isToggled = false;
      let divisionIds = "";
      console.log("this.currentUser.company.sssType : ",this.currentUser.company.sssType);
      console.log("this.currentUser.userInfo[0].rL : ",this.currentUser.userInfo[0].rL);
      if (this.currentUser.company.sssType === 1) {

        this.stockiestService.getSubmittedSSSForSSSClosing(this.currentUser.companyId, this.sssClosingForm.value).subscribe(res => {

          if (res.length == 0 || res == undefined) {
            alert("No Record Found");
          } else {
            console.log("getSubmittedSSSForSSSClosing", res);
            this.createForm(res);

            this.isShowSSSClosingForm = true;
          }
          this._changeDetectorRef.detectChanges();
        }, err => {
          console.log(err)
          alert("No Record Found");
        })
      } else if (this.currentUser.company.sssType === 0) {
        if (this.currentUser.company.isDivisionExist == true) {

          divisionIds = this.currentUser.userInfo[0].divisionId[0];

          this.sssClosingForm.patchValue({ divisionId: divisionIds })
        }
        
        if (this.currentUser.userInfo[0].rL === 0) {
          
          if(this.currentUser.company.isERPExist==true){

            let formData={...this.sssClosingForm.value};
            const date=this.getFirstAndLastDate(formData.month,formData.year);
            formData["fromDate"]=date[0];
            formData["toDate"]=date[1];
            const empCode =await this.getEmployee(formData.userId);
            formData["employeeCode"]=empCode;
            formData["APIEndPoint"]=this.currentUser.company.APIEndPoint;
            formData["rL"]=this.currentUser.userInfo[0].rL;
            this.stockiestService.getSingleUserSSSERP(this.currentUser.companyId, formData, stateId).subscribe(res => {
                  console.log("res 11",res);         
              if (res.length == 0 || res == undefined ||res.statusCode == 401) {
                this._toastr.success('Stock and Sales is already submitted for the selected month');
              } else {
               
                this.singleUserSSSFormMethod(res);
                this.isShowSingleUserSSS = true;
              }
              this._changeDetectorRef.detectChanges();
            }, err => {
              console.log(err)
              this._toastr.success('No Record Found');
            }
          )
            
          }else{
          this.stockiestService.getSingleUserSSS(this.currentUser.companyId, this.sssClosingForm.value, stateId).subscribe(res => {

            if (res.length == 0 || res == undefined) {
              this._toastr.success('Stock and Sales is already submitted for the selected month');
            } else {
              console.log("getSingleUserSSS 22", res);
              this.singleUserSSSFormMethod(res);
              this.isShowSingleUserSSS = true;
            }
            this._changeDetectorRef.detectChanges();
          }, err => {
            console.log(err)
            this._toastr.success('No Record Found');
          })
        }

        }else if(this.currentUser.userInfo[0].rL === 1){

          this.stockiestService.getSingleUserSSS(this.currentUser.companyId, this.sssClosingForm.value, stateId).subscribe(res => {
            if (res.length == 0 || res == undefined) {
              this._toastr.success('Stock and Sales is already submitted for the selected month');
            } else {
              console.log("getSingleUserSSS", res);
              this.singleUserSSSFormMethod(res);
              this.isShowSingleUserSSS = true;
            }
            this._changeDetectorRef.detectChanges();
          }, err => {
            console.log(err)
            this._toastr.success('No Record Found');
          })
        

        } else {
          this.stockiestService.getSingleUserSSS(this.currentUser.companyId, this.sssClosingForm.value, stateId).subscribe(res => {

            if (res.length == 0 || res == undefined) {
              this._toastr.success('Stock and Sales is already submitted for the selected month');
            } else {
              this.singleUserSSSFormMethod(res);
              
              this.isShowSingleUserSSS = true;
            }
            this._changeDetectorRef.detectChanges();
          }, err => {
            console.log(err)
            this._toastr.success('No Record Found');
          })
        
      }
    }
      this._changeDetectorRef.detectChanges();
    
  }
  submitClosing() {
    this.stockiestService.submitClosingDetails(this.currentUser, this.sssClosingForm.value, this.myform.value.resultList).subscribe(res => {
      if (res.length == 0 || res == undefined) {
        this._toastr.success('No Record Found');
      } else {
        if (res[0].status === 'Ok') {
          this.isShowSSSClosingForm = false;
          this._toastr.success("Closing Submit Successfully for fill details !", "Closing Submit Successfully !!");
        }
      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      console.log(err)
      this._toastr.error("There is some issue while submitting closing!!");
    })
  }

  submitSingleUserSSS() {
    if (this.currentUser.userInfo[0].rL === 1) {
      let userDetails = {
        stateId: this.currentUser.userInfo[0].stateId,
        districtId: this.currentUser.userInfo[0].districtId,
        userId: this.currentUser.userInfo[0].userId,
        sssSchemeFormula: this.currentUser.company.sssSchemeFormula ? true : false
      }
      this.stockiestService.submitSingleUserSSSDetails(this.currentUser.companyId, this.sssClosingForm.value, this.sinleUserSSS.value.resultList, userDetails).subscribe(res => {

        if (res.length == 0 || res == undefined) {
          alert("No Record Found");
        } else {
          if (res[0].status === 'Ok') {
            this.isShowSingleUserSSS = false;
            this._toastr.success("SSS Submit Successfully for fill details !", "SSS Submit Successfully !!");
          }
          else if (res[0].status === 'Zero') {
            this.isShowSingleUserSSS = false;
            this._toastr.error("Please enter the amount !!");
          }
        }
        this._changeDetectorRef.detectChanges();
      }, err => {
        console.log(err)
        this._toastr.error("error !", "SSS Not Submit Successfully !!");
      })
    } else {
      let userDetailObj;

      if (this.showEmployee) {
        userDetailObj = {
          stateId: this.sssClosingForm.value.stateId,
          districtId: this.sssClosingForm.value.districtId,
          userId: this.sssClosingForm.value.userId,
          sssSchemeFormula: this.currentUser.company.sssSchemeFormula

        }
      } else {
        userDetailObj = {
          stateId: this.currentUser.userInfo[0].stateId,
          districtId: this.currentUser.userInfo[0].districtId,
          userId: this.currentUser.userInfo[0].userId,
          sssSchemeFormula: this.currentUser.company.sssSchemeFormula
        }
      }

      this.stockiestService.submitSingleUserSSSDetails(this.currentUser.companyId, this.sssClosingForm.value, this.sinleUserSSS.value.resultList, userDetailObj).subscribe(res => {

        if (res.length == 0 || res == undefined) {
          alert("No Record Found");
        } else {
          if (res[0].status === 'Ok') {
            this.isShowSingleUserSSS = false;
            this._toastr.success("SSS Submit Successfully for fill details !", "SSS Submit Successfully !!");
          }
          else if (res[0].status === 'Zero') {
            this.isShowSingleUserSSS = false;
            this._toastr.error("Please enter the amount !!");
          }
        }
        this._changeDetectorRef.detectChanges();
      }, err => {
        console.log(err)
        this._toastr.error("error !", "SSS Not Submit Successfully !!");
      })
    }
  }



}
