import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SssClosingSubmissionComponent } from './sss-closing-submission.component';

describe('SssClosingSubmissionComponent', () => {
  let component: SssClosingSubmissionComponent;
  let fixture: ComponentFixture<SssClosingSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SssClosingSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SssClosingSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
