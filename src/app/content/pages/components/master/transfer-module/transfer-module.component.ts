import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { SelectionModel } from '@angular/cdk/collections';
import { AreaService } from '../../../../../core/services/Area/area.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { DataTransferHistoryService } from '../../../../../core/services/data-transfer-history.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { UserDetailsService } from '../../../../../core/services/user-details.service';

@Component({
  selector: 'm-transfer-module',
  templateUrl: './transfer-module.component.html',
  styleUrls: ['./transfer-module.component.scss']
})

export class TransferModuleComponent implements OnInit {
  public showState: boolean = true;

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;


  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  showTransfer = false;
  designationData: any;
  dataSource;
  isShowDivision = false;
  showReport=false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  constructor(  private changeDetectorRef: ChangeDetectorRef,
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private AreaService: AreaService,
    private UserAreaMappingService: UserAreaMappingService,
    private DataTransferHistoryService: DataTransferHistoryService,
    private _userdetails:UserDetailsService,private toastr: ToastrService) { }

    transferData = new FormGroup({
      transferType: new FormControl("withData"),
      designation:new FormControl(null, Validators.required),
      divisionId:new FormControl(null),
      employeeId:new FormControl(null, Validators.required),
      districtInfo:new FormControl(null, Validators.required),
      stateInfo:new FormControl(null)


    })

  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.transferData.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }
  }


  transferType(val) {
    this.transferData.patchValue({ transferType: val });
    if (this.transferData.value.transferType === 'withoutData') {
      this.showReport=true
      this.showState=true
    } else if(this.transferData.value.transferType === 'withData'){
      this.showReport=true
      this.showState=false
    }
  }
  getDesignation(val) {
    this.transferData.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: [this.transferData.value.divisionId],
        status: [true],
        designationObject: [this.transferData.value.designation]
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
    }

  }

  getDivisionValue($event) {
    this.transferData.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: [this.transferData.value.divisionId],
          designationLevel: [this.currentUser.userInfo[0].designationLevel]
        })
      
    }
  }
  getStateValue(val) {
    this.transferData.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {      
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: [this.designationLevel],
          companyId: this.companyId,
          "stateId": this.transferData.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: [this.transferData.value.divisionId]
        })

    } else {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
    }
  }
  getDistrictValue(val) {
    this.transferData.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.transferData.patchValue({ employeeId: val });
    if (this.transferData.value.transferType === 'withoutData') {
      this.showReport=true
      this.showState=true
    } else if(this.transferData.value.transferType === 'withData'){
      this.showReport=true
      this.showState=false
    }

    let fullDetails=this.callEmployeeComponent.getFullEmpDetails(this.transferData.value.employeeId);
    if (this.isDivisionExist === true) {      
      this.callDistrictComponent.getDistrictsBasedOnDivision({
        designationLevel: [this.designationLevel],
        companyId: this.companyId,
        "stateId": fullDetails.stateId,
        isDivisionExist: this.isDivisionExist,
        division: [this.transferData.value.divisionId]
      })

  } else {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, fullDetails.stateId, true);
  }
  }
  Transfer(){
    console.log('this.transferData',this.transferData.value);
    
    let passingObj={
      companyId:this.currentUser.companyId,
      userId:this.transferData.value.employeeId,
      transferStateId:this.transferData.value.stateInfo,
      transferDistrictId:this.transferData.value.districtInfo,
      transferType:this.transferData.value.transferType
    }
    if(this.isDivisionExist== true)
    {
      passingObj["divisionId"]=this.transferData.value.divisionId
    }
    this._userdetails.TransferUser(passingObj).subscribe(res=>{
      if(res.count>0){
        this.toastr.success("You have successfully transfer the User", "User Has been Transfered");
        this.transferData.reset();
      }else{
        this.toastr.error("There is some issue while Transfering");

      }

    })
  }

}
