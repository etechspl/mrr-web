import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferModuleComponent } from './transfer-module.component';

describe('TransferModuleComponent', () => {
  let component: TransferModuleComponent;
  let fixture: ComponentFixture<TransferModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
