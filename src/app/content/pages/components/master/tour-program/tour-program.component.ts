import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { ActivityService } from '../../../../../core/services/Activity/activity.service';
import { AreaService } from '../../../../../core/services/Area/area.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { TourProgramDetailsComponent } from '../tour-program-details/tour-program-details.component';
import { Router } from '@angular/router';


@Component({
  selector: 'm-tour-program',
  templateUrl: './tour-program.component.html',
  styleUrls: ['./tour-program.component.scss']
})
export class TourProgramComponent implements OnInit {


  currentUser = JSON.parse(sessionStorage.currentUser);
  activities = [];
  patches=[];
  jointWorkedWith = [];
  selectionMode: any;
  mrTPSubmitted: boolean;

  doctors = [];
  chemists = []
  datesSelected: any;
  ipAddress;
  invalidDates: any;
  myInput: any;
  constructor(private activityService: ActivityService,
    private changeDetectorRef: ChangeDetectorRef,
    private _areaservice: AreaService,
    private _hierarchyService: HierarchyService,
    private _userAreaMappingService: UserAreaMappingService,
    private _providerservice: ProviderService,
    private toastr: ToastrService,
    private tpService: TourProgramService,
    private _router: Router) { }

  //getting Tour Program Details Component
  @ViewChild("tpDetailsComponent") tpDetailsComponent: TourProgramDetailsComponent;

  viewMonth: any;
  viewYear: any;
  urlPath: any;
  ngOnInit() {
    //based on designation Level Calender selection mode single or multiple
    if (this.currentUser.userInfo[0].designationLevel > 1) {
      this.selectionMode = "single";
    } else {
      this.mrTPSubmitted = false;
      this.selectionMode = "multiple";
    }
    this.urlPath = this._router.url;
    let month = moment.utc().format("M");
    let year = moment.utc().format("YYYY");

    this.viewMonth = month;
    this.viewYear = year;
    //passing parameter to the TourProgramDetails Component
    this.myInput = {
      "companyId": this.currentUser.companyId,
      "month": month,
      "year": year,
      "userId": this.currentUser.id
    }

    //year drop down in calendar
    let yearDropDown = parseInt(year) + 1;
    this.yearRange = "2018:" + yearDropDown;

    //getting activity
    this.activityService.getActivities(this.currentUser.companyId).subscribe(res => {
      for (const activity of res[0].activitiesForWeb) {
        this.activities=[];
        let obj = {
          Key: activity.name + "#-#" + activity.id,
          name: activity.name
        }
        this.activities.push(obj);
      }
      this.changeDetectorRef.detectChanges();
    })

    //on first call tp submitted dates are not disabled so call the method for next  month
    let obj = {
      companyId: this.currentUser.companyId,
      createdBy: this.currentUser.id,
      month: month + 1,
      year: year
    }
    this.getTPSubmittedDate(obj);

    // then call the method for the current month for disabled the submitted dates
    let obj1 = {
      companyId: this.currentUser.companyId,
      createdBy: this.currentUser.id,
      month: month,
      year: year
    }
    this.getTPSubmittedDate(obj1);
  }

  //getting disabled dates for the selected month
  getTPSubmittedDate(obj: object) {
    this.tpService.getTPSubmittedDates(obj).subscribe(res => {
      this.invalidDates = [];
      if(res.length>0){
        if (res[0].dates.length > 0) {
          for (const date of res[0].dates) {
            var dd = moment(date).add('minutes', -330).format('MM/DD/YYYY');
            this.invalidDates.push(new Date(dd));
          }
          this.changeDetectorRef.detectChanges();
        }
      }
      

    },
      err => {
        console.log(err)
      })
  }
  
  getPatches(plan):Promise<any>{
    return new Promise((resolve, reject) => {
      let obj={companyId:this.currentUser.companyId,"type": "lowerWithUpper","isDivisionExist":this.currentUser.company.isDivisionExist ,division:this.currentUser.userInfo[0].divisionId }
      if(plan === "mrPlan"){   
        obj["supervisorId"]=this.model.jointWorkedWith;
        this._userAreaMappingService.getPatchDetailsUserWise(obj).subscribe(res=>{
          console.log("res if: ",res);
          this.patches=[];
          for(const patch of res){
            let obj = {
              Key: patch.patchName + "#-#" + patch.id,
              name: patch.patchName  
            }
            this.patches.push(obj);
          }
          resolve(this.patches) 
        })
      }else if (plan === "individual") {
        obj["supervisorId"]=this.currentUser.userId;
        this._userAreaMappingService.getPatchDetailsUserWise(obj).subscribe(res=>{
          console.log("res else : ",res);
          this.patches=[];
          for(const patch of res){
            let obj = {
              Key: patch.patchName + "#-#" + patch.id,
              name: patch.patchName
            }
            this.patches.push(obj);
          }
          resolve(this.patches)
        })
      }else{
        
        obj["supervisorId"]=this.currentUser.userId;
        this._userAreaMappingService.getPatchDetailsUserWise(obj).subscribe(res=>{
          this.patches=[];
          for(const patch of res){
            let obj = {
              Key: patch.patchName + "#-#" + patch.id,
              name: patch.patchName
            }
            this.patches.push(obj);
          }
          resolve(this.patches)
        })
      }
    })    
  
    
  }
  //calling method on change the month
  monthChange($event) {
    this.viewMonth = $event.month;
    this.viewYear = $event.year;

    this.myInput = {
      "companyId": this.currentUser.companyId,
      "month": this.viewMonth,
      "year": this.viewYear,
      "userId": this.currentUser.id
    }
    //get disabled the submitted dates
    this.getTPSubmittedDate({
      companyId: this.currentUser.companyId,
      createdBy: this.currentUser.id,
      month: parseInt($event.month),
      year: parseInt($event.year)
    });

    this.tpDetailsComponent.getSubmittedTP(this.currentUser.companyId, this.currentUser.id, parseInt($event.month), parseInt($event.year));

  }
  //calling method on change the year
  yearChange($event) {
    this.viewYear = $event.year;
    //get disabled the submitted dates
    this.myInput = {
      "companyId": this.currentUser.companyId,
      "month": this.viewMonth,
      "year": this.viewYear,
      "userId": this.currentUser.id
    }
    this.getTPSubmittedDate({
      companyId: this.currentUser.companyId,
      createdBy: this.currentUser.id,
      month: parseInt($event.month),
      year: parseInt($event.year)
    });
    this.tpDetailsComponent.getSubmittedTP(this.currentUser.companyId, this.currentUser.id, parseInt($event.month), parseInt($event.year));

  }

  //calling method on select the dates 
  onSelectDate($event) {
    if (this.model.hasOwnProperty("choosePlan") === true && this.model["choosePlan"] === "mrPlan" && this.model["jointWorkedWith"] !== undefined) {
      this.getTPExistOfDay();
    }
  }

  //getting the details of the selected date for mrPlan 
  getTPExistOfDay() {
    let date = moment.utc(this.datesSelected);
    let obj = {
      where: {
        createdBy: this.model["jointWorkedWith"],
        companyId: this.currentUser.companyId,
        date: date.local().format("YYYY-MM-DDT00:00:00.000[Z]")
      }
    }
    this.tpService.getTPExistOfDay(obj).subscribe(res => {
      if (JSON.parse(JSON.stringify(res)).length === 0) {
        this.toastr.info("data not available for the selected date", "TP Data not available");
        delete this.model["activity"];
        this.model["area1"] = [];
        this.model["isDayPlan"] = false;
        this.model["plannedDoctors"] = [];
        this.model["plannedChemists"] = [];
        this.model["remarks"] = "";
        this.mrTPSubmitted = false;
        this.changeDetectorRef.detectChanges();
      } else {
        this.mrTPSubmitted = true;
        this.model["activity"] = res[0].activity + "#-#" + res[0].TPStatus;
        this.model["remarks"] = res[0].remarks;
        this.model["isDayPlan"] = res[0].isDayPlan;
        //removing first element which is empty object in area
        this.model.area1.pop();
        if (res[0].area.length > 0) {
          for (const areaObj of res[0].area) {
            let obj = {
              "from": areaObj.fromId + "#-#" + areaObj.from,
              "to": areaObj.toId + "#-#" + areaObj.to
            };
            this.model.area1.push(obj);
          }
          if (res[0].plannedDoctors.length > 0) {
            for (const doctorObj of res[0].plannedDoctors) {
              this.model.plannedDoctors.push(doctorObj.Name + "#-#" + doctorObj.id + "#-#" + doctorObj.areaId);
            }
          }

          if (res[0].plannedChemists.length > 0) {
            for (const chemistObj of res[0].plannedChemists) {
              this.model.plannedChemists.push(chemistObj.Name + "#-#" + chemistObj.id + "#-#" + chemistObj.areaId);
            }
          }
        }
        this.changeDetectorRef.detectChanges();
      }
    }, err => {

    })
  }
  count: number = 0;
  form = new FormGroup({});
  yearRange: string;
  model = {
    area1: [],
    isDayPlan: false,
    jointWorkedWith:[],
    patch:[],
    plannedDoctors: [],
    plannedChemists: []
  };
  // form creation is here...
  fields: FormlyFieldConfig[] = [
    {
      key: 'choosePlan',
      type: 'radio',
      templateOptions: {
        label: 'Choose Plan Type',
        placeholder: 'Placeholder',
        required: true,
        options: [
          { value: "individual", label: 'Make Individual Plan' },
          { value: "mrPlan", label: 'Choose MR Plane' }
        ],
        change: (field, $event) => {
          this.count++;
          if (this.count > 1) {
            this.datesSelected = undefined;
          }
          if ($event.value === "individual") {
            this.mrTPSubmitted = false;
            this.selectionMode = "multiple";
            this.toastr.info("Multiple dates can be selected for making individual plan", "Multiple Date selection");
            //form is restting
            delete this.model["jointWorkedWith"];
            delete this.model["activity"];
            this.model["area1"] = [{}];
            this.model["isDayPlan"] = false;
            this.model["plannedDoctors"] = [];
            this.model["plannedChemists"] = [];
            this.model["remarks"] = "";

          } else {
            if (this.datesSelected === undefined || this.datesSelected === null) {
              this.toastr.error("Select date to copy TP of employee", "Date field required");
              //form is restting
              delete this.model["activity"];
              delete this.model["jointWorkedWith"];
              this.model["area1"] = [{}];
              this.model["isDayPlan"] = false;
              this.model["plannedDoctors"] = [];
              this.model["plannedChemists"] = [];
              this.model["remarks"] = "";

            }
            this.selectionMode = "single";
          }
          this.changeDetectorRef.detectChanges();

        }
      },

      hideExpression: () => {
        if (this.currentUser.userInfo[0].designationLevel > 1) {
          return false;
        } else {
          return true;
        }
      },
    }, {
      key: "jointWorkedWith",
      type: "select",
      templateOptions: {
        required: true,
        placeholder: "Select Joint Workied With",
        options: this.jointWorkedWith,
        valueProp: 'userId',
        labelProp: 'name',
        change: (field, $event) => {
          this.getTPExistOfDay();
          this.getPatches("mrPlan");
        }
      },

      hideExpression: () => {
        if (this.currentUser.userInfo[0].designationLevel > 1) {
          if (this.model["choosePlan"] === "mrPlan") {
            return false;
          } else {
            return true;
          }
        } else {
          return true;
        }
      },
      lifecycle: {
        onInit: (form, field) => {
          let obj = {
            supervisorId: this.currentUser.id,
            companyId: this.currentUser.companyId,
            status: true,
            type: "lowerWithUpper"
          }
          this._hierarchyService.getManagerHierarchy(obj).subscribe(res => {
            this.jointWorkedWith = JSON.parse(JSON.stringify(res));
            field.templateOptions.options = this.jointWorkedWith;

            this.changeDetectorRef.detectChanges();
          })
        }
      }
    }, {
      key: 'activity',
      type: 'select',
      templateOptions: {
        label: 'Activity',
        placeholder: 'Activity',
        options: [],
        valueProp: 'Key',
        labelProp: 'name',
        required: true,
        change: (field, $event) => {
          if (this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {

          } else {
            this.model["area1"] = [{}];
            this.model["isDayPlan"] = false;
            this.model["plannedDoctors"] = [];
            this.model["plannedChemists"] = [];
          }
        }

      },
      hideExpression: () => {
        if (this.currentUser.userInfo[0].designationLevel === 1) {
          return false;
        } else {
          if (this.model["choosePlan"] === "mrPlan") {
            if (this.mrTPSubmitted === true) {
              return false;
            } else {
              return true;
            }
          } else if (this.model["choosePlan"] === "individual") {
            return false;
          } else if (this.model["choosePlan"] === undefined) {
            return true;
          }
        }
      },
      lifecycle: {
        onInit: (form, field) => {
          this.activityService.getActivities(this.currentUser.companyId).subscribe(res => {
            this.activities=[];
            for (const activity of res[0].activitiesForWeb) {

              let obj = {
                Key: activity.name + "#-#" + activity.id,
                name: activity.name
              }
              this.activities.push(obj);
              field.templateOptions.options = this.activities;
            }

          })

        }
      }
    }, 
    //-------- add patches wise details for MCW.......
    {
      key: 'patch',
      type: 'select',
      templateOptions: {
        label: 'Patch',
        placeholder: 'Patch',
        options: [],
        valueProp: 'Key',
        multiple: true,
        labelProp: 'name',
        required: true,
        change: (field, $event) => {
          if (this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
          } else {
            this.model["area1"] = [{}];
            this.model["isDayPlan"] = false;
            this.model["plannedDoctors"] = [];
            this.model["plannedChemists"] = [];
          }
        }
      },  hideExpression: () => {
         if (this.currentUser.company.validation.patchWiseReporting === true && this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
          return false;
         } else{
          return true;
         }
      },
      lifecycle: {
        onInit: async (form, field) => {
          const response = await this.getPatches("").catch(err=>{console.log(err); });
          if(response.length>0){
            field.templateOptions.options = response
          }
         
        }
      }
    },
    {
      className: 'col-sm-7',
      key: 'area1',
      type: 'repeats',
      hideExpression: () => {
        if (this.model["choosePlan"] === "mrPlan") {
          if ((this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.mrTPSubmitted === true && this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
            return false;
          } else {
            return true;
          }
        } else if (this.model["choosePan"] === "individual") {
          if ((this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
            return false;
          } else {
            return true;
          }
        } else {
          if ((this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.model.hasOwnProperty("activity") === true && this.model["activity"] !== undefined) {
            if (parseInt(this.model["activity"].split("#-#")[1]) == 1)
              return false;
            else
              return true;
          } else {
            return true;
          }
        }
      },
      templateOptions: {
        label: this.currentUser.company.lables.areaLabel,
        btnText: 'Add ' + this.currentUser.company.lables.areaLabel
      },
      fieldArray: {
        fieldGroupClassName: 'row',
        className: 'row',
        fieldGroup: [
          {
            className: 'col-sm-5',
            type: 'select',
            key: 'from',
            templateOptions: {
              label: 'From ' + this.currentUser.company.lables.areaLabel,
              options: [],
            },
            lifecycle: {
              onInit: (form, field) => {

                this._userAreaMappingService.getUserMappedBlocksForMobile(this.currentUser.companyId, this.currentUser.id).subscribe(res => {
                  var a = [];
                  //a.push(res)
                  let areaArr = [];
                  var values = [];
                  JSON.parse(JSON.stringify(res)).filter(area => {
                    if (area.status === true && area.appStatus === "approved") {
                      areaArr.push(area);
                    }
                  })
                  for (var n = 0; n < areaArr.length; n++) {
                    values.push({ 'label': areaArr[n].areaName, 'value': areaArr[n].areaId + "#-#" + areaArr[n].areaName });
                  }
                  field.templateOptions.options = values;
                  this.changeDetectorRef.detectChanges();
                }, err => {
                  console.log(err);
                })
              },
            },
          },
          {
            type: 'select',
            key: 'to',
            className: 'col-sm-5',
            templateOptions: {
              label: 'To ' + this.currentUser.company.lables.areaLabel,
              options: [],
            },
            lifecycle: {
              onInit: (form, field) => {
                this._userAreaMappingService.getUserMappedBlocksForMobile(this.currentUser.companyId, this.currentUser.id).subscribe(res => {
                  let areaArr = [];
                  let values = [];
                  JSON.parse(JSON.stringify(res)).filter(area => {
                    if (area.status === true && area.appStatus === "approved") {
                      areaArr.push(area);
                    }
                  })
                  for (var n = 0; n < areaArr.length; n++) {
                    values.push({ 'label': areaArr[n].areaName, 'value': areaArr[n].areaId + "#-#" + areaArr[n].areaName });
                  }
                  field.templateOptions.options = values;
                  this.changeDetectorRef.detectChanges();
                })
              }
            },
          },
        ],
      },
    }, {
      fieldGroupClassName: "row",
      fieldGroup: [{
        key: 'isDayPlan',
        className: 'col-md-4',
        type: 'checkbox',
        templateOptions: {
          label: 'Add Day Plan',
          change: (field, $event) => {
            if ($event.value === true) { } else {
              this.model["plannedDoctors"] = [];
              this.model["plannedChemists"] = [];
            }
          }
        },
        hideExpression: () => {
          //in company master if isDayPlan is false then isDayPlan check box will not display
          if ((this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.currentUser.company.validation.isDayPlan === true && this.model.hasOwnProperty("activity") === true && this.model["activity"] !== undefined) {
            if (parseInt(this.model["activity"].split("#-#")[1]) === 1) {
              if (this.model.area1.length > 0) {
                if (Object.keys(this.model.area1[0]).length != 0) {
                  return false;
                }
              }
            }

          }
          return true;
        },

      }, {
        key: "plannedDoctors",
        type: "select",
        className: 'col-md-4',
        templateOptions: {
          label: 'Select Doctors',
          placeholder: 'Select Doctors',
          options: [],
          valueProp: 'id',
          labelProp: 'providerName',
          required: true,
          multiple: true
        },
        hideExpression: () => {
          if (this.model.area1.length > 0) {
            if ((this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.model["isDayPlan"] === true) {
              return false;
            }
          }
          return true;
        },
        lifecycle: {
          onInit: (form, field) => {
            const updateOptions = (v) => {
              if (this.model.area1.length === 0) {
                this.model["isDayPlan"] = false;
                this.changeDetectorRef.detectChanges();
              }

              if (this.model.isDayPlan === true && this.model.area1.length > 0) {
                if (Object.keys(this.model.area1[0]).length != 0) {
                  var areaIds = [];
                  for (var n = 0; n < this.model.area1.length; n++) {

                    if (this.model.area1[n].hasOwnProperty("from") === true && this.model.area1[n].hasOwnProperty("to") === true) {
                      areaIds.push(this.model.area1[n]["from"].split("#-#")[0]);
                      areaIds.push(this.model.area1[n]["to"].split("#-#")[0]);
                    }
                  }
                  this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["RMP"]).subscribe(res => {
                    for (const provider of res) {
                      let obj = {
                        id: provider.providerName + "#-#" + provider.id + "#-#" + provider.providerInfo.blockId,
                        providerName: provider.providerName
                      }
                      this.doctors.push(obj);
                    }
                    field.templateOptions.options = this.doctors;
                    this.changeDetectorRef.detectChanges();
                  })
                }
              }
            }
            updateOptions(form.value);
            this.form.valueChanges.subscribe(v => updateOptions(v));
          }
        }
      }, {
        key: "plannedChemists",
        type: "select",
        className: 'col-md-4',
        templateOptions: {
          label: 'Select ' + this.currentUser.company.lables.vendorLabel,
          placeholder: 'Select ' + this.currentUser.company.lables.vendorLabel,
          options: [],
          valueProp: 'id',
          labelProp: 'providerName',
          multiple: true
        },
        hideExpression: () => {
          if (this.model.area1.length > 0) {
            if ( (this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.model["isDayPlan"] === true) {
              return false;
            }
          }
          return true;
        },
        lifecycle: {
          onInit: (form, field) => {
            const updateOptions = (v) => {

              if (this.model.isDayPlan === true && this.model.area1.length > 0) {
                if (Object.keys(this.model.area1[0]).length != 0) {
                  var areaIds = [];
                  for (var n = 0; n < this.model.area1.length; n++) {
                    if (this.model.area1[n].hasOwnProperty("from") === true && this.model.area1[n].hasOwnProperty("to") === true) {
                      areaIds.push(this.model.area1[n]["from"].split("#-#")[0]);
                      areaIds.push(this.model.area1[n]["to"].split("#-#")[0]);
                    }
                  }
                  this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["Drug", "Stockist"]).subscribe(res => {
                    for (const provider of res) {
                      let obj = {
                        id: provider.providerName + "#-#" + provider.id + "#-#" + provider.providerInfo.blockId,
                        providerName: provider.providerName
                      }
                      this.chemists.push(obj);
                    }
                    field.templateOptions.options = this.chemists;
                    this.changeDetectorRef.detectChanges();
                  })
                }
              }
            }
            updateOptions(form.value);
            this.form.valueChanges.subscribe(v => updateOptions(v));
          }
        }
      }]
    }, {
      key: 'remarks',
      type: 'textarea',
      templateOptions: {
        label: 'Remarks',
        placeholder: 'Remarks',
        rows: 3
      },
      hideExpression: () => {
        if (this.model["choosePlan"] === "mrPlan") {
          if (this.mrTPSubmitted) {
            return false;
          } else {
            return true;
          }

        } else if (this.model["choosePan"] === "individual") {
          return false;
        } else if (this.model.hasOwnProperty("choosePlan") === false || this.model["choosePlan"] === undefined) {
          return false
        }
      }
    }];

  validate() {
    if (this.datesSelected === undefined || this.datesSelected.length === 0) {
      this.toastr.error("Please Select Date to Submit TP", "Dates Required");
      return true;
    }

    if (parseInt(this.model["activity"].split("#-#")[1]) === 1) {
      for (let i = 0; i < this.model["area1"].length; i++) {
        if ((this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.model["area1"][i].hasOwnProperty("from") === false) {
          this.toastr.error("Please select the from area", "From Area is required");
          return false;
        }
        if ((this.currentUser.company.validation.hasOwnProperty("patchWiseReporting") && this.currentUser.company.validation.patchWiseReporting === false) && this.model["area1"][i].hasOwnProperty("to") === false) {
          this.toastr.error("Please select the to area", "To Area is required");
          return false;
        }
      }
    }
    return true;
  }
  getPatchDetails(data):Promise<any>{
    return new Promise((resolve, reject) => {
      this._providerservice.findAlreadyInsertedPatch(data).subscribe(res=>{
        if(res.length>0){
          resolve(res);
        }else{
          reject([]);
        }

      })
    });  
  }
  getProviderDetails(data):Promise<any>{
    return new Promise((resolve, reject) => {
      this._providerservice.getProviderDetail(data).subscribe(res=>{
        if(res.length>0){
          resolve(res);
        }else{
          reject([]);
        }

      })
    });  
  }

  async submit(model: any) {
    if (this.form.valid && this.validate() === true) {
      
      let defaultDate = "1900-01-01";
      let tpArr = [];
      let tpDates = [];
      if (this.selectionMode === "single") {
        tpDates.push(this.datesSelected)
      } else {
        tpDates = this.datesSelected;
      }

      for (const dateObj of tpDates) {
  
        let areaArr = [];
        let tpInsertObj = {};
        let doctors = [];
        let chemists = [];
        let patchIds=[];
        let providerIds=[];

        if ( (this.currentUser.company.validation.patchWiseReporting == false) && this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
          
          for (const area of model.area1) {
            let obj = {
              from: area.from.split("#-#")[1],
              fromId: area.from.split("#-#")[0],
              to: area.to.split("#-#")[1],
              toId: area.to.split("#-#")[0]
            }
            areaArr.push(obj)
          }

          tpInsertObj["area"] = areaArr;
          tpInsertObj["approvedArea"] = areaArr;
          if (this.model.isDayPlan === true) {
            tpInsertObj["plannedArea"] = areaArr;
          } else {
            tpInsertObj["plannedArea"] = []
          }

          for (const doctorObj of this.model.plannedDoctors) {
            doctors.push({
              "Name": doctorObj.split("#-#")[0],
              "id": doctorObj.split("#-#")[1],
              "areaId": doctorObj.split("#-#")[2]
            })
          }

          for (const chemistObj of this.model.plannedChemists) {
            chemists.push({
              "Name": chemistObj.split("#-#")[0],
              "id": chemistObj.split("#-#")[1],
              "areaId": chemistObj.split("#-#")[2]
            })
          }
        }
        if(this.currentUser.company.validation.patchWiseReporting=== true && this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1){
         
          this.model.patch.forEach(element => {
            patchIds.push(element.split("#-#")[1])
          });
          let obj={
            where:{
              companyId:this.currentUser.companyId,
              _id:{inq:patchIds}
            }
          }
          const response = await this.getPatchDetails(obj).catch(err=>{console.log(err); });
          if(response.length>0){
            response.forEach(element => {
              element.providerId.forEach(ids => {
                providerIds.push(ids)
              });

            });
          }
          let providerObj={
            where:{
              companyId:this.currentUser.companyId,
              _id:{inq:providerIds}
            }
          }
          const providerResponse = await this.getProviderDetails(providerObj).catch(err=>{console.log(err); });
          if(providerResponse.length>0){
            providerResponse.forEach(providerRes => {
              if(providerRes.providerType=="RMP"){
                doctors.push({
              "Name": providerRes.providerName,
              "id":providerRes.id,
              "areaId": providerRes.blockId,
              })   
              }else{
                chemists.push({
                  "Name": providerRes.providerName,
                  "id":providerRes.id,
                  "areaId": providerRes.blockId,
                  }) 
              }
              
            });
          }
        
        }

        tpInsertObj["TPStatus"] = this.model["activity"].split("#-#")[1];
        let date = moment(dateObj).format("YYYY-MM-DD");
        let tpDate = moment.utc(new Date(date)).format("YYYY-MM-DDT00:00:00.000+0000");
        tpInsertObj["date"] = tpDate
        tpInsertObj["companyId"] = this.currentUser.companyId;
        tpInsertObj["month"] = parseInt(moment.utc(date).format('M'));
        tpInsertObj["year"] = parseInt(moment.utc(date).format('YYYY'));
        tpInsertObj["designation"] = this.currentUser.userInfo[0].designation;
        tpInsertObj["designationLevel"] = this.currentUser.userInfo[0].designationLevel;
        tpInsertObj["stateId"] = this.currentUser.userInfo[0].stateId;
        tpInsertObj["districtId"] = this.currentUser.userInfo[0].districtId;
        tpInsertObj["isDayPlan"] = this.model.isDayPlan;
        tpInsertObj["createdBy"] = this.currentUser.id;
        tpInsertObj["sendForApproval"] = false;

        tpInsertObj["activity"] = this.model["activity"].split("#-#")[0];
        tpInsertObj["approvedActivity"] = this.model["activity"].split("#-#")[0];

        tpInsertObj["doctors"] = doctors;
        tpInsertObj["approvedDoctors"] = doctors;

        tpInsertObj["chemists"] = chemists;
        tpInsertObj["approvedChemists"] = chemists;

        if (this.model.isDayPlan === true) {
          tpInsertObj["plannedDoctors"] = doctors;
          tpInsertObj["plannedChemists"] = chemists;
          tpInsertObj["plannedActivity"] = this.model["activity"].split("#-#")[0];
        } else {
          tpInsertObj["plannedDoctors"] = [];
          tpInsertObj["plannedChemists"] = [];
          tpInsertObj["plannedActivity"] = "";

        }
        if(this.currentUser.company.validation.patchWiseReporting=== true){
          tpInsertObj["plannedDoctors"] = doctors;
          tpInsertObj["plannedChemists"] = chemists;
          tpInsertObj["patchId"] = patchIds;
          tpInsertObj["plannedActivity"] = this.model["activity"].split("#-#")[0];
        } else {
          tpInsertObj["plannedDoctors"] = [];
          tpInsertObj["plannedChemists"] = [];
          tpInsertObj["plannedActivity"] = "";

        }
        tpInsertObj["planningSubmissionDate"] = moment.utc().format("YYYY-MM-DDT00:00:00.000+0000");
        tpInsertObj["planningModificationDate"] = moment.utc(defaultDate).format("YYYY-MM-DDT00:00:00.000+0000");

        tpInsertObj["tpSubmissionDate"] = moment.utc(defaultDate).format("YYYY-MM-DDT00:00:00.000+0000");
        tpInsertObj["remarks"] = this.model["remarks"];
        tpInsertObj["approvedRemarks"] = "";
        tpInsertObj["approvedAt"] = moment.utc(defaultDate).format("YYYY-MM-DDT00:00:00.000+0000");
        tpInsertObj["approvalStatus"] = false;
        tpArr.push(tpInsertObj);
      }
      this.tpService.insertTourProgram(tpArr).subscribe(res => {
        this.getTPSubmittedDate({
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          month: this.viewMonth,
          year: this.viewYear
        });
        this.tpDetailsComponent.getSubmittedTP(this.currentUser.companyId, this.currentUser.id, parseInt(this.viewMonth), parseInt(this.viewYear));
        this.model = {
          area1: [{}],
          isDayPlan: false,
          jointWorkedWith:[],
          patch:[],
          plannedDoctors: [],
          plannedChemists: []
        };
        this.datesSelected = undefined;
        this.mrTPSubmitted = false;
        this.toastr.success("TP Created Successfully for the selected Dates");
        this.changeDetectorRef.detectChanges();

      },
        error => {
          this.getTPSubmittedDate({
            companyId: this.currentUser.companyId,
            userId: this.currentUser.id,
            month: this.viewMonth,
            year: this.viewYear
          });
          this.tpDetailsComponent.getSubmittedTP(this.currentUser.companyId, this.currentUser.id, parseInt(this.viewMonth), parseInt(this.viewYear));
          this.datesSelected = undefined;
          this.mrTPSubmitted = false;
          this.model = {
            area1: [{}],
            isDayPlan: false,
            jointWorkedWith:[],
            patch:[],
            plannedDoctors: [],
            plannedChemists: []
          };
          this.toastr.success("TP Created Successfully for the selected Dates");
          this.changeDetectorRef.detectChanges();
        })
    }
  }
}