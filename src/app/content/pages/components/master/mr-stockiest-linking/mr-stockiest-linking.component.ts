import { SelectionModel } from '@angular/cdk/collections';
import { DistrictComponent } from '../../filters/district/district.component';
import { URLService } from '../../../../../core/services/URL/url.service';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { StockiestComponent } from '../../filters/stockiest/stockiest.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit,ViewChild ,ChangeDetectorRef} from '@angular/core';
import {MatSnackBar, MatPaginator, MatTableDataSource} from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import Swal from "sweetalert2";
import { StateComponent } from '../../filters/state/state.component';
@Component({
  selector: 'm-mr-stockiest-linking',
  templateUrl: './mr-stockiest-linking.component.html',
  styleUrls: ['./mr-stockiest-linking.component.scss']
})
export class MrStockiestLinkingComponent implements OnInit {
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callStateComponentAgain") callStateComponentAgain: StateComponent;
  @ViewChild("callDistrictComponentAgain") callDistrictComponentAgain: DistrictComponent;
  @ViewChild("callStockiestComponent") callStockiestComponent: StockiestComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;

  @ViewChild("paginator") paginator: MatPaginator;
  @ViewChild("paginator1") paginator1: MatPaginator;
  // @ViewChild('paginator') 
  // set paginator(value: MatPaginator) {
  //   this.dataSource1.paginator= value;
  // }
  // @ViewChild('paginator1') 
  // set paginator1(value: MatPaginator) {
  //   this.dataSource.paginator = value;
  // }
  
  check: boolean;

  selection = new SelectionModel(true, []);
  displayedColumns = ['select','districtName','name'];
  displayedColumns1 = ['select','districtName','stockiestName'];

  dataSource;
  dataSource1;
  isShowToBeMappedStockist = false;
  isShowMappedStockist= false;

  constructor(private _changeDetectorRef: ChangeDetectorRef,private _toastr: ToastrService,private snackBar: MatSnackBar,private urlService: URLService,private stockiestService:StockiestService,private userDetailService:UserDetailsService) {
  }

  currentUser = JSON.parse(sessionStorage.currentUser);
  linkingForm = new FormGroup({
    stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null),
    stockiestInfo:new FormControl(null),
    radioInfo:new FormControl(null),
  })

  getStateValue(val) {
    this.linkingForm.patchValue({ stateInfo: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId,val,true);
  }
  getStateValueAgain(val) {
    this.linkingForm.patchValue({ stateInfo: val });
    this.callDistrictComponentAgain.getDistricts(this.currentUser.companyId,val,true);
  }
  
  ngOnInit() {
  }

  ngAfterViewInit(){
    this._changeDetectorRef.detectChanges();
  }
  getStockiestToBeMapped(val) {
    this.stockiestService.getStockiestToBeMapped(this.currentUser.companyId, val).subscribe(stockiestToBeMapped => {
        this.dataSource1 = new MatTableDataSource(stockiestToBeMapped);
        setTimeout(() => {
          this.dataSource1.paginator = this.paginator;
        }, 200);
        this.isShowToBeMappedStockist = true;
      }, err => {
        console.log(err);
      })
      this._changeDetectorRef.detectChanges()
  }

  getEmployeeValue(val){
    this.userDetailService.getEmployeeLinkValue(this.currentUser.companyId, val).subscribe(employeeLinkDetail => {
        this.dataSource = new MatTableDataSource(employeeLinkDetail);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator1;
        }, 200);

        this.isShowMappedStockist = true;
      }, err => {
        console.log(err);
      })
      this._changeDetectorRef.detectChanges()
  }
  
  getRadioValue(radioVal){
    this.linkingForm.patchValue({ radioInfo: radioVal });
  }
  
  //mapping stockiest
  mapStockiests(selectedStockiest) {
    this.stockiestService.mapStockiests(this.currentUser.companyId, this.linkingForm.value.stateInfo, selectedStockiest,this.linkingForm.value.radioInfo,this.currentUser.userInfo[0].userId)
      .subscribe(mapStokist => {
        this.isShowToBeMappedStockist = false;
        this.isShowMappedStockist= false;
        // this._toastr.success("Stockiest Mapped Successfully !", "Stockiest Mapped Successfully !!");
        (Swal as any).fire({
          title: `Stockiest Mapped Successfully !`,
          type: "success",
        });
        this.selection.clear();
        this.callStateComponent.setBlank();
        this.callStateComponentAgain.setBlank();
        this.callDistrictComponent.setBlank();
        this.callDistrictComponentAgain.setBlank();
        this.linkingForm.reset();
        this._changeDetectorRef.detectChanges();
      }, err => {
        this.isShowToBeMappedStockist = false;
        this.isShowMappedStockist= false;
        this._toastr.error("Something went wrong !", "Stockiest Mapping Failed !!");
        console.log(err)
        this._changeDetectorRef.detectChanges();
      })
     
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(element => {
        this.selection.select(element)
      });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
