import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MrStockiestLinkingComponent } from './mr-stockiest-linking.component';

describe('MrStockiestLinkingComponent', () => {
  let component: MrStockiestLinkingComponent;
  let fixture: ComponentFixture<MrStockiestLinkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MrStockiestLinkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MrStockiestLinkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
