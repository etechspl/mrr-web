import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaNotificationComponent } from './area-notification.component';

describe('AreaNotificationComponent', () => {
  let component: AreaNotificationComponent;
  let fixture: ComponentFixture<AreaNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
