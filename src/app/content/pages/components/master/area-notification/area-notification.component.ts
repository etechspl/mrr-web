import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { SelectionModel } from '@angular/cdk/collections';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
@Component({
  selector: 'm-area-notification',
  templateUrl: './area-notification.component.html',
  styleUrls: ['./area-notification.component.scss']
})
export class AreaNotificationComponent implements OnInit {
  isShowOnLoadTable = false;
  showProcessing = true;
  showData = false;
  approvalType;
  showRMPData = false;
  showApproveButton = false;
  showDeletetButton = false;
  showRMPList = false;
  employeewiseCount = false;
  displayedColumns = [];
  dataSource;
  dataSource1;
  dataSource2;
  dataSourceForRMP;
  isShowApprove = false;
  isShowDelete = false;
  header;
  displayedColumns1 = [/*'select', */'sn', 'stateName', 'districtName', 'userName', 'count'];
  selection = new SelectionModel(true, []);
  otherTypes = ['Drug', 'Stockist'];

  constructor(
    private toastr: ToastrService,
    private userAreaMappingService: UserAreaMappingService,
    private providerService: ProviderService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.onLoad(this.currentUser.companyId, this.currentUser.userInfo[0].designationLevel,this.currentUser.id);
    if(this.currentUser.company.lables.other){
      this.currentUser.company.lables.otherType.forEach(i=>{
        let a = Object.keys(i);
        this.otherTypes.push(i.key);
      });
    }
  }
  
  currentUser = JSON.parse(sessionStorage.currentUser);
  approvalMaster = new FormGroup({
    approvalType: new FormControl(null, Validators.required),
  });


  //first table data on load
  onLoad(companyId, designationLevel,loggedUserId) {
    this.providerService.apprAndDelAreaDocVenCount(companyId,this.currentUser.company.validation.approvalUpto, designationLevel,loggedUserId).subscribe(res => {
      this.dataSource1 = res;
      console.log("________________________________________________",this.dataSource1)
      this.showProcessing = false;
      this.isShowOnLoadTable = true;
      (!this._changeDetectorRef['destroyed'])?this._changeDetectorRef.detectChanges(): null;
    }, err => {
      console.log(err)
    });
  }
  approveEmployeewiseRequests(requestType) {
    console.log("------------------------>",requestType)
    
    this.employeewiseCount = false;
    this.showData = false;
    this.showRMPData = false;
    this.isShowApprove = true;
    this.isShowDelete = false;
    this.showApproveButton = true;
    this.approvalType = requestType;
    this.header = 'Addition';
    this.showProcessing = true;
    if (requestType === this.currentUser.company.lables.areaLabel) {
      this.gettingEmployeewiseAreaDetail('addition');
    }
    this._changeDetectorRef.detectChanges();
  }



  
  //getting area count empoyeewise
  gettingEmployeewiseAreaDetail(requestType) {
    this.userAreaMappingService.getEmployeewiseAreaCount(this.currentUser.companyId, this.currentUser.company.validation.approvalUpto, this.currentUser.userInfo[0].designationLevel, requestType, this.currentUser.id).subscribe(res => {
      this.showProcessing = false;
      if (res.length == 0 || res == undefined) {
        this.employeewiseCount = false;
        this.toastr.info("No Record Found");
      } else {
        this.dataSource = res;
        this.employeewiseCount = true;
      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      this.toastr.info("No Record Found");
    });
  }


  deleteEmployeewiseRequests(requestType) {
    this.employeewiseCount = false;
    this.showRMPData = false;
    this.showData = false;
    this.isShowApprove = false;
    this.isShowDelete = true;
    this.showDeletetButton = true;
    this.approvalType = requestType;
    this.header = 'Deletion';
    this.showProcessing = true;
    if (requestType === this.currentUser.company.lables.areaLabel) {
      this.gettingEmployeewiseAreaDetail('deletion');
    }
    this._changeDetectorRef.detectChanges();
  }


  
  approveRequests(userId, approvalType) {
    console.log("start -1")
    this.showApproveButton = true;
    this.showDeletetButton = false;
    this.showRMPData = false;
    this.showProcessing = true;
    if (approvalType === this.currentUser.company.lables.areaLabel) {
      console.log("start -1.1");
      this.gettingAreaDetail(this.currentUser, [userId], 'approval');
      this.displayedColumns = ['select', 'stateName', 'districtName', 'userName', 'areaName', 'type', 'status'];
    } 
  }
  
  deleteRequests(userId, approvalType) {
    console.log("start-2");
    this.showDeletetButton = true;
    this.showApproveButton = false;
    this.showRMPData = false;
    this.showProcessing = true;
    if (approvalType === this.currentUser.company.lables.areaLabel) {
      this.gettingAreaDetail(this.currentUser, [userId], 'delete');
      this.displayedColumns = ['select', 'stateName', 'districtName', 'userName', 'areaName', 'type', 'status','deleteReason'];
    } 
  }

  gettingAreaDetail(currentUserValue, userIds, requestType) {
    this.userAreaMappingService.getMappedAreasForApproval(currentUserValue, userIds, requestType).subscribe(res => {
      this.showProcessing = false;
      if (res.length == 0 || res == undefined) {
        this.showData = false;
        this.toastr.info("No Record Found");
      } else {
        this.dataSource2 = res;
        this.showData = true;
      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      this.toastr.info("No Record Found");
    });
  }


   /** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource2.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource2.forEach(element => {
        this.selection.select(element)
      });
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected1() {
    const numSelected1 = this.selection.selected.length;
    const numRows = this.dataSourceForRMP.length;
    return numSelected1 === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle1() {
    this.isAllSelected1() ?
      this.selection.clear() :
      this.dataSourceForRMP.forEach(element => {
        this.selection.select(element)
      });
  }


  approveDeleteAreaAndMapping(obj, requestType) {
    if(requestType == 'reject'){
      this.rejectAreaRequests(obj, requestType);
    }else {
    this.userAreaMappingService.approveDeleteAreaAndMapping(obj, requestType, this.currentUser.company.id, this.currentUser.id, this.currentUser.company.validation.approvalUpto, this.currentUser.userInfo[0].rL)
      .subscribe(areaapproval => {
        
        if (requestType === 'approve') {
          this.toastr.success('Approved Successfully.', '');
          this.approveEmployeewiseRequests(this.approvalType);
          this.onLoad(this.currentUser.companyId, this.currentUser.userInfo[0].designationLevel,this.currentUser.id);
        } else {
          this.toastr.success('Deleted Successfully.', '');
          this.deleteEmployeewiseRequests(this.approvalType);
          this.onLoad(this.currentUser.companyId, this.currentUser.userInfo[0].designationLevel,this.currentUser.id);
        }
        this._changeDetectorRef.detectChanges();
      }
      , err => {

      });
    }
  }


  rejectAreaRequests(obj, requestType){
    obj.forEach(element => {
      element["areaStutas"] = false;
      //element['updatedBy']=this.currentUser.id;
      element['rejectedBy'] = this.currentUser.id;
      element['rejectionAddDate']=new Date();
    });
     console.log('obj',obj);
    let rejectionMsg = '';
    Swal.fire({
      title: 'Rejection Message',
      showCancelButton: true,
      allowOutsideClick: true,
      input: 'textarea',
      inputPlaceholder: 'Type here ... ',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value === "") {
            resolve('Rejection Message is required!')
          } else {
            resolve('')
          }
        })
      }
    }).then(res => {
      rejectionMsg = res.value;
      console.log('rejectionMsg...DZ',obj)
        // ========================================
      if(this.header == 'Addition' && rejectionMsg){
        this.userAreaMappingService.rejectAreaRequest(obj, 'rejectAddRequest', this.currentUser.company.id, this.currentUser.id, this.currentUser.company.validation.approvalUpto, this.currentUser.userInfo[0].rL, rejectionMsg)
      .subscribe(res =>{
        console.log('res',res);
        this.deleteEmployeewiseRequests(this.approvalType);
          this.onLoad(this.currentUser.companyId, this.currentUser.userInfo[0].designationLevel,this.currentUser.id);
      });
      }
    })
  }



  

}
