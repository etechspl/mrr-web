import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderModuleNewComponent } from './provider-module-new.component';

describe('ProviderModuleNewComponent', () => {
  let component: ProviderModuleNewComponent;
  let fixture: ComponentFixture<ProviderModuleNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderModuleNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderModuleNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
