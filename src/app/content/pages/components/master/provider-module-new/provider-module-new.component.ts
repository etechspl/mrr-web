import { Component, OnInit, ViewChild, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StateComponent } from '../../filters/state/state.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { MonthComponent } from '../../filters/month/month.component';
import { UserCompleteDCRInfoComponent } from '../../reports/user-complete-dcrinfo/user-complete-dcrinfo.component';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { state } from '@angular/animations';
import { log } from 'util';
import { District } from '../../../_core/models/district.model';
import { SelectionModel, DataSource } from '@angular/cdk/collections';
import { MatTableDataSource, MatSort, Sort, MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

declare var $;

export interface list {
  value: string;
  viewValue: string;
}



@Component({
  selector: 'm-provider-module-new',
  templateUrl: './provider-module-new.component.html',
  styleUrls: ['./provider-module-new.component.scss']
})
export class ProviderModuleNewComponent implements OnInit {
  
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild('dataTable') table;

  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;

  currentUser = JSON.parse(sessionStorage.currentUser);

  readonly defaultColumns = ['name']

  //displayedColumns: string[] = [...this.defaultColumns];
  displayedColumns: string[] = [];
  dataSource: MatTableDataSource<any>;

  selection = new SelectionModel(true, []);

  showProcessing = false;
  showAdminAndMGRLevelFilter = false;
  check: boolean;
  data = [];
  public showReport: boolean = true;
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showEmployee: boolean = false;
  isShowDivision = false;
  isDivisionExist = this.currentUser.company.isDivisionExist;
  companyId = this.currentUser.companyId;
  designationLevel = this.currentUser.userInfo[0].designationLevel;


  providerFilter = new FormGroup({
  });
  constructor(private fb: FormBuilder,
    private _providerMaster: ProviderService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService
  ) {
    // this.providerFilter = this.fb.group({


    this.providerFilter = this.fb.group({
      reportType: new FormControl(null),
      type: new FormControl(null),
      search: new FormControl(null, Validators.required),
      searchBy: new FormControl(null, Validators.required),
      //division: new FormControl(null),
      designation: new FormControl(null),
      employees: new FormControl(null),
      searchedValue: new FormControl(null)
    });
  }

  providerUpdateForm: FormGroup = this.fb.group({
    formarray: this.fb.array([])
    //dataSource1: this.fb.array([])
  });

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('paginator') paginator: MatPaginator;
  // ngAfterViewInit( change : SimpleChanges){//***********GOURAV**************** */
  //   this.dataSource.sort = this.sort;//--------------Gourav----------------------
  // }

  degree;
  specialization;
  category;
  doa;
  dob;
  address;
  aadhar;
  licenceNumber;
  frequencyVisit;
  ngOnInit() {

    this.showReport = true;
    this.degree = this.currentUser.company.degree;
    this.specialization = this.currentUser.company.specialization;
    this.category = this.currentUser.company.category;
    this.doa = this.currentUser.company.doa;
    this.dob = this.currentUser.company.dob;
    this.address = this.currentUser.company.address;
    this.aadhar = this.currentUser.company.aadhar;
    this.licenceNumber = this.currentUser.company.licenceNumber;
    this.frequencyVisit = this.currentUser.company.frequencyVisit;

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.providerFilter.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };
      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);
    } else {
      this.isShowDivision = false;
    }
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
    //this.showTabel();
  }

 showTabel(){
   let data = [
     {
       "name": "Gourav",
       "mobile": 7838123812,
       "address": "Kadma",
       "Area": "Jamshedpur"
     },
     {
       "name": "Gourav",
       "mobile": 7838123812,
       "address": "Kadma",
       "Area": "Jamshedpur"
     }
     ,
     {
       "name": "Gourav",
       "mobile": 7838123812,
       "address": "Kadma",
       "Area": "Jamshedpur"
     }
     ,
     {
       "name": "Gourav",
       "mobile": 7838123812,
       "address": "Kadma",
       "Area": "Jamshedpur"
     }
   ]

  // this.providerUpdateForm = this.fb.group({
  //    formarray: this.fb.array([]),
  // })
  //this.displayedColumns = ['name', 'mobile', 'address', 'area'];

  // let formarray = this.providerUpdateForm.get('formarray') as FormArray;

  // data.map(x=>{
  //   //console.log(x)
  //   let newObj = formarray.push(this.fb.group({
  //     name: [x.name],
  //     mobile: [x.mobile],
  //     address: [x.address],
  //     area: [x.Area]
  //   }));

  //   return newObj;
  // })
  
  //this.dataSource = new MatTableDataSource(formarray.value);
 }
 test(val){
   console.log(val)
 }

 getIndex(index:number){
   return index + this.paginator.pageSize * this.paginator.pageIndex
 }

  getReportType(val) {
  this.providerFilter.patchValue({reporttype:val});
    if (val === "Geographical") {
      this.providerFilter.reset();
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.providerFilter.reset();
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }

  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue
  }


  getTypeValue(val) {
   
    this.showReport = true;
    if (val == "State") {
      this.providerFilter.reset();
      this.showState = true;
      this.showDistrict = false;
      this.providerFilter.setControl("stateInfo", new FormControl())
      this.providerFilter.removeControl("employeeId");
      this.providerFilter.removeControl("designation");
    } else if (val == "Headquarter") {
      this.providerFilter.reset();
      this.providerFilter.setControl("stateInfo", new FormControl())
      this.providerFilter.setControl("districtInfo", new FormControl())
      this.providerFilter.removeControl("employeeId");
      this.providerFilter.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.providerFilter.reset();
      this.showEmployee = true;
      this.providerFilter.removeControl("stateInfo");
      this.providerFilter.removeControl("districtInfo");
      this.providerFilter.setControl("designation", new FormControl());
      this.providerFilter.setControl("employeeId", new FormControl());
    } else if (val == "Self") {
      this.providerFilter.reset();
      this.showEmployee = false;
    } if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }
    this.providerFilter.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();
  }

  getDivisionValue(val) {
    console.log(val)
    this.providerFilter.patchValue({ divisionId: val });
    if (this.isDivisionExist === true) {
      if (this.providerFilter.value.type === "State" || this.providerFilter.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.providerFilter.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }
      if (this.providerFilter.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.providerFilter.value.divisionId != null && this.providerFilter.value.designation != null) {
          if (this.providerFilter.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.providerFilter.value.divisionId,
              status: [true],
              designationObject: this.providerFilter.value.designation
            })
          }
        }
      }
    }
  }

  getStateValue(val) {
    this.providerFilter.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.providerFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.providerFilter.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.providerFilter.value.divisionId
        })
        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.providerFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }


  getDistrictValue(val) {
    this.providerFilter.patchValue({ districtInfo: val });
  }

  getDesignation(val) {
    this.providerFilter.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.providerFilter.value.divisionId,
        status: [true],
        designationObject: this.providerFilter.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }
  }


  getEmployeeValue(val) {
    this.providerFilter.patchValue({ employees: val });
  }



  searchinglist: list[] = [
    { value: 'aadhar', viewValue: 'Aadhar' },
    { value: 'address', viewValue: 'Address' },
    { value: 'category', viewValue: 'Category' },
    { value: 'Degree', viewValue: 'Degree' },
    { value: 'doa', viewValue: 'DOA' },
    { value: 'dob', viewValue: 'DOB' },
    { value: 'frequencyVisit', viewValue: 'FrequencyVisit' },
    { value: 'licenceNumber', viewValue: 'LicenceNumber' },
    { value: 'specialization', viewValue: 'Specialization' }
  ];

  getSearch(val) {
    console.log(val)
    this.providerFilter.patchValue({ searchinglist: val });
    //this.displayedColumns = [...this.defaultColumns, ...val]
  }



  searchbylist: list[] = [
    { value: 'Category', viewValue: 'Category' },
    { value: 'Degree', viewValue: 'Degree' },
    { value: 'specialization', viewValue: 'Specialization' }
  ]
  getSearchBy(val) {
    if (val == "Category") {
      this.data = this.currentUser.company.category;
    } else if (val == "Degree") {
      this.data = this.currentUser.company.degree;
    } else if (val == "specialization") {
      this.data = this.currentUser.company.specialization;
    }
  }



  viewReport() {
    this.showReport = true;
    this.showProcessing = true;

    let obj = {};
    // if (this.providerFilter.value.reporttype === "Geographical") {


    if (this.providerFilter.value.type == "State") {
      obj = {
        where: {
          companyId: this.currentUser.companyId,
          stateId: {
            inq: this.providerFilter.value.stateInfo,
          },
          //category: {inq:this.data},//this.providerFilter.value.category,
          status: true
        },
        include: [
          {
            "relation": "state",
            "scope": {
              "fields": {
                "stateName": true,
                "id": true
              }
            }
          },
          {
            relation: 'district',
            scope: {
              fields: {
                'districtName': true,
                'id': true
              }
            }
          },
          {
            relation: 'user',
            scope: {
              fields: {
                'name': true,
                'id': true
              }
            }
          },
          {
            relation: 'block',
            scope: {
              fields: {
                'areaName': true,
                'id': true
              }
            }
          }
        ]
      }
    } else if (this.providerFilter.value.type == "Headquarter") {
      obj = {
        where: {
          companyId: this.currentUser.companyId,
          stateId: {
            inq: this.providerFilter.value.stateInfo,
          },
          districtId: {
            inq: this.providerFilter.value.districtInfo,
          },
          //category:{inq:this.data}, //this.providerFilter.value.category,
          status: true
        },
        include: [
          {
            "relation": "state",
            "scope": {
              "fields": {
                "stateName": true,
                "id": true
              }
            }
          },
          {
            relation: 'district',
            scope: {
              fields: {
                'districtName': true,
                'id': true
              }
            }
          },
          {
            relation: 'user',
            scope: {
              fields: {
                'name': true,
                'id': true
              }
            }
          },
          {
            relation: 'block',
            scope: {
              fields: {
                'areaName': true,
                'id': true
              }
            }
          }
        ],
        // field: this.providerFilter.value.searchinglist
      };

    } else if (this.providerFilter.value.type == "Employee Wise") {
      obj = {
        where: {
          companyId: this.currentUser.companyId,
          userId: {
            inq: this.providerFilter.value.employees,
          },
          status: true
        },
        include: [
          {
            "relation": "state",
            "scope": {
              "fields": {
                "stateName": true,
                "id": true
              }
            }
          },
          {
            relation: 'district',
            scope: {
              fields: {
                'districtName': true,
                'id': true
              }
            }
          },
          {
            relation: 'user',
            scope: {
              fields: {
                'name': true,
                'id': true
              }
            }
          },
          {
            relation: 'block',
            scope: {
              fields: {
                'areaName': true,
                'id': true
              }
            }
          }
        ]
        //  field: this.providerFilter.value.searchinglist
      };
    }

    this._providerMaster.getProviderDetail(obj).subscribe((res) => {

      this._changeDetectorRef.detectChanges()
      
      
      if (res.length == 0 || res == undefined) {
        this.displayedColumns = ['name', 'districtId', 'stateId', 'doa', 'dob', 'category', 'frequencyVisit', 'licenceNumber', 'Degree', 'aadhar', 'address', 'specialization'];
        //this.displayedColumns = ['name', 'mobile', 'address', 'area'];
        
        this.showProcessing = false;
        this.dataSource = res;
        this._changeDetectorRef.detectChanges()
        
        
      } else {
        this.showReport = false;
        this.showProcessing = false;
      // this.providerUpdateForm = this.fb.group({
      //   formarray: this.fb.array([])
      // })
      this.displayedColumns = ['name', 'stateId','districtId','area', 'doa', 'dob', 'category', 'frequencyVisit', 'licenceNumber', 'Degree', 'aadhar', 'address', 'specialization'];
       let formarray = this.providerUpdateForm.get('formarray') as FormArray;
      res.map(item => {
        let newObj = formarray.push(this.fb.group({
          id: [item.id],
          name:[item.user.name],
          category: [item.category],
          state:[item.state.stateName],
          headquarters:[item.district.districtName],
          degree: [item.degree],
          specialization: [item.specialization],
          aadhar: [item.aadhar],
          doa: [item.doa],
          dob: [item.dob],
          frequencyVisit: [item.frequencyVisit],
          licenceNumber: [item.licenceNumber],
          address: [item.address],
          area : [item.block.areaName]
        }));
        
        return newObj
       });
      // console.log('final Data ARRAY', array);
      this.dataSource = new MatTableDataSource(formarray.value);
      this._changeDetectorRef.detectChanges();
      //this.dataSource1 = array;
      console.log('final Data ARRAY', this.dataSource);        
      }
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, 200);
    })
    this._changeDetectorRef.detectChanges();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
  }



  updateButton(obj) {

    const finalObject: any = this.providerUpdateForm.value;
    //for (const iterator of obj) {
    //for (let i=0;i<obj.length;i++) {
    for (let i = 0; i < this.providerUpdateForm.value.formarray.length; i++) {
      const where = {
        id: this.providerUpdateForm.value.formarray[i].id
      }
      this._providerMaster.getProviderUpdateDetail(where, this.providerUpdateForm.value.formarray[i]).subscribe((res) => {
      });

    }
    this.showReport = false;
    this.showProcessing = false;

    this.toastr.success('Providers has been updated Successfully !!!');

  }


}

