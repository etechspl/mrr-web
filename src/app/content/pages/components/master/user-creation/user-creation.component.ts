import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { District } from '../../../_core/models/district.model';
import { UserLogin } from '../../../_core/models/userlogin.model';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { DesignationService } from '../../../../../core/services/designation.service';
import { DistrictService } from '../../../../../core/services/district.service';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';
import { StateService } from '../../../../../core/services/state.service';
import { State } from '../../../_core/models/state.model';
import { MatSnackBar } from '@angular/material';
import { uniqueEmailValidator, uniqueUserNameValidator, uniqueMobileValidator, passwordValidator } from '../../validation/custom-validation';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionService } from '../../../../../core/services/Division/division.service';
import { DailyallowanceService } from '../../../_core/services/dailyallowance.service';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'm-user-creation',
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.scss'],
  providers: [

    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

export class UserCreationComponent implements OnInit {

  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  currentUser = JSON.parse(sessionStorage.currentUser);
  aadharPattern = `^[0-9]{12}$`;
  PANcardPattern = `^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$`;
  districtData: District;
  designationData: any;
  reportingManagerData: UserLogin;
  stateData = [];
  divisionData: any;
  CategoryData: any;
  religionData: any = [];
  public userForm: FormGroup;
  imageProgress = 0;
  obj = JSON.parse(sessionStorage.currentUser);
  showDivisionFilter: boolean = false;
  showDACategoryFilter : boolean=false;
  isShowUsersInfo = false;
  allowedUsers;
  queryObject;
  alreadyCreatedUser;

  constructor(
    private dailyAllowanceService: DailyallowanceService,
    private stateService: StateService,
    private toastr: ToastrService,
    public fb: FormBuilder,
    private districtService: DistrictService,
    private designationServie: DesignationService,
    private userDetailsService: UserDetailsService,
    private hierarchyService: HierarchyService,
    public snackBar: MatSnackBar,
    private http: HttpClient,
    private _changeDetectorRef: ChangeDetectorRef,
    private _divisionMasterService: DivisionService,
    private _districtService: DistrictService,
    private _stateService: StateService,
  ) {
    this.createForm(fb);
  }

  ngOnInit() {
    this.queryObject = {
      'where': {
        companyId: this.obj.companyId,
        status: true
      }
    };
    this.getCurrentActiveUsers();
    this.allowedUsers = this.obj.company.maxUser
    if (this.obj.company.isDivisionExist) {
      this.showDivisionFilter = true
    }

    if(this.obj.companyId=="5cd162dd50ce3f0f80f64e14"){

      this.showDACategoryFilter = true
    }

    this.religionData = this.obj.company.religion;

    this._divisionMasterService.getDivisionNew({
      where: {
        companyId: this.obj.companyId,
        status: true
      }
    }).subscribe(divResult => {
      //// console.log(divResult);
      this.divisionData = divResult;
    }, err => {
      // console.log(err);
    });


    this.dailyAllowanceService.getDaCategory({
      where: {
        companyId: this.obj.companyId,
      }
    }).subscribe(divResult => {
   
      this.CategoryData = divResult;

    }, err => {
      // console.log(err);
    });

    this.stateService.getStateDetail({
      where: {
        //All State
        id: { neq: "5900a7082aaaf719a1a8d74c" } //Dummy State
      },
      order: "stateName ASC"
    }).subscribe(res => {
      //// console.log(res);
      this.stateData = res;
    }, err => {
      // console.log(err)
    });

    this.designationServie.getDesignations(this.obj.companyId).subscribe(res => {
      this.designationData = res;
    })

    this.userDetailsService.getAllManager(this.obj.companyId).subscribe(res => {
      this.reportingManagerData = res;
    })
  }
  getCurrentActiveUsers() {
    this.userDetailsService.getUsersList(this.queryObject).subscribe(res => {
      this.alreadyCreatedUser = res.length;
    });
  }

  getStateValue(val) {
    this.isShowUsersInfo = false;
    this.userForm.patchValue({ stateId: val });
    this.userForm.patchValue({ districtId: null });
    this._districtService.getDistrictNew({
      where: {
        stateId: val
        // assignTo:{
        //   $in:[this.currentUser.companyId]
        // }
      },
      order: 'districtName ASC'
    }).subscribe(districtResult => {
      this.districtData = districtResult;
    }, err => {
      // console.log(err);
    });

  }
  getDistrictValue(val) {
    this.userForm.patchValue({ districtId: val })
  }

  createForm(fb: FormBuilder) {
    this.userForm = fb.group({
      stateId: [null, Validators.required],
      districtId: [null, Validators.required],
      divisionId: null,
      daCategory: null,
      name: [null, Validators.required],
      age: null,
      designation: [null, Validators.required],
      dateOfJoining: [null, Validators.required],
      dateOfReporting: [null, Validators.required],
      reportingManager: null,
      lockingPeriod: [null, Validators.required],
      employeeCode: null,
      email: [null,
        Validators.compose([
          Validators.required,
          Validators.email,
        ]),
        uniqueEmailValidator(this.userDetailsService)
      ],
      dateOfBirth: [null],
      dateOfAnniversary: null,
      mobile: [null,
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10)

        ]),
        uniqueMobileValidator(this.userDetailsService)
      ],
      address: [null, Validators.required],
      bank: [null],
      account: [null],
      ifsc: [null],
      username: [null,
        Validators.compose([
          Validators.required
        ]),
        uniqueUserNameValidator(this.userDetailsService)
      ],
      password: [null, Validators.required],
      confirmPassword: [null, passwordValidator],
      PAN: [null, Validators.compose([, Validators.pattern(this.PANcardPattern)])],
      aadhaar: [null, Validators.compose([Validators.pattern(this.aadharPattern)])],
      DLNumber: null,
      gender: 'Male',
      religion: null
    });

    this.userForm.controls.password.valueChanges.subscribe(
      x => this.userForm.controls.confirmPassword.updateValueAndValidity()
    );


    this.userForm.controls.mobile.valueChanges.subscribe(x => {
      setTimeout(res => {
        this._changeDetectorRef.detectChanges();
      }, 300);
    });
    if (this.obj.company.isDivisionExist) {
      this.userForm.get('divisionId').setValidators([Validators.required])
    }
    if(this.currentUser.company.expense.daType === 'employee wise'){
      this.userForm.setControl('daHQ', new FormControl(0, Validators.required))
      this.userForm.setControl('daEX', new FormControl(0, Validators.required))
      this.userForm.setControl('daOUT', new FormControl(0, Validators.required))
      this.userForm.setControl('mobileAllowance', new FormControl(0, Validators.required))
      this.userForm.setControl('netAllowance', new FormControl(0, Validators.required))
      this.userForm.setControl('specialAllowance', new FormControl(0, Validators.required))
    }
  }
  //Gourav
  get stateId() {
    return this.userForm.get('stateId') as FormControl;
  }
  get districtId() {
    return this.userForm.get('districtId') as FormControl;
  }
  get name() {
    return this.userForm.get('name') as FormControl;
  }
  get employeeCode() {
    return this.userForm.get('employeeCode') as FormControl;
  }
  get designation() {
    return this.userForm.get('designation') as FormControl;
  }

  get daCategory() {
    return this.userForm.get('daCategory') as FormControl;
  }

  get dateOfJoining() {
    return this.userForm.get('dateOfJoining') as FormControl;
  }
  get lockingPeriod() {
    return this.userForm.get('lockingPeriod') as FormControl;
  }
  get email() {
    return this.userForm.get('email') as FormControl;
  }
  get mobile() {
    return this.userForm.get('mobile') as FormControl;
  }
  get username() {
    return this.userForm.get('username') as FormControl;
  }
  get password() {
    return this.userForm.get('password') as FormControl;
  }
  get confirmPassword() {
    return this.userForm.get('confirmPassword') as FormControl;
  }
  send() {
    if (this.obj.company.isDivisionExist) {
      let foundNewDivision = false;
      let previousDivisions = [];
      let query = {};
      this.stateData.forEach((i, index) => {
        if (i.id === this.userForm.value.stateId) {
          query['id'] = this.userForm.value.stateId;
          previousDivisions = i.assignedDivision;
          this.userForm.value.divisionId.forEach((j, index_j) => {
            if (!i.assignedDivision.includes(j)) {
              foundNewDivision = true;
              previousDivisions.push(j);
            }
          });
        }

      });
      if (foundNewDivision) {
        this.stateService.assignDivisionIfNotAssigned(query, previousDivisions).subscribe(res => {
          this.stateService.getStateDetail({ where: { id: { neq: "5900a7082aaaf719a1a8d74c" } }, order: "stateName ASC" }).subscribe(res => {
            this.stateData = res;
          }, err => {
            // console.log(err)
          });
        });
      }
    }
    this.userDetailsService.getUsersList(this.queryObject).subscribe(res => {
      this.alreadyCreatedUser = res.length;
      if (this.alreadyCreatedUser < this.obj.company.maxUser) {
        const msg = "User Created Successfully";
        this.userDetailsService.createLogin(this.userForm.value, this.obj).subscribe(userLogins => {
          //PK 2019-12-26
          if (this.userForm.value.divisionId != null) {
            const mainDivisionObject = this.divisionData.find(div => div.isMainDivision == true);
            if (mainDivisionObject) {
              const mainDivisionExist = this.userForm.value.divisionId.includes(mainDivisionObject.id);
              mainDivisionExist == true ? this.userForm.value.isMainDivision = true : this.userForm.value.isMainDivision = false;
            } else {
              this.userForm.value.isMainDivision = false;
            }
          }
          this.userDetailsService.createUserInfo(this.userForm.value, userLogins, this.obj).subscribe(userInfos => {

            // ----BY Nipun (25 jun 2020),  if daType === 'employee wise' then submit da of employee
            if(this.currentUser.company.expense.daType == 'employee wise'){
              let obj = {
                stateId: userInfos.stateId,
                districtId: userInfos.districtId,
                userId: userInfos.userId,
                designation: userInfos.designation,
                type: this.currentUser.company.expense.daType,
                companyId: userInfos.companyId,
                daHQ: this.userForm.value.daHQ,
                daEX: this.userForm.value.daEX,
                daOUT: this.userForm.value.daOUT,
                specialAllowance: this.userForm.value.specialAllowance,
                netAllowance: this.userForm.value.netAllowance,
                mobileAllowance: this.userForm.value.mobileAllowance,
                createdBy: this.currentUser.userInfo[0].userId,

              };
              if (this.currentUser.company.isDivisionExist === true) {
                obj["divisionId"] = this.userForm.value.divisionId
              }
              this.dailyAllowanceService.createDailyAllowance(obj).subscribe(daRes=>{
              });
            }
            // ---------------END-------------
            //Assigning newly created user's state to the company. 
            this._stateService.getStateDetail({
              where: {
                id: this.userForm.value.stateId
              },
              fields: "assignedTo"
            }).subscribe(stateResult => {
              const assignTo = stateResult[0].assignedTo;
              const assignedDivision = stateResult[0].assignedDivision;
              const isAlreadyAssigned = assignTo.includes(this.obj.companyId);

              // const isAlreadyAssignedDivision=assignedDivision.includes(this.userForm.value.divisionId);

              if (!isAlreadyAssigned) {
                stateResult[0].assignedTo.push(this.obj.companyId);
                this._stateService.updateStateDetail({
                  id: this.userForm.value.stateId
                }, {
                  assignedTo: stateResult[0].assignedTo
                }).subscribe(res => {
                });
              }
              // if(this.currentUser.company.isDivisionExist==true){
              //   if (!isAlreadyAssignedDivision) {
              //     stateResult[0].assignedDivision.push(this.userForm.value.divisionId);
              //     this._stateService.updateStateDetail({
              //       id: this.userForm.value.stateId
              //     }, {
              //       assignedDivision: stateResult[0].assignedDivision
              //       }).subscribe(res => {
              //         // console.log(res);
              //       });
              //   }
              // }
            });

            this._districtService.getDistrictNew({
              where: {
                id: this.userForm.value.districtId
              },
              fields: "assignedTo"
            }).subscribe(districtResult => {
              const assignTo = districtResult[0].assignedTo;
              const isAlreadyAssigned = assignTo.includes(this.obj.companyId);
              if (!isAlreadyAssigned) {
                districtResult[0].assignedTo.push(this.obj.companyId);
                this._districtService.updateDistrictDetail({
                  id: this.userForm.value.districtId
                }, {
                  assignedTo: districtResult[0].assignedTo
                }).subscribe(res => {
                });
              }
            });
            //------------------------END-----------------------------
            if (this.userForm.value.reportingManager != undefined) {
              this.hierarchyService.createHierarchy(this.userForm.value, userLogins, userInfos).subscribe(hierarchyResponse => {
                Swal.fire(msg)
              }, err => {
              });
            } else {
              Swal.fire(msg)
            }
            //document.getElementsByName("imageName")[0]['value'] = ""; //For Reseting Image--------
            this.userForm.reset();
            this.userForm.markAsPristine();
            this.userForm.markAsUntouched();
            //this.createForm(this.fb);
            // this.userForm = undefined
          }, err => {
            // console.log("Error in UserInfo")
          })
        }, err => {
          // console.log(err);
        });
      } else {
        this.toastr.error(`Maximum allowed ${this.alreadyCreatedUser} users created`);
      }
    });
    this.getCurrentActiveUsers();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }
  maxDate = new Date();

  date = new FormControl(moment()); //For Date Format "Month Date,

  getvalue(val) {
    this.userForm.patchValue({ stateAccess: val });
  }

  onChange($event) {
    var stateId = $event;
    this.districtService.getDistricts(this.obj.companyId, stateId).subscribe(districts => {
      this.districtData = districts;
    })

  }
  // getReportingManager(stateId, $event) {
  //   // console.log("method is called----");
  //   // on change Designation get reporting manager
  //   this.userDetailsService.getReportingManager(this.obj.companyId, stateId, $event.designationLevel).subscribe(reportingMangers => {
  //     this.reportingManagerData = reportingMangers;
  //   })
  // }

}
