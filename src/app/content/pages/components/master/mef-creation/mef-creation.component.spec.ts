import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MefCreationComponent } from './mef-creation.component';

describe('MefCreationComponent', () => {
  let component: MefCreationComponent;
  let fixture: ComponentFixture<MefCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MefCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MefCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
