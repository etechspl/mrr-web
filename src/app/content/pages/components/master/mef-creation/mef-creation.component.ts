import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MefService } from '../../../../../core/services/Mef/mef.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FromdateComponent } from '../../filters/fromdate/fromdate.component';

@Component({
  selector: 'm-mef-creation',
  templateUrl: './mef-creation.component.html',
  styleUrls: ['./mef-creation.component.scss']
})
export class MefCreationComponent implements OnInit {
  @ViewChild("fromDate") fromDate: FromdateComponent;
  areas = [];
  providers = [];
  focusProducts = [];
  designationData = [];
  quotationStatus = [
    {
      value: "Pending for approval from store",
      name: "Pending for approval from store"
    }, {
      value: "Pending for approval from doctor",
      name: "Pending for approval from doctor",
    }, {
      value: "High/Low Price",
      name: "High/Low Price",
    }, {
      value: "Approval Done",
      name: "Approval Done",
    }, {
      value: "Others",
      name: "Others"
    }
  ];



  isShowSpecified = false;
  isShowNeedFromSr = false;
  isShowInformedHim = false;
  isShowSolutionGiven = false;
  isShowQuotationDate = false;
  isShowQuotationStatus = false;
  isShowConversionDone = false;
  isShowSrGivenSol = false;
  isShowQuotationOther = false;
  employeeFilter = false;
  designationFilter = false;

  constructor(
    public fb: FormBuilder,
    private _toastr: ToastrService,
    private userAreaMapping: UserAreaMappingService,
    private _providerService: ProviderService,
    private _productService: ProductService,
    private userDetailsService: UserDetailsService,
    private _mefService: MefService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { this.createForm(fb); }

  public mefForm: FormGroup;
  createForm(fb: FormBuilder) {
    this.mefForm = fb.group({
      mefDate: new FormControl(null, [Validators.required]),
      jointWork: new FormControl(null),
      area: new FormControl(null, [Validators.required]),
      provider: new FormControl(null, [Validators.required]),
      competitorProduct: new FormControl(null, [Validators.required]),
      focusProduct: new FormControl(null, [Validators.required]),
      DrFacingProduct: new FormControl(null, [Validators.required]),
      solutionOurProduct: new FormControl(null, [Validators.required]),
      drFeedback: new FormControl(null, [Validators.required]),
      solutionToHisQry: new FormControl(null, [Validators.required]),
      ifYesSpecified: new FormControl(null),
      helpNeedFromSr: new FormControl(null),
      haveInformed: new FormControl(null),
      srGivenSolution: new FormControl(null),
      solutionGiven: new FormControl(null),
      quotation: new FormControl(null, [Validators.required]),
      quotationDate: new FormControl(null),
      quotationStatus: new FormControl(null),
      quotationOther: new FormControl(null),
      conversion: new FormControl(null, [Validators.required]),
      notReason: new FormControl(null),
    });
  }

  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  currentUser = JSON.parse(sessionStorage.currentUser);

  ngOnInit() {
    this.getUserMappedAreas(this.currentUser.id);
    if (this.currentUser.userInfo[0].rL == 1) {
      this.getUserMappedAreas(this.currentUser.id);
      this.createForm(this.fb);
    } else if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 4) {
      this.createForm(this.fb);
      this.mefForm.get("jointWork").setValidators([Validators.required]);
      this.employeeFilter = true;
      this.designationFilter = true;
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, this.currentUser.userInfo[0].stateId, this.currentUser.userInfo[0].districtId, 'Employeewise').subscribe(res => {
        this.designationData = res;
      });
    }
  }

  getMEFDate(val) {
    let day = val.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    let month = val.getMonth() + 1;
    if (month < 10) {
      month = "0" + month
    }
    let year = val.getFullYear();

    let dateFormat = year + "-" + month + "-" + day;
    this.mefForm.patchValue({ mefDate: dateFormat })
  }

  format(date: Date, displayFormat: Object): string {
    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return this._to2digit(day) + '-' + this._to2digit(month) + '-' + year;
    } else {
      return date.toDateString();
    }
  }

  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }

  getMgrEmployee(val) {
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, [true]);
  }

  getUserMappedAreas(userId) {
    if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 4) {
      this.mefForm.patchValue({ jointWork: userId })
    }
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, userId).subscribe(res => {
      this.areas = res;
    });
  }

  getProvider(val) {
    this.mefForm.patchValue({ area: val })
    this._providerService.getMEFProviderList(this.currentUser.companyId, [val]).subscribe(res => {
      this.providers = res;
    });
  }

  getFocusProduct(val) {
    this.mefForm.patchValue({ provider: val })
    this._providerService.getFocusProductList(this.currentUser.companyId, val).subscribe(res => {
      this._productService.getFocusProductBasedOnProvider(this.currentUser.companyId, [res[0].focusProduct]).subscribe(res => {
        this.focusProducts = res;
      });
    });
  }

  getCompetitorProduct(val) {
    console.log(val);
    //this.mefForm.patchValue({ competitorProduct: val })
  }

  setFocusProduct(val) {
    this.mefForm.patchValue({ focusProduct: val })
  }

  getSolutionToHisQry(val) {
    this.mefForm.patchValue({ haveInformed: undefined });
    this.mefForm.patchValue({ srGivenSolution: undefined });
    if (val == 'yes') {
      this.isShowSpecified = true;
      this.isShowNeedFromSr = false;
      this.isShowInformedHim = false;
      this.isShowSrGivenSol=false;
      this.isShowSolutionGiven=false;
    } else {
      this.isShowSpecified = false;
      this.isShowNeedFromSr = true;
      this.isShowInformedHim = true;
      this.isShowSrGivenSol=false;
      this.isShowSolutionGiven=false;
    }
    this.mefForm.patchValue({ solutionToHisQry: val })
  }

  getSrGivenSolution(val) {
    if (val == 'yes') {
      this.isShowSolutionGiven = true;
    } else {
      this.isShowSolutionGiven = false;
    }
  }

  getHaveInformed(val) {
    if (val == 'yes') {
      this.isShowSrGivenSol = true;
    } else {
      this.isShowSrGivenSol = false;
      this.isShowSolutionGiven = false;
    }
  }

  getQuotationGiven(val) {
    if (val == 'yes') {
      this.isShowQuotationDate = true;
      this.isShowQuotationStatus = true;
    } else {
      this.isShowQuotationDate = false;
      this.isShowQuotationStatus = false;
    }
    this.mefForm.patchValue({ quotation: val })
  }

  getconversion(val) {
    if (val == 'yes') {
      this.isShowConversionDone = false;
    } else {
      this.isShowConversionDone = true;
    }
    this.mefForm.patchValue({ conversion: val })
  }

  getQuotationStatus(val) {
    this.isShowQuotationOther = false;
    if (val == 'Others') {
      this.isShowQuotationOther = true;
    }
  }

  getQuotationDate(val) {
    this.mefForm.patchValue({ quotationDate: val })
  }

  submitMEFForm() {
    this._mefService.mefCreation(this.mefForm.value, this.currentUser.userInfo[0], this.currentUser.companyId).subscribe(res => {
      this.isShowSpecified = false;
      this.isShowNeedFromSr = false;
      this.isShowInformedHim = false;
      this.isShowSolutionGiven = false;
      this.isShowQuotationDate = false;
      this.isShowQuotationStatus = false;
      this.isShowConversionDone = false;
      this.isShowSrGivenSol = false;
      this.isShowQuotationOther = false;
      this.mefForm.get("jointWork").setValidators([Validators.required]);
      this.mefForm.reset();
      this.createForm(this.fb);
      this._changeDetectorRef.detectChanges();
      this._toastr.success("Form submit successfully for fill details !", "MEF Tracker Form Submit Successfully !!");
    }, err => {
      console.log(err);
    })
  }

}
