import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { StateComponent } from './../../filters/state/state.component';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { StateService } from '../../../../../core/services/state.service';

@Component({
  selector: 'm-provider-creation',
  templateUrl: './provider-creation.component.html',
  styleUrls: ['./provider-creation.component.scss']
})
export class ProviderCreationComponent implements OnInit {

  areas = [];
  lables = [];
  products = [];
  isDoctorCodeStatus = false;
  isDoctorNameStatus = false;
  isAadharStaus = false;
  isPhoneStatus = false;
  isCategoryStatus = false;
  isDegreeStatus = false;
  isSpecializationStatus = false;
  isDoBStatus = false;
  isDoAStatus = false;
  isFrequencyVisitStatus = false;
  isBuisnessPotentialStatus = false;
  isFocusStatus = false;
  isAddress = false;
  isCityStatus = false;
  isEmailStatus = false;
  islicenceNumberStatus = false;

  //---for required marker---by preeti arora--------
  isDoctorCodeStatusMarker = false;
  isDoctorNameStatusMarker = false;
  isAadharStausMarker = false;
  isPhoneStatusMarker = false;
  isCategoryStatusMarker = false;
  isDegreeStatusMarker = false;
  isSpecializationStatusMarker = false;
  isDoBStatusMarker = false;
  isDoAStatusMarker = false;
  isFrequencyVisitStatusMarker = false;
  isBuisnessPotentialStatusMarker = false;
  isFocusStatusMarker = false;
  isAddressMarker = false;
  isCityStatusMarker = false;
  isEmailStatusMarker = false;
  islicenceNumberStatusMarker = false;
  //---------------------------

  category = [];
  degree = [];
  stateId;

  specialization = [];
  type = [];
  formId = "";
  showDoctorForm = false;
  showVendorForm = false;
  showFamilyInfo = false;
  showOtherInfo = false;
  showOtherInfoVendor = false;
  showDaysTimeInfo = false;
  index: number = 0;
  index1: number = 0;
  index2: number = 0;
  otherCheckbox: boolean = false;
  childForm: FormGroup;
  timeForm: FormGroup;
  daysFormForDocTime: FormGroup;

  setTime = [{
    id: 'Mon',
    value: "Monday"
  }, {
    id: 'Tue',
    value: "Tuesday"
  }, {
    id: 'Wed',
    value: "Wednesday"
  }, {
    id: 'Thu',
    value: "Thusday"
  }, {
    id: 'Fri',
    value: "Friday"
  }, {
    id: 'Sat',
    value: "Saturday"
  }, {
    id: 'Sun',
    value: "Sunday"
  }]
  formType = "doctor";
  constructor(
    public fb: FormBuilder,
    private _toastr: ToastrService,
    private userAreaMapping: UserAreaMappingService,
    private _providerService: ProviderService,
    private _productService: ProductService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _stateService : StateService
  ) { 
    this.createForm(fb); 

  }

  providerForm: FormGroup;
  get data() {
    return this.providerForm.get('data') as FormArray;
  }
  get data1() {
    return this.providerForm.get('data1') as FormArray;
  }

  // get dayTimeInfo(){
  //   return this.providerForm.get('data1').get('daysTimeInfo') as FormArray;
  // }

  get categoryControl() {
    return this.providerForm.get('category') as FormArray;
  }
  get aadhar() {
    return this.providerForm.get('aadhar') as FormControl;
  }

  createForm(fb: FormBuilder) {
    this.providerForm = fb.group({
      reportTypeName: new FormControl('doctor'),
      area: new FormControl('', [Validators.required]),
      doctorCode: new FormControl(''),
      doctorName: new FormControl(''),
      aadhar: new FormControl(0),
      category: new FormControl(''),
      degree: new FormControl(''),
      specialization: new FormControl(''),
      familyInfo: new FormControl(''),
      spouseName: new FormControl(''),
      spouseBirthDate: new FormControl(''),
      spouseProfession: new FormControl(''),
      otherInfo: new FormControl(false),
      birthDateOtherInfo: new FormControl(''),
      annyDate: new FormControl(''),
      frequencyVisit: new FormControl(0),
      contactPerson: new FormControl(''),
      businessPotential: new FormControl(0),
      address: new FormControl(''),
      city: new FormControl(''),
      venAddress: new FormControl(''),
      venCity: new FormControl(''),
      phoneNum: new FormControl(0),
      email: new FormControl(''),
      focusProduct: new FormControl(''),
      type: new FormControl(''),
      ownerName: new FormControl(''),
      ownerDob: new FormControl(''),
      ownerDoa: new FormControl(''),
      shopDoa: new FormControl(''),
      pancard: new FormControl(0),
      gst: new FormControl(''),
      licence: new FormControl(''),
      stateId: new FormControl(''),
      districtId: new FormControl(''),
      userId: new FormControl(''),
      data: this.fb.array([this.fb.group({
        childName: new FormControl(""),
        age: new FormControl(""),
      })]),
      'data1': fb.array([])
    });
  }
  currentUser = JSON.parse(sessionStorage.currentUser);
  stateDate : any;
  async ngOnInit() {
    this.setBasicDetails();
    let response = await this.getStateForVendor();
    this.stateDate = response[0].stateInfo;
    console.log(this.stateDate);
    this._changeDetectorRef.detectChanges()
  }
  getStateForVendor() : Promise<any> {
    return new Promise((resolve,reject)=>{
      this._stateService.getStates(this.currentUser.companyId).subscribe(res=>{
        if (!Boolean(res)) reject();
        else(resolve(res))
      })      
    })
  }
  setBasicDetails(){
    this.reportType('doctor');
    this.formId = "5b3c8e442aaaf712852c1f16";
    this.category = this.currentUser.company.category.sort((a, b) => a.localeCompare(b));
    this.degree = this.currentUser.company.degree.sort((a, b) => a.localeCompare(b));
    this.specialization = this.currentUser.company.specialization.sort((a, b) => a.localeCompare(b));
    this.type = this.currentUser.company.type;
    if (this.currentUser.userInfo[0].rL == 1) {
      this.getUserMappedAreas(this.currentUser.id);
      this.createForm(this.fb);
    } else if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 6) {
      this.getUserMappedAreas(this.currentUser.id);
      this.createForm(this.fb);
    }
    this.getCompanyWiseLables(this.currentUser.company.id);
    this._changeDetectorRef.detectChanges();
  }
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callStateComponent1") callStateComponent1: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDistrictComponent1") callDistrictComponent1: DistrictComponent;
  @ViewChild("callEmployeeComponent1") callEmployeeComponent1: EmployeeComponent;
  
  
  getStateValue(val) {
    if (this.currentUser.userInfo[0].rL == 0) {
      this.stateId = this.providerForm.value.stateId
    } else {
      this.stateId = this.currentUser.userInfo[0].stateId;
    }
    this.providerForm.patchValue({ stateId: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
  }
  getStateValueForVen(val) {
    console.log('chemest',val)
    this.providerForm.patchValue({ stateId: val });
    this.callDistrictComponent1.getDistricts(this.currentUser.companyId, val, true);
  }
  getDistrictValue(val) {
    this.providerForm.patchValue({ districtId: val });
    let districtArr = [];
    districtArr.push(val);
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr)
  }

  getDistrictValueForVen(val) {
    this.providerForm.patchValue({ districtId: val });
    let districtArr = [];
    districtArr.push(val);
    this.callEmployeeComponent1.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr)
  }

  getEmployees(val) {
    this.getUserMappedAreas(val)
  }

  addMoreChild() {
    this.index = this.index + 1;
    this.data.push(this.fb.group({
      childName: new FormControl(""),
      age: new FormControl(""),
    }));
    Object.keys(this.providerForm.controls["data"]["controls"][this.index].controls).forEach((name) => {
      let con = this.providerForm.controls["data"]["controls"][this.index].controls[name];
      con.setErrors(null);
    })
    this._changeDetectorRef.detectChanges();
  }

  deleteChildInfo(index: number) {
    if (this.data.length > 1) {
      this.data.removeAt(index);
    }
    this.index = this.index - 1;
    this._changeDetectorRef.detectChanges();
  }

  callVaildation() {
    this.providerForm.controls.doctorName.setValidators([]);
    this.providerForm.controls.doctorName.updateValueAndValidity();

    this.providerForm.patchValue({ aadhar: 0 });
    this.providerForm.controls.aadhar.setValidators([]);
    this.providerForm.controls.aadhar.updateValueAndValidity();

    this.providerForm.controls.birthDateOtherInfo.setValidators([]);
    this.providerForm.controls.birthDateOtherInfo.updateValueAndValidity();

    this.providerForm.controls.annyDate.setValidators([]);
    this.providerForm.controls.annyDate.updateValueAndValidity();

    this.providerForm.controls.category.setValidators([]);
    this.providerForm.controls.category.updateValueAndValidity();

    this.providerForm.controls.degree.setValidators([]);
    this.providerForm.controls.degree.updateValueAndValidity();

    this.providerForm.controls.specialization.setValidators([]);
    this.providerForm.controls.specialization.updateValueAndValidity();

    this.providerForm.patchValue({ frequencyVisit: 0 });
    this.providerForm.controls.frequencyVisit.setValidators([]);
    this.providerForm.controls.frequencyVisit.updateValueAndValidity();

    this.providerForm.controls.address.setValidators([]);
    this.providerForm.controls.address.updateValueAndValidity();

    this.providerForm.controls.city.setValidators([]);
    this.providerForm.controls.city.updateValueAndValidity();

    this.providerForm.controls.email.setValidators([]);
    this.providerForm.controls.email.updateValueAndValidity();

    this.providerForm.controls.licence.setValidators([]);
    this.providerForm.controls.licence.updateValueAndValidity();

    this.providerForm.controls.focusProduct.setValidators([]);
    this.providerForm.controls.focusProduct.updateValueAndValidity();

    this.providerForm.patchValue({ businessPotential: 0 });
    this.providerForm.controls.businessPotential.setValidators([]);
    this.providerForm.controls.businessPotential.updateValueAndValidity();

    this.providerForm.controls.phoneNum.setValidators([]);
    this.providerForm.patchValue({ phoneNum: 0 });
    this.providerForm.controls.phoneNum.updateValueAndValidity();

    this.getCompanyWiseLables(this.currentUser.company.id);

  }

  // Radio button method..
  reportType(val) {
    this.showDoctorForm = false;
    this.showVendorForm = false;
    this.formType = val;
    
    if (val == 'doctor') {
      this.showDoctorForm = true;
      this.showVendorForm = false;
      this.formId = "5b3c8e442aaaf712852c1f16";
      this.callVaildation();
    } else if (val == 'vendor') {
      this.showDoctorForm = false;
      this.showVendorForm = true;
      //this.providerForm.reset();
      this.formId = "5b3c8e642aaaf712852c1f17";
      this.callVaildation();
    }
    this.providerForm.patchValue({ reportTypeName: val });
  }

  //added by preeti arora--getting labels og current company to make the fields dynamic--
  getCompanyWiseLables(companyId) {
    this._productService.getCompanyLables(companyId).subscribe(lablesRes => {
      this.lables = lablesRes;
      this.lables.forEach(element => {
        if (element.key == "providerCode" && element.labelStatus == true && element.formId == this.formId) {
          this.isDoctorCodeStatus = true;
          if (element.mandatoryStatus == true) {
            this.isDoctorCodeStatusMarker = true;
            this.providerForm.setControl("doctorCode", new FormControl(null, Validators.required))
          }
        }
        if (element.key == "providerName" && element.labelStatus == true && element.formId == this.formId) {
          this.isDoctorNameStatus = true;
          if (element.mandatoryStatus == true) {
            this.isDoctorNameStatusMarker = true;
            this.providerForm.setControl("doctorName", new FormControl(null, Validators.required))
          }

        } if (element.key == "aadhar" && element.labelStatus == true && element.formId == this.formId) {
          this.isAadharStaus = true
          if (element.mandatoryStatus == true) {
            this.isAadharStausMarker = true
            this.providerForm.setControl("aadhar", new FormControl(null, Validators.required))
          }
        } if (element.key == "dob" && element.labelStatus == true && element.formId == this.formId) {
          this.isDoBStatus = true
          if (element.mandatoryStatus == true) {
            this.providerForm.setControl("birthDateOtherInfo", new FormControl(null, Validators.required))
          }

        } if (element.key == "doa" && element.labelStatus == true && element.formId == this.formId) {
          this.isDoAStatus = true;
          if (element.mandatoryStatus == true) {
            this.isDoAStatusMarker = true;
            this.providerForm.setControl("annyDate", new FormControl(null, Validators.required))
          }
        } if (element.key == "category" && element.labelStatus == true && element.formId == this.formId) {
          this.isCategoryStatus = true;
          if (element.mandatoryStatus == true) {
            this.isCategoryStatusMarker = true;

            this.providerForm.setControl("category", new FormControl(null, Validators.required))
          }
        } if (element.key == "degree" && element.labelStatus == true && element.formId == this.formId) {
          this.isDegreeStatus = true;
          if (element.mandatoryStatus == true) {
            this.isDegreeStatusMarker = true;
            this.providerForm.setControl("degree", new FormControl(null, Validators.required))
          }

        } if (element.key == "specialization" && element.labelStatus == true && element.formId == this.formId) {
          this.isSpecializationStatus = true;
          if (element.mandatoryStatus == true) {
            this.isSpecializationStatusMarker = true;
            this.providerForm.setControl("specialization", new FormControl(null, Validators.required))
          }
        }
        if (element.key == "frequencyVisit" && element.labelStatus == true && element.formId == this.formId) {
          this.isFrequencyVisitStatus = true;
          if (element.mandatoryStatus == true) {
            this.isFrequencyVisitStatusMarker = true;
            this.providerForm.setControl("frequencyVisit", new FormControl(null, Validators.required))
          }
        }
        if (element.key == "address" && element.labelStatus == true && element.formId == this.formId) {
          this.isAddress = true;
          if (element.mandatoryStatus == true) {
            this.isAddressMarker = true;
            // this.providerForm.setControl("address", new FormControl(null, Validators.required))
          }
        }
        if (element.key == "phone" && element.labelStatus == true && element.formId == this.formId) {
          this.isPhoneStatus = true;
          if (element.mandatoryStatus == true) {
            this.isPhoneStatusMarker = true;
            this.providerForm.setControl("phoneNum", new FormControl(null, Validators.required))
          }

        } if (element.key == "city" && element.labelStatus == true && element.formId == this.formId) {
          this.isCityStatus = true;
          if (element.mandatoryStatus == true) {
            this.isCityStatusMarker = true;
            //this.providerForm.setControl("city", new FormControl(null, Validators.required))
          }

        }
        if (element.key == "email" && element.labelStatus == true && element.formId == this.formId) {
          this.isEmailStatus = true;
          if (element.mandatoryStatus == true) {
            this.providerForm.setControl("email", new FormControl(null, Validators.required))
            this.otherCheckbox = true;
            this.isEmailStatusMarker = true;
            this.getAdditionalInfo('other', true);
          }
        }
        if (element.key == "licenceNumber" && element.labelStatus == true && element.formId == this.formId) {
          this.islicenceNumberStatus = true;
          if (element.mandatoryStatus == true) {
            this.islicenceNumberStatusMarker = true;
            this.providerForm.setControl("licence", new FormControl(null, Validators.required))
          }
        } if (element.key == "focusProduct" && element.labelStatus == true && element.formId == this.formId) {
          this.isFocusStatus = true;
          if (element.mandatoryStatus == true) {
            this.isFocusStatusMarker = true;
            this.providerForm.setControl("focusProduct", new FormControl(null, Validators.required))
            this.otherCheckbox = true;
            this.getAdditionalInfo('other', true);
          }

        } if (element.key == "businessPotential" && element.labelStatus == true && element.formId == this.formId) {
          this.isBuisnessPotentialStatus = true;
          if (element.mandatoryStatus == true) {

            this.isBuisnessPotentialStatusMarker = true;
            this.providerForm.setControl("businessPotential", new FormControl(null, Validators.required))
          }
        }
      });

    });

  }

  //----------------------End---------------------------------------

  addMoreTime(index: number) {

    // this.data1.push(this.fb.group({
    //   address: new FormControl(""),
    //   city: new FormControl(""),
    //   daysInfo: new FormControl(null)
    // }));

    let clinicalFormGroup = this.fb.group({
      address: new FormControl(""),
      city: new FormControl(""),
      daysInfo: new FormControl(""),
      phones: this.fb.array([])
    });
    this.data1.push(clinicalFormGroup);

    let dataIndex = this.data1.length - 1;
    this.addMoreDayTime(dataIndex);


    // Object.keys(this.providerForm.controls["data1"]["controls"][index].controls).forEach((name) => {
    //   let con = this.providerForm.controls["data1"]["controls"][index].controls[name];
    //   con.setErrors(null);
    // })
    this._changeDetectorRef.detectChanges();
  }

  deleteTimeInfo(index1: number) {
    if (this.data1.length > 1) {
      this.data1.removeAt(index1);
    }
    this.index1 = this.index1 - 1;
    this._changeDetectorRef.detectChanges();
  }

  addMoreDayTime(dataIndex: number) {
    let fg = this.fb.group({
      fromTime: new FormControl(''),
      toTime: new FormControl('')
    });

    (<FormArray>(<FormGroup>(<FormArray>this.providerForm.controls['data1'])
      .controls[dataIndex]).controls['phones']).push(fg);

  }

  deleteMoreDayTime(dataIndex: number, j: number) {
    (<FormArray>(<FormGroup>(<FormArray>this.providerForm.controls['data1'])
      .controls[dataIndex]).controls['phones']).removeAt(j);
  }

  getBirthDateOtherInfo(val) {
    let day = val.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    let month = val.getMonth() + 1;
    if (month < 10) {
      month = "0" + month
    }
    let year = val.getFullYear();

    let dateFormat = year + "-" + month + "-" + day;
    this.providerForm.patchValue({ mefDate: dateFormat })
  }

  getAdditionalInfo(val, eValue) {
    this.providerForm.patchValue({ otherInfo: eValue });
    if (val == 'family' && eValue == true) {
      this.showFamilyInfo = true;
    } else if (val == 'family' && eValue == false) {
      this.showFamilyInfo = false;
    }
    this.data1.removeAt(this.data1.length - 1)
    if (val == 'other' && eValue == true) {
      this.showOtherInfo = true;
      let clinicalFormGroup = this.fb.group({
        address: new FormControl(""),
        city: new FormControl(""),
        daysInfo: new FormControl(""),
        phones: this.fb.array([])
      });
      this.data1.push(clinicalFormGroup);
      let dataIndex = this.data1.length - 1;
      this.addMoreDayTime(dataIndex);
      this._productService.getProductList(this.stateId, this.currentUser.companyId).subscribe(proList => {
        this.products = proList;
      });
    } else if (val == 'other' && eValue == false) {
      this.showOtherInfo = false;
      this.data1.reset();
    }
  }

  getAdditionalInfoVendor(val, eValue) {
    this.showOtherInfoVendor = false;
    if (val == 'other' && eValue == true) {
      this.showOtherInfoVendor = true;
    } else if (val == 'other' && eValue == false) {
      this.showOtherInfoVendor = false;
    }
  }
  getUserMappedAreas(userId) {
    this.providerForm.patchValue({ userId: userId });
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, userId).subscribe(res => {
      this.areas = res;
    });
  }

  saveMoreProvider() {
    console.log('Form Data',this.providerForm.value)

    let finalArrObj = [];
    var data = {};
    
    data = {
      "companyId": this.currentUser.companyId,
      // "stateId": '',//this.currentUser.userInfo[0].stateId,
      // "districtId":'',// this.currentUser.userInfo[0].districtId,
      "blockId": this.providerForm.value.area,
      "providerCode": this.providerForm.value.doctorCode,
      "providerName": this.providerForm.value.doctorName,
      "aadhar": this.providerForm.value.aadhar,
      "phone": this.providerForm.value.phoneNum,
      "category": this.providerForm.value.category,
      "address": this.providerForm.value.address,

      "email": this.providerForm.value.email,
      "isDoctorOtherInfo": this.providerForm.value.otherInfo ? this.providerForm.value.otherInfo : false,
      "status": false,
      "appStatus": 'pending',
      "delStatus": '',
      "mgrAppDate": new Date('1900-01-02T00:00:00'),
      "finalAppDate": new Date('1900-01-02T00:00:00'),
      "mgrDelDate": new Date('1900-01-02T00:00:00'),
      "finalDelDate": new Date('1900-01-02T00:00:00'),
      "appByMgr": '',
      "finalAppBy": '',
      "delByMgr": '',
      "finalDelBy": '',
      "createdAt": new Date(),
      "updatedAt": new Date()
    }
    if (this.currentUser.userInfo[0].rL == 0) {
      data['stateId'] = this.providerForm.value.stateId;
      data['districtId'] = this.providerForm.value.districtId;
    } else {
      data['stateId'] = this.currentUser.userInfo[0].stateId;
      data['districtId'] = this.currentUser.userInfo[0].districtId;
    }
    if (this.providerForm.value.reportTypeName == 'doctor') {
      var childInfos = [];
      let doctorClinicalInfo = [];
      let sitDaysTime = [];
      let doctorSitTime = [];
      let doctorFamilyInfo = {};
      doctorFamilyInfo['spouseName'] = this.providerForm.value.spouseName;
      doctorFamilyInfo['spouseProfession'] = this.providerForm.value.spouseProfession;
      doctorFamilyInfo['spouseBirthDate'] = new Date(this.providerForm.value.spouseBirthDate);
      if (this.providerForm.value.data.length > 0) {
        for (var i = 0; i < this.providerForm.value.data.length; i++) {
          childInfos.push({
            "childName": this.providerForm.value.data[i].childName,
            "childAge": this.providerForm.value.data[i].age
          });
        }
      }
      for (var ii = 0; ii < this.providerForm.value.data1.length; ii++) {
        if (this.providerForm.value.data1[ii].phones.length > 0) {
          for (var iii = 0; iii < this.providerForm.value.data1[ii].phones.length; iii++) {
            sitDaysTime.push({
              fromTime: this.providerForm.value.data1[ii].phones[iii].fromTime,
              toTime: this.providerForm.value.data1[ii].phones[iii].toTime
            });
          }
        }
        doctorClinicalInfo.push({
          "position": ii,
          "city": this.providerForm.value.data1[ii].city,
          "day": this.providerForm.value.data1[ii].daysInfo,
          "doctorSitTime": sitDaysTime
        });

      }
      doctorFamilyInfo['childInfo'] = childInfos;
      data['doctorClinicalInfo'] = doctorClinicalInfo;
      data['providerType'] = 'RMP';
      data['degree'] = this.providerForm.value.degree;
      data['specialization'] = this.providerForm.value.specialization;
      data['isDoctorFamilyInfo'] = this.providerForm.value.familyInfo;
      data['doctorFamilyInfo'] = doctorFamilyInfo; //"doa" : ISODate("1970-01-01T00:00:00.000+0000")
      data['dob'] = this.providerForm.value.birthDateOtherInfo ? _moment(new Date(new Date(this.providerForm.value.birthDateOtherInfo).setHours(0,0,0,0))).format("YYYY-MM-DD") : _moment(new Date(new Date("1970-01-01T00:00:00.000+0000").setHours(0,0,0,0))).format("YYYY-MM-DD"); 
      data['doa'] = this.providerForm.value.annyDate ? _moment(new Date(new Date(this.providerForm.value.annyDate).setHours(0,0,0,0))).format("YYYY-MM-DD") : _moment(new Date(new Date("1970-01-01T00:00:00.000+0000").setHours(0,0,0,0))).format("YYYY-MM-DD"); 
      data['frequencyVisit'] = this.providerForm.value.frequencyVisit;
      data['contactPerson'] = this.providerForm.value.contactPerson;
      data['businessPotential'] = this.providerForm.value.businessPotential;
      data['focusProduct'] = this.providerForm.value.focusProduct;
      data['licenceNumber'] = this.providerForm.value.licence;
    } else if (this.providerForm.value.reportTypeName == 'vendor') {
      let vendorOwnerInfo = {};
      vendorOwnerInfo['ownerName'] = this.providerForm.value.ownerName ? this.providerForm.value.ownerName : "",
        vendorOwnerInfo['ownerDob'] = this.providerForm.value.ownerDob ? this.providerForm.value.ownerDob : "",
        vendorOwnerInfo['ownerDoa'] = this.providerForm.value.ownerDoa ? this.providerForm.value.ownerDoa : "",
        vendorOwnerInfo['shopDoa'] = this.providerForm.value.shopDoa ? this.providerForm.value.shopDoa : "",
        vendorOwnerInfo['panCard'] = this.providerForm.value.pancard ? this.providerForm.value.pancard : "",
        vendorOwnerInfo['gstNum'] = this.providerForm.value.gst ? this.providerForm.value.gst : "",
        vendorOwnerInfo['drugLicenceNumber'] = this.providerForm.value.licence ? this.providerForm.value.licence : "",
        vendorOwnerInfo['address'] = this.providerForm.value.venAddress ? this.providerForm.value.venAddress : "",
        vendorOwnerInfo['city'] = this.providerForm.value.venCity ? this.providerForm.value.venCity : ""
      data['providerType'] = this.providerForm.value.type ? this.providerForm.value.type : "";
      data['vendorOwnerInfo'] = vendorOwnerInfo;
      data['address'] = this.providerForm.value.venAddress ? this.providerForm.value.venAddress : "";
      data['isDoctorOtherInfo'] = false;
      data['isVendorOtherInfo'] = this.providerForm.value.otherInfo ? this.providerForm.value.otherInfo : false;
    }
    if (this.currentUser.userInfo[0].rL == 0) {
      data['status'] = true;
      data['appStatus'] = 'approved';
      data['mgrAppDate'] = new Date();
      data['finalAppDate'] = new Date();
      data['appByMgr'] = this.currentUser.id;
      data['finalAppBy'] = this.currentUser.id;
      data["userId"] = this.providerForm.value.userId;
    } else if (this.currentUser.userInfo[0].rL > 1) {
      if (this.currentUser.company.validation.approvalUpto == 0 || this.currentUser.company.validation.approvalUpto == 1) {
        data['status'] = true;
        data['appStatus'] = 'approved';
        data['mgrAppDate'] = new Date();
        data['finalAppDate'] = new Date();



        data['appByMgr'] = this.currentUser.id;
        data['finalAppBy'] = this.currentUser.id;
        data["userId"] = this.providerForm.value.userId;
      } else {
        data['appStatus'] = 'InProcess';
        data['mgrAppDate'] = new Date();
        data['appByMgr'] = this.currentUser.id;
      }
    } else if (this.currentUser.userInfo[0].rL == 1) {
      data["userId"] = this.currentUser['id'];
      if (this.currentUser.company.validation.approvalUpto == 0) {
        data['status'] = true;
        data['appStatus'] = 'approved';
        data['mgrAppDate'] = new Date();
        data['finalAppDate'] = new Date();
        data['appByMgr'] = this.currentUser.id;
        data['finalAppBy'] = this.currentUser.id;
      }
    }
    finalArrObj.push(data);    
    this._providerService.providerCreation(data).subscribe(res => {
      this.showFamilyInfo = false;
      this.showOtherInfo = false;
      this.showOtherInfoVendor = false;
      this.showDaysTimeInfo = false;
      this.setBasicDetails();
      //this.providerForm.reset();
      let type="doctor"      
      if(res[0].providerType=="RMP"){
         type="doctor"
      }else{
        type="vendor"
      }
       this.reportType(type);
      //  this.createForm(this.fb);
      this.formType = type;
      this._toastr.success("Provider creation successfully !", "Provider creation successfully !!");
      
      this.callStateComponent.setBlank();
      this.callStateComponent1.setBlank();
      this.callDistrictComponent.setBlank();
      this.callDistrictComponent1.setBlank();
      this.callEmployeeComponent.setBlank();
      this.callEmployeeComponent1.setBlank();
      this.callDistrictComponent1.setBlank();
      this._changeDetectorRef.detectChanges();
      
    }, err => {
      console.log(err);
    })
  }
}
