import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';

@Component({
  selector: 'm-edit-provider-location',
  templateUrl: './edit-provider-location.component.html',
  styleUrls: ['./edit-provider-location.component.scss'] 
})
export class EditProviderLocationComponent implements OnInit {
	workingadd: any;
  currentUser = JSON.parse(sessionStorage.currentUser);

  districtData: any;
  districtIds: any;
  districtIdsLength: number;
  designationData = [];

  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  DA_TYPE = this.currentUser.company.expense.daType;
  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  status: boolean= false;
  constructor(
    private dialogRef: MatDialogRef<EditProviderLocationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private changeDetectRef: ChangeDetectorRef,
    
    private formBuilder: FormBuilder,
    public toasterService: ToastrService,
    private userDetailservice: UserDetailsService,
    private userAreaMappingService: UserAreaMappingService,

  ) { }
  locationForm: FormGroup;


  ngOnInit() {
    
    console.log(this.data);
    if(this.currentUser.company.validation.isShowOnlyLatLong===true){
      this.status=true;
      // let i = 0;
      // this.data.workingAddress.forEach(element => {
      //   element['latlong'] = this.data.latlong;
        
      // });
    }
   
    this.getLocationHead();
    this.locationForm = this.formBuilder.group({
      id: new FormControl(this.data.value.providerId),
      providerName: new FormControl(this.data.value.providerName),
      workingAddress: new FormControl(null),
    });
    
  }
  getLocationHead(){
    this.workingadd=this.data.value
  }
  UnTagged(data){
    this.locationForm.patchValue({workingAddress:data})
   
    let object={
      companyId:this.companyId,
      id:this.locationForm.value.id,
      workingAddress:this.locationForm.value.workingAddress,
    }
    console.log(object)
        this.userAreaMappingService.unTaggedProvider(object).subscribe(result => {
          this.toasterService.success('Location has been Untagged Successfully !!!');
          this.changeDetectRef.detectChanges();
          this.dialogRef.close();
          this.getLocationHead();
        },
          err => {
            console.log(err);
          })

  }
 

}
