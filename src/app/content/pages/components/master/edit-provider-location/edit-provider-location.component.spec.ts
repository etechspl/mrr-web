import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProviderLocationComponent } from './edit-provider-location.component';

describe('EditProviderLocationComponent', () => {
  let component: EditProviderLocationComponent;
  let fixture: ComponentFixture<EditProviderLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProviderLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProviderLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
