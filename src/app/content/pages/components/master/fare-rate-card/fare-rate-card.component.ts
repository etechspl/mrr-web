import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { FareRateCardService } from '../../../../../core/services/FareRateCard/fare-rate-card.service';
import { MatTableDataSource, MatTable, MatSort, MatPaginator, MatDialog, MatDialogConfig } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DACategoryComponent } from '../../filters/dacategory/dacategory.component';
import { EditFareRateCardComponent } from '../edit-fare-rate-card/edit-fare-rate-card.component';
declare var $;
import Swal from 'sweetalert2'
import { BrowserJsonp } from '@angular/http/src/backends/browser_jsonp';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';
import { DistrictComponent } from '../../filters/district/district.component';


@Component({
	selector: "m-fare-rate-card",
	templateUrl: "./fare-rate-card.component.html",
	styleUrls: ["./fare-rate-card.component.scss"]
})
export class FareRateCardComponent implements OnInit {
	private formSubmitAttempt: boolean;

	usersData: any;
	isLoading: boolean = false;
	showProcessing: boolean = false;

	//-----nipun(17-01-20) start
	dialogRef: any = null;
	dialogConfig = new MatDialogConfig();
	//-----nipun(17-01-20) end

	currentUser = JSON.parse(sessionStorage.currentUser);

	// isDivisionExist = this.currentUser.company.isDivisionExist;
	companyId = this.currentUser.company.id;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	daType = this.currentUser.company.expense.daType;
	isDivisionExist = this.currentUser.company.isDivisionExist;

	isShowCategory: boolean;
	//isShowDivision: boolean;
	isShowFRCCode: boolean;
	isShowDesignation: boolean;
	isShowState: boolean;
	isShowDistrict: boolean;
	isShowEmployees: boolean;

	frcForm: FormGroup;

	displayedColumns;

	placeHolderForTA: string = "Enter TA per KM";

	placeHolderForFRC = "Select FRC Code";

	constructor(
		private dialog: MatDialog,
		private changeDetectorRef: ChangeDetectorRef,
		private fb: FormBuilder,
		private fareRateCardService: FareRateCardService,
		private userDetailService: UserDetailsService,
		private _toastr: ToastrService,
		private service: FareRateCardService
	) { }

	dataSource = new MatTableDataSource<any>();

	@ViewChild(MatTable) table: MatTable<any>;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(DesignationComponent) designationComponent: DesignationComponent;
	@ViewChild(DACategoryComponent) daCategoryComponent: DACategoryComponent;
	@ViewChild(DivisionComponent) divisionComponent: DivisionComponent;
	@ViewChild(StateComponent) stateComponent: StateComponent;
	@ViewChild(DistrictComponent) districtComponent: DistrictComponent;
	@ViewChild(EmployeeComponent) employeeComponent: EmployeeComponent;

	@ViewChild("table") dataTable;
	companyDataTable: any;
	companyDTOptions: any;
	isShowDivision = false;

	ngOnInit() {
		this.createForm();
		this.fareRateCardService.frcUpdatedOrNot.subscribe(res => {
			(res === 'updated') ? this.getFareRateCardDetails() : '';
		})
		this.displayColumnsToBeView();
		//Commented there is no use of Division concept : 26-12-2019 By PK as per discussion with Anjana Mam
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.frcForm.setControl("divisionId", new FormControl());

			this.divisionComponent.getDivisions({
				companyId: this.companyId,
				designationLevel: this.designationLevel
			});
			this.designationComponent.setBlank();
		}

		if (this.daType === "category wise") {
			this.isShowCategory = true;
			this.isShowFRCCode = false;
			this.isShowDesignation = false;
			this.frcForm.removeControl("designation");
		} else if (this.daType === "Designation-State Wise") {
			this.isShowFRCCode = true;
			this.isShowCategory = false;
			this.isShowDesignation = true;
			this.frcForm.setControl("designation", new FormControl("", Validators.required));
		} else if(this.daType === "employee wise"){
		//	this.isShowCategory = true;
			this.isShowState = true;
			this.isShowFRCCode=true;
			this.isShowDistrict = true;
			this.isShowDesignation = true;
			this.isShowEmployees = true;
			this.frcForm.setControl("state", new FormControl());
			this.frcForm.setControl("district", new FormControl());
			this.frcForm.setControl("designation", new FormControl());
			this.frcForm.setControl("userId", new FormControl("", Validators.required));

		}

		this.getFareRateCardDetails();
	}

	displayColumnsToBeView() {
		//Commented there is no use of Division concept : 26-12-2019 By PK as per discussion with Anjana Mam
		// if (this.isDivisionExist === true) {
		//   if (this.daType === "category wise") {
		//     this.displayedColumns = ['sn', 'divisionName', 'fromDistance', 'toDistance', 'allounceToBeGet', 'applicableTo', 'rate', 'description', 'frcCode'];
		//   } else if (this.daType === "Designation-State Wise") {
		//     this.displayedColumns = ['sn', 'divisionName', 'designation', 'fromDistance', 'toDistance', 'allounceToBeGet', 'applicableTo', 'rate', 'description', 'frcCode'];
		//   }
		// } else {
		//   if (this.daType === "category wise") {
		//     this.displayedColumns = ['sn', 'fromDistance', 'toDistance', 'allounceToBeGet', 'applicableTo', 'rate', 'description', 'frcCode'];
		//   } else if (this.daType === "Designation-State Wise") {
		//     this.displayedColumns = ['sn', 'designation', 'fromDistance', 'toDistance', 'allounceToBeGet', 'applicableTo', 'rate', 'description', 'frcCode'];
		//   }
		// }

		if (this.daType === "category wise") {
			this.displayedColumns = [
				"sn",
				"fromDistance",
				"toDistance",
				"allounceToBeGet",
				"applicableTo",
				"rate",
				"description",
				"frcCode"
			];
		} else if (this.daType === "Designation-State Wise") {
			this.displayedColumns = [
				"sn",
				"designation",
				"fromDistance",
				"toDistance",
				"allounceToBeGet",
				"applicableTo",
				"rate",
				"description",
				"frcCode"
			];
		}
	}

	setPlaceholder($event) {
		if ($event.value === "Lumsum") {
			this.placeHolderForTA = "Enter the Lumsum Amount";
		} else if ($event.value === "KM Wise") {
			this.placeHolderForTA = "Enter the TA per KM";
		}
	}
	setCategoryValue($event, index) {
		this.data.at(index).patchValue({ frcCode: $event });
	}

	//Commented there is no use of Division concept : 26-12-2019 By PK as per discussion with Anjana Mam
	getDivision($event) {
		this.frcForm.patchValue({ "divisionId": $event });
		this.designationComponent.getDesignationBasedOnDivision({
			companyId: this.companyId,
			userLevel: this.designationLevel,
			division: this.frcForm.value.divisionId
		})
		this.daCategoryComponent.getDACategory(this.currentUser.companyId, this.frcForm.value.divisionId)
		if(this.daType == "employee wise"){
			let obj = {
				companyId: this.companyId,
                isDivisionExist: this.isDivisionExist,
                division: $event
			}
			this.stateComponent.getStateBasedOnDivision(obj);
		}
	}
	getDesignation(designationValue) {
		this.frcForm.patchValue({ designation: designationValue });
		// let obj = {
		// 	type: "district",
        // stateId: this.frcForm.value.state,
        // companyId: this.companyId,
		// districtId: this.frcForm.value.district,
		// designation: this.frcForm.value.designation
		// }
		// this.employeeComponent.getEmployeeDesignationStatewiseOrDistrictWise(obj);
		let obj = {};
		if (this.daType === "employee wise") {
			// obj["companyId"] = this.companyId;
			// obj["division"] = [this.frcForm.value.divisionId],
			// obj["status"] = [true];
			// obj["designationObject"] = this.frcForm.value.designation;

			// obj["stateId"] = this.frcForm.value.state;
			// obj["districtId"] = this.frcForm.value.district;
			// ==============================
			obj["companyId"] = this.companyId;
			(obj["division"] = this.frcForm.value.divisionId),
				(obj["status"] = [true]);
			obj["designationObject"] = this.frcForm.value.designation;

			obj["stateId"] = this.frcForm.value.state;
			obj["districtId"] = this.frcForm.value.district;

			this.employeeComponent.getEmployeeListBasedOnDivision(obj);
		}
	}

	
	getStateValue(val: any) {
		this.frcForm.patchValue({ state: val });
		if(this.isDivisionExist){
			let Obj = {
				"companyId": this.currentUser.company.id,
				"stateId": this.frcForm.value.state,
				isDivisionExist: true,
				division: this.frcForm.value.divisionId,
			}
			this.districtComponent.getDistrictsBasedOnDivision(Obj);
		} else {
			this.districtComponent.getDistricts(this.companyId,this.frcForm.value.state,"");
		}
	}
	
	setDistrictValue(event){
		this.frcForm.patchValue({district: event});
		let obj = {
		type: "district",
        stateId: this.frcForm.value.state,
        companyId: this.companyId,
        districtId: this.frcForm.value.district,
		}
	}

	setEmployees(event){
		this.frcForm.patchValue({userId: event});
	}

	get designation() {
		return this.frcForm.get("designation");
	}

	get fromDistance() {

		console.log("fromDistance");

		return this.frcForm.get("fromDistance");
	}
	get toDistance() {
		return this.frcForm.get("toDistance");
	}
	get allounceToBeGet() {
		return this.frcForm.get("allounceToBeGet");
	}
	get frcCode() {
		return this.frcForm.get("frcCode");
	}
	get applicableTo() {
		return this.frcForm.get("applicableTo");
	}
	get rate() {
		return this.frcForm.get("rate");
	}



	/*get frcToBeCreated() {
    return this.frcForm.get('frcToBeCreated');
  }*/

	get data() {
		return this.frcForm.get("data") as FormArray;
	}

	createFRC() {

		console.log("this.frcForm.value.data",this.frcForm.value.data);

		if (this.frcForm.valid) {
			let frcData = [];
			for (let data of this.frcForm.value.data) {


				for (let applicableTo of data.applicableTo) {
					//if daType is Designation-State wise
					if (this.daType === "Designation-State Wise") {
						for (let desig of this.frcForm.value.designation) {
							let obj = {};
							//Commented there is no use of Division concept : 26-12-2019 By PK as per discussion with Anjana Mam
							if (this.isDivisionExist === true) {
								for (const division of this.frcForm.value.divisionId) {
									obj = {
										"companyId": this.currentUser.companyId,
										"designation": desig.designation,
										"designationLevel": desig.designationLevel,
										"fromDistance": data.fromDistance,
										"toDistance": data.toDistance,
										"allounceToBeGet": data.allounceToBeGet,
										"frcCode": data.frcCode,
										"applicableTo": applicableTo,
										"rate": data.rate,
										"description": data.description,
										"divisionId": division,
										"frcType": this.daType,
										status: true
									}
									frcData.push(obj);
								}
							} else {
								obj = {
									"companyId": this.currentUser.companyId,
									"designation": desig.designation,
									"designationLevel": desig.designationLevel,
									"fromDistance": data.fromDistance,
									"toDistance": data.toDistance,
									"allounceToBeGet": data.allounceToBeGet,
									"frcCode": data.frcCode,
									"applicableTo": applicableTo,
									"rate": data.rate,
									"description": data.description,
									"frcType": this.daType,
									status: true
								}
								frcData.push(obj);
							}

							// obj = {
							// 	companyId: this.currentUser.companyId,
							// 	designation: desig.designation,
							// 	designationLevel: desig.designationLevel,
							// 	fromDistance: data.fromDistance,
							// 	toDistance: data.toDistance,
							// 	allounceToBeGet: data.allounceToBeGet,
							// 	frcCode: data.frcCode,
							// 	applicableTo: applicableTo,
							// 	rate: data.rate,
							// 	description: data.description,
							// 	frcType: this.daType
							// };
						}
					} else if (this.daType === "category wise") {
						let obj = {};
						//Commented there is no use of Division concept : 26-12-2019 By PK as per discussion with Anjana Mam
						if (this.isDivisionExist === true) {
							for (const division of this.frcForm.value.divisionId) {
								obj = {
									"companyId": this.currentUser.companyId,
									"fromDistance": data.fromDistance,
									"toDistance": data.toDistance,
									"allounceToBeGet": data.allounceToBeGet,
									"frcCode": data.frcCode,
									"applicableTo": applicableTo,
									"rate": data.rate,
									"description": data.description,
									"divisionId": division,
									"frcType": this.daType,
									status: true
								}
								frcData.push(obj);
							}
						} else {
							obj = {
								"companyId": this.currentUser.companyId,
								"fromDistance": data.fromDistance,
								"toDistance": data.toDistance,
								"allounceToBeGet": data.allounceToBeGet,
								"frcCode": data.frcCode,
								"applicableTo": applicableTo,
								"rate": data.rate,
								"description": data.description,
								"frcType": this.daType,
								status: true
							}
							frcData.push(obj);
						}

						// obj = {
						// 	companyId: this.currentUser.companyId,
						// 	fromDistance: data.fromDistance,
						// 	toDistance: data.toDistance,
						// 	allounceToBeGet: data.allounceToBeGet,
						// 	frcCode: data.frcCode,
						// 	applicableTo: applicableTo,
						// 	rate: data.rate,
						// 	description: data.description,
						// 	frcType: this.daType
						// };
					} else if(this.daType === "employee wise"){
						let obj = {};
						if (this.isDivisionExist === true) {
							for(let user of this.frcForm.value.userId){
								
							for (const division of this.frcForm.value.divisionId) {
								obj = {
									"companyId": this.currentUser.companyId,
									"fromDistance": data.fromDistance,
									"toDistance": data.toDistance,
									"allounceToBeGet": data.allounceToBeGet,
									"frcCode": data.frcCode,
									"applicableTo": applicableTo,
									"rate": data.rate,
									"description": data.description,
									"divisionId": division,
									"frcType": this.daType,
									"userId": user,
									status: true
								}
								frcData.push(obj);
							}
						}

						} else {
							for(let user of this.frcForm.value.userId){
							    obj = {
							    	"companyId": this.currentUser.companyId,
							    	"fromDistance": data.fromDistance,
							    	"toDistance": data.toDistance,
							    	"allounceToBeGet": data.allounceToBeGet,
							    	"frcCode": data.frcCode,
							    	"applicableTo": applicableTo,
							    	"rate": data.rate,
							    	"description": data.description,
							    	"frcType": this.daType,
							    	"userId": user,
							    	status: true
							    }
							    frcData.push(obj);
						    }
						}
					}
				}
			}

			this.formSubmitAttempt = true;

			console.log("frcData",frcData);

			this.fareRateCardService
				.createFareRateCard(frcData)
				.subscribe(res => {
					this.formSubmitAttempt = false;
					this.reset();
					this.getFareRateCardDetails();
					this._toastr.success(
						"For Selected Filter FRC is Created Successfully",
						"FRC Created Successfully!!!"
					);
					this.designationComponent.setBlank();
					this.daCategoryComponent.setBlank();
					// this.divisionComponent.setBlank();
					this.stateComponent.setBlank();
					this.districtComponent.setBlank();
					this.designationComponent.setBlank();
					this.employeeComponent.setBlank();
					this.designationComponent.ngOnInit();
					(!this.changeDetectorRef['destroyed']) ? this.changeDetectorRef.detectChanges() : null;

				});
		}
	}
	reset() {
		this.frcForm.reset();

		Object.keys(
			this.frcForm.controls["data"]["controls"][0].controls
		).forEach(name => {
			let con = this.frcForm.controls["data"]["controls"][0].controls[
				name
			];
			con.setErrors(null);
		});
		//this.frcForm.controls["data"][0] = [this.addMoreFRC()]
	}

	createForm() {
		this.frcForm = this.fb.group({
			data: this.fb.array([
				this.fb.group({
					title: new FormControl(""),
					category: new FormControl(""),
					image: new FormControl(""),
					frcCode: new FormControl("", Validators.required),
					applicableTo: new FormControl("", Validators.required),
					fromDistance: new FormControl("", Validators.required),
					toDistance: new FormControl("", Validators.required),
					allounceToBeGet: new FormControl("", Validators.required),
					rate: new FormControl("", Validators.required),
					description: new FormControl("")
				})
			])
		});
	}

	addMoreFRC() {
		this.data.push(
			this.fb.group({
				fromDistance: new FormControl("", Validators.required),
				toDistance: new FormControl("", Validators.required),
				allounceToBeGet: new FormControl("", Validators.required),
				frcCode: new FormControl("", Validators.required),
				applicableTo: new FormControl("", Validators.required),
				rate: new FormControl("", Validators.required),
				description: new FormControl("")
			})
		);

		(!this.changeDetectorRef['destroyed']) ? this.changeDetectorRef.detectChanges() : null;

	}

	deleteFRC(index) {
		if (this.data.length > 1) {
			this.data.removeAt(index);
		}
		(!this.changeDetectorRef['destroyed']) ? this.changeDetectorRef.detectChanges() : null;

	}

	//getting fare rate card details
	getFareRateCardDetails() {
		let obj = {};
		//Commented there is no use of Division concept : 26-12-2019 By PK as per discussion with Anjana Mam
		if (this.isDivisionExist === true) {
			if (this.frcForm.value.divisionId !== null) {
				obj = {
					divisionId: this.frcForm.value.divisionId,
					isDivisionExist: this.isDivisionExist,
					companyId: this.companyId,
					frcType: this.daType
				}
			} else {
				obj = {
					companyId: this.companyId,
					frcType: this.daType
				}
			}
		} else {
			obj = {
				companyId: this.companyId,
				frcType: this.daType
			}
		}


		this.fareRateCardService.getFareRateCardDetails(obj).subscribe(res => {
			console.log('This is response-------------------------------->',res)
			let array = res.map(item => {
				return {
					...item
				};
			});
			this.dataSource = new MatTableDataSource(array);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;

			//---------------nipun (17-01-20120) creation of datatable
			this.companyDTOptions = {
				pagingType: "full_numbers",
				paging: true,
				ordering: true,
				info: true,
				scrollY: 300,
				scrollX: true,
				fixedColumns: true,
				destroy: true,
				data: res,
				responsive: true,
				dom: "Bfrtip",
				buttons: [

					{
						extend: "csv",
						exportOptions: {
							columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
						}
					},
					{
						extend: "copy",
						exportOptions: {
							columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
						}
					},
					{
						extend: "print",
						exportOptions: {
							columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
						}
					}
				],
				columns: [
					{
						title: "Division",
						data: "divisionMaster.divisionName",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								console.log(data);
								//var data=data.divisionName;
								return data
							}
						},
						defaultContent: "---"
					},
					{
						title: "Designation",
						data: {designation: "designation", employeeWisedesignation: "userInfo"},
						render: (data) => {
							if(this.currentUser.company.expense.daType == 'employee wise'){
								if (!data.userInfo) {
									return "---";
								} else {
									return data.userInfo.designation;
								}
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "Name",
						data: {name: "name", employeeWisedesignation: "userInfo"},
						render: (data) => {
							if(this.currentUser.company.expense.daType == 'employee wise'){
								if (!data.userInfo) {
									return "---";
								} else {
									return data.userInfo.name;
								}
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "From Distance",
						data: "fromDistance",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "To Distance",
						data: "toDistance",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "Allowance To Be Get",
						data: "allounceToBeGet",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "Applicable To",
						data: "applicableTo",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "Fare",
						data: "rate",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "Description",
						data: "description",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "FRC Code",
						data: "frcCode",
						render: function (data) {
							if (data === "") {
								return "---";
							} else {
								return data;
							}
						},
						defaultContent: "---"
					},
					{
						title: "Edit",
						render: function () {
							//		return "<button id='editButton' mat-icon-button ><mat-icon id='edit'>delete_outline</mat-icon></button>"
							return "<a mat-icon-button color='warn' 'id='editButton'><i  class='fas fa-edit'  style='color: DodgerBlue;' id='edit'></i></a>"

						}
					},
					{
						title: "Delete",
						render: function () {
							//return "<button id='deleteButton' class='btn btn-warning btn-sm'><i id='delete' class='la la-edit'></i>&nbsp;Delete</button>";
							return "<a  id='deleteButton'><i id='delete'  class='fa fa-trash' style='color: #f44336;'></i></a>"
						}
					}
				],
				rowCallback: (
					row: Node,
					data: any[] | Object,
					index: number
				) => {
					const self = this;
					$("td", row).unbind("click");
					$("td a", row).bind("click", event => {
						if (event.target.id === "editButton" || event.target.id === "edit") {
							self.editFrcData(data);
							console.log("data++++++",data);
						} else if (event.target.id === "deleteButton" || event.target.id === "delete") {
							self.deleteFrcData(data);
						}
					});
				}
			};
			if (this.currentUser.company.isDivisionExist == true) {
				this.companyDTOptions.columns[0].visible = true;
				this.companyDTOptions.columns[1].visible = true;
			} else {
				this.companyDTOptions.columns[1].visible = true;
				this.companyDTOptions.columns[0].visible = false;
			}


			if (this.currentUser.company.isDivisionExist == true) {

				this.companyDTOptions.buttons.push({
					extend: 'excel',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
					}
				})
			}

			else {

				this.companyDTOptions.buttons.push({
					extend: 'excel',
					exportOptions: {
						columns: [1, 2, 3, 4, 5, 6, 7, 8]
					}
				})
			}

			this.companyDataTable = $(this.dataTable.nativeElement);
			this.companyDataTable.DataTable(this.companyDTOptions);

		});
	}


	editFrcData(obj: any) {
		obj['isShowDesignation'] = this.isShowDesignation;
		this.dialogConfig.data = obj;
		this.dialogConfig.disableClose = true;
		(this.dialogRef === null) ? this.dialogRef = this.dialog.open(EditFareRateCardComponent, this.dialogConfig) : '';
		this.dialogRef.afterClosed().subscribe(() => {
			this.dialogRef = null;
		});
	}

	deleteFrcData(obj: any) {
		let object = {
			where: {
				frcId: obj.id,
				companyId: obj.companyId,
				status: true
			}
		}
		this.service.getFareRateCard(object).subscribe(res => {
			if (res.length > 0) {
				this._toastr.error("This card is already assigned to the STPs");
			} else {
				let update = {
					status: false,
					updatedAt: new Date(),
					updatedBy: this.currentUser.id
				}
				let object = {
					id: obj.id,
					companyId: obj.companyId
				}
				let msg = "Fare Rate Card Will Be Deleted.";
				let buttonMsg = "Yes, Delete it !!!";
				const swalWithBootstrapButtons = Swal.mixin({
					customClass: {
						confirmButton: 'btn btn-success',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false,
				})
				swalWithBootstrapButtons.fire({
					title: 'Are you sure?',
					text: msg,
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: buttonMsg,
					cancelButtonText: 'No, cancel!',
					reverseButtons: true,
					allowOutsideClick: false
				}).then((result) => {
					if (result.value) {
						this.service.deleteFareRateCard(object, update).subscribe(res => {
							if (res) {
								this._toastr.success("Fare Rate Card has been deleted successfully!!");
								this.getFareRateCardDetails()
							}
						});
					}
				})

			}
		});
	}


	// deleteFrcData(obj: any) {
	// 	let msg = "The User Will Be Deleted.";
	//   let buttonMsg = "Yes, Delete it !!!";
	//   const swalWithBootstrapButtons = Swal.mixin({
	//     customClass: {
	//       confirmButton: 'btn btn-success',
	//       cancelButton: 'btn btn-danger'
	//     },
	//     buttonsStyling: false,
	//   });
	//   swalWithBootstrapButtons.fire({
	//     title: 'Are you sure?',
	//     text: msg,
	//     type: 'warning',
	//     showCancelButton: true,
	//     confirmButtonText: buttonMsg,
	//     cancelButtonText: 'No, cancel!',
	//     reverseButtons: true,
	//     allowOutsideClick: false
	//   }).then(res=>{
	//     console.log('response',res)
	//     if(res.value === true){
	//       console.log('approved deletion')
	//     } else {
	//       console.log('cancle deletion')
	//     }
	//   });
	// }
	//---------------nipun  (17-01-20120) end
}
