import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FareRateCardComponent } from './fare-rate-card.component';

describe('FareRateCardComponent', () => {
  let component: FareRateCardComponent;
  let fixture: ComponentFixture<FareRateCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FareRateCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FareRateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
