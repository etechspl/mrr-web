import { SelectionModel } from "@angular/cdk/collections";
import { DistrictService } from "../../../../../core/services/district.service";
import { URLService } from "../../../../../core/services/URL/url.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { MatSlideToggleChange } from "@angular/material";
import Swal from "sweetalert2";
@Component({
	selector: "m-district-details",
	templateUrl: "./district-details.component.html",
	styleUrls: ["./district-details.component.scss"],
})
export class DistrictDetailsComponent implements OnInit {
	urlService;
	districtService;
	//districtData : District;
	check: boolean;

	selection = new SelectionModel(true, []);
	selection1 = new SelectionModel(true, []);

	displayedColumns = ["select", "districtName"];
	displayedColumns1 = ["select", "districtName"];
	dataSource;
	dataSource1;
	isShowToBeMappedDistrict = false;
	isShowMappedDistrict = false;
	//selection = new SelectionModel(true, []);

	constructor(
		private changeDetecterRef: ChangeDetectorRef,
		urlService: URLService,
		districtService: DistrictService
	) {
		this.urlService = urlService;
		this.districtService = districtService;
	}
	currentUser = JSON.parse(sessionStorage.currentUser);
	districtForm = new FormGroup({
		stateInfo: new FormControl(null, Validators.required),
	});

	getStateValue(val) {
		this.districtForm.patchValue({ stateInfo: val });
	}
	ngOnInit() {}
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}

	getDistricts() {
		this.isToggled = false;
		this.getDistrictToBeMapped();
		this.districtService
			.getDistricts(
				this.currentUser.companyId,
				this.districtForm.value.stateInfo,
				false
			)
			.subscribe(
				(districts) => {
					this.dataSource = districts;
					this.isShowMappedDistrict = true;
					this.changeDetecterRef.detectChanges();
				},
				(err) => {
					// console.log(err);
				}
			);
	}

	getDistrictToBeMapped() {
		this.districtService
			.getDistrictToBeMapped(
				this.currentUser.companyId,
				this.districtForm.value.stateInfo
			)
			.subscribe(
				(districtsToBeMapped) => {
					this.dataSource1 = districtsToBeMapped;
					this.isShowToBeMappedDistrict = true;
					this.changeDetecterRef.detectChanges();
				},
				(err) => {
					// console.log(err);
				}
			);
	}

	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected1() {
		const numSelected = this.selection1.selected.length;
		const numRows = this.dataSource1.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle1() {
		this.isAllSelected1()
			? this.selection1.clear()
			: this.dataSource1.forEach((element) => {
					this.selection1.select(element);
			  });
	}
	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.forEach((element) => {
					this.selection.select(element);
			  });
	}

	//mapping districts

	mapDistricts(selectedDistricts) {
		// console.log('districts selected for mapping => ',selectedDistricts);
		this.districtService
			.mapDistricts(
				this.currentUser.companyId,
				this.districtForm.value.stateInfo,
				selectedDistricts
			)
			.subscribe(
				(res) => {
					(Swal as any).fire({
            title: "Districts Mapped Successfully",
						type: "success",
					});
          this.selection1.clear();
					this.getDistricts();
				},
				(err) => {
					(Swal as any).fire({
						title: "Something went wrong!",
						type: "error",
					});
				}
			);
	}

	removeMappedDistricts(selectedDistricts) {
		console.log("selectedDistricts..>..", selectedDistricts);
		this.districtService
			.removeMappedDistricts(
				this.currentUser.companyId,
				this.districtForm.value.stateInfo,
				selectedDistricts
			)
			.subscribe(
				(res) => {
          (Swal as any).fire({
            title: "Removed Mapped Districts Successfully",
						type: "success",
					});
          this.selection.clear();
					this.getDistricts();
				},
				(err) => {
					(Swal as any).fire({
						title: "Something went wrong!",
						type: "error",
					});
				}
			);
	}
}
