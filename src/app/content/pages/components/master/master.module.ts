import { InvestmentRepeatTypeComponents } from './mgr-monthly-investment/repeat-section.types';
import { MgrMonthlyInvestmentComponent } from './mgr-monthly-investment/mgr-monthly-investment.component';
import { NestedListComponent } from './user-access/nested-list/nested-list.component';
import { UserAccessComponent } from './user-access/user-access.component';
import { STPMigrationOrTransferComponent } from './stpmigration-or-transfer/stpmigration-or-transfer.component';
import { RepeatTypeComponents } from './dcr-submission/repeat-section.types';
import { FormlyFieldNgSelect } from './dcr-submission/ng-select.type.component';
import { FiltersModule } from '../filters/filters.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { PartialsModule } from '../../../partials/partials.module';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import {MatChipsModule} from '@angular/material/chips';
// Core
// Core => Services (API)
import { CustomersService } from '../../_core/services';
// Core => Utils
import { HttpUtilsService } from '../../_core/utils/http-utils.service';
import { TypesUtilsService } from '../../_core/utils/types-utils.service';
import { LayoutUtilsService } from '../../_core/utils/layout-utils.service';
import { InterceptService } from '../../_core/utils/intercept.service';
// Shared
import { MasterComponent } from './master.component';
import { UserCreationComponent } from './user-creation/user-creation.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatStepperModule } from '@angular/material/stepper';
import { ProviderModuleNewComponent } from './provider-module-new/provider-module-new.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';


// Material
import {
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatFormFieldModule
} from '@angular/material';
import { DesignationComponent } from './designation/designation.component';
import { DistrictDetailsComponent } from './district-details/district-details.component';
import { CreatedistrictComponent } from './createdistrict/createdistrict.component';
import { AreaCreationComponent } from './area-creation/area-creation.component';
import { AreaEditDialogComponent } from './area-edit-dialog/area-edit-dialog.component';
import { HierarchyComponent } from './hierarchy/hierarchy.component';
import { SssSubmissionComponent } from './sss-submission/sss-submission.component';
import { RepeatTypeComponent } from './sss-submission/repeat-section.type';
import { MrStockiestLinkingComponent } from './mr-stockiest-linking/mr-stockiest-linking.component';
import { ProductCreationComponent } from './product-creation/product-creation.component';
//import { AreaComponent } from '../filters/area/area.component';
import { SssClosingSubmissionComponent } from './sss-closing-submission/sss-closing-submission.component';
import { GiftCreationComponent } from './gift-creation/gift-creation.component';
import { EditGiftComponent } from './edit-gift/edit-gift.component';
import { ApprovalMasterComponent } from './approval-master/approval-master.component';
import { FareRateCardComponent } from './fare-rate-card/fare-rate-card.component';
import { StateWiseDAComponent } from './state-wise-da/state-wise-da.component';
import { StpComponent } from './stp/stp.component';
import { StpApproveComponent } from './stp-approve/stp-approve.component';
import { STPEditComponent } from './stpedit/stpedit.component';
import { TargetSubmissionComponent } from './target-submission/target-submission.component';
import { STPDetailsComponent } from './stpdetails/stpdetails.component';
import { TransferMasterDataComponent } from './transfer-master-data/transfer-master-data.component';
import { TourProgramComponent } from './tour-program/tour-program.component';
import { HolidayComponent } from './holiday/holiday.component';
import { CreateDesignationComponent } from './create-designation/create-designation.component';
import { LockOpenComponent } from './lock-open/lock-open.component';

import { SampleGiftEditComponent } from './sample-gift-edit/sample-gift-edit.component';
import { MefCreationComponent } from './mef-creation/mef-creation.component';
import { SamplesIssueComponent } from './samples-issue/samples-issue.component';
import { DcrSubmissionComponent } from './dcr-submission/dcr-submission.component';
import { DcrPreviewComponent } from './dcr-preview/dcr-preview.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { EditMefTrackerComponent } from './edit-mef-tracker/edit-mef-tracker.component';
import { ModifyTargetComponent } from './modify-target/modify-target.component';
import { ProviderCreationComponent } from './provider-creation/provider-creation.component';
import { ModifyProviderComponent } from './modify-provider/modify-provider.component';
import { CompanyCreationComponent } from './company-creation/company-creation.component';
import { UploadMasterDataComponent } from './upload-master-data/upload-master-data.component';
import { BroadcastMessageComponent } from './broadcast-message/broadcast-message.component';
import { CalendarComponent } from './calendar/calendar.component';
import { TourProgramDetailsComponent } from './tour-program-details/tour-program-details.component';
import { CalendarModule } from 'primeng/calendar';
import { ModifyTourProgramComponent } from './modify-tour-program/modify-tour-program.component';
import { DoctorLocationTaggedComponent } from './doctor-location-tagged/doctor-location-tagged.component';
import { ProviderLocationTaggedComponent } from './provider-location-tagged/provider-location-tagged.component';
import { SssSalesSubmissionComponent } from './sss-sales-submission/sss-sales-submission.component';
import { EditFareRateCardComponent } from './edit-fare-rate-card/edit-fare-rate-card.component';
import { TransferModuleComponent } from './transfer-module/transfer-module.component';
import { DayPlanLockOpenComponent } from './day-plan-lock-open/day-plan-lock-open.component';
import { DoctorVendorComponent } from './doctor-vendor/doctor-vendor.component';
import { CreateDivisionComponent } from './create-division/create-division.component';
import { LockOpenNewComponent } from './lock-open-new/lock-open-new.component';
import { ApllicationFormComponent } from './apllication-form/apllication-form.component';
import { DoctorVendorTableComponent } from './doctor-vendor-table/doctor-vendor-table.component';
import { EditStpComponent } from './edit-stp/edit-stp.component';
import { EditDAComponentComponent } from './state-wise-da/edit-dacomponent/edit-dacomponent.component';
import { DoctorChemistLinkingComponent } from './doctor-chemist-linking/doctor-chemist-linking.component';
import { DoctorChemistReportComponent } from './doctor-chemist-report/doctor-chemist-report.component';
import { DoctorChemistSaleComponent } from './doctor-chemist-sale/doctor-chemist-sale.component';
import { EditProviderLocationComponent } from './edit-provider-location/edit-provider-location.component';
import { ProductGroupsComponent } from './product-groups/product-groups.component';
import { AssignProductToGroupComponent } from './assign-product-to-group/assign-product-to-group.component';
import { PatchCreationComponent } from './patch-creation/patch-creation.component';
import { EditPatchComponent } from './edit-patch/edit-patch.component';
import { UpdateHierarchyComponent } from './update-hierarchy/update-hierarchy.component';
import { SubGroupProductComponent } from './sub-group-product/sub-group-product.component';
import { EditProductSubGroupComponent } from './edit-product-sub-group/edit-product-sub-group.component';
import { MatInputModule } from '@angular/material/input';
import { TourProgramtargetComponent } from './tour-programtarget/tour-programtarget.component';
import { ChemistNotificationComponent } from './chemist-notification/chemist-notification.component';
import { AreaNotificationComponent } from './area-notification/area-notification.component';
import { DoctorNotificationComponent } from './doctor-notification/doctor-notification.component';

const routes: Routes = [
	{
		path: '',
		component: MasterComponent,
		children: [
			{
				path: '',
				redirectTo: 'usercreation',
				pathMatch: 'full'
			},{
				path: 'usercreation',
				component: UserCreationComponent
			}, {
				path: 'doctorVendor',
				component: DoctorVendorComponent
			}, {
				path: 'designations',
				component: DesignationComponent   //9900969918
			}, {
				path: 'district',
				component: DistrictDetailsComponent
			}, {
				path: 'createDistrict',
				component: CreatedistrictComponent
			}, {
				path: 'areaCreation',
				component: AreaCreationComponent
			}, {
				path: 'modifyHierarchy',
				component: HierarchyComponent
			}, {
				path: 'mrStockiestLinking',
				component: MrStockiestLinkingComponent
			}, {
				path: 'submitSSS',
				component: SssSubmissionComponent
			}, {
				path: 'submitClosing',
				component: SssClosingSubmissionComponent
			}, {
				path: 'giftcreation',
				component: GiftCreationComponent
			}, {
				path: 'productCreation',
				component: ProductCreationComponent
			}, {
				path: 'sampleIssue',
				component: SamplesIssueComponent
			}, {
				path: 'approvalMaster',
				component: ApprovalMasterComponent
			},{
				path: 'updateHierarchy',
				component:  UpdateHierarchyComponent
			},
			 {
				path: 'fareRateCard',
				component: FareRateCardComponent
			}, {
				path: 'addDA',
				component: StateWiseDAComponent
			}, {
				path: 'addSTP',
				component: StpComponent
			}, {
				path: 'stpApprove',
				component: StpApproveComponent
			}, {
				path: 'targetSubmit',
				component: TargetSubmissionComponent
			}, {
				path: 'stpDetails',
				component: STPDetailsComponent
			}, {
				path: 'sampleIssue',
				component: SamplesIssueComponent
			}, {
				path: 'TransferMasterData',
				component: TransferMasterDataComponent
			}, {
				path: 'ModifyTarget',
				component: ModifyTargetComponent
			}, {
				path: 'tourProgram',
				component: TourProgramComponent
			}, {
				path: 'LockOpen',
				component: LockOpenComponent
			}, {
				path: 'Holiday',
				component: HolidayComponent
			},{
				path: 'sampleAndGiftIssue',
				component: SamplesIssueComponent
			}, {
				path: 'sampleGiftModify',
				component: SampleGiftEditComponent
			}, {
				path: 'mefCreation',
				component: MefCreationComponent
			}, {
				path: 'addDcr',
				component: DcrSubmissionComponent
			},{
				path: 'editMefTracker',
				component: EditMefTrackerComponent
			},{
				path: 'providerCreation',
				component: ProviderCreationComponent
			},{
				path: 'modifyProvider',
				component: ModifyProviderComponent
			},{
				path: 'uploadData',
				component: UploadMasterDataComponent
			},{
				path: 'broadcastMessage',
				component: BroadcastMessageComponent
			},{
				path: 'calc',
				component: CalendarComponent
			},{
				path: 'providerLocationTagged',
				component: ProviderLocationTaggedComponent
			},{
				path: 'createDesignation',
				component: CreateDesignationComponent
			},
			{
				path: 'EditProvidersInBulk',
				component: ProviderModuleNewComponent
			},{
				path: 'submitSaleWiseSSS',
				component: SssSalesSubmissionComponent
			},{
				path: 'transferModule',
				component: TransferModuleComponent
			},{
				path: 'dayPlanLockOpen',
				component: DayPlanLockOpenComponent
			},
			{
				path: 'createDivision',
				component: CreateDivisionComponent
			},
			{
				path: 'editDailyAllowance',
				component: EditDAComponentComponent
			},
			{
				path: 'stpMigrate',
				component: STPMigrationOrTransferComponent
			},
			{
				path: 'doctorChemistLinking',
				component: DoctorChemistLinkingComponent
			},
			{
				path: 'doctorChemistReport',
				component: DoctorChemistReportComponent
			},
			{
				path: 'DoctorChemistSale',
				component: DoctorChemistSaleComponent
			},{
				path: 'userAccess',
				component: UserAccessComponent
			},
			{
				path: 'mgrinsvestments',
				component: MgrMonthlyInvestmentComponent
			},
			{
				path: 'productGroups',
				component: ProductGroupsComponent
			},
			{
				path:'PatchCreation',
				component:PatchCreationComponent
			},
			{
				path:'subGroup',
				component:SubGroupProductComponent
			},
			{
				path:'TourProgramtargetComponent',
				component:TourProgramtargetComponent
			}
			,
			{
				path:'AreaNotificationComponent',
				component:AreaNotificationComponent
			}
			,
			{
				path:'ChemistNotificationComponent',
				component:ChemistNotificationComponent
			}
			,
			{
				path:'DoctorNotificationComponent',
				component:DoctorNotificationComponent
			}


			
		]
	}
];
export function minlengthValidationMessage(err, field) {
	return `Should have atleast ${field.templateOptions.minLength} digit`;
}
export function maxlengthValidationMessage(err, field) {
	return `This value should be less than ${field.templateOptions.maxLength + 1} digit`;
}
export function minValidationMessage(err, field) {
	return `This value should be more than ${field.templateOptions.min}`;
}
export function maxValidationMessage(err, field) {
	return `This value should be less than ${field.templateOptions.max}`;
}

@NgModule({
	imports: [
		MatSlideToggleModule,
		MatFormFieldModule,
		MatChipsModule,
		ScrollDispatchModule,
		NgxMaterialTimepickerModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		MatDialogModule,
		CommonModule,
		HttpClientModule,
		PartialsModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		MatSliderModule,
		FiltersModule,
		FormlyBootstrapModule,
		//FormlyMaterialModule,	
		FormlyMaterialModule,
		NgSelectModule,
		MatStepperModule,
		CalendarModule,
		MatSlideToggleModule,
		MatFormFieldModule,
		FormlyModule.forRoot({
			types: [
				{ name: 'repeat', component: RepeatTypeComponent },
				{ name: 'ng-select', component: FormlyFieldNgSelect },
				{ name: 'repeats', component: RepeatTypeComponents },
				{ name: 'repeats', component: InvestmentRepeatTypeComponents },
			],
			validationMessages: [
				{ name: 'required', message: 'This field is required' },
				{ name: 'minlength', message: minlengthValidationMessage },
				{ name: 'maxlength', message: maxlengthValidationMessage },
				{ name: 'min', message: minValidationMessage },
				{ name: 'max', message: maxValidationMessage },
			],
		})
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],

	providers: [
		InterceptService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'm-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		HttpUtilsService,
		CustomersService,
		TypesUtilsService,
		LayoutUtilsService
	],
	entryComponents: [
		//EditStpComponent,
		DoctorVendorTableComponent,
		EditFareRateCardComponent,
		UserCreationComponent,
		EditGiftComponent,
		AreaEditDialogComponent,
		STPEditComponent,
		ModifyTourProgramComponent,
		EditProviderLocationComponent,
		AssignProductToGroupComponent,
		EditPatchComponent,
		EditProductSubGroupComponent
	],
	declarations: [
		MasterComponent,
		UserCreationComponent,
		DesignationComponent,
		DistrictDetailsComponent,
		CreatedistrictComponent,
		AreaCreationComponent,
		HierarchyComponent,
		SssSubmissionComponent,
		MrStockiestLinkingComponent,
		RepeatTypeComponent,
		RepeatTypeComponents,
		FormlyFieldNgSelect,
		InvestmentRepeatTypeComponents,
		ProductCreationComponent,
		EditGiftComponent,
		SssClosingSubmissionComponent,
		AreaEditDialogComponent,
		ApprovalMasterComponent,
		ProductCreationComponent,
		FareRateCardComponent,
		StateWiseDAComponent,
		StpComponent,
		StpApproveComponent,
		STPEditComponent,
		TargetSubmissionComponent, DcrSubmissionComponent, DcrPreviewComponent,
		STPDetailsComponent,
		STPEditComponent,
		TargetSubmissionComponent,
		TransferMasterDataComponent,
		TourProgramComponent,
		LockOpenComponent,
		HolidayComponent,
		SampleGiftEditComponent,
		MefCreationComponent,
		SamplesIssueComponent,
		EditMefTrackerComponent,
		ModifyTargetComponent,
		ProviderCreationComponent,
		ModifyProviderComponent,
		CompanyCreationComponent,
		CalendarComponent,
		TourProgramDetailsComponent,
		UploadMasterDataComponent,
		BroadcastMessageComponent,
		CalendarComponent,
		TourProgramDetailsComponent,
		ModifyTourProgramComponent,
		DoctorLocationTaggedComponent,
		ProviderLocationTaggedComponent,
		CreateDesignationComponent,
		ProviderModuleNewComponent,
		SssSalesSubmissionComponent,
		SssSalesSubmissionComponent,
		EditFareRateCardComponent,
		TransferModuleComponent,
		DayPlanLockOpenComponent,
		DoctorVendorComponent,
		CreateDivisionComponent,
		LockOpenNewComponent,
		ApllicationFormComponent,
		DoctorVendorTableComponent,
		// EditStpComponent,
		EditDAComponentComponent,
		STPMigrationOrTransferComponent,
		DoctorChemistLinkingComponent,
		DoctorChemistReportComponent,
		DoctorChemistSaleComponent,
		NestedListComponent,
		UserAccessComponent,
		MgrMonthlyInvestmentComponent,
		InvestmentRepeatTypeComponents,
		EditProviderLocationComponent,
		ProductGroupsComponent,
		AssignProductToGroupComponent,
		PatchCreationComponent,
		EditPatchComponent,
		UpdateHierarchyComponent,
		SubGroupProductComponent,
		EditProductSubGroupComponent,
		TourProgramtargetComponent,
		ChemistNotificationComponent,
		AreaNotificationComponent,
		DoctorNotificationComponent
	]
})
export class MasterModule { }
