import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaEditDialogComponent } from './area-edit-dialog.component';

describe('AreaEditDialogComponent', () => {
  let component: AreaEditDialogComponent;
  let fixture: ComponentFixture<AreaEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaEditDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
