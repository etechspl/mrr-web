import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { AreaService } from './../../../../../core/services/Area/area.service';

@Component({
  selector: 'm-area-edit-dialog',
  templateUrl: './area-edit-dialog.component.html',
  styleUrls: ['./area-edit-dialog.component.scss']
})
export class AreaEditDialogComponent implements OnInit {

  areaTypes = ['HQ', 'EX', 'OUT']
  editAreaForm: FormGroup;
  dataSource;
  currentUser = JSON.parse(sessionStorage.currentUser);

  constructor(public dialogRef: MatDialogRef<AreaEditDialogComponent>,private _toastrService: ToastrService,
    private areaService: AreaService,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private snackBar: MatSnackBar) {
    this.editAreaForm = this.fb.group({
      id: this.data.obj.areaId,
      areaName: [this.data.obj.areaName, Validators.required],
      areaCode: this.data.obj.areaCode,
      type: this.data.obj.type,
    })
  }

  ngOnInit() { }

  getErrorMessage() {
    return this.editAreaForm.hasError('required') ? 'Required field' : '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  saveEditedData() {
    this.areaService.editArea(this.editAreaForm.value, this.currentUser.company.id, this.currentUser.id).subscribe(areaRes => {
      this._toastrService.success('Edited Sucessfully', 'Edited');
    }, err => {
      console.log(err);
      this._toastrService.error('There is some error in area edition', 'Error');
    });
  }

  //-------------------------Allow only number----------------------
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
