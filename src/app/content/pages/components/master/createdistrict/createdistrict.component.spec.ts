import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatedistrictComponent } from './createdistrict.component';

describe('CreatedistrictComponent', () => {
  let component: CreatedistrictComponent;
  let fixture: ComponentFixture<CreatedistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatedistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatedistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
