import { DistrictService } from './../../../../../core/services/district.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'm-createdistrict',
  templateUrl: './createdistrict.component.html',
  styleUrls: ['./createdistrict.component.scss']
})
export class CreatedistrictComponent implements OnInit {
  districtForm: FormGroup;
  constructor(private fb: FormBuilder, public snackBar: MatSnackBar, private districtService: DistrictService) {
    this.createForm();
  }

  ngOnInit() {
  }
  check: boolean;
  currentUser = JSON.parse(sessionStorage.currentUser);

  createForm() {
    this.districtForm = this.fb.group({
      stateInfo: ['', Validators.required],
      districtName: ['', Validators.required]
    })
  }


  getStateValue(val) {
    this.districtForm.patchValue({ stateInfo: val });
  }

  createDistrict() {
    console.log('District Created Successfully');
    this.districtService.createDistrict(this.currentUser.companyId, this.districtForm.value).subscribe(res => {
      this.reset();
      this.openSnackBar('District Created Successfully', '');

    }, err => {
      if (err.error.error.statusCode === 409) {
        this.openSnackBar('District Already Exists, Please map the district', '');
      }

    })

  }
  reset() {
    this.createForm();
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

}
