import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDAComponentComponent } from './edit-dacomponent.component';

describe('EditDAComponentComponent', () => {
  let component: EditDAComponentComponent;
  let fixture: ComponentFixture<EditDAComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDAComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDAComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
