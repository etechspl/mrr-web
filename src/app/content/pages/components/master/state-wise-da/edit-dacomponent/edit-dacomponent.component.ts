import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DailyallowanceService } from '../../../../_core/services/dailyallowance.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'm-edit-dacomponent',
  templateUrl: './edit-dacomponent.component.html',
  styleUrls: ['./edit-dacomponent.component.scss']
}) 
export class EditDAComponentComponent implements OnInit {
 
  previousData;
  disableSaveBtn: Boolean = false;
  fgDA: FormGroup;
  constructor(
    private _toastr: ToastrService,
    private _editDA: DailyallowanceService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditDAComponentComponent>,
    @Inject(MAT_DIALOG_DATA) data,
  ) { 
    this.previousData = data;
   // console.log("previousDataeroor-----",this.previousData);
  }

  ngOnInit() {
    this.fgDA = this.fb.group({
      daHQ: (null),
      daEX: (null),
      daOUT: (null),
      mobileAllowance: (null),
      netAllowance: (null),
      specialAllowance: (null),
    });
    this.fgDA.patchValue({
      daHQ: this.previousData.daHQ,
      daEX: this.previousData.daEX,
      daOUT: this.previousData.daOUT,
      mobileAllowance: this.previousData.mobileAllowance,
      netAllowance: this.previousData.netAllowance,
      specialAllowance: this.previousData.specialAllowance,
    });
  }

  updateAndCloseDA(){
    this.disableSaveBtn = true;
    let obj = {
      id: this.previousData.id,
      companyId: this.previousData.companyId,
      daHQ: this.fgDA.value.daHQ,
      daEX: this.fgDA.value.daEX,
      daOUT: this.fgDA.value.daOUT,
      mobileAllowance: this.fgDA.value.mobileAllowance,
      netAllowance: this.fgDA.value.netAllowance,
      specialAllowance: this.fgDA.value.specialAllowance,
    
    };
    console.log("updated obj",obj);
    this._editDA.editDAAPrl(obj).subscribe(res => {
      if(res['count'] === 1){
        this._toastr.success('STP Updated Successfully !')
        this.disableSaveBtn = false;
        this.dialogRef.close(true);
      } else{
        this._toastr.error('Something went wrong !')
        this.disableSaveBtn = false;
        this.dialogRef.close(false);
      }
    });
  }

  closeModel(flag: any){
    this.dialogRef.close(false);
  }

}
