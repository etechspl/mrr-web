import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateWiseDAComponent } from './state-wise-da.component';

describe('StateWiseDAComponent', () => {
  let component: StateWiseDAComponent;
  let fixture: ComponentFixture<StateWiseDAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateWiseDAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateWiseDAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
