import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import {
	MatTableDataSource,
	MatTable,
	MatSort,
	MatPaginator,
} from "@angular/material";
import {
	FormGroup,
	FormBuilder,
	FormControl,
	Validators,
} from "@angular/forms";
import { StateComponent } from "../../filters/state/state.component";
import { DesignationComponent } from "../../filters/designation/designation.component";
import { DailyallowanceService } from "../../../_core/services/dailyallowance.service";
import { DivisionComponent } from "../../filters/division/division.component";
import { DACategoryComponent } from "../../filters/dacategory/dacategory.component";
import { DistrictComponent } from "../../filters/district/district.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { UserDetailsService } from "../../../../../core/services/user-details.service";
import { ToastrService } from "ngx-toastr";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { EditDAComponentComponent } from "./edit-dacomponent/edit-dacomponent.component";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import * as XLSX from "xlsx";
import Swal from 'sweetalert2'
@Component({
	selector: "m-state-wise-da",
	templateUrl: "./state-wise-da.component.html",
	styleUrls: ["./state-wise-da.component.scss"],
})
export class StateWiseDAComponent implements OnInit {
	constructor(
		private changeDetectRef: ChangeDetectorRef,
		private dialog: MatDialog,
		private _toastr: ToastrService,
		private fb: FormBuilder,
		private dailyAllowanceService: DailyallowanceService,
		private _userDetailService: UserDetailsService,
		private exportAsService: ExportAsService
	) { }
	exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementId: 'tableId1', // the id of html/table element
	}
	isLoading: boolean = false;
	showProcessing: boolean = false;
	check: boolean = true;
	isDivisionMultiple: boolean = false;

	currentUser = JSON.parse(sessionStorage.currentUser);
	companyId = this.currentUser.company.id;
	isDivisionExist = this.currentUser.company.isDivisionExist;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	userId = this.currentUser.userInfo[0].userId;
	daType: string = this.currentUser.company.expense.daType;
	daForm: FormGroup;
	placeholderSpecialAllowance: string = "Postage/Stationary/Frieght Allowances"

	data = [];
	count: number = 0;
	dialogConfig = new MatDialogConfig();
	dialogRef: any = null;
	isShowDivision: boolean;
	isShowCategory: boolean;
	isShowState: boolean;
	isShowDistrict: boolean;
	isShowDesignation: boolean;
	isShowEmployees: boolean;
	displayedColumns = [];
	dataSource = new MatTableDataSource<any>();

	formSubmitAttempt;
	@ViewChild(MatTable) table: MatTable<any>;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;

	@ViewChild(DACategoryComponent) daCategoryComponent: DACategoryComponent;
	@ViewChild(EmployeeComponent) employeeComponent: EmployeeComponent;
	@ViewChild(DivisionComponent) divisionComponent: DivisionComponent;
	@ViewChild(StateComponent) stateComponent: StateComponent;
	@ViewChild(DistrictComponent) districtComponent: DistrictComponent;
	@ViewChild("designationComponent") designationComponent: DesignationComponent;

	placeholder = "Select Category";
	ngOnInit() {
		if (this.isDivisionExist === true) {
			if (this.daType === "employee wise") {
				this.displayedColumns = [
					"sn",
					"division",
					"stateName",
					"district",
					"name",
					"designation",
					"hq_da",
					"ex_da",
					"out_da",
					"mobileAllowance",
					"netAllowance",
					"special_allowance",
					"editDLA",
					"deleteDLA"
				];
			} else if (this.daType === "category wise") {
				this.displayedColumns = [
					"sn",
					"division",
					"category",
					"hq_da",
					"ex_da",
					"out_da",
					"mobileAllowance",
					"netAllowance",
					"special_allowance",
					"editDLA",
					"deleteDLA"
				];
			} else if (this.daType === "Designation-State Wise") {
				this.displayedColumns = [
					"sn",
					"division",
					"stateName",
					"designation",
					"hq_da",
					"ex_da",
					"out_da",
					"mobileAllowance",
					"netAllowance",
					"special_allowance",
					"editDLA",
					"deleteDLA"
				];
			}
		} else {
			if (this.daType === "employee wise") {
				this.placeholderSpecialAllowance = "Enter Salary"
				this.displayedColumns = [
					"sn",
					"stateName",
					"district",
					"name",
					"designation",
					"hq_da",
					"ex_da",
					"out_da",
					"mobileAllowance",
					"netAllowance",
					"special_allowance",
					"editDLA",
					"deleteDLA"
				];
			} else if (this.daType === "category wise") {
				this.displayedColumns = [
					"sn",
					"category",
					"hq_da",
					"ex_da",
					"out_da",
					"mobileAllowance",
					"netAllowance",
					"special_allowance",
					"editDLA",
					"deleteDLA"
				];
			} else if (this.daType === "Designation-State Wise") {
				this.displayedColumns = [
					"sn",
					"stateName",
					"designation",
					"hq_da",
					"ex_da",
					"out_da",
					"mobileAllowance",
					"netAllowance",
					"special_allowance",
					"editDLA",
					"deleteDLA"
				];
			}
		}

		this.createForm();
		this.divisionDetails();
		this.getDailyAllowanceDetails();
		this.getDATypeDetails();
	}

	getDATypeDetails() {
		if (this.daType === "category wise") {
			this.isShowCategory = true;
			this.isShowState = false;
			this.isShowDistrict = false;
			this.isShowDesignation = false;
			this.isShowEmployees = false;
			this.daForm.setControl("category", new FormControl(this.daType));
			this.daForm.removeControl("stateId");
			this.daForm.removeControl("districtId");
			this.daForm.removeControl("designation");
			this.daForm.removeControl("userId");
		} else if (this.daType === "employee wise") {
			this.isShowCategory = false;
			this.isShowState = true;
			this.isShowDistrict = true;
			this.isShowDesignation = true;
			this.isShowEmployees = true;
			this.daForm.setControl("stateId", new FormControl());
			this.daForm.setControl("districtId", new FormControl());
			this.daForm.setControl("designation", new FormControl());
			this.daForm.setControl("userId", new FormControl());
			this.daForm.removeControl("category");
			this.stateComponent.setBlank();
		} else if (this.daType === "Designation-State Wise") {
			this.isShowState = true;
			this.isShowDesignation = true;
			this.daForm.setControl("stateId", new FormControl());
			this.daForm.setControl("designation", new FormControl());
		}
	}

	divisionDetails() {
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.daForm.setControl("divisionId", new FormControl());
			this.divisionComponent.getDivisions({
				companyId: this.companyId,
				designationLevel: this.designationLevel,
			});
		} else {
			this.daForm.removeControl("divisionId");
		}
	}

	//getDivision Values
	setDivisionValue($event) {
		this.daForm.patchValue({ divisionId: $event });
		this.stateComponent.getStateBasedOnDivision({
			companyId: this.companyId,
			division: [$event],
			isDivisionExist: this.isDivisionExist,
		});

		this.daForm.patchValue({ divisionId: $event });
		this.daCategoryComponent.getDACategory(this.currentUser.companyId, [
			this.daForm.value.divisionId,
		]);
	}

	setCategoryValue($event) {
		this.daForm.patchValue({ category: $event });
	}

	setDistrictValue($event) {
		this.daForm.patchValue({ districtId: $event });
		if (this.isDivisionExist === true) {
			this.designationComponent.getDesignationBasedOnDivision({
				companyId: this.companyId,
				userLevel: this.currentUser.userInfo[0].designationLevel,
				division: [this.daForm.value.divisionId],
				stateId: this.daForm.value.stateId,
				districtId: this.daForm.value.districtId,
			});
		} else {
			this.designationComponent.getUniqueDesignationOnStateOrDistrict({
				companyId: this.companyId,
				stateId: this.daForm.value.stateId,
				districtId: this.daForm.value.districtId,
				lookingFor: "onDistrict",
				userLevel: this.currentUser.userInfo[0].designationLevel,
				userId: this.currentUser.id,
			});
		}
	}
	createForm() {
		this.daForm = this.fb.group({
			type: new FormControl(this.daType, Validators.required),
			//stateId: new FormControl("", Validators.required),
			//designation: new FormControl("", Validators.required),
			daHQ: new FormControl("", Validators.required),
			daEX: new FormControl("", Validators.required),
			daOUT: new FormControl("", Validators.required),
			specialAllowance: new FormControl(),
			mobileAllowance: new FormControl(),
			netAllowance: new FormControl(),
		});
	}
	getStateValue(val: any) {
		this.daForm.patchValue({ stateId: val });

		if (val.length > 0 && this.isDivisionExist) {
			if (this.daType === "employee wise") {
				this.districtComponent.getDistrictsBasedOnDivision({
					designationLevel: this.designationLevel,
					companyId: this.companyId,
					stateId: this.daForm.value.stateId,
					isDivisionExist: this.isDivisionExist,
					division: [this.daForm.value.divisionId],
				});
			} else if (this.daType === "Designation-State Wise") {
				this.designationComponent.getDesignationBasedOnDivision({
					companyId: this.companyId,
					userLevel: this.currentUser.userInfo[0].designationLevel,
					division: [this.daForm.value.divisionId],
					stateId: this.daForm.value.stateId,
				});
			}
		} else {
			this.districtComponent.getDistricts(
				this.companyId,
				this.daForm.value.stateId,
				""
			);
		}
	}
	getDesignation(val: any) {
		this.daForm.patchValue({ designation: val });
		let obj = {};
		if (this.daForm.value.type === "employee wise") {
			obj["companyId"] = this.companyId;
			(obj["division"] = [this.daForm.value.divisionId]),
				(obj["status"] = [true]);
			obj["designationObject"] = this.daForm.value.designation;

			obj["stateId"] = this.daForm.value.stateId;
			obj["districtId"] = this.daForm.value.districtId;

			this.employeeComponent.getEmployeeListBasedOnDivision(obj);
		}
	}

	setEmployees($event) {
		this.daForm.patchValue({ userId: $event });
	}
	createDA() {
		if (this.daForm.valid) {
			let obj = {};
			if (this.daType === "category wise") {
				//obj["stateId"]="1";
				// obj["designation"]="1";

				//object for checking if da is exists
				let checkObj = {
					status: true,
					companyId: this.companyId,
					type: this.daType,
					category: this.daForm.value.category,
				};
				if (this.isDivisionExist === true) {
					(checkObj["isDivisionExist"] = this.isDivisionExist),
						(checkObj["divisionId"] = this.daForm.value.divisionId);
				}

				this.dailyAllowanceService
					.checkDataExists(checkObj)
					.subscribe((daExists) => {
						this.count = this.count + 1;
						if (daExists.length > 0) {
							this.createForm();
							this.divisionDetails();
							this.getDATypeDetails();
							this.setBlank();
							this.count = 0;
							this.changeDetectRef.detectChanges();
							this._toastr.info(
								"Data exists for the selected category",
								"Data exists"
							);
						} else {
							obj = this.daForm.value;
							obj["companyId"] = this.companyId;
							obj["createdBy"] = this.userId;

							this.data.push(obj);
							//inserting data
							this.dataInsertion(this.data);
						}
					});
			} else if (this.daType === "employee wise") {
				this._userDetailService
					.getUserFromUserInfo({ userId: this.daForm.value.userId })
					.subscribe((users) => {
						let checkCount = 0;
						for (const user of users) {
							//object for checking id da is exists
							let checkObj = {
								companyId: this.companyId,
								status: true,
								type: this.daType,
								userId: user.userId,
							};
							if (this.isDivisionExist === true) {
								(checkObj[
									"isDivisionExist"
								] = this.isDivisionExist),
									(checkObj[
										"divisionId"
									] = this.daForm.value.divisionId);
							}
							this.dailyAllowanceService
								.checkDataExists(checkObj)
								.subscribe((daExists) => {
									this.count = this.count + 1;
									if (daExists.length > 0) {
										checkCount = checkCount + 1;
										if (
											checkCount ===
											this.daForm.value.userId.length
										) {
											this.createForm();
											this.divisionDetails();
											this.getDATypeDetails();
											this.setBlank();
											this.count = 0;
											this.changeDetectRef.detectChanges();
											this._toastr.info(
												"Data exists for the selected User",
												"Data exists"
											);
										}
									} else {
										obj = {
											stateId: user.stateId,
											districtId: user.districtId,
											userId: user.userId,
											designation: user.designation,
											type: this.daForm.value.type,
											companyId: this.companyId,
											daHQ: this.daForm.value.daHQ,
											daEX: this.daForm.value.daEX,
											daOUT: this.daForm.value.daOUT,
											specialAllowance: this.daForm.value.specialAllowance,
											netAllowance: this.daForm.value.netAllowance,
											mobileAllowance: this.daForm.value.mobileAllowance,
											createdBy: this.userId,
										};
										if (this.isDivisionExist === true) {
											obj[
												"divisionId"
											] = this.daForm.value.divisionId;
										}
										this.data.push(obj);
										if (
											this.count ===
											this.daForm.value.userId.length
										) {
											this.dataInsertion(this.data);
										}
									}
								});
						}
					});
			} else if (this.daType === "Designation-State Wise") {
				//here i m taking the total number because total number of time loop will execute
				//based on that data will insert in daily allowance table.
				let totalNumber =
					this.daForm.value.stateId.length *
					this.daForm.value.designation.length;
				let checkCount = 0;
				for (const state of this.daForm.value.stateId) {
					for (const designation of this.daForm.value.designation) {
						let checkObj = {
							companyId: this.companyId,
							type: this.daType,
							status: true,
							designation: designation.designation,
							stateId: state,
						};
						if (this.isDivisionExist === true) {
							(checkObj[
								"isDivisionExist"
							] = this.isDivisionExist),
								(checkObj[
									"divisionId"
								] = this.daForm.value.divisionId);
						}
						this.dailyAllowanceService
							.checkDataExists(checkObj)
							.subscribe((daExists) => {
								this.count = this.count + 1;
								if (daExists.length > 0) {
									checkCount = checkCount + 1;
									if (checkCount === totalNumber) {
										this.createForm();
										this.divisionDetails();
										this.getDATypeDetails();
										this.setBlank();
										this.count = 0;
										this.changeDetectRef.detectChanges();

										this._toastr.info(
											"Data exists for the selected State & Designation",
											"Data exists"
										);
									}
								} else {
									let obj = {
										companyId: this.currentUser.companyId,
										stateId: state,
										designation: designation.designation,
										daHQ: this.daForm.value.daHQ,
										daEX: this.daForm.value.daEX,
										daOUT: this.daForm.value.daOUT,
										createdBy: this.currentUser.id,
										type: this.daType,
										netAllowance: this.daForm.value
											.netAllowance,
										mobileAllowance: this.daForm.value
											.mobileAllowance,
									};
									if (this.isDivisionExist === true) {
										obj[
											"divisionId"
										] = this.daForm.value.divisionId;
									}
									this.data.push(obj);

									if (totalNumber === this.count) {
										this.dataInsertion(this.data);
									}
								}
							});
					}
				}
			}
		}
	}

	setBlank() {
		this.divisionComponent.setBlank();
		this.daCategoryComponent.setBlank();
		this.districtComponent.setBlank();
		this.stateComponent.setBlank();
		this.employeeComponent.setBlank();
		this.designationComponent.setBlank();
	}

	dataInsertion(data) {
		this.dailyAllowanceService
			.createDailyAllowance(data)
			.subscribe((res) => {
				this.formSubmitAttempt = false;
				this.createForm();
				this.divisionDetails();
				this.getDATypeDetails();

				this.setBlank();

				this.getDailyAllowanceDetails();
				this.data = [];
				this.count = 0;
				this._toastr.success(
					"Data inserted Successfully",
					"Daily Allowance"
				);
			});
	}

	getDailyAllowanceDetails() {
		this.dailyAllowanceService
			.getDailyAllowanceDetails({
				companyId: this.companyId,
				type: this.daType
			})
			.subscribe((res) => {
				let array = res.map((item) => {
					return {
						...item,
					};
				});
				console.log("getDA DATA", array);
				this.dataSource = new MatTableDataSource(array);
				this.dataSource.sort = this.sort;
				this.dataSource.paginator = this.paginator;
			});
	}

	editDA(event, i, row) {
		this.dialogConfig.data = row;
		this.dialogRef === null
			? (this.dialogRef = this.dialog.open(
				EditDAComponentComponent,
				this.dialogConfig
			))
			: null;
		this.dialogRef.afterClosed().subscribe((res) => {
			console.log("Edit Da", res);
			if (res) {
				this.getDailyAllowanceDetails();
			}
			this.dialogRef = null;
		});
	}
	exporttoExcel() {
		// if (this.currentUser.companyId == '5cd162dd50ce3f0f80f64e14') {
		// 	let dataToExport = [];
		// 	dataToExport = this.dataSource.data.map((x) => ({
		// 		'Division': x.divisionMaster.divisionName,
		// 		//'State': x.state.stateName,
		// 		//'District': x.district.districtName,
		// 		'Name': x.userInfo.name,
		// 		'Designation': x.designation,
		// 		'Hq DA': x.daHQ,
		// 		'Ex DA': x.daEX,
		// 		'Out DA': x.daOUT,
		// 		'Mobile': x.mobileAllowance,
		// 		'Net Allounce': x.netAllowance,
		// 		'Special Allounce': x.specialAllowance
		// 	}));
		// 	const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		// 	const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		// 	XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		// 	XLSX.writeFile(workBook, "State Wise DA.xlsx");
		// } else {
			const lastCol = this.displayedColumns.pop();
			// download the file using old school javascript method
			setTimeout(() => {
				this.exportAsService
					.save(this.exportAsConfig, "state wise da")
					.subscribe(() => {
						// save started
						this.displayedColumns.push(lastCol);
					});
			}, 300);
		//}
	}
	// -----------------------------------------mahender sharma ----------------------------------------
	
	deleteDA(event, i, row) {
		let object = {
			where: {
				companyId: row.companyId,
				// divisionId:row.divisionId,
				_id :row.id,
				status: true
			}
		}
		this.dailyAllowanceService.getFareRateCard(object).subscribe(res => {
			if(res.length > 0){
				let update = {
					status: false,
					updatedAt: new Date(),
					updatedBy: this.currentUser.id
				}
				let object = {
					id: row.id,
					companyId: row.companyId
				}
				let msg = "Dally Allowance Card Will Be Deleted.";
				let buttonMsg = "Yes, Delete it !!!";
				const swalWithBootstrapButtons = Swal.mixin({
					customClass: {
						confirmButton: 'btn btn-success',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false,
				})
				swalWithBootstrapButtons.fire({
					title: 'Are you sure?',
					text: msg,
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: buttonMsg,
					cancelButtonText: 'No, cancel!',
					reverseButtons: true,
					allowOutsideClick: false
				}).then((result) => {
					if (result.value) {
						this.dailyAllowanceService.deleteFareRateCard(object, update).subscribe(res => {
							if (res) {
								this._toastr.success("Dally Allowance Card has been deleted successfully!!");
								this.getDailyAllowanceDetails();
							}
						});
					}
				})

			}

		});


	}
}
