import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorChemistReportComponent } from './doctor-chemist-report.component';

describe('DoctorChemistReportComponent', () => {
  let component: DoctorChemistReportComponent;
  let fixture: ComponentFixture<DoctorChemistReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorChemistReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorChemistReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
