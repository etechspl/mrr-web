import {
	Component,
	OnInit,
	ViewChild,
	ChangeDetectorRef,
	ElementRef,
} from "@angular/core";
import { DivisionComponent } from "../../filters/division/division.component";
import { DesignationComponent } from "../../filters/designation/designation.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { FormBuilder, FormGroup } from "@angular/forms";
import { DoctorChemistService } from "../../../../../core/services/DoctorChemist/doctor-chemist.service";
import { MatTableDataSource, MatPaginator } from "@angular/material";
import Swal from "sweetalert2";
import { URLService } from "../../../../../core/services/URL/url.service";
import * as XLSX from "xlsx";
import * as _moment from "moment";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
@Component({
	selector: "m-doctor-chemist-report",
	templateUrl: "./doctor-chemist-report.component.html",
	styleUrls: ["./doctor-chemist-report.component.scss"],
})
export class DoctorChemistReportComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);

	hideDivisionFilter = true;
	selectedDivision;
	loader = 3;
	data;
	userForm: FormGroup;
	preparedData;
	pareToBeDeleted;
	removable = true;
	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId1", // the id of html/table element
	};

	displayedColumns = ["index", "doctor", "chemists"];
	dataSource;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild("TABLE") table: ElementRef;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;

	constructor(
		private _fb: FormBuilder,
		private changeDetectorRef: ChangeDetectorRef,
		private _doctorChemistService: DoctorChemistService,
		private _urlService: URLService,
		private exportAsService: ExportAsService
	) {}

	ngOnInit() {
		this.hideDivisionFilter = this.currentUser.company.isDivisionExist
			? false
			: true;
		this.userForm = this._fb.group({
			userId: [null],
		});
	}

	getDivisionValue(event) {
		this.loader = 3;
		this.selectedDivision = event;
		this.callDesignationComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		this.userForm.patchValue({ userId: null });
		let passingObj = {
			companyId: this.currentUser.companyId,
			userId: this.currentUser.id,
			userLevel: this.currentUser.userInfo[0].designationLevel,
			division: event,
		};
		this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
	}

	getDesignation(event) {
		this.loader = 3;
		this.callEmployeeComponent.setBlank();
		this.userForm.patchValue({ userId: null });
		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					companyId: this.currentUser.companyId,
					designationObject: [event],
					status: [true],
					division: this.selectedDivision,
				};
				this.callEmployeeComponent.getEmployeeListBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.callEmployeeComponent.getEmployeeList(
					this.currentUser.companyId,
					[event],
					[true]
				);
			}
		} else {
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [event.designationLevel], //desig
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			} else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [event.designationLevel], //desig,
					isDivisionExist: true,
					division: this.selectedDivision,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}
		}
	}

	getEmployee(event) {
		this.loader = 0;
		this.userForm.patchValue({ userId: event });
		this.getLinkedProviders(event);
	}

	getLinkedProviders(userId) {
		this._doctorChemistService
			.getDoctorChemistPair(userId, this.currentUser.companyId)
			.subscribe((res) => {
				let content = [];
				this.preparedData = [];
				for (let i = 0; i < res.length; i++) {
					if (content.indexOf(res[i].providerId) == -1) {
						content.push(res[i].providerId);
						let obj = {
							providerId: res[i].providerId,
							doctorName: res[i].providerName,
							chemistList: [
								{
									name: res[i].providerLinkedName,
									providerLinkedId: res[i].providerLinkedId,
									id: res[i].id,
								},
							],
						};
						this.preparedData.push(obj);
					} else {
						for (let j = 0; j < this.preparedData.length; j++) {
							if (
								this.preparedData[j].providerId ===
								res[i].providerId
							) {
								let chemistName = {
									name: res[i].providerLinkedName,
									providerLinkedId: res[i].providerLinkedId,
									id: res[i].id,
								};
								this.preparedData[j].chemistList.push(
									chemistName
								);
							}
						}
					}
				}
				this.loader = 2;
				this.dataSource = new MatTableDataSource(this.preparedData);
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
				}, 100);
				// this.dataSource = this.preparedData;
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
				// this.data = res;
			});
	}

	remove(element, el): void {
		(Swal as any)
			.fire({
				title: `Are you sure to delete selected ${this.currentUser.company.lables.vendorLabel}`,
				// text: "You won't be able to revert this!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Yes, delete it!",
			})
			.then((res) => {
				if (res.value) {
					let id = el.id;
					this._doctorChemistService
						.deleteDoctorChemistPair(id)
						.subscribe((res) => {
							if (res && res.count) {
								this.getLinkedProviders(
									this.userForm.value.userId
								);
							}
						});
				}
			});
	}

	//   exporttoExcel() {
	// 		// download the file using old school javascript method
	// 		this.exportAsService
	// 			.save(this.exportAsConfig, "POB Details")
	// 			.subscribe(() => {
	// 				// save started
	// 			});
	// 	}
	exporttoExcel(): void {
		let dataToExport = this.dataSource.data.map((x) => ({
			Doctor: x.doctorName,
			Chemist: x.chemistList.map((chemist) => chemist.name).toString(),
		}));
		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "Linking Report.xlsx");
	}

	PrintTableFunction() {
		let printContents, popupWin;

		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;
		printContents = document.getElementById("tabledivId1").innerHTML;
		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;

		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;


			align:right;
	  
		  
	}
	
	
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  width:300px;
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  width:300px;
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4 landscape;
		max-height:100%;
		max-width:100%}
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
	  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
	
	  </div>
	
	  <div style="text-align: right;">
	  <h2 style="margin-top: 0px; margin-bottom: 5px">POB Report</h2>
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  ${printContents}</body>
	  </html>
	  `);
		// popupWin.document.close();
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
}
