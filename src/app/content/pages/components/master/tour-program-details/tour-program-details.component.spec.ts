import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourProgramDetailsComponent } from './tour-program-details.component';

describe('TourProgramDetailsComponent', () => {
  let component: TourProgramDetailsComponent;
  let fixture: ComponentFixture<TourProgramDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourProgramDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourProgramDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
