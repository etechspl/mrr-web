import { Component, OnInit, Input, ChangeDetectorRef, Inject } from '@angular/core';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import { ModifyTourProgramComponent } from '../modify-tour-program/modify-tour-program.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'm-tour-program-details',
  templateUrl: './tour-program-details.component.html',
  styleUrls: ['./tour-program-details.component.scss']
})
export class TourProgramDetailsComponent implements OnInit {

  @Input() myInput: any;
  @Input() urlPath: any;
  isShowSendForApproval: boolean;
  constructor(public dialog: MatDialog, private _changeDetectorRef: ChangeDetectorRef, private _tourProgramService: TourProgramService, private _toastr: ToastrService) { }

  ngOnInit() {

    this.getSubmittedTP(this.myInput.companyId, this.myInput.userId, parseInt(this.myInput.month), parseInt(this.myInput.year));
  }

  currentUser = JSON.parse(sessionStorage.currentUser);
  COMPANY_ID = this.currentUser.companyId;

  displayedColumns = ['actions', 'date', 'submittedActivity', 'apprvActivity', 'submittedDoctor', 'apprvDoctor', 'submittedChemist', 'approvedChemists', 'submittedRemark', 'apprvRemark'];
  dataSource;
  tpSubmittionDate = "-------";
  tpApprovalDate = "-------";
  tpApprovedBy = "-------";
  msgString;
  showMsgString = false;
  showTPDetails = false;
  isColor: string;
  getSubmittedTP(companyId: string, userId: string, month: number, year: number) {
    this._tourProgramService.viewTourProgram(companyId, userId, month, year).subscribe(res => {
      let array = [];
      array.push(res)
      //this.dataSourceForTPInDetail.push(res);
      if (array[0].length > 0) {
      
        this.dataSource = array[0];
        this.tpSubmittionDate = array[0][0].sendForApprovalDate;
        this.tpApprovalDate = array[0][0].approvedAt;
        this.tpApprovedBy = array[0][0].apporvedByName;
        this.showTPDetails = true;
          
        if (this.tpApprovedBy === '-------') {
          if(this.currentUser.company.validation.patchWiseReporting == true){
            this.displayedColumns = ['actions', 'date', 'submittedActivity', 'apprvActivity',  'submittedDoctor', 'apprvDoctor', 'submittedChemist', 'approvedChemists', 'submittedRemark', 'apprvRemark'];
          }else{
            this.displayedColumns = ['actions', 'date', 'submittedActivity', 'apprvActivity', 'submittedArea', 'approvedArea', 'submittedDoctor', 'apprvDoctor', 'submittedChemist', 'approvedChemists', 'submittedRemark', 'apprvRemark'];
          }
        } else {

          if(this.currentUser.company.validation.patchWiseReporting == true){
            
            this.displayedColumns = ['date', 'submittedActivity', 'apprvActivity', 'submittedDoctor', 'apprvDoctor', 'submittedChemist', 'approvedChemists', 'submittedRemark', 'apprvRemark'];
          }else{
            this.displayedColumns = ['date', 'submittedActivity', 'apprvActivity', 'submittedArea', 'approvedArea', 'submittedDoctor', 'apprvDoctor', 'submittedChemist', 'approvedChemists', 'submittedRemark', 'apprvRemark'];
          }
        }
      } else {
        this._toastr.info('For Selected Filter data not found', 'Data Not Found');
        this.dataSource = [];
        this.tpSubmittionDate = "-------";
        this.tpApprovalDate = "-------";
        this.tpApprovedBy = "-------";
        this.showTPDetails = false;
      }

      let numberOfDays = moment(year + "-" + month, "YYYY-MM").daysInMonth();
      
      

      if ((this.tpSubmittionDate === "-------" || this.tpSubmittionDate === "01-Jan-1900")&& this.tpApprovedBy === "-------" && numberOfDays === parseInt(this.dataSource.length) && this.urlPath.indexOf("tourProgram") > -1) {
        this.isShowSendForApproval = true;
        this.showMsgString = true;
        this.isColor = "red";
        this.msgString = "To send your tp for approval, click on button \"Send For Approval\"";
      } else if (this.urlPath.indexOf("tourProgram") > -1 && this.tpApprovedBy === "-------" && this.tpSubmittionDate === "-------") {
        this.showMsgString = true;
        this.isShowSendForApproval = false;
        this.isColor = "blue";
        this.msgString = "You will see the \"Send For Approval\" button after submitting the complete month TP ";
      } else {
        this.isShowSendForApproval = false;
        this.showMsgString = false;
        this.isColor = "";
      }
      this._changeDetectorRef.detectChanges();
    })
  }

  sendForApproval() {
    this._tourProgramService.updateTP({
      "companyId": this.myInput.companyId,
      "createdBy": this.myInput.userId,
      "month": parseInt(this.myInput.month),
      "year": parseInt(this.myInput.year)
    }).subscribe(res => {
      this.getSubmittedTP(this.myInput.companyId, this.myInput.userId, parseInt(this.myInput.month), parseInt(this.myInput.year));
      this._toastr.success("TP successfully send for approval.", "Send For Approval");
    })
  }
  editObject:any;
  editTP(data: any) {
    this.editObject = data;
    console.log("edit object=",this.editObject)
    const dialogRef = this.dialog.open(ModifyTourProgramComponent, {
      width: '85%',
      height: '90%',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      let month=['Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec']
      let dateSplit=[this.editObject.date.split("-")];
      let getMonth=dateSplit[0][1];

      let finalMonth=month.indexOf(getMonth)+1;
      let finalYear=parseInt(dateSplit[0][2])
    //  let month = moment(this.editObject.date).format("MM");
    //  let year = moment(this.editObject.date).format("YYYY");
   
  //    Math.abs(parseInt(month));
  //    Math.abs(parseInt(year));
   

      this.getSubmittedTP(this.COMPANY_ID, this.editObject.createdBy, finalMonth, finalYear)
      //this.animal = result;
    });

    console.log("Go for edit Tp Program ", data);

  }

  deleteTP(data: any) {
    // if TP is approved then can not be delete
    if (data.apporvedById === this.tpApprovedBy) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false,
      })
      swalWithBootstrapButtons.fire({
        title: 'Are you sure want to delete Tour Program for the day ' + data.date + ' ?',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true,
        allowOutsideClick: false
      }).then((result) => {
        if (result.value) {
          this._tourProgramService.deleteTP({
            id: data._id
          }).subscribe(tpData => {
            swalWithBootstrapButtons.fire({
              title: 'Tour Program deleted successfully!!!',
              text: 'Tour Program for the day ' + data.date + ' has been deleted successfully',
              type: 'success',
              allowOutsideClick: false
            }).then((result2) => {
              this.getSubmittedTP(this.myInput.companyId, this.myInput.userId, parseInt(this.myInput.month), parseInt(this.myInput.year));
            });
          }, err => {
            Swal.fire('Oops...', err.error.error.message, 'error');
            console.log("Error while deleting...");
            this._changeDetectorRef.detectChanges();
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Area was not deleted.',
            'error'
          )
          this._changeDetectorRef.detectChanges();
        }
      });
    }
  }
}
