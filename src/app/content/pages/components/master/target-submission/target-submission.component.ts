import { MonthComponent } from "../../filters/month/month.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { DistrictComponent } from "../../filters/district/district.component";
import { StockiestService } from "../../../../../core/services/Stockiest/stockiest.service";
import { TargetService } from "../../../../../core/services/Target/target.service";
import { UserDetailsService } from "../../../../../core/services/user-details.service";
import {
	FormGroup,
	FormControl,
	Validators,
	FormBuilder,
	FormArray,
} from "@angular/forms";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { StateComponent } from "../../filters/state/state.component";
import { MatSlideToggleChange } from "@angular/material";
import { YearComponent } from "../../filters/year/year.component";

@Component({
	selector: "m-target-submission",
	templateUrl: "./target-submission.component.html",
	styleUrls: ["./target-submission.component.scss"],
})
export class TargetSubmissionComponent implements OnInit {
	isToggled = true;
	checkedVarEmp = false;
	checkedVarHq = false;
	checkedVarQuarterly = false;
	checkedVarMonthly = false;
	checkedVarYearly = false;
	checkedVarProduct = true;
	checkedVarAmount = false;
	showEmpReportType = true;
	showHqReportType = true;
	showMonthFilter = false;
	showYearFilter = true;
	showFinancialFilter = false;
	typeFilter = false;
	stateFilter = false;
	districtFilter = false;
	employeeFilter = false;
	yearlySubmitType = false;
	targetType = false;
	quarterly = true;
	isShowTargetDetailProductwise = false;
	isShowTargetDetailAmountwise = false;
	isShowTargetDetail = false;
	showViewReport = false;
	public showReport = false;
	public showState: boolean = true;
	firstQuarter = false;
	secondQuarter = false;
	thirdQuarter = false;
	fourthQuarter = false;
	yearlyTarget = false;
	districtFilterForMulti = false;
	public showDistrict: boolean = false;
	groupSelectField = false;
	groupName = false;
	Quarters = [
		{ value: "1", viewValue: "Jan-Mar" },
		{ value: "2", viewValue: "Apr-June" },
		{ value: "3", viewValue: "July-Sep" },
		{ value: "4", viewValue: "Oct-Dec" },
	];
	Groups = [
		{ value: "All", viewValue: "All Products" },
		{ value: "groupwise", viewValue: "Group-wise" },
	];

	GroupName = [];
	displayedColumns = [
		"productName",
		"productRate",
		"opening",
		"totalPrimaryQty",
		"totalPrimaryAmt",
		"totalSaleReturnQty",
		"totalSaleReturnAmt",
		"secondarySaleQty",
		"secondarySaleValue",
		"totalStock",
		"totalStockValue",
		"closing",
		"closingvalue",
	];
	displayedColumns1 = [
		"productName" /*, 'productRate'*/,
		"jan",
		"feb",
		"march",
		"april",
		"may",
		"june",
		"july",
		"aug",
		"sep",
		"oct",
		"nov",
		"dec",
	];
	displayedColumns2 = [
		"hqName" /*, 'productRate'*/,
		"jan",
		"feb",
		"march",
		"april",
		"may",
		"june",
		"july",
		"aug",
		"sep",
		"oct",
		"nov",
		"dec",
	];
	dataSource;

	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callDistrictComponentAgainForMulti")
	callDistrictComponentAgainForMulti: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callYearComponent") callYearComponent: YearComponent;

	constructor(
		private fb: FormBuilder,
		private _toastr: ToastrService,
		private stockiestService: StockiestService,
		private _targetService: TargetService,
		private _changeDetectorRef: ChangeDetectorRef,
		private _userDetailservice: UserDetailsService
	) {}

	ngOnInit() {
		if (
			this.currentUser.company.target.targetInfo == "Employeewise" ||
			this.currentUser.company.target.targetInfo == "all"
		) {
			this.createTargetFormEmployeewise(this.fb);
			this.checkedVarEmp = true;
			this.checkedVarHq = false;
			this.reportType("Employeewise");
		} else {
			this.createTargetFormHeadquarterwise(this.fb);
			this.checkedVarEmp = false;
			this.checkedVarHq = true;
			this.reportType(this.currentUser.company.target.targetInfo);
		}

		if (
			this.currentUser.company.target.targetDuration == "Quarterly" ||
			this.currentUser.company.target.targetDuration == "all"
		) {
			this.checkedVarQuarterly = true;
			this.checkedVarMonthly = false;
			this.checkedVarYearly = false;
			this.yearlyType("Quarterly");
		} else if (
			this.currentUser.company.target.targetDuration == "Monthly"
		) {
			this.checkedVarQuarterly = false;
			this.checkedVarMonthly = true;
			this.checkedVarYearly = false;
			this.yearlyType(this.currentUser.company.target.targetDuration);
		} else if (this.currentUser.company.target.targetDuration == "Yearly") {
			this.checkedVarQuarterly = false;
			this.checkedVarMonthly = false;
			this.checkedVarYearly = true;
			this.yearlyType(this.currentUser.company.target.targetDuration);
		}

		if (
			this.currentUser.company.target.targetType == "productwise" ||
			this.currentUser.company.target.targetType == "all"
		) {
			this.checkedVarProduct = true;
			this.checkedVarAmount = false;
			this.targetSubmitType("productwise");
		} else {
			this.checkedVarProduct = false;
			this.checkedVarAmount = true;
			this.targetSubmitType(this.currentUser.company.target.targetType);
		}
		// if(this.currentUser.companyId==='6099c30fb797a8103c114580'){
		// 	this.callYearComponent.
		// }
	}

	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}

	currentUser = JSON.parse(sessionStorage.currentUser);
	public target: FormGroup;
	createTargetFormEmployeewise(fb: FormBuilder) {
		this.target = fb.group({
			reportType: new FormControl(null, Validators.required),
			yearlyType: new FormControl(null, Validators.required),
			targetSubmitType: new FormControl(null, Validators.required),
			stateInfo: new FormControl(null, Validators.required),
			districtInfo: new FormControl(null, Validators.required),
			userInfo: new FormControl(null),
			year: new FormControl(null, Validators.required),
			quarterType: new FormControl(null),
			groupName: new FormControl(null),
			selectedGroupName: new FormControl(null),
		});
	}
	createTargetFormHeadquarterwise(fb: FormBuilder) {
		this.target = fb.group({
			reportType: new FormControl(null, Validators.required),
			yearlyType: new FormControl(null, Validators.required),
			targetSubmitType: new FormControl(null, Validators.required),
			stateInfo: new FormControl(null, Validators.required),
			districtInfo: new FormControl(null, Validators.required),
			userInfo: new FormControl(null),
			year: new FormControl(null, Validators.required),
			quarterType: new FormControl(null),
			groupName: new FormControl(null),
			selectedGroupName: new FormControl(null),
		});
	}

	target1 = new FormGroup({
		reportType: new FormControl(null, Validators.required),
		yearlyType: new FormControl(null, Validators.required),
		targetSubmitType: new FormControl(null, Validators.required),
		stateInfo: new FormControl(null, Validators.required),
		districtInfo: new FormControl(null, Validators.required),
		userInfo: new FormControl(null),
		year: new FormControl(null, Validators.required),
		quarterType: new FormControl(null),
	});

	target2 = new FormGroup({
		reportType: new FormControl(null, Validators.required),
		yearlyType: new FormControl(null, Validators.required),
		targetSubmitType: new FormControl(null, Validators.required),
		stateInfo: new FormControl(null, Validators.required),
		districtInfo: new FormControl(null, Validators.required),
		userInfo: new FormControl(null),
		year: new FormControl(null, Validators.required),
		quarterType: new FormControl(null),
	});

	productListForSingle = [];
	targetSubmitFormDetail(productArr) {
		this.productListForSingle = productArr;
		this.target1 = this.fb.group({
			productName: [[Validators.required]],
			productId: [],
			Jan: [],
			Feb: [],
			March: [],
			April: [],
			May: [],
			June: [],
			July: [],
			Aug: [],
			Sep: [],
			Oct: [],
			Nov: [],
			Dec: [],
			quantity: [],
			resultList: new FormArray([]),
		});

		const productFGs = this.productListForSingle.map((product) => {
			return this.fb.group({
				productName: [product.productName],
				productId: [product.id],
				Jan: [product.Jan],
				Feb: [product.Feb],
				March: [product.March],
				April: [product.April],
				May: [product.May],
				June: [product.June],
				July: [product.July],
				Aug: [product.Aug],
				Sep: [product.Sep],
				Oct: [product.Oct],
				Nov: [product.Nov],
				Dec: [product.Dec],
				quantity: [product.quantity],
			});
		});
		const productFormArray: FormArray = this.fb.array(productFGs);
		this.target1.setControl("resultList", productFormArray);
	}

	productListForSingleAmountwise = [];
	targetSubmitFormDetailAmountwise(productArr) {
		this.productListForSingleAmountwise = productArr;
		this.target2 = this.fb.group({
			Jan: [],
			Feb: [],
			March: [],
			April: [],
			May: [],
			June: [],
			July: [],
			Aug: [],
			Sep: [],
			Oct: [],
			Nov: [],
			Dec: [],
			quantity: [],
			resultList: new FormArray([]),
		});

		const productFGs1 = this.productListForSingleAmountwise.map(
			(product1) => {
				return this.fb.group({
					Jan: [product1.Jan],
					Feb: [product1.Feb],
					March: [product1.March],
					April: [product1.April],
					May: [product1.May],
					June: [product1.June],
					July: [product1.July],
					Aug: [product1.Aug],
					Sep: [product1.Sep],
					Oct: [product1.Oct],
					Nov: [product1.Nov],
					Dec: [product1.Dec],
					quantity: [product1.quantity],
				});
			}
		);
		const productFormArray1: FormArray = this.fb.array(productFGs1);
		this.target2.setControl("resultList1", productFormArray1);
	}

	reportType(val) {
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		this.target.patchValue({ reportType: val });
		this.target.patchValue({ stateInfo: null });
		this.target.patchValue({ districtInfo: null });
		this.target.patchValue({ userInfo: null });
		this.userData = [];
		this.callStateComponent.setBlank();
		this.callDistrictComponentAgainForMulti
			? this.callDistrictComponentAgainForMulti.setBlank()
			: null;
		this.callDistrictComponentAgainForMulti
			? this.callDistrictComponentAgainForMulti.removeList()
			: null;
		this.callDistrictComponent
			? this.callDistrictComponent.setBlank()
			: null;
		this.callDistrictComponent
			? this.callDistrictComponent.removeList()
			: null;
		if (val === "Employeewise") {
			// this.createTargetFormEmployeewise(this.fb);
			this.target.get("userInfo").setValidators([Validators.required]);
			if (this.currentUser.userInfo[0].rL === 0) {
				this.typeFilter = true;
				this.stateFilter = true;
				this.districtFilter = true;
				this.employeeFilter = true;
				this.yearlySubmitType = true;
				this.showMonthFilter = false;
				this.targetType = true;
				this.districtFilterForMulti = false;
			} else if (this.currentUser.userInfo[0].rL > 1) {
				this.typeFilter = false;
				this.stateFilter = true;
				this.districtFilter = false;
				this.employeeFilter = true;
				this.yearlySubmitType = true;
				this.showMonthFilter = false;
				this.targetType = true;
				this.districtFilterForMulti = false;
			}
		} else if (val === "Headquarterwise") {
			// this.createTargetFormHeadquarterwise(this.fb);
			this.target.get("userInfo").clearValidators();
			if (this.currentUser.userInfo[0].rL === 0) {
				this.typeFilter = true;
				this.stateFilter = true;
				this.districtFilter = false;
				this.employeeFilter = false;
				this.yearlySubmitType = true;
				this.showMonthFilter = false;
				this.targetType = true;
				this.districtFilterForMulti = true;
			} else if (this.currentUser.userInfo[0].rL > 1) {
				this.typeFilter = true;
				this.stateFilter = false;
				this.districtFilter = false;
				this.employeeFilter = false;
				this.yearlySubmitType = true;
				this.showMonthFilter = false;
				this.targetType = true;
				this.districtFilterForMulti = true;
			}
		}
		this.target.patchValue({ reportType: val });
	}

	setEmployeeDetail(val) {
		this.isShowTargetDetail = false;
		this.target.patchValue({ userInfo: val });
	}

	yearlyType(val) {
		this.showYearFilter = false;
		this.quarterly = false;
		this.firstQuarter = false;
		this.secondQuarter = false;
		this.thirdQuarter = false;
		this.fourthQuarter = false;
		this.yearlyTarget = false;
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		this.target.patchValue({ yearlyType: val });
		this.target.patchValue({ year: null });
		this.target.patchValue({ quarterType: null });
		this.callYearComponent.setBlank();
		if (val === "Quarterly") {
			this.showYearFilter = true;
			this.quarterly = true;
			this.yearlyTarget = false;
		} else if (val === "Monthly") {
			this.showYearFilter = true;
			this.quarterly = false;
			this.firstQuarter = true;
			this.secondQuarter = true;
			this.thirdQuarter = true;
			this.fourthQuarter = true;
			this.yearlyTarget = false;
		} else if (val === "Yearly") {
			this.showYearFilter = true;
			this.quarterly = false;
			this.firstQuarter = false;
			this.secondQuarter = false;
			this.thirdQuarter = false;
			this.fourthQuarter = false;
			this.yearlyTarget = true;
		}
	}

	targetSubmitType(val) {
		this.groupSelectField = false;
		this.groupName = false;
		this.isShowTargetDetail = false;
		this.isShowTargetDetailProductwise = false;
		this.isShowTargetDetailAmountwise = false;
		this.showViewReport = false;
		this.showViewReportamountwise = false;
		this.target.patchValue({ targetSubmitType: val });
		this.target.patchValue({ groupName: null });
		this.target.patchValue({ selectedGroupName: null });
		if (val == "productwise") {
			this.groupSelectField = true;
		}
	}

	getStateValue(val) {
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		this.showReport = false;
		let state = [];
		state.push(val);
		this.target.patchValue({ stateInfo: state });
		this.target.patchValue({ districtInfo: null });
		this.target.patchValue({ userInfo: null });
		this.userData = [];

		this.callDistrictComponent
			? this.callDistrictComponent.setBlank()
			: null;
		this.callDistrictComponent
			? this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
			  )
			: null;
		if (this.target.value.reportType == "Headquarterwise") {
			this.callDistrictComponentAgainForMulti.setBlank();
			this.callDistrictComponentAgainForMulti.getDistricts(
				this.currentUser.companyId,
				val,
				true
			);
		}
	}
	userData = [];
	getDistrictValue(val) {
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		this.showReport = false;
		let district = [];
		if (this.target.value.reportType == "Employeewise") {
			district.push(val);
			this.target.patchValue({ districtInfo: district });
		} else if (this.target.value.reportType == "Headquarterwise") {
			for (let i = 0; i < val.length; i++) {
				district.push(val[i]);
			}
			this.target.patchValue({ districtInfo: district });
		}
		//this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, district);
		this._userDetailservice
			.getEmployeeLinkValue(this.currentUser.companyId, district)
			.subscribe(
				(res) => {
					this.userData = res;
				},
				(err) => {
					console.log(err);
					alert("Oooops! \nNo Record Found");
				}
			);
	}

	getMonth(val) {
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		this.showReport = false;
		this.target.patchValue({ month: val });
	}

	getYear(val) {
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		this.showReport = false;
		this.target.patchValue({ year: val });
	}

	getQuarter(val) {
		this.firstQuarter = false;
		this.secondQuarter = false;
		this.thirdQuarter = false;
		this.fourthQuarter = false;
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		if (val == "1") {
			this.firstQuarter = true;
			this.secondQuarter = false;
			this.thirdQuarter = false;
			this.fourthQuarter = false;
		} else if (val == "2") {
			this.secondQuarter = true;
			this.firstQuarter = false;
			this.thirdQuarter = false;
			this.fourthQuarter = false;
		} else if (val == "3") {
			this.firstQuarter = false;
			this.secondQuarter = false;
			this.thirdQuarter = true;
			this.fourthQuarter = false;
		} else if (val == "4") {
			this.firstQuarter = false;
			this.secondQuarter = false;
			this.thirdQuarter = false;
			this.fourthQuarter = true;
		}
		this.target.patchValue({ quarterType: val });
	}

	getGroupProduct(val) {
		this.groupName = false;
		this.isShowTargetDetail = false;
		this.isShowTargetDetailAmountwise = false;
		this.isShowTargetDetailProductwise = false;
		this.target.patchValue({ groupName: val });
		if (val == "groupwise") {
			this.groupName = true;
			this._targetService.getGroup(this.currentUser.companyId).subscribe(
				(getGroup) => {
					if (getGroup.length > 0) {
						this.GroupName = getGroup;
						this.isShowTargetDetail = false;
						this.isShowTargetDetailAmountwise = false;
						this.isShowTargetDetailProductwise = false;
						this._changeDetectorRef.detectChanges();
					} else {
						this._changeDetectorRef.detectChanges();
						this._toastr.success(
							"Please select another details",
							"No Groups Found for selected Details !!"
						);
					}
				},
				(err) => {}
			);
		}
	}

	getGroup(val) {
		this.target.patchValue({ selectedGroupName: val });
	}

	/*getManagerFlag(val){
  }*/

	private productResultArr1 = ["1"];
	viewTargetDetail() {
		// ---------by nipun (10-Apr-2020) to check validation (which control is causing form to be invalid)
		// Object.keys(this.target.controls).forEach((i)=>{
		//   console.log(`%c${i}`,"font-size: 15px;color: #4285f4;font-weight: 700;")
		//   console.log(`%cValue    : %c${this.target.controls[''+i].value} `,"font-size: 15px;color: #00ab4d;font-weight: 700;","font-size: 15px;color: darkcyan;font-weight: 700;border-bottom: 5px solid #ff7a7a;");
		//   console.log(`%cValidity : %c${this.target.controls[''+i].valid} `,"font-size: 15px;color: #fbf504;font-weight: 700;","font-size: 15px;color: darkcyan;font-weight: 700;border-bottom: 5px solid #ff7a7a;");
		//   console.log(`%cErrorrs  : %c${this.target.controls[''+i].errors} `,"font-size: 15px;color: #f95f5f;font-weight: 700;","font-size: 15px;color: darkcyan;font-weight: 700;border-bottom: 5px solid #ff7a7a;");
		// });
		// ---------by nipun (10-Apr-2020) to check validation (which control is causing form to be invalid)

		if (this.target.value.targetSubmitType == "productwise") {
			if (this.target.value.groupName == "groupwise") {
				this._targetService
					.getProductBasedOnStateForTargetModule(
						this.currentUser.companyId,
						this.target.value.stateInfo,
						this.target.value.selectedGroupName,
						1
					)
					.subscribe(
						(getProduct) => {
							if (getProduct.length > 0) {
								this.targetSubmitFormDetail(getProduct);
								this.isShowTargetDetail = true;
								this.isShowTargetDetailAmountwise = false;
								this.isShowTargetDetailProductwise = true;
								this.isToggled = false;
								this._changeDetectorRef.detectChanges();
							} else {
								this._changeDetectorRef.detectChanges();
								this._toastr.success(
									"Please select another details",
									"No Products Found for selected Details !!"
								);
							}
						},
						(err) => {}
					);
			} else {
				this._targetService
					.getProductBasedOnStateForTargetModule(
						this.currentUser.companyId,
						this.target.value.stateInfo,
						this.target.value.selectedGroupName,
						0
					)
					.subscribe(
						(getProduct) => {
							if (getProduct.length > 0) {
								this.targetSubmitFormDetail(getProduct);
								this.isShowTargetDetail = true;
								this.isShowTargetDetailAmountwise = false;
								this.isShowTargetDetailProductwise = true;
								this.isToggled = false;
								this._changeDetectorRef.detectChanges();
							} else {
								this._changeDetectorRef.detectChanges();
								this._toastr.success(
									"Please select another details",
									"No Products Found for selected Details !!"
								);
							}
						},
						(err) => {}
					);
			}
		} else if (this.target.value.targetSubmitType == "amountwise") {
			this.targetSubmitFormDetailAmountwise(this.productResultArr1);
			this.isShowTargetDetail = true;
			this.isShowTargetDetailProductwise = false;
			this.isShowTargetDetailAmountwise = true;
			this.isToggled = false;
			this._changeDetectorRef.detectChanges();
		}
	}
	showViewReportamountwise = false;
	submitTargetProductwise() {
		this._targetService
			.createTarget(
				this.currentUser.companyId,
				this.target.value,
				this.target1.value.resultList,
				this.currentUser.id
			)
			.subscribe(
				(target) => {
					if (target.length > 0) {
						if (this.target.value.reportType == "Employeewise") {
							this._toastr.error(
								`Target Already Submitted for User :  ${target[0].alreadySubmitUser}`
							);
						} else if (
							this.target.value.reportType == "Headquarterwise"
						) {
							this._toastr.error(
								`Target Already Submitted for Heaquarters : ${target[0].alreadySubmitDistrict}`
							);
						}
						this.isShowTargetDetail = false;
						this.isShowTargetDetailAmountwise = false;
						this.isShowTargetDetailProductwise = false;
						this._changeDetectorRef.detectChanges();
					} else {
						this._toastr.success(
							"",
							"Productwise Target Submit Successfully"
						);
						this.isShowTargetDetail = false;
						this.isShowTargetDetailAmountwise = false;
						this.isShowTargetDetailProductwise = false;
						this._changeDetectorRef.detectChanges();
					}
				},
				(err) => {}
			);
	}
	submitTargetAmountwise() {
		this._targetService
			.createTarget(
				this.currentUser.companyId,
				this.target.value,
				this.target2.value.resultList1,
				this.currentUser.id
			)
			.subscribe(
				(target) => {
					if (target.length > 0) {
						if (this.target.value.reportType == "Employeewise") {
							this._toastr.error(
								`Target Already Submitted for User :  ${target[0].alreadySubmitUser} `
							);
						} else if (
							this.target.value.reportType == "Headquarterwise"
						) {
							this._toastr.error(
								`Target Already Submitted for Heaquarters :  ${target[0].alreadySubmitDistrict} `
							);
						}
						this.isShowTargetDetail = false;
						this.isShowTargetDetailAmountwise = false;
						this.isShowTargetDetailProductwise = false;
						this._changeDetectorRef.detectChanges();
					} else {
						this._toastr.success(
							"",
							"Amountwise Target Submit Successfully"
						);
						this.isShowTargetDetail = false;
						this.isShowTargetDetailAmountwise = false;
						this.isShowTargetDetailProductwise = false;
						this._changeDetectorRef.detectChanges();
					}
				},
				(err) => {}
			);
	}
}
