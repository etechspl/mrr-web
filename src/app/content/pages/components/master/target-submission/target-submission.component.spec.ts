import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetSubmissionComponent } from './target-submission.component';

describe('TargetSubmissionComponent', () => {
  let component: TargetSubmissionComponent;
  let fixture: ComponentFixture<TargetSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
