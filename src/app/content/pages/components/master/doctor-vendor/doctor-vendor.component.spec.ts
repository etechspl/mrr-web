import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorVendorComponent } from './doctor-vendor.component';

describe('DoctorVendorComponent', () => {
  let component: DoctorVendorComponent;
  let fixture: ComponentFixture<DoctorVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
