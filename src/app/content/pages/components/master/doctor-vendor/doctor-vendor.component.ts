import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { DistrictComponent } from "../../filters/district/district.component";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { DoctorVendorTableComponent } from "../doctor-vendor-table/doctor-vendor-table.component";
import { ToastrService } from "ngx-toastr";
import { StateComponent } from "../../filters/state/state.component";
import { DivisionComponent } from "../../filters/division/division.component";

@Component({
	selector: "m-doctor-vendor",
	templateUrl: "./doctor-vendor.component.html",
	styleUrls: ["./doctor-vendor.component.scss"]
})
export class DoctorVendorComponent implements OnInit {
	showTable: boolean = true;
	selectedStateId: any;
	selectedDistrictId: any = null;
	doctorVendorDataTable: any;
	areaIds = [];
	doctorsListSearching = [];
	doctorsList = [];
	chemistList = [];
	stockistList = [];
	doctorsLabel;
	chemistLabel;
	stockistLabel;
	employeeSelected;
	showLoader = false;
	showNoData = false;
	showDesignation = false;
	showEmployeeList = false;
	currentUser = JSON.parse(sessionStorage.currentUser);
	displayedColumns = [
		"providerName",
		"category",
		"chemist",
		"stockist",
		"Assign"
	];

	dialogRef: any = null;
	dialogConfig = new MatDialogConfig();
	currentUserLabels;

	constructor(
		private toasterService: ToastrService,
		private providerService: ProviderService,
		private changeDetectorRef: ChangeDetectorRef,
		private dialog: MatDialog
	) {}

	@ViewChild("division") divisionSelector: DivisionComponent;
	@ViewChild("state") stateSelector: StateComponent;
	@ViewChild("getDistricts") getDistricts: DistrictComponent;
	@ViewChild("employeeControl") _employeeComponent: EmployeeComponent;

	ngOnInit() {
		(this.currentUser.designation !== 'MR')?this.getDivisionsList():this.getDoctorListForMr();
		this.currentUserLabels = {
			doctorLabel: this.doctorsLabel = this.currentUser.company.lables.doctorLabel,
			chemistLabel: this.chemistLabel = this.currentUser.company.lables.vendorLabel,
			stockistLabel: this.stockistLabel = this.currentUser.company.lables.stockistLabel
		};
	}

	getDoctorListForMr(){
		this.showLoader = true;
		this.employeeSelected = this.currentUser.id;
		this.getList();
	}

	getDivisionsList() {
		let obj = {
			designationLevel: this.currentUser.designationLevel,
			companyId: this.currentUser.companyId
		};
		(this.currentUser.designationLevel > 1)? (obj["supervisorId"] = this.currentUser.supervisorId): null;
		this.divisionSelector.getDivisions(obj);
	}

	getDivision(event) {
		console.log("++++++++", event);
		let obj = {
			division: event
		};
		this.stateSelector.getStateBasedOnDivision(obj);
	}

	getState(event) {
		this.showDesignation = false;
		this.showEmployeeList = false;
		this.selectedDistrictId = null;
		this.selectedStateId = event;
		this.employeeSelected = null;
		this.getDistricts.getDistricts(this.currentUser.companyId, event, true);
	}

	getDistrict(event) {
		this.selectedDistrictId = event;
		this.showDesignation = true;
		this.showEmployeeList = false;
		this.employeeSelected = null;
	}

	getDesignation(event) {
		this._employeeComponent.getEmployeeDesignatiuonWise(
			this.currentUser.companyId,
			this.selectedDistrictId,
			event.designationLevel,
			true
		);
		this.employeeSelected = null;
		this.showEmployeeList = true;
	}
	getEmployee(event) {
		this.employeeSelected = event;
	}

	getList() {
		let obj = {
			where: {
				userId: this.employeeSelected
			}
		};
		this.providerService.getAreaIdsAsPerEmployeeId(obj).subscribe(res => {
			res.map(item => {
				if (item.areaId) {
					this.areaIds.push(item.areaId);
				}
			});
			let query  = {}
			if(this.currentUser.designation !== 'MR'){
				query = {
					where: {
						companyId: this.currentUser.companyId,
						districtId: this.selectedDistrictId,
						blockId: { inq: this.areaIds }
					},
					include: "linkingDetail"
				};
			}else {
				query = {
					where: {
						companyId: this.currentUser.companyId,
						blockId: { inq: this.areaIds }
					},
					include: "linkingDetail"
				};

			}
			

			this.providerService.getProvidersList(query).subscribe(res1 => {
				this.doctorsList.length = 0;
				this.stockistList.length = 0;
				this.chemistList.length = 0;
				if (!res.length) {
					this.toasterService.info("No Provider found for given Employee");
					this.showLoader = false;
					this.showNoData = true;
					return;
				}

				let dl = [];

				res1.forEach(item => {
					if (item.providerType === "RMP") {
						dl.push(item);
					} else if (item.providerType === "Stockist") {
						this.stockistList.push(item);
					} else if (item.providerType === "Drug") {
						this.chemistList.push(item);
					}
				});

				dl.forEach(itemi => {
					itemi["stockist"] = [];
					itemi["chemist"] = [];
					let rawstockistString = "";
					let rawchemistString = "";
					itemi.linkingDetail.map(i => {
						if (i.companyId === this.currentUser.companyId) {
							i.linkedVendor.map(vendorId => {
								this.stockistList.forEach(itemj => {
									if (itemj.id === vendorId) {
										itemi.stockist.push(itemj);
										rawstockistString += itemj.providerName + ", ";
									}
								});
								this.chemistList.forEach(itemk => {
									if (itemk.id === vendorId) {
										itemi.chemist.push(itemk);
										rawchemistString += itemk.providerName + ", ";
									}
								});
							});
						}
					});

					itemi["stockistString"] = rawstockistString.substring( 0, rawstockistString.length - 2);
					itemi["chemistString"] = rawchemistString.substring( 0, rawchemistString.length - 2);
				});

				dl.sort((a, b) => {
					let A = a.providerName + "".toUpperCase();
					let B = b.providerName + "".toUpperCase();
					if (A < B) {return -1;} else if (A > B) {return 1;} else {return 0;}
				});

				this.doctorsList = dl;
				this.doctorsListSearching = dl;
				if (!this.doctorsList.length) {
					this.toasterService.info("No doctor found .");
					this.showLoader = false;
					this.showNoData = true;
					return;
				}
				
				this.showLoader = false;
				this.showTable = false;
				this.changeDetectorRef.detectChanges();

				this.stockistList.sort((a, b) => {
					let A = a.providerName + "".toUpperCase();
					let B = b.providerName + "".toUpperCase();
					if (A < B) {return -1;} else if (A > B) {return 1;} else {return 0;}
				});
				this.chemistList.sort((a, b) => {
					let A = a.providerName + "".toUpperCase();
					let B = b.providerName + "".toUpperCase();
					if (A < B) {return -1;} else if (A > B) {return 1;} else {return 0;}
				});
				console.log('doctorsList:- ',this.doctorsList);
				console.log('chemistList:- ',this.chemistList);
				console.log('stockistList:- ',this.stockistList);
			});
		});

		this.changeDetectorRef.detectChanges();

	}

	assignVendors(event) {
		let obj = {
			doctorDetails: event,
			stockistList: this.stockistList,
			chemistList: this.chemistList,
			labels: this.currentUserLabels
		};

		this.dialogConfig.data = obj;

		if (this.dialogRef === null) {
			this.dialogRef = this.dialog.open(
				DoctorVendorTableComponent,
				this.dialogConfig
			);
		}

		this.dialogRef.afterClosed().subscribe(res => {
			if (res === "success") {
				this.getList();
				this.changeDetectorRef.detectChanges();
			}
			this.dialogRef = null;
		});
	}
	searchDoctor(event){
		if(event.target.value === ''){
			this.doctorsList = this.doctorsListSearching;
		} else {
			// console.log('keyup event : ', event.target.value);
			this.doctorsList = this.doctorsListSearching.filter(item=>{
				return (item.providerName.toUpperCase().indexOf(event.target.value.toUpperCase()) > -1)
			});
		}
	}
}
