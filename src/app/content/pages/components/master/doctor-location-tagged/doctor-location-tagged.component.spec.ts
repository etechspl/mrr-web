import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorLocationTaggedComponent } from './doctor-location-tagged.component';

describe('DoctorLocationTaggedComponent', () => {
  let component: DoctorLocationTaggedComponent;
  let fixture: ComponentFixture<DoctorLocationTaggedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorLocationTaggedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorLocationTaggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
