import { Component, OnInit, ViewChild } from '@angular/core';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DesignationComponent } from '../designation/designation.component';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'm-doctor-location-tagged',
  templateUrl: './doctor-location-tagged.component.html',
  styleUrls: ['./doctor-location-tagged.component.scss']
})
export class DoctorLocationTaggedComponent implements OnInit {
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  //dcrForm: FormGroup;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  constructor() { }
  currentUser = JSON.parse(sessionStorage.currentUser);
  public showDivisionFilter: boolean = false;
  public showReportType: boolean = true;
  public showType: boolean = true;
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showArea: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showDesignation: boolean = false;
  public isShowTeamInfo: boolean = false;
  showProcessing: boolean = false;

  providerForm = new FormGroup({
    division: new FormControl(null),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    status: new FormControl(null),
    designation: new FormControl(null)

  })
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }

    this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithArea");
    if (this.currentUser.userInfo[0].rL == 1) {
      this.showReportType = false;
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showType = false;
      this.showArea = true;
      this.showDivisionFilter = false;
    }
  }
   getDivisionValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.providerForm.patchValue({ division: val });

    // if (this.currentUser.userInfo[0].designationLevel == 0) {
    //   let divisionIds = {
    //     division: val
    //   };
    //   this.callStateComponent.getStateBasedOnDivision(divisionIds);

    // } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
    //   let passingObj = {
    //     companyId: this.currentUser.companyId,
    //     isDivisionExist: true,
    //     division: this.providerForm.value.division,
    //     supervisorId: this.currentUser.id,
    //   }
    //   this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)
    // }


    if (this.currentUser.userInfo[0].designationLevel == 0) {
        //=======================Clearing Filter===================
              
       /// this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        //this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      
    } 

  }

  getStateValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.providerForm.patchValue({ stateInfo: val });

    this.callDistrictComponent.setBlank();

      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.providerForm.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }

    getDistrictValue(val) {
      this.showProcessing = false;
      this.showReport = false;
      this.providerForm.patchValue({ districtInfo: val });
  
    }
    getEmployeeValue(val) {
      this.showProcessing = false;
      this.showReport = false;
      this.providerForm.patchValue({ employeeId: val });
    }
    getStatusValue(val) {
      this.showProcessing = false;
      this.showReport = false;
      this.providerForm.patchValue({ status: val });
  
  
    }
  }

  

