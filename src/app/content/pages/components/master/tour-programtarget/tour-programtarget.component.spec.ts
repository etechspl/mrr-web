import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourProgramtargetComponent } from './tour-programtarget.component';

describe('TourProgramtargetComponent', () => {
  let component: TourProgramtargetComponent;
  let fixture: ComponentFixture<TourProgramtargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourProgramtargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourProgramtargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
