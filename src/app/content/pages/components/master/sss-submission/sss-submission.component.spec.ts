import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SssSubmissionComponent } from './sss-submission.component';

describe('SssSubmissionComponent', () => {
  let component: SssSubmissionComponent;
  let fixture: ComponentFixture<SssSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SssSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SssSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
