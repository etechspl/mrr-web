import { Component, OnInit, ViewChild } from '@angular/core';
import { MonthComponent } from '../../filters/month/month.component';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MappedStockistComponent } from '../../filters/mapped-stockist/mapped-stockist.component';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';

@Component({
  selector: 'm-sss-submission',
  templateUrl: './sss-submission.component.html',
  styleUrls: ['./sss-submission.component.scss']
})

export class SssSubmissionComponent implements OnInit {
  showState=false;
  showDistrict=false;
  showEmployee=false;
  isShowSSSForm = false;
  minDate = new Date(2018, 1, 1);
  maxDate = new Date();

  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;

  monthModel = new FormControl('', [Validators.required]);
  yearModel = new FormControl('', [Validators.required]);

  filter: FormGroup;
  submissionForm: FormGroup;
  constructor(private fb: FormBuilder, private stockiestService: StockiestService, private snackBar: MatSnackBar) {
    this.filter = this.fb.group({
      monthModel: null,
      yearModel: null
    })

    this.submissionForm = this.fb.group({
      productSelectBox: this.fb.array([this.fb.group({ product: '' })]),
      mrp: this.fb.array([this.fb.group({ mrpRate: '' })]),
      specialRate: this.fb.array([this.fb.group({ specRate: '' })]),
      quantity: this.fb.array([this.fb.group({ qty: '' })])
    })

  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.currentUser.userInfo[0].rL === 1||this.currentUser.userInfo[0].rL === 2) {
      this.getStockists(this.currentUser.id);
      this.getStateValue(this.currentUser.userInfo[0].stateId);
      this.getDistrictValue(this.currentUser.userInfo[0].districtId);
    }else if(this.currentUser.userInfo[0].rL === 0){
      this.showState=true;
      this.showDistrict=true;
      this.showEmployee=true;
    }
  }

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMappedStockistComponent") callMappedStockistComponent: MappedStockistComponent;
  check: boolean;

  currentUser = JSON.parse(sessionStorage.currentUser);
  sssForm = new FormGroup({
    stateId: new FormControl(null, Validators.required),
    districtId: new FormControl(null, Validators.required),
    userId: new FormControl(null, Validators.required),
    stockistId: new FormControl(null, Validators.required),
    month: new FormControl(null, Validators.required),
    year: new FormControl(null, Validators.required),
    saleType: new FormControl(null, Validators.required),
    invoiceNo: new FormControl(null, Validators.required),
    invoiceDate: new FormControl(null, Validators.required),
  })

  getStateValue(val) {
    this.isShowSSSForm = false;
    this.sssForm.patchValue({ stateId: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);
  }

  getDistrictValue(district) {
    let districtArr = [];
    this.isShowSSSForm = false;
    this.sssForm.patchValue({ districtId: district });
    districtArr.push(district);
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr);
  }

  getStockists(val) {
    let userArr = [];
    this.isShowSSSForm = false;
    this.sssForm.patchValue({ userId: val });
    userArr.push(val);
    this.callMappedStockistComponent.getMappedStockists(this.currentUser.companyId, userArr)
  }
  setStockistValue(val) {
    this.isShowSSSForm = false;
    this.sssForm.patchValue({ stockistId: val });
  }

  getMonth(val) {
    this.isShowSSSForm = false;
    this.sssForm.patchValue({ month: val });
    if(this.sssForm.value.year){
      this.getYear(this.sssForm.value.year);
    }
  }

  getYear(val) {
    if(this.sssForm.value.month){
      this.maxDate = new Date(val, this.sssForm.value.month, 0);
    }
    // this.maxDate = new Date(val, this.sssForm.value.month, 0);
    this.isShowSSSForm = false;
    this.sssForm.patchValue({ year: val })
  }

  callBackProductOptions = [];
  form = new FormGroup({});
  model = {
    saleType: null,
    sssSubmitFormDetail: [{
      productOtionValue: this.callBackProductOptions,
      netRate: null
    }],
  };
  options: FormlyFormOptions = {};

  saleType(val) {
    if (val === 'primarySale') {
      this.model = {
        saleType: 'primarySale',
        sssSubmitFormDetail: [{
          productOtionValue: this.callBackProductOptions,
          netRate: null
        }],

      };
    } else if (val === 'returnSale') {
      this.model = {
        saleType: 'returnSale',
        sssSubmitFormDetail: [{
          productOtionValue: this.callBackProductOptions,
          netRate: null
        }],
      };
    }
    this.isShowSSSForm = false;
    this.sssForm.patchValue({ saleType: val })
  }

  getInvoiceNum(val) {
    //console.log(this.sssForm.value.invoiceNo);
  }

  getToDate(val) {
    this.sssForm.patchValue({ invoiceDate: val })
  }

  private productResultArr = [];
  submitSSS() {
    // -------nipun to check which field of form is invalid
    // Object.keys(this.sssForm.controls).forEach((i)=>{
    //   console.log(`%c${i}`,"font-size: 15px;color: #4285f4;font-weight: 700;")
    //   console.log(`%cValue    : %c${this.sssForm.controls[''+i].value} `,"font-size: 15px;color: #00ab4d;font-weight: 700;","font-size: 15px;color: darkcyan;font-weight: 700;border-bottom: 5px solid #ff7a7a;");
    //   console.log(`%cValidity : %c${this.sssForm.controls[''+i].valid} `,"font-size: 15px;color: #fbf504;font-weight: 700;","font-size: 15px;color: darkcyan;font-weight: 700;border-bottom: 5px solid #ff7a7a;");
    //   console.log(`%cErrorrs  : %c${this.sssForm.controls[''+i].errors} `,"font-size: 15px;color: #f95f5f;font-weight: 700;","font-size: 15px;color: darkcyan;font-weight: 700;border-bottom: 5px solid #ff7a7a;");
    // });
    this.isToggled = false;
    this.stockiestService.checkForClosingSubmitted(this.currentUser.companyId, this.sssForm.value).subscribe(res => {
      if (res === '' || res.length === 0 || res[0].closing === 0) {
        this.saleType(this.sssForm.value.saleType);
        this.isShowSSSForm = true;
        this.productResultArr = [];
        this.stockiestService.getProductBasedOnState(this.currentUser.companyId, this.sssForm.value.stateId).subscribe(getProduct => {
          this.productResultArr.push(getProduct);

          for (let i = 0; i < this.productResultArr[0].length; i++) {
            this.callBackProductOptions.push({
              label: this.productResultArr[0][i].productName,
              value: this.productResultArr[0][i].id + ',' + this.productResultArr[0][i].productName,
              productCode: this.productResultArr[0][i].productCode,
              ptr: this.productResultArr[0][i].ptr,
              ptw: this.productResultArr[0][i].ptw,
              mrp: this.productResultArr[0][i].mrp,
            });
          }
          if (this.sssForm.value.saleType === 'primarySale') {
            this.model = {
              saleType: 'primarySale',
              sssSubmitFormDetail: [{
                productOtionValue: this.callBackProductOptions,
                netRate: null
              }],
            };
          } else if (this.sssForm.value.saleType === 'returnSale') {
            this.model = {
              saleType: 'returnSale',
              sssSubmitFormDetail: [{
                productOtionValue: this.callBackProductOptions,
                netRate: null
              }],
            };
          }
        }, err => {
        });
      } else {
        this.openSnackBar('You can not submit SSS details', 'Because Closing is Alreay submitted !!');
      }
    });
  }

  getProductAlreadyExist(companyId, value) {
    this.stockiestService.checkProductAlraedyExistOrNot(companyId, value, this.sssForm.value).subscribe(checkedProduct => {
      this.productResultArr = [];
      this.productResultArr.push(checkedProduct);
      if (checkedProduct.length > 0) {
        this.openSnackBar('Product is Alreay submitted.', '');
        this.saleType(this.sssForm.value.saleType);
        this.isShowSSSForm = true;
      } else {
        /* this.stockiestService.setProductRateFromMaster(companyId, value, this.sssForm.value.stateId).subscribe(setProductRate => {
           console.log(setProductRate);
           if (this.sssForm.value.saleType === 'primarySale') {
             this.model = {
               saleType: 'primarySale',
               sssSubmitFormDetail: [{
                 productOtionValue: this.callBackProductOptions,
                 netRate:setProductRate[0].mrp
               }],
             };
           } else if (this.sssForm.value.saleType === 'returnSale') {
             this.model = {
               saleType: 'returnSale',
               sssSubmitFormDetail: [{
                 productOtionValue: this.callBackProductOptions,
                 netRate:setProductRate[0].mrp
               }],
             };
           }
         });*/
      }
    }, err => {
    })
  }
  fields: FormlyFieldConfig[] = [
    {
      key: 'sssSubmitFormDetail',
      type: 'repeat',
      fieldArray: {
        fieldGroupClassName: 'row',
        templateOptions: {
          btnText: 'Add another product',
        },
        fieldGroup: [
          {
            type: 'select',
            key: 'product',
            className: 'col-sm-3',
            templateOptions: {
              type: 'select',
              label: 'Select Product Name',
              options: this.callBackProductOptions,
              required: true,
              change: (field, $event) => {
                this.getProductAlreadyExist(this.currentUser.companyId, field);
              }
            },
          },
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'netRate',
            templateOptions: {
              label: 'Net Rate:',
              placeholder: 'fill netRate',
              required: true,
              maxLength: 5,
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              },
            },
          },
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'splRate',
            templateOptions: {
              label: 'Special Rate:',
              placeholder: 'fill specialRate',
              maxLength: 5,
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              },
            },
          },
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'opening',
            templateOptions: {
              label: 'Opening:',
              placeholder: 'fill opening',
              required: true,
              maxLength: 5
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              }, },
            hideExpression: (model) => {
              if (this.model.saleType === 'primarySale') {
                return false;
              }return true;
            }, }
            /*,
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'return',
            templateOptions: {
              label: 'Sale Return:',
              placeholder: 'fill Return',
              required: true,
              maxLength: 5
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              },
            },
            hideExpression: (model) => {
              if (this.model.saleType === 'returnSale') {
                return false;
              }
              return true;

            },
          }*/,
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'primary',
            templateOptions: {
              label: 'Primary Sale:',
              placeholder: 'fill primary',
              required: true,
              maxLength: 5
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              },
            },
            hideExpression: (model) => {
              if (this.model.saleType === 'primarySale') {
                return false;
              }
              return true;}
          },
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'breakage',
            templateOptions: {
              label: 'Breakage Return:',
              placeholder: 'fill Return',
              required: true,
              maxLength: 5
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              },
            },
            hideExpression: (model) => {
              if (this.model.saleType === 'returnSale') {
                return false;
              }return true;
            },
          },
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'saleable',
            templateOptions: {
              label: 'Saleable Return:',
              placeholder: 'fill Return',
              required: true,
              maxLength: 5
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              },
            },
            hideExpression: (model) => {
              if (this.model.saleType === 'returnSale') {
                return false;
              }
              return true;

            },
          },
          {
            className: 'col-sm-2',
            type: 'input',
            key: 'other',
            templateOptions: {
              label: 'Other Return:',
              placeholder: 'fill Return',
              required: true,
              maxLength: 5
            },
            validators: {
              ip: {
                expression: (c) => !c.value || /[0-9]/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" Only Number input allow`,
              },
            },
            hideExpression: (model) => {
              if (this.model.saleType === 'returnSale') {
                return false;
              } return true;
            },
          }

        ],
      },
    },
  ];

  submit() {
    //submit SSS
    this.stockiestService.submitSSS(this.currentUser.companyId, this.sssForm.value, this.model, this.currentUser.userInfo[0].userId)
      .subscribe(mapStokist => {
        this.saleType(this.sssForm.value.saleType);
        this.isShowSSSForm = false;
        this.openSnackBar('SSS submit Successfully.', '');
      }, err => {
        this.saleType(this.sssForm.value.saleType);
        this.isShowSSSForm = false;
        this.openSnackBar('SSS submit Successfully.', '');
      })
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
