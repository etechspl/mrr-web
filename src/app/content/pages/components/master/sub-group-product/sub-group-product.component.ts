import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { ProductGroupFilterComponent } from '../../filters/product-group-filter/product-group-filter.component';
import { ProductsComponent } from '../../filters/products/products.component';
import { StateComponent } from '../../filters/state/state.component';
import { EditProductSubGroupComponent } from '../edit-product-sub-group/edit-product-sub-group.component';

@Component({
  selector: 'm-sub-group-product',
  templateUrl: './sub-group-product.component.html',
  styleUrls: ['./sub-group-product.component.scss']
})
export class SubGroupProductComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);
	showProductGroupDetails: boolean = false;
	dataSource: MatTableDataSource<any>;
	displayedColumns = [
		"srno",
		"divisionName",
		"stateName",
		"productGroupName",
		"productSubGroupName",
		"prodName",
		"pts",
		"ptr",
		"mrp",
		"action",
		"action1",
	];
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	companyId = this.currentUser.companyId;
	DA_TYPE = this.currentUser.company.expense.daType;
	IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
	isShowDivision = false;
  public showDivisionFilter: boolean = false;
	@ViewChild("dataTable") table;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callProductComponent") callProductComponent: ProductsComponent;
	@ViewChild("callProductGroup") callProductGroup: ProductGroupFilterComponent;

	public showProcessing: boolean = false;

  ProductgroupForm = new FormGroup({});

	constructor(
		private fb: FormBuilder,
		private _productService: ProductService,
		public _toasterService: ToastrService,
		private changeDetectedRef: ChangeDetectorRef,
		public dialog: MatDialog
	) {
		this.ProductgroupForm = this.fb.group({
			companyId: null,
			stateInfo: [null, Validators.required],
			productInfo: [null, Validators.required],
			productSubGroupName: new FormControl(null),
			divisionId: new FormControl(null),
			groupInfo:new FormControl(null),
		});
	}

  ngOnInit() {
    
		if (this.currentUser.company.isDivisionExist == true) {
			this.ProductgroupForm.get("divisionId").setValidators([
				Validators.required,
			]);
			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.isShowDivision = true;
		}

		this.showTable();
  }
  getStateValue(val) {
		this.ProductgroupForm.patchValue({ stateInfo: val });
		this.callProductGroup.getProductGroupData(this.companyId,this.IS_DIVISION_EXIST,this.ProductgroupForm.value);
		this.callProductComponent.getProductsDataForGroup(this.companyId,this.IS_DIVISION_EXIST,this.ProductgroupForm.value.divisionId);
	}

	getDivisionValue($event) {
		this.ProductgroupForm.patchValue({ assignedStates: null });
		(this.callStateComponent) ? this.callStateComponent.setBlank() : null;
		this.ProductgroupForm.patchValue({ divisionId: $event });
		if (this.IS_DIVISION_EXIST === true) {
		  this.callStateComponent.getStateBasedOnDivision({
			companyId: this.currentUser.companyId,
			isDivisionExist: this.IS_DIVISION_EXIST,
			division: this.ProductgroupForm.value.divisionId,
			designationLevel: this.currentUser.userInfo[0].designationLevel
		  })
		}
	  }

	getProductsDataForGroup(val) {
		this.ProductgroupForm.patchValue({ productInfo: val });
	}
	
	getProductGroupData(val){
		this.ProductgroupForm.patchValue({ groupInfo: val });
	}
	

  SubmitGroup(){
	this._productService.createProductSubGroups(this.ProductgroupForm.value,this.currentUser.companyId).subscribe((productResponse) => {
		this._toasterService.success(
			"Product Sub-Group Created Successfully"
		);
		this.showTable();
		this.changeDetectedRef.detectChanges();
	},
	(err) => {
		// console.log(err);
		this._toasterService.error(
			"Product Sub-Group is Already Exists"
		);
	}
);
this.ProductgroupForm.reset();
this.changeDetectedRef.detectChanges();
this.showTable();
  }
  

  showTable(){
		
		this._productService
		.getProductSubGroupDetails(
			this.currentUser.companyId,
			this.ProductgroupForm.value
		)
		.subscribe(
			(res) => {
				res.sort(function (a, b) {
					if (a.hasOwnProperty("stateName") === true && b.hasOwnProperty("stateName") === true) {
						var textA = a.stateName.toUpperCase();
						var textB = b.stateName.toUpperCase();
						return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
					}
				});
				
				this.showProductGroupDetails = true;
				this.dataSource=res;
				this.dataSource = new MatTableDataSource(res);
				this.changeDetectedRef.detectChanges();
			},
			(err) => {
				
			}
		);
  }

  applyFilter(filterValue: string) {
	filterValue = filterValue.trim(); // Remove whitespace
	filterValue = filterValue.toLowerCase();
	this.dataSource.filter = filterValue
  }



  editProductGroup(value) {
	  //console.log("values : ",value);
	const dialogRef = this.dialog.open(EditProductSubGroupComponent, {
		width: "60%",
		height: "60%",
		data: { value },
	});
	dialogRef.afterClosed().subscribe((result) => {
		setTimeout(() => {
			this.showTable();
			this.changeDetectedRef.detectChanges();
		}, 50);
	});
	this.changeDetectedRef.detectChanges();
}



deleteProductGroup(obj) {
	this._productService
		.deleteProductSubGroup(obj, this.currentUser.company.id)
		.subscribe(
			(groupDelResponse) => {
				this._toasterService.success("Product Sub Group is Deleted");
				this.showTable();
				this.changeDetectedRef.detectChanges();
			},
			(err) => {
				console.log(err);
				this._toasterService.error(
					"Error in Product Group component"
				);
			}
		);
}


}
