import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubGroupProductComponent } from './sub-group-product.component';

describe('SubGroupProductComponent', () => {
  let component: SubGroupProductComponent;
  let fixture: ComponentFixture<SubGroupProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubGroupProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubGroupProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
