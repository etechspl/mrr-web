import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SamplesIssueComponent } from './samples-issue.component';

describe('SamplesIssueComponent', () => {
  let component: SamplesIssueComponent;
  let fixture: ComponentFixture<SamplesIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SamplesIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SamplesIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
