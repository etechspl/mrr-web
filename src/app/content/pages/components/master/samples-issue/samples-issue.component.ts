import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { GiftService } from '../../../../../core/services/Gift/gift.service';
import { TargetService } from '../../../../../core/services/Target/target.service';
import { SampleService } from '../../../../../core/services/sample/sample.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatSlideToggleChange } from '@angular/material';
import { StateComponent } from '../../filters/state/state.component';

@Component({
  selector: 'm-samples-issue',
  templateUrl: './samples-issue.component.html',
  styleUrls: ['./samples-issue.component.scss']
})
export class SamplesIssueComponent implements OnInit {
  searchTerm = '';

  showStateReportType = true;
  showEmpReportType = true;
  showHqReportType = true;
  moduleType = true;
  stateFilter = false;
  designationFilterForUserwiseOnly=false;
  districtFilter = false;
  employeeFilter = false;
  designationFilter = false;
  districtFilterForMulti = false;
  designationData = [];
  isShowIssueSample = false;
  isShowIssueGift = false;

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDistrictComponentAgainForMulti") callDistrictComponentAgainForMulti: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  sampleIssueFilter: FormGroup;
  showState = false;
  showDistrict = false;
  showDesignation = false;
  showEmployee = false;
  constructor(
    private fb: FormBuilder,
    private _toastr: ToastrService,
    private userDetailsService: UserDetailsService,
    private _targetService: TargetService,
    private _samplesService:SampleService,
    private _giftService: GiftService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.reportType('Employeewise');
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  
  displayedColumns = ['sno','productName', 'productCode','receipt'];
  currentUser = JSON.parse(sessionStorage.currentUser);
  sampleAndGift = new FormGroup({
    reportType: new FormControl(null, Validators.required),
    stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null),
    userInfo: new FormControl(null),
    designation: new FormControl(null, Validators.required),
    moduleTypeName: new FormControl(null, Validators.required),
  })

  sample = new FormGroup({
    productName: new FormControl(null, Validators.required),
    productCode: new FormControl(null),
    productId: new FormControl(null),
    quantity: new FormControl(null),
  })

  gift = new FormGroup({
    giftName: new FormControl(null, Validators.required),
    giftCode: new FormControl(null),
    giftId: new FormControl(null),
    quantity: new FormControl(null),
  })

  productListForSample = [];
  sampleIssueDetailArr(productArr) {
    this.productListForSample = productArr;
    this.sample = this.fb.group({
      productName: [[Validators.required]],
      productCode: [[Validators.required]],
      productId: [],
      quantity: [],
      resultList: new FormArray([])
    });
    const productFGs = this.productListForSample.map(product => {
      return this.fb.group({
        productName: [product.productName],
        productCode: [product.productCode],
        productId: [product.id],
        quantity: [product.quantity],
      })
    });
    const productFormArray: FormArray = this.fb.array(productFGs);
    this.sample.setControl('resultList', productFormArray);
  }

  productListForGift = [];
  giftDetailArr(productArr) {
    this.productListForGift = productArr;
    this.gift = this.fb.group({
      giftName: [[Validators.required]],
      giftCode: [],
      giftId: [],
      quantity: [],
      resultList: new FormArray([])
    });
    const productFGs1 = this.productListForGift.map(product1 => {
      return this.fb.group({
        giftName: [product1.giftName],
        giftCode: [product1.giftCode],
        giftId: [product1.id],
        quantity: [product1.quantity],
      })
    });
    const productFormArray1: FormArray = this.fb.array(productFGs1);
    this.gift.setControl('resultList1', productFormArray1);
  }

  reportType(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    this.sampleAndGift.patchValue({ reportType: val });
    if (val === 'Employeewise') {
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = true;
        this.districtFilterForMulti = false;
        this.designationFilter = false;
        this.designationFilterForUserwiseOnly=true;
        this.sampleAndGift.get('designation').setValidators([Validators.required]);
        this.sampleAndGift.get('userInfo').setValidators([Validators.required]);
        this.sampleAndGift.get('districtInfo').clearValidators();
        this.sampleAndGift.patchValue({stateInfo: null});
        this.sampleAndGift.patchValue({districtInfo: null});
        this.sampleAndGift.patchValue({designation: null});
        this.sampleAndGift.patchValue({userInfo: null});
        (this.callDistrictComponent)?this.callDistrictComponent.setBlank():null;
        (this.callDistrictComponentAgainForMulti)?this.callDistrictComponentAgainForMulti.setBlank():null;
        (this.callEmployeeComponent)?this.callEmployeeComponent.setBlank():null;
        (this.callStateComponent)?this.callStateComponent.setBlank():null;

      }
    } else if (val === 'Statewise') {
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.districtFilterForMulti = false;
        this.designationFilter = true;
        this.designationFilterForUserwiseOnly=false;
        this.sampleAndGift.get('designation').clearValidators();
        this.sampleAndGift.get('userInfo').clearValidators();
        this.sampleAndGift.get('districtInfo').clearValidators();
        this.sampleAndGift.patchValue({userInfo: null});
        this.sampleAndGift.patchValue({designation: null});
        this.sampleAndGift.patchValue({stateInfo: null});
        this.sampleAndGift.patchValue({districtInfo: null});
        (this.callDistrictComponent)?this.callDistrictComponent.setBlank():null;
        (this.callDistrictComponentAgainForMulti)?this.callDistrictComponentAgainForMulti.setBlank():null;
        (this.callEmployeeComponent)?this.callEmployeeComponent.setBlank():null;
        (this.callStateComponent)?this.callStateComponent.setBlank():null;
      }
    } else if (val === 'Headquarterwise') {
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.districtFilterForMulti = true;
        this.designationFilter = true;
        this.designationFilterForUserwiseOnly=false;
        this.sampleAndGift.get('districtInfo').setValidators([Validators.required]);
        this.sampleAndGift.get('designation').clearValidators();
        this.sampleAndGift.get('userInfo').clearValidators();
        this.sampleAndGift.patchValue({userInfo: null});
        this.sampleAndGift.patchValue({designation: null});
        this.sampleAndGift.patchValue({districtInfo: null});
        this.sampleAndGift.patchValue({stateInfo: null});
        (this.callDistrictComponent)?this.callDistrictComponent.setBlank():null;
        (this.callDistrictComponentAgainForMulti)?this.callDistrictComponentAgainForMulti.setBlank():null;
        (this.callEmployeeComponent)?this.callEmployeeComponent.setBlank():null;
        (this.callStateComponent)?this.callStateComponent.setBlank():null;
      }
    }
  }

  setEmployeeDetail(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    this.sampleAndGift.patchValue({ userInfo: val })
  }



  moduleSubmitType(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    this.sampleAndGift.patchValue({ moduleTypeName: val })
  }

  getStateValue(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    let state = [];
    state.push(val);
    this.sampleAndGift.patchValue({ stateInfo: state });
    this.sampleAndGift.patchValue({userInfo: null});
    this.sampleAndGift.patchValue({designation: null});
    this.sampleAndGift.patchValue({districtInfo: null});
    (this.callDistrictComponent)?this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true):null;
    (this.callDistrictComponent)?this.callDistrictComponent.setBlank():null;
    (this.callEmployeeComponent)?this.callEmployeeComponent.setBlank():null;
    (this.callEmployeeComponent)?this.callEmployeeComponent.removelist():null;
    if (this.sampleAndGift.value.reportType == 'Headquarterwise') {
      this.callDistrictComponentAgainForMulti.getDistricts(this.currentUser.companyId, val,true);
      this.callDistrictComponentAgainForMulti.setBlank();
    } else if (this.sampleAndGift.value.reportType == 'Statewise'|| this.sampleAndGift.value.reportType == 'Employeewise') {
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, val, this.sampleAndGift.value.districtInfo, this.sampleAndGift.value.reportType).subscribe(res => {
        this.designationData = res;
      });
    }
  }

  getEmployeeValue(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    let district = [];
    if (this.sampleAndGift.value.reportType == 'Employeewise') {
      this.sampleAndGift.patchValue({ designation: val });
      this.sampleAndGift.patchValue({userInfo: null});
      this.callEmployeeComponent.setBlank();
      this.callEmployeeComponent.removelist();
      this.callEmployeeComponent.getMgrList(this.currentUser.companyId, this.sampleAndGift.value.stateInfo,val,[true]);
    } else if (this.sampleAndGift.value.reportType == 'Headquarterwise') {
      for (let i = 0; i < val.length; i++) {
        district.push(val[i]);
      }
      this.sampleAndGift.patchValue({ districtInfo: district });
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, this.sampleAndGift.value.stateInfo, this.sampleAndGift.value.districtInfo, this.sampleAndGift.value.reportType).subscribe(res => {
        this.designationData = res;
      });
    }
  }
  dataSource=[];
  viewSampleAndGiftDetail() {
    this.isToggled = false;
    if (this.sampleAndGift.value.moduleTypeName == 'sample') {
      this._targetService.getProductBasedOnStateForTargetModule(this.currentUser.companyId, this.sampleAndGift.value.stateInfo, this.sampleAndGift.value.selectedGroupName, 0).subscribe(getProduct => {
        if (getProduct.length > 0) {
          this.dataSource=getProduct;
          this.sampleIssueDetailArr(getProduct);
          this.isShowIssueSample = true;
          this.isShowIssueGift = false;
          this._changeDetectorRef.detectChanges();
        } else {
          this._changeDetectorRef.detectChanges();
          this._toastr.success("Please select another details", "No Samples Found for selected Details !!");
        }
      }, err => {
      });
    } else if (this.sampleAndGift.value.moduleTypeName == 'gift') {
      this._giftService.getGiftBasedOnState(this.currentUser.companyId, this.sampleAndGift.value.stateInfo, this.sampleAndGift.value.selectedGroupName, 0).subscribe(getGift => {
        if (getGift.length > 0) {
          this.giftDetailArr(getGift);
          this.isShowIssueSample = false;
          this.isShowIssueGift = true;
          this._changeDetectorRef.detectChanges();
        } else {
          this._changeDetectorRef.detectChanges();
          this._toastr.success("Please select another details", "No Gifts Found for selected Details !!");
        }
      }, err => {
      });
    }
  }
  issueSample() {
    this._samplesService.sampleIssueDetail( this.sampleAndGift.value, this.sample.value.resultList,this.currentUser.companyId, this.currentUser.id)
      .subscribe(sample => {
        if (sample.length > 0) {
            this._toastr.success("Sample Issued Successfully !!", "Sample issued successfully for selected details!!");
          this._changeDetectorRef.detectChanges();
        }
        this.isShowIssueSample = false;
        this.isShowIssueGift = false;
        this._changeDetectorRef.detectChanges();
      }, err => {
      })
  }
  issueGift() {
    this._samplesService.sampleIssueDetail( this.sampleAndGift.value, this.gift.value.resultList1,this.currentUser.companyId, this.currentUser.id)
      .subscribe(gift => {
        if (gift.length > 0) {
            this._toastr.success("Gift Issued Successfully !!", "Gift issued successfully for selected details!!");
          this._changeDetectorRef.detectChanges();
        }
        this.isShowIssueSample = false;
        this.isShowIssueGift = false;
        this._changeDetectorRef.detectChanges();
      }, err => {
      })
  }
}
