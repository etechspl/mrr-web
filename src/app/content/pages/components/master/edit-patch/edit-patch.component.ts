import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MatPaginator, MatSlideToggleChange, MatSort, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';

@Component({
  selector: 'm-edit-patch',
  templateUrl: './edit-patch.component.html',
  styleUrls: ['./edit-patch.component.scss']
})
export class EditPatchComponent implements OnInit {
  EditPatchForm: FormGroup;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator1') paginator1: MatPaginator;

  displayedColumns = [
		"select",
		"sn",
		"providerName",
		"providerType",
		"patch",
		"providerCode",
		"specialization",
		"degree",
		"category"
  ];
  displayedColumns2 = [
		"select",
		"sn",
		"providerName",
		"providerType",
		"providerCode",
		"specialization",
		"degree",
		"category"
	];
  workingadd: any;
  dataSource: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
	@ViewChild(MatSort) sort: MatSort;
	isToggled = true;

  currentUser = JSON.parse(sessionStorage.currentUser);

  districtData: any;
  districtIds: any;
  districtIdsLength: number;
  showProcessing:boolean=false;
  showMoreProviders:boolean=false;
  showSelectedPatches:boolean=false;
  showData:boolean=false;
  designationData = [];
  selection = new SelectionModel<any>(true, []);
  selection1 = new SelectionModel<any>(true, []);

	selectedValue = []; toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
  }
  showSelectedPatch:boolean=false;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  DA_TYPE = this.currentUser.company.expense.daType;
  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  constructor(
    private dialogRef: MatDialogRef<EditPatchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private changeDetectRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private _userareamappingservice: UserAreaMappingService,
	private fb: FormBuilder,
	private _providerservice:ProviderService,
    public toasterService: ToastrService,
  ) { 
    this.EditPatchForm = this.fb.group({
      stateInfo: this.data.data.stateId,
      patchName:this.data.data.patchName,
      districtInfo:this.data.data.districtId,
      employeeId: this.data.data.userId,
      designation: this.data.data.patchName,
    });
  } 

  ngOnInit() {
   
	this.showProcessing=true;
	this.getPatchDetail();
  }
  getPatchDetail(){
	let obj={
		providerId:this.data.data.providerId,
		companyId:this.currentUser.companyId
	  }
	  
	  this._userareamappingservice.getPatchDetail(obj).subscribe(res=>{
		if (res == null || res.length == 0 || res == undefined) {
			      this.showSelectedPatches=false;
				  this.toasterService.info('Record is not found for selected filter.', 'No Record Found');
				  this.dialogRef.close();
				  (Swal as any).fire({
					title: "No Providers Linked With this Patch, Please click on Create More",
					type: "error",
				});
			  } else {
		 this.showSelectedPatches=true;
		  this.showProcessing=false;
				  res.forEach(element => {
					  if(element.providerType=="RMP"){
						  element.providerType=this.currentUser.company.lables.doctorLabel
					  } else if(element.providerType=="Drug"){
						  element.providerType=this.currentUser.company.lables.vendorLabel
					  }
				  });
				  this.dataSource2 = res;
				  !this.changeDetectRef["destroyed"]
					  ? this.changeDetectRef.detectChanges() 
					  : null;
				  this.dataSource2 = new MatTableDataSource(res);
				  setTimeout(() => {
					  this.dataSource2.paginator = this.paginator1;
				  }, 100);
				  setTimeout(() => {
					  this.dataSource2.sort = this.sort;
				  }, 100);
				  !this.changeDetectRef["destroyed"]
					  ? this.changeDetectRef.detectChanges()
					  : null;
			  }
			  this.changeDetectRef.detectChanges();
		  }, err => {
			  console.log(err)
			  this.showProcessing = false;
			  this.toasterService.info('Record is not found for selected filter.', 'No Record Found');
			  this.changeDetectRef.detectChanges();
		  })
  }
  craeteMorePatch(){
    this.getMoreProviders();
  }
  createPatch(){
	let selectedProviders: any = this.selection.selected;
	let title = "";
	if (this.EditPatchForm.value.patchName == null) {
		title = "Please Enter the Patch Name !!"
	}
	if (selectedProviders.length == 0) {
		title = "Please Select the Providers !!"
	}
	if (selectedProviders.length == 0 || this.EditPatchForm.value.patchName == null) {
		(Swal as any).fire({
			title: title,
			type: "error",
		});
	}
	else {
		let providerId = [];
		let obj = {};
		let areaId = new Set();
		let districtId, stateId;
		selectedProviders.forEach((element, i) => {
			element["index"] = i;
			providerId.push(element.providerId);
			areaId.add(element.areaId);
			districtId = element.districtId;
			stateId = element.stateId;
		});
		let areaArray = Array.from(areaId);
		obj = {
			companyId: this.currentUser.companyId,
			areaId: areaArray,
			providerId: providerId,
			userId: this.EditPatchForm.value.employeeId,
			stateId: stateId,
			districtId: districtId,
			patchName: this.EditPatchForm.value.patchName,
			status: true
		}
		
		this._providerservice.updatePatch(obj).subscribe(res => {
			if (res) {
				(Swal as any).fire({
					title: "Sucessfully Updated",
					type: "success",
				});
				
				this.selection.clear();
				!this.changeDetectRef["destroyed"]
					? this.changeDetectRef.detectChanges()
					: null;
				this.dialogRef.close();
			} else {
				(Swal as any).fire({
					title: `There is some error in creation !!`,
					type: "error",
				});
			}

		})

	}
  }
  removePatch(){
    let selectedProviders: any = this.selection1.selected;
    if (selectedProviders.length == 0 || this.EditPatchForm.value.patchName == null) {
			(Swal as any).fire({
				title: "Please Select Providers",
				type: "error",
			});
		}
		else {
			let providerId = [];
			selectedProviders.forEach((element, i) => {
				element["index"] = i;
				providerId.push(element._id);
      });
      let obj={
        providerId:providerId,
        userId:this.data.data.userId,
        companyId:this.currentUser.companyId,
        patchName:this.EditPatchForm.value.patchName
      }
      this._userareamappingservice.removePatches(obj).subscribe(res=>{
		if(res){
			(Swal as any).fire({
							title: "Successfully Deleted",
							type: "success",
						});
						!this.changeDetectRef["destroyed"]
						? this.changeDetectRef.detectChanges()
						: null;
						this.dialogRef.close();
		}else {
			(Swal as any).fire({
				title: `There is some error in creation !!`,
				type: "error",
			});
		}
      })
      
    }
  }

  updatePatchName(){
	  let obj={
        userId:this.data.data.userId,
        companyId:this.currentUser.companyId,
        patchName:this.data.data.patchName
	  }

	  let updateData={
		 updatedAt:new Date(),
		 patchName:this.EditPatchForm.value.patchName
	  }

	  this._userareamappingservice.updatePatcheName(obj,updateData).subscribe(res=>{
		if(res){
			(Swal as any).fire({
							title: "Successfully Updated",
							type: "success",
						});
						!this.changeDetectRef["destroyed"]
						? this.changeDetectRef.detectChanges()
						: null;
			this.EditPatchForm.controls['patchName'].setValue(this.EditPatchForm.value.patchName);

		}else {
			(Swal as any).fire({
				title: `There is some error in creation !!`,
				type: "error",
			});
		}
      })
  }
  getMoreProviders(){
    this.showMoreProviders=true;
    this.showProcessing=true;

    let userIds=this.data.data.userId;
    this._userareamappingservice.getProvidersListWithPatchName(this.currentUser.companyId, "Employee Wise", this.EditPatchForm.value.stateInfo, this.EditPatchForm.value.districtInfo, "", [userIds],"", [this.EditPatchForm.value.status],"","").subscribe(res => {
			if (res == null || res.length == 0 || res == undefined) {
				this.toasterService.info('Record is not found for selected filter.', 'No Record Found');
			} else {
        this.showProcessing=false;
        //this.showMoreProviders=true;
        this.showData=true;
				res.forEach(element => {
					if(element.providerType=="RMP"){
						element.providerType=this.currentUser.company.lables.doctorLabel
					} else if(element.providerType=="Drug"){
						element.providerType=this.currentUser.company.lables.vendorLabel
					}
				});
				this.dataSource = res;
				!this.changeDetectRef["destroyed"]
					? this.changeDetectRef.detectChanges() 
					: null;
				this.dataSource = new MatTableDataSource(res);
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
				}, 100);
				setTimeout(() => {
					this.dataSource.sort = this.sort;
				}, 100);
				!this.changeDetectRef["destroyed"]
					? this.changeDetectRef.detectChanges()
					: null;
			}
			this.changeDetectRef.detectChanges();
		}, err => {
			console.log(err)
			this.showProcessing = false;
			this.toasterService.info('Record is not found for selected filter.', 'No Record Found');
			this.changeDetectRef.detectChanges();
		})
  }
  isAllSelected() {
		const numSelected = this.selection.selected.length;		
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
  }
  isAllSelected1() {	  
		const numSelected = this.selection1.selected.length;
		const numRows = this.dataSource2.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
  }
  masterToggle1() {
	this.isAllSelected1()
		? this.selection1.clear()
		: this.dataSource2.data.forEach((row) => this.selection1.select(row));
}
  applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
 

}
