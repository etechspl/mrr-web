import { StockiestService } from './../../../../../core/services/Stockiest/stockiest.service';
import { ProductService } from './../../../../../core/services/Product/product.service';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { SelectionModel } from '@angular/cdk/collections';
import { ProvidersComponent } from './../../filters/providers/providers.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { DoctorChemistService } from '../../../../../core/services/DoctorChemist/doctor-chemist.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
@Component({
  selector: 'm-doctor-chemist-sale',
  templateUrl: './doctor-chemist-sale.component.html',
  styleUrls: ['./doctor-chemist-sale.component.scss']
})
export class DoctorChemistSaleComponent implements OnInit {


  showState = true;
  showDistrict = true;
  showEmployee = true; 
  check: boolean;
  isToggled = true;
  providerSaleForm:FormGroup;
  productwiseSale:FormGroup;
  public investment: FormControl = new FormControl();
  public acheived: FormControl = new FormControl();
  differenceValue:number=0;
  productInfo:FormArray;
  isShowTable=false;
  dataSource;
  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(
    private _toastr: ToastrService, 
    private fb: FormBuilder,
    private monthYearService: MonthYearService, 
    private snackBar: MatSnackBar,
    private _changeDetectorRef: ChangeDetectorRef,
    private userAreaMappingService: UserAreaMappingService,
    private _doctorChemistService: DoctorChemistService,
    private _productService:ProductService
    ) { 
      this.createSaleForm();
      this.createProductForm();
    }

    createSaleForm(){
      this.providerSaleForm = new FormGroup({
        stateId: new FormControl(null, Validators.required),
        districtId: new FormControl(null, Validators.required),
        userId: new FormControl(null),
        providerId: new FormControl(null, Validators.required),
        month: new FormControl(null, Validators.required),
        year: new FormControl(null, Validators.required),
        divisionId: new FormControl(null),
      });
    }
    createProductForm(){
      this.productwiseSale = new FormGroup({
        products: new FormArray([])
      });
    }

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callProvidersComponent") callProvidersComponent: ProvidersComponent;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = ['index', 'doctor','chemists','Total Sale'];
  

  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {}

  getProductDetails(){
      this.createProductForm();
      this.getProductList();
      this.isShowTable=true;
      (!this._changeDetectorRef['destroyed'])? this._changeDetectorRef.detectChanges(): null;
  }

  getProductList(){
    this._productService.getProductList(this.productwiseSale.value.stateId,this.currentUser.companyId).subscribe(res => {
      console.log('res',res);
      if(res.length>0){
        res.forEach(element => {
          this.productInfo = this.productwiseSale.get('products') as FormArray
          const group = new FormGroup({
            productId:new FormControl(element.id),
            productName: new FormControl(element.productName),
            sale: new FormControl(0),
          })
          console.log('group',group);
          this.productInfo.push(group);
        });
        (!this._changeDetectorRef['destroyed'])? this._changeDetectorRef.detectChanges(): null;
        console.log('productInfo',this.productInfo);
      }
    }, err => {
      alert("Error while getting the areas");
    })
  }

  differencePoints(){
    this.differenceValue=this.investment.value-this.acheived.value;
  }
  totalSaleAmt=0;
  totalSaleValue(){
    console.log(this.productwiseSale.value['products'])
    if(this.productwiseSale.value['products'].length>0){
      this.totalSaleAmt=0;
      this.productwiseSale.value['products'].forEach(element => {
        this.totalSaleAmt=this.totalSaleAmt+element.sale;
        });
    }else{
      this.totalSaleAmt=0;
    }
  }
  
  getAreas(companyId, userId) {
    this.userAreaMappingService.getMappedAreas(companyId, userId).subscribe(res => {
      let response = [];
      response.push(res);
      if (response[0].length > 0) {
        let areas = [];
        res.forEach(element => {
          areas.push(element.areaId);
        });
        console.log('areas', areas);
        this.callProvidersComponent.getProvidersData(this.currentUser.companyId, areas, ['RMP']);
      }
    }, err => {
      alert("Error while getting the areas");
    })
  }

  getStateValue(val) {
    this.providerSaleForm.patchValue({ stateId: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);

  }

  getDistrictValue(val) {
    let districtArr = [];
    this.providerSaleForm.patchValue({ districtId: val });
    districtArr.push(val);
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr);
  }

  getProviders(val) {
    this.providerSaleForm.patchValue({ userId: val });
    this.getAreas(this.currentUser.companyId, val)
  }

  setProviderValue(val) {
    this.providerSaleForm.patchValue({ providerId: val._id });
    this.getLinkedProviders(this.providerSaleForm.value.providerId)
  }

  getMonth(val) {
    this.providerSaleForm.patchValue({ month: val })
  }

  getYear(val) {
    this.providerSaleForm.patchValue({ year: val })
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }


  loader;
  preparedData;
  getLinkedProviders(provider) {
    this._doctorChemistService.getDoctorwiseChemistPair(provider, this.currentUser.companyId).subscribe(res => {
      let content = [];
      this.preparedData = [];
      for(let i = 0; i < res.length; i++ ){
        if(content.indexOf(res[i].providerId) == -1){ 
          content.push(res[i].providerId);
          let obj = {
            providerId: res[i].providerId,
            doctorName: res[i].providerName,
            chemistList: [{name: res[i].providerLinkedName, providerLinkedId: res[i].providerLinkedId, id: res[i].id}]
          }
          this.preparedData.push(obj);
        } else {
          for(let j = 0; j < this.preparedData.length; j++){
            if (this.preparedData[j].providerId === res[i].providerId) {
              let chemistName = {name: res[i].providerLinkedName, providerLinkedId: res[i].providerLinkedId, id: res[i].id};
              this.preparedData[j].chemistList.push(chemistName);
            }
          }
        }
      }
      this.loader = 2;
      this.dataSource = new MatTableDataSource(this.preparedData);
      setTimeout(()=>{
        this.dataSource.paginator = this.paginator;
      }, 100);
      // this.dataSource = this.preparedData;
      (!this._changeDetectorRef['destroyed'])? this._changeDetectorRef.detectChanges(): null;
      // this.data = res;
    });
  }

  remove(element, el): void {}

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  submitDetails(){
    console.log(this.investment.value);
    console.log(this.acheived.value);
    console.log(this.providerSaleForm.value);
    console.log(this.productwiseSale.value);
    let param={};
    let paramArr=[];
    this.productwiseSale.value['products'].forEach(element => {
      param={
        companyId:this.currentUser.companyId,
        stateId:this.providerSaleForm.value.stateId,
        districtId:this.providerSaleForm.value.districtId,
        userId:this.providerSaleForm.value.userId,
        providerId:this.providerSaleForm.value.providerId,
        productId:element.productId,
        productName:element.productName,
        month:this.providerSaleForm.value.year,
        year:this.providerSaleForm.value.month,
        sale:element.sale,
        investment:this.investment.value,
        acheived:this.acheived.value
      }
      paramArr.push(param);
    });

    
  }

}
