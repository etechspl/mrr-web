import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorChemistSaleComponent } from './doctor-chemist-sale.component';

describe('DoctorChemistSaleComponent', () => {
  let component: DoctorChemistSaleComponent;
  let fixture: ComponentFixture<DoctorChemistSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorChemistSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorChemistSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
