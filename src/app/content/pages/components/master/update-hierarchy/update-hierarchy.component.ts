import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggle, MatSlideToggleChange, MatSort, MatTableDataSource } from '@angular/material';
import { values } from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { DesignationService } from '../../../../../core/services/Designation/designation.service';
import { DistrictService } from '../../../../../core/services/district.service';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { StateService } from '../../../../../core/services/state.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { DivisionService } from '../../../../../core/services/Division/division.service';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';
import { DivisionComponent } from '../../filters/division/division.component';

@Component({
  selector: 'm-update-hierarchy',
  templateUrl: './update-hierarchy.component.html',
  styleUrls: ['./update-hierarchy.component.scss']
})
export class UpdateHierarchyComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser)
  
  desgInfo: any;
  stateData: any;
  IS_DIVISION_EXIST;
  // stateIds: any;
  // stateIdsLength: any;
  showData: boolean = false;
  selection = new SelectionModel(true, []);
  selection1 = new SelectionModel(true, []);
  desig: any;
  statein:any;
  isToggled: boolean = true;
  userData: any = [];
  userId: any;
  dataSource: MatTableDataSource<any>;
  dataSource1: MatTableDataSource<any>;
  stateName: any;
  displayedColumns = ['select', 'state','name' ,'designation'];
  displayedColumns1 = ['select','state','name', 'designation'];
  showProcessing: boolean = false;
  primaryContact: any;
  isShowDivision: boolean = false;
  divisionData: any;

  constructor(
    private _designationService: DesignationService,
    private _stateService: StateService,
    private _toastrService: ToastrService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _userDetailsService: UserDetailsService,
    private _urlService: URLService,
    private _divisionService : DivisionService,
    private _hierarchyService:HierarchyService
  ) { }

  hierarchyForm = new FormGroup({
    designation: new FormControl(null, Validators.required),
    state: new FormControl(null, Validators.required),
    division : new FormControl(null)
  })

  @ViewChild('callStateComponent') callStateComponent: StateComponent;
  @ViewChild('callEmployeeComponent') callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;

   ngOnInit() {
    this.IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
    if (this.IS_DIVISION_EXIST === true) {
      this.isShowDivision = true;
      this.hierarchyForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

     this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }
  }


  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked
  }

  getDesignation(val){
    this.desig = val.designation;
    this.hierarchyForm.patchValue({ designation: val });
  }

  getStateValue(val) {
   
    this.hierarchyForm.patchValue({ state: val });
    
  }
  getDivisionValue($event) {
    this.hierarchyForm.patchValue({ division: $event });
    if (this.IS_DIVISION_EXIST === true) {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.hierarchyForm.value.division,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }
    
  }
  

  getData() {
   
    this.isToggled = false;
    this.showProcessing = true;
    let companyId = this.currentUser.companyId;
    let status = true;
    let i = 0;
    let data = [];
    let obj = {
      companyId: this.currentUser.companyId,
      stateId: this.hierarchyForm.value.state,
      designationId:this.hierarchyForm.value.designation.designationLevel
    }
    console.log('this.obj',obj);

    console.log(this.hierarchyForm.value);



    if(this.currentUser.company.isDivisionExist === true){
      this._userDetailsService.getEmployeeOnStateDesignationAndDivision(
        this.currentUser.companyId,
        this.hierarchyForm.value.state,
        this.hierarchyForm.value.designation.designationLevel,
        [true],this.desig,
        this.hierarchyForm.value.division
      ).subscribe((res) => {
        console.log("employee",res)
        if (res == null || res == undefined || res == 0) {
          this.isToggled = true;
          this.showProcessing = false;
          this.showData = false;
          this._toastrService.error("Selected Data Not Found");
        } else {
          this.showProcessing = false;
          this.showData = true;
          this.isToggled = false;
  
          this.dataSource = new MatTableDataSource(res)
  
          this._changeDetectorRef.detectChanges();
        }
      })
  
      this._userDetailsService.getAllManagerOnDivision(companyId,this.hierarchyForm.value.division).subscribe((res) => {
          
        data = res.filter((v) => {
          if (v.designationLevel > this.hierarchyForm.value.designation.designationLevel)
            return v;
        })
        this.dataSource1 = new MatTableDataSource(data);
  
        this._changeDetectorRef.detectChanges();
      })
    }else{
      this._userDetailsService.getuserList(obj).subscribe((res:any)=>{
        console.log("res : ",res);
        if(res.length==0||res == undefined){
          this._toastrService.info('NO Data Found');
        }else{
          this.showProcessing = false;
          this.showData = true;
          this.isToggled = false;
          this.dataSource=new MatTableDataSource(res);
         
          this._changeDetectorRef.detectChanges();
        }
      })

      this._userDetailsService.getAllManager(this.currentUser.companyId).subscribe(res=>{
        
        if(res.length==0||res == undefined){
          this._toastrService.info('No Manager Data Found');
        }else{
          data = res.filter((v) => {
            // console.log("v value",v)
            if (v.designationLevel > this.hierarchyForm.value.designation.designationLevel)
              return v;
          })
          this.dataSource1 = new MatTableDataSource(data);
         
          this._changeDetectorRef.detectChanges();
        }
      })

    }
    

  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(element => {
        this.selection.select(element)
      });
  }

  isAllSelected1() {
    const numSelected1 = this.selection1.selected.length;
    const numRows = this.dataSource1.data.length;
    return numSelected1 === numRows;
  }

  primaryClick(element){
    this.primaryContact=element;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle1() {
    this.isAllSelected1() ?
      this.selection1.clear() :
      this.dataSource1.data.forEach(element => {
        this.selection1.select(element)
      });
  }

  createHierarchy(selectedDetail1, selectedData2) {
   
    let manager = [];
    let params = {};
    let i = 0
    params['employee'] = selectedDetail1;
    params['employeeDesignation'] = this.hierarchyForm.value.designation.designationLevel;
    params['designationName'] = this.hierarchyForm.value.designation.designation;
    params['managerId'] = selectedData2.id;
    params['managerName'] =selectedData2.name; 
    params['managerDesignation'] =selectedData2.designation; 
    params['managerDesignationLevel'] =selectedData2.designationLevel; 
    params['companyId'] = this.currentUser.companyId;
    params['division']= this.hierarchyForm.value.divisionId;
    console.log("params------>",params);
    this._hierarchyService.hierarchyMappingWithTeam(params).subscribe((hierarchyResponse) => {
     console.log(hierarchyResponse, 'inside api call');
      // if(hierarchyResponse){
      //   this._toastrService.success(
      //     "Hierarchy Mapping Successfully"
      //   );
      //   this.showData=false;
      //   this._changeDetectorRef.detectChanges();
      // }
      // else{
      //   this._toastrService.error(
      //     "Seletced Employees are Already Mapped"
      //   );
      // }
      this._toastrService.success(
        "Hierarchy Mapping Successfully"
      );
      this.showData=false;
      this._changeDetectorRef.detectChanges();
    },
    (err) => {
      // console.log(err);
      this._toastrService.error(
        "Seletced Employees are Already Mapped"
      );
    }
  );

  }
}
