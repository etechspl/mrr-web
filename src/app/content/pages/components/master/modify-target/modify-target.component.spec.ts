import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyTargetComponent } from './modify-target.component';

describe('ModifyTargetComponent', () => {
  let component: ModifyTargetComponent;
  let fixture: ComponentFixture<ModifyTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
