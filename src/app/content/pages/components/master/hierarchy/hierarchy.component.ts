import { EmployeeComponent } from './../../filters/employee/employee.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HierarchyService } from './../../../../../core/services/hierarchy-service/hierarchy.service';
import { URLService } from './../../../../../core/services/URL/url.service';
import { Component, OnInit, ViewChild,ChangeDetectorRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ToastrService } from 'ngx-toastr'; 
import { DesignationComponent } from '../../filters/designation/designation.component';
import { StateComponent } from '../../filters/state/state.component';
import { MatSlideToggleChange } from '@angular/material';
@Component({
  selector: 'm-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss']
})
export class HierarchyComponent implements OnInit {
  public isShowMappedData: boolean = false;
  dataSource;
  selection = new SelectionModel(true, []);
  desig : string;
  displayedColumns = ['select', 'sn', 'stateName', 'districtName', 'name', 'designation'];
  ngOnInit() {
  }

  isToggled = true; 
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  constructor(
     public urlService: URLService,
      public hierarchyService: HierarchyService,
      private _changeDetectorRef:ChangeDetectorRef,
      private toastr: ToastrService
      ) {}
      
  currentUser = JSON.parse(sessionStorage.currentUser);
  hierarchyForm = new FormGroup({
    stateInfo: new FormControl(null, Validators.required),
    userId: new FormControl(null, Validators.required),
    designation: new FormControl(null, Validators.required)
  })

  
  getDesignation(val) {
    this.desig = val.designation;
    this.hierarchyForm.patchValue({ designation: val });
    this.hierarchyForm.patchValue({ stateInfo: null }); 
    this.hierarchyForm.patchValue({ userId: null });
    this.callEmployeeComponent.setBlank();
    this.callEmployeeComponent.removelist();
    this.callStateComponent.setBlank();
    
  }

  getStateValue(val) {
    this.hierarchyForm.patchValue({ stateInfo: val }); 
    this.hierarchyForm.patchValue({ userId: null });
    this.callEmployeeComponent.getMgrList(this.currentUser.companyId,this.hierarchyForm.value.stateInfo, this.hierarchyForm.value.designation.designationLevel, [true], this.desig);  
    this.callEmployeeComponent.setBlank();
  }

  getEmployees(val) { 
    this.hierarchyForm.patchValue({ userId: val });
  }

  getHierarchy() {
    var object = {
      companyId: this.currentUser.companyId,
      supervisorId: this.hierarchyForm.value.userId,
      type:'lower'
    };
    this.hierarchyService.getManagerHierarchy(object).subscribe(res => {
      let resultArr=[];
      resultArr.push(res);
        if (resultArr[0].length == 0 || res == undefined) {
          this.isShowMappedData = false;
          this.toastr.info("Mapping Data Not Found");
        } else {
          this.isToggled = false;
          this.isShowMappedData = true;
          this.dataSource = res;
        }
        this._changeDetectorRef.detectChanges();
      },
      err => {
        console.log(err);
      }
    )

  }

  //delete Hierarchy
  deleteHierarchy(selectedDetail) {
    this.hierarchyService.deleteHierarchys(selectedDetail)
      .subscribe(hierarchy => {
        this.toastr.info('Hierarchy Deleted Successfully.','');
        this.getHierarchy();
      }, err => {
        this.toastr.info('Hierarchy Deleted Successfully.','');
        this.getHierarchy();
      });
      this._changeDetectorRef.detectChanges();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(element => {
        this.selection.select(element)
      });
  }

}
