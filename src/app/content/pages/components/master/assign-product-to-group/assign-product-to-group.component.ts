import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatSlideToggleChange, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { ProductsComponent } from '../../filters/products/products.component';
import { StateComponent } from '../../filters/state/state.component';

@Component({
  selector: 'm-assign-product-to-group',
  templateUrl: './assign-product-to-group.component.html',
  styleUrls: ['./assign-product-to-group.component.scss']
})
export class AssignProductToGroupComponent implements OnInit {
  isShowDivision = false;
  public showDivisionFilter: boolean = false;
  assignProductForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  companyId = this.currentUser.companyId;
  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;

  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callProductComponent") callProductComponent: ProductsComponent;
  
  constructor(public dialogRef: MatDialogRef<AssignProductToGroupComponent>,
    private toastrService: ToastrService, 
    private _productService: ProductService,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder) {
    this.assignProductForm = this.fb.group({
      id: this.data.val.id,
      groupName : this.data.val.proGrpName,
      stateInfo: [null, Validators.required],
      productInfo: [null, Validators.required],
    })
  }

  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      this.assignProductForm.get('divisionId').setValidators([Validators.required])
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.isShowDivision = true;
    }
    
  }
  isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
  }
  
  
  
    getDivisionValue($event) {
      this.assignProductForm.patchValue({ stateInfo: null });
      (this.callStateComponent) ? this.callStateComponent.setBlank() : null;
      this.assignProductForm.patchValue({ divisionId: $event });
      if (this.IS_DIVISION_EXIST === true) {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.assignProductForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
  
      }
    }
    
    getStateValue(val) {
    this.assignProductForm.patchValue({ stateInfo: val });
    this.callProductComponent.getProductsDataForGroup(this.companyId,this.IS_DIVISION_EXIST,this.assignProductForm.value.divisionId);
    }

    getProductsDataForGroup(val) {
      this.assignProductForm.patchValue({ productInfo: val });
      }
     
 
  SubmitGroup(){
    
    this._productService.assignProductToGroup(this.assignProductForm.value, this.currentUser.company.id).subscribe(response => {
      this.toastrService.success('Assign Products Successfully...');
      this.dialogRef.close();
    }, err => { 
      console.log(err);
      this.toastrService.error('Error in Assign Products');
    });
  }

}
