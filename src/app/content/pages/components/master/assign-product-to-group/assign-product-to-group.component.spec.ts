import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignProductToGroupComponent } from './assign-product-to-group.component';

describe('AssignProductToGroupComponent', () => {
  let component: AssignProductToGroupComponent;
  let fixture: ComponentFixture<AssignProductToGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignProductToGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignProductToGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
