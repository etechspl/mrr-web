import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TypeComponent } from '../../filters/type/type.component';
import { BroadcastMessageService } from '../../../../../core/services/BroadcastMessage/broadcast-message.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { MatTableDataSource, MatTable, MatSort, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import { SelectionModel } from '@angular/cdk/collections';
import { StateComponent } from '../../filters/state/state.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { FromdateComponent } from '../../filters/fromdate/fromdate.component';

@Component({
  selector: 'm-broadcast-message',
  templateUrl: './broadcast-message.component.html',
  styleUrls: ['./broadcast-message.component.scss']
})
export class BroadcastMessageComponent implements OnInit {

  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  
  displayedColumns = ['select','sn','state', 'district', 'userInfo', 'subject', 'message', 'validity'];
  dataSource = new MatTableDataSource([]); ;
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent:TypeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callFromdateComponent") callFromdateComponent: FromdateComponent;
  show = true;
  selection = new SelectionModel(true, []);
  constructor( private fb: FormBuilder,private _userdetails:UserDetailsService,  private _changeDetectorRef:ChangeDetectorRef,private _broadcastmessageservice:BroadcastMessageService) { }
  showAdminAndMGRLevelFilter = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  broadcastMessageForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    designation: new FormControl(null),
    validity: new FormControl(null,Validators.required),
    Message:new FormControl(null,Validators.required),
    Subject:new FormControl(null,Validators.required)
    })
  check: boolean = false;
  getReportType(val) {
    this.broadcastMessageForm.patchValue({reportType: val});
    this.broadcastMessageForm.patchValue({type: null});
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.showDistrict = false;
      // Implement by Anjana 29-08-2019 
      if(this.broadcastMessageForm.value.type=="Employee Wise"){
        this.showEmployee = true;
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }


  broadcastMessage: FormGroup = this.fb.group({
    formarray: this.fb.array([])
  });
  get formarray() {
    return this.broadcastMessage.get('formarray') as FormArray;
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      this.showDistrict = false;

      this.broadcastMessageForm.get('stateInfo').setValidators([Validators.required])
      this.broadcastMessageForm.patchValue({ stateInfo: null});
      (this.callStateComponent)?this.callStateComponent.setBlank():null;

      this.broadcastMessageForm.get('districtInfo').clearValidators();
      this.broadcastMessageForm.patchValue({ districtInfo: null});
      (this.callDistrictComponent)?this.callDistrictComponent.setBlank():null;
      
      this.broadcastMessageForm.get('designation').clearValidators();
      this.broadcastMessageForm.patchValue({ designation: null});
      (this.callDesignationComponent)?this.callDesignationComponent.setBlank():null;

      this.broadcastMessageForm.get('employeeId').clearValidators();
      this.broadcastMessageForm.patchValue({ employeeId: null});
      if(this.callEmployeeComponent){
        this.callEmployeeComponent.setBlank();
        this.callEmployeeComponent.removelist()
      }

    } else if (val == "Headquarter") {
      
      this.showDistrict = true;
            
      this.broadcastMessageForm.get('stateInfo').setValidators([Validators.required])
      this.broadcastMessageForm.patchValue({ stateInfo: null});
      this.callStateComponent.setBlank();

      this.broadcastMessageForm.get('districtInfo').setValidators([Validators.required]);
      this.broadcastMessageForm.patchValue({ districtInfo: null});
      
      this.broadcastMessageForm.get('designation').clearValidators();
      this.broadcastMessageForm.patchValue({ designation: null});
      (this.callDesignationComponent)?this.callDesignationComponent.setBlank():null;

      this.broadcastMessageForm.get('employeeId').clearValidators();
      this.broadcastMessageForm.patchValue({ employeeId: null});
      if(this.callEmployeeComponent){
        this.callEmployeeComponent.setBlank();
        this.callEmployeeComponent.removelist()
      }

    } else if (val == "Employee Wise") {

      this.showEmployee = true;
      
      this.broadcastMessageForm.get('designation').setValidators([Validators.required]);
      this.broadcastMessageForm.patchValue({ designation: null });
      (this.callDesignationComponent)?this.callDesignationComponent.setBlank():null;

      this.broadcastMessageForm.get('employeeId').setValidators([Validators.required]);
      this.broadcastMessageForm.patchValue({ employeeId: null });
      if(this.callEmployeeComponent){
        this.callEmployeeComponent.setBlank();
        this.callEmployeeComponent.removelist()
      }

      this.broadcastMessageForm.get('stateInfo').clearValidators()
      this.broadcastMessageForm.patchValue({ stateInfo: null});
      (this.callStateComponent)?this.callStateComponent.setBlank():null;

      this.broadcastMessageForm.get('districtInfo').clearValidators();
      this.broadcastMessageForm.patchValue({ districtInfo: null});
      (this.callDistrictComponent)?this.callDistrictComponent.setBlank():null;
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    this.broadcastMessageForm.patchValue({ type: val });

  }
  getStateValue(val) {
    this.broadcastMessageForm.patchValue({ stateInfo: val });

    if (this.broadcastMessageForm.value.type == "Headquarter") {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);
      this.callDistrictComponent.setBlank();
      this.broadcastMessageForm.patchValue({ districtInfo: null});
    }
  }
  getDistrictValue(val) {
    this.broadcastMessageForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.broadcastMessageForm.patchValue({ employeeId: val });
  }
  getValidDate(val) {
    this.broadcastMessageForm.patchValue({ validity: val });

  }


  getDesignation(val) {
    this.broadcastMessageForm.patchValue({ designation: val });
    this.broadcastMessageForm.patchValue({ employeeId: null });
    this.callEmployeeComponent.setBlank();
    this.callEmployeeComponent.removelist();
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
  }
  
  ngOnInit() {
    this. getBroadcastMessageDetail();
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      
    } else {
      this.dataSource.data.forEach((row, index) => {
        this.selection.select(row);
      })

    }
  }

 
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected1 = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected1 === numRows;
  }

  
  submitRecords(){
    let obj={};
    if(this.broadcastMessageForm.value.type=="State"){
      obj={
        companyId:this.currentUser.companyId,
        type:this.broadcastMessageForm.value.type,
        stateId:this.broadcastMessageForm.value.stateInfo,
        rL:0
      }

    }else if(this.broadcastMessageForm.value.type=="Headquarter"){
      obj={
        companyId:this.currentUser.companyId,
        stateId:this.broadcastMessageForm.value.stateInfo,
        districtIds:this.broadcastMessageForm.value.districtInfo,
        status:true,
        rL:0
      }
    }else {
      obj={
        companyId:this.currentUser.companyId,
        type:this.broadcastMessageForm.value.type,
        employeeId:this.broadcastMessageForm.value.employeeId,
        rL:0
      }
    }
    this._userdetails.getUserInfoOnTypeBasis(obj).subscribe(res=>{
      let finalArray = []
      let responseArray = JSON.parse(JSON.stringify(res));
          if(responseArray.length){
            responseArray.forEach((value, index)=>{
            let obj={
            "userId":value.userId,
            "companyId":this.currentUser.companyId,
            "stateId":value.stateId,
            "districtId":value.districtId,
            "subject":this.broadcastMessageForm.value.Subject,
            "message":this.broadcastMessageForm.value.Message,
            "validity":this.broadcastMessageForm.value.validity,
            "status":true
          }
          finalArray.push(obj);
          if((responseArray.length - 1) == index){
            this._broadcastmessageservice.createBroadcaseMessgae(finalArray).subscribe(result=>{
            this.reset();
            this.getBroadcastMessageDetail();
            Swal.fire(
              'Broadcast Message Successfully Added!',
              'success'
            )
            this._changeDetectorRef.detectChanges()
  
          })
          }
          });
          } else {
            Swal.fire(
              'In Selected Options there is no User found!!!'
            )
          }
         
      
    })
  }
  getBroadcastMessageDetail(){
    this._broadcastmessageservice.getBroadcastMessageDetail(this.currentUser.companyId).subscribe(res => {
      
      if (JSON.parse(JSON.stringify(res)).length == 0 || res == undefined) {
        this.dataSource.data=[];
      } else {
        this.dataSource.data = JSON.parse(JSON.stringify(res));
        // override filterPredicate method of MatTableDataSource Filter- To apply filter on nested object in mat-table
        this.dataSource.filterPredicate = (data, filter: string)  => {
          const accumulator = (currentTerm, key) => {
            return this.nestedFilterCheck(currentTerm, data, key);
          };
          const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
          // Transform the filter by converting it to lowercase and removing whitespace.
          const transformedFilter = filter.trim().toLowerCase();
          return dataStr.indexOf(transformedFilter) !== -1;
        };

      }
      this._changeDetectorRef.detectChanges();
    }, err => {
    });

  }
  nestedFilterCheck(search, data, key) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
        }
      }
    } else {
      search += data[key];
    }
    return search;
  }
  // method Called when anything type on Search Text Field
   applyFilter(filterValue: string) {
      
     this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {

    this.broadcastMessageForm.reset();
    (this.callDistrictComponent)?this.callDistrictComponent.setBlank():null;
    (this.callEmployeeComponent)?this.callEmployeeComponent.setBlank():null;
    (this.callTypeComponent)?this.callTypeComponent.setBlank():null;
    (this.callStateComponent)?this.callStateComponent.setBlank():null;
    (this.callDesignationComponent)?this.callDesignationComponent.setBlank():null;
    (this.callFromdateComponent)?this.callFromdateComponent.setBlank():null;

   }
   deleteMessage(){
     let obj=this.selection.selected
     let messageId=[];
     for(let n=0;n<obj.length;n++){
      messageId.push(obj[n].id)
     }
     this._broadcastmessageservice.deleteBroadcastMessages(messageId).subscribe(res=>{
      this.show = false;
       if(res!=undefined){
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            this.selection.clear() 
            this.getBroadcastMessageDetail();
          }
        })
      
       }
        
     })
   }

  
  
}
