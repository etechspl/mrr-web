import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferMasterDataComponent } from './transfer-master-data.component';

describe('TransferMasterDataComponent', () => {
  let component: TransferMasterDataComponent;
  let fixture: ComponentFixture<TransferMasterDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferMasterDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferMasterDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
