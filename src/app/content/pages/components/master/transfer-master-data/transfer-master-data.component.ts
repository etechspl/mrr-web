import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import {
	FormBuilder,
	Validators,
	FormControl,
	FormGroup,
} from "@angular/forms";
import { DistrictComponent } from "../../filters/district/district.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { SelectionModel } from "@angular/cdk/collections";
import { AreaService } from "../../../../../core/services/Area/area.service";
import { UserAreaMappingService } from "../../../../../core/services/UserAreaMpping/user-area-mapping.service";
import { DataTransferHistoryService } from "../../../../../core/services/data-transfer-history.service";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
import { DivisionComponent } from "../../filters/division/division.component";
import { StateComponent } from "../../filters/state/state.component";
import { UserDetailsService } from "../../../../../core/services/user-details.service";
import { MatSlideToggleChange } from "@angular/material";
import Swal from "sweetalert2";
@Component({
	selector: "m-transfer-master-data",
	templateUrl: "./transfer-master-data.component.html",
	styleUrls: ["./transfer-master-data.component.scss"],
})
export class TransferMasterDataComponent implements OnInit {
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callEmployeeComponent1")
	callEmployeeComponent1: EmployeeComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;

	showSampleType = false;
	showTransferData = false;
	showTransfer = false;
	showSampleState = false;
	showHq = false;
	showEmp = false;
	showGiftsType = false;
	showTransferProviders = false;
	designationData: any;
	showProductsStateWise = false;
	showTransferProvidersData = false;
	dataSource;
	dataSource1;
	dataSource2;
	newUserArea = false;

	selection = new SelectionModel(true, []);
	selection1 = new SelectionModel(true, []);
	selection2 = new SelectionModel(true, []);

	displayedColumns = ["select", "providerName"];
	displayedColumns1 = ["select", "areaName"];
	constructor(
		private changeDetectorRef: ChangeDetectorRef,
		private toastrService: ToastrService,
		private fb: FormBuilder,
		private AreaService: AreaService,
		private UserAreaMappingService: UserAreaMappingService,
		private DataTransferHistoryService: DataTransferHistoryService,
		private ProviderService: ProviderService,
		private UserDetailsService: UserDetailsService
	) {
		this.transferMasterData = this.fb.group({
			stateInfo: new FormControl(null, Validators.required),
			employeeId: new FormControl(null, Validators.required),
			employeeIdTo: new FormControl(null, Validators.required),
			// districtId: new FormControl(null, Validators.required),
			// designation: new FormControl(null, Validators.required),
			transferType: new FormControl(null),
			transferFinalData: new FormControl(null),
			newUserAreasId: new FormControl(null),
		});
	}
	currentUser = JSON.parse(sessionStorage.currentUser);
	isShowDivision = false;
	isDivisionExist = this.currentUser.company.isDivisionExist;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	companyId = this.currentUser.companyId;
	public showState: boolean = true;
	districtAndDesignationLevelOfEmployee = [];
	transferMasterData = new FormGroup({
		stateInfo: new FormControl(null),
		employeeId: new FormControl(null),
		employeeIdTo: new FormControl(null),
		// districtId: new FormControl(null),
		// designation: new FormControl(null),
		transferType: new FormControl(null),
		transferFinalData: new FormControl(null),
		newUserAreasId: new FormControl(null),
	});

	ngOnInit() {
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.transferMasterData.setControl("divisionId", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};

			if (this.currentUser.userInfo[0].rL > 1) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}
		this.transferType("Area");
	}

	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}

	getDivisionValue($event) {
		this.callStateComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		this.callEmployeeComponent1.setBlank();
		this.callEmployeeComponent.removelist();
		this.callEmployeeComponent1.removelist();
		this.transferMasterData.patchValue({ divisionId: $event });
		if (this.isDivisionExist === true) {
			this.callStateComponent.getStateBasedOnDivision({
				companyId: this.currentUser.companyId,
				isDivisionExist: this.isDivisionExist,
				division: [this.transferMasterData.value.divisionId],
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			});
		}
	}

	transferType(val) {
		this.transferMasterData.patchValue({ transferType: val });
		if (val === "Area") {
			this.showTransfer = true;
			this.showHq = true;
			this.showEmp = true;
			this.showTransferData = false;
			this.showTransferProviders = false;
			this.showTransferProvidersData = false;
			this.newUserArea = false;
		} else if (val === "RMP") {
			this.showTransfer = true;
			this.showHq = true;
			this.showEmp = true;
			this.showTransferData = false;
			this.showTransferProvidersData = false;
			this.newUserArea = false;
		} else if (val === "Drug") {
			this.showTransfer = true;
			this.showHq = true;
			this.showTransferData = false;
			this.showEmp = true;
			this.showTransferProvidersData = false;
			this.newUserArea = false;
		} else if (val === "Stockist") {
			this.showTransfer = true;
			this.showTransferData = false;
			this.showHq = true; 
			this.showEmp = true;
			this.showTransferData = false;
			this.showTransferProvidersData = false;
			this.newUserArea = false;
		}
	}

	getStateValue(val) {
		this.transferMasterData.patchValue({ stateInfo: val });
		let obj = {
			companyId: this.currentUser.companyId,
			stateId: val,
		};
		if (this.isDivisionExist === true) {
			obj["divisionId"] = this.transferMasterData.value.divisionId;
		}
		this.callEmployeeComponent.setBlank();
		this.callEmployeeComponent1.setBlank();
		this.callEmployeeComponent.getEmployeeListOfStates(obj);
		this.callEmployeeComponent1.getEmployeeListOfStates(obj);
		this.transferMasterData.patchValue({ employeeId: null });
		this.transferMasterData.patchValue({ employeeIdTo: null });
		// this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);
		// })
	}

	// getDistrictValue(val) {
	//   this.transferMasterData.patchValue({ districtId: val });
	//   this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, [val]);
	//   this.callEmployeeComponent1.getEmployeeListOnDistricts(this.currentUser.companyId, [val]);

	// }

	// getDesignation(val) {
	//   this.showTransferData = false;
	//   this.transferMasterData.patchValue({ designation: [val] });
	// }

	getEmployeeValue($event) {
		this.showTransferData = false;
		this.transferMasterData.patchValue({ employeeId: $event });
		// By Aakash on (04-03-2020)
		this.callEmployeeComponent.stateEmployee.map((a) => {
			if (a.userId == $event) {
				this.districtAndDesignationLevelOfEmployee.push(a.districtId);
				this.districtAndDesignationLevelOfEmployee.push(
					a.designationLevel
				);
			}
		});
	}
	getEmployeeValueTo(val) {
		this.showTransferData = false;
		this.transferMasterData.patchValue({ employeeIdTo: val });
	}

	//------------getting providers by Preeti Arora------------------//

	TransferProvider(ProvidersIds) {
		!this.changeDetectorRef["destroyed"]
			? this.changeDetectorRef.detectChanges()
			: null;
		this.UserAreaMappingService.getProvidersList(
			this.currentUser.companyId,
			"Area",
			[ProvidersIds.stateId],
			[ProvidersIds.districtId],
			[ProvidersIds.id],
			[this.transferMasterData.value.employeeId],
			[this.transferMasterData.value.transferType],
			[true],
			"",
			"",
			"",
			""
		).subscribe(
			(resProviders) => {
				if (resProviders.length > 0) {
					this.dataSource1 = resProviders;
					this.showTransferProvidersData = true;
					this.newUserArea = true;
					this.showTransferData = false;
					!this.changeDetectorRef["destroyed"]
						? this.changeDetectorRef.detectChanges()
						: null;
				} else {
					this.toastrService.error("No Data Found");
					this.showTransferProvidersData = false;
					this.newUserArea = false;
					!this.changeDetectorRef["destroyed"]
						? this.changeDetectorRef.detectChanges()
						: null;
				}
			},
			(err) => {
				console.log(err);
			}
		);
	}
	//------------getting new user areaid----
	newUserAreaId(getAreaId) {
		this.transferMasterData.patchValue({ newUserAreasId: getAreaId });
	}
	//---------------------final provider data transfer-------------
	TransferProviderData(oldUserProviderIds) {
    
		let oldUserProvidersIds = [];
		this.showTransferData = false;
		this.showTransferProviders = true;
		this.showTransferProvidersData = false;
		this.newUserArea = false;
		for (var i = 0; i < oldUserProviderIds.length; i++) {
			oldUserProvidersIds.push(oldUserProviderIds[i]._id);
		}

		!this.changeDetectorRef["destroyed"]
			? this.changeDetectorRef.detectChanges()
			: null;
		this.ProviderService.transferProviderToNewUser(
			this.currentUser.companyId,
			this.transferMasterData.value.newUserAreasId,
			oldUserProviderIds,
			this.transferMasterData.value.employeeId,
			this.transferMasterData.value.employeeIdTo
		).subscribe(
			(res) => {
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
				this.DataTransferHistoryService.transferRecord(
					this.currentUser.companyId,
					oldUserProvidersIds,
					this.transferMasterData.value.employeeId,
					this.transferMasterData.value.employeeIdTo,
					this.transferMasterData.value.transferType,
					this.currentUser.companyId
				).subscribe(
					(res) => {
						this.selection1.clear();
						!this.changeDetectorRef["destroyed"]
							? this.changeDetectorRef.detectChanges()
							: null;
						// this.toastrService.success(this.transferMasterData.value.transferType + ' has been transferred sucessfully');
						(Swal as any).fire({
							title: `${this.transferMasterData.value.transferType} has been transferred sucessfully !!`,
							type: "success",
						});

					},
					(err) => {
						console.log(err);
					}
				);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	//------------transfer Area by Preeti Arora------------------//

	TransferArea(selectedArea) {
		!this.changeDetectorRef["destroyed"]
			? this.changeDetectorRef.detectChanges()
			: null;
		this.showTransferData = false;
		this.UserAreaMappingService.transferAreaToNewUser(
			this.currentUser.companyId,
			selectedArea,
			this.transferMasterData.value.employeeId,
			this.transferMasterData.value.employeeIdTo
		).subscribe(
			(res) => {
				this.showTransferData = false;
				//------------------------------------------
				this.ProviderService.updateUserIdInProviders(
					this.currentUser.companyId,
					selectedArea,
					this.transferMasterData.value.employeeId,
					this.transferMasterData.value.employeeIdTo
				).subscribe(
					(res) => {
						!this.changeDetectorRef["destroyed"]
							? this.changeDetectorRef.detectChanges()
							: null;
					},
					(err) => {
						console.log(err);
					}
				);
				//------------------------------------
				this.DataTransferHistoryService.transferRecord(
					this.currentUser.companyId,
					selectedArea,
					this.transferMasterData.value.employeeId,
					this.transferMasterData.value.employeeIdTo,
					this.transferMasterData.value.transferType,
					this.currentUser.companyId
				).subscribe(
					(res) => {
						this.showTransferData = false;
						this.selection.clear();
						// this.toastrService.success(this.transferMasterData.value.transferType + ' has been transferred sucessfully');
						(Swal as any).fire({
							title: `${this.transferMasterData.value.transferType} has been transferred sucessfully !!`,
							type: "success",
						});
					},
					(err) => {
						console.log(err);
					}
				);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	//---------------getting details of area----------------------
	getDetails() {
		this.AreaService.getAreaList(
			this.currentUser.companyId,
			this.districtAndDesignationLevelOfEmployee[0],
			this.districtAndDesignationLevelOfEmployee[1],
			[this.transferMasterData.value.employeeId]
		).subscribe(
			(GetOldUserArea) => {
				if (this.transferMasterData.value.transferType == "Area") {
					this.showTransferProviders = false;
					this.showTransferData = true;
					this.showTransferProvidersData = false;
					this.newUserArea = false;
					!this.changeDetectorRef["destroyed"]
						? this.changeDetectorRef.detectChanges()
						: null;
				} else {
					this.showTransferProviders = true;
				}
				this.dataSource = GetOldUserArea;
				this.dataSource.sort((a, b) =>
					a.areaName.localeCompare(b.areaName)
				);
				this.isToggled = false;
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
			},
			(err) => {
				console.log(err);
			}
		);

		this.AreaService.getAreaList(
			this.currentUser.companyId,
			this.districtAndDesignationLevelOfEmployee[0],
			this.districtAndDesignationLevelOfEmployee[1],
			[this.transferMasterData.value.employeeIdTo]
		).subscribe(
			(GetNewUserArea) => {
				this.dataSource2 = GetNewUserArea;
				this.dataSource2.sort((a, b) =>
					a.areaName.localeCompare(b.areaName)
				);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.length;
		return numSelected === numRows;
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.forEach((element) => {
					this.selection.select(element);
			  });
	}
	isAllSelected1() {
		const numSelected = this.selection1.selected.length;
		const numRows = this.dataSource1.length;
		return numSelected === numRows;
	}

	masterToggle1() {
		this.isAllSelected1()
			? this.selection1.clear()
			: this.dataSource1.forEach((element) => {
					this.selection1.select(element);
			  });
  }

}
