import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { STPService } from '../../../../../core/services/stp.service';
import { ToastrService } from 'ngx-toastr';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MatTableDataSource, MatTable, MatSort, MatPaginator, MatSlideToggleChange } from '@angular/material';
import { StpApprovalService } from '../../../../../core/services/stp-approval.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EditStpComponent } from '../edit-stp/edit-stp.component';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
@Component({
  selector: 'm-stpdetails',
  templateUrl: './stpdetails.component.html',
  styleUrls: ['./stpdetails.component.scss']
})
export class STPDetailsComponent implements OnInit {

  disableDeleteBtn = false;
  selectedItems= [];
  itemsToBeSelected= [];
  selection = new SelectionModel<any>(true, []);
  dialogRef: any = null;
  dialogConfig = new MatDialogConfig();

  constructor(

    private stpApprovalService: StpApprovalService,
    private fb: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    private stpService: STPService,
    private _toastr: ToastrService,
	private dialog: MatDialog,
	private exportAsService: ExportAsService
  ) {

  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  ngOnInit() {
    this.createSTPForm();
  }

  //variable declaration
  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
  isShowType: boolean = false;
  isShowState: boolean = false;
  isShowDistrict: boolean = false;
  isShowUser: boolean = false;
  showProcessing : boolean = false;
  check: boolean = false;
  isShowData: boolean = false;
  stpDetailsForm: FormGroup;

  displayedColumns2 = ['select', 'sn', 'fromArea', 'toArea', 'type', 'distance', 'freqVisit', 'appByMgr', 'appByAdm','submissionDate','mgrAppDate','finalAppDate','edit'];
  dataSource2 = new MatTableDataSource<any>();

  @ViewChild("districtComponent") districtComponent: DistrictComponent;
  @ViewChild("employeeComponent") employeeComponent: EmployeeComponent;

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator)
  set paginator(value: MatPaginator) {
    this.dataSource2.paginator = value;
  }
  isLoading: false;
  //form creation
  createSTPForm() {
    if (this.currentUser.userInfo[0].rL === 0) {
      this.stpDetailsForm = this.fb.group({
        //type: new FormControl(''),
        stateId: new FormControl('', Validators.required),
        districtId: new FormControl('', Validators.required),
        userId: new FormControl('', Validators.required)
      })
    } else if (this.currentUser.userInfo[0].rL > 1)
      this.stpDetailsForm = this.fb.group({
        type: new FormControl('', Validators.required),
        //stateId: new FormControl(''),
        //districtId: new FormControl(''),
        // userId: new FormControl('')
      })
  }
  //set State Value
  getStateValue(stateId: string) { 
    // CLEAR FILTERS
    this.districtComponent.setBlank();
    this.employeeComponent.setBlank();
    // CLEAR FILTERS
    this.stpDetailsForm.patchValue({ stateId: stateId, districtId: '', userId: '' });
    this.districtComponent.getDistricts(this.currentUser.companyId, stateId,true);
  }
  //get District Value
  getDistrictValue(districtId: string) {
    this.stpDetailsForm.patchValue({ districtId: districtId, userId: '' });
    let obj = {
      companyId: this.currentUser.companyId,
      stateId: this.stpDetailsForm.value.stateId,
      districtId: this.stpDetailsForm.value.districtId
    }
    //this.employeeComponent.getEmployeeListWithMgr(obj);

    this.employeeComponent.getEmployeeListWithMgr(obj);
  }
  getUserId(userId: string) {
    this.stpDetailsForm.patchValue({ userId: userId })
  }

  //get type
  getType($event) {
    if (this.currentUser.userInfo[0].rL > 1) {
      if ($event.value === 'Team') {
        this.stpDetailsForm.addControl('stateId', new FormControl('', Validators.required));
        this.stpDetailsForm.addControl('districtId', new FormControl('', Validators.required));
        this.stpDetailsForm.addControl('userId', new FormControl('', Validators.required));
      } else {
        this.stpDetailsForm.removeControl('stateId');
        this.stpDetailsForm.removeControl('districtId');
        this.stpDetailsForm.removeControl('userId');
      }
    }

  }

  generateReport() {
    this.isToggled = false;
    this.showProcessing= true;
    if (this.stpDetailsForm.valid) {
      let obj = {};
      if (this.currentUser.userInfo[0].rL === 0) {
        obj["companyId"] = this.currentUser.companyId;
        obj["userId"] = this.stpDetailsForm.value.userId;
      } else if (this.currentUser.userInfo[0].rL > 1) {
        obj["companyId"] = this.currentUser.companyId;
        if (this.stpDetailsForm.value.type === 'Self') {
          obj["userId"] = this.currentUser.id;
        } else if (this.stpDetailsForm.value.type === 'Team') {
          obj["userId"] = this.stpDetailsForm.value.userId;
        }
      }
      console.log('DZ',obj);
      
      this.stpService.getSTPDetails(obj).subscribe(res => {
        let array = res.map(item => {
          return {
            ...item
          };
        });
        
        array.sort(function(a, b){
          if(a.fromArea.areaName < b.fromArea.areaName) { return -1; }
          if(a.fromArea.areaName > b.fromArea.areaName) { return 1; }
          
          return 0;
        });

        if (array.length > 0) {
          this.showProcessing= false;
          this.dataSource2 = new MatTableDataSource(array);
          this.itemsToBeSelected = array;
          this.dataSource2.sort = this.sort;
          this.isShowData = true;
          this.dataSource2.paginator = this.paginator;
        } else {
          this._toastr.info("STP Data is not available", "Data not available");
          this.showProcessing= false;
          this.isShowData = false;
        }
        this.changeDetectorRef.detectChanges();
      })
    }
  }

  getSelectionValue($event, i: number, row: any) {
    let id = row.id;
    if(this.selectedItems.indexOf(id) < 0){
      this.selectedItems.push(id);
    } else if(this.selectedItems.indexOf(id) >=0 ) {
      this.selectedItems.splice(this.selectedItems.indexOf(id), 1);
    }
  }

  isAllSelected() {
    const numSelected = this.selectedItems.length;
    const numRows = this.dataSource2.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedItems = [];
    } else {
      this.selectedItems = [];
      this.itemsToBeSelected.forEach((row, index) => {
        this.selection.select(row);
        this.selectedItems.push(row.id);
      })

    }

  }

  deleteSTP(){
    this.stpApprovalService.deleteSTPApprovalReq(this.selectedItems).subscribe(res => {
      if(JSON.parse(res.result).count > 0){
        this._toastr.success(' Selected STP deleted successfully.');
        this.generateReport();
      } else {
        this._toastr.error(' Something went wrong.');
      }
    })
  }

  editStp(event,i,row){
    this.dialogConfig.data = row;
    (this.dialogRef === null)?(this.dialogRef = this.dialog.open(EditStpComponent, this.dialogConfig)):null;
    this.dialogRef.afterClosed().subscribe((res)=> {

      if(res){
        this.generateReport();
      }
      this.dialogRef = null;
    });

  }

  exporttoExcel() {
     const lastCol = this.displayedColumns2.pop();
    // download the file using old school javascript method
    setTimeout (() => {
    this.exportAsService.save(this.exportAsConfig, 'Finalize Expense Report').subscribe(() => {
      // save started

      this.displayedColumns2.push(lastCol)

    });
  }, 300);
  }
}
