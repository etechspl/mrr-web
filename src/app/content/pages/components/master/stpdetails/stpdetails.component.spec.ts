import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { STPDetailsComponent } from './stpdetails.component';

describe('STPDetailsComponent', () => {
  let component: STPDetailsComponent;
  let fixture: ComponentFixture<STPDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ STPDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(STPDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
