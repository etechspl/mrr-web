import { Component } from '@angular/core';
import { FieldArrayType, FormlyFormBuilder } from '@ngx-formly/core';

@Component({
  selector: 'formly-repeat-section',
  template: `

    <div *ngFor="let field of field.fieldGroup; let i = index;" >
      <div *ngIf="field.fieldGroup[0].key=='providerId' || field.fieldGroup[0].key=='vendorProviderId'">
          <p><br/></p>
          <p>
            <b *ngIf="field.fieldGroup[0].key=='providerId'">{{i+1}}. {{this.currentUser.company.lables.doctorLabel}} Visited Info</b>
           <b *ngIf="field.fieldGroup[0].key=='vendorProviderId'">{{i+1}}. {{this.currentUser.company.lables.vendorLabel}} / {{this.currentUser.company.lables.stockistLabel}} Visited Info</b>
            
            
          </p>
         
          <hr style="height:1px;border:none;color:#333;background-color:#333;">
          <p><br/></p>
      </div>
      <formly-group
        [field]="field"
        [options]="options"
        [form]="formControl" style="border:1px;">
        <div class="col-sm-1 d-flex align-items-center">
          <button class="btn btn-danger" type="button" (click)="remove(i)" *ngIf="field.fieldGroup[0].key=='productId' || field.fieldGroup[0].key=='gift'" >{{to.removebtnText}}</button>
          <button class="btn btn-danger" type="button" (click)="customRemove(i)" *ngIf="field.fieldGroup[0].key!='productId' &&  field.fieldGroup[0].key!='gift'">Remove</button>
        </div>
      </formly-group>
    </div>
  
    <div style="margin:30px 0;">
      <button class="btn btn-primary" type="button" (click)="add()">{{ to.btnText }}</button>
    </div>
 
  `,
})
export class InvestmentRepeatTypeComponents extends FieldArrayType {
  constructor(builder: FormlyFormBuilder) {
    super(builder);
  }
  currentUser = JSON.parse(sessionStorage.currentUser);
  customRemove(i) {
    console.log("this.to.remove :",this.to.remove);
    if (this.to.remove > 0) {
      this.to.remove(this, i);
    } else {
      this.remove(i);
    }
  }
}