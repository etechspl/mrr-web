import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MgrMonthlyInvestmentComponent } from './mgr-monthly-investment.component';

describe('MgrMonthlyInvestmentComponent', () => {
  let component: MgrMonthlyInvestmentComponent;
  let fixture: ComponentFixture<MgrMonthlyInvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MgrMonthlyInvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MgrMonthlyInvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
