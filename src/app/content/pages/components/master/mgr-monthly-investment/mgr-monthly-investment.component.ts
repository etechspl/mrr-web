import { formatDate } from '@angular/common';
import { InvestmentRepeatTypeComponents } from './repeat-section.types';
import { MGRInvestmentService } from './../../../../../core/services/MGRInvestment/mgrinvestment.service';
import { MatStepper, MatDialog } from '@angular/material';
import { UserAreaMappingService } from './../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { GiftService } from './../../../../../core/services/Gift/gift.service';
import { ProductService } from './../../../../../core/services/Product/product.service';
import { ProviderService } from './../../../../../core/services/Provider/provider.service';
import { HierarchyService } from './../../../../../core/services/hierarchy-service/hierarchy.service';
import { AreaService } from './../../../../../core/services/Area/area.service';
import { ToastrService } from 'ngx-toastr';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
export interface StepType {
  label: string;
  fields: FormlyFieldConfig[];
}
import Swal from 'sweetalert2';
import * as moment from 'moment';
@Component({
  selector: 'm-mgr-monthly-investment',
  templateUrl: './mgr-monthly-investment.component.html',
  styleUrls: ['./mgr-monthly-investment.component.scss']
})
export class MgrMonthlyInvestmentComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);
  activedStep = 0;
  investmentId="";
  resultArr = [];
  result = []

  JointWorkWithvalues = [];
  constructor(private toastr: ToastrService, 
    private _mgrInvestmentservice: MGRInvestmentService, 
    private _areaservice: AreaService, 
    private _hierarchy: HierarchyService,  
    private _providerservice: ProviderService, 
    private _chagedetect: ChangeDetectorRef, 
    private _productservice: ProductService, 
    private _giftService: GiftService, 
    public dialog: MatDialog,
    private _changeDetectorRef:ChangeDetectorRef,  
    private userAreaMappingService: UserAreaMappingService) { }
 
  ngOnInit() {
   }

  model = {
    date: new Date(),
    area: [],
    jointWork: [],
    remarks: '',
    doctor: [{ 'providerId': [{}] }],
    vendor: [{ 'vendorProviderId': [{}] }],
  };


 
  showDCRSummary = true; 
  openDialog(stepper) {
    if(sessionStorage.getItem("investmentId") != undefined) {
      this.showDCRSummary = false;
      this.getUserCompleteInvestmentDetails(sessionStorage.getItem("investmentId"));
    } else {
      this.showDCRSummary = true;
    }
  }


  getManagersAreaList(companyId, districtId, designation, hierarchy, field) {
    this._areaservice.getAreaList(companyId, districtId, designation, hierarchy).subscribe((res: any) => {
      var a = [];
      a.push(res)
      var values = [];
      for (var n = 0; n < a[0].length; n++) {
        values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
      }

      this.getFields(this.steps[0].fields, 'from', 3).templateOptions.options = values
      this.getFields(this.steps[0].fields, 'to', 3).templateOptions.options = values

    }
    );
  }


  getInfoBasedOnDate(userId, value){

    this.showDCRSummary = true;
    this.resultArr = [];
    this._mgrInvestmentservice.getUserDateWiseInvestmentDetail(userId, value.model.date, value.model.date).subscribe(res => {

      this.resultArr.push(res);
      if (this.resultArr[0].length > 0) {
        //console.log(this.resultArr[0][0].workingStatus);
        let workingwith = [], area = [];
        

        for (var i = 0; i < this.resultArr[0][0].jointWork.length; i++) {
          workingwith.push(this.resultArr[0][0].jointWork[i]);
        }

        for (var i = 0; i < this.resultArr[0][0].visitedBlock.length; i++) {
          area.push({ "from": this.resultArr[0][0].visitedBlock[i].fromId + '#-#' + this.resultArr[0][0].visitedBlock[i].from, "to": this.resultArr[0][0].visitedBlock[i].toId + '#-#' + this.resultArr[0][0].visitedBlock[i].to });
        }
        
        this.model = {
          date: value.model.date,
          remarks: this.resultArr[0][0].remarks,
          area: area,
          jointWork: workingwith,
          //doctor: [{ 'providerId': [{}] }],
          doctor: [],
          vendor: [{ 'vendorProviderId': [{}] }],
        }
        this._chagedetect.detectChanges();

      }else{
        this.model = {
          date: value.model.date,
          remarks: '',
          area: [],
          jointWork: [],
          doctor: [],
          vendor: [{ 'vendorProviderId': [{}] }]
        }
        this.showDCRSummary = false;
        this._chagedetect.detectChanges();
      }
    })

  }
  
  getProviderInfos(value, providerType) {
    var areaIds = [];
    if (Object.keys(value).length != 0) {
      for (var n = 0; n < value.length; n++) {
        if (value[n].from != undefined) {
          areaIds.push(value[n].from.split("#-#")[0]);
          areaIds.push(value[n].to.split("#-#")[0]);
        }

      }
      if (providerType.includes('RMP')) {
        this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, providerType).subscribe((res: any[]) => {

          //this.getField(this.steps[1].fields, 'providerId').templateOptions.options = res;
          this.getField(this.steps[1].fields, 'doctor').templateOptions.options = res
        });
      } else if (providerType.includes('Drug') || providerType.includes('Stockist')) {
        this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, providerType).subscribe((res: any[]) => this.getField(this.steps[2].fields, 'vendorProviderId').templateOptions.options = res);
      }
      this._chagedetect.detectChanges();

    }
  }

  steps: StepType[] = [
    {
      label: 'Main',
      fields: [
        { template: '<hr />' },
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-6',
              key: 'date',
              type: 'input',
              templateOptions: {
                type: 'date',
                maxDate: new Date(),
                label: 'Date',
                change: (field, $event) => {
                  this.getInfoBasedOnDate(this.currentUser.id, field);
                  this._chagedetect.detectChanges();
                },
                required: true,
                
              },
            },
            {
              className: 'col-6',
              key: 'jointWork',
              type: 'select',
              templateOptions: {
                label: 'Joint Workwith',
                multiple: true,
                change: (field, $event) => {
                  if (this.currentUser.userInfo[0].designationLevel > 1) {
                    var hierarchy = [];
                    for (let n = 0; n < this.model.jointWork.length; n++) {
                      hierarchy.push(this.model.jointWork[n].id)
                    }
                    this.getManagersAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], 1, hierarchy, field);
                    this._chagedetect.detectChanges();
                  }

                }
              },
             
              lifecycle: {
                onInit: (form, field) => {
                  var filter = {};

                  filter = { companyId: this.currentUser.companyId, supervisorId: this.currentUser.id, type: "lowerWithUpper" };

                  this._hierarchy.getManagerHierarchy(filter).subscribe(res => {
                    var a = [];
                    a.push(res)
                    var values = [];
                    for (var n = 0; n < a[0].length; n++) {
                      values.push({ 'label': a[0][n].name + " (" + a[0][n].designation + ")", 'value': { id: a[0][n].userId, Name: a[0][n].name } });
                    }
                    field.templateOptions.options = values;
                    this._chagedetect.detectChanges();
                  })
                },
              },
              
            },]
        },
        {
          fieldGroupClassName: 'row',
         
          fieldGroup: [
            {
              className: 'section-label col-sm-12',
              template: '<div><strong><font color="#2471A3">' + this.currentUser.company.lables.areaLabel + ' Details:</font></strong></div><hr />',
            },
          ]
        },
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-sm-12',
              key: 'area',
              type: 'repeats',
              templateOptions: {
                label: this.currentUser.company.lables.areaLabel,
                btnText: 'Add ' + this.currentUser.company.lables.areaLabel,
              },
              fieldArray: {
                fieldGroupClassName: 'row',
                className: 'row',
                fieldGroup: [
                  {
                    className: 'col-sm-6',
                    type: 'select',
                    key: 'from',
                    templateOptions: {
                      label: 'From:',
                      options: [],
                      //required: true,
                    },
                    lifecycle: {
                      onInit: (form, field) => {

                        if (this.currentUser.userInfo[0].designationLevel == 1) {
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel, [this.currentUser.id]).subscribe(res => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })
                        } 
                        else if (this.model.jointWork.length != 0) {
                          var hierarchy = [];
                          for (let n = 0; n < this.model.jointWork.length; n++) {
                            hierarchy.push(this.model.jointWork[n].id)
                          }
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], 1, hierarchy).subscribe((res: any) => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();

                          })
                        }else{
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel, [this.currentUser.id]).subscribe(res => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })
                        }

                      },

                    },
                  },
                  {
                    type: 'select',
                    key: 'to',
                    className: 'col-sm-6',
                    templateOptions: {
                      label: 'To:',
                      options: [],
                    },
                    lifecycle: {
                      onInit: (form, field) => {
                        if (this.currentUser.userInfo[0].designationLevel == 1) {
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel, [this.currentUser.id]).subscribe(res => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })
                        } else if (this.model.jointWork.length != 0) {
                          var hierarchy = [];
                          for (let n = 0; n < this.model.jointWork.length; n++) {
                            hierarchy.push(this.model.jointWork[n].id)
                          }
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], 1, hierarchy).subscribe((res: any) => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();

                          })
                        }
                      }
                    },
                  },
                ],
              },
            },]
        },
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-6',
              key: 'remarks',
              type: 'textarea',
              templateOptions: {
                type: 'textarea',
                label: 'Remarks',
                // required: false,
              },
             
            }]
        },
      ],
    },
    
    // Doctor Model
    {
      label: this.currentUser.company.lables.doctorLabel,
      fields: [
        { template: '<hr />' },
        {

          key: 'doctor',
          type: 'repeats',
          templateOptions: {
            label: 'Add ' + this.currentUser.company.lables.doctorLabel,
            btnText: 'Add More ' + this.currentUser.company.lables.doctorLabel,
            remove: (type: InvestmentRepeatTypeComponents, i) => {
              console.log("i :",i);
              Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                 // type.remove(i);
                  this._chagedetect.detectChanges();
                  Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )
                }
              })
            }
          },
          
          fieldArray: {
            fieldGroupClassName: 'row',
            className: 'row',
            fieldGroup: [
              {
                className: 'col-sm-6',
                type: 'select',
                key: 'providerId',
                templateOptions: {
                  label: 'Select ' + this.currentUser.company.lables.doctorLabel,
                  options: [],
                  valueProp: 'id',
                  labelProp: 'providerName',
                  multiple: false,
                  // required: true,
                },

                lifecycle: {
                  onInit: (form, field) => {

                    if (this.model.area != null || Object.keys(this.model.area[0]).length != 0) {
                      var a = [];
                      a.push(this.model.area);
                      var areaIds = [];
                      for (var n = 0; n < a[0].length; n++) {
                        areaIds.push(a[0][n].from.split("#-#")[0]);
                        areaIds.push(a[0][n].to.split("#-#")[0]);
                      }

                      this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["RMP"]).subscribe(res => {
                        var a = [];
                        a.push(res)

                        let newDoctorArray = res.map(doc => ({ providerName: doc.providerName, id: `${doc.id}#-#${doc.category}` }));

                        field.templateOptions.options = newDoctorArray;
                        this._chagedetect.detectChanges();

                      })
                      //  this.getDoctorInfos(this.model.area);
                      //   this._chagedetect.detectChanges();
                    } else {

                    }
                  }
                },
              },
              {
                className: 'col-sm-6',
                key: 'jointWorkWithId',
                type: 'select',
                templateOptions: {
                  label: 'Joint Workwith',
                  multiple: true,
                  options: this.JointWorkWithvalues,
                  //  valueProp: 'userId',
                  //   labelProp: 'name',
                  //required: true,
                },
                lifecycle: {
                  onInit: (form, field) => {
                    field.templateOptions.options = this.JointWorkWithvalues;
                    this._chagedetect.detectChanges();
                  }
                }

              },

              {
                className: 'section-label col-sm-6',
                template: '<div><strong><font color="#2471A3">Product Details:</font></strong></div><hr />',
               
              },
              {
                className: 'section-label col-sm-6',
                template: '<div><strong><font color="#2471A3">Gift Details:</font></strong></div><hr />',
                
              },
              {
                key: 'productDetails',
                className: 'col-6',
                type: 'repeats',
                templateOptions: {
                  label: 'Product',
                  btnText: 'Add Product',
                  removebtnText: 'Del'
                },
                fieldArray: {
                  fieldGroupClassName: 'row',
                  fieldGroup: [
                    {
                      className: 'col-4',
                      type: 'ng-select',
                      key: 'productId',
                      templateOptions: {
                        label: 'Product:',
                        placeholder: 'Select Product',
                        options: [],
                        multiple: false,
                        // valueProp: 'id',
                        // labelProp: 'productName',
                        required: false,
                      },

                      lifecycle: {
                        onInit: (form, field) => {
                          this._productservice.getProductDetailOnStateBasis(this.currentUser.userInfo[0].stateId, this.currentUser.companyId).subscribe(res => {
                            console.log("product resuklt---",res)
                            var a = [];
                            a.push(res)
                            field.templateOptions.options = a[0];
                            this._chagedetect.detectChanges();
                          })
                          let obj = {
                            companyId: this.currentUser.companyId,
                            userId: this.currentUser.userInfo[0].userId,
                            designationLevel: this.currentUser.userInfo[0].designationLevel,
                          }
                          this.userAreaMappingService.getProviderLocationTagged(obj).subscribe(result => {
                          })
                        },
                      },
                    },
                    {
                      className: 'col-3',
                      type: 'input',
                      key: 'productQty',
                      templateOptions: {
                        type: 'number',
                        label: 'Sold:',
                        placeholder: 'Sold',
                        required: true,
                      },
                      // expressionProperties: {
                      //   'templateOptions.required': 'model.productId != undefined'
                      // },
                    },
                    {
                      className: 'col-3',
                      type: 'input',
                      key: 'productFree',
                      templateOptions: {
                        type: 'number',
                        label: 'Free :',
                        placeholder: 'Free',
                        required: true,
                      },

                    }],
                }
              },
              {
                key: 'giftDetails',
                className: 'col-6',
                type: 'repeats',
                templateOptions: {
                  label: 'Gift',
                  btnText: 'Add Gift',
                  removebtnText: 'Del',
                },
                fieldArray: {
                  fieldGroupClassName: 'row',
                  fieldGroup: [
                    {
                      className: 'col-sm-6',
                      type: 'ng-select',
                      key: 'gift',
                      templateOptions: {
                        label: 'Select Gift:',
                        placeholder: 'Select Gift',
                        options: [],
                        multiple: false,
                        // valueProp: 'id',
                        //  labelProp: 'giftName',
                        required: true,
                      },

                      lifecycle: {
                        onInit: (form, field) => {
                          this._giftService.getGiftDetail(this.currentUser.companyId).subscribe(res => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].giftName, 'value': a[0][n]._id + "#-#" + a[0][n].giftName + "#-#" + a[0][n].giftCost });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })

                        },
                      },
                    },
                    {
                      className: 'col-sm-3',
                      type: 'input',
                      key: 'giftQty',
                      templateOptions: {
                        type: 'number',
                        label: 'Qty:',
                        placeholder: 'Qty',
                        required: true,
                      },
                      expressionProperties: {
                        'templateOptions.required': 'model.gift != undefined'
                      },
                    },

                  ],
                }
              },
            ],

          },

        }
      ],

    },

      // Vendor model
      {
        label: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel,
        fields: [
          { template: '<hr />' },
          {
  
            key: 'vendor',
            type: 'repeats',
            templateOptions: {
              label: 'Add ' + this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel,
              btnText: 'Add More ' + this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel,
              remove: (type: InvestmentRepeatTypeComponents, i) => {
                Swal.fire({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  allowOutsideClick: false,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                  if (result.value) {
                    //type.remove(i);
                    this._chagedetect.detectChanges();
                    Swal.fire(
                      'Deleted!',
                      'Your file has been deleted.',
                      'success'
                    )
                  }
                })
              },
            },
            fieldArray: {
              fieldGroupClassName: 'row',
              className: 'row',
              fieldGroup: [
                {
                  className: 'col-sm-6',
                  type: 'select',
                  key: 'vendorProviderId',
                  templateOptions: {
                    label: 'Select ' + this.currentUser.company.lables.vendorLabel + ':',
                    options: [],
                    valueProp: 'id',
                    labelProp: 'providerName',
                    multiple: false
                    // required: true,
                  },
  
                  lifecycle: {
                    onInit: (form, field) => {
                      if (this.model.area != null || Object.keys(this.model.area[0]).length != 0) {
                        var a = [];
                        a.push(this.model.area);
                        var areaIds = [];
                        for (var n = 0; n < a[0].length; n++) {
                          areaIds.push(a[0][n].from.split("#-#")[0]);
                          areaIds.push(a[0][n].to.split("#-#")[0]);
                        }
                        this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["Drug", "Stockist"]).subscribe(res => {
                          var a = [];
                          a.push(res)
                          field.templateOptions.options = a[0];
                          this._chagedetect.detectChanges();
                        })
                        //  this.getDoctorInfos(this.model.area);
                        //   this._chagedetect.detectChanges();
                      } else {
                      }
  
                    }
                  },
                },
                {
                  key: 'productDetails',
                  className: 'col-sm-6',
                  type: 'repeats',
                  templateOptions: {
                    label: 'Product',
                    btnText: 'Add Product',
                    removebtnText: 'Del',
                  },
                  fieldArray: {
                    fieldGroupClassName: 'row',
                    fieldGroup: [
                      {
                        className: 'col-sm-4',
                        type: 'ng-select',
                        key: 'productId',
                        templateOptions: {
                          label: 'Select Product:',
                          placeholder: 'Select Product',
                          options: [],
                          multiple: false,
                          //  valueProp: 'id',
                          // labelProp: 'productName',
                          required: true,
                        },
                        lifecycle: {
                          onInit: (form, field) => {
                            this._productservice.getProductDetailOnStateBasis(this.currentUser.userInfo[0].stateId, this.currentUser.companyId).subscribe(res => {
                              var a = [];
                              a.push(res)
  
                              field.templateOptions.options = a[0];
                              this._chagedetect.detectChanges();
                            })
                          },
                        },
                      },
                      {
                        className: 'col-sm-3',
                        type: 'input',
                        key: 'productQty',
                        templateOptions: {
                          type: 'number',
                          label: 'Sold:',
                          placeholder: 'Sold',
                          required: true,
                        },
  
                      },
                      {
                        className: 'col-sm-3',
                        type: 'input',
                        key: 'productFree',
                        templateOptions: {
                          type: 'number',
                          label: 'Free :',
                          placeholder: 'Free',
                          required: true,
                        },
  
                      },
  
                    ],
                  },
                },{
                  key: 'giftDetails',
                  className: 'col-6',
                  type: 'repeats',
                  templateOptions: {
                    label: 'Gift',
                    btnText: 'Add Gift',
                    removebtnText: 'Del',
                  },
                  fieldArray: {
                    fieldGroupClassName: 'row',
                    fieldGroup: [
                      {
                        className: 'col-sm-6',
                        type: 'ng-select',
                        key: 'gift',
                        templateOptions: {
                          label: 'Select Gift:',
                          placeholder: 'Select Gift',
                          options: [],
                          multiple: false,
                          required: true,
                        },
  
                        lifecycle: {
                          onInit: (form, field) => {
                            this._giftService.getGiftDetail(this.currentUser.companyId).subscribe(res => {
                              var a = [];
                              a.push(res)
                              var values = [];
                              for (var n = 0; n < a[0].length; n++) {
                                values.push({ 'label': a[0][n].giftName, 'value': a[0][n]._id + "#-#" + a[0][n].giftName + "#-#" + a[0][n].giftCost });
                              }
                              field.templateOptions.options = values;
                              this._chagedetect.detectChanges();
                            })
  
                          },
                        },
                      },
                      {
                        className: 'col-sm-3',
                        type: 'input',
                        key: 'giftQty',
                        templateOptions: {
                          type: 'number',
                          label: 'Qty:',
                          placeholder: 'Qty',
                          required: true,
                        },
                        expressionProperties: {
                          'templateOptions.required': 'model.gift != undefined'
                        },
                      },
  
                    ],
                  }
                },
  
                {
                  className: 'col-sm-6',
                  type: 'textarea',
                  key: 'remarks',
                  templateOptions: {
                    label: 'Remarks:',
                    // required: true,
                  },
                },
  
  
              ],
            },
  
          },
        ],
  
      },
      {
        label: 'Preview',
        fields: []
      },
    ]




  form = new FormArray(this.steps.map(() => new FormGroup({})));
  options = this.steps.map(() => <FormlyFormOptions>{});
  prevStep(step) {
    this.activedStep = step - 1;

  }

  nextStep(step, stepper: MatStepper) {

    if (step == 0) {
      var dt = new Date();
      let currentDate = formatDate(new Date(), 'yyyy/MM/dd', 'en');
      let selectedDate = formatDate(this.model.date, 'yyyy/MM/dd', 'en');
      var diff = Math.floor(new Date().getTime() - new Date(this.model.date).getTime());
      var day = 1000 * 60 * 60 * 24;
      var days: number = Math.floor(diff / day);

      if (currentDate < selectedDate) {
        Swal.fire({
          type: 'error',
          title: 'Advance date selection is not allowed !',
          allowOutsideClick: false,
          // text: 'Advance DCR is not allowed !',
        })

      } else {
        this.result = [];
        this._mgrInvestmentservice.getInvestmentDetailsDoneOrNot("" + this.model.date, this.currentUser.id).subscribe(res => {
        this.result.push(res);
if(this.result[0].length > 0){
  Swal.fire({
    type: 'error',
    title: 'Already Submitted...',
    allowOutsideClick: false,
    // text: 'Advance DCR is not allowed !',
  })
}else{
        Swal.fire({
          title: 'Are you sure?',
          text: "You want to Continue...",
          type: 'warning',
          showCancelButton: true,
          allowOutsideClick: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Continue.. it!'
        }).then((result) => {
          if (result.value) {
            this.JointWorkWithvalues = [];

              this.getProviderInfos(this.model.area, ['RMP']);
              this.getProviderInfos(this.model.area, ['Drug']);
              var workingwith = [];
              if (this.model.jointWork != undefined || this.model.jointWork.length != 0 || Object.keys(this.model.jointWork[0]).length === 0) {

                for (var n = 0; n < this.model.jointWork.length; n++) {
                  this.JointWorkWithvalues.push({ 'label': this.model.jointWork[n].Name, 'value': { id: this.model.jointWork[n].id, Name: this.model.jointWork[n].Name } });
                  workingwith.push({ id: this.model.jointWork[n].id, Name: this.model.jointWork[n].Name })
                }
            }
            this.model = {
              date: this.model.date,
              remarks: this.model.remarks,
              area: this.model.area,
              jointWork: this.model.jointWork,
              doctor: [],
              vendor: [{ 'vendorProviderId': [{}] }]
            }


            this.resultArr = [];
            this._mgrInvestmentservice.getUserInvestmentDetailsOnDate("" + this.model.date, this.currentUser.id).subscribe(res => {

            this.resultArr.push(res);
            var visitedBlock = [];
            var areas = [];
            var InvestmentObject = [];
            var obj = {};
            if (this.resultArr[0].length > 0) {
              sessionStorage.removeItem("investmentId");
              sessionStorage.removeItem("investmentStatus");
              sessionStorage.setItem('investmentStatus', '1');
              sessionStorage.setItem("investmentId", this.resultArr[0][0].id);

              
                if (this.resultArr[0][0].info != undefined || this.resultArr[0][0].info.length > 0) {
                  let doctorDetails = [];
                  let vendorDetails = [];
                  for (var i = 0; i < this.resultArr[0][0].info.length; i++) {
                    let productList = [];
                    let giftList = [];
                    if (this.resultArr[0][0].info[i].providerType == "RMP") {
                        if (this.resultArr[0][0].info[i].productDetails.length > 0) {
                          for (var n = 0; n < this.resultArr[0][0].info[i].productDetails.length; n++) {
                            let productObj = {};
                            productObj['productQty'] = this.resultArr[0][0].info[i].productDetails[n].productSold;
                            productObj['productFree'] = this.resultArr[0][0].info[i].productDetails[n].productFreeQty;
                            productObj['productId'] = this.resultArr[0][0].info[i].productDetails[n].productId + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productName + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productPrice;
                            productList.push(productObj)
                          }
                        }
                        if (this.resultArr[0][0].info[i].giftDetails.length > 0) {
                          for (var n = 0; n < this.resultArr[0][0].info[i].giftDetails.length; n++) {
                            let giftObj = {};
                            giftObj['gift'] = this.resultArr[0][0].info[i].giftDetails[n].giftId + "#-#" + this.resultArr[0][0].info[i].giftDetails[n].giftName + "#-#" + this.resultArr[0][0].info[i].giftDetails[n].giftPrice
                            giftObj['giftQty'] = this.resultArr[0][0].info[i].giftDetails[n].giftQty
                            giftList.push(giftObj)
                          }
                        }

                        let alreadySubmittedDetail = this.resultArr[0][0].info[i];
                        this._providerservice.getProviderCategory({ where: { id: this.resultArr[0][0].info[i].providerId } }).subscribe(res => {
                
                          
                          doctorDetails.push({
                            "providerId": alreadySubmittedDetail.providerId + "#-#" + res[0].category,
                            "jointWorkWithId": alreadySubmittedDetail.jointWorkWithId,
                            "productDetails": productList,
                            "giftDetails": giftList,
                            "remarks": alreadySubmittedDetail.remarks
                          })
                        })
                        console.log("doctorDetails : ",doctorDetails);
                    } else {
                      if (this.resultArr[0][0].info[i].productDetails.length > 0) {
                        for (var n = 0; n < this.resultArr[0][0].info[i].productDetails.length; n++) {
                          let productObj = {};
                          productObj['productQty'] = this.resultArr[0][0].info[i].productDetails[n].productSold;
                          productObj['productFree'] = this.resultArr[0][0].info[i].productDetails[n].productFreeQty;
                          productObj['productId'] = this.resultArr[0][0].info[i].productDetails[n].productId + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productName + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productPrice;
                          productList.push(productObj)

                        }

                      }
                      vendorDetails.push({
                        "vendorProviderId": this.resultArr[0][0].info[i].providerId,
                        "productDetails": productList,
                        "remarks": this.resultArr[0][0].info[i].remarks,
                      })
                    }
                  }
                  
                  
                  this.model.doctor = doctorDetails;
                  this.model.vendor = vendorDetails;
                }
              // let areas=[];
              // var visitedBlock=[];
              if (Object.keys(this.model['area']).length > 0) {
                areas.push(this.model['area']);
                for (var n = 0; n < areas[0].length; n++) {

                  if (areas[0][n].from !== undefined) {
                    visitedBlock.push({
                      "from": areas[0][n].from.split("#-#")[1],
                      "to": areas[0][n].to.split("#-#")[1],
                      "fromId": areas[0][n].from.split("#-#")[0],
                      "toId": areas[0][n].to.split("#-#")[0],
                    });
                  }

                }

              }
              obj['visitedBlock'] = visitedBlock;
             if (this.model['jointWork'] !== undefined || this.model['jointWork'].length > 0) {
                obj['jointWork'] = this.model['jointWork'];
              } else {
                obj['jointWork'] = [];
              }
              if (this.model['remarks'] !== undefined) {
                obj['remarks'] = this.model['remarks'];
              } else {
                obj['remarks'] = "";
              }
              // obj['timeIn'] =  formatDate(new Date(), 'YYYY-MM-DDThh:mm:ss.sTZD', 'en-US', '+0530');
              obj['dcrModificationDate'] = new Date();

              InvestmentObject.push(obj);

              this._mgrInvestmentservice.submitInvestment(obj, step, sessionStorage.getItem("investmentId")).subscribe(res => {
               
                  stepper.selectedIndex = step + 1;
              })


            
           } else{

            if (Object.keys(this.model['area']).length > 0) {

              areas.push(this.model['area']);
              for (var n = 0; n < areas[0].length; n++) {
                if (areas[0][n].from !== undefined) {
                  visitedBlock.push({
                    "from": areas[0][n].from.split("#-#")[1],
                    "to": areas[0][n].to.split("#-#")[1],
                    "fromId": areas[0][n].from.split("#-#")[0],
                    "toId": areas[0][n].to.split("#-#")[0],
                  });
                }

              }
            }

            obj['companyId'] = this.currentUser['companyId'];
            obj['stateId'] = this.currentUser['userInfo'][0].stateId;
            obj['districtId'] = this.currentUser['userInfo'][0].districtId;
            obj['submitBy'] = this.currentUser['id'];
            obj['submissionDate'] = new Date(this.model['date']);;
            obj['visitedBlock'] = visitedBlock;
            if (this.model['jointWork'] !== undefined || this.model['jointWork'].length > 0) {
              obj['jointWork'] = this.model['jointWork'];
            } else {
              obj['jointWork'] = [];
            }
            if (this.model['remarks'] !== undefined) {
              obj['remarks'] = this.model['remarks'];
            } else {
              obj['remarks'] = "";
            }
            obj['InvestmentVersion'] = 'draft';

            InvestmentObject.push(obj);

            //SubmitCode is pending....
            this._mgrInvestmentservice.submitInvestment(InvestmentObject, step, "0").subscribe(res => {
              sessionStorage.removeItem("investmentId");
              sessionStorage.setItem("investmentId", res[0].id);
              sessionStorage.setItem('investmentStatus', '1');
                stepper.selectedIndex = step + 1;
              
            });

          }

          Swal.fire(
            'Saved!',
            'Your Record has been Saved...',
            'success'
          )
          });
          }
        });
      }
    });
      }
      

    }else if (step == 1) {
      if (this.model['doctor'][0] != undefined && this.model['doctor'][0].hasOwnProperty('providerId') == true) {
        if (Object.keys(this.model['doctor'][0]['providerId'][0]).length > 0) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to Continue...",
            type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Save it!'
          }).then((result) => {
            if (result.value) {
              var doctorDcrObj = [];
              for (var n = 0; n < this.model['doctor'].length; n++) {
                let doctorDcrObject = {};
                let productInfo = [];
                let giftInfo = [];
                let giftObj = {};
                let productObj = {};
                let productSold = 0;
                let productFree=0;
                let productPob = 0;
                let FinalPrice=0;
                let totProduct=0;

                doctorDcrObject['companyId'] = this.currentUser['companyId'];
                doctorDcrObject['investmentId'] = sessionStorage.getItem("investmentId");
                doctorDcrObject['submissionDate'] = new Date(this.model['date']);;
                doctorDcrObject['providerId'] = this.model['doctor'][n]['providerId'].toString().split("#-#")[0];
                doctorDcrObject['stateId'] = this.currentUser['userInfo'][0].stateId;
                doctorDcrObject['districtId'] = this.currentUser['userInfo'][0].districtId;
                doctorDcrObject['submitBy'] = this.currentUser['id'];

                if (this.model['doctor'][n]['remarks'] !== undefined) {
                  doctorDcrObject['remarks'] = this.model['doctor'][n]['remarks'];
                }
                if (this.model['doctor'][n].hasOwnProperty('productDetails')) {
                  if (this.model['doctor'][n]['productDetails'].length > 0 || this.model['doctor'][n]['productDetails'][0] !== undefined) {
                    for (let h = 0; h < this.model['doctor'][n]['productDetails'].length; h++) {
                      let productQty = this.model['doctor'][n]['productDetails'][h]['productQty'];
                      let productFree= this.model['doctor'][n]['productDetails'][h]['productFree'];
                      let tProduct = productQty + productFree;
                      // this._productservice.getProductInfoOnProductIdBasis(this.model['doctor'][n]['productinfo'][h].productId).subscribe(response=>{
                      productObj = {
                        "productName": this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[1],
                        "productId": this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[0],
                        "productSold": productQty,
                        "productFreeQty" : productFree,
                        "productPrice": this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2],
                        "totalProductQty":tProduct,
                        "FinalPrice" : parseInt(this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2]) / tProduct * productQty,
                        "pob" : (parseInt(this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2]) / tProduct * productQty)*productQty
                        //"pob": tProduct * parseInt(this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2])
                      }
                      productInfo.push(productObj)
                      totProduct=productFree + productQty;
                      FinalPrice = parseInt(this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2]) / totProduct * productQty;
                      productPob = productPob + FinalPrice * productQty;
                      doctorDcrObject['productPob'] = productPob;

                      //})
                      productSold = productSold + this.model['doctor'][n]['productDetails'][h]['productQuantity'];
                      productFree= productFree + this.model['doctor'][n]['productDetails'][h]['productFreeQty'];
                      //  productInfo.push(productObj)
                    }
                    doctorDcrObject['productDetails'] = productInfo;
                  } else {
                    doctorDcrObject['productDetails'] = [];
                  }
                } else {
                  doctorDcrObject['productDetails'] = [];
                }

              //  doctorDcrObject['productSold'] = productSold;
              //  doctorDcrObject['productFree'] = productFree;

                if (this.model['doctor'][n].hasOwnProperty('giftDetails')) {
                  if (this.model['doctor'][n]['giftDetails'].length > 0 || this.model['doctor'][n]['giftDetails'][0] !== undefined) {
                    for (let h = 0; h < this.model['doctor'][n]['giftDetails'].length; h++) {
                      giftObj = {
                        "giftId": this.model['doctor'][n]['giftDetails'][h]['gift'].split("#-#")[0],
                        "giftName": this.model['doctor'][n]['giftDetails'][h]['gift'].split("#-#")[1],
                        "giftQty": this.model['doctor'][n]['giftDetails'][h]['giftQty'],
                        "giftPrice": this.model['doctor'][n]['giftDetails'][h]['gift'].split("#-#")[2],
                      }
                      giftInfo.push(giftObj)
                    }
                    doctorDcrObject['giftDetails'] = giftInfo;
                    //  doctorDcrObject['giftDetails']=this.model['doctor'][n]['giftDetails'];
                  } else {
                    doctorDcrObject['giftDetails'] = [];
                  }

                } else {
                  doctorDcrObject['giftDetails'] = [];
                }

                if (this.model['doctor'][n].hasOwnProperty('jointWorkWithId')) {
                  doctorDcrObject['jointWorkWithId'] = this.model['doctor'][n]['jointWorkWithId'];
                } else {
                  doctorDcrObject['jointWorkWithId'] = [];
                }
                doctorDcrObject['providerType'] = 'RMP';
                
                doctorDcrObject['providerId'] = doctorDcrObject['providerId'].split("#-#")[0];

                doctorDcrObj.push(doctorDcrObject)

                console.log("doctorDcrObj :",doctorDcrObj);
              }

              this._mgrInvestmentservice.deleteInvestmentVisitDetail(sessionStorage.getItem('investmentId'), ['RMP']).subscribe(res => {
                this._mgrInvestmentservice.submitInvestment(doctorDcrObj, step, sessionStorage.getItem("investmentId")).subscribe(res => {
                })
              }, err => {
                console.log("Error : ", err)
              })
              
               

              stepper.selectedIndex = step + 1;
              Swal.fire(
                'Saved!',
                'Your Record has been Saved...',
                'success'
              )
            }
          });
        }else {
          stepper.selectedIndex = step + 1;
        }
      } else {
        this._mgrInvestmentservice.deleteInvestmentVisitDetail(sessionStorage.getItem('investmentId'), ['RMP']).subscribe(res => {
        }, err => {
          console.log("Error : ", err)
        })
        stepper.selectedIndex = step + 1;
      }

    } else if (step == 2) {
      var vendorDcrObj = [];
      let giftInfo = [];
      let giftObj = {};
      if (this.model['vendor'][0] != undefined && this.model['vendor'][0].hasOwnProperty('vendorProviderId') == true) {
        if (Object.keys(this.model['vendor'][0]['vendorProviderId'][0]).length > 0) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to Continue...",
            type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Save it!'
          }).then((result) => {
            if (result.value) {
              
              for (var n = 0; n < this.model['vendor'].length; n++) {
                let vendorDcrObject = {};
                let productInfo = [];
                let productObj = {};
                let productSold = 0;
                let productFreeQty=0;
                let productPob = 0;
                let totProduct=0;
                let FinalPrice=0;

                vendorDcrObject['companyId'] = this.currentUser['companyId'];
                vendorDcrObject['investmentId'] = sessionStorage.getItem("investmentId");
                vendorDcrObject['submissionDate'] = new Date(this.model['date']);
                vendorDcrObject['providerId'] = this.model['vendor'][n]['vendorProviderId'];
                vendorDcrObject['stateId'] = this.currentUser['userInfo'][0].stateId;
                vendorDcrObject['districtId'] = this.currentUser['userInfo'][0].districtId;
                vendorDcrObject['submitBy'] = this.currentUser['id'];
                vendorDcrObject['jointWorkWithId'] = [];

                if (this.model['vendor'][n]['remarks'] !== undefined) {
                  vendorDcrObject['remarks'] = this.model['vendor'][n]['remarks'];
                }
                if (this.model['vendor'][n].hasOwnProperty('productDetails')) {
                  if (this.model['vendor'][n]['productDetails'].length > 0 || this.model['vendor'][n]['productDetails'][0] != undefined) {
                    for (let h = 0; h < this.model['vendor'][n]['productDetails'].length; h++) {
                      let productQty = this.model['vendor'][n]['productDetails'][h]['productQty'];
                      let productFree = this.model['vendor'][n]['productDetails'][h]['productFree'];
                      let tProduct=productQty + productFree;
                      // this._productservice.getProductInfoOnProductIdBasis(this.model['doctor'][n]['productinfo'][h].productId).subscribe(response=>{
                      productObj = {
                        "productName": this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[1],
                        "productId": this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[0],
                        "productSold": productQty,
                        "productFreeQty" : productFree,
                        "productPrice": this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2],
                        "totalProductQty":tProduct,
                        "FinalPrice" : parseInt(this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2]) / tProduct * productQty,
                        "pob" : (parseInt(this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2]) / tProduct * productQty)*productQty
                       
                        //"pob": tProduct * parseInt(this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2])
                      }
                      productInfo.push(productObj)
                      totProduct=productFree + productQty;
                      FinalPrice = parseInt(this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2]) / totProduct * productQty;
                      productPob = productPob + FinalPrice * productQty;
                      vendorDcrObject['productPob'] = productPob;

                      //})
                      productSold = productSold + this.model['vendor'][n]['productDetails'][h]['productQuantity'];
                      productFreeQty = productFreeQty + this.model['vendor'][n]['productDetails'][h]['availableStock'];
                     //  productInfo.push(productObj)
                    }
                    vendorDcrObject['productDetails'] = productInfo;
                  } else {
                    vendorDcrObject['productDetails'] = [];
                  }
                }
                if (this.model['vendor'][n].hasOwnProperty('giftDetails')) {
                  if (this.model['vendor'][n]['giftDetails'].length > 0 || this.model['vendor'][n]['giftDetails'][0] !== undefined) {
                    for (let h = 0; h < this.model['vendor'][n]['giftDetails'].length; h++) {
                      giftObj = {
                        "giftId": this.model['vendor'][n]['giftDetails'][h]['gift'].split("#-#")[0],
                        "giftName": this.model['vendor'][n]['giftDetails'][h]['gift'].split("#-#")[1],
                        "giftQty": this.model['vendor'][n]['giftDetails'][h]['giftQty'],
                        "giftPrice": this.model['vendor'][n]['giftDetails'][h]['gift'].split("#-#")[2],
                      }
                      giftInfo.push(giftObj)
                    }
                    vendorDcrObject['giftDetails'] = giftInfo;
                    //  doctorDcrObject['giftDetails']=this.model['doctor'][n]['giftDetails'];
                  } else {
                    vendorDcrObject['giftDetails'] = [];
                  }

                } else {
                  vendorDcrObject['giftDetails'] = [];
                }

                vendorDcrObject['providerType'] = 'Drug';

                vendorDcrObj.push(vendorDcrObject)
              }
              this._mgrInvestmentservice.deleteInvestmentVisitDetail(sessionStorage.getItem('investmentId'), ['Drug']).subscribe(res => {
                this._mgrInvestmentservice.submitInvestment(vendorDcrObj, step, sessionStorage.getItem("investmentId")).subscribe(res => {
                  this.openDialog(stepper);
                })
              }, err => {
                console.log("Error : ", err)
              })
              

              stepper.selectedIndex = step + 1;
              Swal.fire(
                'Saved!',
                'Your Record has been Saved...',
                'success'
              )
            }
          })


        } else {
          vendorDcrObj.push([])
          this._mgrInvestmentservice.submitInvestment(vendorDcrObj, step, sessionStorage.getItem("investmentId")).subscribe(res => { 
            this.openDialog(stepper);
           })
          stepper.selectedIndex = step + 1;
        }
      } else {
        this._mgrInvestmentservice.deleteInvestmentVisitDetail(sessionStorage.getItem('investmentId'), ['Drug', 'Stockist']).subscribe(res => {
           }, err => {
          console.log("Error : ", err)
        })
        stepper.selectedIndex = step + 1;
      }

    }

  }
  
  submit(stepper: MatStepper) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to finally Save it...",
      type: 'warning',
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Save it!'
    }).then((result) => {
      if (result.value > 0) {
        
        // 11 is a random number for final submission. 
        this._mgrInvestmentservice.submitInvestment({ 'InvestmentVersion': 'complete' }, 11, sessionStorage.getItem("investmentId")).subscribe(res => {
          });
        
        sessionStorage.removeItem("investmentId");
        stepper.selectedIndex = 0;
        this.showDCRSummary = true;
        Swal.fire(
          'Saved!',
          'Your DCR has been Submitted Successfully...',
          'success'
        )
      }
    })
    this.model = {
      date :new Date(),
      remarks: '',
      area: [{}],
      jointWork: [],
      doctor: [{ 'providerId': [{}] }],
      vendor: [{ 'vendorProviderId': [{}] }],
    };

    this.form.reset();
    //this.form.setErrors(null);


  }
  resetAll() {
    this.options.forEach(option => option.resetModel());
  }



  getField(fields, key: string): FormlyFieldConfig {
    for (let i = 0, len = fields.length; i < len; i++) {

      if (fields[i].key === key) {
        return fields[i];
      }
      if (fields[i].fieldGroup && fields[i].fieldGroup.length > 0) {
        return this.getField(fields[i].fieldGroup, key);
      }
    }
  }
  // getFields is used t get manager's area
  getFields(fields, key: string, index: number): FormlyFieldConfig {
    for (let i = index, len = fields.length; i < len; i++) {

      if (fields[i].key === key) {
        return fields[i];
      }
      if (fields[i].fieldGroup && fields[i].fieldGroup.length > 0) {
        return this.getField(fields[i].fieldGroup, key);
      }
    }
  }


  dataSource={};

  getUserCompleteInvestmentDetails(investmentId){
    this._mgrInvestmentservice.getUserCompleteInvestmentDetails(investmentId).subscribe(res=>{
      if(res==0 || res==undefined){
        alert("No Record Found");
      }else{
        this.dataSource=res[0];
        console.log("this.dataSource :",this.dataSource);
        this._changeDetectorRef.detectChanges();
      }
     },err=>{
      console.log(err)
    })
  } 

}
