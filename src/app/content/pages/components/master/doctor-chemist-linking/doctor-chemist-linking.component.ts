import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";
import { DivisionComponent } from "../../filters/division/division.component";
import { DesignationComponent } from "../../filters/designation/designation.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
import { DoctorChemistService } from "../../../../../core/services/DoctorChemist/doctor-chemist.service";
import { ToastrService } from "ngx-toastr";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import { URLService } from "../../../../../core/services/URL/url.service";
import * as XLSX from "xlsx";
import * as _moment from "moment";

@Component({
	selector: "m-doctor-chemist-linking",
	templateUrl: "./doctor-chemist-linking.component.html",
	styleUrls: ["./doctor-chemist-linking.component.scss"],
})
export class DoctorChemistLinkingComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	hideDivisionFilter = true;
  @ViewChild('TABLE') table: ElementRef;
	userForm: FormGroup;

	doctorDataSource = new MatTableDataSource([]);
	chemistDataSource = new MatTableDataSource([]);

	doctorDisplayedColumns = ["checkbox", "sn", "doctorName"];
	chemistDisplayedColumns = ["checkbox", "sn", "chemistName"];

	selectedDivision;
	loader = 3;

	selectionDoctor = new SelectionModel<any>(true, []);
	selectionChemist = new SelectionModel<any>(true, []);

	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId1", // the id of html/table element
	};

	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	// @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;

	@ViewChild("doctorPaginator") doctorPaginator: MatPaginator;
	@ViewChild("chemistPaginator") chemistPaginator: MatPaginator;

	constructor(
		private toast: ToastrService,
		private doctorChemistService: DoctorChemistService,
		private fb: FormBuilder,
		private changeDetectorRef: ChangeDetectorRef,
		private providerService: ProviderService,
    private _urlService: URLService,
    private exportAsService: ExportAsService,
	) {}

	ngOnInit() {
		this.hideDivisionFilter = this.currentUser.company.isDivisionExist
			? false
			: true;

		let obj = {
			companyId: this.currentUser.companyId,
			userId: this.currentUser.id,
			designationLevel: this.currentUser.userInfo[0].designationLevel,
		};
		this.callDivisionComponent.getDivisions(obj);
		this.userForm = this.fb.group({
			userId: [null],
		});
	}

	getDivisionValue(event) {
		this.loader = 3;
		this.selectedDivision = event;
		this.callDesignationComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		this.userForm.patchValue({ userId: null });
		let passingObj = {
			companyId: this.currentUser.companyId,
			userId: this.currentUser.id,
			userLevel: this.currentUser.userInfo[0].designationLevel,
			division: event,
		};
		this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
	}

	getDesignation(event) {
		this.loader = 3;
		this.callEmployeeComponent.setBlank();
		this.userForm.patchValue({ userId: null });
		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					companyId: this.currentUser.companyId,
					designationObject: [event],
					status: [true],
					division: this.selectedDivision,
				};
				this.callEmployeeComponent.getEmployeeListBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.callEmployeeComponent.getEmployeeList(
					this.currentUser.companyId,
					[event],
					[true]
				);
			}
		} else {
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [event.designationLevel], //desig
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			} else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [event.designationLevel], //desig,
					isDivisionExist: true,
					division: this.selectedDivision,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}
		}
	}

	getEmployee(event) {
		this.loader = 0;
		this.selectionChemist.clear();
		this.selectionDoctor.clear();
		this.userForm.patchValue({ userId: event });
		this.getAllChemists(event);
		this.getAllDoctors(event);
	}

	getAllDoctors(userId) {
		let query = {
			where: {
				companyId: this.currentUser.companyId,
				appStatus: "approved",
				providerType: "RMP",
				userId: userId,
				status:true
			},
		};
		this.providerService.getProvidersList(query).subscribe((doctRes) => {
			this.doctorDataSource = new MatTableDataSource(doctRes);
			setTimeout(() => {
				this.doctorDataSource.paginator = this.doctorPaginator;
			}, 1000);
			this.loader += 1;
			!this.changeDetectorRef["destroyed"]
				? this.changeDetectorRef.detectChanges()
				: null;
		});
	}
	getAllChemists(userId) {
		let query = {
			where: {
				companyId: this.currentUser.companyId,
				appStatus: "approved",
				providerType: { inq: ["Drug", "Stockist"] },
				userId: userId,
				status:true
			},
		};
		this.providerService.getProvidersList(query).subscribe((cheRes) => {
			this.chemistDataSource = new MatTableDataSource(cheRes);
			setTimeout(() => {
				this.chemistDataSource.paginator = this.chemistPaginator;
			}, 1000);
			this.loader += 1;
			!this.changeDetectorRef["destroyed"]
				? this.changeDetectorRef.detectChanges()
				: null;
		});
	}
	applyDoctorFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.doctorDataSource.filter = filterValue.trim().toLowerCase();
	}

	applyChemistFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.chemistDataSource.filter = filterValue.trim().toLowerCase();
	}

	isAllSelectedDoctor() {
		const numSelected = this.selectionDoctor.selected.length;
		const numRows = this.doctorDataSource.data.length;
		return numSelected === numRows;
	}

	masterToggleDoctor() {
		this.isAllSelectedDoctor()
			? this.selectionDoctor.clear()
			: this.doctorDataSource.data.forEach((row) =>
					this.selectionDoctor.select(row)
			  );
	}

	checkboxLabelDoctor(row?: any): string {
		if (!row) {
			return `${this.isAllSelectedDoctor() ? "select" : "deselect"} all`;
		}
		return `${
			this.selectionDoctor.isSelected(row) ? "deselect" : "select"
		} row ${row.position + 1}`;
	}

	isAllSelectedChemist() {
		const numSelected = this.selectionChemist.selected.length;
		const numRows = this.chemistDataSource.data.length;
		return numSelected === numRows;
	}

	masterToggleChemist() {
		this.isAllSelectedChemist()
			? this.selectionChemist.clear()
			: this.chemistDataSource.data.forEach((row) =>
					this.selectionChemist.select(row)
			  );
	}

	checkboxLabelChemist(row?: any): string {
		if (!row) {
			return `${this.isAllSelectedChemist() ? "select" : "deselect"} all`;
		}
		return `${
			this.selectionChemist.isSelected(row) ? "deselect" : "select"
		} row ${row.position + 1}`;
	}

	submitData() {
		let data = [];
		this.selectionDoctor.selected.forEach((doctor, docIndex) => {
			this.selectionChemist.selected.forEach((chemist, cheIndex) => {
				let obj = {
					userId: this.userForm.value.userId,
					companyId: this.currentUser.companyId,
					providerId: doctor.id,
					providerName: doctor.providerName,
					providerDistrictId: doctor.districtId,
					providerStateId: doctor.stateId,
					providerLinkedId: chemist.id,
					providerLinkedName: chemist.providerName,
					providerLinkedDistrictId: chemist.districtId,
					providerLinkedStateId: chemist.stateId,
					createdBy: this.currentUser.userInfo[0].userId,
					createdAt: new Date(),
					updatedAt: new Date(),
					status: true,
				};
				data.push(obj);
				if (
					data.length ===
					this.selectionDoctor.selected.length *
						this.selectionChemist.selected.length
				) {
					this.doctorChemistService
						.addDoctorChemistPair(data)
						.subscribe((res) => {
							if (res) {
								this.toast.success("Doctor Chemist Linked !");
								this.selectionDoctor.clear();
								this.selectionChemist.clear();
								this.userForm.reset();
								// this.callDivisionComponent.setBlank();
								// this.callDesignationComponent.setBlank();
								this.callEmployeeComponent.setBlank();
								this.doctorDataSource = new MatTableDataSource(
									[]
								);
								this.chemistDataSource = new MatTableDataSource(
									[]
								);
								this.loader = 3;

								!this.changeDetectorRef["destroyed"]
									? this.changeDetectorRef.detectChanges()
									: null;
							}
						});
				}
			});
		});
	}

  
  exportAsExcel()
  {
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);//converts a DOM TABLE element to a worksheet
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Provider.xlsx');

  }
	PrintTableFunction() {
		let printContents, popupWin;

		const currentDateAndTime = _moment(new Date()).format(
			"DD/MM/YYYY, dddd , HH:mm A"
		);
		const department = this.currentUser.section;
		const companyName = this.currentUser.companyName;

		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;

		console.log(this.doctorDataSource.filteredData);

		let tableAll: any = [];

		let tableDoctor: any = [];
		let tableChemist: any = [];

		let tableChemistcount: any = [];

		this.chemistDataSource.filteredData.forEach(
			(chemistData, index, arr) => {
				tableChemist.push(
					`
        <tr>
        <td class="tg-oiyu">${index + 1}</td>
        <td class="tg-oiyu">${chemistData.providerName}</td>
        </tr>
        `
				);
			}
		);
		this.doctorDataSource.filteredData.forEach((doctorData, index, arr) => {
			tableDoctor.push(`
      <tr>
      <td class="tg-oiyu">${index + 1}</td>
      <td class="tg-oiyu">${doctorData.providerName}</td>
      </tr>
    `);
		});

		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
    <html>
  <head>
  <style>
  img {
  float: left;
  }

  .flex-container {
  display: flex;
  justify-content: space-between;
  }

  .tg {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tg td {
  font-family: Arial, sans-serif;
  font-size: 14px;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  table {
    border-collapse: collapse;
}

  tg.th {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg .tg-lboi {
  border-color: inherit;
  text-align: left;
  vertical-align: middle;

  }

  .tg .tg-u6fn {
  font-weight: bold;
  font-size: 18px;
  background-color: #efefef;
  color:black;
  border-color: inherit;
  text-align: left;
  vertical-align: top;
  padding-top: 0px;
  padding-bottom: 0px;
  }

  .tg .tg-yz93 {
  border-color: inherit;
  text-align: right;
  vertical-align: middle
  }

  .tb2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

   td {
  font-family: Arial, sans-serif;
  font-size: 12px;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  th {
    background-color:#AAB7B8;
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-weight: normal;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  
  }

  .tb2 .tg-oiyu {
  font-size: 10px;
  text-align: left;
  vertical-align: middle
  }
  .tg-bckGnd {
  font-weight: bold;
  background-color: #efefef;
  }


  .tb2 .tg-3ppa {
  font-size: 10px;
  background-color: #efefef;
  color:black;
  text-align: left;
  vertical-align: middle;
  padding-top: 2px;
  padding-bottom: 2px;
  }

  .footer {
  position: sticky;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #efefef;
  color: rgb(2, 2, 2);
  text-align: right
  }



  button {
  background-color: #008CBA;
  border: none;
  color: white;
  margin: 5px 0px;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  }

  @media print {
  @page { size: A4; margin: 5mm; }

  .footer {
  position: relative;
  }
  tr {
  page-break-inside: avoid;
  }

  button{
  display : none;
  }
  }
  </style>
  </head>
  <body onload="window.print();window.close()" style="padding-bottom:30px">
  <div class="flex-container">
  <div style="width: 40%; ">
  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

  </div>

  <h2 style="margin-top: 0px; margin-bottom: 5px " ; align="left"><u>Doctor Chemist Linking Report</u></h2>

 <div>
    
    <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
    <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
    </div>
  </div>
  </div>
  </div>
  <div class="flex-container">
  <div>

  <table style="margin-top:10px; margin-left:10px;">
  <tr>
  <th class="tg-3ppa"><span style="font-weight:600">S.No.</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Doctor Name</span></th>
  </tr>
  <tr>
  ${tableDoctor}
  </tr>
  </table>

  </div>
  <div>
  <table style="margin-top:10px; margin-left:10px;">
  <tr>
  <th class="tg-3ppa"><span style="font-weight:600">S. No.</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Chemist Name</span></th>
  </tr>
  <tr>
  ${tableChemist}
  </tr>
  </table>
  </div>
  </div>

  </body>
  </html>
  `);
		// popupWin.document.close();
	}
}
