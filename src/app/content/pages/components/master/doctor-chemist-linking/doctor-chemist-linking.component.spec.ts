import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorChemistLinkingComponent } from './doctor-chemist-linking.component';

describe('DoctorChemistLinkingComponent', () => {
  let component: DoctorChemistLinkingComponent;
  let fixture: ComponentFixture<DoctorChemistLinkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorChemistLinkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorChemistLinkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
