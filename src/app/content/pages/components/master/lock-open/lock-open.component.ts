
import { Component, OnInit, ViewChild, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, NgModel } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material/core';

import { SelectionModel, DataSource } from '@angular/cdk/collections';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';

import { ToastrService } from 'ngx-toastr';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';


const moment = _rollupMoment || _moment;
declare var $;



export const MY_FORMATS = {
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
  },
  display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'short' },
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};

export interface PeriodicElement {
  sn: number;
  select: boolean
  fieldForceName: string;
  hq: string
  designation: string
  state: string
  lastDcr: string
}
const ELEMENT_DATA: PeriodicElement[] = [
  { sn: 1, select: true, fieldForceName: 'pankaj', hq: "delhi", designation: "new delhi", state: "delhi", lastDcr: "1/2/2019" },
  { sn: 2, select: true, fieldForceName: 'mohan', hq: "noida", designation: "gr. noida", state: "up", lastDcr: "11/12/2019" },
  { sn: 3, select: true, fieldForceName: 'ramu', hq: "patna", designation: "patna", state: "bihar", lastDcr: "10/9/2019" },
  { sn: 4, select: true, fieldForceName: 'shiv', hq: "delhi", designation: "delhi", state: "delhi", lastDcr: "10/2/2019" },

];


@Component({
  selector: 'm-lock-open',
  templateUrl: './lock-open.component.html',
  styleUrls: ['./lock-open.component.scss']
})
export class LockOpenComponent implements OnInit {
  showFilters = false;
  showHq = false;
  showEmp = false;
  showState = false;
  showDes = false;
  showDiv = false;
  showValidInput = false;
  showRedioFilter = false;
  showDate = false;
  showDelButton = false;
  showAddIcon = false;
  LockPeriod = false;
  showReport = false;
  aa = false;
  bb = false;
  showProcessing = false;
  selectedStates;

  displayedColumns = ['sn', 'select', 'fieldForceName', 'hq', 'designation', 'state', 'lastDcr'];
  //dataSource;
  selection = new SelectionModel(true, []);
  showReleaseButton = false;
  dataSource = ELEMENT_DATA;
  textLockOpen: any;

  dataArraySection: FormGroup;
  LockOpenForm: FormGroup;
  radioData: any;
  constructor(private fb: FormBuilder,
    private userDetailsService: UserDetailsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastrService: ToastrService,
    private _dcrReportService: DcrReportsService
  ) {
    this.LockOpenForm = this.fb.group({
      stateInfo: new FormControl(),
      employeeId: new FormControl(),
      districtId: new FormControl(),
      designation: new FormControl(),
      lockType: new FormControl(),
      lockDate: new FormControl(),
      lockingPeriod: new FormControl(),
      LockFilterType: new FormControl(),
      lockOpenDateData: this.fb.array([this.fb.group({
        lockOpenInput: new FormControl(),
        filterName: new FormControl(),
      })]),

    });
    this.dataArraySection = this.fb.group({
      data1: this.fb.array([])
    });

  }

  currentUser = JSON.parse(sessionStorage.currentUser);

  ngOnInit() {
    this.addMoreData();

  }
  @Output() valueChange = new EventEmitter();

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;


  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
  data = [];


  // lockOpenCategory(val) {
  //   this.LockOpenForm.patchValue({ lockType: val });

  //   if (val === 'day') {
  //     this.showRedioFilter = true;
  //     this.showFilters = false;

  //   } else if (val === 'date') {
  //     this.showRedioFilter = true;
  //     this.showFilters = false;
  //     this.showValidInput = false;

  //   }

  // }

  lockOpenType(val) {
    this.LockOpenForm.reset();
    this.aa = true;
    this.bb = false;
    this.LockOpenForm.patchValue({ LockFilterType: val });
    if (val === 'state') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        // this.showDate = true;
        // this.showDelButton = true;;
        // this.showAddIcon = true;
      }
      else {
        this.showValidInput = true;
        // this.showAddIcon = false;
        // this.showDate = false;
      }
      this.showFilters = true;
      this.showEmp = false;
      this.showDiv = false;
      this.showHq = false;
      this.showDes = false;
      this.showState = true;
    }
    else if (val === 'Headquarter') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        this.showDate = true;
        this.showDelButton = true;
        this.showAddIcon = true;
      }
      else {
        this.showValidInput = true;
        this.showAddIcon = false;
        this.showDate = false;
      }
      this.showFilters = true;
      this.showEmp = false;
      this.showDiv = false;
      this.showHq = true;
      this.showDes = false;
      this.showState = true;
    }
    else if (val === 'designation') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        this.showDate = true;
        this.showDelButton = true;;
        this.showAddIcon = true;

      }
      else {
        this.showValidInput = true;
        this.showAddIcon = false;
        this.showDate = false;

      }
      this.showFilters = true;
      this.showEmp = false;
      this.showDiv = false;
      this.showHq = false;
      this.showDes = true;
      this.showState = false;


    }
    else if (val === 'employee') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        this.showDate = true;
        this.showDelButton = true;;
        this.showAddIcon = true;

      }
      else {
        this.showValidInput = true;
        this.showAddIcon = false;
        this.showDate = false;

      }
      this.showFilters = true;
      this.showEmp = true;
      this.showDiv = false;
      this.showHq = false;
      this.showDes = true;
      this.showState = false;

    }
    else if (val === 'delayedDcr') {
      this.aa = false;
      this.bb = true;
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = true;
      }
      else {
        this.showValidInput = false;
      }
      this.showFilters = true;
      // this.showDiv=true;
      this.showState = true;
      this.showEmp = false;
      this.showHq = false;
      this.showDes = false;

    }

  }

  getStateValue(val) {
    this.LockOpenForm.patchValue({ stateInfo: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, [true]);
  }
  getDistrictValue(val) {
    this.LockOpenForm.patchValue({ districtId: val });
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, val);
  }
  getDesignation(val) {
    this.LockOpenForm.patchValue({ designation: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);

  }
  getEmployeeValue(val) {
    this.LockOpenForm.patchValue({ employeeId: val });
  }

  getDivisionValue(val) {
    this.LockOpenForm.patchValue({ divisionId: val });
  }



  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    this.showReleaseButton = true;
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.showReleaseButton = true;
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(element => {
        this.selection.select(element)
      });
  }


  getDelayedValue() {
    this.showReport = false;
    this.showProcessing = true;


    //  if (val == "userId") {
    //       this.data = this.currentUser.company.userId;

    //     } else if (val == "divisionId") {
    //       this.data = this.currentUser.company.divisionId;
    //       }

    // let obj = {
    //    companyId: this.currentUser.companyId,
    //    userId: ["5b33ca57f87a57439085de2a"],

    // }



    //this._dcrReportService.getDelayedvaluesWise(obj).subscribe(res => {

    this._changeDetectorRef.detectChanges();

    if (this.dataSource.length > 0) {
      this.showProcessing = false;
      this.showReport = true;



      this._changeDetectorRef.detectChanges();
    }


    this._changeDetectorRef.detectChanges();

    //  })

  }


  releaseButton(obj) {
    this.showReleaseButton = true;
    this.showProcessing = false;

  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  //---submit lock open details-------------

  SubmitLockOpenDetails() {
    
    this.userDetailsService.getLockOpenDetails(this.currentUser.company.id, this.LockOpenForm.value.stateInfo, this.LockOpenForm.value.districtId, this.LockOpenForm.value.employeeId, this.LockOpenForm.value.designation, "day", this.LockOpenForm.value.LockFilterType, this.LockOpenForm.value.lockingPeriod).subscribe(LockOpenResponse => {
      this.toastrService.success('Lock Days has been updated!!');
      this.textLockOpen = "";
      this.LockOpenForm.patchValue({ lockingPeriod: '' })
      this.selectedStates = [];
      // this.callStateComponent.changedValue([]);
    }, err => {
      this.toastrService.error('Error in opening lock days!!');
    });
  }

  addMoreLockOpenDate() {
    const lockOpen = this.LockOpenForm.controls.lockOpenDateData as FormArray;
    lockOpen.push(this.fb.group({
      lockOpenInput: '',
    }));
  }
  //-----------------------
  //---- add lock date --------------




  //-----------------------------------

  yourFunction(val) {
    let day = val.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    let month = val.getMonth() + 1;
    if (month < 10) {
      month = "0" + month
    }
    let year = val.getFullYear();
    var Date = year + "-" + month + "-" + day;
    this.LockOpenForm.patchValue({ lockDate: Date });
  }

  format(date: Date, displayFormat: Object): string {
    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return this._to2digit(day) + '-' + this._to2digit(month) + '-' + year;
    } else {
      return date.toDateString();
    }
  }
  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }

  addMoreData() {
    // (<FormArray>this.dataArraySection.get('data1')).push(this.addSkillFromGroup());
    //console.log("Index Data =>", val);
    const add = this.dataArraySection.get('data1') as FormArray;
    add.push(this.fb.group({
      calenderData: [],
    }))
    console.log("Index Data =>", this.dataArraySection.value);
    (!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
  }

  deleteData(index: number) {
    const add = this.dataArraySection.get('data1') as FormArray;
    add.removeAt(index);
    (!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
  }

  getFromDate(event) {
    console.log(event);

  }
  lockOpenCategory(event) {
    console.log("radio event->", event);
    this.radioData = event;
  }

}
