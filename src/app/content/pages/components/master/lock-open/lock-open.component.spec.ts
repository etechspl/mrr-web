import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LockOpenComponent } from './lock-open.component';

describe('LockOpenComponent', () => {
  let component: LockOpenComponent;
  let fixture: ComponentFixture<LockOpenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LockOpenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockOpenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
