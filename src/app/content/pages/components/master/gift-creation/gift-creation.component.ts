import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { GiftService } from '../../../../../core/services/Gift/gift.service';
import { MatSnackBar, MatDialog, MatSlideToggleChange } from '@angular/material';
import { EditGiftComponent } from '../edit-gift/edit-gift.component';

@Component({
  selector: 'm-gift-creation',
  templateUrl: './gift-creation.component.html',
  styleUrls: ['./gift-creation.component.scss']
})

export class GiftCreationComponent implements OnInit {
  showGiftDetails = false;
  displayedColumns = ['giftName', 'giftCode', 'giftCost', 'status', 'action', 'action1'];
  dataSource;
  currentUser = JSON.parse(sessionStorage.currentUser);

  giftForm = new FormGroup({
  });

  constructor(
    private toastrService: ToastrService,
    private _giftService: GiftService,
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private changeDetectedRef: ChangeDetectorRef
  ) {
    this.giftForm = this.fb.group({
      id: [null],
      giftName: [null, Validators.required],
      giftCode: [null, Validators.required],
      giftMrp: [null, Validators.required],

    })
  }

  ngOnInit() {
    this.showTable();
  }
  isToggled = true; 
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  showTable() {
    this._giftService.getGiftDetail(this.currentUser.company.id).subscribe(res => {

      if (res < 0 || res == "") {
        this.showGiftDetails = false;
      } else {
        this.dataSource = res;
        this.showGiftDetails = true;
        this.changeDetectedRef.detectChanges();
      }
    })
    this.changeDetectedRef.detectChanges();
  }
  //---------------Create new gift------------------------------
  showGift() {
    this.isToggled = false;
    this._giftService.createGift(this.giftForm.value, this.currentUser.company.id, this.currentUser.id).subscribe(giftResponse => {
      this.toastrService.success('Gift Created Successfully...');
      // this.giftForm.setErrors(null);
      this.giftForm.reset();
      // ---------commented by nipun (02-apr-2020)
      // Object.keys(this.giftForm.controls).forEach((name)=>{
      //   let con = this.giftForm.controls[name];
      //   con.setErrors(null);
      // })

    }, err => {
      console.log(err);
      this.toastrService.error('Gift Already Created..');
    });
    this.showTable();
    this.changeDetectedRef.detectChanges();

  }
  //---------------------edit gifts------------------------------------
  editGift(obj) {
    const dialogRef = this.dialog.open(EditGiftComponent, {
      data: { obj }
    });
    this.showTable();
    this.changeDetectedRef.detectChanges();

  }
  //----------Delete Gift--------------------------
  deleteGift(obj) {
    this._giftService.deleteGift(obj, this.currentUser.company.id, this.currentUser.id).subscribe(giftDelResponse => {
      this.toastrService.success('Gift has been Deleted');

    }, err => {
      console.log(err);
      this.toastrService.error('Error in gift edit component');
    });
    this.showTable();
    this.changeDetectedRef.detectChanges();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
  //-------------------------Allow only number----------------------
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
