import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCreationComponent } from './gift-creation.component';

describe('GiftCreationComponent', () => {
  let component: GiftCreationComponent;
  let fixture: ComponentFixture<GiftCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
