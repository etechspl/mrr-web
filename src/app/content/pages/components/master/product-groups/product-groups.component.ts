import {
	FormGroup,
	FormBuilder,
	Validators,
	FormControl,
} from "@angular/forms";
import { MatDialog, MatTableDataSource, MatSlideToggleChange } from "@angular/material";
import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { DivisionComponent } from "../../filters/division/division.component";
import { StateComponent } from "../../filters/state/state.component";
import { ProductsComponent } from "../../filters/products/products.component";
import { ToastrService } from "ngx-toastr";
import { ProductService } from "../../../../../core/services/Product/product.service";
import { AssignProductToGroupComponent } from "../assign-product-to-group/assign-product-to-group.component";
import { EditAssignedProductGroupComponent } from "../edit-assigned-product-group/edit-assigned-product-group.component";
declare var $;
@Component({
	selector: "m-product-groups",
	templateUrl: "./product-groups.component.html",
	styleUrls: ["./product-groups.component.scss"],
})
export class ProductGroupsComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	showProductGroupDetails: boolean = false;
	dataSource: MatTableDataSource<any>;
	
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	companyId = this.currentUser.companyId;
	DA_TYPE = this.currentUser.company.expense.daType;
	IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
	isShowDivision = false;
	displayedColumns = [
		"srno",
		"stateName",
		"productGroupName",
		"prodName",
		"pts",
		"ptr",
		"mrp",
		"action",
		"action1",
	];
	public showDivisionFilter: boolean = false;
	@ViewChild("dataTable") table;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callProductComponent") callProductComponent: ProductsComponent;

	public showProcessing: boolean = false;
	ngOnInit() {
	
		if (this.currentUser.company.isDivisionExist == true) {
			this.ProductgroupForm.get("divisionId").setValidators([
				Validators.required,
			]);
			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.isShowDivision = true;
		}

		this.showTable();
	}

	ProductgroupForm = new FormGroup({});

	constructor(
		private fb: FormBuilder,
		private _productService: ProductService,
		public _toasterService: ToastrService,
		private changeDetectedRef: ChangeDetectorRef,
		public dialog: MatDialog
	) {
		this.ProductgroupForm = this.fb.group({
			companyId: null,
			stateInfo: [null, Validators.required],
			productInfo: [null, Validators.required],
			productGroupName: new FormControl(null),
			divisionId: new FormControl(null),
		});
	}

	getDivisionValue($event) {
		this.ProductgroupForm.patchValue({ assignedStates: null });
		(this.callStateComponent) ? this.callStateComponent.setBlank() : null;
		this.ProductgroupForm.patchValue({ divisionId: $event });
		if (this.IS_DIVISION_EXIST === true) {
		  this.callStateComponent.getStateBasedOnDivision({
			companyId: this.currentUser.companyId,
			isDivisionExist: this.IS_DIVISION_EXIST,
			division: this.ProductgroupForm.value.divisionId,
			designationLevel: this.currentUser.userInfo[0].designationLevel
		  })
		}
	  }

	  getStateValue(val) {
		this.ProductgroupForm.patchValue({ stateInfo: val });
		this.callProductComponent.getProductsDataForGroup(this.companyId,this.IS_DIVISION_EXIST,this.ProductgroupForm.value.divisionId);
	}

	getProductsDataForGroup(val) {
		this.ProductgroupForm.patchValue({ productInfo: val });
	}

	SubmitGroup() {
	
		this._productService.createProductGroups(this.ProductgroupForm.value,this.currentUser.company.id).subscribe((productResponse) => {
					this._toasterService.success(
						"Product Group Created Successfully"
					);
					this.showTable();
					this.changeDetectedRef.detectChanges();
				},
				(err) => {
					// console.log(err);
					this._toasterService.error(
						"Product Group is Already Exists"
					);
				}
			);
		this.ProductgroupForm.reset();
		this.changeDetectedRef.detectChanges();
		this.showTable();
	}

	showTable() {
		
		this._productService
			.getProductDetails(
				this.currentUser.companyId,
				this.ProductgroupForm.value
			)
			.subscribe(
				(res) => {
					res.sort(function (a, b) {
						if (a.hasOwnProperty("stateName") === true && b.hasOwnProperty("stateName") === true) {
							var textA = a.stateName.toUpperCase();
							var textB = b.stateName.toUpperCase();
							return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
						}
					});
					
					this.showProductGroupDetails = true;
					this.dataSource=res;
					this.dataSource = new MatTableDataSource(res);
					
					this.changeDetectedRef.detectChanges();
				},
				(err) => {
					
				}
			);
	}

	editProductGroup(value) {
		const dialogRef = this.dialog.open(EditAssignedProductGroupComponent, {
			width: "60%",
			height: "60%",
			data: { value },
		});
		dialogRef.afterClosed().subscribe((result) => {
			setTimeout(() => {
				this.showTable();
				this.changeDetectedRef.detectChanges();
			}, 50);
		});
		this.changeDetectedRef.detectChanges();
	}

	//----------Delete Group--------------------------



	applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase();
		this.dataSource.filter = filterValue
	  }

	  
	deleteProductGroup(obj) {
		this._productService
			.deleteProductGroup(obj, this.currentUser.company.id)
			.subscribe(
				(groupDelResponse) => {
					this._toasterService.success("Product Group is Deleted");
					this.showTable();
					this.changeDetectedRef.detectChanges();
				},
				(err) => {
					console.log(err);
					this._toasterService.error(
						"Error in Product Group component"
					);
				}
			);
	}
}
