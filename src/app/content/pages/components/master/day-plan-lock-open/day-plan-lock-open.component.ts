import { UserDetailsService } from './../../../../../core/services/user-details.service';
import { TypeComponent } from './../../filters/type/type.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ExportAsService } from 'ngx-export-as';
import { ToastrService } from 'ngx-toastr';
import { StateComponent } from './../../filters/state/state.component';
import { DivisionComponent } from './../../filters/division/division.component';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'm-day-plan-lock-open',
  templateUrl: './day-plan-lock-open.component.html',
  styleUrls: ['./day-plan-lock-open.component.scss']
})
export class DayPlanLockOpenComponent implements OnInit {
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showEmployee: boolean = false;
  public showReport: boolean = false;
  showAdminAndMGRLevelFilter = false;
  chosenProgram = 'dp';
  date: any;
  selecetdState = [];
  selecetdDistricts = [];
  TypeSelected: any;
  tourProgramForm: FormGroup;

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _toastrService: ToastrService,
    private _userInfo:UserDetailsService,
    // private exportAsService: ExportAsService
  ) { }

  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;

  dayPlanForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    designation: new FormControl(null),
    employeeId: new FormControl(null),
    lockOpenDate : new FormControl(null, Validators.required),
    divisionId: new FormControl(null),
    lockOpenDateTp: new FormControl(null),

  })

getReportType(val) {

    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }

  getTypeValue(val) {
    this.showReport = false;
    this.TypeSelected = val;
    if (val == "State") {
      this.showDistrict = false;
      this.dayPlanForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.dayPlanForm.removeControl("employeeId");
      this.dayPlanForm.removeControl("designation");
    } else if (val == "Headquarter") {
      this.dayPlanForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.dayPlanForm.setControl("districtInfo", new FormControl(null, Validators.required))
      this.dayPlanForm.removeControl("employeeId");
      this.dayPlanForm.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      this.dayPlanForm.removeControl("stateInfo");
      this.dayPlanForm.removeControl("districtInfo");
      this.dayPlanForm.setControl("designation", new FormControl(null, Validators.required));
      this.dayPlanForm.setControl("employeeId", new FormControl(null, Validators.required));
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if(this.isDivisionExist===true){
      this.isShowDivision = true;
    }else{
      this.isShowDivision = false;
    }

    this.dayPlanForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }

  getStateValue(val) {
    this.selecetdState = val;
    this.dayPlanForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.dayPlanForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.dayPlanForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.dayPlanForm.value.divisionId
        })

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.dayPlanForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrictValue(val) {
    this.dayPlanForm.patchValue({ districtInfo: val });
    this.selecetdDistricts = val;
  }
  getEmployeeValue(val) {
    this.dayPlanForm.patchValue({ employeeId: val });
  }

  getDesignation(val) {
    this.dayPlanForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.dayPlanForm.value.divisionId,
        status: [true],
        designationObject: this.dayPlanForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  //setting division into dcrform
  getDivisionValue($event) {
    this.dayPlanForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.dayPlanForm.value.type === "State" || this.dayPlanForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.dayPlanForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.dayPlanForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.dayPlanForm.value.divisionId != null && this.dayPlanForm.value.designation != null) {
          if (this.dayPlanForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.dayPlanForm.value.divisionId,
              status: [true],
              designationObject: this.dayPlanForm.value.designation
            })
          }
        }
      }
    }
  }

  ngOnInit() {

   //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
   if (this.isDivisionExist === true) {
    this.isShowDivision = true;
    this.dayPlanForm.setControl('divisionId', new FormControl(null, Validators.required));
    let obj = {
      companyId: this.currentUser.companyId,
      designationLevel: this.currentUser.userInfo[0].designationLevel
    };

    if (this.currentUser.userInfo[0].rL > 1) {
      obj["supervisorId"] = this.currentUser.id;
    }
    this.callDivisionComponent.getDivisions(obj);

  } else {
    this.isShowDivision = false;
  }
  if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
    this.showAdminAndMGRLevelFilter = true;
    this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
  }

  }

  updateRecords(obj: string){
  this._userInfo.updateDayPlanLockOpen(this.dayPlanForm.value,this.currentUser.company.id,this.currentUser.id ,this.currentUser.company.isDivisionExist, obj).subscribe(res => {
      if (res) {
        if(res && res.ok && res.ok >=1){
          this._toastrService.success("Day Plan Lock Opened Successfully...");
        } else if(res&& res.msg){
          this._toastrService.error(res.msg);
        }
		}
     }, err => {
       console.log(err);
       this._toastrService.error('Some thing went wrong...');
      //  this._toastrService.error('Day Plan Lock Already Opened..');
     });
     this.dayPlanForm.reset();
     this.selecetdState = [];
     this.TypeSelected = null;
    //  this.chosenProgram  = null;
     this.selecetdDistricts  = null;
      this.showDistrict = false;
  }

  //----------------by nipun (22-01-2020) added tpor program loc-------

  changedProgram(obj: any){
	  this.chosenProgram = obj;
    if(obj === 'dp'){
      // this.dayPlanForm.controls.lockOpenDate.setValidators([null,Validators.required]);
      // this.dayPlanForm.controls.lockOpenDateTp.setValidators([null]);
    } else {
      // this.dayPlanForm.controls.lockOpenDateTp.setValidators([null,Validators.required]);
      // this.dayPlanForm.controls.lockOpenDate.setValidators([null]);
    }
  }

  validatedate(){
    if(this.dayPlanForm.value.lockOpenDateTp > 31 ){
      this.dayPlanForm.get('lockOpenDateTp').setValue(31);
    } else if(this.dayPlanForm.value.lockOpenDateTp < 1 ) {
      this.dayPlanForm.get('lockOpenDateTp').setValue(1)
    }
    // (this.dayPlanForm.value.lockOpenDateTp > 31 )?this.dayPlanForm.get('lockOpenDateTp').setValue(31):((this.dayPlanForm.value.lockOpenDateTp < 1 )?this.dayPlanForm.get('lockOpenDateTp').setValue(1):null);
  }

  //----------------by nipun (22-01-2020) end -------------------------

}
