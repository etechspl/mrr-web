import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayPlanLockOpenComponent } from './day-plan-lock-open.component';

describe('DayPlanLockOpenComponent', () => {
  let component: DayPlanLockOpenComponent;
  let fixture: ComponentFixture<DayPlanLockOpenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DayPlanLockOpenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayPlanLockOpenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
