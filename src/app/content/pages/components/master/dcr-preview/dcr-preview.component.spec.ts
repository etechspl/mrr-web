import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcrPreviewComponent } from './dcr-preview.component';

describe('DcrPreviewComponent', () => {
  let component: DcrPreviewComponent;
  let fixture: ComponentFixture<DcrPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcrPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcrPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
