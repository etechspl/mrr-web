import { DcrReportsService } from './../../../../../core/services/DCR/dcr-reports.service';
import { DcrSubmissionComponent } from './../dcr-submission/dcr-submission.component';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { UserDetailsService } from '../../../../../core/services/user-details.service';

@Component({
  selector: 'm-dcr-preview',
  templateUrl: './dcr-preview.component.html',
  styleUrls: ['./dcr-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DcrPreviewComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);

  constructor(public _dcrReportsService:DcrReportsService,private _changeDetectorRef:ChangeDetectorRef, private userDetailService:UserDetailsService) { }

  ngOnInit() {
//    this.getUserCompleteDCRDetails();

  }
 // dcrId=this.data[1];
  dataSource={};
  showGeoReportType=false;
  getUserCompleteDCRDetails(dcrId){
    this._dcrReportsService.getUserCompleteDCRDetails(dcrId).subscribe(res=>{
      if(res==0 || res==undefined){
        alert("No Record Found");     
        this.showGeoReportType=false;
      }else{ 
        if (res[0].dcrMeetings!=null && res[0].dcrMeetings!= undefined) {
          for (let i = 0; i < res[0].dcrMeetings.length; i++) {
            this.userDetailService.getEmployees(this.currentUser.companyId,res[0].dcrMeetings[i].meetingWith).subscribe(empRes=>{
              res[0].dcrMeetings[i].meetingWith = empRes[0].name
            },err=>{
              console.log(err);
              
            })
          }
        }       
        this.dataSource=res[0];
        this.showGeoReportType=true;
        this._changeDetectorRef.detectChanges();
      } 
      
     },err=>{
      console.log(err)
    })
  } 
}
