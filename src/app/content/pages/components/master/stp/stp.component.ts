import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { AreaComponent } from '../../filters/area/area.component';
import { STPService } from '../../../../../core/services/stp.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource, MatTable, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { STPEditComponent } from '../stpedit/stpedit.component';
import { checkSTPExists } from '../../validation/custom-validation';
import * as moment from 'moment';

@Component({
  selector: 'm-stp',
  templateUrl: './stp.component.html',
  styleUrls: ['./stp.component.scss']
})
export class StpComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    private stpService: STPService,
    private userAreaMapping: UserAreaMappingService,
    private _toastr: ToastrService,
    private dialog: MatDialog
  ) {

  }
  isLoading: boolean = false;
  areas = [];
  stpForm: FormGroup;
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  index: number = 0;
  currentUser = JSON.parse(sessionStorage.currentUser);

  @ViewChild("areaComponent") areaComponent: AreaComponent;
  get data() {
    return this.stpForm.get('data') as FormArray;
  }

  private hasError(i: number): boolean {
    return this.data.controls[i].get('toArea').hasError('unique')
  }
  private isTouched(i: number): boolean {
    return this.data.controls[i].get('toArea').touched;
  }


  ngOnInit() {
    this.createSTPForm();
    //getting area for repeating section
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, this.currentUser.id).subscribe(res => {
      this.areas = res;
    })
    this.getSTPDetails({ companyId: this.currentUser.companyId, userId: this.currentUser.id });

  }

  displayedColumns2 = ['sn', 'fromArea', 'toArea', 'type', 'distance', 'freqVisit', 'appByMgr', 'appByAdm', 'action'];
  dataSource2 = new MatTableDataSource<any>();
  getSTPDetails(obj: object) {
    this.stpService.getSTPDetails(obj).subscribe(res => {
      let array = res.map(item => {
        return {
          ...item
        };
      });
      if (array.length > 0) {
        console.log("array=>",array);
        
        this.dataSource2 = new MatTableDataSource(array);
        this.dataSource2.sort = this.sort;
        this.dataSource2.paginator = this.paginator;
      } else {
        this._toastr.info("STP Data is not available", "Data not available");
      }
      this.changeDetectorRef.detectChanges();
    })
  }

  createSTPForm() {
    this.stpForm = this.fb.group({
      data: this.fb.array([this.fb.group({
        fromArea: new FormControl("", Validators.required),
        toArea: new FormControl("", Validators.required, checkSTPExists(this.stpService, 0)),
        type: new FormControl("", Validators.required),
        distance: new FormControl("", Validators.required),
        freqVisit: new FormControl("")
      })])
    })
    this.stpForm.controls["data"]["controls"][0].get('fromArea').valueChanges.subscribe(
      x => {
        //this.data.controls[0].get('fromArea').updateValueAndValidity();
      }
    )
  }


  addMoreSTP() {
    this.index = this.index + 1;
    this.data.push(this.fb.group({
      fromArea: new FormControl("", Validators.required),
      toArea: new FormControl("", [Validators.required], checkSTPExists(this.stpService, this.index)),
      type: new FormControl("", Validators.required),
      distance: new FormControl("", Validators.required),
      freqVisit: new FormControl("")
    }));
    Object.keys(this.stpForm.controls["data"]["controls"][this.index].controls).forEach((name) => {
      console.log(name)
      let con = this.stpForm.controls["data"]["controls"][this.index].controls[name];
      con.setErrors(null);
    })
    this.changeDetectorRef.detectChanges();
  }

  deleteSTP(index: number) {

    if (this.data.length > 1) {
      this.data.removeAt(index);
    }
    this.index = this.index - 1;
    this.changeDetectorRef.detectChanges();
  }

  createSTP() {
    if (this.stpForm.valid) {
      let data = [];
      for (const stp of this.stpForm.value.data) {
        let obj = {};
        if (this.currentUser.userInfo[0].rL === 1) {
          obj = {
            companyId: this.currentUser.companyId,
            stateId: this.currentUser.userInfo[0].stateId,
            districtId: this.currentUser.userInfo[0].districtId,
            userId: this.currentUser.id,
            fromArea: stp.fromArea,
            toArea: stp.toArea,
            distance: stp.distance,
            freqVisit: stp.freqVisit,
            type: stp.type,
            status: false,
            appByMgr: '',
            appByAdm: '',
            frcId: '1',
            mgrAppDate: moment.utc("1900-01-01T00:00:00.000"),
            mgrDelDate: moment.utc("1900-01-01T00:00:00.000"),
            finalAppDate: moment.utc("1900-01-01T00:00:00.000"),
            finalDelDate: moment.utc("1900-01-01T00:00:00.000")
          }
          if (this.currentUser.company.expense.stpApprovalUpto === 1) {
            obj["appStatus"] = 'pending';
          } else if (this.currentUser.company.expense.stpApprovalUpto === 2) {
            obj["appStatus"] = 'InProcess';
          }
          console.log("object stp -",obj)
          data.push(obj);
        }
      }
      this.stpService.createSTP(data).subscribe(res => {
        this._toastr.success('Paired STP Created Successfully', 'STP Creation');
        this.getSTPDetails({ companyId: this.currentUser.companyId, userId: this.currentUser.id });
        this.index = 0
        this.stpForm.reset();
        Object.keys(this.stpForm.controls["data"]["controls"][0].controls).forEach((name) => {
          let con = this.stpForm.controls["data"]["controls"][0].controls[name];
          con.setErrors(null);
        })
        for (let i = 0; i < this.stpForm.controls["data"]["controls"].length; i++) {

          if (i != 0) {
            this.data.removeAt(i);
          }

        }
        this.changeDetectorRef.detectChanges();
      })
    }
  }

  openDialog(obj: object): void {
    const dialogRef = this.dialog.open(STPEditComponent, {
      height: '300px',
      data: { obj }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getSTPDetails({ companyId: this.currentUser.companyId, userId: this.currentUser.id });
    });

  }
}
