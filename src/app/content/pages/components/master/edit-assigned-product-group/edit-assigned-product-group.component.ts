import { ChangeDetectorRef, Component, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EventEmitter } from 'events';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';

@Component({
  selector: 'm-edit-assigned-product-group',
  templateUrl: './edit-assigned-product-group.component.html',
  styleUrls: ['./edit-assigned-product-group.component.scss']
})
export class EditAssignedProductGroupComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);
  productInfo: any; 
  companyId = this.currentUser.companyId;
  productData=[];
  selecteddata;
  dataSource;
  public showNewProduct: boolean = false;
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean ;
  EditAssignedProduct: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<EditAssignedProductGroupComponent>,
  private _userareamappingservice: UserAreaMappingService,
  private changeDetectRef: ChangeDetectorRef,
  private formBuilder: FormBuilder,
  public toasterService: ToastrService,
  private _productService: ProductService
  ) {
  this.getProductHead();

  this.EditAssignedProduct = this.formBuilder.group({
    stateId: this.data.value.stateId,
    stateName: this.data.value.stateName,
    productGroupName: this.data.value.productGroupName,
    selecteddata : new FormControl(null),
  });

   }

  ngOnInit() {
    this._productService.getProductDetails(this.currentUser.companyId,this.EditAssignedProduct.value).subscribe(res => {
      this.dataSource=res;
    })
    
  }
  getProductHead(){
    this.productInfo=this.data.value 
  }


  remove(data){
let obj={
  companyId:this.companyId,
  stateId:this.EditAssignedProduct.value.stateId,
  productGroupName:this.EditAssignedProduct.value.productGroupName,
  productId:data.id
}
 
this._productService.removeProductFromGroup(obj).subscribe(result => {
  this.toasterService.success('Product Deleted !!!');
  this.onNoClick();
},
  err => {
    console.log(err);
  }) 
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }



  changedValue(val) {
    this.valueChange.emit(val);
  } 
  setBlank(){
    this.selecteddata = [];
    this.productData.length = 0;
  }


  getProducts(data){
    this.showNewProduct=true;
    this._productService.getProductsForEditForm(data,this.companyId).subscribe(result => {
      this.productData = result;
    },  
      err => {
        console.log(err); 
      }) 
  }

  editGroupName(){
    let obj1={
      stateId : this.data.value.stateId,
      companyId : this.currentUser.companyId,
      productGroupName : this.data.value.productGroupName,
      } 
      
    let updateData={
      updatedAt:new Date(),
      productGroupName:this.EditAssignedProduct.value.productGroupName
     }

     this._userareamappingservice.updateProductGroupNames(obj1,updateData).subscribe(res=>{
      if(res){
        (Swal as any).fire({
          title:"Sucessfully Update",
          type : "sucess"
        });
        !this.changeDetectRef["destroyed"]
        ? this.changeDetectRef.detectChanges()
        : null;
        this.EditAssignedProduct.controls['productGroupName'].setValue(this.EditAssignedProduct.value.productGroupName);
      }else{
        (Swal as any).fire({
          title:`Some Error Encountered in Edition ... !!!`,
          type: `error`,
        });
      }
    });
    }
  

  addNewProduct(data){
    this._productService.EditProductGroups(this.EditAssignedProduct.value,this.currentUser.company.id,data.value).subscribe((productResponse) => {
    this.toasterService.success(
      "Product Group Edit Successfully"
    );
  },
    (err) => {
      // console.log(err);
      this.toasterService.error(
        "Product Group is Already Exists"
      );
    });
  } 

  
 
}
