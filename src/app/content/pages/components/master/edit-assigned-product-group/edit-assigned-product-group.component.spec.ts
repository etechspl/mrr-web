import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAssignedProductGroupComponent } from './edit-assigned-product-group.component';

describe('EditAssignedProductGroupComponent', () => {
  let component: EditAssignedProductGroupComponent;
  let fixture: ComponentFixture<EditAssignedProductGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAssignedProductGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAssignedProductGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
