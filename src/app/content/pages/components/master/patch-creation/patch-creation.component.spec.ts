import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchCreationComponent } from './patch-creation.component';

describe('PatchCreationComponent', () => {
  let component: PatchCreationComponent;
  let fixture: ComponentFixture<PatchCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
