import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import {
	FormGroup,
	FormControl,
	Validators,
	FormBuilder,
	FormArray,
} from "@angular/forms";
import {
	MatTableDataSource,
	MatPaginator,
	MatSort,
	Sort,
	MatSlideToggleChange,
	MatDialog,
} from "@angular/material";
import { MonthComponent } from "../../filters/month/month.component";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
import { AreaService } from "../../../../../core/services/Area/area.service";
import { SelectionModel } from "@angular/cdk/collections";
import Swal from "sweetalert2";
import { DistrictComponent } from "../../filters/district/district.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { TypeComponent } from "../../filters/type/type.component";
import { StatusComponent } from "../../filters/status/status.component";
import { DivisionComponent } from "../../filters/division/division.component";
import { StateComponent } from "../../filters/state/state.component";
import { CrmService } from "../../../../../core/services/CRM/crm.service";
import { DesignationComponent } from "../../filters/designation/designation.component";
import { ToastrService } from "ngx-toastr";
import { URLService } from "../../../../../core/services/URL/url.service";
import { UserAreaMappingService } from "../../../../../core/services/UserAreaMpping/user-area-mapping.service";
import { EditPatchComponent } from "../edit-patch/edit-patch.component";

@Component({
	selector: 'm-patch-creation',
	templateUrl: './patch-creation.component.html',
	styleUrls: ['./patch-creation.component.scss']
})
export class PatchCreationComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;
	rl = this.currentUser.userInfo[0].rL;
	@ViewChild('paginator') paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")

	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild('paginator1') paginator1: MatPaginator;

	@ViewChild("setStatus") setStatus: StatusComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;
	public showState: boolean = true;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	isShowDivision = false;
	showAlreadyCreatedPatches: boolean = false;
	showProviderDetail: boolean = false;
	showSponsorshipFilter: boolean = false;
	showLinkedProviderDetail: boolean = false;
	showSponsorshipTable: boolean = false;
	showProcessing: boolean = false;
	check: boolean;
	ifProvidersNotPatched:boolean=false;
	isToggled = true;
	dataSource: MatTableDataSource<any>;
	dataSource2: MatTableDataSource<any>;
	displayedColumns = [
		"select",
		"sn",
		"providerName",
		"providerType",
		"patch",
		"providerCode",
		"specialization",
		"degree",
		"category"
	];
	displayedColumns2 = [
		"edit",
		"sn",
		"patchName",
	];

	selection = new SelectionModel<any>(true, []);
	selectedValue = []; toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
	constructor(private _providerMaster: ProviderService,
		private changeDetectorRef: ChangeDetectorRef,
		private fb: FormBuilder,public dialog: MatDialog,
		private _userareamappingservice: UserAreaMappingService
		, private _providerservice: ProviderService, private changedetectorref: ChangeDetectorRef, private _toastrService: ToastrService, private _urlService: URLService
	) { }

	ngOnInit() {
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.PatchForm.setControl(
				"divisionId",
				new FormControl()
			);
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			if (this.rl == 2) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}

	}
	PatchForm: FormGroup = this.fb.group({
		status: new FormControl([true]),
		stateInfo: new FormControl(null),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		designation: new FormControl(null),
		category: new FormControl([])
	});
	PatchProviderForm: FormGroup = this.fb.group({
		patchName: new FormControl(null),
	});
	AlreadyPatchForm: FormGroup = this.fb.group({
		patchName: new FormControl(null),
	});

	getDivisionValue(event) {
		this.callStateComponent.setBlank();
		this.callDistrictComponent.setBlank();
		this.callDesignationComponent.setBlank();
		this.PatchForm.patchValue({ divisionId: event });
		if (this.isDivisionExist === true) {
			this.callStateComponent.getStateBasedOnDivision({
				companyId: this.currentUser.companyId,
				isDivisionExist: this.isDivisionExist,
				division: this.PatchForm.value.divisionId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			});
		}
	}
	getStateValue(val) {
		this.callDistrictComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		this.callDesignationComponent.setBlank();
		this.PatchForm.patchValue({ stateInfo: val });

		if (this.isDivisionExist === true) {
			this.callDistrictComponent.getDistrictsBasedOnDivision({
				designationLevel: this.currentUser.userInfo[0].designationLevel,
				companyId: this.currentUser.companyId,
				stateId: this.PatchForm.value.stateInfo,
				isDivisionExist: this.isDivisionExist,
				division: this.PatchForm.value.divisionId,
			});
		} else {
			this.callDistrictComponent.getDistricts(
				this.currentUser.companyId,
				val,
				true
			);
		}
	}
	getStatusValue(val) {}
	getDistrictValue(val) {
		this.callDesignationComponent.setBlank();
		this.callDesignationComponent.ngOnInit();
		this.PatchForm.patchValue({ districtInfo: val });
	}
	getDesignation(val) {
		this.callEmployeeComponent.setBlank();

		this.PatchForm.patchValue({ designation: val });
		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.isDivisionExist === true) {
				this.callEmployeeComponent.getEmployeeListBasedOnDivision({
					companyId: this.currentUser.companyId,
					division: this.PatchForm.value.divisionId,
					districtId: this.PatchForm.value.districtInfo,
					status: [true],
					designationObject: this.PatchForm.value
						.designation,
				});
			} else {
				let designationArray = val.map((element) => {
					return element.designationLevel;
				});
				this.callEmployeeComponent.getEmployeeDesignatiuonWise(
					this.currentUser.companyId,
					this.PatchForm.value.districtInfo,
					designationArray,
					[true]
				);
			}
		} else {
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [val[0].designationLevel], //desig
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			} else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [val[0].designationLevel], //desig,
					isDivisionExist: true,
					division: this.PatchForm.value.divisionId,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}
		}
	}
	getEmployeeValue(val) {
		this.PatchForm.patchValue({ employeeId: val });
	}
	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	craetePatch() {
		let selectedProviders: any = this.selection.selected;
		let title = "";
		if (this.PatchProviderForm.value.patchName == null) {
			title = "Please Enter the Patch Name !!"
		}
		if (selectedProviders.length == 0) {
			title = "Please Select the Providers !!"
		}
		if (selectedProviders.length == 0 || this.PatchProviderForm.value.patchName == null) {
			(Swal as any).fire({
				title: title,
				type: "error",
			});
		}
		else {
			let providerId = [];
			let obj = {};
			let areaId = new Set();
			let districtId, stateId;
			selectedProviders.forEach((element, i) => {
				element["index"] = i;
				providerId.push(element.providerId);
				areaId.add(element.areaId);
				districtId = element.districtId;
				stateId = element.stateId;
			});
			let areaArray = Array.from(areaId);
			obj = {
				companyId: this.currentUser.companyId,
				areaId: areaArray,
				providerId: providerId,
				userId: this.PatchForm.value.employeeId,
				stateId: stateId,
				districtId: districtId,
				patchName: this.PatchProviderForm.value.patchName,
				status: true
			}
			this._providerservice.createPatch(obj).subscribe(res => {
				if (res.length > 0) {
					(Swal as any).fire({
						title: res[0],
						type: res[1],
					});
					this.showProviderDetail = false;
					
					this.ngOnInit(); 
					this.selection.clear();
					this.PatchProviderForm.controls['patchName'].setValue(null);
					!this.changeDetectorRef["destroyed"]
						? this.changeDetectorRef.detectChanges()
						: null;
				    this.getPatchesDetails();
				} else {
					(Swal as any).fire({
						title: `There is some error in creation !!`,
						type: "error",
					});
				}

			})

		}

	}
	getProvider() {
		this.showProcessing = true;
		this.ifProvidersNotPatched=false;
		let userIds = this.PatchForm.value.employeeId;
		this._userareamappingservice.getProvidersListWithPatchName(this.currentUser.companyId, "Employee Wise", this.PatchForm.value.stateInfo, this.PatchForm.value.districtInfo, this.PatchForm.value.areaInfo, [userIds], this.PatchForm.value.providerType, [this.PatchForm.value.status], this.PatchForm.value.designation, this.PatchForm.value.category).subscribe(res => {
			if (res == null || res.length == 0 || res == undefined) {
				this.showReport = false;
				this.showProcessing = false;
				this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
			} else {
				this.showProviderDetail = true;
				this.showProcessing = false;
				res.forEach(element => {
					if(element.providerType=="RMP"){
						element.providerType=this.currentUser.company.lables.doctorLabel
					} else if(element.providerType=="Drug"){
						element.providerType=this.currentUser.company.lables.vendorLabel
					}
					
				});
				this.dataSource = res;
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
				this.dataSource = new MatTableDataSource(res);
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
				}, 100);
				setTimeout(() => {
					this.dataSource.sort = this.sort;
				}, 100);
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
			}
			this.changedetectorref.detectChanges();
		}, err => {
			console.log(err)
			this.showReport = false;
			//this.showProcessing = false;
			this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
			this.changedetectorref.detectChanges();
		})
	}
	getPatchesDetails() {
		this.isToggled = false;
		this.showAlreadyCreatedPatches=false;
		let userIds = this.PatchForm.value.employeeId;
		let obj = {
			where:{
				companyId: this.currentUser.companyId,
				userId: this.PatchForm.value.employeeId,
				patchStatus: true
			}
		}
		this._providerservice.findAlreadyInsertedPatch(obj).subscribe(res => {
			this.showProcessing = true;
			this.showProviderDetail=false;
			if (res.length > 0) {
				this.showProcessing = false;
				this.showAlreadyCreatedPatches = true;
				this.ifProvidersNotPatched=false;
				this.dataSource2 = new MatTableDataSource(res);
				setTimeout(() => {
					this.dataSource2.paginator = this.paginator1;
				}, 100);
				setTimeout(() => {
					this.dataSource2.sort = this.sort;
				}, 100);
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
			} else {
				this.showAlreadyCreatedPatches=false;
				this.showProcessing = false;
				this.ifProvidersNotPatched=true;
							!this.changeDetectorRef["destroyed"]
							? this.changeDetectorRef.detectChanges()
							: null;
			}

		})
	}
	editPacthes(data){

		const dialogRef = this.dialog.open(EditPatchComponent, {
			width: '90%',
			height:"80%",
			data: { data }
		  });
		dialogRef.afterClosed().subscribe((result) => {
			setTimeout(() => {
			  this.getPatchesDetails();
			  this.changeDetectorRef.detectChanges();
			  },50);
		  });
	}

}
