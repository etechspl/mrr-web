import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GiftService } from '../../../../../core/services/Gift/gift.service';

@Component({
  selector: 'm-edit-gift',
  templateUrl: './edit-gift.component.html',
  styleUrls: ['./edit-gift.component.scss']
})
export class EditGiftComponent implements OnInit {

  giftEditForm: FormGroup;
  dataSource;
  currentUser = JSON.parse(sessionStorage.currentUser);

  constructor(public dialogRef: MatDialogRef<EditGiftComponent>,
    private toastrService: ToastrService,
    private _giftService: GiftService,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder) {
    this.giftEditForm = this.fb.group({
      id: this.data.obj._id,
      giftName: this.data.obj.giftName,
      giftCode: this.data.obj.giftCode,
      giftCost: this.data.obj.giftCost,
    })
  }


  ngOnInit() {
    this._giftService.getGiftDetail(this.currentUser.company.id).subscribe(res => {
      this.dataSource = res;
    })
  }


  getErrorMessage() {
    return this.giftEditForm.hasError('required') ? 'Required field' :
      '';
  }


  onNoClick(): void {
    this.dialogRef.close();
  }


  stopEdit() {
    this._giftService.editGift(this.giftEditForm.value, this.currentUser.company.id, this.currentUser.id).subscribe(giftResponse => {
      this.toastrService.success('Gift Modified Successfully...');
      this.ngOnInit();
    }, err => {
      console.log(err);
      this.toastrService.error('Error in gift edit component');
    });

  }

  //-------------------------Allow only number----------------------
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
}
