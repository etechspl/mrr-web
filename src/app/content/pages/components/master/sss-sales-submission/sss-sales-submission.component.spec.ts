import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SssSalesSubmissionComponent } from './sss-sales-submission.component';

describe('SssSalesSubmissionComponent', () => {
  let component: SssSalesSubmissionComponent;
  let fixture: ComponentFixture<SssSalesSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SssSalesSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SssSalesSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
