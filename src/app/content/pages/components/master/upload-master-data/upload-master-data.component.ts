import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import * as XLSX from 'xlsx';
import { AreaComponent } from '../../filters/area/area.component';
import { StateComponent } from '../../filters/state/state.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StatusComponent } from '../../filters/status/status.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AreaService } from '../../../../../core/services/Area/area.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { DivisionService } from '../../../../../core/services/Division/division.service';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { StateService } from '../../../../../core/services/state.service';
import { DistrictService } from '../../../../../core/services/district.service';
import { District } from '../../../_core/models/district.model';
import { DesignationService } from '../../../../../core/services/designation.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { UserLogin } from '../../../_core/models/userlogin.model';
import { HolidayService } from '../../../../../core/services/Holiday/holiday.service';
import { forEach } from '@angular/router/src/utils/collection';
import { forkJoin } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import validator from 'validator';
import Swal from "sweetalert2";
import * as moment from "moment";
import { async } from '@angular/core/testing';

declare var $;
@Component({
  selector: 'm-upload-master-data',
  templateUrl: './upload-master-data.component.html',
  styleUrls: ['./upload-master-data.component.scss']
})
export class UploadMasterDataComponent implements OnInit {
  fileSelected: boolean = false;
  totalExcelData: number = 0;
  ressCount: number = 0;
  dataUploading: boolean = false;
  checkAreaAlreadyExistRes: any;

  constructor(private _holidayService: HolidayService,
    private _userDetailService: UserDetailsService,
    private _desigService: DesignationService,
    private _stateService: StateService,
    private _districtservice: DistrictService,
    private _productService: ProductService,
    private _divisionService: DivisionService,
    private _userAreaMappingService: UserAreaMappingService,
    private _areaService: AreaService,
    private _providerService: ProviderService,
    private changedetectorref: ChangeDetectorRef,
    private _toastrService: ToastrService) { }
  currentUser = JSON.parse(sessionStorage.currentUser);
  public showDivisionFilter: boolean = false;
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showArea: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public isShowTeamInfo: boolean = false;
  public showUpload: boolean = false;
  public multipleStatus: boolean = false;
  @ViewChild("callAreaComponent") callAreaComponent: AreaComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild('dataTable') table1;
  @ViewChild('file') fileRef: ElementRef;
  dataTable: any;
  dtOptions: any;
  dtTrigger: any;

  //inside export class
  selectedType = "";
  selected;
  arrayBuffer: any;
  attachment;
  downloadUrl: string = "";
  downloadFileName: string = "";
  file: File;
  DataNotUpdated = [];
  areaResult: any
  viewTable: boolean = false;
  uploadingData = new FormGroup({
    division: new FormControl(null),
    stateInfo: new FormControl(null, [Validators.required]),
    districtInfo: new FormControl(null),
    areaInfo: new FormControl(null),
    employeeId: new FormControl(null),

  });
  showProcessing: boolean = false;
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  incomingfile(event) {
    this.DataNotUpdated = [];
    this.viewTable = false;
    if (event.target.files[0]) {
      this.fileSelected = true;
      this.file = event.target.files[0];
    }

  }
  getType(event) {
    this.selectedType = event;
    this.callStateComponent.setBlank();
    this.callDistrictComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    this.fileRef.nativeElement.value = null;
    this.fileSelected = false;
    this.DataNotUpdated = [];
    this.viewTable = false;
    if (this.selectedType == 'Doctor' || this.selectedType == 'Chemist' || this.selectedType == 'Stockist') {
      if (this.selectedType == 'Doctor') {
        this.downloadUrl = "../../assets/Excel/DoctorList.xlsx"
        this.downloadFileName = "DoctorList.xlsx";
      } else {
        this.downloadUrl = "../../assets/Excel/ChemistORStockist.xlsx";
        this.downloadFileName = "ChemistORStockist.xlsx";
      }
      this.multipleStatus = false;
      this.showEmployee = true;
      this.showDistrict = true;
      this.showState = true;
      this.uploadingData = new FormGroup({
        stateInfo: new FormControl(null, [Validators.required]),
        districtInfo: new FormControl(null, [Validators.required]),
        employeeId: new FormControl(null, [Validators.required])

      })
    } else if (this.selectedType == 'Employee') {
      this.downloadUrl = "../../assets/Excel/EmployeeData.xlsx"
      this.downloadFileName = "EmployeeData.xlsx";
      this.multipleStatus = false;
      this.showEmployee = false;
      this.showDistrict = false;
      this.showState = false;
      this.showUpload = true;
      this.uploadingData = new FormGroup({
      })
    } else if(this.selectedType == 'Holiday'){
      this.downloadUrl = "../../assets/Excel/Holiday.xlsx"
      this.downloadFileName = "Holiday.xlsx";
      this.multipleStatus = true;
      this.showUpload = true;

    }else {
      this.downloadUrl = "../../assets/Excel/Product.xlsx"
      this.downloadFileName = "Product.xlsx";
      this.multipleStatus = true;
      this.showState = false;
      this.showUpload = true;
      this.showEmployee = false;
      this.showDistrict = false;
      this.uploadingData = new FormGroup({
        stateInfo: new FormControl(null, [Validators.required])
      })
    }

  }
  //setting division into dcrform
  getDivisionValue($event) {
    this.uploadingData.patchValue({ division: $event });
    if (this.currentUser.company.isDivisionExist === true) {
      if (this.uploadingData.value.type === "State" || this.uploadingData.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.currentUser.company.isDivisionExist,
          division: this.uploadingData.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.uploadingData.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.uploadingData.value.divisionId != null && this.uploadingData.value.designation != null) {
          if (this.uploadingData.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.currentUser.companyId,
              division: this.uploadingData.value.divisionId,
              status: [true],
              designationObject: this.uploadingData.value.designation
            })
          }
        }
      }
    }
  }
  getStateValue(val) {
    this.viewTable = false;
    this.callDistrictComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    this.uploadingData.patchValue({ stateInfo: val });
    if (this.currentUser.company.isDivisionExist == true && (this.selectedType !== 'Product' && this.selectedType !== 'Holiday')) {
      let passingObj = {
        division: this.uploadingData.value.division,
        companyId: this.currentUser.companyId,
        stateId: [val],
        status: true
      };
      this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
    } else if (this.currentUser.company.isDivisionExist == false && (this.selectedType !== 'Product' && this.selectedType !== 'Holiday')) {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, [val], true);
    }

  }
  getDistrictValue(val) {
    this.viewTable = false;
    this.callEmployeeComponent.setBlank();
    this.uploadingData.patchValue({ districtInfo: val });
    let passingObj = {
      companyId: this.currentUser.companyId,
      stateId: [this.uploadingData.value.stateInfo],
      districtId: [val],
      status: [true],
      divisionId: ""
    }
    if (this.currentUser.company.isDivisionExist == true) {
      passingObj.divisionId = this.uploadingData.value.division;
    }
    this.callEmployeeComponent.getEmployeeStateWiseOrDistrictWise(passingObj);
  }

  getEmployeeValue(val) {
    this.viewTable = false;
    this.showUpload = true;
    this.uploadingData.patchValue({ employeeId: val });

  }
  async makeProvider(data){
   
    data.forEach( async element => {
      let filter={
        "stateId": this.uploadingData.value.stateInfo,
        "districtId": this.uploadingData.value.districtInfo,
        "areaName": element.areaName
      }
      this._areaService.checkAreaAlreadyExist(this.currentUser.companyId, filter).subscribe( async areaRes => {
        await areaRes
        console.log("area result =>",areaRes);
        if(areaRes.length>0){
       this.checkAreaAlreadyExistRes = areaRes;
       let areaIds = []; 
       areaRes.forEach(async(areaRes )=> {                        
       await areaIds.push(areaRes.id);
       let userAreaObj = {
        areaId: areaRes.id,
        userId: this.uploadingData.value.employeeId,
        companyId: this.currentUser.companyId,
     }                      
this._userAreaMappingService.getMappedAreasOfEmployeesForFileUpload(userAreaObj).subscribe( async(userAreaRes)=>{
   await userAreaRes;
   console.log("mapping Res",userAreaRes);
   
   if(userAreaRes.length>0){
     console.log("----provider will be created----");
   }
   else{
     console.log("area mapping be created");
          let areaMapObj = {
            "companyId": this.currentUser.companyId,
            "stateId": this.uploadingData.value.stateInfo,
            "districtId": this.uploadingData.value.districtInfo,
            "areaId": areaRes.id,
            "userId": this.uploadingData.value.employeeId,
            "status": true,
            "mappedData": [
              "Doctor",
              "Chemist",
              "Stockist"
            ],
            "appStatus": "approved",
            "delStatus": "",
            "mgrAppDate": new Date(),
            "finalAppDate": new Date(),
            "appByMgr": this.currentUser.id,  //change Admin or MGR Id
            "finalAppBy": this.currentUser.id, //change Admin or MGR Id
            "mgrDelDate": "1900-01-01T00:00:00.000+0000",
            "finalDelDate": "1900-01-01T00:00:00.000+0000",
            "delByMgr": "",
            "finalDelBy": ""
          }
          this._userAreaMappingService.createUserAreaMapping(areaMapObj).subscribe(async responses => {
            await responses
            console.log("provider will be inserted---");
            
             })       
   }

})
 
      
      });

                                      }else{
  // Insert Area into Area Collection
  let newAreaObject = {
    "stateId": this.uploadingData.value.stateInfo,
    "districtId": this.uploadingData.value.districtInfo,
    "areaName": element.areaName.toUpperCase(),
    "areaCode": "",
    "companyId": this.currentUser.companyId,
    "type": "EX",
    "status": true,
    "appStatus": "approved",
    "delStatus": "",
    "ipAddress": "",
    "geoLocation": [],
    "mgrAppDate": new Date(),
    "finalAppDate": new Date(),
    //  "mgrDelDate": "1900-01-01T00:00:00.000+0000",
    //  "finalDelDate": "1900-01-01T00:00:00.000+0000",
    "appByMgr": "",
    "finalAppBy": "",
    "delByMgr": "",
    "finalDelBy": ""
  }
    this.viewTable = false;
    this._areaService.createArea(this.currentUser, newAreaObject).subscribe( async area => {
      await area
      if (area) {
        this._toastrService.success("Area created successfully !!!");
        (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;

      }
      if (JSON.parse(JSON.stringify(area)).id !== undefined) {
        let areaId = JSON.parse(JSON.stringify(area)).id;
        let areaMapObj = {
          "companyId": this.currentUser.companyId,
          "stateId": this.uploadingData.value.stateInfo,
          "districtId": this.uploadingData.value.districtInfo,
          "areaId": areaId,
          "userId": this.uploadingData.value.employeeId,
          "status": true,
          "mappedData": [
            "Doctor",
            "Chemist",
            "Stockist"
          ],
          "appStatus": "approved",
          "delStatus": "",
          "mgrAppDate": new Date(),
          "finalAppDate": new Date(),
          "appByMgr": this.currentUser.id,  //change Admin or MGR Id
          "finalAppBy": this.currentUser.id, //change Admin or MGR Id
          "mgrDelDate": "1900-01-01T00:00:00.000+0000",
          "finalDelDate": "1900-01-01T00:00:00.000+0000",
          "delByMgr": "",
          "finalDelBy": ""
        }
        this._userAreaMappingService.createUserAreaMapping(areaMapObj).subscribe(async responses => {
          await responses
          console.log("area and mapping is created");

           })  
      }

       })
            }
          
    }); 
  })
}

  UploadData() {
    this.dataUploading = true;
    this.viewTable = false;
    let issueExist = false;
    this.showProcessing = true;
    this.DataNotUpdated = [];
    this.ressCount = 0;
    this.totalExcelData = 0;
    // console.clear();
    //  let DataNotUpdated:any=[];
    let fileReader = new FileReader();
    fileReader.onload = async (e) => {
      this.arrayBuffer = fileReader.result;
      let data = new Uint8Array(this.arrayBuffer);
      let arr = new Array();
      for (let i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      let bstr = arr.join("");
      let workbook = XLSX.read(bstr, { type: "binary",cellDates:true });
      let first_sheet_name = workbook.SheetNames[0];
      let worksheet = workbook.Sheets[first_sheet_name];
      let datas : any = XLSX.utils.sheet_to_json(worksheet, { raw: true});
      this.totalExcelData = datas.length;


      let i = 0;
      if ((this.selectedType == "Doctor" || this.selectedType == "Chemist" || this.selectedType == "Stockist") && datas.length > 0) {
        let areaList: any = []

        let test=[{
           areaName:"ALIGARH",
           providerName:"Preeti",
           mobile:0

        },{
           areaName:"ALIGARH",
           providerName:"P K",
           mobile:8273827
        }]
        // let abc= this.makeProvider(test)

        for (let n = 0; n < datas.length; n++) {
          const data = JSON.parse(JSON.stringify(datas[n]));
          /**
           * validations
           */
          if (this.selectedType == "Doctor" && (!data.Doctor_Name)) {
            issueExist = true;
            this.showProcessing = false;
            this.viewTable = false;

            (Swal as any).fire({
              title: " Alert !!",
              type: "error",
              text: `Doctor's data is missing !!`,
            });

            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
          } else if ((this.selectedType == "Chemist" || this.selectedType == "Stockist") && (!data.VendorOrStockiest_Name)) {
            issueExist = true;
            this.showProcessing = false;
            (Swal as any).fire({
              title: " Alert !!",
              type: "error",
              text: `Vendor's data is missing !!`,
            });
            this.viewTable = false;
            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;

          }
          if (this.selectedType == "Doctor") {
            if ((!data.Doctor_Name)) {
              issueExist = true;
              this.showProcessing = false;
              let newObject = { ...data };

              newObject['remarks'] = `Doctor's Name does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
              (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;

            } else if (!data.Area) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Doctor's Area does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            } else if (!data.Area_Type) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Doctor's Area Type does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            } else if (!data.Degree) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Doctor's Degree does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            } else if (!data.Specialization) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Doctor's Specialization does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            }
          } else if (this.selectedType == "Chemist") {
            if ((!data.VendorOrStockiest_Name)) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Chemist's Name does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            } else if (!data.Area) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Chemist's Area does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            } else if (!data.Area_Type) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Chemist's Area Type does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            }
          } else if (this.selectedType == "Stockist") {
            if ((!data.VendorOrStockiest_Name)) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Stockist's Name does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            } else if (!data.Area) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Stockist's Area does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            } else if (!data.Area_Type) {
              issueExist = true;
              this.showProcessing = false;
              const newObject = { ...data };
              newObject.remarks = `Stockist's Area Type does not exist at ${n + 2}`;
              this.DataNotUpdated.push(newObject);
            }
          }


          if ((JSON.parse(JSON.stringify(datas[n])).Doctor_Name !== undefined && JSON.parse(JSON.stringify(datas[n])).Doctor_Name !== "") || (JSON.parse(JSON.stringify(datas[n])).VendorOrStockiest_Name !== undefined && JSON.parse(JSON.stringify(datas[n])).VendorOrStockiest_Name !== "")) {
            if (JSON.parse(JSON.stringify(datas[n])).Area != "" && JSON.parse(JSON.stringify(datas[n])).Area != undefined) {
              let areaName = JSON.parse(JSON.stringify(datas[n])).Area.toUpperCase();
              // areaName = areaName.toUpperCase();
              let mobile = 0, frequencyVisit = 0, specialization = "", providerName = "", category = "", doctorAddress = "", degree = "", areaType = "", DOA = new Date("1900-01-01T00:00:00.000+0000"), DOB = new Date("1900-01-01T00:00:00.000+0000");
              // Check mobile number
              if (JSON.parse(JSON.stringify(datas[n])).Mobile !== undefined && typeof (JSON.parse(JSON.stringify(datas[n])).Mobile) == 'number') {
                mobile = JSON.parse(JSON.stringify(datas[n])).Mobile
              }
              // Check frequencyVisit exist in excel
              if (JSON.parse(JSON.stringify(datas[n])).Frequency_Visit !== undefined && typeof (JSON.parse(JSON.stringify(datas[n])).Frequency_Visit) == 'number') {
                frequencyVisit = JSON.parse(JSON.stringify(datas[n])).Frequency_Visit
              }
              // Check specialization exist in excel
              if (this.selectedType == "Doctor" && JSON.parse(JSON.stringify(datas[n])).Specialization !== undefined) {
                specialization = JSON.parse(JSON.stringify(datas[n])).Specialization
              }
              // Check category exist in excel
              if (JSON.parse(JSON.stringify(datas[n])).Category !== undefined) {
                category = JSON.parse(JSON.stringify(datas[n])).Category
              }
              // Check doctorAddress exist in excel
              if (JSON.parse(JSON.stringify(datas[n])).Doctor_Address !== undefined) {
                doctorAddress = JSON.parse(JSON.stringify(datas[n])).Address
              }
              // Check degree exist in excel
              if (this.selectedType == "Doctor" && JSON.parse(JSON.stringify(datas[n])).Degree !== undefined) {
                degree = JSON.parse(JSON.stringify(datas[n])).Degree
              }
              // Check areaType exist in excel
              if (JSON.parse(JSON.stringify(datas[n])).Area_Type !== undefined) {
                areaType = JSON.parse(JSON.stringify(datas[n])).Area_Type
              }
              // Check DOA exist in excel
              if (JSON.parse(JSON.stringify(datas[n])).DOA !== undefined) {
                DOA = JSON.parse(JSON.stringify(datas[n])).DOA
              }
              // Check DOB exist in excel
              if (JSON.parse(JSON.stringify(datas[n])).DOB !== undefined) {
                DOB = JSON.parse(JSON.stringify(datas[n])).DOB
              }
              if (this.selectedType == "Doctor") {
                providerName = JSON.parse(JSON.stringify(datas[n])).Doctor_Name;
              } else {
                providerName = JSON.parse(JSON.stringify(datas[n])).VendorOrStockiest_Name;
              }

              //---------we will start from here by preeti arora--------------------//

              //----------end-------------------------------------------//
              let filter = {
                "stateId": this.uploadingData.value.stateInfo,
                "districtId": this.uploadingData.value.districtInfo,
                "areaName": areaName
              }

              console.log("area filter",filter);
              
              

              // await new Promise(resolve => setTimeout(resolve, 100));
              // synchronous
              // this._areaService.checkAreaAlreadyExist(this.currentUser.companyId, filter).subscribe(res => {
              if (!issueExist) {
                this.viewTable = false;
                this._areaService.checkAreaAlreadyExist(this.currentUser.companyId, filter).subscribe(async (res) => {
                  console.log("area result=>",res);
                  await res
                  this.checkAreaAlreadyExistRes = res;
                  let providerType = "";
                  let providerDetails = {};
                  if (this.selectedType == "Doctor") {
                    providerType = "RMP";
                  } else {
                    if (this.selectedType == "Vendor") {
                      providerType = "Drug";
                    } else {
                      providerType = "Stockist";
                    }
                  }
                  
                  
                  if (res.length > 0) {
                    //---area created with same name---------
                    let areaIds = []; 
                    res.forEach(async(areaRes )=> {  
                      // await areaIds.push(areaRes.id);
                      let userAreaObj = {
                        areaId: areaRes.id,
                        userId: this.uploadingData.value.employeeId,
                        companyId: this.currentUser.companyId,
                      }
                      console.log("area object=>",userAreaObj);
                      
                      this._userAreaMappingService.getMappedAreasOfEmployeesForFileUpload(userAreaObj).subscribe( async (userAreaRes)=>{
                        await userAreaRes
                        if(userAreaRes.length > 0){
                          console.log('only provider will be created here-->>');
                          let params = {
                            companyId: this.currentUser.companyId,
                            blockId: res[0].id,
                            providerName: providerName,
                            providerType: providerType
                          }
                            this._providerService.checkProviderExistStatus(params).subscribe( async result => {
                              await result
                              if (result.length > 0 ) {
                                // already data available
                               await this.DataNotUpdated.push(datas[n]);
                                // if (n == datas.length - 1){
                                // this.DataNotUpdated.forEach((element, i) => { 
                                  if (providerType == "RMP") {
                                    providerDetails = {
                                      "stateId": res[0].stateId,
                                      "districtId": res[0].districtId,
                                      "blockId": res[0].id,
                                      "providerType": providerType,
                                      "providerName": providerName + '' + (i + 1),
                                      "providerCode": "",
                                      "category": category,
                                      "address": doctorAddress,
                                      "phone": mobile,
                                      "licenceNumber": "",
                                      "status": true,
                                      "createdAt": new Date(),
                                      "updatedAt": new Date(),
                                      "userId": this.uploadingData.value.employeeId,
                                      "companyId": this.currentUser.companyId,
                                      "geoLocation": [],
                                      "doa": DOA,
                                      "dob": DOB,
                                      "frequencyVisit": frequencyVisit,
                                      "focusProduct": [
      
                                      ],
                                      "degree": degree,
                                      "specialization": specialization,
                                      "email": "",
                                      "businessPotential": 0,
                                      "appStatus": "approved",
                                      "delStatus": "",
                                      "designationLevel": 1,
                                      "ipAddress": "",
                                      "licenceStatus": false,
                                      "mgrAppDate": new Date(),
                                      "finalAppDate": new Date(),
                                      "appByMgr": this.currentUser.id, //change Admin or MGR Id
                                      "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                                      "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                      "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                      "delByMgr": "",
                                      "finalDelBy": "",
                                      "onLocation": false
                                    }
                                  } else {
                                    providerDetails = {
                                      "stateId": res[0].stateId,
                                      "districtId": res[0].districtId,
                                      "blockId": res[0].id,
                                      "providerType": providerType,
                                      "providerName": providerName + ' ' + (i + 1),
                                      "providerCode": "",
                                      "category": category,
                                      "address": doctorAddress,
                                      "phone": mobile,
                                      "licenceNumber": "",
                                      "status": true,
                                      "createdAt": new Date(),
                                      "updatedAt": new Date(),
                                      "userId": this.uploadingData.value.employeeId,
                                      "companyId": this.currentUser.companyId,
                                      "geoLocation": [],
                                      "doa": DOA,
                                      "dob": DOB,
                                      "frequencyVisit": 0,
                                      "focusProduct": [
      
                                      ],
                                      "degree": "",
                                      "specialization": "",
                                      "email": "",
                                      "businessPotential": 0,
                                      "appStatus": "approved",
                                      "delStatus": "",
                                      "designationLevel": 1,
                                      "ipAddress": "",
                                      "licenceStatus": false,
                                      "mgrAppDate": new Date(),
                                      "finalAppDate": new Date(),
                                      "appByMgr": this.currentUser.id, //change Admin or MGR Id
                                      "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                                      "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                      "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                      "delByMgr": "",
                                      "finalDelBy": "",
                                      "onLocation": false
                                    }
                                  }
                                  // Here --insertion code for provider
                                  if (!issueExist) {
                                    this.viewTable = false;
                                    this._providerService.uploadProviderData(providerDetails).subscribe(async ress => {
                                       await ress
                                      if (ress) {
                                        this.ressCount++;
                                        (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                        this.showProcessing = false;
                                        if (this.ressCount == this.totalExcelData) {
                                          this.dataUploading = false;
                                          (Swal as any).fire({
                                            title: "Providers added successfully !!!",
                                            type: "success",
                                            confirmButtonColor: '#3085d6',
                                            confirmButtonText: 'OK'
                                          }).then(window.location.reload());
                            
                                            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                        }
                                      }
                                    })
                                  }
                                // });
                                // }
                                // this.DataNotUpdated = [];
                              } else {
                                if (providerType == "RMP") {
      
                                  providerDetails = {
                                    "stateId": res[0].stateId,
                                    "districtId": res[0].districtId,
                                    "blockId": res[0].id,
                                    "providerType": providerType,
                                    "providerName": providerName,
                                    "providerCode": "",
                                    "category": category,
                                    "address": doctorAddress,
                                    "phone": mobile,
                                    "licenceNumber": "",
                                    "status": true,
                                    "createdAt": new Date(),
                                    "updatedAt": new Date(),
                                    "userId": this.uploadingData.value.employeeId,
                                    "companyId": this.currentUser.companyId,
                                    "geoLocation": [],
                                    "doa": DOA,
                                    "dob": DOB,
                                    "frequencyVisit": frequencyVisit,
                                    "focusProduct": [
      
                                    ],
                                    "degree": degree,
                                    "specialization": specialization,
                                    "email": "",
                                    "businessPotential": 0,
                                    "appStatus": "approved",
                                    "delStatus": "",
                                    "designationLevel": 1,
                                    "ipAddress": "",
                                    "licenceStatus": false,
                                    "mgrAppDate": new Date(),
                                    "finalAppDate": new Date(),
                                    "appByMgr": this.currentUser.id, //change Admin or MGR Id
                                    "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                                    "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                    "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                    "delByMgr": "",
                                    "finalDelBy": "",
                                    "onLocation": false
                                  }
                                } else {
                                  providerDetails = {
                                    "stateId": res[0].stateId,
                                    "districtId": res[0].districtId,
                                    "blockId": res[0].id,
                                    "providerType": providerType,
                                    "providerName": providerName,
                                    "providerCode": "",
                                    "category": category,
                                    "address": doctorAddress,
                                    "phone": mobile,
                                    "licenceNumber": "",
                                    "status": true,
                                    "createdAt": new Date(),
                                    "updatedAt": new Date(),
                                    "userId": this.uploadingData.value.employeeId,
                                    "companyId": this.currentUser.companyId,
                                    "geoLocation": [],
                                    "doa": DOA,
                                    "dob": DOB,
                                    "frequencyVisit": 0,
                                    "focusProduct": [
      
                                    ],
                                    "degree": "",
                                    "specialization": "",
                                    "email": "",
                                    "businessPotential": 0,
                                    "appStatus": "approved",
                                    "delStatus": "",
                                    "designationLevel": 1,
                                    "ipAddress": "",
                                    "licenceStatus": false,
                                    "mgrAppDate": new Date(),
                                    "finalAppDate": new Date(),
                                    "appByMgr": this.currentUser.id, //change Admin or MGR Id
                                    "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                                    "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                    "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                    "delByMgr": "",
                                    "finalDelBy": "",
                                    "onLocation": false
                                  }
                                }
                                // Here --insertion code for provider
                                if (!issueExist) {
                                  this.viewTable = false;
                                  this._providerService.uploadProviderData(providerDetails).subscribe( async ress => {
                                    await ress
                                    if (ress) {
                                      this.ressCount++;
                                      (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                      if (this.ressCount == this.totalExcelData) {
                                        (Swal as any).fire({
                                          title: "Providers added successfully !!!",
                                          type: "success",
                                          confirmButtonColor: '#3085d6',
                                          confirmButtonText: 'OK'
                                        }).then(() =>  window.location.reload())

                                          (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                      }
                                    }
                                  })
                                }
                              }
                            })
                        } 
                        else{
                          await areaList.push(areaName)
                          console.log('else.. jab user area nahi mila...number 5');
                          
                          // will do area mapping and will create providers--
                       
                          if (!issueExist) {
                            this.viewTable = false;
                                let areaMapObj = {
                                  "companyId": this.currentUser.companyId,
                                  "stateId": this.uploadingData.value.stateInfo,
                                  "districtId": this.uploadingData.value.districtInfo,
                                  "areaId": areaRes.id,
                                  "userId": this.uploadingData.value.employeeId,
                                  "status": true,
                                  "mappedData": [
                                    "Doctor",
                                    "Chemist",
                                    "Stockist"
                                  ],
                                  "appStatus": "approved",
                                  "delStatus": "",
                                  "mgrAppDate": new Date(),
                                  "finalAppDate": new Date(),
                                  "appByMgr": this.currentUser.id,  //change Admin or MGR Id
                                  "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                                  "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                  "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                  "delByMgr": "",
                                  "finalDelBy": ""
                                }
                                this._userAreaMappingService.createUserAreaMapping(areaMapObj).subscribe(async responses => {
                                  await responses
                                  if (responses) {
                                    (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                  }
                                  if (providerType == "RMP") {
                                    providerDetails = {
                                      "stateId": this.uploadingData.value.stateInfo,
                                      "districtId": this.uploadingData.value.districtInfo,
                                      "blockId": areaRes.id,
                                      "providerType": providerType,
                                      "providerName": providerName,
                                      "providerCode": "",
                                      "category": category,
                                      "address": doctorAddress,
                                      "phone": mobile,
                                      "licenceNumber": "",
                                      "status": true,
                                      "userId": this.uploadingData.value.employeeId,
                                      "companyId": this.currentUser.companyId,
                                      "geoLocation": [],
                                      "doa": DOA,
                                      "dob": DOB,
                                      "frequencyVisit": frequencyVisit,
                                      "focusProduct": [
      
                                      ],
                                      "degree": "",
                                      "specialization": specialization,
                                      "email": "",
                                      "businessPotential": 0,
                                      "appStatus": "approved",
                                      "delStatus": "",
                                      "designationLevel": 1,
                                      "ipAddress": "",
                                      "licenceStatus": false,
                                      "mgrAppDate": new Date(),
                                      "finalAppDate": new Date(),
                                      "appByMgr": this.currentUser.id,  //change Admin or MGR Id
                                      "finalAppBy": this.currentUser.id,  //change Admin or MGR Id
                                      "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                      "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                      "delByMgr": "",
                                      "finalDelBy": "",
                                      "onLocation": false
                                    }
                                  } else {
                                    providerDetails = {
                                      "stateId": this.uploadingData.value.stateInfo,
                                      "districtId": this.uploadingData.value.districtInfo,
                                      "blockId": areaRes.id,
                                      "providerType": providerType,
                                      "providerName": providerName,
                                      "providerCode": "",
                                      "category": category,
                                      "address": doctorAddress,
                                      "phone": mobile,
                                      "licenceNumber": "",
                                      "status": true,
                                      "userId": this.uploadingData.value.employeeId,
                                      "companyId": this.currentUser.companyId,
                                      "geoLocation": [],
                                      "doa": DOA,
                                      "dob": DOB,
                                      "frequencyVisit": 0,
                                      "focusProduct": [
      
                                      ],
                                      "degree": "",
                                      "specialization": "",
                                      "email": "",
                                      "businessPotential": 0,
                                      "appStatus": "approved",
                                      "delStatus": "",
                                      "designationLevel": 1,
                                      "ipAddress": "",
                                      "licenceStatus": false,
                                      "mgrAppDate": new Date(),
                                      "finalAppDate": new Date(),
                                      "appByMgr": this.currentUser.id, //change Admin or MGR Id
                                      "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                                      "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                      "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                      "delByMgr": "",
                                      "finalDelBy": "",
                                      "onLocation": false
                                    }
                                  }
                                  this._providerService.uploadProviderData(providerDetails).subscribe(async providerres => {
                                    await providerres
                                    if (providerres) {
                                      this.showProcessing = false;
                                      this.ressCount++;
                                      console.log('ressCount...', this.ressCount);
                                      if (this.ressCount == this.totalExcelData) {
                                        (Swal as any).fire({
                                          title: "Providers added successfully !!!",
                                          type: "success",
                                          confirmButtonColor: '#3085d6',
                                          confirmButtonText: 'OK'
                                        }).then((result) => {
                                          if (result.value) {
                                            window.location.reload();
                                          }
                                        })
                                          (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                      }
                                      (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
      
                                    }
                                  })
                                });
      
                          }
                        }
                      })
                   
                    });
                  } else {
                    console.log("area master m koi data  nahi mila hai so create krne aaya hu...... number 7...");
                    areaList.push(areaName)
                    // Insert Area into Area Collection
                    let newAreaObject = {
                      "stateId": this.uploadingData.value.stateInfo,
                      "districtId": this.uploadingData.value.districtInfo,
                      "areaName": areaName.toUpperCase(),
                      "areaCode": "",
                      "companyId": this.currentUser.companyId,
                      "type": areaType,
                      "status": true,
                      "appStatus": "approved",
                      "delStatus": "",
                      "ipAddress": "",
                      "geoLocation": [],
                      "mgrAppDate": new Date(),
                      "finalAppDate": new Date(),
                      //  "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                      //  "finalDelDate": "1900-01-01T00:00:00.000+0000",
                      "appByMgr": "",
                      "finalAppBy": "",
                      "delByMgr": "",
                      "finalDelBy": ""
                    }
                    if (!issueExist) {
                      this.viewTable = false;
                      this._areaService.createArea(this.currentUser, newAreaObject).subscribe( async area => {
                        await area
                        this._toastrService.success("Area created successfully !!!");
                        if (area) {
                          (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;

                        }
                        if (JSON.parse(JSON.stringify(area)).id !== undefined) {
                          let areaId = JSON.parse(JSON.stringify(area)).id;
                          let areaMapObj = {
                            "companyId": this.currentUser.companyId,
                            "stateId": this.uploadingData.value.stateInfo,
                            "districtId": this.uploadingData.value.districtInfo,
                            "areaId": areaId,
                            "userId": this.uploadingData.value.employeeId,
                            "status": true,
                            "mappedData": [
                              "Doctor",
                              "Chemist",
                              "Stockist"
                            ],
                            "appStatus": "approved",
                            "delStatus": "",
                            "mgrAppDate": new Date(),
                            "finalAppDate": new Date(),
                            "appByMgr": this.currentUser.id,  //change Admin or MGR Id
                            "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                            "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                            "finalDelDate": "1900-01-01T00:00:00.000+0000",
                            "delByMgr": "",
                            "finalDelBy": ""
                          }
                          this._userAreaMappingService.createUserAreaMapping(areaMapObj).subscribe(responses => {
                            if (responses) {
                              // this._toastrService.success("Area mapped successfully !!!");
                              (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;

                            }
                            if (providerType == "RMP") {
                              providerDetails = {
                                "stateId": this.uploadingData.value.stateInfo,
                                "districtId": this.uploadingData.value.districtInfo,
                                "blockId": areaId,
                                "providerType": providerType,
                                "providerName": providerName,
                                "providerCode": "",
                                "category": category,
                                "address": doctorAddress,
                                "phone": mobile,
                                "licenceNumber": "",
                                "status": true,
                                "userId": this.uploadingData.value.employeeId,
                                "companyId": this.currentUser.companyId,
                                "geoLocation": [],
                                "doa": DOA,
                                "dob": DOB,
                                "frequencyVisit": frequencyVisit,
                                "focusProduct": [

                                ],
                                "degree": "",
                                "specialization": specialization,
                                "email": "",
                                "businessPotential": 0,
                                "appStatus": "approved",
                                "delStatus": "",
                                "designationLevel": 1,
                                "ipAddress": "",
                                "licenceStatus": false,
                                "mgrAppDate": new Date(),
                                "finalAppDate": new Date(),
                                "appByMgr": this.currentUser.id,  //change Admin or MGR Id
                                "finalAppBy": this.currentUser.id,  //change Admin or MGR Id
                                "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                "delByMgr": "",
                                "finalDelBy": "",
                                "onLocation": false
                              }
                            } else {
                              providerDetails = {
                                "stateId": this.uploadingData.value.stateInfo,
                                "districtId": this.uploadingData.value.districtInfo,
                                "blockId": areaId,
                                "providerType": providerType,
                                "providerName": providerName,
                                "providerCode": "",
                                "category": category,
                                "address": doctorAddress,
                                "phone": mobile,
                                "licenceNumber": "",
                                "status": true,
                                "userId": this.uploadingData.value.employeeId,
                                "companyId": this.currentUser.companyId,
                                "geoLocation": [],
                                "doa": DOA,
                                "dob": DOB,
                                "frequencyVisit": 0,
                                "focusProduct": [

                                ],
                                "degree": "",
                                "specialization": "",
                                "email": "",
                                "businessPotential": 0,
                                "appStatus": "approved",
                                "delStatus": "",
                                "designationLevel": 1,
                                "ipAddress": "",
                                "licenceStatus": false,
                                "mgrAppDate": new Date(),
                                "finalAppDate": new Date(),
                                "appByMgr": this.currentUser.id, //change Admin or MGR Id
                                "finalAppBy": this.currentUser.id, //change Admin or MGR Id
                                "mgrDelDate": "1900-01-01T00:00:00.000+0000",
                                "finalDelDate": "1900-01-01T00:00:00.000+0000",
                                "delByMgr": "",
                                "finalDelBy": "",
                                "onLocation": false
                              }
                            }
                            this._providerService.uploadProviderData(providerDetails).subscribe(providerres => {
                              console.log('else...number 8');
                              if (providerres) {
                                // this._toastrService.success("Providers added successfully !!!");
                                this.showProcessing = false;
                                (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;

                              }
                            })
                          });

                        }
                      })
                    }
                    // Area Create service called


                    // first aread need to create after that other operation will be performed
                  }
                })
              }

            } else {
              issueExist = true;
              this.showProcessing = false;

              (Swal as any).fire({
                title: "Error",
                type: "error",
                text: `Area Undefined at line ${n + 2}
`,
              });
              // this.DataNotUpdated.push(datas[n]);
              // inform user regarding this
            }
          } else {
            issueExist = true;
            this.viewTable = true;
            this.showProcessing = false;
            // this.DataNotUpdated.push(datas[n]);
            // inform user regarding this

          }

        }
    
    
    
      } else if (this.selectedType == "Product" && datas.length > 0) {
        this.DataNotUpdated = [];
        for (var n = 0; n < datas.length; n++) {
          const data = JSON.parse(JSON.stringify(datas[n]));
          /**
           * validations
           */
          if (!data.Product_Name) {
            issueExist = true;
            this.showProcessing = false;
            this.viewTable = false;
            let newObject = { ...data };
            newObject['remarks'] = `Product Name doesn't exist at ${n + 2}`;
            this.DataNotUpdated.push(newObject);
            this.alreadyExistProductsTable();
            (Swal as any).fire({
              title: " Alert !!",
              type: "error",
              text: `Product's data is missing !!`,
            });
            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
          } else if (!data.Product_Type) {
            issueExist = true;
            this.showProcessing = false;
            let newObject = { ...data };
            newObject['remarks'] = `Product Type doesn't exist at ${n + 2}`;
            this.DataNotUpdated.push(newObject);
            this.alreadyExistProductsTable();
            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
          } else if (!data.MRP) {
            issueExist = true;
            this.showProcessing = false;
            let newObject = { ...data };
            newObject['remarks'] = `Product MRP doesn't exist at ${n + 2}`;
            this.DataNotUpdated.push(newObject);
            this.alreadyExistProductsTable();
            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
          }
          
          if (!issueExist) {
            if (data.Product_Name !== undefined && data.Product_Name !== "") {
              let productobj = {};
              let divisionName = "", productName = "", productCode = "", productType = "", productShortName = "", productPackageSize = "", ptw = 0, ptr = 0, mrp = 0, stateName = "", sampleRate = 0, samplePackageSize = "", includeVat = 0;
              
              if (data.Division_Name !== undefined && data.Division_Name !== "") {
                divisionName = data.Division_Name;
              }
              if (data.Product_Name !== undefined && data.Product_Name !== "") {
                productName = data.Product_Name;
              }
              if (data.productCode !== undefined && data.productCode !== "") {
                productCode = data.Product_Code;
              }
              if (data.Product_Type !== undefined && data.Product_Type !== "") {
                productType = data.Product_Type;
              }
              if (data.Product_Short_Name !== undefined && data.Product_Short_Name !== "") {
                productShortName = data.Product_Short_Name;
              }
              if (data.Product_Package_Size !== undefined && data.Product_Package_Size !== "") {
                productPackageSize = data.Product_Package_Size;
              }
              if (data.PTW_OR_PTS !== undefined && typeof (data.PTW_OR_PTS) == 'number') {
                ptw = data.PTW_OR_PTS;
              }
              if (data.PTR !== undefined && typeof (data.PTR) == 'number') {
                ptr = data.PTR;
              }
              if (data.MRP !== undefined && typeof (data.MRP) == 'number') {
                mrp = data.MRP;
              }
              if (data.State !== undefined && data.State !== "") {
                stateName = data.State;
              }
              if (data.Sample_Rate !== undefined && data.Sample_Rate !== "") {
                //console.log("Sample Rate==" + data.Sample_Rate)
                sampleRate = data.Sample_Rate;
              }
              if (data.Sample_Package_Size !== undefined && data.Sample_Package_Size !== "") {
                samplePackageSize = data.Sample_Package_Size;
              }
              if (data.includeVat !== undefined && data.includeVat !== "") {
                includeVat = data.includeVat;
              }
              let productFilter = {};
              productFilter = {
                companyId: this.currentUser.companyId,
                productName: productName,
                ptw: ptw,
                ptr: ptr,
                mrp: mrp,
                status: true
              }
              this._productService.checkProductExistStatus(productFilter).subscribe(productRes => {
                
                if (productRes.length > 0) {
                  issueExist = true;
                  this.showProcessing = false;
                  let newObject = { ...data };
                  newObject['remarks'] = `${productRes[0].productName} already exist in database`;
                  this.DataNotUpdated.push(newObject);
                  this.alreadyExistProductsTable();
                  (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                  (Swal as any).fire({
                    title: `${productRes[0].productName} already exist in database`,
                    type: "info",
                  });
                  (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                } else {
                  // Product Insertion code with divisionId
                  if (this.currentUser.company.isDivisionExist == true) {
                    
                    if (divisionName !== undefined && divisionName !== "") {
                      let divisionId;
                      let param = {
                        where:{
                          companyId: this.currentUser.companyId,
                          divisionName: divisionName,
                          // designationLevel:this.currentUser.userInfo[0].designationLevel
                        }
                      }
                      // Check division exist status
                      this._divisionService.getDivisionNew(param).subscribe(divisionRes => {
                        
                        if (divisionRes.length > 0) {
                          divisionId = divisionRes[0].id;
                          productobj = {
                            "divisionId": [divisionId],
                            "productName": productName,
                            "productCode": productCode,
                            "productType": productType,
                            "productShortName": productShortName,
                            "productPackageSize": productPackageSize,
                            "ptw": ptw,
                            "mrp": mrp,
                            "ptr": ptr,
                            "sampleRate" : 0, 
                            "includeVat" :0, 
                            "assignedStates": this.uploadingData.value.stateInfo,
                            "fileId": [],
                            "status": true,
                            "updatedBy":"Preeti"
                          }
                          

                          if (!issueExist) {
                            this._productService.createProduct(productobj, this.currentUser.companyId).subscribe(productRes => {
                              if (productRes) {
                                this.ressCount++;
                                (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                this.showProcessing = false;
                                if (this.ressCount == this.totalExcelData) {
                                  this.dataUploading = false;
                                  (Swal as any).fire({
                                    title: "Products added successfully !!!",
                                    type: "success",
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'OK'
                                  }).then((result) => {
                                    if (result.value) {
                                      window.location.reload();
                                    }
                                  })
                                    (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                }
                              }
                            });
                          }

                        } else {
                          // Create new division 
                          this._divisionService.createDivision(param).subscribe(divisionRes => {
                            divisionId = divisionRes[0].id;
                            productobj = {
                              "divisionId": this.currentUser.divisionId,
                              "productName": productName,
                              "productCode": productCode,
                              "productType": productType,
                              "productShortName": productShortName,
                              "productPackageSize": productPackageSize,
                              "ptw": ptw,
                              "mrp": mrp,
                              "ptr": ptr,
                              "assignedStates": this.uploadingData.value.stateInfo,
                              "status": true,
                              "fileId": []
                            }
                            if (!issueExist) {
                              this._productService.createProduct(productobj, this.currentUser.companyId).subscribe(productRes => {
                                if (productRes) {
                                  this.ressCount++;
                                  (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                  this.showProcessing = false;
                                  if (this.ressCount == this.totalExcelData) {
                                    this.dataUploading = false;
                                    (Swal as any).fire({
                                      title: "Products added successfully !!!",
                                      type: "success",
                                      confirmButtonColor: '#3085d6',
                                      confirmButtonText: 'OK'
                                    }).then((result) => {
                                      if (result.value) {
                                        window.location.reload();
                                      }
                                    })
                                      (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                                  }
                                }
                              })
                            }
                          })
                        }
                      })
                    } else {
                    }
                  } else {
                    // Product Insertion code without division concept
                    productobj = {
                      "productName": productName,
                      "productCode": productCode,
                      "productType": productType,
                      "productShortName": productShortName,
                      "productPackageSize": productPackageSize,
                      "ptw": ptw,
                      "mrp": mrp,
                      "ptr": ptr,
                      "assignedStates": this.uploadingData.value.stateInfo,
                      "sampleRate": sampleRate,
                      "samplePackageSize": samplePackageSize,
                      "includeVat": includeVat,
                      "status": true
                    }
                    if (!issueExist) {
                      this._productService.createProduct(productobj, this.currentUser.companyId).subscribe(productRes => {
                        if (productRes) {
                          this.ressCount++;
                          (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                          this.showProcessing = false;
                          if (this.ressCount == this.totalExcelData) {
                            this.dataUploading = false;
                            (Swal as any).fire({
                              title: "Products added successfully !!!",
                              type: "success",
                              confirmButtonColor: '#3085d6',
                              confirmButtonText: 'OK'
                            }).then((result) => {
                              if (result.value) {
                                window.location.reload();
                              }
                            })
                              (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
                          }
                        }
                      });
                    }
                    // Product Insertion code

                  }
                  //}
                }
              })


            } else {
              // this.DataNotUpdated.push(datas[n]);
            }
          }
        }
        
      } else if (this.selectedType == "Employee" && datas.length > 0) {
        this.DataNotUpdated = [];
        for (let n = 0; n < datas.length; n++) {
          const data = JSON.parse(JSON.stringify(datas[n]));

          if (data.Name && data.Name.trim().length > 0 &&
            data.State && data.State.trim().length > 0 &&
            data.Mobile && typeof data.Mobile == "number" &&
            data.Headquater && data.Headquater.trim().length > 0 &&
            data.Designation && data.Designation.trim().length > 0 &&
            data.Username && data.Username.trim().length > 0 &&
            data.Password && data.Password.trim().length > 0) {

            if (validator.isEmail(data.Email) !== true) {
              const newObject = { ...data };
              newObject.remarks = "Employee Email is not validated";
              this.DataNotUpdated.push(newObject);
            }

            //this._userDetailService.createLogin(userLoginObject, this.currentUser).subscribe(async userLogins => {
            const stateObj = { "where": { "stateName": data.State.toUpperCase() } };
            this._stateService.getStateDetail(stateObj).subscribe(stateResult => {

              if (stateResult.length > 0) {

                if (!stateResult[0].assignedTo.includes(this.currentUser.companyId.toString())) {
                  //State Not Assigned...
                  const dataToBeUpdate = stateResult;
                  dataToBeUpdate.assignedTo.push(this.currentUser.companyId);
                  dataToBeUpdate.updatedAt = new Date();
                  this._stateService.updateStateDetail(stateObj, dataToBeUpdate).subscribe();
                }
                //--------------------Checking District------------------
                const hqObj = { where: { stateId: stateResult[0].stateId, districtName: data.Headquater.toUpperCase() } };
                this._districtservice.getDistrictNew(hqObj).subscribe(hqResult => {

                  if (hqResult.length > 0) {
                    if (!hqResult[0].assignedTo.includes(this.currentUser.companyId.toString())) {
                      //Hq Not Assigned...
                      const dataToBeUpdate = hqResult[0];
                      dataToBeUpdate.assignedTo.push(this.currentUser.companyId);
                      dataToBeUpdate.updatedAt = new Date();
                      this._districtservice.updateDistrictDetail(hqObj.where, dataToBeUpdate).subscribe();
                    }

                    //Checking Designation------
                    const desObj = {
                      "companyId": this.currentUser.companyId,
                      "designation": data.Designation
                    }
                    this._desigService.getDesignationId(desObj).subscribe(desRes => {
                      if (desRes.length > 0) {
                        const checkUserExist = {
                          where: {
                            companyId: this.currentUser.companyId,
                            name: data.Name,
                            mobile: data.Mobile
                          }
                        }
                        const checkUsernameExist = {
                          where: {
                            companyId: this.currentUser.companyId,
                            username: data.Username
                          }
                        }
                        forkJoin(
                          this._userDetailService.getUsersList(checkUserExist),
                          this._userDetailService.getUsersList(checkUsernameExist)
                        ).subscribe(userResult => {
                          if (userResult[0].length > 0) {
                            const newObject = { ...data };
                            newObject.remarks = `Employee Name and Mobile no. already exist.`;
                            this.DataNotUpdated.push(newObject);
                          }

                          if (userResult[1].length > 0) {

                            const newObject = { ...data };
                            newObject.remarks = `Employee Username already exist.`;
                            this.DataNotUpdated.push(newObject);

                          }
                        });


                      } else {
                        const newObject = { ...data };
                        newObject.remarks = `Employee Designation does not exist in master, Please Create First`;
                        this.DataNotUpdated.push(newObject);
                      }
                    });

                  } else {
                    const newObject = { ...data };
                    newObject.remarks = `Employee ${this.currentUser.company.lables.hqLabel} does not exist in master, Please Create First`;
                    this.DataNotUpdated.push(newObject);
                  }
                });
              } else {
                //State Not Created....
                const newObject = { ...data };
                newObject.remarks = "Employee State is not in State Master, Please Create First";
                this.DataNotUpdated.push(newObject);
              }
            });
            //});
          } else {
            if (!data.Name) {
              const newObject = { ...data };
              newObject.remarks = "Employee Name does not exist.";
              this.DataNotUpdated.push(newObject);
            }
            if (!data.Mobile) {
              const newObject = { ...data };
              newObject.remarks = "Employee Mobile Number does not exist.";
              this.DataNotUpdated.push(newObject);
            } else if (typeof data.Mobile !== "number") {
              const newObject = { ...data };
              newObject.remarks = "Employee Mobile Number is not valid.";
              this.DataNotUpdated.push(newObject);
            }
            if (!data.State) {
              const newObject = { ...data };
              newObject.remarks = `Employee State does not exist.`;
              this.DataNotUpdated.push(newObject);
            }
            if (!data.Headquater) {
              const newObject = { ...data };
              newObject.remarks = `Employee ${this.currentUser.company.lables.hqLabel} does not exist.`;
              this.DataNotUpdated.push(newObject);
            }
            if (!data.Designation) {
              const newObject = { ...data };
              newObject.remarks = "Employee Designation does not exist.";
              this.DataNotUpdated.push(newObject);
            }
            if (!data.Email) {
              const newObject = { ...data };
              newObject.remarks = "Employee Email does not exist.";
              this.DataNotUpdated.push(newObject);
            }
            if (!data.Username) {
              const newObject = { ...data };
              newObject.remarks = "Employee Username does not exist.";
              this.DataNotUpdated.push(newObject);
            }
            if (!data.Password) {
              const newObject = { ...data };
              newObject.remarks = "Employee Password does not exist.";
              this.DataNotUpdated.push(newObject);
            }
          }




        }

        setTimeout(() => {
          if (this.DataNotUpdated.length > 0) {
            this.dtOptions = {
              "pagingType": 'full_numbers',
              "paging": true,
              "ordering": true,
              "info": true,
              "scrollY": 300,
              "scrollX": true,
              "fixedColumns": true,
              destroy: true,
              data: this.DataNotUpdated,
              responsive: true,
              "autoWidth": true,

              columns: [
                {
                  title: 'Remarks',
                  data: 'remarks',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                }, {
                  title: 'Designation',
                  data: 'Designation',
                  render: function (data) {
                    if (data === '') {
                      return '-----'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '-----'
                },
                {
                  title: 'Name',
                  data: 'Name',
                  render: function (data) {
                    if (data === '') {
                      return '-----'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '-----'
                },
                {
                  title: 'Address',
                  data: 'Address',
                  render: function (data) {
                    if (data === '') {
                      return '----'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '----'
                },
                {
                  title: 'State',
                  data: 'State',
                  render: function (data) {
                    if (data === '') {
                      return '-----'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '-----'
                }
                ,
                {
                  title: 'Headquater',
                  data: 'Headquater',
                  render: function (data) {
                    if (data === '') {
                      return '-----'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '-----'
                },
                {
                  title: 'DOB_yyyy_mm_dd',
                  data: 'DOB_yyyy_mm_dd',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'Anniversary_Date_yyyy_mm_dd',
                  data: 'Anniversary_Date_yyyy_mm_dd',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'DOJ_yyyy_mm_dd',
                  data: 'DOJ_yyyy_mm_dd',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'Mobile',
                  data: 'Mobile',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'Email',
                  data: 'Email',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'Phone_No',
                  data: 'Phone_No',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'Gender',
                  data: 'Gender',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'Username',
                  data: 'Username',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                },
                {
                  title: 'Password',
                  data: 'Password',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'
                }
              ],
              dom: 'Bfrtip',
              buttons: [
                'copy', 'csv', 'excel', 'print'
              ]
            };
            this.changedetectorref.detectChanges();
            this.showProcessing = false;
            this.viewTable = true;
            this.dataTable = $(this.table1.nativeElement);
            this.dataTable.DataTable(this.dtOptions);
            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
          } else {
            for (let n = 0; n < datas.length; n++) {
              const data = JSON.parse(JSON.stringify(datas[n]));

              const stateObj = { "where": { "stateName": data.State.toUpperCase() } };
              this._stateService.getStateDetail(stateObj).subscribe(stateResult => {

                if (stateResult.length > 0) {

                  //--------------------Checking District------------------
                  const hqObj = { where: { stateId: stateResult[0].stateId, districtName: data.Headquater.toUpperCase() } };
                  this._districtservice.getDistrictNew(hqObj).subscribe(hqResult => {

                    if (hqResult.length > 0) {

                      //Getting Designation------
                      const desObj = {
                        "companyId": this.currentUser.companyId,
                        "designation": data.Designation
                      }
                      this._desigService.getDesignationId(desObj).subscribe(desRes => {
                        if (desRes.length > 0) {
                          const userLoginObject = {
                            "companyId": this.currentUser.companyId,
                            "companyName": this.currentUser.companyName,
                            "name": data.Name,
                            "address": data.hasOwnProperty("Address") ? data.Address : '',
                            "dateOfBirth": new Date(data.DOB_yyyy_mm_dd),
                            "dateOfAnniversary": new Date(data.DOB_yyyy_mm_dd),
                            "dateOfJoining": new Date(data.DOB_yyyy_mm_dd),
                            "aadhaar": "",
                            "PAN": "",
                            "DLNumber": "",
                            "mobile": data.Mobile,
                            "email": data.hasOwnProperty("Email") ? data.Email.indexOf("@") > -1 ? data.Email : 'noemailexist@domain.com' : 'noemailexist@domain.com',
                            "emailVerified": false,
                            // "Phone_No": data.hasOwnProperty("Phone_No") ? typeof (data.Phone_No) === "number" ? data.Phone_No : '0000000000' : '0000000000',
                            "age": 0,
                            "gender": data.hasOwnProperty("Gender") ? data.Gender.trim().length > 0 ? data.Gender : 'Other' : 'Other',
                            "username": data.Username,
                            "password": data.Password,
                            "viewPassword": data.Password,
                          };
                          this._userDetailService.createLogin(userLoginObject, this.currentUser).subscribe(userLoginResult => {
                            const userInfoDetails = {
                              "stateId": stateResult[0].id,
                              "districtId": hqResult[0].id,
                              "designation": { "designation": desRes[0].designation, "designationLevel": desRes[0].designationLevel },
                              "name": data.Name,
                              "dateOfReporting": new Date(),
                              "lockingPeriod": this.currentUser.company.validation.editLockingPeriod,
                              "religion": "Others"
                            }
                            this._userDetailService.createUserInfo(userInfoDetails, userLoginResult, this.currentUser).subscribe(userInfo => {
                              console.log("Done From Here...");

                            });
                          });

                        }
                      });

                    }
                  });
                }
              });


            }

            this.viewTable = false;
            this.showProcessing = false;
            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
            this._toastrService.success("Employee list has been uploaded successfully !!!");
          }
        }, 3000)

      } else if (this.selectedType == "Holiday" && datas.length > 0) {
        let errorObject;
        this.DataNotUpdated = [];
        let Holidays = [];
        let issues = [];
        datas.forEach((i, index) => {
          let issue = false;
          let err = {};

          // let holidayDatefrom = new Date(new Date(new Date().setMonth(parseInt(i.Holiday_From_Date.substr(3, 2)) - 1)).setDate(i.Holiday_From_Date.substr(0, 2))).setFullYear(i.Holiday_From_Date.substr(6, 4))
          // let holidayDateto = new Date(new Date(new Date().setMonth(parseInt(i.Holiday_To_Date.substr(3, 2)) - 1)).setDate(i.Holiday_To_Date.substr(0, 2))).setFullYear(i.Holiday_To_Date.substr(6, 4))
          // let holidayDatefrom = new Date(new Date(new Date().setMonth(parseInt(i.Holiday_From_Date.substr(3, 2)) - 1)).setDate(i.Holiday_From_Date.substr(0, 2))).setFullYear(i.Holiday_From_Date.substr(6, 4))
          // let holidayDateto = new Date(new Date(new Date().setMonth(parseInt(i.Holiday_To_Date.substr(3, 2)) - 1)).setDate(i.Holiday_To_Date.substr(0, 2))).setFullYear(i.Holiday_To_Date.substr(6, 4))
          let dates = this.getDatesArray(new Date(i.From_yyyy_mm_dd), new Date(i.To_yyyy_mm_dd));
          console.log('dates',dates)
          if(!dates.length){
            issue = true;
            err["dateFrom"] = `Holiday's From Date at line no ${index + 2} is Greater than To Date`
            err["dateTo"] = `Holiday's To Date at line no ${index + 2} is smaller than From Date`
          }

          if (!(i.From_yyyy_mm_dd instanceof Date) ) {
            issue = true;
            err["dateFrom"] = `Holiday's From Date at line no ${index + 2} is invalid`
          }
          if(!i.From_yyyy_mm_dd){
            issue = true;
            err["dateFrom"] = `Holiday's From Date is required at line no ${index + 2}`
          }
          if (!(i.To_yyyy_mm_dd instanceof Date) ) {
            issue = true;
            err["dateTo"] = `Holiday's To Date at line no ${index + 2} is invalid`
          }
          if(!i.To_yyyy_mm_dd){
            issue = true;
            err["dateTo"] = `Holiday's To Date is required at line no ${index + 2}`
          }
          if(!i.Holiday_Name){
            issue = true;
            err["name"] = `Holiday's Name is required at line no ${index + 2}`
          }
          for(let d = 0; d <= dates.length-1; d++){
            let holidayobj = {
              "companyId": this.currentUser.companyId,
              "assignedState": this.uploadingData.value.stateInfo,
              "holidayDate": dates[d],
              "holidayName": i.Holiday_Name,
              "status": true
            }
            Holidays.push(holidayobj);
          }
          if(issue){
            issues.push(err);
          }
          
        });
        if (issues.length) {
          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            destroy: true,
            data: issues,
            responsive: true,
            "autoWidth": true,

            columns: [
              {
                title: 'Name',
                data: 'Name',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'From Date',
                data: 'dateFrom',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'To Date',
                data: 'dateTo',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }
            ],
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'print'
            ]
          }
          
          this.viewTable = true;
          this.dataTable = $(this.table1.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          this.showProcessing = false;
          (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
          
        } else {
          this._holidayService.uploadHolidays(Holidays).subscribe(res => {
            console.log('creayed holidays ', res);
            this.showProcessing = false;
            this.selectedType = "";
            this.uploadingData.reset();
            this.fileRef.nativeElement.value = null;
            this.callStateComponent.setBlank();
            (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
            (Swal as any).fire({
              title: "Holidays added successfully !",
              type: "success",
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'OK'
            })
          })
        }
      }else {
        for (var n = 0; n < datas.length; n++) {
          let holidayobj = {};
          let date = "", holiday = "";
          if (JSON.parse(JSON.stringify(datas[n])).Date !== undefined && JSON.parse(JSON.stringify(datas[n])).Date !== "") {
            date = JSON.parse(JSON.stringify(datas[n])).Date;
          }
          if (JSON.parse(JSON.stringify(datas[n])).Holiday !== undefined && JSON.parse(JSON.stringify(datas[n])).Holiday !== "") {
            holiday = JSON.parse(JSON.stringify(datas[n])).Holiday;
          }
          holidayobj = {
            "companyId": this.currentUser.companyId,
            "assignedState": [this.uploadingData.value.stateInfo],
            "holidayDate": new Date(date),
            "holidayName": holiday,
            "status": true
          }
          this._holidayService.uploadHolidays(holidayobj).subscribe(holidayRes => {

          });
        }
      }

      if (this.DataNotUpdated.length > 0 && (this.selectedType == "Doctor")) {
        var self = this;
        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: self.DataNotUpdated,
          columns: [
            {
              title: "Remarks",
              data: 'remarks',
              render: function (data) {
                if (data === '') {
                  return `---`
                } else {
                  return ` <span style="color:red">${data}</span>`
                }
              },
              defaultContent: `---`
            },
            {
              title: this.currentUser.company.lables.doctorLabel + "_Name",
              data: 'Doctor_Name',
              render: function (data) {
                if (data === '') {
                  return ` <span style="color:red">${this.currentUser.company.lables.doctorLabel} Name should not be blank</span>`
                } else {
                  return data
                }
              },
              defaultContent: ` <span style="color:red">${this.currentUser.company.lables.doctorLabel} Name should not be blank </span>`
            },
            {
              title: 'Address',
              data: 'Address',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Area',
              data: 'Area',
              render: function (data) {
                if (data === '') {
                  return ` <span style="color:red"> ${this.currentUser.company.lables.areaLabel} should not be blank </span>`
                } else {
                  return data
                }
              },
              defaultContent: ` <span style="color:red"> ${this.currentUser.company.lables.areaLabel} should not be blank </span>`
            },
            {
              title: 'Area_Type',
              data: 'Area_Type',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }
            ,
            {
              title: 'Category',
              data: 'Category',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }
            ,
            {
              title: 'DOA',
              data: 'DOA',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'DOB',
              data: 'DOB',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Degree',
              data: 'Degree',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Frequency_Visit',
              data: 'Frequency_Visit',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Headquarter',
              data: 'Headquarter',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Mobile',
              data: 'Mobile',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Specialization',
              data: 'Specialization',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }],
          dom: 'Bfrtip',
          buttons: [
            'copy', 'csv', 'excel', 'print'
          ]
        };
        (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
        this.showProcessing = false;
        this.viewTable = true;
        this.dataTable = $(this.table1.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
      } else if (this.DataNotUpdated.length > 0 && (this.selectedType == "Chemist")) {
        var self = this;
        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: self.DataNotUpdated,
          columns: [{
            title: "Remarks",
            data: 'remarks',
            render: function (data) {
              if (data === '') {
                return `---`
              } else {
                return ` <span style="color:red">${data}</span>`
              }
            },
            defaultContent: `---`
          },
          {
            title: this.currentUser.company.lables.vendorLabel + "_Name",
            data: 'Doctor_Name',
            render: function (data) {
              if (data === '') {
                return `<span style="color:red">${this.currentUser.company.lables.vendorLabel} Name should not be blank</span>`
              } else {
                return data
              }
            },
            defaultContent: `<span style="color:red">${this.currentUser.company.lables.vendorLabel} Name should not be blank </span>`
          },
          {
            title: 'Address',
            data: 'Address',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Area',
            data: 'Area',
            render: function (data) {
              if (data === '') {
                return ` <span style="color:red"> ${this.currentUser.company.lables.areaLabel} should not be blank </span>`
              } else {
                return data
              }
            },
            defaultContent: ` <span style="color:red"> ${this.currentUser.company.lables.areaLabel} should not be blank </span>`
          },
          {
            title: 'Area_Type',
            data: 'Area_Type',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }
            ,
          {
            title: 'Category',
            data: 'Category',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }
            ,
          {
            title: 'DOA',
            data: 'DOA',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'DOB',
            data: 'DOB',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Frequency_Visit',
            data: 'Frequency_Visit',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Headquarter',
            data: 'Headquarter',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Mobile',
            data: 'Mobile',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }],
          dom: 'Bfrtip',
          buttons: [
            'copy', 'csv', 'excel', 'print'
          ]
        };
        (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
        this.showProcessing = false;
        this.viewTable = true;
        this.dataTable = $(this.table1.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
      } else if (this.DataNotUpdated.length > 0 && (this.selectedType == "Stockist")) {
        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: this.DataNotUpdated,
          columns: [
            {
              title: "Remarks",
              data: 'remarks',
              render: function (data) {
                if (data === '') {
                  return `---`
                } else {
                  return ` <span style="color:red">${data}</span>`
                }
              },
              defaultContent: `---`
            }, {
              title: this.currentUser.company.lables.stockistLabel + "_Name",
              data: 'Doctor_Name',
              render: function (data) {
                if (data === '') {
                  return ` <span style="color:red">${this.currentUser.company.lables.stockistLabel} Name should not be blank</span>`
                } else {
                  return data
                }
              },
              defaultContent: ` <span style="color:red">${this.currentUser.company.lables.stockistLabel} Name should not be blank </span>`
            },
            {
              title: 'Address',
              data: 'Address',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Area',
              data: 'Area',
              render: function (data) {
                if (data === '') {
                  return ` <span style="color:red"> ${this.currentUser.company.lables.areaLabel} should not be blank </span>`
                } else {
                  return data
                }
              },
              defaultContent: ` <span style="color:red"> ${this.currentUser.company.lables.areaLabel} should not be blank </span>`
            },
            {
              title: 'Area_Type',
              data: 'Area_Type',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }
            ,
            {
              title: 'Category',
              data: 'Category',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }
            ,
            {
              title: 'DOA',
              data: 'DOA',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'DOB',
              data: 'DOB',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Frequency_Visit',
              data: 'Frequency_Visit',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Headquarter',
              data: 'Headquarter',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Mobile',
              data: 'Mobile',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }],
          dom: 'Bfrtip',
          buttons: [
            'copy', 'csv', 'excel', 'print'
          ]
        };
        (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
        this.showProcessing = false;
        this.viewTable = true;
        this.dataTable = $(this.table1.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
      }

    }
    
    fileReader.readAsArrayBuffer(this.file);
    setTimeout(() => {
      (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
    }, 1000);
    
  }

      alreadyExistProductsTable(){
        if (this.DataNotUpdated.length > 0 && this.selectedType == "Product") {
          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            destroy: true,
            data: this.DataNotUpdated,
            columns: [
              {
                title: "Remarks",
                data: 'remarks',
                render: function (data) {
                  if (data === '') {
                    return `---`
                  } else {
                    return ` <span style="color:red">${data}</span>`
                  }
                },
                defaultContent: `---`
              }, {
                title: "Product_Name",
                data: 'Product_Name',
                render: function (data) {
                  if (data === '') {
                    return 'Product_Name should not be blank'
                  } else {
                    return data
                  }
                },
                defaultContent: 'Product_Name should not be blank'
              },
              {
                title: 'Product_Type',
                data: 'Product_Type',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Product_Short_Name',
                data: 'Product_Short_Name',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Product_Package_Size',
                data: 'Product_Package_Size',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }
              ,
              {
                title: 'Company_Name',
                data: 'Company_Name',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }
              ,
              {
                title: 'PTW_PTS',
                data: 'PTW_PTS',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'DOB',
                data: 'DOB',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'PTR',
                data: 'MRP',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Sample_Rate',
                data: 'Sample_Rate',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Sample_Package_Size',
                data: 'Sample_Package_Size',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }],
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'print'
            ]
          };
          (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
          this.showProcessing = false;
          this.viewTable = true;
          this.dataTable = $(this.table1.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
        }
      }
      getDatesArray(start, stop) {
        let dateArray = [];
        let currentDate = moment(start);
        let stopDate = moment(stop);
        while (currentDate <= stopDate) {
          currentDate = moment(currentDate).add(1, 'days');
          dateArray.push( new Date(moment(currentDate).format('YYYY-MM-DD')) )
        }
        return dateArray;
    }
    // isValidDate(d) {
    //   return d instanceof Date && !isNaN(d);
    // }
}