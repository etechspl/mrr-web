import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorVendorTableComponent } from './doctor-vendor-table.component';

describe('DoctorVendorTableComponent', () => {
  let component: DoctorVendorTableComponent;
  let fixture: ComponentFixture<DoctorVendorTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorVendorTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorVendorTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
