import { Component, OnInit, Input, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: "m-doctor-vendor-table",
	templateUrl: "./doctor-vendor-table.component.html",
	styleUrls: ["./doctor-vendor-table.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DoctorVendorTableComponent implements OnInit {
	@Input("ChemistData") ChemistData: any;
	@Input("StockistData") StockistData: any;
	currentUser = JSON.parse(sessionStorage.currentUser);
	displayedColumns = ["providerName", "category", "Chemist"];
  doctorData;
  fg: FormGroup;
  selectedChemist = [];
  selectedStockist = [];


	constructor(
    private toastservice: ToastrService,
    private providerService:ProviderService,
    private fb: FormBuilder,
		public dialogRef: MatDialogRef<DoctorVendorTableComponent>,
		@Inject(MAT_DIALOG_DATA) data
	) {
		this.doctorData = data;
	}

	ngOnInit() {
    this.fg = this.fb.group({
      chemist: [null],
      stockist:[null]
    });

    this.doctorData.stockistList.map(item =>{
      this.doctorData.doctorDetails.stockist.map(itemi =>{
        if(itemi.id === item.id ){
          this.selectedStockist.push(item.id);
        }
      })
    });
    this.doctorData.chemistList.map(item => {
      this.doctorData.doctorDetails.chemist.map(itemi =>{
        if(itemi.id === item.id){
          this.selectedChemist.push(item.id);
        }
      });
    });

    // this.sortStockistAndChemist();
  }

  sortStockistAndChemist(){
    this.doctorData.chemistList.sort((a,b)=>{
      let A = a.providerName+''.toUpperCase();
      let B = b.providerName+''.toUpperCase();
      if(A < B){return -1} else if(A>B){return 1} else{return 0}
    });
    this.doctorData.stockistList.sort((a,b)=>{
      let A = a.providerName+''.toUpperCase();
      let B = b.providerName+''.toUpperCase();
      if(A < B){return -1} else if(A>B){return 1} else{return 0}
    });
  }

  selectChemist(event){
    this.selectedChemist = event.value;
  }
  
  selectStockist(event){
    this.selectedStockist = event.value;

  }
  Update(){

    let selectedVendors = [];
    selectedVendors = selectedVendors.concat(this.selectedChemist,this.selectedStockist);
    let query = {
      providerId: this.doctorData.doctorDetails.id,
      companyId: this.currentUser.companyId
    };
    let data = {
      providerId: this.doctorData.doctorDetails.id,
      providerName: this.doctorData.doctorDetails.providerName,
      companyId: this.currentUser.companyId,
      userId: this.doctorData.doctorDetails.userId,
      linkedVendor: selectedVendors,
      blockId: this.doctorData.doctorDetails.blockId,
      status: this.doctorData.doctorDetails.status,
      updatedAt: new Date().toISOString()
    }
    this.providerService.updateDoctorVendorLinking(query, data).subscribe(res=>{
      if(!res.error){
        this.toastservice.success('Assigned Successfully !')
        this.dialogRef.close("success");
      } else{
        this.toastservice.error('Something went wrong')
        this.dialogRef.close("fail");
      }
    })

  }

	closeModel() {
		this.dialogRef.close("cancel");
	}
}
