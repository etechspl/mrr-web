import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { GiftService } from '../../../../../core/services/Gift/gift.service';
import { TargetService } from '../../../../../core/services/Target/target.service';
import { SampleService } from '../../../../../core/services/sample/sample.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { StateComponent } from '../../filters/state/state.component';
import { MatSlideToggleChange } from '@angular/material';
import { ExportAsService, ExportAsConfig} from 'ngx-export-as';

@Component({
  selector: 'm-sample-gift-edit',
  templateUrl: './sample-gift-edit.component.html',
  styleUrls: ['./sample-gift-edit.component.scss']
})
export class SampleGiftEditComponent implements OnInit {

  showStateReportType = true;
  showEmpReportType = true;
  showHqReportType = true;
  moduleType = true;
  stateFilter = false;
  designationFilterForUserwiseOnly = false;
  districtFilter = false;
  employeeFilter = false;
  designationFilter = false;
  districtFilterForMulti = false;
  designationData = [];
  isShowIssueSample = false;
  isShowIssueGift = false;

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDistrictComponentAgainForMulti") callDistrictComponentAgainForMulti: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;


  constructor(
    private fb: FormBuilder,
    private _toastr: ToastrService,
    private userDetailsService: UserDetailsService,
    private _targetService: TargetService,
    private _samplesService: SampleService,
    private _giftService: GiftService,
    private _changeDetectorRef: ChangeDetectorRef,
    private exportAsService: ExportAsService
  ) { }

  ngOnInit() {
    this.reportType('Employeewise');
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
	  elementId: 	'tableId1' // the id of html/table element
  }

  exportAsConfig1: ExportAsConfig = {
    type:'xlsx',
    elementId : 'tableID'
  }

  displayedColumns = ['sno', 'productName', 'productCode', 'receipt'];
  currentUser = JSON.parse(sessionStorage.currentUser);
  editSampleAndGift = new FormGroup({
    reportType: new FormControl(null),
    stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null),
    userInfo: new FormControl(null, Validators.required),
    designation: new FormControl(null, Validators.required),
    moduleTypeName: new FormControl(null, Validators.required),
  })

  sample = new FormGroup({
    productName: new FormControl(null, Validators.required),
    productCode: new FormControl(null),
    productId: new FormControl(null),
    quantity: new FormControl(null),
  })

  gift = new FormGroup({
    giftName: new FormControl(null, Validators.required),
    giftCode: new FormControl(null),
    giftId: new FormControl(null),
    quantity: new FormControl(null),
  })

  productListForSample = [];
  sampleIssueDetailArr(productArr) {
    this.productListForSample = productArr;
    this.sample = this.fb.group({
      id: [[Validators.required]],
      stateName: [],
      districtName: [],
      userName: [],
      productName: [[Validators.required]],
      productId: [],
      opening: [],
      receipt: [],
      balance: [],
      issueDate: [],
      resultList: new FormArray([])
    });
    const productFGs = this.productListForSample.map(product => {
      return this.fb.group({
        id: [product.id],
        stateName: [product.stateName],
        districtName: [product.districtName],
        userName: [product.userName],
        productName: [product.productName],
        productId: [product.productId],
        opening: [product.opening],
        receipt: [product.receipt],
        balance: [product.balance],
        issueDate: [product.issueDate],
      })
    });
    const productFormArray: FormArray = this.fb.array(productFGs);
    this.sample.setControl('resultList', productFormArray);
  }

  productListForGift = [];
  giftDetailArr(productArr) {
    this.productListForGift = productArr;
    this.gift = this.fb.group({
      id: [[Validators.required]],
      stateName: [],
      districtName: [],
      userName: [],
      giftName: [[Validators.required]],
      productId: [],
      opening: [],
      receipt: [],
      balance: [],
      issueDate: [],
      resultList: new FormArray([])
    });
    const productFGs1 = this.productListForGift.map(product1 => {
      return this.fb.group({
        id: [product1.id],
        stateName: [product1.stateName],
        districtName: [product1.districtName],
        userName: [product1.userName],
        giftName: [product1.giftName],
        productId: [product1.productId],
        opening: [product1.opening],
        receipt: [product1.receipt],
        balance: [product1.balance],
        issueDate: [product1.issueDate],
      })
    });
    const productFormArray1: FormArray = this.fb.array(productFGs1);
    this.gift.setControl('resultList1', productFormArray1);
  }

  reportType(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    this.editSampleAndGift.patchValue({ reportType: val });
    if (val === 'Employeewise') {
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = true;
        this.districtFilterForMulti = false;
        this.designationFilter = false;
        this.designationFilterForUserwiseOnly = true;
      }
    } else if (val === 'Statewise') {
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.districtFilterForMulti = false;
        this.designationFilter = true;
        this.designationFilterForUserwiseOnly = false;
      }
    } else if (val === 'Headquarterwise') {
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.districtFilterForMulti = true;
        this.designationFilter = true;
        this.designationFilterForUserwiseOnly = false;
      }
    }
    this.editSampleAndGift.patchValue({ reportType: val })
  }

  setEmployeeDetail(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    this.editSampleAndGift.patchValue({ userInfo: val })
  }



  moduleSubmitType(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    this.editSampleAndGift.patchValue({ moduleTypeName: val })
  }

  getStateValue(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    let state = [];
    state.push(val);
    this.editSampleAndGift.patchValue({ stateInfo: state });
    this.designationData = [];
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
    if (this.editSampleAndGift.value.reportType == 'Headquarterwise') {
      this.callDistrictComponentAgainForMulti.getDistricts(this.currentUser.companyId, val, true);
    } else if (this.editSampleAndGift.value.reportType == 'Statewise' || this.editSampleAndGift.value.reportType == 'Employeewise') {
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, val, this.editSampleAndGift.value.districtInfo, this.editSampleAndGift.value.reportType).subscribe(res => {
        this.designationData = res;
      });
    }
  }

  getEmployeeValue(val) {
    this.isShowIssueSample = false;
    this.isShowIssueGift = false;
    let district = [];
    if (this.editSampleAndGift.value.reportType == 'Employeewise') {
      this.editSampleAndGift.patchValue({ designation: val });
      this.callEmployeeComponent.getMgrList(this.currentUser.companyId, this.editSampleAndGift.value.stateInfo, val, [true]);
    } else if (this.editSampleAndGift.value.reportType == 'Headquarterwise') {
      for (let i = 0; i < val.length; i++) {
        district.push(val[i]);
      }
      this.editSampleAndGift.patchValue({ districtInfo: district });
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, this.editSampleAndGift.value.stateInfo, this.editSampleAndGift.value.districtInfo, this.editSampleAndGift.value.reportType).subscribe(res => {
        this.designationData = res;
      });
    }
  }
  dataSource = [];
  viewSampleAndGiftDetail() {
    this.isToggled= false;
    if (this.editSampleAndGift.value.moduleTypeName == 'sample') {
      this._samplesService.getSampleGiftIssueDetailForEdit(this.currentUser.companyId, this.editSampleAndGift.value, this.currentUser.userInfo[0].rL).subscribe(getIssueDetail => {
        if (getIssueDetail.length > 0) {
          this.sampleIssueDetailArr(getIssueDetail);
          this.isShowIssueSample = true;
          this.isShowIssueGift = false;
          this._changeDetectorRef.detectChanges();
        } else {
          this._changeDetectorRef.detectChanges();
          this._toastr.success("Please select another details", "No Samples Found for selected Details !!");
        }
      }, err => {
      });
    } else if (this.editSampleAndGift.value.moduleTypeName == 'gift') {
      this._samplesService.getSampleGiftIssueDetailForEdit(this.currentUser.companyId, this.editSampleAndGift.value, this.currentUser.userInfo[0].rL).subscribe(getGift => {
        if (getGift.length > 0) {
          this.giftDetailArr(getGift);
          this.isShowIssueSample = false;
          this.isShowIssueGift = true;
          this._changeDetectorRef.detectChanges();
        } else {
          this._changeDetectorRef.detectChanges();
          this._toastr.success("Please select another details", "No Gifts Found for selected Details !!");
        }
      }, err => {
      });
    }
    this.editSampleAndGift.patchValue({ stateInfo: null })
    this.editSampleAndGift.patchValue({ designation: null })
    this.editSampleAndGift.patchValue({ userInfo: null })
    this.callStateComponent.setBlank();
    this.callEmployeeComponent.setBlank();

  }
  modifySample() {
    this._samplesService.modifySampleIssueDetail(this.editSampleAndGift.value, this.sample.value.resultList, this.currentUser.companyId)
      .subscribe(sample => {
        if (sample.length > 0) {
          this._toastr.success("Sample Modify Successfully !!", "Sample Modify successfully for selected details!!");
          this._changeDetectorRef.detectChanges();
        }
        this.isShowIssueSample = false;
        this.isShowIssueGift = false;
        this._changeDetectorRef.detectChanges();
      }, err => {
      })
  }
  modifyGift() {
    this._samplesService.modifySampleIssueDetail(this.editSampleAndGift.value, this.gift.value.resultList1, this.currentUser.companyId)
      .subscribe(gift => {
        if (gift.length > 0) {
          this._toastr.success("Gift Modify Successfully !!", "Gift Modify successfully for selected details!!");
          this._changeDetectorRef.detectChanges();
        }
        this.isShowIssueSample = false;
        this.isShowIssueGift = false;
        this._changeDetectorRef.detectChanges();
      }, err => {
      })
  }

  
  exportToExcel() {   
    // download the file using old school javascript method
    setTimeout (() => {    
    this.exportAsService.save(this.exportAsConfig, 'Sample And Gift Report').subscribe(() => {
      // save started
    });
  },300);
 
  }

  exportToExcel1() {   
    // download the file using old school javascript method
    setTimeout (() => {    
    this.exportAsService.save(this.exportAsConfig1, 'Sample And Gift Report').subscribe(() => {
      // save started
    });
  },300);
 
  }
}
