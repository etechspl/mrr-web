import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleGiftEditComponent } from './sample-gift-edit.component';

describe('SampleGiftEditComponent', () => {
  let component: SampleGiftEditComponent;
  let fixture: ComponentFixture<SampleGiftEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleGiftEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleGiftEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
