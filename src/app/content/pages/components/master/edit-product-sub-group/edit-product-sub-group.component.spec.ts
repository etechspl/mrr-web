import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProductSubGroupComponent } from './edit-product-sub-group.component';

describe('EditProductSubGroupComponent', () => {
  let component: EditProductSubGroupComponent;
  let fixture: ComponentFixture<EditProductSubGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProductSubGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProductSubGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
