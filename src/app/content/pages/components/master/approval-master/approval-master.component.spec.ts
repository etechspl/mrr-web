import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalMasterComponent } from './approval-master.component';

describe('ApprovalMasterComponent', () => {
  let component: ApprovalMasterComponent;
  let fixture: ComponentFixture<ApprovalMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
