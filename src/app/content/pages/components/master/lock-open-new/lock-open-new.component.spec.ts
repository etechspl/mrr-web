import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LockOpenNewComponent } from './lock-open-new.component';

describe('LockOpenNewComponent', () => {
  let component: LockOpenNewComponent;
  let fixture: ComponentFixture<LockOpenNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LockOpenNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockOpenNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
