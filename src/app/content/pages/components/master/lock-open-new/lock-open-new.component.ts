
import { Component, OnInit, ViewChild, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material/core';

import { SelectionModel, DataSource } from '@angular/cdk/collections';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';

import { ToastrService } from 'ngx-toastr';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DistrictComponent } from '../../filters/district/district.component';

import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';



const moment = _rollupMoment || _moment;
declare var $;

export const MY_FORMATS = {
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
  },
  display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'short' },
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};


@Component({
  selector: 'm-lock-open-new',
  templateUrl: './lock-open-new.component.html',
  styleUrls: ['./lock-open-new.component.scss']
})
export class LockOpenNewComponent implements OnInit {
  showFilters = false;
  showHq = false;
  showEmp = false;
  showState = false;
  showDes = false;
  showDiv= false;
  showValidInput = false;
  showRedioFilter = false;
  showDate = false;
  showDelButton = false;
  showAddIcon = false;
  LockPeriod=false;
  showReport = false;
  aa = false;
  bb= false;
 showProcessing = false;
 public showDivisionFilter: boolean = false;
 
  displayedColumns =  ['select', 'Name', 'district', 'designation', 'state','lastDCRDate'];
  dataSource;
  selection = new SelectionModel(true, []);
  showReleaseButton = false;
 //dataSource = ELEMENT_DATA;


  LockOpenForm: FormGroup;
  constructor(private fb: FormBuilder, 
             private userDetailsService: UserDetailsService,
             private _changeDetectorRef: ChangeDetectorRef,
             private toastrService: ToastrService,
             private _dcrReportService: DcrReportsService
     ) {
    this.LockOpenForm = this.fb.group({
      stateInfo: new FormControl("", Validators.required),
      employeeId: new FormControl("", Validators.required),
      districtId: new FormControl("", Validators.required),
      designation: new FormControl("", Validators.required),
      lockType: new FormControl("", Validators.required),
      lockDate: new FormControl("", Validators.required),
      lockingPeriod: new FormControl("", Validators.required),
      LockFilterType: new FormControl("", Validators.required),
      divisionId: new FormControl("", Validators.required),
      lockOpenDateData: this.fb.array([this.fb.group({
      lockOpenInput: new FormControl("", Validators.required),
      })]),
      
    });

  }

  currentUser = JSON.parse(sessionStorage.currentUser);

  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      this.LockOpenForm.setControl('division', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  @Output() valueChange = new EventEmitter();

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
   data = [];
  

  lockOpenCategory(val) {
    this.LockOpenForm.patchValue({ lockType: val });

    if (val === 'day') {
      this.showRedioFilter = true;
      this.showFilters = false;

    } else if (val === 'date') {
      this.showRedioFilter = true;
      this.showFilters = false;
      this.showValidInput = false;

    }

  }

  lockOpenType(val) {
    this.LockOpenForm.reset();
    this.aa = true;
    this.bb = false;
    this.LockOpenForm.patchValue({ LockFilterType: val });
    if (val === 'state') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        this.showDate = true;
        this.showDelButton = true;;
        this.showAddIcon = true;
      } else {
        this.showValidInput = true;
        this.showAddIcon = false;
        this.showDate = false;
      }
      this.showFilters = true;
      this.showEmp = false;
      this.showDiv=false;
      this.showHq = false;
      this.showDes = false;
      this.showState = true;
    }else if (val === 'Headquarter') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        this.showDate = true;
        this.showDelButton = true;
        this.showAddIcon = true;

      }else {
        this.showValidInput = true;
        this.showAddIcon = false;
        this.showDate = false;
      }
      this.showFilters = true;
      this.showEmp = false;
      this.showDiv=false;
      this.showHq = true;
      this.showDes = true;
      this.showState = true;
     } else if (val === 'designation') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        this.showDate = true;
        this.showDelButton = true;;
        this.showAddIcon = true;
     }
      else {
        this.showValidInput = true;
        this.showAddIcon = false;
        this.showDate = false;
      }
      this.showFilters = true;
      this.showEmp = false;
      this.showDiv=false;
      this.showHq = false;
      this.showDes = true;
      this.showState = false;
     } else if (val === 'employee') {
      if (this.LockOpenForm.value.lockType == "date") {
        this.showValidInput = false;
        this.showDate = true;
        this.showDelButton = true;;
        this.showAddIcon = true;
     } else {
        this.showValidInput = true;
        this.showAddIcon = false;
        this.showDate = false;
    }
      this.showFilters = true;
      this.showEmp = true;
      this.showDiv=false;
      this.showHq = false;
      this.showDes = true;
      this.showState = false;
    } else if (val === 'delayedDcr') {
      this.aa = false;
       this.bb = true;
      if (this.LockOpenForm.value.lockType == "date") {
          this.showValidInput = true;
    } else {
        this.showValidInput = false;
      }
      this.showFilters = true;
      // this.showDiv=true;
      this.showState = true;
      this.showEmp = false;
      this.showHq = false;
     this.showDes = false;
   }
  
  }

  getStateValue(val) {
    this.LockOpenForm.patchValue({ stateInfo: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,[true]);
  }
  getDistrictValue(val) {
    this.LockOpenForm.patchValue({ districtId: val });
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, val);
  }
  getDesignation(val) {
    this.LockOpenForm.patchValue({ designation: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
  }
  getEmployeeValue(val) {
    console.log("employee--",val)
    this.LockOpenForm.patchValue({ employeeId: val });
  }

  
  getDivisionValue(val) {
     this.LockOpenForm.patchValue({ divisionId: val });
   
  //  let passingObj = {
  //     companyId: this.currentUser.companyId,
  //     userId: this.currentUser.id,
  //     userLevel: this.currentUser.userInfo[0].designationLevel,
  //     division: [val]
  //   }    
  //   console.log("passing",passingObj)
  //  this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
  }

  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    this.showReleaseButton = true;
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.showReleaseButton = true;
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(element => {
        this.selection.select(element)
      });
  }

  getDelayedValue(){
    this.showReport = false;
    this.showProcessing=true;
   
    this._dcrReportService.insertAllLockedDcr(this.currentUser.company.id,this.LockOpenForm.value.divisionId, this.LockOpenForm.value.stateInfo, this.LockOpenForm.value.districtId, this.LockOpenForm.value.employeeId, this.LockOpenForm.value.designation, this.LockOpenForm.value.lockType, "state", this.LockOpenForm.value.lockingPeriod).subscribe(res => {
      if(res == true){
         this._dcrReportService.getAllLockedDcr(this.currentUser.company.id,this.LockOpenForm.value.divisionId, this.LockOpenForm.value.stateInfo, this.LockOpenForm.value.districtId, this.LockOpenForm.value.employeeId, this.LockOpenForm.value.designation, this.LockOpenForm.value.lockType, "state", this.LockOpenForm.value.lockingPeriod)
         .subscribe(res => {
         this.dataSource=res
         this._changeDetectorRef.detectChanges();
         if (this.dataSource.length > 0) {


          this.showProcessing = false;
          this.showReport = true;
          this._changeDetectorRef.detectChanges();
        }
           this._changeDetectorRef.detectChanges();
       
         })  
      }



   })

 }


  releaseButton(obj) {
    this.showReleaseButton = true;
    //this.showProcessing=false;
    let userId=[];
    for(var i=0;i<obj.length;i++)
    {
      userId.push(obj[i]._id.submitBy)
    }
    var param={
      userId:userId,
      companyId:this.currentUser.companyId
    }
   
    this._dcrReportService.releaseUserOpeningLock(param).subscribe(res => {
      console.log("res--",res)

    })
     
   
   
  }





  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  //---submit lock open details-------------

  SubmitLockOpenDetails() { 
    console.log("hii")  
    this.userDetailsService.getLockOpenDetails(this.currentUser.company.id, this.LockOpenForm.value.stateInfo, this.LockOpenForm.value.districtId, this.LockOpenForm.value.employeeId, this.LockOpenForm.value.designation, this.LockOpenForm.value.lockType, this.LockOpenForm.value.LockFilterType, this.LockOpenForm.value.lockingPeriod).subscribe(LockOpenResponse => {
      this.toastrService.success('Lock Days has been updated!!');
    }, err => {
      this.toastrService.error('Error in opening lock days!!');
    });
  }

  addMoreLockOpenDate() {
    const lockOpen = this.LockOpenForm.controls.lockOpenDateData as FormArray;
    lockOpen.push(this.fb.group({
      lockOpenInput: '',
    }));
  }
  //-----------------------
  //---- add lock date --------------


  
  
  //-----------------------------------

  yourFunction(val) {
    let day = val.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    let month = val.getMonth() + 1;
    if (month < 10) {
      month = "0" + month
    }
    let year = val.getFullYear();
    var Date = year + "-" + month + "-" + day;
    this.LockOpenForm.patchValue({ lockDate: Date });
  }

  format(date: Date, displayFormat: Object): string {
    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return this._to2digit(day) + '-' + this._to2digit(month) + '-' + year;
    } else {
      return date.toDateString();
    }
  }
  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }
}

