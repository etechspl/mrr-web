import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { GiftService } from '../../../../../core/services/Gift/gift.service';
import { MatSnackBar, MatDialog } from '@angular/material';

@Component({
  selector: 'm-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.scss']
})
export class HolidayComponent implements OnInit {
  showGiftDetails = false;
 // displayedColumns = ['giftName', 'giftCode', 'giftCost','status','action','action1'];
  dataSource;
 currentUser = JSON.parse(sessionStorage.currentUser);

 holidayForm=new FormGroup({
  });

  constructor(
    private toastrService: ToastrService,
    private _giftService :GiftService,
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    
    ) {

  this.holidayForm = this.fb.group({
    
    id : new FormControl(), 
    holidayDate:new FormControl(null,Validators.required),
    holiday:new FormControl(null,Validators.required),
    
  })
 }

  ngOnInit() {} 
   //---------------Create new gift------------------------------
   submitHoliday(){
    this._giftService.createGift(this.holidayForm.value,this.currentUser.company.id,this.currentUser.id).subscribe(giftResponse => {
      this.toastrService.success('Gift Created Successfully...');
      // this.giftForm.setErrors(null);
      this.holidayForm.reset();
      
      Object.keys(this.holidayForm.controls).forEach((name)=>{
        let con = this.holidayForm.controls[name];
        con.setErrors(null);
      })
   
     }, err => { 
       console.log(err);
       this.toastrService.error('Gift Already Created..');
     });

     
 }


}

