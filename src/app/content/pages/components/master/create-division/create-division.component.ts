import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
declare var $;
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DivisionService } from '../../../../../core/services/Division/division.service';
import Swal from 'sweetalert2'
import { UserDetailsService } from '../../../../../core/services/user-details.service';
///import { d } from '@angular/core/src/render3';
import * as _moment from 'moment';
import { URLService } from '../../../../../core/services/URL/url.service';
import { MatSlideToggleChange } from '@angular/material';

@Component({
  selector: 'm-create-division',
  templateUrl: './create-division.component.html',
  styleUrls: ['./create-division.component.scss']
})
export class CreateDivisionComponent implements OnInit {
  @ViewChild("divisionDataTable") dataTable;
  divisionDTOptions: any;
  divisionDataTable: any;
  isShowDivisionDetails = false;
  dataSource;
  currentUser = JSON.parse(sessionStorage.currentUser);

  divisionForm = new FormGroup({
  });
  constructor(
    private divisionService: DivisionService,
    private userDetailsService: UserDetailsService,
    private toastrService: ToastrService,
    private changeDetectedRef: ChangeDetectorRef,
	private fb: FormBuilder,
	private _urlService: URLService
  ) {
    this.divisionForm = this.fb.group({
      divisionName: new FormControl(null, Validators.required)
    })
  }

  ngOnInit() {
    this.getDivisions();
  }
  isToggled = true; 
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  // .......................Get Divisions in DATATABLE..................

  getDivisions() {
    let object = {
      "companyId": this.currentUser.companyId,
      "designationLevel": this.currentUser.designationLevel,
    }
    this.divisionService.getDivisionWithMappedState(object).subscribe(res => {
      res.map((item, index) => {
        item['index'] = index;
	  });
	  const PrintTableFunction = this.PrintTableFunction.bind(this);
	   this.divisionDTOptions = {
        "pagingType": 'full_numbers',
        "paging": true,
        "ordering": true,
        "info": true,
        "scrollY": 300,
        "scrollX": true,
        "fixedColumns": true,
        "columnDefs": [ 
          { "width": "45px", "targets": 0 },
          { "width": "230px", "targets": 1 },
          { "width": "300px", "targets": 2 },
          { "width": "190px", "targets": 3 }],
        destroy: true,
        data: res,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible'
            }
          },
                 //********************* UMESH 26-03-2020 ************************ */
  {
    extend: 'print',
    title: function () {
    return 'State List';
    },
    exportOptions: {
    columns: ':visible'
    },
    action: function (e, dt, node, config) {
    PrintTableFunction(res);
    }
    }
        ],
        columns: [
          {
            title: 'S. No.',
            data: 'index',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data + 1;
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Division Name',
            data: 'divisionName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Mapped States',
            data: 'states',
            render: function (data) {
              if (Array.isArray(data)) {
                let str = data.reduce((a, c) => `${a}${['', ','][+!!a]} ${c.stateName || ''}`, '').replace(/\s\s+/g, ' ').trim();
                return str || '---';
              }
              return '---'

            },
            defaultContent: '---'
          },
          {
            title: 'Status',
            data: 'status',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                if (data == true) {
                  return "Active"
                } else if (data == false) {
                  return "Inactive"
                }
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Change Status',

            data: 'status',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                let status = "Deactivate";
                if (data == true) {
                  return "<button id='inActiveButtonId' class='btn btn-warning btn-sm'><i class='la la-check'></i>&nbsp;&nbsp;" + status + "</button>"
                } else if (data == false) {
                  status = "Activate";
                  return "<button id='activeButotonId' class='btn btn-warning btn-sm'><i class='la la-check'></i>&nbsp;&nbsp;&nbsp;&nbsp" + status + "</button>"
                }
              }
            }
          }
        ],
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          $('td', row).unbind('click');
          $('td button', row).bind('click', (event) => {

            let rowObject = JSON.parse(JSON.stringify(data));

            if (event.target.id == "inActiveButtonId") {
              //>>>>>>>>>>>>>>>>>> Deactivate || Make it FALSE <<<<<<<<<<<<<<<<<<
              self.updatedUserStatus(rowObject, false);

            } else if (event.target.id == "activeButotonId") {
              //>>>>>>>>>>>>>>>>>> Activate || Make it TRUE <<<<<<<<<<<<<<<<<<
              self.updatedUserStatus(rowObject, true);
            }
          });

        }
      }
      this.changeDetectedRef.detectChanges();
      this.divisionDataTable = $(this.dataTable.nativeElement);
      this.divisionDataTable.DataTable(this.divisionDTOptions);
    })
  }


  // .......................Update User Status..................
  updatedUserStatus(obj, changeTo) {
    let msg = "The User Will Be Activated.";
    let buttonMsg = "Yes, Activate it !!!";
    if (changeTo == false) {
      msg = "The User Will Be Deactivated.";
      buttonMsg = "Yes, Deactivate it !!!";
    }
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: msg,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: buttonMsg,
      cancelButtonText: 'No, cancel!',
      reverseButtons: true,
      allowOutsideClick: false
    }).then(res => {

      let where = {
        "divisionId": obj.divisionId,
        "status": true
      }
      this.userDetailsService.getUsersForTrueStatus({ where }).subscribe(d => {
        // console.log("REsponse from USER DETAILS....>...", d.length);
        if (d.length > 0 && res.value === true) {
          this.toastrService.error('This division cannot be deactivated because it has active Users');
        }
        if (res.value === true && d.length === 0) {
          let where = {
            "id": obj.divisionId
          }
          let data = {
            "status": !obj.status,
          }
          this.divisionService.updateStatus(where, data).subscribe(res => {
            this.toastrService.success('Division Status Changed Successfully.');
            this.getDivisions();
          });
        }
      })
    })
  }

  // .......................Create new division..................
  createDivision() {
    let obj = {
      companyId: this.currentUser.companyId,
      divisionName: this.divisionForm.value.divisionName,
      status: true,
    }
    console.log("Form value-----------", this.divisionForm.value);
    this.divisionService.createDivision(obj).subscribe(response => {
      if (!response.error) {
        this.getDivisions();
        this.toastrService.success('Division Created Successfully.');
        this.divisionForm.reset();
        this.changeDetectedRef.detectChanges();
      } else if (response.error) {
        this.toastrService.error('Something went wrong.');
      }
    })
  }



  /*****************************************UMESH 26-03-2020 *************/


PrintTableFunction(data?: any): void {
	// const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
	const currentDateAndTime = _moment(new Date()).format('DD/MM/YYYY, dddd , HH:mm A');
	const department = this.currentUser.section;
	const designation = this.currentUser.designation;
	const companyName = this.currentUser.companyName;

	const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

	const tableRows: any = [];
	data.forEach(element => {
		let statenames = ``;
		element.states.forEach(i => {
            statenames += i.stateName+`, `;
        });
	tableRows.push(`<tr>
	<td class="tg-oiyu">${element.index}</td>
	<td class="tg-oiyu">${element.divisionName}</td>
	<td class="tg-oiyu">${statenames}</td>
	</tr>`)
	});

   let showHeaderAndTable: boolean = false;
	// this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
	let printContents, popupWin;
	popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
	popupWin.document.open();
	popupWin.document.write(`
	<html>
	<head>
	<style>
	img {
	float: left;
	}

	.flex-container {
	display: flex;
	justify-content: space-between;
	}

	.tg {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg .tg-lboi {
	border-color: inherit;
	text-align: left;
	vertical-align: middle;

	}

	.tg .tg-u6fn {
	font-weight: bold;
	font-size: 18px;
	background-color: #efefef;
	color:black;
	border-color: inherit;
	text-align: left;
	vertical-align: top;
	padding-top: 0px;
	padding-bottom: 0px;
	}

	.tg .tg-yz93 {
	border-color: inherit;
	text-align: right;
	vertical-align: middle
	}
	.tb2 {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tb2 td {
	font-family: Arial, sans-serif;
	font-size: 12px;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 th {
	font-family: Arial, sans-serif;
	font-size: 12px;
	font-weight: normal;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 .tg-oiyu {
	font-size: 10px;
	text-align: left;
	vertical-align: middle
	}
	.tg-bckGnd {
	font-weight: bold;
	background-color: #efefef;
	}

	.tb2 .tg-3ppa {
	font-size: 10px;
	background-color: #efefef;
	color:black;
	text-align: left;
	vertical-align: middle;
	padding-top: 2px;
	padding-bottom: 2px;
	}

	.footer {
	position: sticky;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: #efefef;
	color: rgb(2, 2, 2);
	text-align: right
	}

	button {
	background-color: #008CBA;
	border: none;
	color: white;
	margin: 5px 0px;
	padding: 2px 10px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
	border-radius: 12px;
	cursor: pointer;
	outline: none;
	}
	@media print {
	@page { size: A4; margin: 5mm; }
	.footer {
	position: relative;
	}
	tr {
	page-break-inside: avoid;
	}
	button{
	display : none;
	}
	}
	</style>
	</head>
	<body onload="window.print();window.close()" style="padding-bottom:30px">
	<div class="flex-container">
	<div style="width: 40%; ">
	<img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	<h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

	</div>

	<div style="text-align: right;">
	<h2 style="margin-top: 0px; margin-bottom: 5px">Division Details</h2>
	<div>
	<button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	<button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	</div>
	</div>
	</div>

	<div>
	${/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/''}
	<table class="tb2">
	<tr>
	<th class="tg-3ppa"><span style="font-weight:600">'S. No.</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Division Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Mapped States</span></th>

	</tr>
	${tableRows.join('')}
	</table>
	</div>
	<div class="footer flex-container">
	<p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>

	<p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
	</div>
	</body>
	</html>
	`);

	// popupWin.document.close();

	}
}
