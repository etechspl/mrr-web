import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivityService } from '../../../../../core/services/Activity/activity.service';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
import * as moment from 'moment';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';

@Component({
  selector: 'm-modify-tour-program',
  templateUrl: './modify-tour-program.component.html',
  styleUrls: ['./modify-tour-program.component.scss']
})
export class ModifyTourProgramComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModifyTourProgramComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastr: ToastrService,
    private activityService: ActivityService,
    private _hierarchyService: HierarchyService,
    private tpService: TourProgramService,
    private _userAreaMappingService: UserAreaMappingService,
    private changeDetectorRef: ChangeDetectorRef,
    private _providerservice: ProviderService) { }

  
  
  

  doctors = [];
  chemists = []
  datesSelected;
  activities = [];
  jointWorkedWith = [];
  mrTPSubmitted;
  currentUser = JSON.parse(sessionStorage.currentUser);
  COMPANY_ID = this.currentUser.companyId;


  count: number;
  form = new FormGroup({});
  selectionMode = "single";
  model = {

  };

  minDateValue;
  maxDateValue;

  ngOnInit() {
    console.log(this.data);
    this.tpService.getTPExistOfDay({
      where: {
        id: this.data._id
      }
    }).subscribe(res => {

      //let date = moment.utc(res[0].date);
      this.model = {
        activity: res[0].activity + "#-#" + res[0].TPStatus,
        isDayPlan: res[0].isDayPlan,
        remarks: res[0].remarks
      }

      //this.datesSelected = "Sat Sep 07 2019 00:00:00 GMT+0530 (India Standard Time)"
      this.datesSelected = moment(res[0].date).format("YYYY-MM-DD");
      this.minDateValue = new Date(res[0].date);
      this.maxDateValue = new Date(res[0].date);

      if (res[0].area.length > 0) {
        // ADD AREA IN MODEL OBJECT
        this.model['area1'] = [];
        for (const area of res[0].area) {
          let obj = {
            from: area.fromId + "#-#" + area.from,
            to: area.toId + "#-#" + area.to
          }
          this.model['area1'].push(obj);
        }
      } else {
        console.log("coming in else part--");
        this.model['area1'] = [];
      }

      // ADD DOCTOR IN MODEL OBJECT
      this.model["plannedDoctors"] = [];
      if (res[0].hasOwnProperty["plannedDoctors"] === true && res[0].plannedDoctors.length > 0) {
        for (const doctorObj of res[0].plannedDoctors) {
          this.model["plannedDoctors"].push(doctorObj.Name + "#-#" + doctorObj.id + "#-#" + doctorObj.areaId);
        }
      }

      this.model["plannedChemists"] = [];
      if (res[0].hasOwnProperty["plannedChemists"] === true && res[0].plannedChemists.length > 0) {
        for (const chemistObj of res[0].plannedChemists) {
          this.model["plannedChemists"].push(chemistObj.Name + "#-#" + chemistObj.id + "#-#" + chemistObj.areaId);
        }
      }
      //ADD CHEMIST IN MODEL OBJECT
    })



    this.changeDetectorRef.detectChanges();

    //getting activity
    this.activityService.getActivities(this.currentUser.companyId).subscribe(res => {
      for (const activity of res[0].activitiesForWeb) {
        let obj = {
          Key: activity.name + "#-#" + activity.id,
          name: activity.name
        }
        this.activities.push(obj);

      }
      this.changeDetectorRef.detectChanges();
    })
  }


  fields: FormlyFieldConfig[] = [
    {
      key: 'choosePlan',
      type: 'radio',
      templateOptions: {
        label: 'Choose Plan Type',
        placeholder: 'Placeholder',
        required: true,
        options: [
          { value: "individual", label: 'Make Individual Plan' },
          { value: "mrPlan", label: 'Choose MR Plane' }
        ],
        change: (field, $event) => {
          this.count++;
          if (this.count > 1) {
            this.datesSelected = undefined;
          }
          if ($event.value === "individual") {
            this.mrTPSubmitted = false;
            this.selectionMode = "multiple";
            this.toastr.info("Multiple dates can be selected for making individual plan", "Multiple Date selection");
            //form is restting
            delete this.model["jointWorkedWith"];
            delete this.model["activity"];
            this.model["area1"] = [{}];
            this.model["isDayPlan"] = false;
            this.model["plannedDoctors"] = [];
            this.model["plannedChemists"] = [];
            this.model["remarks"] = "";

          } else {
            if (this.datesSelected === undefined || this.datesSelected === null) {
              this.toastr.error("Select date to copy TP of employee", "Date field required");
              //form is restting
              delete this.model["activity"];
              delete this.model["jointWorkedWith"];
              this.model["area1"] = [{}];
              this.model["isDayPlan"] = false;
              this.model["plannedDoctors"] = [];
              this.model["plannedChemists"] = [];
              this.model["remarks"] = "";

            }
            this.selectionMode = "single";
          }
          this.changeDetectorRef.detectChanges();

        }
      },

      hideExpression: () => {
        if (this.currentUser.userInfo[0].designationLevel > 1) {
          return false;
        } else {
          return true;
        }
      },
    }, {
      key: "jointWorkedWith",
      type: "select",
      templateOptions: {
        required: true,
        placeholder: "Select Joint Workied With",
        options: this.jointWorkedWith,
        valueProp: 'userId',
        labelProp: 'name',
        change: (field, $event) => {
          this.getTPExistOfDay();
        }
      },

      hideExpression: () => {
        if (this.currentUser.userInfo[0].designationLevel > 1) {
          if (this.model["choosePlan"] === "mrPlan") {
            return false;
          } else {
            return true;
          }
        } else {
          return true;
        }

      },
      lifecycle: {
        onInit: (form, field) => {
          let obj = {
            supervisorId: this.currentUser.id,
            companyId: this.currentUser.companyId,
            status: true,
            type: "lowerWithUpper"
          }
          this._hierarchyService.getManagerHierarchy(obj).subscribe(res => {
            this.jointWorkedWith = JSON.parse(JSON.stringify(res));
            field.templateOptions.options = this.jointWorkedWith;

            this.changeDetectorRef.detectChanges();
          })
        }
      }
    }, {
      key: 'activity',
      type: 'select',
      templateOptions: {
        label: 'Activity',
        placeholder: 'Activity',
        options: [],
        valueProp: 'Key',
        labelProp: 'name',
        required: true,
        change: (field, $event) => {
          if (this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {

          } else {
            this.model["area1"] = [{}];
            this.model["isDayPlan"] = false;
            this.model["plannedDoctors"] = [];
            this.model["plannedChemists"] = [];
          }
        }

      },
      hideExpression: () => {
        if (this.currentUser.userInfo[0].designationLevel === 1) {
          return false;
        } else {
          if (this.model["choosePlan"] === "mrPlan") {
            if (this.mrTPSubmitted === true) {
              return false;
            } else {
              return true;
            }
          } else if (this.model["choosePlan"] === "individual") {
            return false;
          } else if (this.model["choosePlan"] === undefined) {
            return true;
          }
        }
      },
      lifecycle: {
        onInit: (form, field) => {
          this.activityService.getActivities(this.currentUser.companyId).subscribe(res => {
            for (const activity of res[0].activitiesForWeb) {
              let obj = {
                Key: activity.name + "#-#" + activity.id,
                name: activity.name
              }
              this.activities.push(obj);
              field.templateOptions.options = this.activities;
            }

          })

        }
      }
    }, {
      className: 'col-sm-7',
      key: 'area1',
      type: 'repeats',
      hideExpression: () => {
        if (this.model["choosePlan"] === "mrPlan") {
          if (this.mrTPSubmitted === true && this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
            return false;
          } else {
            return true;
          }
        } else if (this.model["choosePan"] === "individual") {
          if (this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
            return false;
          } else {
            return true;
          }
        } else {
          if (this.model.hasOwnProperty("activity") === true && this.model["activity"] !== undefined) {
            if (parseInt(this.model["activity"].split("#-#")[1]) == 1)
              return false;
            else
              return true;
          } else {
            return true;
          }
        }
      },
      templateOptions: {
        label: this.currentUser.company.lables.areaLabel,
        btnText: 'Add ' + this.currentUser.company.lables.areaLabel
      },
      fieldArray: {
        fieldGroupClassName: 'row',
        className: 'row',
        fieldGroup: [
          {
            className: 'col-sm-5',
            type: 'select',
            key: 'from',
            templateOptions: {
              label: 'From ' + this.currentUser.company.lables.areaLabel,
              options: [],
            },
            lifecycle: {
              onInit: (form, field) => {

                this._userAreaMappingService.getUserMappedBlocksForMobile(this.currentUser.companyId, this.currentUser.id).subscribe(res => {
                  var a = [];
                  //a.push(res)
                  let areaArr = [];
                  var values = [];
                  JSON.parse(JSON.stringify(res)).filter(area => {
                    if (area.status === true && area.appStatus === "approved") {
                      areaArr.push(area);
                    }
                  })
                  for (var n = 0; n < areaArr.length; n++) {
                    values.push({ 'label': areaArr[n].areaName, 'value': areaArr[n].areaId + "#-#" + areaArr[n].areaName });
                  }
                  field.templateOptions.options = values;
                  this.changeDetectorRef.detectChanges();
                }, err => {
                  console.log(err);
                })
              },
            },
          },
          {
            type: 'select',
            key: 'to',
            className: 'col-sm-5',
            templateOptions: {
              label: 'To ' + this.currentUser.company.lables.areaLabel,
              options: [],
            },
            lifecycle: {
              onInit: (form, field) => {
                this._userAreaMappingService.getUserMappedBlocksForMobile(this.currentUser.companyId, this.currentUser.id).subscribe(res => {
                  let areaArr = [];
                  let values = [];
                  JSON.parse(JSON.stringify(res)).filter(area => {
                    if (area.status === true && area.appStatus === "approved") {
                      areaArr.push(area);
                    }
                  })
                  for (var n = 0; n < areaArr.length; n++) {
                    values.push({ 'label': areaArr[n].areaName, 'value': areaArr[n].areaId + "#-#" + areaArr[n].areaName });
                  }
                  field.templateOptions.options = values;
                  this.changeDetectorRef.detectChanges();
                })
              }
            },
          },
        ],
      },
    }, {
      fieldGroupClassName: "row",
      fieldGroup: [{
        key: 'isDayPlan',
        className: 'col-md-4',
        type: 'checkbox',
        templateOptions: {
          label: 'Add Day Plan',
          change: (field, $event) => {
            if ($event.value === true) { } else {
              this.model["plannedDoctors"] = [];
              this.model["plannedChemists"] = [];
            }
          }
        },
        hideExpression: () => {
          //in company master if isDayPlan is false then isDayPlan check box will not display
          if (this.currentUser.company.validation.isDayPlan === true && this.model.hasOwnProperty("activity") === true && this.model["activity"] !== undefined) {
            if (parseInt(this.model["activity"].split("#-#")[1]) === 1) {

              if (this.model["area1"].length > 0) {
                if (Object.keys(this.model["area1"][0]).length != 0) {
                  return false;
                }
              }
            }

          }
          return true;
        },

      }, {
        key: "plannedDoctors",
        type: "select",
        className: 'col-md-4',
        templateOptions: {
          label: 'Select Doctors',
          placeholder: 'Select Doctors',
          options: [],
          valueProp: 'id',
          labelProp: 'providerName',
          required: true,
          multiple: true
        },
        hideExpression: () => {
          if (this.model["area1"].length > 0) {
            if (this.model["isDayPlan"] === true) {
              return false;
            }
          }
          return true;
        },
        lifecycle: {
          onInit: (form, field) => {
            const updateOptions = (v) => {
              if (this.model["area1"].length === 0) {
                this.model["isDayPlan"] = false;
                this.changeDetectorRef.detectChanges();
              }
              if (this.model["isDayPlan"] === true && this.model["area1"].length > 0) {
                if (Object.keys(this.model["area1"][0]).length != 0) {
                  var areaIds = [];
                  for (var n = 0; n < this.model["area1"].length; n++) {
                    if (this.model["area1"][n] !== undefined && this.model["area1"][n].hasOwnProperty("from") === true && this.model["area1"][n].hasOwnProperty("to") === true) {
                      areaIds.push(this.model["area1"][n]["from"].split("#-#")[0]);
                      areaIds.push(this.model["area1"][n]["to"].split("#-#")[0]);
                    }
                  }
                  this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["RMP"]).subscribe(res => {
                    for (const provider of res) {
                      let obj = {
                        id: provider.providerName + "#-#" + provider.id + "#-#" + provider.providerInfo.blockId,
                        providerName: provider.providerName
                      }
                      this.doctors.push(obj);
                    }
                    field.templateOptions.options = this.doctors;
                    this.changeDetectorRef.detectChanges();
                  })
                }
              }
            }
            updateOptions(form.value);
            this.form.valueChanges.subscribe(v => updateOptions(v));
          }
        }
      }, {
        key: "plannedChemists",
        type: "select",
        className: 'col-md-4',
        templateOptions: {
          label: 'Select ' + this.currentUser.company.lables.vendorLabel,
          placeholder: 'Select ' + this.currentUser.company.lables.vendorLabel,
          options: [],
          valueProp: 'id',
          labelProp: 'providerName',
          multiple: true
        },
        hideExpression: () => {
          if (this.model["area1"].length > 0) {
            if (this.model["isDayPlan"] === true) {
              return false;
            }
          }
          return true;
        },
        lifecycle: {
          onInit: (form, field) => {
            const updateOptions = (v) => {

              if (this.model["isDayPlan"] === true && this.model["area1"].length > 0) {
                if (Object.keys(this.model["area1"][0]).length != 0) {
                  var areaIds = [];
                  for (var n = 0; n < this.model["area1"].length; n++) {
                    if (this.model["area1"][n] !== undefined && this.model["area1"][n].hasOwnProperty("from") === true && this.model["area1"][n].hasOwnProperty("to") === true) {
                      areaIds.push(this.model["area1"][n]["from"].split("#-#")[0]);
                      areaIds.push(this.model["area1"][n]["to"].split("#-#")[0]);
                    }
                  }
                  this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["Drug", "Stockist"]).subscribe(res => {
                    for (const provider of res) {
                      let obj = {
                        id: provider.providerName + "#-#" + provider.id + "#-#" + provider.providerInfo.blockId,
                        providerName: provider.providerName
                      }
                      this.chemists.push(obj);
                    }
                    field.templateOptions.options = this.chemists;
                    this.changeDetectorRef.detectChanges();
                  })
                }
              }
            }
            updateOptions(form.value);
            this.form.valueChanges.subscribe(v => updateOptions(v));
          }
        }
      }]
    }, {
      key: 'remarks',
      type: 'textarea',
      templateOptions: {
        label: 'Remarks',
        placeholder: 'Remarks',
        rows: 3
      },
      hideExpression: () => {
        if (this.model["choosePlan"] === "mrPlan") {
          if (this.mrTPSubmitted) {
            return false;
          } else {
            return true;
          }

        } else if (this.model["choosePan"] === "individual") {
          return false;
        } else if (this.model.hasOwnProperty("choosePlan") === false || this.model["choosePlan"] === undefined) {
          return false
        }
      }
    }];


  submit(){
    if(this.form.valid && this.validate()===true){
      let defaultDate = "1900-01-01";
      let tpArr = [];
      let tpDates = [];
      if (this.selectionMode === "single") {
        tpDates.push(this.datesSelected)
      } else {
        tpDates = this.datesSelected;
      }

      for (const dateObj of tpDates) {
        console.log("TP Dates is ",dateObj)
        let areaArr = [];
        let tpInsertObj = {};
        let doctors = [];
        let chemists = [];
  
        if (this.model.hasOwnProperty("activity") === true && parseInt(this.model["activity"].split("#-#")[1]) === 1) {
          for (const area of this.model["area1"]) {
            let obj = {
              from: area.from.split("#-#")[1],
              fromId: area.from.split("#-#")[0],
              to: area.to.split("#-#")[1],
              toId: area.to.split("#-#")[0]
            }
            areaArr.push(obj)
          }
  
          tpInsertObj["area"] = areaArr;
          tpInsertObj["approvedArea"] = areaArr;
          if (this.model["isDayPlan"] === true) {
            tpInsertObj["plannedArea"] = areaArr;
          } else {
            tpInsertObj["plannedArea"] = []
          }
  
          for (const doctorObj of this.model["plannedDoctors"]) {
            doctors.push({
              "Name": doctorObj.split("#-#")[0],
              "id": doctorObj.split("#-#")[1],
              "areaId": doctorObj.split("#-#")[2]
            })
          }
            
          for (const chemistObj of this.model["plannedChemists"]) {
            chemists.push({
              "Name": chemistObj.split("#-#")[0],
              "id": chemistObj.split("#-#")[1],
              "areaId": chemistObj.split("#-#")[2]
            })
          }

          tpInsertObj["doctors"] = doctors;
          tpInsertObj["approvedDoctors"] = doctors;          

          tpInsertObj["chemists"] = chemists;
          tpInsertObj["approvedChemists"] = chemists;

          if (this.model["isDayPlan"] === true) {
            tpInsertObj["plannedDoctors"] = doctors;
            tpInsertObj["plannedChemists"] = chemists;
          } else {
            tpInsertObj["plannedDoctors"] = [];
            tpInsertObj["plannedChemists"] = [];
            
    
          }

        }else{
          tpInsertObj["area"] = areaArr;
          tpInsertObj["approvedArea"] = areaArr;    
          tpInsertObj["plannedArea"] = [];

          tpInsertObj["doctors"] = [];
          tpInsertObj["approvedDoctors"] = [];          

          tpInsertObj["chemists"] = [];
          tpInsertObj["approvedChemists"] = [];

          tpInsertObj["plannedDoctors"] = [];
            tpInsertObj["plannedChemists"] = [];

        }

  
        tpInsertObj["TPStatus"] = this.model["activity"].split("#-#")[1];
        let date = moment(dateObj).format("YYYY-MM-DD");
        let tpDate = moment.utc(date).format("YYYY-MM-DDT00:00:00.000[Z]")
       
        tpInsertObj["date"] = tpDate
        tpInsertObj["companyId"] = this.currentUser.companyId;
        tpInsertObj["month"] = parseInt(moment.utc(date).format('M'));
        tpInsertObj["year"] = parseInt(moment.utc(date).format('YYYY'));
        tpInsertObj["designation"] = this.currentUser.userInfo[0].designation;
        tpInsertObj["designationLevel"] = this.currentUser.userInfo[0].designationLevel;
        tpInsertObj["stateId"] = this.currentUser.userInfo[0].stateId;
        tpInsertObj["districtId"] = this.currentUser.userInfo[0].districtId;
        tpInsertObj["isDayPlan"] = this.model["isDayPlan"];
        tpInsertObj["createdBy"] = this.currentUser.id;
        tpInsertObj["sendForApproval"] = false;
        tpInsertObj["plannedActivity"] = this.model["activity"].split("#-#")[0];
  
        tpInsertObj["activity"] = this.model["activity"].split("#-#")[0];        
        tpInsertObj["approvedActivity"] = this.model["activity"].split("#-#")[0];
          
        //tpInsertObj["planningSubmissionDate"] = moment.utc().format("YYYY-MM-DDT00:00:00.000+0000");
        tpInsertObj["planningModificationDate"] = moment.utc().format("YYYY-MM-DDT00:00:00.000+0000");
  
        //tpInsertObj["tpSubmissionDate"] = moment.utc(defaultDate).format("YYYY-MM-DDT00:00:00.000+0000");
        tpInsertObj["remarks"] = this.model["remarks"];
        tpInsertObj["approvedRemarks"] = "";
        //tpInsertObj["approvedAt"] = moment.utc(defaultDate).format("YYYY-MM-DDT00:00:00.000+0000");
        tpInsertObj["approvalStatus"] = false;
        tpInsertObj["updatedAt"]= new Date();
        

        this.tpService.updateTP(tpInsertObj).subscribe(res=>{
          if(res["count"]>=1){
            this.toastr.success("Tour Program Updated Successfully","Tour Program Updated!!!");            
          }
          this.dialogRef.close();
        })
      }
      
    }

   
  }
  getTPExistOfDay() {
    let date = moment.utc(this.datesSelected);
    let obj = {
      where: {
        createdBy: this.model["jointWorkedWith"],
        companyId: this.currentUser.companyId,
        date: date.local().format("YYYY-MM-DDT00:00:00.000[Z]")
      }
    }
    this.tpService.getTPExistOfDay(obj).subscribe(res => {
      if (JSON.parse(JSON.stringify(res)).length === 0) {
        this.toastr.info("data not available for the selected date", "TP Data not available");
        delete this.model["activity"];
        this.model["area1"] = [{}];
        this.model["isDayPlan"] = false;
        this.model["plannedDoctors"] = [];
        this.model["plannedChemists"] = [];
        this.model["remarks"] = "";
        this.mrTPSubmitted = false;
        this.changeDetectorRef.detectChanges();
      } else {
        this.mrTPSubmitted = true;
        this.model["activity"] = res[0].activity + "#-#" + res[0].TPStatus;
        this.model["remarks"] = res[0].remarks;
        this.model["isDayPlan"] = res[0].isDayPlan;
        //removing first element which is empty object in area
        this.model["area1"].pop();
        if (res[0].area.length > 0) {
          for (const areaObj of res[0].area) {
            let obj = {
              "from": areaObj.fromId + "#-#" + areaObj.from,
              "to": areaObj.toId + "#-#" + areaObj.to
            };
            this.model["area1"].push(obj);
          }
          if (res[0].plannedDoctors.length > 0) {
            for (const doctorObj of res[0].plannedDoctors) {
              this.model["plannedDoctors"].push(doctorObj.Name + "#-#" + doctorObj.id + "#-#" + doctorObj.areaId);
            }
          }

          if (res[0].plannedChemists.length > 0) {
            for (const chemistObj of res[0].plannedChemists) {
              this.model["plannedChemists"].push(chemistObj.Name + "#-#" + chemistObj.id + "#-#" + chemistObj.areaId);
            }
          }
        }
        this.changeDetectorRef.detectChanges();
      }
    }, err => {

    })
  }

  validate() {
    if (this.datesSelected === undefined || this.datesSelected.length === 0) {
      this.toastr.error("Please Select Date to Submit TP", "Dates Required");
      return true;
    }

    if (parseInt(this.model["activity"].split("#-#")[1]) === 1) {
      for (let i = 0; i < this.model["area1"].length; i++) {
        if (this.model["area1"][i].hasOwnProperty("from") === false) {
          this.toastr.error("Please select the from area", "From Area is required");
          return false;
        }
        if (this.model["area1"][i].hasOwnProperty("to") === false) {
          this.toastr.error("Please select the to area", "To Area is required");
          return false;
        }
      }
    }
    return true;
  }


}
