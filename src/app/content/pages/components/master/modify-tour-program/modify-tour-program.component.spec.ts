import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyTourProgramComponent } from './modify-tour-program.component';

describe('ModifyTourProgramComponent', () => {
  let component: ModifyTourProgramComponent;
  let fixture: ComponentFixture<ModifyTourProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyTourProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyTourProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
