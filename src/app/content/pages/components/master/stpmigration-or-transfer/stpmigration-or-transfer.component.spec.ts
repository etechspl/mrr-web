import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { STPMigrationOrTransferComponent } from './stpmigration-or-transfer.component';

describe('STPMigrationOrTransferComponent', () => {
  let component: STPMigrationOrTransferComponent;
  let fixture: ComponentFixture<STPMigrationOrTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ STPMigrationOrTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(STPMigrationOrTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
