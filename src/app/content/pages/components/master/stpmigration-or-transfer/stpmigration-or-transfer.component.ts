import { SelectionModel } from '@angular/cdk/collections';
import { STPService } from './../../../../../core/services/stp.service';
import { MatSlideToggleChange, MatTableDataSource, MatSort, MatPaginator, MatTable } from '@angular/material';
import { FormBuilder, FormControl, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DistrictComponent } from './../../filters/district/district.component';
import { DivisionComponent } from './../../filters/division/division.component';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { StateComponent } from './../../filters/state/state.component';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import * as _moment from 'moment';

declare var $;
@Component({
  selector: 'm-stpmigration-or-transfer',
  templateUrl: './stpmigration-or-transfer.component.html',
  styleUrls: ['./stpmigration-or-transfer.component.scss']
})
export class STPMigrationOrTransferComponent implements OnInit {
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callEmployeeComponent1") callEmployeeComponent1: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(value: MatPaginator) {
    this.dataSource2.paginator = value;
  }
  isLoading: false;
 
  constructor(
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private stpService: STPService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) { 
    this.stpMigrate = this.fb.group({
      stateInfo: new FormControl(null, Validators.required),
      employeeId: new FormControl(null, Validators.required),
      employeeIdTo: new FormControl(null, Validators.required)

    });
  }
 
  displayedColumns2 = ['select', 'sn', 'fromArea', 'toArea', 'type', 'distance', 'freqVisit', 'appByMgr', 'appByAdm','submissionDate','mgrAppDate','finalAppDate'];
  dataSource2 = new MatTableDataSource<any>();

  districtAndDesignationLevelOfEmployee = [];
  currentUser = JSON.parse(sessionStorage.currentUser);
  isShowDivision = false;
  showProcessing=false;
  stpTableOptions;
  isShowData=false;
  itemsToBeSelected= [];
  selectedValue = [];
  selection = new SelectionModel<any>(true, []);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  public showState: boolean = true;

  stpMigrate = new FormGroup({
    stateInfo: new FormControl(null),
    employeeId: new FormControl(null),
    employeeIdTo: new FormControl(null)

  })

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.stpMigrate.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    } 
  }

  getDivisionValue($event) {
    this.isShowData = false;
    this.callStateComponent.setBlank()
    this.callEmployeeComponent.setBlank()
    this.callEmployeeComponent1.setBlank()
    this.callEmployeeComponent.removelist()
    this.callEmployeeComponent1.removelist()
    this.stpMigrate.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      this.callStateComponent.getStateBasedOnDivision({
        companyId: this.currentUser.companyId,
        isDivisionExist: this.isDivisionExist,
        division: [this.stpMigrate.value.divisionId],
        designationLevel: this.currentUser.userInfo[0].designationLevel
      })
    }
  }

  getStateValue(val) {
    this.isShowData = false;
    this.stpMigrate.patchValue({ stateInfo: val });
    let obj={
      companyId:this.currentUser.companyId,
      stateId:val
    }
    if (this.isDivisionExist === true) {
      obj["divisionId"]=this.stpMigrate.value.divisionId
    }
    this.callEmployeeComponent.setBlank()
    this.callEmployeeComponent1.setBlank()
    this.callEmployeeComponent.getEmployeeListOfStates(obj)
    this.callEmployeeComponent1.getEmployeeListOfStates(obj)
    this.stpMigrate.patchValue({ employeeId: null });
    this.stpMigrate.patchValue({ employeeIdTo: null });
  
  }

 
  getEmployeeValue($event) {
    this.isShowData = false;
    this.stpMigrate.patchValue({ employeeId: $event });  
    this.callEmployeeComponent.stateEmployee.map(a=>{
      if(a.userId == $event){
        this.districtAndDesignationLevelOfEmployee.push(a.districtId);
        this.districtAndDesignationLevelOfEmployee.push(a.designationLevel);
      }
    })
  }
  getEmployeeValueTo(val) {
    this.isShowData = false;
    this.stpMigrate.patchValue({ employeeIdTo: val });
  }

  

  stpDetailsForm: FormGroup = this.fb.group({
    formArray: this.fb.array([]),
  });
  get formArray() {
    return this.stpDetailsForm.get('formArray') as FormArray;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource2.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      
      this.dataSource2.data.forEach((row, index) => {
        this.formArray.controls[index].patchValue({ checked: false });
        this.selection.clear();
      })
    } else {
      this.dataSource2.data.forEach((row, index) => {
        this.formArray.controls[index].patchValue({ checked: true });
        this.selection.select(row);
      })
    }

  }
 
  getSelectionValue($event, i: number, row: object) {
    this.selectedValue.push(row);
  }

  getSTPDetails(){
    this.isToggled = false;
    this.selectedValue = [];
    this.showProcessing= true;
    let obj={};
        obj["companyId"] = this.currentUser.companyId;
        obj["userId"] = this.stpMigrate.value.employeeId;
    this.stpService.getSTPMigrateDetails(obj).subscribe(res => {
      let array = res.map(item => {
        return {
          ...item
        };
      });
      if (array.length > 0) {
        let array = res.map(item => {
          this.formArray.push(this.fb.group({
            id: new FormControl(item.id),
            checked: new FormControl(false),
            distance:new FormControl(item.distance),
            type:new FormControl(item.type)
          }));
          return {
            ...item
          };
        });
        this.showProcessing= false;
        this.dataSource2 = new MatTableDataSource(array);
        this.itemsToBeSelected = array;
        this.dataSource2.sort = this.sort;
        this.isShowData = true;
        this.dataSource2.paginator = this.paginator;
      } else {
        this.toastrService.info("STP Data is not available", "Data not available");
        this.showProcessing= false;
        this.isShowData = false;
      }        
      this._changeDetectorRef.detectChanges();
    })
  }


  migrateSTP(){
    let formArrayData = this.stpDetailsForm.value.formArray
    .filter(i => i.checked)
    .map(i => { delete i.checked; return i });
  if (formArrayData.length === 0) {
    this.toastrService.error('', 'Please Select the Checkbox')
  } else {
    let obj = {
            companyId: this.currentUser.companyId,
           employeeId: this.stpMigrate.value.employeeId,
           employeeIdTo: this.stpMigrate.value.employeeIdTo
          }
         
  for (let i = 0; i < formArrayData.length; i++) {
    obj['id'] = formArrayData[i].id;
    obj['distance'] = formArrayData[i].distance;
    obj['type']=formArrayData[i].type;
    this.stpService.migrateSTP(obj).subscribe(res => {
      if(res.length>0){
  this.toastrService.success("STP Data is Migrate Successfully");
  this.showProcessing= false;
  this.isShowData = false;
  this.stpDetailsForm.reset();
      }else{
        this.toastrService.info("STP Data is Already Migrated");
        this.showProcessing= false;
        this.isShowData = false;
        this.stpDetailsForm.reset();  
      }
this._changeDetectorRef.detectChanges();
    })
  }
}
}

  transferSTP(){
    
    let formArrayData = this.stpDetailsForm.value.formArray
    .filter(i => i.checked)
    .map(i => { delete i.checked; return i });
  if (formArrayData.length === 0) {
    this.toastrService.error('', 'Please Select the Checkbox')
  } else {
    let obj = {
            companyId: this.currentUser.companyId,
           emloyeeId: this.stpMigrate.value.employeeId,
           emloyeeIdTo: this.stpMigrate.value.employeeIdTo
          }
         
  for (let i = 0; i < formArrayData.length; i++) {
    obj['id'] = formArrayData[i].id;
    obj['distance'] = formArrayData[i].distance;
    obj['type']=formArrayData[i].type;
    this.stpService.transferStp(obj).subscribe(res => {
if(res.count>0){
  this.toastrService.success("STP Data is transfered Successfully");
  this.showProcessing= false;
  this.isShowData = false;
  this.stpDetailsForm.reset();
}else{
   this.toastrService.info("STP Data is not transfered");
        this.showProcessing= false;
        this.isShowData = false;
        this.stpDetailsForm.reset();
}
this._changeDetectorRef.detectChanges();
    })
  }
}
  }

}
