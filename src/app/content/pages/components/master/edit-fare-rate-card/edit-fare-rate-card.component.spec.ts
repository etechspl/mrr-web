import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFareRateCardComponent } from './edit-fare-rate-card.component';

describe('EditFareRateCardComponent', () => {
  let component: EditFareRateCardComponent;
  let fixture: ComponentFixture<EditFareRateCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFareRateCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFareRateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
