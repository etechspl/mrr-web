import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { FareRateCardService } from '../../../../../core/services/FareRateCard/fare-rate-card.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'm-edit-fare-rate-card',
  templateUrl: './edit-fare-rate-card.component.html',
  styleUrls: ['./edit-fare-rate-card.component.scss']
})
export class EditFareRateCardComponent implements OnInit {

  companyId: any;
  previousData: any;
  editFareRateCard: FormGroup;
  
  constructor(
    private toast: ToastrService,
    private service: FareRateCardService,
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditFareRateCardComponent>,
    public userDetailsService: UserDetailsService,
    @Inject(MAT_DIALOG_DATA) data
  ) { 
    this.previousData = data;
  }

  ngOnInit() {
    this.editFareRateCard = this.formBuilder.group({
      designation: ['', Validators.compose([Validators.required])],
      fromDistance: [null, Validators.compose([Validators.required])],
      toDistance: [null, Validators.compose([Validators.required])],
      allounceToBeGet: [null, Validators.compose([Validators.required])],
      rate: [null, Validators.compose([Validators.required])],
      frcCode: [null, Validators.compose([Validators.required])],
      description: [null, Validators.compose([Validators.required])],
      applicableTo: [null, Validators.compose([Validators.required])],
    });
    this.companyId = JSON.parse(sessionStorage.getItem('currentUser')).companyId;

    this.assignFRCValues();
  }

  assignFRCValues(){
    this.editFareRateCard.patchValue({
      
      designation: this.previousData.designation,
      fromDistance: this.previousData.fromDistance,
      toDistance: this.previousData.toDistance,
      allounceToBeGet: this.previousData.allounceToBeGet,
      rate: this.previousData.rate,
      frcCode: this.previousData.frcCode,
      description: this.previousData.description,
      applicableTo: this.previousData.applicableTo,
    });
  }

 

  updateAndClose(){
    let obj = {
      id: this.previousData.id,
      designation: this.editFareRateCard.controls.designation.value,
      fromDistance: this.editFareRateCard.controls.fromDistance.value,
      toDistance: this.editFareRateCard.controls.toDistance.value,
      allounceToBeGet:this.editFareRateCard.controls.allounceToBeGet.value,
      rate: this.editFareRateCard.controls.rate.value,
      frcCode: this.editFareRateCard.controls.frcCode.value,
      description: this.editFareRateCard.controls.description.value,
      applicableTo: this.editFareRateCard.controls.applicableTo.value,
      companyId: this.companyId,
    }
    this.service.updateFareRateCard(obj).subscribe(res => {
      (res.count) ?  this.closeModel('1'): this.closeModel('2');
    });
  }

  closeModel(obj: any){
    if(obj === '1'){
      this.service.frcUpdatedOrNot.emit('updated');
      this.dialogRef.close();
      this.toast.success('data update successfully');
    } else if(obj === '2'){
      this.dialogRef.close();
      this.toast.success('Something went wrong!');
    } else {
      this.dialogRef.close();
    }
  }

 

}
