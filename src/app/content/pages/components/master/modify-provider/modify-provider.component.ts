import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { StateComponent } from '../../filters/state/state.component';

@Component({
  selector: 'm-modify-provider',
  templateUrl: './modify-provider.component.html',
  styleUrls: ['./modify-provider.component.scss']
})
export class ModifyProviderComponent implements OnInit {
  areas = [];
  providers = [];
  providersForm = [];
  category = [];
  degree = [];
  specialization = [];
  type = [];
  focusProduct = [];
  products = [];
  areaSelected;
  showDoctorForm = false;
  showFamilyInfo = false;
  showOtherInfo = false;
  showFormButton = false;
  showOtherInfoVendor = false;
  showVendorForm = false;

  constructor(
    public fb: FormBuilder,
    private _toastr: ToastrService,
    private userAreaMapping: UserAreaMappingService,
    private _providerService: ProviderService,
    private _productService: ProductService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { this.modifyForm(fb); }

  public modifyProvider: FormGroup;

  modifyForm(fb: FormBuilder) {
    this.modifyProvider = fb.group({
      reportTypeName: new FormControl('doctor'),
      areaOutside: new FormControl('', [Validators.required]),
      area: new FormControl('', [Validators.required]),
      provider: new FormControl('', [Validators.required]),
      doctorCode: new FormControl(''),
      doctorName: new FormControl('', [Validators.required]),
      aadhar: new FormControl(0),
      category: new FormControl(''),
      degree: new FormControl(''),
      specialization: new FormControl(''),
      familyInfo: new FormControl(''),
      spouseName: new FormControl(''),
      spouseBirthDate: new FormControl(''),
      spouseProfession: new FormControl(''),
      otherInfo: new FormControl(''),
      birthDateOtherInfo: new FormControl(''),
      annyDate: new FormControl(''),
      frequencyVisit: new FormControl(0),
      contactPerson: new FormControl(''),
      businessPotential: new FormControl(0),
      address: new FormControl(''),
      city: new FormControl(''),
      venAddress: new FormControl(''),
      venCity: new FormControl(''),
      phoneNum: new FormControl(0),
      email: new FormControl(''),
      focusProduct: new FormControl(''),
      type: new FormControl(''),
      ownerName: new FormControl(''),
      ownerDob: new FormControl(''),
      ownerDoa: new FormControl(''),
      shopDoa: new FormControl(''),
      pancard: new FormControl(''),
      gst: new FormControl(''),
      licence: new FormControl(''),
      stateId: new FormControl(''),
      districtId: new FormControl(''),
      userId: new FormControl(''),
      data: this.fb.array([this.fb.group({
        childName: new FormControl(""),
        age: new FormControl(""),
      })]),
      data1: this.fb.array([this.fb.group({
        address: new FormControl(""),
        city: new FormControl(""),
        daysInfo: new FormControl(""),
        fromTime: new FormControl(""),
        toTime: new FormControl(""),
      })]),
    });
  }
  currentUser = JSON.parse(sessionStorage.currentUser);
  ngOnInit() {
    this.modifyForm(this.fb);
    this.category = this.currentUser.company.category;
    this.degree = this.currentUser.company.degree;
    this.specialization = this.currentUser.company.specialization;
    this.type = this.currentUser.company.type;
    if (this.currentUser.userInfo[0].rL == 1) {
      this.getUserMappedAreas(this.currentUser.id);
    } else if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 4) {
      this.getUserMappedAreas(this.currentUser.id);
    }
  }
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  getStateValue(val) {
    this.modifyProvider.patchValue({ stateId: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);

  }
  
  getDistrictValue(val) {
    this.modifyProvider.patchValue({ districtId: val });
    let districtArr = [];
    districtArr.push(val);
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr)
  }

  getEmployees(val) {
    this.getUserMappedAreas(val)
  }
  reportType(val) {
    this.callStateComponent.setBlank();
    this.callDistrictComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    this.modifyForm(this.fb);
    this.showDoctorForm = false;
    this.showVendorForm = false;
    this.showFormButton = false;
    this.modifyProvider.patchValue({ area: undefined });
    this.modifyProvider.patchValue({ provider: undefined });
    this.modifyProvider.patchValue({ reportTypeName: val });
  }

  getUserMappedAreas(userId) {
    this.modifyProvider.patchValue({ userId: userId });
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, userId).subscribe(res => {
      this.areas = res;
    });
  }

  getProvider(val) {
    this.showDoctorForm = false;
    this.showVendorForm = false;
    this.showFormButton = false;
    this.modifyProvider.patchValue({ areaOutside: val })
    /*this._providerService.getProviderListForSelectedBlock(this.currentUser.companyId, [val]).subscribe(res => {
      this.providers = res;
    });*/
    let whereObj = {
      "companyId": this.currentUser.companyId,
      "blockId": this.modifyProvider.value.areaOutside,
      status:true
    };
    if (this.modifyProvider.value.reportTypeName == "doctor") {
      whereObj['providerType'] = ["RMP"];
    } else if (this.modifyProvider.value.reportTypeName == "vendor") {
      whereObj['providerType'] = ['Drug', 'Chemist', 'Stockist'];

    }
    this._providerService.getProviderDetails(whereObj).subscribe(getProvider => {
      this.providers = getProvider;
      this._changeDetectorRef.detectChanges();
    });
  }

  getFocusProduct(val) {
    this._providerService.getFocusProductList(this.currentUser.companyId, val).subscribe(res => {
      this._productService.getFocusProductBasedOnProvider(this.currentUser.companyId, [res[0].focusProduct]).subscribe(res => {
        this.focusProduct = res;
      });
    });
  }

  getEditFormInfo(val) {
    this._providerService.setProviderDetailForEditInfo(val).subscribe(res => {
      this.modifyProvider.get("area").setValue(res[0].blockId);
      if (this.modifyProvider.value.reportTypeName == "doctor") {
        this.modifyProvider.get("doctorCode").setValue(res[0].providerCode);
        this.modifyProvider.get("address").setValue(res[0].address);

        this.modifyProvider.get("doctorName").setValue(res[0].providerName);
        this.modifyProvider.get("aadhar").setValue(res[0].aadhar);
        this.modifyProvider.get("category").setValue(res[0].category);
        this.modifyProvider.get("degree").setValue(res[0].degree);
        this.modifyProvider.get("specialization").setValue(res[0].specialization);
        this.modifyProvider.get("phoneNum").setValue(res[0].mobile);
        if (res[0].isDoctorFamilyInfo == true) {
          this.showFamilyInfo = true;
          this.modifyProvider.get("familyInfo").setValue(res[0].isDoctorFamilyInfo);
          this.modifyProvider.get("spouseName").setValue(res[0].doctorFamilyInfo.spouseName);
          this.modifyProvider.get("spouseProfession").setValue(res[0].doctorFamilyInfo.spouseProfession);
          this.modifyProvider.get("spouseBirthDate").setValue(res[0].doctorFamilyInfo.spouseBirthDate);
        }
        if (res[0].isDoctorOtherInfo == true) {
          this.getFocusProduct(val);
          this._productService.getProductList(this.currentUser.userInfo[0].stateId, this.currentUser.companyId).subscribe(proList => {
            this.products = proList;
          });
          this.showOtherInfo = true;
          this.modifyProvider.get("otherInfo").setValue(res[0].isDoctorOtherInfo);
          this.modifyProvider.get("birthDateOtherInfo").setValue(res[0].dob);
          this.modifyProvider.get("annyDate").setValue(res[0].doa);
          this.modifyProvider.get("frequencyVisit").setValue(res[0].frequencyVisit);
          this.modifyProvider.get("contactPerson").setValue(res[0].contactPerson);
          this.modifyProvider.get("businessPotential").setValue(res[0].businessPotential);
          this.modifyProvider.get("email").setValue(res[0].email);
          this.modifyProvider.get("focusProduct").setValue(res[0].focusProduct);
          this.modifyProvider.get("licence").setValue(res[0].licenceNumber);
        }
        this.showDoctorForm = true;
        this.showVendorForm = false;
        this.showFormButton = true;
      } else if (this.modifyProvider.value.reportTypeName == "vendor") {
        this.modifyProvider.get("type").setValue(res[0].providerType);
        this.modifyProvider.get("doctorCode").setValue(res[0].providerCode);
        this.modifyProvider.get("address").setValue(res[0].address);

        this.modifyProvider.get("doctorName").setValue(res[0].providerName);
        this.modifyProvider.get("aadhar").setValue(res[0].aadhar);
        this.modifyProvider.get("phoneNum").setValue(res[0].mobile);
        if (res[0].isVendorOtherInfo == true) {
          this.showOtherInfoVendor = true;
          this.modifyProvider.get("otherInfo").setValue(res[0].isVendorOtherInfo);
          this.modifyProvider.get("ownerName").setValue(res[0].vendorOwnerInfo.ownerName);
          this.modifyProvider.get("ownerDob").setValue(res[0].vendorOwnerInfo.ownerDob);
          this.modifyProvider.get("ownerDoa").setValue(res[0].vendorOwnerInfo.ownerDoa);
          this.modifyProvider.get("shopDoa").setValue(res[0].vendorOwnerInfo.shopDoa);
          this.modifyProvider.get("venAddress").setValue(res[0].vendorOwnerInfo.address);
          this.modifyProvider.get("venCity").setValue(res[0].vendorOwnerInfo.city);
          this.modifyProvider.get("email").setValue(res[0].email);
          this.modifyProvider.get("pancard").setValue(res[0].vendorOwnerInfo.panCard);
          this.modifyProvider.get("gst").setValue(res[0].vendorOwnerInfo.gstNum);
          this.modifyProvider.get("licence").setValue(res[0].vendorOwnerInfo.drugLicenceNumber);
          this.modifyProvider.get("category").setValue(res[0].category);
        }
        this.showDoctorForm = false;
        this.showVendorForm = true;
        this.showFormButton = true;
      }
    });
  }

  getAdditionalInfo(val, eValue) {
    if (val == 'family' && eValue == true) {
      this.showFamilyInfo = true;
    } else if (val == 'family' && eValue == false) {
      this.showFamilyInfo = false;
    }
    if (val == 'other' && eValue == true) {
      this.showOtherInfo = true;
      this._productService.getProductList(this.currentUser.userInfo[0].stateId, this.currentUser.companyId).subscribe(proList => {
        this.products = proList;
      });
    } else if (val == 'other' && eValue == false) {
      this.showOtherInfo = false;
    }
  }

  getAdditionalInfoVendor(val, eValue) {
    this.showOtherInfoVendor = false;
    if (val == 'other' && eValue == true) {
      this.showOtherInfoVendor = true;
    } else if (val == 'other' && eValue == false) {
      this.showOtherInfoVendor = false;
    }
  }



  modifyProviderForm() {
    let finalArrObj = [];
    var data = {};
    data = {
      "id": this.modifyProvider.value.provider,
      "blockId": this.modifyProvider.value.area,
      "providerCode": this.modifyProvider.value.doctorCode,
      "address": this.modifyProvider.value.address,
      "providerName": this.modifyProvider.value.doctorName,
      "aadhar": this.modifyProvider.value.aadhar,
      "mobile": this.modifyProvider.value.phoneNum,
      "category": this.modifyProvider.value.category,
      "email": this.modifyProvider.value.email,
    }
    if (this.modifyProvider.value.reportTypeName == 'doctor') {
      let doctorFamilyInfo = {};
      doctorFamilyInfo['spouseName'] = this.modifyProvider.value.spouseName;
      doctorFamilyInfo['spouseProfession'] = this.modifyProvider.value.spouseProfession;
      doctorFamilyInfo['spouseBirthDate'] = new Date(this.modifyProvider.value.spouseBirthDate);
      /*var childInfos = [];
      let doctorClinicalInfo = [];
      let sitDaysTime = [];
      let doctorSitTime = [];
      for (var i = 0; i < this.modifyProvider.value.data.length; i++) {
        childInfos.push({
          "childName": this.modifyProvider.value.data[i].childName,
          "childAge": this.modifyProvider.value.data[i].age
        });
      }
      for (var ii = 0; ii < this.modifyProvider.value.data1.length; ii++) {
        doctorClinicalInfo.push({
          "position": ii,
          "address": this.modifyProvider.value.data1[ii].address,
          "city": this.modifyProvider.value.data1[ii].city,
          "day": this.modifyProvider.value.data1[ii].daysInfo,
          "from": this.modifyProvider.value.data1[ii].fromTime,
          "to": this.modifyProvider.value.data1[ii].toTime,
        });
      }
      doctorFamilyInfo['childInfo'] = childInfos;
      data['doctorClinicalInfo'] = doctorClinicalInfo;*/
      data['degree'] = this.modifyProvider.value.degree;
      data['specialization'] = this.modifyProvider.value.specialization;
      data['doctorFamilyInfo'] = doctorFamilyInfo;
      data['dob'] = new Date(this.modifyProvider.value.birthDateOtherInfo);
      data['doa'] = new Date(this.modifyProvider.value.annyDate);
      data['frequencyVisit'] = this.modifyProvider.value.frequencyVisit;
      data['contactPerson'] = this.modifyProvider.value.contactPerson;
      data['businessPotential'] = this.modifyProvider.value.businessPotential;
      data['focusProduct'] = this.modifyProvider.value.focusProduct;
      data['licenceNumber'] = this.modifyProvider.value.licence;
      data['isDoctorFamilyInfo'] = this.modifyProvider.value.familyInfo;
      data['isDoctorOtherInfo'] = this.modifyProvider.value.otherInfo;
    } else if (this.modifyProvider.value.reportTypeName == 'vendor') {
      let vendorOwnerInfo = {};
      vendorOwnerInfo['ownerName'] = this.modifyProvider.value.ownerName,
        vendorOwnerInfo['ownerDob'] = this.modifyProvider.value.ownerDob,
        vendorOwnerInfo['ownerDoa'] = this.modifyProvider.value.ownerDoa,
        vendorOwnerInfo['shopDoa'] = this.modifyProvider.value.shopDoa,
        vendorOwnerInfo['panCard'] = this.modifyProvider.value.pancard,
        vendorOwnerInfo['gstNum'] = this.modifyProvider.value.gst,
        vendorOwnerInfo['drugLicenceNumber'] = this.modifyProvider.value.licence,
        vendorOwnerInfo['address'] = this.modifyProvider.value.venAddress,
        vendorOwnerInfo['city'] = this.modifyProvider.value.venCity
      data['providerType'] = this.modifyProvider.value.type;
      data['vendorOwnerInfo'] = vendorOwnerInfo;
     
    }

    finalArrObj.push(data);
    this._providerService.modifyProvider(data).subscribe(res => {
      this.showDoctorForm = false;
      this.showFamilyInfo = false;
      this.showOtherInfo = false;
      this.showFormButton = false;
      this.showOtherInfoVendor = false;
      this.showVendorForm = false;
      this.modifyProvider.reset();
      this.modifyForm(this.fb);
      this._toastr.success("Your Provider Details updated successfully !", "Provider Details updated !!");
      this._changeDetectorRef.detectChanges();
    }, err => {
      console.log(err);
    })
  }



}
