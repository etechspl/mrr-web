import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { MatSnackBar } from '@angular/material';
import { DivisionComponent } from '../../filters/division/division.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';


@Component({
  selector: 'm-product-creation',
  templateUrl: './product-creation.component.html',
  styleUrls: ['./product-creation.component.scss']
})
export class ProductCreationComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);

  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  DA_TYPE = this.currentUser.company.expense.daType;
  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  isShowDivision = false;
  public showDivisionFilter: boolean = false;

  productCreationForm: FormGroup;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  constructor(
    public snackBar: MatSnackBar,
    private fb: FormBuilder,
    private _productService: ProductService,
    public _toasterService: ToastrService,
  ) {
    if(this.currentUser.companyId == "625550a61315d10ff9fd3020"){
      console.log("athila")
      this.productCreationForm = this.fb.group({
        companyId: null,
        divisionId: null,
        assignedStates: [null, Validators.required],
        productName: [null, Validators.required],
        productCode: null,
        productType: null,
        productShortName: null,
        productPackageSize: null,
        samplePackageSize: null,
        ptw: [null],
        ptr: [null],
        mrp: [null, Validators.required],
        sampleRate: null,
        includeVat: null,
      })
    }
    else{
      console.log("tarun")
      this.productCreationForm = this.fb.group({
        companyId: null,
        divisionId: null,
        assignedStates: [null, Validators.required],
        productName: [null, Validators.required],
        productCode: null,
        productType: null,
        productShortName: null,
        productPackageSize: null,
        samplePackageSize: null,
        ptw: [null, Validators.required],
        ptr: [null, Validators.required],
        mrp: [null, Validators.required],
        sampleRate: null,
        includeVat: null,
      })
    }
    
  }

  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      this.productCreationForm.get('divisionId').setValidators([Validators.required])
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.isShowDivision = true;
    }
  }

  getStateValue(val) {
    this.productCreationForm.patchValue({ assignedStates: val });

    if (this.IS_DIVISION_EXIST === true) {
      if (this.productCreationForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          stateId: this.productCreationForm.value.assignedStates,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.productCreationForm.value.divisionId
        })
      }
    } else {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
    }
  }

  getDivisionValue($event) {
    this.productCreationForm.patchValue({ assignedStates: null });
    (this.callStateComponent) ? this.callStateComponent.setBlank() : null;
    this.productCreationForm.patchValue({ divisionId: $event });
    if (this.IS_DIVISION_EXIST === true) {
      this.callStateComponent.getStateBasedOnDivision({
        companyId: this.currentUser.companyId,
        isDivisionExist: this.IS_DIVISION_EXIST,
        division: this.productCreationForm.value.divisionId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      })

      //getting employee if we change the division filter after select the designation
      if (this.productCreationForm.value.divisionId != null && this.productCreationForm.value.designation != null) {
        if (this.productCreationForm.value.designation.length > 0) {
          this.callEmployeeComponent.getEmployeeListBasedOnDivision({
            companyId: this.companyId,
            division: this.productCreationForm.value.divisionId,
            status: [true],
            designationObject: this.productCreationForm.value.designation
          })
        }
      }
    }
  }

  showReport() {
    this._productService.createProduct(this.productCreationForm.value, this.currentUser.company.id).subscribe(productResponse => {
      this._toasterService.success('Product Created Successfully');
    }, err => {
      console.log(err);
      this._toasterService.error('Product Are Already Exists');
    });
    this.productCreationForm.reset();
    this.callStateComponent.setBlank();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
