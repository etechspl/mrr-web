import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'formly-field-ng-select',
  template: `

    <label for="formly_8_select_workingStatus_1" class="ng-star-inserted">{{to.label}} </label><br>
    <ng-select [items]="to.options"
      [bindLabel]="labelProp"
      [bindValue]="valueProp"
      [multiple]="to.multiple"
      [searchable]="true"
      [hideSelected]="true"
      [formControl]="formControl" class="custom" >
    </ng-select>
  `,
})
export class FormlyFieldNgSelect extends FieldType {
  get labelProp(): string { return this.to.labelProp || 'label'; }
  get valueProp(): string { return this.to.valueProp || 'value'; }
  get groupProp(): string { return this.to.groupProp || 'group'; }
}