import { RepeatTypeComponents } from './repeat-section.types';
import { TourProgramService } from './../../../../../core/services/TourProgram/tour-program.service';
import { DcrPreviewComponent } from './../dcr-preview/dcr-preview.component';
import { MatDialog, MatStepper } from '@angular/material';
import { formatDate } from '@angular/common';
import { GiftService } from './../../../../../core/services/Gift/gift.service';
import { ProductService } from './../../../../../core/services/Product/product.service';
import { ProviderService } from './../../../../../core/services/Provider/provider.service';
import { HierarchyService } from './../../../../../core/services/hierarchy-service/hierarchy.service';
import { DcrReportsService } from './../../../../../core/services/DCR/dcr-reports.service';
import { AreaService } from './../../../../../core/services/Area/area.service'
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup, FormArray } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import * as moment from 'moment';

import { ActivityService } from '../../../../../core/services/Activity/activity.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { truncateWithEllipsis } from '@amcharts/amcharts4/.internal/core/utils/Utils';
export interface StepType {
  label: string;
  fields: FormlyFieldConfig[];
}
@Component({
  selector: 'm-dcr-submission',
  templateUrl: './dcr-submission.component.html',
  styleUrls: ['./dcr-submission.component.scss']
})
export class DcrSubmissionComponent implements OnInit {
  activedStep = 0;
  currentUser = JSON.parse(sessionStorage.currentUser);

  resultArr = [];
  dcrId: "";
  JointWorkWithvalues = [];
  tpStatus = "";
  @ViewChild("callDcrPreviewComponent") callDcrPreviewComponent: DcrPreviewComponent;

  constructor(private toastr: ToastrService, private _areaservice: AreaService, private _hierarchy: HierarchyService, private _dcrReportsService: DcrReportsService, private _providerservice: ProviderService, private _chagedetect: ChangeDetectorRef, private _productservice: ProductService, private _giftService: GiftService, public dialog: MatDialog, private _tpservice: TourProgramService,
    private _activityService: ActivityService, private userAreaMappingService: UserAreaMappingService) { }

  ngOnInit() {

  }
  model = {
    workingStatus: "",
    deviate: null,
    deviateReason: "",
    area: [],
    jointWork: [],
    remarks: '',
    doctor: [{ 'providerId': [{}] }],
    date: new Date(),
    vendor: [{ 'vendorProviderId': [{}] }],
    dcrMeetings: [{}],
    dcrExpense: [{}]
    // workingWith: this.callBackWorkingOptions
  };


  showDCRSummary = true;
  openDialog(stepper) {

    if (sessionStorage.getItem("dcrId") != undefined) {
      this.showDCRSummary = false;
      this.callDcrPreviewComponent.getUserCompleteDCRDetails(sessionStorage.getItem("dcrId"));

    } else {
      this.showDCRSummary = true;
    }
  }


  getTPInfoBasedOnDate(userId, value) {

    console.log("this.currentUser : ", this.currentUser.company.validation.patchWiseReporting);
    this.showDCRSummary = true;
    this.resultArr = [];
    if (this.currentUser.company.validation.patchWiseReporting == true) {
      let patch = [];
      let tpresponse = [];
      this._tpservice.getPlannedPatchOnDate(value.model.date, userId).subscribe(res => {
        console.log("res : ", res);
        tpresponse.push(res);
        if (tpresponse[0].length > 0) {
          if (tpresponse[0][0].approvalStatus == true) {
            if (tpresponse[0][0].hasOwnProperty('patchId')) {
              console.log("tpresponse[0][0].patchId.length : ", tpresponse[0][0].patchId);
              for (var i = 0; i < tpresponse[0][0].patchId.length; i++) {
                patch.push({ "patch": tpresponse[0][0].patch[i] });
              }
            }
          }
        }
      })



    } else {
      this._dcrReportsService.getUserDateWiseDCRDetail(userId, value.model.date, value.model.date).subscribe(res => {

        this.resultArr.push(res);
        if (this.resultArr[0].length > 0) {
          //console.log(this.resultArr[0][0].workingStatus);
          let workingwith = [], area = [], deviateStatus = false;
          for (var i = 0; i < this.resultArr[0][0].jointWork.length; i++) {
            workingwith.push(this.resultArr[0][0].jointWork[i]);
          }

          for (var i = 0; i < this.resultArr[0][0].visitedBlock.length; i++) {
            area.push({ "from": this.resultArr[0][0].visitedBlock[i].fromId + '#-#' + this.resultArr[0][0].visitedBlock[i].from, "to": this.resultArr[0][0].visitedBlock[i].toId + '#-#' + this.resultArr[0][0].visitedBlock[i].to });
          }
          if (this.resultArr[0][0].deviateReason != undefined && this.resultArr[0][0].deviateReason != "") {
            deviateStatus = true;
          }
          this.model = {
            date: value.model.date,
            workingStatus: this.resultArr[0][0].workingStatus + "#-#" + this.resultArr[0][0].DCRStatus,
            remarks: this.resultArr[0][0].remarks,
            deviate: deviateStatus,
            deviateReason: this.resultArr[0][0].deviateReason,
            area: area,
            jointWork: workingwith,
            //doctor: [{ 'providerId': [{}] }],
            doctor: [],
            vendor: [{ 'vendorProviderId': [{}] }],
            dcrMeetings: [{}],
            dcrExpense: [{}]
          }
          this._chagedetect.detectChanges();

        } else {
          let area = [];
          let tpresponse = [];
          this._tpservice.getPlannedAreaOnDate(value.model.date, userId).subscribe(res => {
            tpresponse.push(res);
            if (tpresponse[0].length > 0) {
              if (tpresponse[0][0].approvalStatus == true) {
                if (tpresponse[0][0].hasOwnProperty('area')) {
                  for (var i = 0; i < tpresponse[0][0].area.length; i++) {
                    area.push({ "from": tpresponse[0][0].area[i].fromId + '#-#' + tpresponse[0][0].area[i].from, "to": tpresponse[0][0].area[i].toId + '#-#' + tpresponse[0][0].area[i].to });
                  }
                  this.model = {
                    date: value.model.date,
                    workingStatus: tpresponse[0][0].approvedActivity + '#-#' + tpresponse[0][0].TPStatus,
                    remarks: '',
                    deviate: true,
                    deviateReason: "",
                    area: area,
                    jointWork: [],
                    //doctor: [{ 'providerId': [{}] }],
                    doctor: [],
                    vendor: [{ 'vendorProviderId': [{}] }],
                    dcrMeetings: [{}],
                    dcrExpense: [{}]

                  }
                  this._chagedetect.detectChanges();
                } else {
                  this.model = {
                    date: value.model.date,
                    workingStatus: tpresponse[0][0].approvedActivity + '#-#' + tpresponse[0][0].TPStatus,
                    remarks: '',
                    deviate: null,
                    deviateReason: "",
                    area: [],
                    jointWork: [],
                    //doctor: [{ 'providerId': [{}] }],
                    doctor: [],
                    vendor: [{ 'vendorProviderId': [{}] }],
                    dcrMeetings: [{}],
                    dcrExpense: [{}]
                  }
                }
              } else {

                this.model = {
                  date: value.model.date,
                  workingStatus: '',
                  remarks: '',
                  deviate: null,
                  deviateReason: "",
                  area: [],
                  jointWork: [],
                  //doctor: [{ 'providerId': [{}] }],
                  doctor: [],
                  vendor: [{ 'vendorProviderId': [{}] }],
                  dcrMeetings: [{}],
                  dcrExpense: [{}]
                }
                this.toastr.error("Tour Programm is not approved!!")
              }


            } else {
              this.model = {
                date: value.model.date,
                workingStatus: '',
                remarks: '',
                deviate: null,
                deviateReason: "",
                area: [],
                jointWork: [],
                //doctor: [{ 'providerId': [{}] }],
                doctor: [],
                vendor: [{ 'vendorProviderId': [{}] }],
                dcrMeetings: [{}],
                dcrExpense: [{}]
              }
              this._chagedetect.detectChanges();
              this.toastr.error("Tour Programm is not submitted!!")
            }
          });
        }
      })
    }

  }
  blockIds:any
  getProviderInfos(value, providerType) {
    var areaIds = [];
    if (Object.keys(value).length != 0) {
      for (var n = 0; n < value.length; n++) {
        if (value[n].from != undefined) {
          areaIds.push(value[n].from.split("#-#")[0]);
          areaIds.push(value[n].to.split("#-#")[0]);
        }

      }
      if (providerType.includes('RMP')) {
        this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, providerType).subscribe((res: any[]) => {
          console.log("res------------------------------------>", res);
           this.blockIds = res[0].providerInfo.blockId;
          console.log('this is myy blockids',this.blockIds)
          //this.getField(this.steps[1].fields, 'providerId').templateOptions.options = res;
          this.getField(this.steps[1].fields, 'doctor').templateOptions.options = res
        });
      } else if (providerType.includes('Drug') || providerType.includes('Stockist')) {
        this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, providerType).subscribe((res: any[]) => this.getField(this.steps[2].fields, 'vendorProviderId').templateOptions.options = res);
      }
      this._chagedetect.detectChanges();

    }
  }
  getManagersAreaList(companyId, districtId, designation, hierarchy, field) {
    this._areaservice.getAreaList(companyId, districtId, designation, hierarchy).subscribe((res: any) => {
      var a = [];
      a.push(res)
      var values = [];
      for (var n = 0; n < a[0].length; n++) {
        values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
      }

      this.getFields(this.steps[0].fields, 'from', 3).templateOptions.options = values
      this.getFields(this.steps[0].fields, 'to', 3).templateOptions.options = values

    }


    );

  }

  steps: StepType[] = [
    {
      label: 'DCR Main',
      fields: [
        { template: '<hr />' },
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-4',
              key: 'date',
              type: 'input',
              templateOptions: {
                type: 'date',
                maxDate: new Date(),
                label: 'Date',
                required: true,
                change: (field, $event) => {
                  this.getTPInfoBasedOnDate(this.currentUser.id, field);
                  this._chagedetect.detectChanges();
                }
              },
            },

            {
              className: 'col-4',
              key: 'workingStatus',
              type: 'select',
              templateOptions: {
                label: 'Working Status',
                // placeholder: 'Working Status',
                options: [
                  // { label: 'Working', value: 'Working' },
                  // { label: 'Meeting', value: 'Meeting' },
                  // { label: 'Leave', value: 'Leave' },
                  // { label: 'Transit', value: 'Transit' },
                  // { label: 'Working and Half Day', value: 'Working' },
                ],
                required: true
              },
              expressionProperties: {
                'templateOptions.disabled': 'model.workingStatus=="Working" && model.deviate == false'
              },
              lifecycle: {
                onInit: (form, field) => {
                  this._activityService.getActivities(this.currentUser.companyId).subscribe(res => {
                    let workingStatusArray = [];
                    for (let i = 0; i < res[0].activities.length; i++) {
                      workingStatusArray.push({
                        label: res[0].activities[i].name,
                        value: res[0].activities[i].name + "#-#" + res[0].activities[i].id
                      });
                    }
                    field.templateOptions.options = workingStatusArray;
                    this._chagedetect.detectChanges();
                  }, err => {
                    console.log(err);
                  })


                }
              }
            },
            {
              className: 'col-4',
              key: 'jointWork',
              type: 'select',
              templateOptions: {
                label: 'Joint Workwith',
                multiple: true,
                change: (field, $event) => {
                  if (this.currentUser.userInfo[0].designationLevel > 1) {
                    var hierarchy = [];
                    for (let n = 0; n < this.model.jointWork.length; n++) {
                      hierarchy.push(this.model.jointWork[n].id)
                    }
                    this.getManagersAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], 1, hierarchy, field);
                    this._chagedetect.detectChanges();
                  }

                }
              },
              hideExpression: () => {
                // access to the main model can be through `this.model` or `formState`
                if (this.model.workingStatus.split("#-#")[1] != "1") {
                  return true
                } else {
                  return false;
                }
              },
              lifecycle: {
                onInit: (form, field) => {
                  var filter = {};

                  filter = { companyId: this.currentUser.companyId, supervisorId: this.currentUser.id, type: "lowerWithUpper" };

                  this._hierarchy.getManagerHierarchy(filter).subscribe(res => {
                    var a = [];
                    a.push(res)
                    var values = [];
                    for (var n = 0; n < a[0].length; n++) {
                      values.push({ 'label': a[0][n].name + " (" + a[0][n].designation + ")", 'value': { id: a[0][n].userId, Name: a[0][n].name } });
                    }
                    field.templateOptions.options = values;
                    this._chagedetect.detectChanges();
                  })
                },
              },
            },]
        },
        {
          fieldGroupClassName: 'row',
          hideExpression: () => {
            // access to the main model can be through `this.model` or `formState`
            if (this.model.workingStatus.split("#-#")[1] != "1") {
              return true
            } else {
              return false;
            }

          },
          fieldGroup: [
            {
              className: 'section-label col-sm-5',
              template: '<div><strong><font color="#2471A3">Deviate Info:</font></strong></div><hr />',

            },
            {
              className: 'section-label col-sm-7',
              template: '<div><strong><font color="#2471A3">' + this.currentUser.company.lables.areaLabel + ' Details:</font></strong></div><hr />',
            },
          ]
        },
        {
          fieldGroupClassName: 'row',
          hideExpression: () => {
            // access to the main model can be through `this.model` or `formState`
            if (this.model.workingStatus.split("#-#")[1] != "1") {
              return true
            } else {
              return false;
            }
          },
          fieldGroup: [
            {
              className: 'col-2',
              key: 'deviate',
              type: 'checkbox',
              templateOptions: {
                label: 'Deviate Reason',
              },

            },
            {
              className: 'col-3',
              hideExpression: 'model.deviate',
            },
            {
              className: 'col-3',
              key: 'deviateReason',
              type: 'textarea',
              templateOptions: {
                type: 'textarea',
                label: 'Reason',
                required: true,
              },
              hideExpression: '!model.deviate',
            },
            {
              className: 'col-sm-7',
              key: 'area',
              type: 'repeats',
              templateOptions: {
                label: this.currentUser.company.lables.areaLabel,
                btnText: 'Add ' + this.currentUser.company.lables.areaLabel,
              },
              fieldArray: {
                fieldGroupClassName: 'row',
                className: 'row',
                fieldGroup: [
                  {
                    className: 'col-sm-5',
                    type: 'select',
                    key: 'from',
                    templateOptions: {
                      label: 'From:',
                      options: [],
                      //required: true,
                    },
                    lifecycle: {
                      onInit: (form, field) => {

                        console.log("this.currentUser.company : ", this.currentUser.company);
                        if (this.currentUser.userInfo[0].designationLevel == 1) {
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel, [this.currentUser.id]).subscribe(res => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })
                        }
                        else if (this.model.jointWork.length != 0) {
                          var hierarchy = [];
                          for (let n = 0; n < this.model.jointWork.length; n++) {
                            hierarchy.push(this.model.jointWork[n].id)
                          }
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], 1, hierarchy).subscribe((res: any) => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();

                          })
                        } else {
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel, [this.currentUser.id]).subscribe(res => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })
                        }

                      },

                    },
                  },
                  {
                    type: 'select',
                    key: 'to',
                    className: 'col-sm-5',
                    templateOptions: {
                      label: 'To:',
                      options: [],
                      // required: true
                    },
                    // expressionProperties: {
                    //   'templateOptions.required': 'model.workingStatus == "Working"',
                    //   // 'templateOptions.disabled':'model.workingStatus != "Working"'
                    // }, 
                    lifecycle: {
                      onInit: (form, field) => {
                        if (this.currentUser.company.patchWiseReporting == true) {
                          console.log("Hello Ifffffffffff");
                        } else {
                          console.log("Hello Elseeeeeee");
                        }
                        if (this.currentUser.userInfo[0].designationLevel == 1) {
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel, [this.currentUser.id]).subscribe(res => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })
                        } else if (this.model.jointWork.length != 0) {
                          var hierarchy = [];
                          for (let n = 0; n < this.model.jointWork.length; n++) {
                            hierarchy.push(this.model.jointWork[n].id)
                          }
                          this._areaservice.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], 1, hierarchy).subscribe((res: any) => {
                            var a = [];
                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].areaName, 'value': a[0][n].id + "#-#" + a[0][n].areaName });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();

                          })
                        }
                      }
                    },
                  },
                ],
              },
            },]
        },
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-6',
              key: 'remarks',
              type: 'textarea',
              templateOptions: {
                type: 'textarea',
                label: 'Remarks',
                // required: false,
              },
              expressionProperties: {
                'templateOptions.required': 'model.workingStatus.split("#-#")[1] != "1"',
                // 'templateOptions.disabled':'model.workingStatus != "Working"'
              },
            }]
        },
      ],
    },

    // Doctor Model
    {
      label: this.currentUser.company.lables.doctorLabel,
      fields: [
        { template: '<hr />' },
        {

          key: 'doctor',
          type: 'repeats',
          templateOptions: {
            label: 'Add ' + this.currentUser.company.lables.doctorLabel,
            btnText: 'Add More ' + this.currentUser.company.lables.doctorLabel,
            remove: (type: RepeatTypeComponents, i) => {
              Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  // type.remove(i);
                  this._chagedetect.detectChanges();
                  Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )
                }
              })
            }
          },
          hideExpression: () => {
            // access to the main model can be through `this.model` or `formState`
            if (this.model.workingStatus.split("#-#")[1] != "1") {
              return true
            } else {
              return false;
            }

          },
          fieldArray: {
            fieldGroupClassName: 'row',
            className: 'row',
            fieldGroup: [
              {
                className: 'col-sm-6',
                type: 'select',
                key: 'providerId',
                templateOptions: {
                  label: 'Select ' + this.currentUser.company.lables.doctorLabel,
                  options: [],
                  valueProp: 'id',
                  labelProp: 'providerName',
                  multiple: false,
                  change: (field, $event) => {
                    console.log("value=", $event.value);
                    //this.getFocusProductBasisOnDoctor($event.value)
                  }
                  // required: true,
                },
                expressionProperties: {
                  'templateOptions.disabled': 'model.workingStatus=="Working" && model.deviate == false'
                },

                lifecycle: {
                  onInit: (form, field) => {

                    if (this.model.area != null || Object.keys(this.model.area[0]).length != 0) {
                      var a = [];
                      a.push(this.model.area);
                      var areaIds = [];
                      for (var n = 0; n < a[0].length; n++) {
                        areaIds.push(a[0][n].from.split("#-#")[0]);
                        areaIds.push(a[0][n].to.split("#-#")[0]);
                      }

                      this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["RMP"]).subscribe(res => {
                        console.log("this is block id ",res)
                        
                        var a = [];
                        a.push(res)

                        let newDoctorArray = res.map(doc => ({ providerName: doc.providerName, id: `${doc.id}#-#${doc.category}`, areaCode:doc.providerInfo  }));

                        field.templateOptions.options = newDoctorArray;
                        this._chagedetect.detectChanges();

                      })
                      //  this.getDoctorInfos(this.model.area);
                      //   this._chagedetect.detectChanges();
                    } else {

                    }
                  }
                },
              },
              {
                className: 'col-sm-6',
                key: 'jointWorkWithId',
                type: 'select',
                templateOptions: {
                  label: 'Joint Workwith',
                  multiple: true,
                  options: this.JointWorkWithvalues,
                  //  valueProp: 'userId',
                  //   labelProp: 'name',
                  //required: true,
                },
                lifecycle: {
                  onInit: (form, field) => {
                    field.templateOptions.options = this.JointWorkWithvalues;
                    this._chagedetect.detectChanges();
                  }
                }

              },

              {
                className: 'section-label col-sm-6',
                template: '<div><strong><font color="#2471A3">Product Details:</font></strong></div><hr />',
                hideExpression: (field, $event) => {

                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] != "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                },
              },
              {
                className: 'section-label col-sm-6',
                template: '<div><strong><font color="#2471A3">Gift Details:</font></strong></div><hr />',
                hideExpression: (field, $event) => {

                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] != "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                key: 'productDetails',
                className: 'col-6',
                type: 'repeats',
                templateOptions: {
                  label: 'Product',
                  btnText: 'Add Product',
                  removebtnText: 'Del'
                },
                fieldArray: {
                  fieldGroupClassName: 'row',
                  fieldGroup: [
                    {
                      className: 'col-4',
                      type: 'ng-select',
                      key: 'productId',
                      templateOptions: {
                        label: 'Product:',
                        placeholder: 'Select Product',
                        options: [],
                        multiple: false,
                        // valueProp: 'id',
                        // labelProp: 'productName',
                        required: true,
                      },

                      lifecycle: {
                        onInit: (form, field) => {
                          this._productservice.getProductDetailOnStateBasis(this.currentUser.userInfo[0].stateId, this.currentUser.companyId).subscribe(res => {
                            var a = [];
                            console.log("this is block id 2",res)

                            a.push(res)
                            field.templateOptions.options = a[0];
                            this._chagedetect.detectChanges();
                          })
                          let obj = {
                            companyId: this.currentUser.companyId,
                            userId: this.currentUser.userInfo[0].userId,
                            designationLevel: this.currentUser.userInfo[0].designationLevel,
                          }
                          this.userAreaMappingService.getProviderLocationTagged(obj).subscribe(result => {
                          })
                        },
                      },
                    },
                    {
                      className: 'col-3',
                      type: 'input',
                      key: 'productQuantity',
                      templateOptions: {
                        type: 'number',
                        label: 'Sample:',
                        placeholder: 'Qty',
                        
                        required: false,
                      },
                      // expressionProperties: {
                      //   'templateOptions.required': 'model.productId != undefined'
                      // },
                    },
                    {
                      className: 'col-3',
                      type: 'input',
                      key: 'availableStock',
                      templateOptions: {
                        type: 'number',
                        label: 'Order:',
                        placeholder: 'Qty',
                        required: false,
                      },

                    }],
                }
              },
              {
                key: 'giftDetails',
                className: 'col-6',
                type: 'repeats',
                templateOptions: {
                  label: 'Gift',
                  btnText: 'Add Gift',
                  removebtnText: 'Del',
                },
                fieldArray: {
                  fieldGroupClassName: 'row',
                  fieldGroup: [
                    {
                      className: 'col-sm-6',
                      type: 'ng-select',
                      key: 'gift',
                      templateOptions: {
                        label: 'Select Gift:',
                        placeholder: 'Select Gift',
                        options: [],
                        multiple: false,
                        // valueProp: 'id',
                        //  labelProp: 'giftName',
                        required: true,
                      },

                      lifecycle: {
                        onInit: (form, field) => {
                          this._giftService.getGiftDetail(this.currentUser.companyId).subscribe(res => {
                            var a = [];
                            console.log("this is block id 3 ",res)

                            a.push(res)
                            var values = [];
                            for (var n = 0; n < a[0].length; n++) {
                              values.push({ 'label': a[0][n].giftName, 'value': a[0][n]._id + "#-#" + a[0][n].giftName + "#-#" + a[0][n].giftCost });
                            }
                            field.templateOptions.options = values;
                            this._chagedetect.detectChanges();
                          })

                        },
                      },
                    },
                    {
                      className: 'col-sm-3',
                      type: 'input',
                      key: 'giftQty',
                      templateOptions: {
                        type: 'number',
                        label: 'Qty:',
                        placeholder: 'Qty',
                        required: true,
                      },
                      // expressionProperties: {
                      //   'templateOptions.required': 'model.gift != undefined'
                      // },
                    },

                  ],
                }
              },
              {
                className: 'section-label col-sm-12',
                template: '<hr />',
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] != "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                },
              },
              {
                className: 'col-sm-3',
                type: 'input',
                key: 'callObjective',
                templateOptions: {
                  label: 'Call Objective:',
                  options: [],
                  required: false,
                }, hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] != "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-3',
                type: 'input',
                key: 'callOutcomes',
                templateOptions: {
                  label: 'Call Outcomes:',
                  options: [],
                  required: false,
                }, hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] != "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                },
              },

              //--------------MEF Start---------------------------
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'competitorProduct',
                templateOptions: {
                  label: 'Competitor Product He/She is Using:',
                },
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }

                }

              },
              {
                className: 'col-sm-6',
                key: 'productDiscussed',
                type: 'select',
                templateOptions: {
                  label: 'Name of our product discussed',
                  multiple: true,
                  options: [],
                  required: true,
                },
                lifecycle: {
                  onInit: (form, field) => {
                    this._productservice.getProductDetailOnStateBasis(this.currentUser.userInfo[0].stateId, this.currentUser.companyId).subscribe(res => {
                      var a = [];
                      console.log("this is block id 5",res)

                      a.push(res)
                      field.templateOptions.options = a[0];
                      this._chagedetect.detectChanges();
                    })
                    //   this._chagedetect.detectChanges();
                  }
                },
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'problemFacedByDr',
                templateOptions: {
                  label: 'What is the problem Dr. is facing with Comp. Product*',
                },
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'solutionGiven',
                templateOptions: {
                  label: 'What solution I have given by telling our product*',
                }, hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'drFeedback',
                templateOptions: {
                  label: 'What is Drs feedback/query about our product*',
                },
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'section-label col-sm-12',
                template: '<hr />',
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-3',
                type: 'radio',
                key: 'SolutionOnquery',
                templateOptions: {
                  label: 'Have I given him solution to his query?*',
                  options: [
                    {
                      "label": "Yes",
                      "value": true
                    },
                    {
                      "label": "No",
                      "value": false
                    }
                  ]
                },
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'yesOnSolution',
                hideExpression: (field, $event) => {
                  if (field.SolutionOnquery == true) return false
                  else return true
                },
                templateOptions: {
                  label: 'If yes then specified',
                }
              },
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'noOnSolution',
                templateOptions: {
                  label: 'What help I need from senior?',
                },
                hideExpression: (field, $event) => {
                  if (field.SolutionOnquery == false) return false
                  else return true
                }
              },
              {
                className: 'col-sm-3',
                type: 'radio',
                key: 'informedSenior',
                templateOptions: {
                  label: 'Have I informed him?',
                  options: [
                    {
                      "label": "Yes",
                      "value": true
                    },
                    {
                      "label": "No",
                      "value": false
                    }
                  ]
                },
                expressionProperties: {
                  'templateOptions.required': 'model.providerId != undefined'

                },
                hideExpression: (field, $event) => {
                  if (field.SolutionOnquery == false) return false
                  else return true
                }
              },
              {
                className: 'col-sm-3',
                type: 'radio',
                key: 'seniorSolution',
                templateOptions: {
                  label: 'Has Senior given solution?',
                  options: [
                    {
                      "label": "Yes",
                      "value": true
                    },
                    {
                      "label": "No",
                      "value": false
                    }
                  ]
                },
                hideExpression: (field, $event) => {
                  if (field.SolutionOnquery == true) {
                    field.informedSenior = false;
                  }
                  if (field.informedSenior == true) { return false }
                  else if (field.SolutionOnquery == true) { return true }
                  else {
                    return true
                  }
                }
              },
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'solutionGiven',
                templateOptions: {
                  label: 'Solution given'
                },
                hideExpression: (field, $event) => {
                  if (field.SolutionOnquery == true) {
                    field.seniorSolution = false;
                  }

                  if (field.seniorSolution == true) return false
                  else if (field.SolutionOnquery == true) return true
                  else return true
                }
              },
              {
                className: 'section-label col-sm-12',
                template: '<hr />',
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-4',
                type: 'radio',
                key: 'quatationGiven',
                templateOptions: {
                  label: 'Quotation Given?',
                  options: [
                    {
                      "label": "Yes",
                      "value": true
                    },
                    {
                      "label": "No",
                      "value": false
                    }
                  ]
                },
                expressionProperties: {
                  'templateOptions.required': 'model.providerId != undefined'

                },
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-4',
                key: 'quotationDate',
                type: 'input',
                templateOptions: {
                  type: 'date',
                  maxDate: new Date(),
                  label: 'Quotation Date',
                  required: true,
                },
                hideExpression: (field, $event) => {
                  if (field.quatationGiven == true) return false
                  else return true
                }
              },
              {
                className: 'col-sm-4',
                key: 'quotationStatus',
                type: 'select',
                templateOptions: {
                  label: 'Quotation Status',
                  multiple: false,
                  options: [
                    {
                      "value": "pendingApprovalFromStore",
                      "label": "Pending for approval from store"
                    },
                    {
                      "value": "PendingApprovalFromDoctor",
                      "label": "Pending for approval from doctor"
                    },
                    {
                      "value": "Price",
                      "label": "High/Low Price"
                    },
                    {
                      "value": "approvalDone",
                      "label": "Approval Done"
                    },
                    {
                      "value": "others",
                      "label": "Others"
                    }
                  ],
                  required: true,
                },
                hideExpression: (field, $event) => {
                  if (field.quatationGiven == true) return false
                  else return true
                }
              },
              {
                className: 'section-label col-sm-12',
                template: '<hr />',
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                }
              },
              {
                className: 'col-sm-4',
                type: 'radio',
                key: 'conversationDone',
                templateOptions: {
                  label: 'Conversation Done or Not*',
                  options: [
                    {
                      "label": "Yes",
                      "value": true
                    },
                    {
                      "label": "No",
                      "value": false
                    }
                  ]
                },
                hideExpression: (field, $event) => {
                  if (field.providerId) {
                    if (field.providerId.split("#-#")[1] == "MEF") return false;
                    else return true;
                  } else {
                    return true;
                  }
                },
                expressionProperties: {
                  'templateOptions.required': 'model.providerId != undefined'

                }
              },
              {
                className: 'col-sm-6',
                type: 'textarea',
                key: 'notDoneConversationReason',
                templateOptions: {
                  label: 'If Not Specify Reason',
                }, hideExpression: (field, $event) => {
                  if (field.conversationDone == true) return false
                  else return true
                },

              }
              //---------------MEF END----------------------------



            ],

          },

        }
      ],

    },
    // Unlisted Doctor model
    /*{
     label: 'Unlisted Doctor',
     fields: [
       { template: '<hr />' },
       {
         
         key: 'unlisteddoctor',
         type: 'repeat',
         templateOptions: {
         label: 'Doctor Details',
         btnText: 'Add More Doctor',
         },
       
       fieldArray: {
       fieldGroupClassName: 'row',
       className: 'row',
       fieldGroup: [
         {
           className: 'col-sm-4',
           type: 'input',
           key: 'unlisteddoctors',
           templateOptions: {
             label: 'Doctor Name:',
             required: true,
           },
         },
         {
           className: 'col-sm-4',
           type: 'select',
           key: 'degree',
           templateOptions: {
             label: 'degree:',
             required: true,
           },
           lifecycle: {
             onInit: (form, field) => {
               var a=[];
                 if(this.currentUser.company.degree.length>0){
                   for(var n=0;n<this.currentUser.company.degree.length;n++){
                     a.push({"label":this.currentUser.company.degree[n],"value":this.currentUser.company.degree[n]})
                   }
                 }
                 field.templateOptions.options = a;
               
             },
           },
         },
         {
           className: 'col-sm-4',
           type: 'select',
           key: 'specialization',
           templateOptions: {
             label: 'Specialization:',
             required: true,
           },
           lifecycle: {
             onInit: (form, field) => {
               var a=[];
                 if(this.currentUser.company.specialization.length>0){
                   for(var n=0;n<this.currentUser.company.specialization.length;n++){
                     a.push({"label":this.currentUser.company.specialization[n],"value":this.currentUser.company.specialization[n]})
                   }
                 }
                 field.templateOptions.options = a;
               
             },
           },
         },
         {
           className: 'col-sm-4',
           type: 'select',
           key: 'area',
           templateOptions: {
             label: 'Area:',
             options:[],
             valueProp: 'id',
             labelProp: 'areaName',
             required: true,
           },
           lifecycle: {
             onInit: (form, field) => {
               this._areaservice.getAreaList( this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel,[this.currentUser.id]).subscribe(res=>{
                 var a=[];
                 a.push(res)
                 console.log("inside ethod ...")
                 console.log(a)
                 field.templateOptions.options = a[0];
               })
          
             },
             
           },
         },
         {
           className: 'col-sm-4',
           key: 'workingWith',
           type: 'select',
           templateOptions: {
             label: 'Joint Workwith',
             multiple: true,
             options:[],
             valueProp: 'userId',
             labelProp: 'name',
             required: true,
           },
           lifecycle: {
             onInit: (form, field) => {
               this._hierarchy.getManagerHierarchy({companyId:this.currentUser.companyId,supervisorId:this.currentUser.id, type:"upper"}).subscribe(res=>{
                 var a=[];
                 a.push(res)
                 console.log("inside ethod ...")
                 console.log(a)
                 field.templateOptions.options = a[0];
                })
             },
           },
         },
         {
           className: 'section-label col-sm-7',
           template: '<div><strong><font color="#2471A3">Product Details:</font></strong></div><hr />',
         },
         {
           className: 'section-label col-sm-5',
           template: '<div><strong><font color="#2471A3">Gift Details:</font></strong></div><hr />',
         },
         {
           key: 'productinfo',
           className: 'col-7',
           type: 'repeat',
           templateOptions: {
           label: 'Product',
           btnText: 'Add Product',
           removebtnText: 'Del',
           },
           fieldArray: {
           fieldGroupClassName: 'row',
           fieldGroup: [
             {
               className: 'col-4',
               type: 'select',
               key: 'productId',
               templateOptions: {
                 label: 'Product:',
                 placeholder:'Select Product',
                 options:[],
                 valueProp: 'id',
                 labelProp: 'productName',
               // required: true,
               },
               lifecycle: {
                 onInit: (form, field) => {
                   this._productservice.getProductList(this.currentUser.userInfo[0].stateId,this.currentUser.companyId).subscribe(res=>{
                     var a=[];
                     a.push(res)
                     console.log(a[0])
                 
                     field.templateOptions.options = a[0];
                   })
                 },
               },
             },
             {
               className: 'col-3',
               type: 'input',
               key: 'productQuantity',
               templateOptions: {
                 type:'number',
                 label: 'Sample:',
                 placeholder:'Qty',
                 required: true,
               },
             },
             {
               className: 'col-3',
               type: 'input',
               key: 'availableStock',
               templateOptions: {
                 type:'number',
                 label: 'Order:',
                 placeholder:'Qty',
                 required: true,
               },
             },],
           },
         },
         {
         key: 'giftinfo',
         className: 'col-5',
         type: 'repeat',
         templateOptions: {
         label: 'Gift',
         btnText: 'Add Gift',
         removebtnText: 'Del',
       },
       fieldArray: {
       fieldGroupClassName: 'row',
       fieldGroup: [
         {
           className: 'col-sm-6',
           type: 'select',
           key: 'gift',
           templateOptions: {
             label: 'Select Gift:',
             placeholder:'Select Gift',
             options:[],
             valueProp: 'id',
             labelProp: 'giftName',
           // required: true,
           },
           lifecycle: {
             onInit: (form, field) => {
               this._giftService.getGiftDetail(this.currentUser.companyId).subscribe(res=>{
                 var a=[];
                 a.push(res)
                 console.log(a[0])
             
                 field.templateOptions.options = a[0];
               })
               },
             },
         },
         {
           className: 'col-sm-3',
           type: 'input',
           key: 'qty',
           templateOptions: {
             type:'number',
             label: 'Qty:',
             placeholder:'Qty',
             required: true,
                 },
               },
                   
             ],
           },
         }, 
         {
           className: 'section-label col-sm-12',
           template: '<hr />',
         },
         {
           className: 'col-sm-3',
           type: 'input',
           key: 'callobjective',
           templateOptions: {
             label: 'Call Objective:',
             options:[],
             required: false,
           },
         },
         {
           className: 'col-sm-3',
           type: 'input',
           key: 'calloutcomes',  
           templateOptions: {
             label: 'Call Outcomes:',
             options:[],
             required: false,
           },
         },
         {
           className: 'col-sm-2',
           type: 'radio',
           key: 'detailingstatus',
           templateOptions: {
             label: 'Was Detailing Done:',
             options: [
               {
                 "label": "Yes",
                 "value": "Yes"
               },
               {
                 "label": "No",
                 "value": "No"
               }
             ],
   
             required: true,
           },
         },
     {
       className: 'col-sm-3',
       type: 'textarea',
       key: 'doctorremarks',
       templateOptions: {
         label: 'Remarks:',
         required: true,
       },
      }, 
      
     
     ],
     },
   
   },
     ],
     
    },*/
    // Vendor model
    {
      label: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel,
      fields: [
        { template: '<hr />' },
        {

          key: 'vendor',
          type: 'repeats',
          templateOptions: {
            label: 'Add ' + this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel,
            btnText: 'Add More ' + this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel,
            remove: (type: RepeatTypeComponents, i) => {
              Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  //type.remove(i);
                  this._chagedetect.detectChanges();
                  Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )
                }
              })
            },
          },
          hideExpression: () => {
            // access to the main model can be through `this.model` or `formState`
            if (this.model.workingStatus.split("#-#")[1] != "1") {
              return true
            } else {
              return false;
            }

          },
          fieldArray: {
            fieldGroupClassName: 'row',
            className: 'row',
            fieldGroup: [
              {
                className: 'col-sm-4',
                type: 'ng-select',
                key: 'vendorProviderId',
                templateOptions: {
                  label: 'Select ' + this.currentUser.company.lables.vendorLabel + ':',
                  options: [],
                  valueProp: 'id',
                  labelProp: 'providerName',
                  multiple: false
                  // required: true,
                },

                lifecycle: {
                  onInit: (form, field) => {
                    if (this.model.area != null || Object.keys(this.model.area[0]).length != 0) {
                      var a = [];
                      
                      a.push(this.model.area);
                      var areaIds = [];
                      for (var n = 0; n < a[0].length; n++) {
                        areaIds.push(a[0][n].from.split("#-#")[0]);
                        areaIds.push(a[0][n].to.split("#-#")[0]);
                      }
                      this._providerservice.getProviderListBasedOnArea(this.currentUser.companyId, areaIds, ["Drug", "Stockist"]).subscribe(res => {
                        var a = [];
                        a.push(res)
                        field.templateOptions.options = a[0];
                        this._chagedetect.detectChanges();
                      })
                      //  this.getDoctorInfos(this.model.area);
                      //   this._chagedetect.detectChanges();
                    } else {
                    }

                  }
                },
              },
              {
                key: 'productDetails',
                className: 'col-sm-6',
                type: 'repeats',
                templateOptions: {
                  label: 'Product',
                  btnText: 'Add Product',
                  removebtnText: 'Del',
                },
                fieldArray: {
                  fieldGroupClassName: 'row',
                  fieldGroup: [
                    {
                      className: 'col-sm-4',
                      type: 'ng-select',
                      key: 'productId',
                      templateOptions: {
                        label: 'Select Product:',
                        placeholder: 'Select Product',
                        options: [],
                        multiple: false,
                        //  valueProp: 'id',
                        // labelProp: 'productName',
                        required: true,
                      },
                      lifecycle: {
                        onInit: (form, field) => {
                          this._productservice.getProductDetailOnStateBasis(this.currentUser.userInfo[0].stateId, this.currentUser.companyId).subscribe(res => {
                            var a = [];
                            a.push(res)

                            field.templateOptions.options = a[0];
                            this._chagedetect.detectChanges();
                          })
                        },
                      },
                    },
                    {
                      className: 'col-sm-3',
                      type: 'input',
                      key: 'productQuantity',
                      templateOptions: {
                        type: 'number',
                        label: 'Sample Qty:',
                        placeholder: 'Sample',
                        required: true,
                      },

                    },
                    {
                      className: 'col-sm-3',
                      type: 'input',
                      key: 'availableStock',
                      templateOptions: {
                        type: 'number',
                        label: 'Order Qty:',
                        placeholder: 'Order',
                        required: true,
                      },
                      // expressionProperties: {
                      //   'templateOptions.required': 'model.productId != undefined'
                      // },
                    },

                  ],
                },
              },

              {
                className: 'col-sm-4',
                type: 'textarea',
                key: 'remarks',
                templateOptions: {
                  label: 'Remarks:',
                  // required: true,
                },
              },


            ],
          },

        },
      ],

    },
    // unlisted Vendor model
    /*{
      label: 'Unlisted Vendor',
      fields: [
        { template: '<hr />' },
        {
          
          key: 'unlistedvendor',
          type: 'repeat',
          templateOptions: {
          label: 'Add Vendor',
          btnText: 'Add More Vendor',
           
        },
        
        fieldArray: {
        fieldGroupClassName: 'row',
        className: 'row',
        fieldGroup: [
          
          {
            className: 'col-sm-6',
            type: 'input',
            key: 'unlistedvendors',
            templateOptions: {
              label: 'Vendor Name:',
              required: true,
            },
          },
          {
            className: 'col-sm-6',
            type: 'select',
            key: 'area',
            templateOptions: {
              label: 'Area:',
              options:[],
              valueProp: 'id',
              labelProp: 'areaName',
              required: true,
            },
            lifecycle: {
              onInit: (form, field) => {
                this._areaservice.getAreaList( this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel,[this.currentUser.id]).subscribe(res=>{
                  var a=[];
                  a.push(res)
                  console.log("inside ethod ...")
                  console.log(a)
                  field.templateOptions.options = a[0];
                })
            
              },
              
            },
          },
          {
            key: 'unlistedproductinfo',
            className: 'col-sm-12',
            type: 'repeat',
            templateOptions: {
            label: 'Product',
            btnText: 'Add Product',
            removebtnText: 'Del',
          },
          fieldArray: {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-sm-4',
              type: 'select',
              key: 'productId',
              templateOptions: {
                label: 'Select Product:',
                placeholder:'Select Product',
                options:[],
                valueProp: 'id',
                labelProp: 'productName',
               // required: true,
              },
              lifecycle: {
                onInit: (form, field) => {
                  this._productservice.getProductList(this.currentUser.userInfo[0].stateId,this.currentUser.companyId).subscribe(res=>{
                    var a=[];
                    a.push(res)
                    console.log(a[0])
                 
                    field.templateOptions.options = a[0];
                   })
                },
              },
            },
            {
              className: 'col-sm-2',
              type: 'input',
              key: 'productQuantity',
              templateOptions: {
                type: 'number',
                label: 'Sample Qty:',
                placeholder:'Sample',
                required: true,
              },
            },
            {
              className: 'col-sm-2',
              type: 'input',
              key: 'availableStock',
              templateOptions: {
                type: 'number',
                label: 'Order Qty:',
                placeholder:'Order',
                required: true,
              },
            },
            
          ],
        },
      },
      
      {
        className: 'col-sm-4',
        type: 'textarea',
        key: 'vendorremarks',
        templateOptions: {
          label: 'Remarks:',
          required: true,
        },
       }, 
       
      
      ],
      },
    
    },
      ],
      
    },*/
    // Meetings model
    {
      label: 'Meetings',
      fields: [
        { template: '<hr />' },
        {

          key: 'dcrMeetings',
          type: 'repeats',
          templateOptions: {
            label: 'Meetings',
            btnText: 'Add More',
            remove: (type: RepeatTypeComponents, i) => {
              Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  //type.remove(i);
                  this._chagedetect.detectChanges();
                  Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )
                }
              })
            },
          },
          hideExpression: () => {
            // access to the main model can be through `this.model` or `formState`
            let returnValue: boolean = true;
            // switch (this.model.workingStatus) {
            //   case "Working":
            //     returnValue = false;
            //     break;
            //   case "Meeting":
            //     returnValue = false;
            //     break;
            //   default:
            //     returnValue = true;
            //     break;
            // }
            // return returnValue;

            if (this.model.workingStatus.split("#-#")[1] == "1") {
              returnValue = false;
            } else if (this.model.workingStatus.split("#-#")[0] == "Meeting") {
              returnValue = false;
            } else {
              returnValue = true;
            }
            return returnValue;

          },
          fieldArray: {
            fieldGroupClassName: 'row',
            className: 'row',
            fieldGroup: [
              {
                className: 'col-sm-3',
                key: 'meetingWith',
                type: 'ng-select',
                templateOptions: {
                  label: 'Meeting With',
                  multiple: false,
                  options: [],
                  //  valueProp: 'userId',
                  //     labelProp: 'name',
                  // required: true,
                },
                lifecycle: {
                  onInit: (form, field) => {
                    this._hierarchy.getManagerHierarchy({ companyId: this.currentUser.companyId, supervisorId: this.currentUser.id, type: "lowerWithUpper" }).subscribe(res => {
                      var a = [];
                      a.push(res);
                      var values = [];
                      for (var n = 0; n < a[0].length; n++) {
                        values.push({ 'label': a[0][n].name + " (" + a[0][n].designation + ")", 'value': a[0][n].userId });
                      }
                      field.templateOptions.options = values;
                      this._chagedetect.detectChanges();
                      //  field.templateOptions.options = a[0];
                      //  this._chagedetect.detectChanges();
                    })
                  },
                },
              },
              {
                className: 'col-sm-3',
                type: 'textarea',
                key: 'subject',
                templateOptions: {
                  label: 'Subject:',
                  //  required: true,
                },
                expressionProperties: {
                  'templateOptions.required': 'model.meetingWith != undefined'
                },
              },
              {
                className: 'col-sm-4',
                type: 'textarea',
                key: 'remark',
                templateOptions: {
                  label: 'Remark:',
                  // required: true,
                },
                expressionProperties: {
                  'templateOptions.required': 'model.meetingWith != undefined'
                },
              }


            ],
          },

        },

      ],
    },
    // Misc Expense model
    {
      label: 'Misc Expenses',
      fields: [
        { template: '<hr />' },
        {

          key: 'dcrExpense',
          type: 'repeats',
          templateOptions: {
            label: 'Misc Expenses',
            btnText: 'Add More',
            remove: (type: RepeatTypeComponents, i) => {
              Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  //type.remove(i);
                  this._chagedetect.detectChanges();
                  Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )
                }
              })
            },

          },
          hideExpression: () => {
            // access to the main model can be through `this.model` or `formState`
            if (this.model.workingStatus.split("#-#")[1] != "1") {
              return true
            } else {
              return false;
            }

          },
          fieldArray: {
            fieldGroupClassName: 'row',
            className: 'row',
            fieldGroup: [
              {
                className: 'col-sm-4',
                key: 'expenseType',
                type: 'ng-select',
                templateOptions: {
                  label: 'Expense Type',
                  options: [
                    { label: 'Internet', value: 'Internet' },
                    { label: 'Courier', value: 'Courier' },
                    { label: 'Fax', value: 'Fax' },
                    { label: 'Mobile', value: 'Mobile' },
                    { label: 'Other', value: 'Other' },
                  ],
                  multiple: false
                  // required: true,
                },
              },
              {
                className: 'col-sm-3',
                type: 'input',
                key: 'amount',
                templateOptions: {
                  label: 'Amount:',
                  type: 'number',
                  //  required: true,
                },
                expressionProperties: {
                  'templateOptions.required': 'model.expenseType != undefined'
                },
              },
              {
                className: 'col-sm-3',
                type: 'textarea',
                key: 'remark',
                templateOptions: {
                  label: 'Remark:',
                  //  required: true,
                },
                expressionProperties: {
                  'templateOptions.required': 'model.expenseType != undefined'
                },
              },


            ],
          },

        },

      ],
    },

    {
      label: 'Preview',
      fields: []
    }
  ];
  form = new FormArray(this.steps.map(() => new FormGroup({})));
  options = this.steps.map(() => <FormlyFormOptions>{});
  prevStep(step) {
    this.activedStep = step - 1;

  }

  nextStep(step, stepper: MatStepper) {
    //  this.activedStep = step + 1;
    if (step == 0) {
      let currentDate = formatDate(new Date(), 'yyyy/MM/dd', 'en');
      let dcrDate = formatDate(this.model.date, 'yyyy/MM/dd', 'en');
      var diff = Math.floor(new Date().getTime() - new Date(this.model.date).getTime());
      var day = 1000 * 60 * 60 * 24;
      var days: number = Math.floor(diff / day);

      //  console.log(currentDate);
      if (currentDate < dcrDate || days > this.currentUser.userInfo[0].lockingPeriod) {
        if (currentDate < dcrDate) {

          Swal.fire({
            type: 'error',
            title: 'Advance DCR is not allowed !',
            allowOutsideClick: false,
            // text: 'Advance DCR is not allowed !',
          })

        } else if (days > this.currentUser.userInfo[0].lockingPeriod) {
          Swal.fire({
            type: 'error',
            title: 'DCR has been locked !',
            // text: 'Advance DCR is not allowed !',
            allowOutsideClick: false
          })
        }


      } else {

        Swal.fire({
          title: 'Are you sure?',
          text: "You want to Continue...",
          type: 'warning',
          showCancelButton: true,
          allowOutsideClick: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Continue.. it!'
        }).then((result) => {
          if (result.value) {
            this.JointWorkWithvalues = [];
            if (this.model['workingStatus'].split("#-#")[1] == "1") { //1 is for Working type DCR

              this.getProviderInfos(this.model.area, ['RMP']);
              this.getProviderInfos(this.model.area, ['Drug']);
              var workingwith = [];
              if (this.model.jointWork != undefined || this.model.jointWork.length != 0 || Object.keys(this.model.jointWork[0]).length === 0) {

                for (var n = 0; n < this.model.jointWork.length; n++) {
                  this.JointWorkWithvalues.push({ 'label': this.model.jointWork[n].Name, 'value': { id: this.model.jointWork[n].id, Name: this.model.jointWork[n].Name } });
                  workingwith.push({ id: this.model.jointWork[n].id, Name: this.model.jointWork[n].Name })
                }

              }
            }

            this.model = {
              date: this.model.date,
              workingStatus: this.model.workingStatus,
              remarks: this.model.remarks,
              area: this.model.area,
              deviate: false,
              deviateReason: this.model.deviateReason,
              jointWork: this.model.jointWork,
              //doctor: [{ 'providerId': [{}] }],
              doctor: [],
              vendor: [{ 'vendorProviderId': [{}] }],
              dcrMeetings: [{}],
              dcrExpense: [{}]
            }

            this.resultArr = [];
            this._dcrReportsService.getUserCompleteDCROnDateBasis("" + this.model.date, this.currentUser.id).subscribe(res => {

              this.resultArr.push(res);

              var visitedBlock = [];
              var areas = [];
              var dcrObject = [];
              var obj = {};
              if (this.resultArr[0].length > 0) {
                sessionStorage.removeItem("dcrId");
                sessionStorage.removeItem("dcrStatus");
                sessionStorage.setItem('dcrStatus', '1');
                sessionStorage.setItem("dcrId", this.resultArr[0][0].id);
                if (this.resultArr[0][0].dcrMeetings != undefined) {
                  this.model['dcrMeetings'] = this.resultArr[0][0].dcrMeetings;
                }
                if (this.resultArr[0][0].dcrExpense != undefined) {
                  this.model.dcrExpense = this.resultArr[0][0].dcrExpense;
                }

                if (this.model['workingStatus'].split("#-#")[1] == "1") { //1 is for Working type DCR
                  if (this.resultArr[0][0].info != undefined || this.resultArr[0][0].info.length > 0) {
                    let doctorDetails = [];
                    let vendorDetails = [];
                    for (var i = 0; i < this.resultArr[0][0].info.length; i++) {
                      let productList = [];
                      let giftList = [];
                      if (this.resultArr[0][0].info[i].providerType == "RMP") {
                        if (this.resultArr[0][0].info[i].hasOwnProperty("mefAnswers")) {

                          // if (this.resultArr[0][0].info[i].mefAnswers.productDiscussed.length > 0) {
                          //   for (var n = 0; n < this.resultArr[0][0].info[i].mefAnswers.productDiscussed.length; n++) {
                          // //    let productObj = {};
                          // console.log(this.resultArr[0][0].info[i].mefAnswers.productDetails[n].productId + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productName + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productPrice);

                          //    // this.resultArr[0][0].info[i].productDetails[n].productId + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productName + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productPrice;
                          //    // productList.push(this.resultArr[0][0].info[i].mefAnswers.productDetails[n].productId + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productName + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productPrice)
                          //   }
                          // }
                          let xyz = this.resultArr[0][0].info[i];
                          for (var n = 0; n < this.resultArr[0][0].info[i].mefAnswers.productDiscussed.length; n++) {

                            this._providerservice.getProductDetails({ where: { id: this.resultArr[0][0].info[i].mefAnswers.productDiscussed[n] } }).subscribe(res => {

                              productList.push(res[0].id + "#-#" + res[0].productName + "#-#" + res[0].ptr)

                            });
                          }

                          doctorDetails.push({
                            "providerId": this.resultArr[0][0].info[i].providerId + "#-#MEF",
                            "jointWorkWithId": this.resultArr[0][0].info[i].jointWorkWithId,
                            "productDiscussed": productList,
                            competitorProduct: this.resultArr[0][0].info[i].mefAnswers.competitorProduct,
                            conversationDone: this.resultArr[0][0].info[i].mefAnswers.conversionDone,
                            notDoneConversationReason: this.resultArr[0][0].info[i].mefAnswers.conversionNoReason,
                            drFeedback: this.resultArr[0][0].info[i].mefAnswers.doctorFeedback,
                            problemFacedByDr: this.resultArr[0][0].info[i].mefAnswers.drProductProblem,
                            informedSenior: this.resultArr[0][0].info[i].mefAnswers.isInformToSenior,
                            SolutionOnquery: this.resultArr[0][0].info[i].mefAnswers.isQuerySolnGiven,
                            quotationDate: this.resultArr[0][0].info[i].mefAnswers.quoteDate,
                            quatationGiven: this.resultArr[0][0].info[i].mefAnswers.quoteGiven,
                            quotationStatus: this.resultArr[0][0].info[i].mefAnswers.quoteStatus,
                            seniorHelp: this.resultArr[0][0].info[i].mefAnswers.noOnSolution,
                            solutionGiven: this.resultArr[0][0].info[i].mefAnswers.problemSolnToDr,
                            yesOnSolution: this.resultArr[0][0].info[i].mefAnswers.querySoln,
                          })




                        } else {
                          if (this.resultArr[0][0].info[i].productDetails.length > 0) {
                            for (var n = 0; n < this.resultArr[0][0].info[i].productDetails.length; n++) {
                              let productObj = {};
                              productObj['productQuantity'] = this.resultArr[0][0].info[i].productDetails[n].productQuantity;
                              productObj['availableStock'] = this.resultArr[0][0].info[i].productDetails[n].availableStock;
                              productObj['productId'] = this.resultArr[0][0].info[i].productDetails[n].productId + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productName + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productPrice;
                              productList.push(productObj)
                            }
                          }
                          if (this.resultArr[0][0].info[i].giftDetails.length > 0) {
                            for (var n = 0; n < this.resultArr[0][0].info[i].giftDetails.length; n++) {
                              let giftObj = {};
                              giftObj['gift'] = this.resultArr[0][0].info[i].giftDetails[n].giftId + "#-#" + this.resultArr[0][0].info[i].giftDetails[n].giftName + "#-#" + this.resultArr[0][0].info[i].giftDetails[n].giftPrice
                              giftObj['giftQty'] = this.resultArr[0][0].info[i].giftDetails[n].giftQty
                              giftList.push(giftObj)
                            }
                          }

                          let alreadySubmittedDetail = this.resultArr[0][0].info[i];
                          this._providerservice.getProviderCategory({ where: { id: this.resultArr[0][0].info[i].providerId } }).subscribe(res => {


                            doctorDetails.push({
                              "providerId": alreadySubmittedDetail.providerId + "#-#" + res[0].category,
                              "jointWorkWithId": alreadySubmittedDetail.jointWorkWithId,
                              "productDetails": productList,
                              "giftDetails": giftList,
                              "detailingDone": alreadySubmittedDetail.detailingDone,
                              "callOutcomes": alreadySubmittedDetail.callOutcomes,
                              "callObjective": alreadySubmittedDetail.callObjective,
                              "remarks": alreadySubmittedDetail.remarks
                            })
                          })

                        }

                      } else {
                        if (this.resultArr[0][0].info[i].productDetails.length > 0) {
                          for (var n = 0; n < this.resultArr[0][0].info[i].productDetails.length; n++) {
                            let productObj = {};
                            productObj['productQuantity'] = this.resultArr[0][0].info[i].productDetails[n].productQuantity;
                            productObj['availableStock'] = this.resultArr[0][0].info[i].productDetails[n].availableStock;
                            productObj['productId'] = this.resultArr[0][0].info[i].productDetails[n].productId + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productName + "#-#" + this.resultArr[0][0].info[i].productDetails[n].productPrice;
                            productList.push(productObj)

                          }

                        }
                        vendorDetails.push({
                          "vendorProviderId": this.resultArr[0][0].info[i].providerId,
                          "productDetails": productList,
                          "detailingDone": false,
                          "remarks": this.resultArr[0][0].info[i].remarks,
                        })
                      }
                    }


                    this.model.doctor = doctorDetails;
                    this.model.vendor = vendorDetails;
                  }
                }
                // let areas=[];
                // var visitedBlock=[];
                if (Object.keys(this.model['area']).length > 0) {
                  areas.push(this.model['area']);
                  for (var n = 0; n < areas[0].length; n++) {

                    if (areas[0][n].from !== undefined) {
                      visitedBlock.push({
                        "from": areas[0][n].from.split("#-#")[1],
                        "to": areas[0][n].to.split("#-#")[1],
                        "fromId": areas[0][n].from.split("#-#")[0],
                        "toId": areas[0][n].to.split("#-#")[0],
                      });
                    }

                  }

                }
                if (this.model.deviate == true) {
                  obj['deviateReason'] = this.model['deviateReason'];
                }
                obj['visitedBlock'] = visitedBlock;
                obj['workingStatus'] = this.model.workingStatus.split("#-#")[0];//this.model['workingStatus'];
                if (this.model['jointWork'] !== undefined || this.model['jointWork'].length > 0) {
                  obj['jointWork'] = this.model['jointWork'];
                } else {
                  obj['jointWork'] = [];
                }
                if (this.model['remarks'] !== undefined) {
                  obj['remarks'] = this.model['remarks'];
                } else {
                  obj['remarks'] = "";
                }
                // obj['timeIn'] =  formatDate(new Date(), 'YYYY-MM-DDThh:mm:ss.sTZD', 'en-US', '+0530');
                obj['dcrModificationDate'] = new Date();

                dcrObject.push(obj);

                this._dcrReportsService.submitDCR(obj, step, sessionStorage.getItem("dcrId")).subscribe(res => {
                  if (this.model['workingStatus'].split("#-#")[1] != "1" && this.model['workingStatus'].split("#-#")[0] != "Meeting") {
                    stepper.selectedIndex = 5;
                  } else if (this.model['workingStatus'].split("#-#")[0] == "Meeting") {
                    stepper.selectedIndex = 3;
                  } else {
                    stepper.selectedIndex = step + 1;
                  }
                })


              } else {
                //this._dcrReportsService.submitDCR(this.currentUser,this.model,step);
                if (this.model['workingStatus'].split("#-#")[1] == "1") { //1 is for Working type DCR
                  if (Object.keys(this.model['area']).length > 0) {

                    areas.push(this.model['area']);
                    for (var n = 0; n < areas[0].length; n++) {
                      if (areas[0][n].from !== undefined) {
                        visitedBlock.push({
                          "from": areas[0][n].from.split("#-#")[1],
                          "to": areas[0][n].to.split("#-#")[1],
                          "fromId": areas[0][n].from.split("#-#")[0],
                          "toId": areas[0][n].to.split("#-#")[0],
                        });
                      }

                    }
                  }
                }
                if (this.model['deviateReason'] != "" && this.model['deviateReason'] != undefined && this.model['deviateReason'] != null) {
                  obj['deviateReason'] = this.model['deviateReason'];
                }

                obj['companyId'] = this.currentUser['companyId'];
                obj['stateId'] = this.currentUser['userInfo'][0].stateId;
                obj['districtId'] = this.currentUser['userInfo'][0].districtId;
                obj['submitBy'] = this.currentUser['id'];
                obj['dcrDate'] = new Date(this.model['date']);
                obj['visitedBlock'] = visitedBlock;
                obj['workingStatus'] = this.model['workingStatus'].split("#-#")[0];
                if (this.model['jointWork'] !== undefined || this.model['jointWork'].length > 0) {
                  obj['jointWork'] = this.model['jointWork'];
                } else {
                  obj['jointWork'] = [];
                }
                if (this.model['remarks'] !== undefined) {
                  obj['remarks'] = this.model['remarks'];
                } else {
                  obj['remarks'] = "";
                }
                // obj['timeIn'] =  formatDate(new Date(), 'YYYY-MM-DDThh:mm:ss.sTZD', 'en-US', '+0530');
                obj['dcrSubmissionDate'] = new Date();
                obj['punchDate'] = moment.utc(new Date()).format("YYYY-MM-DD");
                obj['timeIn'] = new Date();
                obj['callModifiedDate'] = new Date();
                obj['timeOut'] = new Date();
                obj['reportedFrom'] = "Web";
                obj['DCRStatus'] = parseInt(this.model['workingStatus'].split("#-#")[1]);
                obj['dcrVersion'] = 'draft';
                obj['isDayPlan'] = false;

                obj['geoLocation'] = [];

                dcrObject.push(obj);
                this._dcrReportsService.submitDCR(dcrObject, step, "0").subscribe(res => {
                  sessionStorage.removeItem("dcrId");
                  sessionStorage.removeItem("dcrStatus");
                  sessionStorage.setItem("dcrId", res[0].id);

                  if (this.model['workingStatus'].split("#-#")[1] != "1" && this.model['workingStatus'].split("#-#")[0] != "Meeting") {
                    this.openDialog(stepper);
                    stepper.selectedIndex = 5;
                  } else if (this.model['workingStatus'].split("#-#")[0] == "Meeting") {
                    stepper.selectedIndex = 3;
                  } else {
                    stepper.selectedIndex = step + 1;
                  }
                })
              }

              Swal.fire(
                'Saved!',
                'Your Record has been Saved...',
                'success'
              )
            })

          }

        })
      }


    } else if (step == 1) {

      //this.getUserCompleteDCRDetails("5c25e869b5156b26a87a7bcd")
      
      
      if (this.model['doctor'][0] != undefined && this.model['doctor'][0].hasOwnProperty('providerId') == true) {
        console.log('GOURAV---------->',this.model,'this.blockIds',this.blockIds)
        if (Object.keys(this.model['doctor'][0]['providerId'][0]).length > 0) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to Continue...",
            type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Save it!'
          }).then((result) => {
            if (result.value) {
              var doctorDcrObj = [];
              for (var n = 0; n < this.model['doctor'].length; n++) {
                let doctorDcrObject = {};
                let productInfo = [];
                let giftInfo = [];
                let giftObj = {};
                let productObj = {};
                let sampleQty = 0;
                let purchasedQty = 0;
                let productPob = 0;
                let mefAnswers = {};
                let discussedProduct = [];
                doctorDcrObject['blockId'] = this.blockIds;//this.model['blockId'];   // for kaam chalana
                doctorDcrObject['companyId'] = this.currentUser['companyId'];
                doctorDcrObject['dcrId'] = sessionStorage.getItem("dcrId");
                doctorDcrObject['dcrDate'] = new Date(this.model['date']);
                doctorDcrObject['detailingDone'] = this.model['doctor'][n]['detailingDone'];
                doctorDcrObject['isOutsideTp'] = false;
                doctorDcrObject['isDayPlan'] = false;
                doctorDcrObject['providerId'] = this.model['doctor'][n]['providerId'].toString().split("#-#")[0];
                doctorDcrObject['stateId'] = this.currentUser['userInfo'][0].stateId;
                doctorDcrObject['districtId'] = this.currentUser['userInfo'][0].districtId;
                doctorDcrObject['submitBy'] = this.currentUser['id'];

                if (this.model['doctor'][n]['providerId'].toString().split("#-#")[1] === "MEF") {
                  for (let h = 0; h < this.model['doctor'][n]['productDiscussed'].length; h++) {
                    console.log("product discussed->", this.model['doctor'][n]['productDiscussed']);

                    discussedProduct.push(this.model['doctor'][n]['productDiscussed'][h].split("#-#")[0])
                  }
                }

                mefAnswers = {
                  productDiscussed: discussedProduct,
                  "competitorProduct": this.model['doctor'][n]['competitorProduct'],
                  "drProductProblem": this.model['doctor'][n]['problemFacedByDr'],
                  "problemSolnToDr": this.model['doctor'][n]['solutionGiven'],
                  "doctorFeedback": this.model['doctor'][n]['drFeedback'],
                  "isQuerySolnGiven": this.model['doctor'][n]['SolutionOnquery'],
                  "querySoln": this.model['doctor'][n]['yesOnSolution'],
                  "seniorHelp": this.model['doctor'][n]['noOnSolution'],
                  "isInformToSenior": this.model['doctor'][n]['informedSenior'],
                  "conversionDone": this.model['doctor'][n]['conversationDone'],
                  "conversionNoReason": this.model['doctor'][n]['notDoneConversationReason'],
                  "quoteGiven": this.model['doctor'][n]['quatationGiven'],
                  "quoteStatus": this.model['doctor'][n]['quotationStatus'],
                  "quoteDate": this.model['doctor'][n]['quotationDate'],
                }
                if (this.model['doctor'][n]['providerId'].toString().split("#-#")[1] === "MEF") {
                  doctorDcrObject['mefAnswers'] = mefAnswers

                }
                if (this.model['doctor'][n]['remarks'] !== undefined) {
                  doctorDcrObject['remarks'] = this.model['doctor'][n]['remarks'];
                }
                if (this.model['doctor'][n]['callObjective'] !== undefined) {
                  doctorDcrObject['callObjective'] = this.model['doctor'][n]['callObjective'];
                }
                if (this.model['doctor'][n]['callOutcomes'] !== undefined) {
                  doctorDcrObject['callOutcomes'] = this.model['doctor'][n]['callOutcomes'];
                }
                if (this.model['doctor'][n].hasOwnProperty('productDetails')) {
                  if (this.model['doctor'][n]['productDetails'].length > 0 || this.model['doctor'][n]['productDetails'][0] !== undefined) {
                    for (let h = 0; h < this.model['doctor'][n]['productDetails'].length; h++) {
                      let productQty = this.model['doctor'][n]['productDetails'][h]['productQuantity'];
                      let availableStk = this.model['doctor'][n]['productDetails'][h]['availableStock'];

                      // this._productservice.getProductInfoOnProductIdBasis(this.model['doctor'][n]['productinfo'][h].productId).subscribe(response=>{
                      productObj = {
                        "productName": this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[1],
                        "productId": this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[0],
                        "productQuantity": productQty,
                        "availableStock": availableStk,
                        "productPrice": this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2],
                        "pob": availableStk * parseInt(this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2])
                      }
                      productInfo.push(productObj)
                      productPob = productPob + availableStk * parseInt(this.model['doctor'][n]['productDetails'][h]['productId'].split("#-#")[2]);
                      doctorDcrObject['productPob'] = productPob;
                      //})
                      sampleQty = sampleQty + this.model['doctor'][n]['productDetails'][h]['productQuantity'];
                      purchasedQty = purchasedQty + this.model['doctor'][n]['productDetails'][h]['availableStock'];
                      //  productInfo.push(productObj)
                    }
                    doctorDcrObject['productDetails'] = productInfo;
                  } else {
                    doctorDcrObject['productDetails'] = [];
                  }
                } else {
                  doctorDcrObject['productDetails'] = [];
                }

                doctorDcrObject['sampleQty'] = sampleQty;
                doctorDcrObject['purchasedQty'] = purchasedQty;

                if (this.model['doctor'][n].hasOwnProperty('giftDetails')) {
                  if (this.model['doctor'][n]['giftDetails'].length > 0 || this.model['doctor'][n]['giftDetails'][0] !== undefined) {
                    for (let h = 0; h < this.model['doctor'][n]['giftDetails'].length; h++) {
                      giftObj = {
                        "giftId": this.model['doctor'][n]['giftDetails'][h]['gift'].split("#-#")[0],
                        "giftName": this.model['doctor'][n]['giftDetails'][h]['gift'].split("#-#")[1],
                        "giftQty": this.model['doctor'][n]['giftDetails'][h]['giftQty'],
                        "giftPrice": this.model['doctor'][n]['giftDetails'][h]['gift'].split("#-#")[2],
                      }
                      giftInfo.push(giftObj)
                    }
                    doctorDcrObject['giftDetails'] = giftInfo;
                    //  doctorDcrObject['giftDetails']=this.model['doctor'][n]['giftDetails'];
                  } else {
                    doctorDcrObject['giftDetails'] = [];
                  }

                } else {
                  doctorDcrObject['giftDetails'] = [];
                }

                if (this.model['doctor'][n].hasOwnProperty('jointWorkWithId')) {
                  doctorDcrObject['jointWorkWithId'] = this.model['doctor'][n]['jointWorkWithId'];
                } else {
                  doctorDcrObject['jointWorkWithId'] = [];
                }
                doctorDcrObject['providerType'] = 'RMP';
                /* this._providerservice.getProvidersBlockId(""+this.model['doctor'][n]['doctors']).subscribe(res=>{
                   doctorDcrObject['blockId']=res[0]['blockId'];
                 })*/
                doctorDcrObject['providerId'] = doctorDcrObject['providerId'].split("#-#")[0];

                doctorDcrObj.push(doctorDcrObject)
              }
              if (sessionStorage.getItem('dcrStatus') == '1') {

                this._dcrReportsService.deleteDCRProviderVisitDetail(sessionStorage.getItem('dcrId'), ['RMP']).subscribe(res => {



                  this._dcrReportsService.submitDCR(doctorDcrObj, step, sessionStorage.getItem("dcrId")).subscribe(res => {
                  })
                }, err => {
                  console.log("Error : ", err)
                })



              } else {
                this._dcrReportsService.submitDCR(doctorDcrObj, step, sessionStorage.getItem("dcrId")).subscribe(res => {
                })
              }


              stepper.selectedIndex = step + 1;
              Swal.fire(
                'Saved!',
                'Your Record has been Saved...',
                'success'
              )
            }
          })


        } else {
          stepper.selectedIndex = step + 1;
        }
      } else {
        this._dcrReportsService.deleteDCRProviderVisitDetail(sessionStorage.getItem('dcrId'), ['RMP']).subscribe(res => {
        }, err => {
          console.log("Error : ", err)
        })
        stepper.selectedIndex = step + 1;
      }

    } else if (step == 2) {

      console.log("this.model-2", this.model);
      if (this.model['vendor'][0] != undefined && this.model['vendor'][0].hasOwnProperty('vendorProviderId') == true) {
        if (Object.keys(this.model['vendor'][0]['vendorProviderId'][0]).length > 0) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to Continue...",
            type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Save it!'
          }).then((result) => {
            if (result.value) {
              var vendorDcrObj = [];
              for (var n = 0; n < this.model['vendor'].length; n++) {
                let vendorDcrObject = {};
                let productInfo = [];
                let productObj = {};
                let sampleQty = 0;
                let purchasedQty = 0;
                let productPob = 0;
                vendorDcrObject['blockId'] = "123";
                //    console.log(doctorDcrObject['blockId'])
                vendorDcrObject['companyId'] = this.currentUser['companyId'];
                vendorDcrObject['dcrId'] = sessionStorage.getItem("dcrId");
                vendorDcrObject['dcrDate'] = new Date(this.model['date']);
                vendorDcrObject['isOutsideTp'] = false;
                vendorDcrObject['isDayPlan'] = false;
                vendorDcrObject['providerId'] = this.model['vendor'][n]['vendorProviderId'];
                vendorDcrObject['stateId'] = this.currentUser['userInfo'][0].stateId;
                vendorDcrObject['districtId'] = this.currentUser['userInfo'][0].districtId;
                vendorDcrObject['submitBy'] = this.currentUser['id'];
                vendorDcrObject['detailingDone'] = false;
                vendorDcrObject['jointWorkWithId'] = [];

                if (this.model['vendor'][n]['remarks'] !== undefined) {
                  vendorDcrObject['remarks'] = this.model['vendor'][n]['remarks'];
                }
                if (this.model['vendor'][n].hasOwnProperty('productDetails')) {
                  if (this.model['vendor'][n]['productDetails'].length > 0 || this.model['vendor'][n]['productDetails'][0] != undefined) {
                    for (let h = 0; h < this.model['vendor'][n]['productDetails'].length; h++) {
                      let productQty = this.model['vendor'][n]['productDetails'][h]['productQuantity'];
                      let availableStk = this.model['vendor'][n]['productDetails'][h]['availableStock'];

                      // this._productservice.getProductInfoOnProductIdBasis(this.model['doctor'][n]['productinfo'][h].productId).subscribe(response=>{
                      productObj = {
                        "productName": this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[1],
                        "productId": this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[0],
                        "productQuantity": productQty,
                        "availableStock": availableStk,
                        "productPrice": this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2],
                        "pob": availableStk * parseInt(this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2])
                      }
                      productInfo.push(productObj)
                      productPob = productPob + availableStk * parseInt(this.model['vendor'][n]['productDetails'][h]['productId'].split("#-#")[2]);
                      vendorDcrObject['productPob'] = productPob;
                      //})
                      sampleQty = sampleQty + this.model['vendor'][n]['productDetails'][h]['productQuantity'];
                      purchasedQty = purchasedQty + this.model['vendor'][n]['productDetails'][h]['availableStock'];
                      //  productInfo.push(productObj)
                    }
                    vendorDcrObject['productDetails'] = productInfo;
                  } else {
                    vendorDcrObject['productDetails'] = [];
                  }
                }

                vendorDcrObject['sampleQty'] = sampleQty;
                vendorDcrObject['purchasedQty'] = purchasedQty;

                vendorDcrObject['providerType'] = 'Drug';

                vendorDcrObj.push(vendorDcrObject)
              }
              if (sessionStorage.getItem('dcrStatus') == '1') {
                this._dcrReportsService.deleteDCRProviderVisitDetail(sessionStorage.getItem('dcrId'), ['Drug', 'Stockist']).subscribe(res => {
                  this._dcrReportsService.submitDCR(vendorDcrObj, step, sessionStorage.getItem("dcrId")).subscribe(res => {
                  })
                }, err => {
                  console.log("Error : ", err)
                })
              } else {
                this._dcrReportsService.submitDCR(vendorDcrObj, step, sessionStorage.getItem("dcrId")).subscribe(res => {
                })
              }


              stepper.selectedIndex = step + 1;
              Swal.fire(
                'Saved!',
                'Your Record has been Saved...',
                'success'
              )
            }
          })


        } else {
          stepper.selectedIndex = step + 1;
        }
      } else {
        this._dcrReportsService.deleteDCRProviderVisitDetail(sessionStorage.getItem('dcrId'), ['Drug', 'Stockist']).subscribe(res => {

        }, err => {
          console.log("Error : ", err)
        })
        stepper.selectedIndex = step + 1;
      }
    } else if (step == 3) {
      console.log("this.model-3", this.model);
      let dcrMeetingInfo = [];
      if (this.model['dcrMeetings'][0] != undefined && this.model['dcrMeetings'][0].hasOwnProperty('meetingWith') == true) {
        if (Object.keys(this.model['dcrMeetings'][0]['meetingWith'][0]).length > 0) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to Continue...",
            type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Save it!'
          }).then((result) => {
            if (result.value) {

              for (var n = 0; n < this.model['dcrMeetings'].length; n++) {
                let dcrMeetingsDetails = {};
                dcrMeetingsDetails['id'] = "" + n;
                dcrMeetingsDetails['meetingWith'] = this.model['dcrMeetings'][n]['meetingWith'];
                dcrMeetingsDetails['subject'] = this.model['dcrMeetings'][n]['subject'];
                dcrMeetingsDetails['remark'] = this.model['dcrMeetings'][n]['remark'];
                dcrMeetingInfo.push(dcrMeetingsDetails)
              }
              this._dcrReportsService.submitDCR(dcrMeetingInfo, step, sessionStorage.getItem("dcrId")).subscribe(res => { })

              stepper.selectedIndex = step + 1;
              Swal.fire(
                'Saved!',
                'Your Record has been Saved...',
                'success'
              )
            }
          })
        } else {
          dcrMeetingInfo.push([])
          this._dcrReportsService.submitDCR(dcrMeetingInfo, step, sessionStorage.getItem("dcrId")).subscribe(res => { })
          stepper.selectedIndex = step + 1;

        }
      } else {
        stepper.selectedIndex = step + 1;
      }
    } else if (step == 4) {
      console.log("this.model-4", this.model);
      let dcrExpenseInfo = [];
      if (this.model['dcrExpense'].length > 0) {

        if (this.model['dcrExpense'][0]['expenseType'] != undefined && Object.keys(this.model['dcrExpense'][0]['expenseType']).length > 0) {

          for (var n = 0; n < this.model['dcrExpense'].length; n++) {
            let dcrExpenseDetails = {};
            dcrExpenseDetails['id'] = "" + n;
            dcrExpenseDetails['expenseType'] = this.model['dcrExpense'][n]['expenseType'];
            dcrExpenseDetails['amount'] = this.model['dcrExpense'][n]['amount'];
            dcrExpenseDetails['remark'] = this.model['dcrExpense'][n]['remark'];
            dcrExpenseInfo.push(dcrExpenseDetails)
          }
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to Continue...",
            type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Save it!'
          }).then((result) => {
            if (result.value) {
              this._dcrReportsService.submitDCR(dcrExpenseInfo, step, sessionStorage.getItem("dcrId")).subscribe(res => {
                this.openDialog(stepper);
              })

              stepper.selectedIndex = step + 1;
              Swal.fire(
                'Saved!',
                'Your Record has been Saved...',
                'success'
              )

            }
          })
        } else {
          dcrExpenseInfo.push([])
          this._dcrReportsService.submitDCR(dcrExpenseInfo, step, sessionStorage.getItem("dcrId")).subscribe(res => { this.openDialog(stepper); })

          stepper.selectedIndex = step + 1;
        }
      } else {
        this.openDialog(stepper);
        stepper.selectedIndex = step + 1;
      }

    }
  }

  submit(stepper: MatStepper) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to finally Save it...",
      type: 'warning',
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Save it!'
    }).then((result) => {
      if (result.value) {
        // 11 is a random number for final submission. 
        this._dcrReportsService.submitDCR({ 'dcrVersion': 'complete' }, 11, sessionStorage.getItem("dcrId")).subscribe(res => { });

        sessionStorage.removeItem("dcrId");
        sessionStorage.removeItem("dcrStatus");
        stepper.selectedIndex = 0;
        this.showDCRSummary = true;
        Swal.fire(
          'Saved!',
          'Your DCR has been Submitted Successfully...',
          'success'
        )
      }
    })
    this.model = {
      date: new Date(),
      workingStatus: "",
      remarks: '',
      deviate: false,
      deviateReason: "",
      area: [{}],
      jointWork: [],
      doctor: [{ 'providerId': [{}] }],
      vendor: [{ 'vendorProviderId': [{}] }],
      dcrMeetings: [{}],
      dcrExpense: [{}]
      // workingWith: this.callBackWorkingOptions
    };

    this.form.reset();
    //this.form.setErrors(null);


  }
  resetAll() {
    this.options.forEach(option => option.resetModel());
  }
  getField(fields, key: string): FormlyFieldConfig {
    for (let i = 0, len = fields.length; i < len; i++) {

      if (fields[i].key === key) {
        return fields[i];
      }
      if (fields[i].fieldGroup && fields[i].fieldGroup.length > 0) {
        return this.getField(fields[i].fieldGroup, key);
      }
    }
  }
  // getFields is used t get manager's area
  getFields(fields, key: string, index: number): FormlyFieldConfig {
    for (let i = index, len = fields.length; i < len; i++) {

      if (fields[i].key === key) {
        return fields[i];
      }
      if (fields[i].fieldGroup && fields[i].fieldGroup.length > 0) {
        return this.getField(fields[i].fieldGroup, key);
      }
    }
  }

}