import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcrSubmissionComponent } from './dcr-submission.component';

describe('DcrSubmissionComponent', () => {
  let component: DcrSubmissionComponent;
  let fixture: ComponentFixture<DcrSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcrSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcrSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
