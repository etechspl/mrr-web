import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'm-nested-list',
  templateUrl: './nested-list.component.html',
  styleUrls: ['./nested-list.component.scss']
})
export class NestedListComponent implements OnInit {

  @Input() list: any[]
  @Output() listChange = new EventEmitter<any[]>()
  @Input() L = 0

  constructor() { }

  checkAllChildren(item:any){
    if(item.children){
      item.children.forEach(element => {
        element.checked = true
        if(element.children || element.actions){
          this.checkAllChildren(element)
        }
      });
    }
  }

  uncheckAllChildren(item:any){
    if(item.children){
      item.children.forEach(element => {
        element.checked = false
        if(element.children || element.actions){
          // console.log('recursive')
          this.uncheckAllChildren(element)
        }
      });
    }
   
  }

  private isChecked(index) {
    const checkbox = Object.assign({}, this.list[index])
    if(!checkbox.children) {
      checkbox.children = []
    }
    
    return !!checkbox.children.length && checkbox.children.every(c=>c.checked)
  }

  checkboxChecked(event?, index? , item?) {
    const checkbox = this.list[index]
      if(checkbox && event) {
        if(event.checked) {
          this.checkAllChildren(checkbox)
        } else {
          this.uncheckAllChildren(checkbox)
        }
      } 
    this.listChange.emit(this.list)
  }

  ngOnInit() {
  }


}
