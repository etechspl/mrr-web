import { UserDetailsService } from './../../../../../core/services/user-details.service';
import { UserService } from './../../../../../core/services/user.service';
import { forkJoin } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DesignationComponent } from '../../filters/designation/designation.component';
declare var $;

@Component({
  selector: 'm-user-access',
  templateUrl: './user-access.component.html',
  styleUrls: ['./user-access.component.scss']
})
export class UserAccessComponent implements OnInit {


  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  companyId = this.currentUser.companyId;
  showUserAccessForm = false;
  list;
  list1;
  dataSource;
  dtOptions: any;
  @ViewChild('dataTable') table;
  dataTable: any;
  oTable: any;
  createButton = false;
  updateButton = false;



  constructor(
    private toastrService: ToastrService,
    private _user: UserDetailsService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) { }

  userFilterForm = new FormGroup({
    employeeId: new FormControl(null),
    designation: new FormControl(null)
  })

  userAccessForm = new FormGroup({

  })


  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
   }

  getDesignation(val) {
    this.userFilterForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.userFilterForm.value.divisionId,
        status: [true],
        designationObject: this.userFilterForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  getEmployeeValue(val) {
    this.userFilterForm.patchValue({ employeeId: val });
  }

  viewuserAccess() {
    this.isToggled = false;
    this.showUserAccessForm = true;
      this._user.getUserAccessForCheck(this.userFilterForm.value, this.currentUser.companyId).subscribe(res => {
  
        if (res.length > 0) {
          this.updateButton = true;
          this.createButton = false;
          forkJoin([this._user.getUserAccessForCheck(this.userFilterForm.value, this.currentUser.companyId), this._user.getRolesLabel()])
            .subscribe(([res, roles]) => {
  
              let finalres = []
              for (let n = 0; n < res.length; n++) {
                finalres.push(res[n])
              }
  
              if (roles.length > 0) {
                this.list = roles[0].labels;
              }
              for (let i = 0; i < this.list[0].children.length; i++) {
                for (let j = 0; j < finalres[0].items.length; j++) {
                  if (this.list[0].children[i].label === finalres[0].items[j].title) {
                    this.list[0].children[i].checked = true;
                    if (finalres[0].items[j].hasOwnProperty('submenu')) {
                      for (let ii = 0; ii < this.list[0].children[i].children.length; ii++) {
                        for (let k = 0; k < finalres[0].items[j].submenu.length; k++) {
                          if (finalres[0].items[j].submenu[k].title === this.list[0].children[i].children[ii].label) {
                            this.list[0].children[i].children[ii].checked = true
                            if (finalres[0].items[j].submenu[k].hasOwnProperty('submenu')) {
                              for (let iii = 0; iii < this.list[0].children[i].children[ii].children.length; iii++) {
                                for (let kk = 0; kk < finalres[0].items[j].submenu[k].submenu.length; kk++) {
                                  if (finalres[0].items[j].submenu[k].submenu[kk].title === this.list[0].children[i].children[ii].children[iii].label) {
                                    this.list[0].children[i].children[ii].children[iii].checked = true;
                                  } else { }
                                }
                              }
                            } else { }
                          } else { }
                        }
                      }
                    } else { }
                  } else { }
                }
              }
            });
          this.updateButton = true;
          this.createButton = false;
          setTimeout(() => {
            this._changeDetectorRef.detectChanges();
          }, 500);
          
        } else {
          this._user.getRolesLabel().subscribe(res => {
            if (res.length > 0) {
              this.list = res[0].labels;
              
              setTimeout(() => {
                this._changeDetectorRef.detectChanges();
              }, 500);
            }
          });
          this.createButton = true;
          this.updateButton = false;
          this._changeDetectorRef.detectChanges();
        }
        setTimeout(() => {
          this._changeDetectorRef.detectChanges();
        }, 500);
      });
     // this.addUserAccess();
     setTimeout(() => {
      this._changeDetectorRef.detectChanges();
    }, 500);
  }
  
   addUserAccess() {
    let sublist = [];
    let sublist2 = [];
    let request = [];
    this.list[0].children.forEach(checkbox => {
      if (checkbox.hasOwnProperty('children') && checkbox.checked == true) {
        checkbox.children.forEach(val => {
          if (val.hasOwnProperty('children') && val.checked == true) {
            val.children.forEach(val1 => {
              if (val1.checked == true) {

                const obj11 = {
                  "title": val1.menu.title,
                  "page": val1.menu.page
                }
                sublist2.push(obj11);
              }

            });
            const obj1 = {
              "title": val.label,
              "bullet": val.menu.bullet,
              "submenu": sublist2
            }
            sublist.push(obj1)
            sublist2 = [];

          } else {
            if (val.checked == true) {
              const obj1 = {
                "title": val.menu.title,
                "page": val.menu.page
              }
              sublist.push(obj1)
            }
          }
        });
        const obj = {
          "title": checkbox.label,
          "root": checkbox.menu.root,
          "bullet": checkbox.menu.bullet,
          "icon": checkbox.menu.icon,
          "submenu": sublist
        }

        request.push(obj)
        sublist = [];
      } else {
        if (checkbox.checked) {
          const obj = {
            "title": checkbox.label,
            "desc": checkbox.menu.desc,
            "root": checkbox.menu.root,
            "icon": checkbox.menu.icon,
            "page": checkbox.menu.page,
            "badge": checkbox.menu.badge,
            "translate": checkbox.menu.translate
          }
          request.push(obj)

        }
      }
    });
    return request;
  }

  addAccess() {
    this._user.adduserAccessDetails(this.addUserAccess(), this.userFilterForm.value, this.currentUser.companyId).subscribe(result => {
      this.toastrService.success('User Access has been Created Successfully...');
      this.userFilterForm.reset();
      this.callEmployeeComponent.setBlank();
      this.showUserAccessForm = false;
      this._changeDetectorRef.detectChanges();
    });
  } 

  updateUserAccess() {
    this._user.updateUserAccessDetails(this.addUserAccess(), this.userFilterForm.value, this.currentUser.companyId).subscribe(result => {
      this.toastrService.success('User Access has been Updated Successfully...');
      this.userFilterForm.reset();
      this.callEmployeeComponent.setBlank();
      this.showUserAccessForm = false;
      this._changeDetectorRef.detectChanges();

    });
  }

}

