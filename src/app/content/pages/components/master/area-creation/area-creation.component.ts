import { EmployeeComponent } from './../../filters/employee/employee.component';
import { AreaService } from './../../../../../core/services/Area/area.service';
import { DistrictComponent } from './../../filters/district/district.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { MatSnackBar, MatDialog, MatSlideToggleChange } from '@angular/material';
import { AreaEditDialogComponent } from '../area-edit-dialog/area-edit-dialog.component';
import Swal from 'sweetalert2'
import { StateComponent } from '../../filters/state/state.component';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
//import { DesignationComponent } from '../../filters/designation/designation.component';

@Component({
  selector: 'm-area-creation',
  templateUrl: './area-creation.component.html',
  styleUrls: ['./area-creation.component.scss']
})
export class AreaCreationComponent implements OnInit {

  constructor(
    private areaService: AreaService,
    private userAreaMappingService: UserAreaMappingService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private _changeDetectorRef: ChangeDetectorRef,
    private exportAsService: ExportAsService,
   // private _designationService : DesignationComponent
  ) { }

  displayedColumns = [];
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  desgInfo : any

  ngOnInit() {
    if (this.currentUser.userInfo[0].rL === 1) {
      this.getEmployees(this.currentUser.id);
      this.displayedColumns = ['districtName', 'userName', 'areaName', 'type', 'status', 'action', 'action1'];
    } else if (this.currentUser.userInfo[0].rL === 0) {
      this.displayedColumns = ['stateName', 'districtName', 'userName', 'areaName', 'type', 'status', 'action', 'action1'];
    }

    
  }
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  check: boolean;
  showAreaList = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  areaForm = new FormGroup({
    stateId: new FormControl(null, [Validators.required]),
    districtId: new FormControl(null, [Validators.required]),
    userId: new FormControl(null, [Validators.required]),
    areaName: new FormControl(null, [Validators.required]),
    areaCode: new FormControl(null, []),
    type: new FormControl(null, [Validators.required])
  })

  areaTypes = ['HQ', 'EX', 'OUT']
  myAreas;

  
  getStateValue(val) {
    this.showAreaList = false;
    this.areaForm.patchValue({ stateId: val });
    this.areaForm.patchValue({ districtId: null });
    this.areaForm.patchValue({ userId: null });
    (this.callEmployeeComponent) ? this.callEmployeeComponent.removelist() : null;
    (this.callEmployeeComponent) ? this.callEmployeeComponent.setBlank() : null;
    (this.callDistrictComponent) ? this.callDistrictComponent.setBlank() : null;
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);

  }


  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1' // the id of html/table element
  }


  getDistrictValue(val) {
    this.showAreaList = false;
    this.areaForm.patchValue({ districtId: val });
    this.areaForm.patchValue({ userId: null });
    let districtArr = [];
    districtArr.push(val);
    (this.callEmployeeComponent) ? this.callEmployeeComponent.setBlank() : null;
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr)
  }
  getEmployees(val) {
    this.showAreaList = false;
    this.areaForm.patchValue({ userId: val });
    this.getAreas(this.currentUser.companyId, val)
  }

  getAreas(companyId, userId) {
    this.userAreaMappingService.getMappedAreas(companyId, userId).subscribe(res => {
      let response = [];
      response.push(res);
      if (response[0].length > 0) {
        this.showAreaList = true;
        this.myAreas = res;
      }
    }, err => {
      alert("Error while getting the areas");
    })
  }

  createArea() {
    this.isToggled = false;
    this.areaService.checkAreaAlreadyExist(this.currentUser.companyId, this.areaForm.value).subscribe(result => {
      if (result.length > 0) {
        let res = result[0];
        this.userAreaMappingService.checkMappingAlreadyExist(result[0], this.areaForm.value.userId).subscribe(mappingRes => {
          if (mappingRes.length > 0) {
            this.openSnackBar('Area is Already created for selected details !', 'Please enter another area !!');
          } else {
            this.createMapping(res);
          }
        }, err => {
          // console.log("Error while creating mapping");
        })

      } else {
        this.areaService.createArea(this.currentUser, this.areaForm.value).subscribe(res => {
          this.createMapping(res);
        }, err => {
          alert("Error while creating the area");
        })
      }
    });
    (this.callDistrictComponent) ? this.callDistrictComponent.setBlank() : null;
    (this.callEmployeeComponent) ? this.callEmployeeComponent.setBlank() : null;
    (this.callEmployeeComponent) ? this.callEmployeeComponent.removelist() : null;
    (this.callStateComponent) ? this.callStateComponent.setBlank() : null;
  }

  createMapping(res: object) {
    var data = {
      "companyId": res['companyId'],
      "stateId": res['stateId'],
      "districtId": res['districtId'],
      "areaId": res['id'],
      "status": false,
      "appStatus": 'pending',
      "delStatus": '',
      "mgrAppDate": new Date('1900-01-02T00:00:00'),
      "finalAppDate": new Date('1900-01-02T00:00:00'),
      "mgrDelDate": new Date('1900-01-02T00:00:00'),
      "finalDelDate": new Date('1900-01-02T00:00:00'),
      "appByMgr": '',
      "finalAppBy": '',
      "delByMgr": '',
      "finalDelBy": ''
    }
    if (this.currentUser.userInfo[0].rL == 0) {
      data['status'] = true;
      data['appStatus'] = 'approved';
      data['mgrAppDate'] = new Date();
      data['finalAppDate'] = new Date();
      data['appByMgr'] = this.currentUser.id;
      data['finalAppBy'] = this.currentUser.id;
      data["userId"] = this.areaForm.value.userId;
    } else if (this.currentUser.userInfo[0].rL > 1) {
      if (this.currentUser.company.validation.approvalUpto == 0 || this.currentUser.company.validation.approvalUpto == 1) {
        data['status'] = true;
        data['appStatus'] = 'approved';
        data['mgrAppDate'] = new Date();
        data['finalAppDate'] = new Date();
        data['appByMgr'] = this.currentUser.id;
        data['finalAppBy'] = this.currentUser.id;
        data["userId"] = this.areaForm.value.userId;
      } else {
        data['appStatus'] = 'InProcess';
        data['mgrAppDate'] = new Date();
        data['appByMgr'] = this.currentUser.id;
      }
    } else if (this.currentUser.userInfo[0].rL == 1) {
      data["userId"] = this.currentUser['id'];
      if (this.currentUser.company.validation.approvalUpto == 0) {
        data['status'] = true;
        data['appStatus'] = 'approved';
        data['mgrAppDate'] = new Date();
        data['finalAppDate'] = new Date();
        data['appByMgr'] = this.currentUser.id;
        data['finalAppBy'] = this.currentUser.id;
      }
    }
    this.userAreaMappingService.createUserAreaMapping(data).subscribe(response => {
    }, err => {
      // console.log("Error while creating mapping");
    });
    this.getAreas(this.currentUser.companyId, this.areaForm.value.userId)
    // this.areaForm.reset({
    // areaName: null,
    // areaCode: null,
    // type: null
    // });
    this.areaForm.patchValue({ stateId: null });
    this.areaForm.patchValue({ districtId: null });
    this.areaForm.patchValue({ userId: null });
    this.areaForm.patchValue({ areaName: null });
    this.areaForm.patchValue({ areaCode: null });
    this.areaForm.patchValue({ type: null });
  }

  //---------------------edit Area------------------------------------
  editArea(obj) {
  const dialogRef = this.dialog.open(AreaEditDialogComponent, {
      width: '90%',
			height:"80%",
      data: { obj }
    });
    dialogRef.afterClosed().subscribe((result) => {
			setTimeout(() => {
        this.getAreas(this.currentUser.companyId, this.areaForm.value.userId);
			  this._changeDetectorRef.detectChanges();
			  },50);
		  });
  }
  //----------Delete Area and Mapping--------------------------
  deleteAreaMapping(obj) {
    console.log("obj-------------------------->",obj)
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })
    swalWithBootstrapButtons.fire({
      title: 'Are you sure want to delete?',
      text: 'Enter Deletion Reason',
      input: 'text',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true,
      allowOutsideClick: false
    }).then((result) => {
      console.log("result",result)
      if (result.value) {
        obj["id"]=obj._id;
        this.userAreaMappingService.deleteAreaMapping(obj, this.currentUser.company.id, this.currentUser.id, this.currentUser.company.validation.approvalUpto, this.currentUser.userInfo[0].rL, result.value).subscribe(areaRes => {
          swalWithBootstrapButtons.fire({
            title: 'Area and Mapping deleted successfully!!!',
            text: 'Your area has been deleted successfully!',
            type: 'success',
            allowOutsideClick: false
          }).then((result2) => {
            this.getAreas(this.currentUser.companyId, this.areaForm.value.userId);
            this._changeDetectorRef.detectChanges();
          });
        }, err => {
          Swal.fire('Oops...', err.error.error.message, 'error');
          this._changeDetectorRef.detectChanges();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Area was not deleted.',
          'error'
        )
        this._changeDetectorRef.detectChanges();
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }


  exporttoExcel() {

    // download the file using old school javascript method

    const lastCol = this.displayedColumns.pop();
    const seclastCol = this.displayedColumns.pop();

    setTimeout(() => {

      this.exportAsService.save(this.exportAsConfig, 'Area List Report').subscribe(() => {
        // save started

        this.displayedColumns.push(lastCol);
        this.displayedColumns.push(seclastCol);
      });
    }, 300);

  }
  

}