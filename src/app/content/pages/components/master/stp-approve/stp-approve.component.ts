import { StateComponent } from './../../filters/state/state.component';
import { DivisionComponent } from './../../filters/division/division.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { StpApprovalService } from '../../../../../core/services/stp-approval.service';
import { MatTableDataSource, MatTable, MatSort, MatPaginator, MatSlideToggleChange, MatSelect, MatOption } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ToastrService } from 'ngx-toastr';
import { FormArray, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FareRateCardService } from '../../../../../core/services/FareRateCard/fare-rate-card.service';
import { count } from 'rxjs/operators';
import * as XLSX from 'xlsx';
@Component({
  selector: 'm-stp-approve',
  templateUrl: './stp-approve.component.html',
  styleUrls: ['./stp-approve.component.scss']
})
export class StpApproveComponent implements OnInit {
  // @ViewChild('tableExport') tableRef: ElementRef;

  stpDivForm = new FormGroup({
  });
  IS_DIVISION_EXIST;
  COMPANY_ID;
  designationLevel;
  typeFilter: any;
  FRCValue: any;
  UserWiseSTPData: any;
  stpType_: any;
  frcType_: any;
  FRCValueRequired: Array<string> = [];

  constructor(private stpApprovalService: StpApprovalService,
    private _toastr: ToastrService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _fareRateCardService: FareRateCardService,
    private fb: FormBuilder) { }
  currentUser = JSON.parse(sessionStorage.currentUser);

  companyId = this.currentUser.companyId;
  daType = this.currentUser.company.expense.daType;
  isDivisionExist = this.currentUser.company.isDivisionExist;
  division = this.currentUser.company.divisionId;


  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatTable) table1: MatTable<any>;
  @ViewChild(MatTable) table2: MatTable<any>;

  @ViewChild('sort') sort: MatSort;
  @ViewChild('sort1') sort1: MatSort;
  @ViewChild('sort2') sort2: MatSort;

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator1') paginator1: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  @ViewChild('table2') table2ref: ElementRef;
  @ViewChild('table3') table3ref: ElementRef;
  isShowDivision = false;
  isShowStatwise: boolean = false;
  isShowUserwise: boolean = false;
  isShowSTPRequest: boolean = false;
  isShowfilter: boolean = false;

  selectedRow: any;
  disableApprovalBtn = false;
  disableDeleteBtn = false;
  selection = new SelectionModel<any>(true, []);
  dataSource: MatTableDataSource<any>;
  dataSource1: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;

  types = [];
  stpType = [
    { value: 'HQ', viewValue: 'HQ' },
    { value: 'OUT', viewValue: 'OUT' },
    { value: 'EX', viewValue: 'EX' }
  ];
  allSelected = false;
  @ViewChild('select') select: MatSelect;
  @ViewChild('allStpTypeSelected') allStpTypeSelected: MatOption;

  displayedColumns1;
  displayedColumns;
  //--------Preeti arora----------

  // ------------------nipun (09-01-2020)start added userName in display column---
  displayedColumns2 = ['select', 'sn', 'fromArea', 'toArea', 'stpActivity', 'type', 'distance', 'freqVisit', 'appByMgr'];
  message: string = "";
  editorForm: FormGroup;
  isToggled = true;

  displayCoulums() {
    if (this.isDivisionExist === true) {
      this.displayedColumns = ['sn', 'divisionName', 'stateName', 'districtName', 'userRequest'];
    } else {
      this.displayedColumns = ['sn', 'stateName', 'districtName', 'userRequest'];
    }
  }
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.currentUser);
    this.COMPANY_ID = this.currentUser.companyId;
    this.IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
    this.division = this.currentUser.company.divisionId;
    this.designationLevel = this.currentUser.userInfo[0].designationLevel;


    if (this.IS_DIVISION_EXIST === true) {
      this.isShowDivision = true;
      this.stpDivForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }


    if (this.currentUser.userInfo[0].designationLevel === 0) {
      this.displayedColumns2 = ['select', 'sn', 'fromArea', 'toArea', 'stpActivity', 'type', 'distance', 'freqVisit', 'appByMgr'];
    } else {
      this.displayedColumns2 = ['select', 'sn', 'fromArea', 'toArea', 'stpActivity', 'type', 'distance', 'freqVisit', 'appByMgr'];
    }
    // ------------------nipun (09-01-2020)end added userName in display column---
    if (this.IS_DIVISION_EXIST === true) {
    } else {

      this.displayCoulums();
      this.getSTPStateWise();
    }
    this.displayCoulums();
    //this.getSTPStateWise();
    this.displayCoulums1();
    this.editorForm = new FormGroup({
      "editor": new FormControl(null)
    })
  }

  displayCoulums1() {
    if (this.isDivisionExist === true) {
      this.displayedColumns1 = ['sn', 'divisionName', 'stateName', 'districtName', 'name', 'designation', 'userRequest'];
    } else {
      this.displayedColumns1 = ['sn', 'stateName', 'districtName', 'name', 'designation', 'userRequest'];
    }
  }

  getDivisionValue(val) {
    this.stpDivForm.patchValue({ divisionId: val });
  }

  showReport() {
    this.isShowDivision = true;
    let obj = {};
    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.IS_DIVISION_EXIST === true) {
        obj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          userId: this.currentUser.id,
          rl: this.currentUser.userInfo[0].rL,
          delStatus: { neq: 'approved' },
          division: this.stpDivForm.value.divisionId,
          type: 'state'
        }
      } else {
        obj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          userId: this.currentUser.id,
          rl: this.currentUser.userInfo[0].rL,
          delStatus: { neq: 'approved' },
          type: 'state'
        }
      }

      this.stpApprovalService.getSTPForApprovalDivisionWise({
        obj
      }).subscribe(res => {
        let array = res.map(item => {
          return {
            ...item
          };
        });
        if (array.length > 0) {
          this.isShowStatwise = true;
          this.isShowUserwise = false;
          this.isShowfilter = false;
          this.isShowSTPRequest = false;
          this.dataSource = new MatTableDataSource(array);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

          this.message = "";
        } else {
          this.isShowStatwise = false;
          this.isShowUserwise = false;
          this.isShowSTPRequest = false;
          this.message = "# of STP Request is pending for Approval: 0"
          this._toastr.info("Data not available", "No STP Request is pending for approval");
        }
        this._changeDetectorRef.detectChanges();
      });
    } else if (this.currentUser.userInfo[0].rL > 1) {
      let obj = {};
      if (this.IS_DIVISION_EXIST === true) {
        obj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          isDivisionExist: this.isDivisionExist,
          divisionId: this.stpDivForm.value.divisionId,
          rl: this.currentUser.userInfo[0].rL,
          delStatus: { neq: 'approved' },
          type: 'hierarchy'
        }
      } else {
        obj = {
          isDivisionExist: this.isDivisionExist,
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          rl: this.currentUser.userInfo[0].rL,
          delStatus: { neq: 'approved' },
          type: 'hierarchy'
        }
      }
      //getting Teams STP for Approval
      this.stpApprovalService.getSTPForApprovalDivisionWise({
        obj

      }).subscribe(res => {
        let array = res.map(item => {
          return {
            ...item
          };
        });
        if (array.length > 0) {
          this.isShowStatwise = false;
          this.isShowUserwise = true;
          this.isShowSTPRequest = false;
          this.dataSource1 = new MatTableDataSource(array);
          this.dataSource1.sort = this.sort1;
          this.dataSource1.paginator = this.paginator1;

        } else {
          this.isShowStatwise = false;
          this.isShowUserwise = false;
          this.isShowSTPRequest = false;
          this.message = "# of STP Request is pending for Approval: 0"
          this._toastr.info("Data not available", "No STP Request is pending for approval");
        }
      })
    }

  }


  getSTPStateWise() {
    if (this.currentUser.userInfo[0].rL === 0) {
      let obj = {};
      if (this.IS_DIVISION_EXIST === true) {
        obj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          userId: this.currentUser.id,
          rl: this.currentUser.userInfo[0].rL,
          division: this.stpDivForm.value.divisionId,
          delStatus: { neq: 'approved' },
          type: 'state'
        }
      } else {
        obj = {

          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          userId: this.currentUser.id,
          rl: this.currentUser.userInfo[0].rL,
          delStatus: { neq: 'approved' },
          type: 'state'
        }
      }
      this.stpApprovalService.getSTPForApproval({
        obj
      }).subscribe(res => {
        let array = res.map(item => {
          return {
            ...item
          };
        });
        if (array.length > 0) {
          this.isShowStatwise = true;
          this.isShowUserwise = false;
          this.isShowfilter = false;
          this.isShowSTPRequest = false;
          this.dataSource = new MatTableDataSource(array);

          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

          this.message = "";
        } else {
          this.isShowStatwise = false;
          this.isShowUserwise = false;
          this.isShowSTPRequest = false;
          this.message = "# of STP Request is pending for Approval: 0"
          this._toastr.info("Data not available", "No STP Request is pending for approval");
        }
        this._changeDetectorRef.detectChanges();
      });
    } else if (this.currentUser.userInfo[0].rL > 1) {
      //getting Teams STP for 
      let obj = {};
      if (this.IS_DIVISION_EXIST === true) {
        obj = {
          isDivisionExist: this.isDivisionExist,
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          rl: this.currentUser.userInfo[0].rL,
          delStatus: { neq: 'approved' },
          type: 'hierarchy',
          division: this.stpDivForm.value.divisionId,
        }
      } else {
        obj = {
          isDivisionExist: this.isDivisionExist,
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          rl: this.currentUser.userInfo[0].rL,
          delStatus: { neq: 'approved' },
          type: 'hierarchy'
        }
      }
      this.stpApprovalService.getSTPForApproval({ obj }).subscribe(res => {
        let array = res.map(item => {
          return {
            ...item
          };
        });
        if (array.length > 0) {
          this.isShowStatwise = false;
          this.isShowUserwise = true;
          this.isShowSTPRequest = false;
          this.dataSource1 = new MatTableDataSource(array);
          this.dataSource1.sort = this.sort1;
          this.dataSource1.paginator = this.paginator1;

        } else {
          this.isShowStatwise = false;
          this.isShowUserwise = false;
          this.isShowSTPRequest = false;
          this.message = "# of STP Request is pending for Approval: 0"
          this._toastr.info("Data not available", "No STP Request is pending for approval");
        }
      })
    }
  }
  getUserWiseSTPApprovalReq(row: object) {
    this.isToggled = false;
    this.UserWiseSTPData = row;
    let obj = {};
    if (this.IS_DIVISION_EXIST === true) {
      obj = {
        companyId: this.currentUser.companyId,
        isDivisionExist: this.isDivisionExist,
        districtId: this.UserWiseSTPData.districtId,
        userId: this.currentUser.id,
        rl: this.currentUser.userInfo[0].rL,
        delStatus: { neq: 'approved' },
        division: this.stpDivForm.value.divisionId,
        type: 'user',
        stateId: row['stateId']
      }
    } else {
      obj = {
        companyId: this.currentUser.companyId,
        isDivisionExist: this.isDivisionExist,
        userId: this.currentUser.id,
        districtId: this.UserWiseSTPData.districtId,
        rl: this.currentUser.userInfo[0].rL,
        delStatus: { neq: 'approved' },
        type: 'user',
        stateId: row['stateId']
      }
    }

    this.stpApprovalService.getSTPForApproval({
      obj
    }).subscribe(res => {
      let array = res.map(item => {
        return {
          ...item
        };
      });
      if (array.length > 0) {
        if (this.currentUser.userInfo[0].rL === 0) {
          this.isShowStatwise = true;
        } else {
          this.isShowStatwise = false;
        }

        this.isShowUserwise = true;
        this.isShowfilter = false;
        this.isShowSTPRequest = false;
        this.dataSource1 = new MatTableDataSource(array);

        this.dataSource1.sort = this.sort;
        this.dataSource1.paginator = this.paginator1;

      } else {
        this.isShowStatwise = true;
        this.isShowUserwise = false;
        this.isShowSTPRequest = false;
        this._toastr.info("Data not available", "No STP Request is pending for approval");
        this.getSTPStateWise();
      }
      this._changeDetectorRef.detectChanges();

    });
    this._changeDetectorRef.detectChanges();
    this.table2ref.nativeElement.scrollIntoView();
  }
  //creating STP approval Form

  stpApproveForm: FormGroup = this.fb.group({
    formarray: this.fb.array([])
  });
  get formarray() {
    return this.stpApproveForm.get('formarray') as FormArray;
  }
  changedFRCValue(obj) {
    this.FRCValue = obj;
    console.log('this.FRCValue', this.FRCValue);

  }
  changedValue(obj) {
    this.isShowSTPRequest = true;
    this.typeFilter = obj;

    this.selectedRow['companyId'] = this.currentUser.companyId;
    this.selectedRow['rl'] = this.currentUser.userInfo[0].rL;

    this.stpApprovalService.getSTPApprovalForUser(this.selectedRow).subscribe(res => {
      //clearing the formarray because after approving the data form should display empty.
      this.stpApproveForm = this.fb.group({
        formarray: this.fb.array([])
      });
      let array = res
        .filter(item => this.typeFilter.includes(item.type))
        .map(item => {

          this.formarray.push(this.fb.group({
            id: new FormControl(null),
            distance: new FormControl(item.distance),
            frcId: new FormControl(item.frcId),
            type: new FormControl(item.type),
            stpActivity: new FormControl(item.stpActivity),
          }));
          this._changeDetectorRef.detectChanges();

          let obj = {
            companyId: this.companyId,
            frcType: this.daType
          }
          if (this.daType === "category wise") {
            obj["daCategory"] = this.selectedRow["daCategory"];
          }
          if (this.daType === "Designation-State Wise") {
            obj["designation"] = this.selectedRow["designation"]
          }
          if (this.isDivisionExist === true) {
            obj["isDivisionExist"] = this.isDivisionExist;
            obj["divisionId"] = [this.selectedRow["divisionId"]];
          }
          if (this.daType == 'employee wise') {
            obj["userId"] = res[0].userId;
          }
          console.log(this.selectedRow);
          
          return {
            //this.currentUser.companyId, row["designation"],this.currentUser.company.expense.daType
            frcId: this.getFareRateCardDetails(obj),
            ...item
          };
        });
      if (array.length > 0) {
        if (this.currentUser.userInfo[0].rL === 0) {
          this.isShowStatwise = true;
        } else {
          this.isShowStatwise = false;
        }
        this.isShowUserwise = true;
        this.isShowSTPRequest = true;
        //  this.dataSource2 =array;
        this.dataSource2 = new MatTableDataSource(array);
        this.dataSource2.sort = this.sort2;
        this.dataSource2.paginator = this.paginator2;

        // this._changeDetectorRef.detectChanges();

      } else {
        this.isShowUserwise = false;
        this.isShowSTPRequest = false;
        this._toastr.info("Data not available", "No STP Request is pending for approval");
      }
      this._changeDetectorRef.detectChanges();
    })
    this._changeDetectorRef.detectChanges();
  }
  getSTPApprovalReq(row: object) {
    this.selectedRow = row;
    this.isShowfilter = true;
    this.isShowSTPRequest = false;
    this.table3ref.nativeElement.scrollIntoView();


    // row['companyId'] = this.currentUser.companyId;
    // row['rl'] = this.currentUser.userInfo[0].rL;

    // this.stpApprovalService.getSTPApprovalForUser(row).subscribe(res => {
    //   //clearing the formarray because after approving the data form should display empty.
    //   this.stpApproveForm = this.fb.group({
    //     formarray: this.fb.array([])
    //   });
    //   let array = res.map(item => {

    //     this.formarray.push(this.fb.group({
    //       id: new FormControl(null),
    //       distance: new FormControl(item.distance),
    //       frcId: new FormControl(item.frcId),
    //       type: new FormControl(item.type),
    //     }));
    //     this._changeDetectorRef.detectChanges();

    //     let obj = {
    //       companyId: this.companyId,
    //       frcType: this.daType
    //     }
    //     if (this.daType === "category wise") {
    //       obj["daCategory"] = row["daCategory"];
    //     }
    //     if (this.daType === "Designation-State Wise") {
    //       obj["designation"] = row["designation"]
    //     }
    //     if (this.isDivisionExist === true) {
    //       obj["isDivisionExist"] = this.isDivisionExist;
    //       obj["divisionId"] = [row["divisionId"]];
    //     }
    //     return {
    //       //this.currentUser.companyId, row["designation"],this.currentUser.company.expense.daType
    //       frcId: this.getFareRateCardDetails(obj),
    //       ...item
    //     };
    //   });
    //   if (array.length > 0) {
    //     if (this.currentUser.userInfo[0].rL === 0) {
    //       this.isShowStatwise = true;
    //     } else {
    //       this.isShowStatwise = false;
    //     }
    //     this.isShowUserwise = true;
    //     this.isShowSTPRequest = true;
    //     //  this.dataSource2 =array;
    //     this.dataSource2 = new MatTableDataSource(array);
    //     this.dataSource2.sort = this.sort2;
    //     this.dataSource2.paginator = this.paginator2;

    //     // this._changeDetectorRef.detectChanges();

    //   } else {
    //     this.isShowUserwise = false;
    //     this.isShowSTPRequest = false;
    //     this._toastr.info("Data not available", "No STP Request is pending for approval");
    //   }

    //   this._changeDetectorRef.detectChanges();
    // })
  }
  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect

    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }

  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource2.data.length;
    return numSelected === numRows;
  }
  selectSingle() {
    if (this.allStpTypeSelected.selected) {
      this.allStpTypeSelected.deselect();
      return false;
    }
    if (
      this.typeFilter.length ==
      this.stpType.length
    )
      this.allStpTypeSelected.select();

  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.FRCValueRequired = [];
      this.dataSource2.data.forEach((row, index) => {
        this.formarray.controls[index].patchValue({ id: null });
      })
    } else {
      this.dataSource2.data.forEach((row, index) => {
        if (row["toArea"] && row["fromArea"]) {
          this.FRCValueRequired.push('req');
        }
        this.formarray.controls[index].patchValue({ id: row["id"] });
        this.selection.select(row);
      })
      // this._toastr.info("Please select FRC Code for selected SN.", "FRC Code required.")

    }

  }


  getSelectionValue($event, i: number, row: object) {

    if ($event.checked) {
      this.formarray.controls[i].patchValue({ id: row["id"] });
      if (row["toArea"] && row["fromArea"]) {
        this.FRCValueRequired.push('req');
      }
    } else {
      if (row["toArea"] && row["fromArea"]) {
        this.FRCValueRequired.shift();
      }
      this.formarray.controls[i].patchValue({ id: null });
    }
  }




  //get FRC Code list based on desigantion
  //companyId: string, desigantion: string,daType:string
  getFareRateCardDetails(obj) {
    // console.log("adgfh",obj)
    this._fareRateCardService.getFareRateCardDetails(obj).subscribe(res => {
      this.types = res;
      
    })
  
  }

  count = 0;


  //approve stp method
  approveSTP() {
    if (this.FRCValue == null && (this.FRCValueRequired.length > 0)) {
      this._toastr.error('', `Select FRC Code`);
    } else {

      let formArrayData = JSON.parse(JSON.stringify(this.stpApproveForm.value));
      let obj = {
        rl: this.currentUser.userInfo[0].rL,
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id
      }
      let noOfStpToBeUpdated = 0;
      let stpUpdated = 0;
      for (let i = 0; i < formArrayData.formarray.length; i++) {
        if (!formArrayData.formarray[i].stpActivity || formArrayData.formarray[i].stpActivity == "Select") {
          obj['frcId'] = this.FRCValue;
        }
        obj['id'] = formArrayData.formarray[i].id;
        obj['distance'] = formArrayData.formarray[i].distance;
        obj['type'] = formArrayData.formarray[i].type;
        obj['stpApprovalUpto'] = this.currentUser.company.expense.stpApprovalUpto;
        noOfStpToBeUpdated += 1;
        this.stpApprovalService.approveSTP(obj).subscribe(res => {
        })
      }
      this._toastr.success('', `STP Approved Successfully`);
      this.getSTPStateWise();
      this.getUserWiseSTPApprovalReq(this.UserWiseSTPData);
      this.FRCValueRequired = [];
      this.stpType_ = "";
      this.frcType_ = "";
      this.FRCValue = null;
      // this._changeDetectorRef.detectChanges();
    }

    // if (this.validate(formArrayData) === true) {
    //   for (let i = 0; i < formArrayData.formarray.length; i++) {


    //     if ((formArrayData.formarray[i].id !== null && formArrayData.formarray[i].id !== undefined)) {
    //       if (formArrayData.formarray[i].distance === null || formArrayData.formarray[i].distance < 0 || formArrayData.formarray[i].distance === undefined || formArrayData.formarray[i].distance === '') {
    //         this._toastr.error("Distance should not be empty & distance should be greater than equal to 0 for SN. " + (i + 1), "Distance Required.")
    //         return;
    //       }

    //       if (this.currentUser.userInfo[0].designationLevel === 0) {
    //         if (formArrayData.formarray[i].frcId === "1" || formArrayData.formarray[i].frcId === null || formArrayData.formarray[i].frcId == undefined) {
    //           this._toastr.error("Please select FRC Code for the SN. number " + (i + 1), "FRC Code required.")
    //           return;
    //         }
    //         obj['frcId'] = formArrayData.formarray[i].frcId;
    //       }

    //       obj['id'] = formArrayData.formarray[i].id;
    //       obj['distance'] = formArrayData.formarray[i].distance;
    //       obj['type'] = formArrayData.formarray[i].type;
    //       obj['stpApprovalUpto'] = this.currentUser.company.expense.stpApprovalUpto;
    //       noOfStpToBeUpdated += 1;
    //       this.stpApprovalService.approveSTP(obj).subscribe(res => {
    //         this.getSTPStateWise();

    //       })
    //     }
    //     this.isShowSTPRequest = false;
    //   }
    //   this._toastr.success('', 'STP Approved Successfully');
    //   formArrayData = {};
    // }

  }

  // validate(formArrayData: any) {
  //   let count = 0;
  //   let count1 = 0;
  //   for (let i = 0; i < formArrayData.formarray.length; i++) {

  //     if (this.currentUser.userInfo[0].designationLevel === 0) {
  //       if (formArrayData.formarray[i].id !== null && formArrayData.formarray[i].frcId === "1") {
  //         this._toastr.error("Please select the FRC Code for the S.N. " + (i + 1));
  //         count = 1;
  //         break;
  //       }
  //     }

  //     if (formArrayData.formarray[i].id === null && formArrayData.formarray[i].frcId !== "1") {
  //       this._toastr.error("Please select the Checkbox value for the S.N. " + (i + 1));
  //       count = 1;
  //       break;
  //     }


  //   }
  //   if (count === 1 || count1 === 1) {
  //     return false;
  //   } else {
  //     return true;
  //   }


  // }

  //---------nipun (17-01-2020) ------to delete stp approvals ---

  deleteSTP() {
    this.disableApprovalBtn = true;
    this.disableDeleteBtn = true;
    let idArray = [];
    this.stpApproveForm.value.formarray.forEach(item => {
      if (item.id !== null) {
        return idArray.push(item.id);
      }
    });
    this.stpApprovalService.deleteSTPApprovalReq(idArray).subscribe(res => {

      if (JSON.parse(res.result).count > 0) {
        this._toastr.success('selected approvals deleted successfully.');
        this.getSTPApprovalReq(this.selectedRow);
      }
      this.disableApprovalBtn = false;
      this.disableDeleteBtn = false;
    });
  }
  //---------nipun (17-01-2020) ------end
  // exportToExcel() {
  //   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tableRef.nativeElement );
  //   const wb: XLSX.WorkBook = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  //   XLSX.writeFile(wb, 'STP Approve.xlsx');
  // }
}