import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StpApproveComponent } from './stp-approve.component';

describe('StpApproveComponent', () => {
  let component: StpApproveComponent;
  let fixture: ComponentFixture<StpApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StpApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StpApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
