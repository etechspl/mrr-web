import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderLocationTaggedComponent } from './provider-location-tagged.component';

describe('ProviderLocationTaggedComponent', () => {
  let component: ProviderLocationTaggedComponent;
  let fixture: ComponentFixture<ProviderLocationTaggedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderLocationTaggedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderLocationTaggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
