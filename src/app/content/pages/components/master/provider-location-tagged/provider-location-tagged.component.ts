import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import moment, * as _moment from 'moment';
import { District } from '../../../_core/models/district.model';

import { EmployeeComponent } from '../../filters/employee/employee.component';
import { ToastrService } from 'ngx-toastr';

import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { DivisionComponent } from '../../filters/division/division.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';

import { StateComponent } from '../../filters/state/state.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { URLService } from '../../../../../core/services/URL/url.service';
import { LatLongService } from '../../../../../core/services/LatLong/lat-long.service';
import { EditProviderLocationComponent } from '../edit-provider-location/edit-provider-location.component';
declare var $;

@Component({
  selector: 'm-provider-location-tagged',
  templateUrl: './provider-location-tagged.component.html',
  styleUrls: ['./provider-location-tagged.component.scss']
})

export class ProviderLocationTaggedComponent implements OnInit {

  @ViewChild("userDataTable") dataTable1;
  userDataTable: any;
  userDTOptions: any;

  currentUser = JSON.parse(sessionStorage.currentUser);

  districtData: any;
  districtIds: any;
  districtIdsLength: number;
  designationData = [];
  dataSource;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  DA_TYPE = this.currentUser.company.expense.daType;
  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  isShowDivision = false;
  dialogRef: any = null;

  public showDivisionFilter: boolean = false;
  isShowUsersInfo = false;
  displayedColumns = ['select', 'srno', 'stateName', 'districtName', 'areaName', 'name', 'providerName', 'providerCode', 'providerType', 'degree', 'specialization', 'category', 'address', 'phone', 'frequencyVisit', 'dcrDate', 'workingAddress', 'workingAddressTime', 'action'];
  showProcessing: boolean = false;

  selection = new SelectionModel<any>(true, []);

  providerLocationForm: FormGroup;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  status: boolean = false;

  constructor(public snackBar: MatSnackBar, private fb: FormBuilder,
    private dialog: MatDialog,
    public toasterService: ToastrService,
    private userAreaMappingService: UserAreaMappingService,
    private changeDetectedRef: ChangeDetectorRef,
    private _urlService: URLService,
    private _latLongService: LatLongService,
  ) {
    this.providerLocationForm = this.fb.group({
      companyId: null,
      division: new FormControl(null),
      assignedStates: null,
      designation: new FormControl(null, Validators.required),
      employeeId: new FormControl(null, Validators.required)
    })
  }
  headquarterData: District;
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.isShowDivision = true;
    }
    if (this.currentUser.company.validation.isShowOnlyLatLong === true) {
      this.status = true;
    }

  }



  getStateValue(val) {

    this.providerLocationForm.patchValue({ stateId: val });

    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        division: this.providerLocationForm.value.divisionId,
        companyId: this.currentUser.companyId,
        stateId: val[0],
      };
      this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);
    } else if (this.currentUser.company.isDivisionExist == false) {
      this.providerLocationForm.patchValue({ stateId: val });
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
    }
  }


  getDivisionValue(val) {
    this.providerLocationForm.patchValue({ division: val });
    if (this.currentUser.userInfo[0].designationLevel == 0) {
      //=======================Clearing Filter===================
      this.providerLocationForm.patchValue({ designation: '' });
      this.providerLocationForm.patchValue({ employeeId: '' });
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================

      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      //=======================Clearing Filter===================
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    }


  }
  getDesignation(val) {
    this.callEmployeeComponent.setBlank();
    this.providerLocationForm.patchValue({ designation: val.designationLevel });
    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.providerLocationForm.value.division
        }

        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel] //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.providerLocationForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

    }
  }

  getEmployeeValue(val) {
    this.providerLocationForm.patchValue({ employeeId: val });
  }
  // getLastDCR(value){
  //   let obj = {
  //     providerId: value.providerId,
  //     submitBy:value.userId,
  //     companyId:value.companyId
  //   }
  //   this.userAreaMappingService.getLasrDCR(obj).subscribe(result => {
  //   },
  //     err => {
  //       console.log(err);
  //     })

  // }


  unTaggedProvider(value) {
    const dialogRef = this.dialog.open(EditProviderLocationComponent, {
      width: '60%',
      height: "60%",
      data: { value }
    });
    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.providerLocation();
        this.changeDetectedRef.detectChanges();
      }, 50);
    });
    this.changeDetectedRef.detectChanges();
  }

  // let obj = {
  //   id: value.providerId
  // }
  // let msg = "The Provider will be Untagged.";
  // let buttonMsg = "Yes, Untagged it !!!";
  // const swalWithBootstrapButtons = Swal.mixin({
  //   customClass: {
  //     confirmButton: 'btn btn-success',
  //     cancelButton: 'btn btn-danger'
  //   },
  //   buttonsStyling: false,
  // })

  // swalWithBootstrapButtons.fire({
  //   title: 'Are you sure?',
  //   text: msg,
  //   type: 'warning',
  //   showCancelButton: true,
  //   confirmButtonText: buttonMsg,
  //   cancelButtonText: 'No, cancel!',
  //   reverseButtons: true,
  //   allowOutsideClick: false
  // }).then((result) => {
  //   if (result.value) {
  //     this.userAreaMappingService.unTaggedProvider(obj).subscribe(result => {
  //       this.toasterService.success('Provider has been Updated Successfully !!!');
  //       this.providerLocation();
  //     },
  //       err => {
  //         console.log(err);
  //       })
  //   }
  // });
  // }


  providerLocation() {
    this.isToggled = false;
    this.showProcessing = true;
    this.isShowUsersInfo = false;

    this.changeDetectedRef.detectChanges();

    let obj = {
      companyId: this.currentUser.companyId,
      userId: this.providerLocationForm.value.employeeId,
      designationLevel: this.providerLocationForm.value.designation
    }
    if (this.currentUser.company.isDivisionExist == true) {
      obj["divisionId"] = this.providerLocationForm.value.division;
    }
    this.userAreaMappingService.getProviderLocationTagged(obj).subscribe(async (result) => {
      let docLabel = this.currentUser.company.lables.doctorLabel;
      let venLabel = this.currentUser.company.lables.vendorLabel;
      let stockLabel = this.currentUser.company.lables.stockistLabel;
      this.showProcessing = false;
      if (result.length == 0) {
        this.isShowUsersInfo = false;
        this.showProcessing = false;
        this.toasterService.error("Data Not Found.")
        this.changeDetectedRef.detectChanges();
      }
      
      result.forEach(element => {
        if (this.currentUser.company.validation.hasOwnProperty('isShowOnlyLatLong') && this.currentUser.company.validation.isShowOnlyLatLong == true) {
          if (element.geoLocation.length > 0) {
            let data1 = []
            element.geoLocation.forEach(async data => {
              
              if (data.lat && data.long) {
                data1.push("Lat: " + data.lat + ", Long: " + data.long+" ")
              } else {
                data1.push("Lat: ---, Long: ---")
              }
              element['latlong'] = data1;
            });
          } else {
            let data1 = [];
            data1.push(("Lat: ---, Long: ---"))
            element['latlong'] = data1;
          }
        }
      });


      result.map((item, index) => {
        item['index'] = index;
      });

      setTimeout(() => {

        this.isShowUsersInfo = true;
        this.dataSource = this.getAddressBasedOnLatLong(result);
        this.dataSource = new MatTableDataSource(this.getAddressBasedOnLatLong(result));

        this.dataSource.filteredData.forEach(element => {

          if (element.areaName == undefined || element.areaName == null || element.areaName == '') {
            element['areaName'] = "---";
          }
          if (element.providerCode == undefined || element.providerCode == null || element.providerCode == '') {
            element['providerCode'] = "---";
          }
          if (element.providerType == undefined || element.providerType == null || element.providerType == '') {
            element['providerType'] = "---";
          }
          if (element.providerType == "RMP") {
            element['providerType'] = docLabel;
          } else if (element.providerType == "Drug") {
            element['providerType'] = venLabel;
          } else if (element.providerType == "Stockist") {
            element['providerType'] = stockLabel;
          }
          if (element.areaName == undefined || element.areaName == null) {
            element['areaName'] = "---";
          }

          if (element.degree == undefined || element.degree == null) {
            element['degree'] = "---";
          }
          if (element.specialization == undefined || element.specialization == null) {
            element['specialization'] = "---";
          }
          if (element.category == undefined || element.category == null) {
            element['category'] = "---";
          }
          if (element.address == undefined || element.address == null) {
            element['address'] = "---";
          }
          if (element.phone == undefined || element.phone == null) {
            element['phone'] = "---";
          }
          if (element.frequencyVisit == undefined || element.frequencyVisit == null) {
            element['frequencyVisit'] = "---";
          }
          if (element.dcrDate == undefined || element.dcrDate == null) {
            element['dcrDate'] = "---";
          }
          if (element.workingAddress == undefined || element.workingAddress == null) {
            element['workingAddress'] = [{ address: "----", time: "----" }];
          }
        });
        this.changeDetectedRef.detectChanges();
      }, 500);

      this.changeDetectedRef.detectChanges();
    },
      err => {
        console.log(err);
      })
    this.providerLocationForm.patchValue({})
    this.changeDetectedRef.detectChanges();
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAddressBasedOnLatLong(result) {
    setTimeout(() => {
      result.forEach((item, i) => {
        let workingAdd = [];
        if (item.geoLocation.length <= 0 || item.geoLocation.length == undefined || item.geoLocation == null || item.geoLocation == []) {
          item['workingAddress'] = [{ address: "----", time: "----" }];
        }
        else {
          item.geoLocation.forEach((locationData, j, arr) => {

            if (locationData != null) {
              var latlng1 = { lat: parseFloat(locationData.lat), long: parseFloat(locationData.long) }
              this._latLongService.getAddressOnLongLatBasis(latlng1).subscribe(res => {
                if (res.length > 0) {

                  workingAdd.push({
                    address: res[0].address,
                    time: moment.utc(new Date(locationData.time)).format('D MMM YYYY HH:MM'),
                    latLong: { lat: parseFloat(locationData.lat), long: parseFloat(locationData.long) }
                  })
                  if (Object.is(arr.length - 1, j)) {
                    item['workingAddress'] = workingAdd;
                  }
                  this.changeDetectedRef.detectChanges();

                } else {
                  var geocoder = geocoder = new google.maps.Geocoder();
                  var latlng1 = { lat: parseFloat(locationData.lat), lng: parseFloat(locationData.long) };
                  geocoder.geocode({ 'location': latlng1 }, (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                      if (results === null || results === undefined) {
                        workingAdd.push({
                          address: "----",
                          time: "----"
                        })
                      } else {
                        workingAdd.push({
                          address: results[0].formatted_address,
                          time: moment(locationData.time).format('D MMM YYYY'),
                          latLong: latlng1

                        })
                      }
                    }
                    if (Object.is(arr.length - 1, j)) {
                      item['workingAddress'] = workingAdd;
                      this.changeDetectedRef.detectChanges();
                    }
                  });
                }

              });
            } else {
              workingAdd.push({
                address: "----",
                time: "----"
              })
              item['workingAddress'] = workingAdd;

              this.changeDetectedRef.detectChanges();
            }

          });
        }
      })

    }, 300);
    this.changeDetectedRef.detectChanges();
    return result;
  }

  Untagget(obj) {
    console.log(obj)
    let object = {
      companyId: this.companyId,
      id: obj,
    }
    this.userAreaMappingService.unTaggedMultipleProviders(object).subscribe(result => {
      this.toasterService.success('Location has been Untagged Successfully !!!');
      // this.providerLocation();
    },
      err => {
        console.log(err);
      })

  }
  isAllSelected1() {
    const numSelected1 = this.selection.selected.length;
    const numRows = this.dataSource.filteredData.length;
    return numSelected1 === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle1() {
    if (this.isAllSelected1()) {
      this.selection.clear()
    } else {
      this.dataSource.filteredData.forEach(element => {
        this.selection.select(element)
      });
    }
  }
}