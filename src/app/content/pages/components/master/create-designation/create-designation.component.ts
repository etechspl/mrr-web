import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DesignationService } from '../../../../../core/services/Designation/designation.service';

@Component({
  selector: 'm-create-designation',
  templateUrl: './create-designation.component.html',
  styleUrls: ['./create-designation.component.scss']
})
export class CreateDesignationComponent implements OnInit {

  currentUser = JSON.parse(sessionStorage.currentUser);

  designationForm = new FormGroup({
  });

  constructor(
    private toastrService: ToastrService,
    private _desigService: DesignationService,
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,

  ) {

    this.designationForm = this.fb.group({
      designation: new FormControl(null, Validators.required),
      designationLevel: new FormControl(null, Validators.required),
      fullName: new FormControl(null, Validators.required)
    })

  }


  ngOnInit() {

  }

  submitdesignation() {
  

    let whereObject = {
      where: {
        companyId: this.currentUser.company.id, 
        fullName: this.designationForm.value.fullName,
        designation: this.designationForm.value.designation,
        designationLevel: this.designationForm.value.designationLevel
      }
    }
    this._desigService.isDesignationExist(whereObject).subscribe((res: any) => {

      if (res.length > 0) {
        alert("DesignationAlready Exist...")
      } else {
        const object = {
          companyId: this.currentUser.company.id,
          fullName: this.designationForm.value.fullName,
          designation: this.designationForm.value.designation,
          designationLevel: this.designationForm.value.designationLevel,
        }

        this._desigService.createDesig(object).subscribe(response => {
          this.toastrService.success('Designation Created Successfully...');
         }, err => { 
           console.log(err);
           this.toastrService.error('Designation Already Created..');
         });
      }
    })

  }

}
