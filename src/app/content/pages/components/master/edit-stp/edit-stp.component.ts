import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { STPService } from '../../../../../core/services/stp.service';
import { ToastrService } from 'ngx-toastr';
import { FareRateCardComponent } from '../fare-rate-card/fare-rate-card.component';
import { FareRateCardService } from '../../../../../core/services/FareRateCard/fare-rate-card.service';

@Component({
  selector: 'm-edit-stp',
  templateUrl: './edit-stp.component.html',
  styleUrls: ['./edit-stp.component.scss']
})
export class EditStpComponent implements OnInit {
  stpType = [
    { value: 'HQ', viewValue: 'HQ' },
    { value: 'OUT', viewValue: 'OUT' },
    { value: 'EX', viewValue: 'EX' }
  ];
  frc = [];
  previousData;
  disableSaveBtn: Boolean = false;
  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));

  fg: FormGroup;
  constructor(
    private _toastr: ToastrService,
    private _sTPService: STPService,
    private _stpService: STPService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditStpComponent>,
    private _fareRateCardService: FareRateCardService,
    @Inject(MAT_DIALOG_DATA) data,
  ) {
    this.previousData = data;
  }

  ngOnInit() {

    this.fg = this.fb.group({
      fromArea: [null, Validators.required],
      toArea: [null, Validators.required],
      type: [null, Validators.required],
      frc: [null,Validators.required],
      distance: [null, Validators.required],
    });
    this.fg.patchValue({
      fromArea: this.previousData.fromArea.areaName,
      toArea: this.previousData.toArea.areaName,
      type: this.previousData.type,
      distance: this.previousData.distance,
      frc: this.previousData.frc,
    });

    let obj = {
      companyId: this.previousData.companyId,
      userId: this.previousData.userId,
    }
    this._sTPService.getUserInfo(obj).subscribe(result => {
      let where = {

      }
      if (this.currentUser.company.expense.daType === "category wise") {
        where["daCategory"] = result[0]["daCategory"];
        where["companyId"] = result[0]["companyId,"],
          where["type"] = result[0]["type"]
      }
      if (this.currentUser.company.expense.daType === "Designation-State Wise") {
        where["designation"] = result[0]["designation"],
          where["companyId"] = result[0]["companyId"],
          where["type"] = result[0]["type"]
      }
      if (this.currentUser.company.isDivisionExist === true) {
        where["isDivisionExist"] = true;
        where["divisionId"] = result[0]["divisionId"];
      }
      if (this.currentUser.company.expense.daType == 'employee wise') {
        where["userId"] = result[0].userId;
        where["companyId"] = result[0]["companyId"]
          //where["type"] = result[0]["type"]
          where["frcType"] = this.currentUser.company.expense.daType;
      }
      console.log('rty',where);
      
      this._fareRateCardService.getFareRateCardDetails(where).subscribe(res => {
        console.log(res);
        
        this.frc = res;
      })


    })


  }

  updateAndClose() {
    // This method is responsible for the default process of editting in same employes data.. works for all.
    if (this.previousData.managerStatus == true) {
      this.disableSaveBtn = false;
      this.createManagerStp(this.previousData)
    } else {
      console.log('this',this.previousData);
      
      
      this.disableSaveBtn = true;
      let obj = {
        id: this.previousData.id,
        companyId: this.previousData.companyId,
        distance: this.fg.value.distance,
        type: this.fg.value.type,
        frcId: this.fg.value.frc
      };
      this._sTPService.editSTPApproval(obj).subscribe(res => {
        if (res['count'] === 1) {
          this._toastr.success('STP Updated Successfully !')
          this.disableSaveBtn = false;
          this.dialogRef.close(true);
        } else {
          this._toastr.error('Something went wrong !')
          this.disableSaveBtn = false;
          this.dialogRef.close(false);
        }
      });
    }
  }

  createManagerStp(val){
    //console.log('create manager',val);
    this.disableSaveBtn = false;
    let obj = {};
    let date =new Date();
    obj['companyId'] = this.currentUser.companyId
    obj['userId'] = val.managerData.employeeName;
    obj['fromArea'] = val.fromArea.id;
    obj['toArea'] = val.toArea.id;
    obj['distance'] = this.fg.value.distance;
    obj['type'] = this.fg.value.type;
    obj['createdAt'] = date;
    obj['updatedAt'] = date;
    obj['appByMgr'] = '';
    obj['mgrAppDate'] = date;
    obj['mgrDelDate'] = date;
    obj['finalDelDate'] = date;
    obj['appByAdmin'] = '';
    obj['finalAppDate'] = date;
    obj['status'] = true;
    obj['appStatus'] = 'approved';
    obj['freqVisit'] = val.frequencyVisit;
    obj['stateId'] = val.fromArea.stateId;
    obj['districtId'] = val.toArea.districtId;
    obj['frcId'] = this.fg.value.frc;

    this._stpService.createSTPs(obj).subscribe(res => {
      if (res == null || res.length == 0 || res == undefined) {
        this._toastr.error('Something went wrong...')
        this.dialogRef.close(false);
      }
      console.log(res);
      this._toastr.success('STP created');
      this.dialogRef.close(true);
    })
  }

  closeModel(flag: any) {
    this.dialogRef.close(false);
  }

}
