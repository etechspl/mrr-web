import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStpComponent } from './edit-stp.component';

describe('EditStpComponent', () => {
  let component: EditStpComponent;
  let fixture: ComponentFixture<EditStpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditStpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
