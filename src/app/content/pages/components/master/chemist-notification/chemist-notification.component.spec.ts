import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChemistNotificationComponent } from './chemist-notification.component';

describe('ChemistNotificationComponent', () => {
  let component: ChemistNotificationComponent;
  let fixture: ComponentFixture<ChemistNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChemistNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChemistNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
