import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MefService } from '../../../../../core/services/Mef/mef.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FromdateComponent } from '../../filters/fromdate/fromdate.component';

@Component({
  selector: 'm-edit-mef-tracker',
  templateUrl: './edit-mef-tracker.component.html',
  styleUrls: ['./edit-mef-tracker.component.scss']
})
export class EditMefTrackerComponent implements OnInit {
  userData: any;
  areas = [];
  providers = [];
  providersForm = [];
  focusProducts = [];
  designationData = [];
  areaSelected;
  providerSelected;
  quotationStatusSelected;
  quotationStatus = [
    {
      value: "Pending for approval from store",
      name: "Pending for approval from store"
    }, {
      value: "Pending for approval from doctor",
      name: "Pending for approval from doctor",
    }, {
      value: "High/Low Price",
      name: "High/Low Price",
    }, {
      value: "Approval Done",
      name: "Approval Done",
    }, {
      value: "Others",
      name: "Others"
    }
  ];


  editForm = false;
  isShowSpecified = false;
  isShowNeedFromSr = false;
  isShowInformedHim = false;
  isShowSolutionGiven = false;
  isShowQuotationDate = false;
  isShowQuotationStatus = false;
  isShowConversionDone = false;
  isShowSrGivenSol = false;
  isShowQuotationOther = false;
  employeeFilter = false;
  designationFilter = false;

  constructor(
    public fb: FormBuilder,
    private _toastr: ToastrService,
    private userAreaMapping: UserAreaMappingService,
    private _providerService: ProviderService,
    private _productService: ProductService,
    private userDetailsService: UserDetailsService,
    private _mefService: MefService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { this.modifyForm(fb); this.editMefMainForm(fb) }

  public editMefFormMain: FormGroup;
  public editMefForm: FormGroup;

  editMefMainForm(fb: FormBuilder) {
    this.editMefFormMain = fb.group({
      mainMefDate: new FormControl(null, [Validators.required]),
      editProvider: new FormControl(null, [Validators.required]),
    });
  }

  modifyForm(fb: FormBuilder) {
    this.editMefForm = fb.group({
      id: new FormControl(null, [Validators.required]),
      mefDate: new FormControl(null, [Validators.required]),
      jointWork: new FormControl(null),
      area: new FormControl(null, [Validators.required]),
      provider: new FormControl(null, [Validators.required]),
      competitorProduct: new FormControl(null, [Validators.required]),
      focusProduct: new FormControl(null, [Validators.required]),
      DrFacingProduct: new FormControl(null, [Validators.required]),
      solutionOurProduct: new FormControl(null, [Validators.required]),
      drFeedback: new FormControl(null, [Validators.required]),
      solutionToHisQry: new FormControl(null, [Validators.required]),
      ifYesSpecified: new FormControl(null),
      helpNeedFromSr: new FormControl(null),
      haveInformed: new FormControl(null),
      srGivenSolution: new FormControl(null),
      solutionGiven: new FormControl(null),
      quotation: new FormControl(null, [Validators.required]),
      quotationDate: new FormControl(null),
      quotationStatus: new FormControl(null),
      quotationOther: new FormControl(null),
      conversion: new FormControl(null, [Validators.required]),
      notReason: new FormControl(null),
    });
  }

  ngOnInit() {
    this.getUserMappedAreas(this.currentUser.id);
    if (this.currentUser.userInfo[0].rL == 1) {
      this.getUserMappedAreas(this.currentUser.id);
      this.modifyForm(this.fb);
    } else if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 4) {
      this.modifyForm(this.fb);
      this.editMefForm.get("jointWork").setValidators([Validators.required]);
      this.employeeFilter = true;
      this.designationFilter = false;
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, this.currentUser.userInfo[0].stateId, this.currentUser.userInfo[0].districtId, 'Employeewise').subscribe(res => {
        this.designationData = res;
      });
    }
  }

  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  currentUser = JSON.parse(sessionStorage.currentUser);

  getMEFDate(val) {
    let day = val.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    let month = val.getMonth() + 1;
    if (month < 10) {
      month = "0" + month
    }
    let year = val.getFullYear();

    let dateFormat = year + "-" + month + "-" + day;
    this._mefService.getMefDetailForEditForm(this.currentUser.companyId, dateFormat, this.currentUser.userInfo[0].userId).subscribe(res => {
      this.providersForm = res;
    });
    this.editMefFormMain.patchValue({ mainMefDate: dateFormat })
  }

  getMEFDateChild(val) {
    let day = val.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    let month = val.getMonth() + 1;
    if (month < 10) {
      month = "0" + month
    }
    let year = val.getFullYear();
    let dateFormat1 = year + "-" + month + "-" + day;
    this.editMefForm.patchValue({ mefDate: dateFormat1 })
  }

  getEditFormInfo(val) {
    this.editForm = true;
    this._mefService.setMefDetailForEditInfo(val).subscribe(res => {
      this.editMefForm.get("id").setValue(val);
      this.editMefForm.get("mefDate").setValue(res[0].date);
      this.areaSelected = res[0].areaId;
      this.providerSelected = res[0].providerId + '';
      if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 6) {
        this.getMgrEmployee({ designationLevel: 1 });
        this.editMefForm.patchValue({ jointWork: res[0].jointWorkWith[0] })
      }
      this.getProvider(res[0].areaId);
      this.getFocusProduct(res[0].providerId);
      this.setFocusProduct(res[0].productDiscussed);
      this.editMefForm.get("competitorProduct").setValue(res[0].competitorProduct);
      this.editMefForm.get("DrFacingProduct").setValue(res[0].drProductProblem);
      this.editMefForm.get("solutionOurProduct").setValue(res[0].problemSolnToDr);
      this.editMefForm.get("drFeedback").setValue(res[0].doctorFeedback);
      if (res[0].isQuerySolnGiven == true) {
        this.isShowSpecified = true;
        this.isShowNeedFromSr = false;
        this.editMefForm.get("solutionToHisQry").setValue('yes');
        this.editMefForm.get("ifYesSpecified").setValue(res[0].querySoln);
      } else {
        this.isShowNeedFromSr = true;
        this.isShowInformedHim = true;
        this.isShowSpecified = false;
        this.editMefForm.get("solutionToHisQry").setValue('no');
        this.editMefForm.get("helpNeedFromSr").setValue(res[0].seniorHelp);
        if (res[0].isInformToSenior == true) {
          this.isShowSrGivenSol = true;
          this.editMefForm.get("haveInformed").setValue('yes');
          if (res[0].isSolnFromSenior == true) {
            this.isShowSolutionGiven = true;
            this.editMefForm.get("srGivenSolution").setValue('yes');
            this.editMefForm.get("solutionGiven").setValue(res[0].solnGivenBySenior);
          } else {
            this.editMefForm.get("srGivenSolution").setValue('no');
          }
        } else {
          this.editMefForm.get("haveInformed").setValue('no');
        }
      }
      if (res[0].quoteGiven == true) {
        this.isShowQuotationDate = true;
        this.isShowQuotationStatus = true;
        this.editMefForm.get("quotation").setValue('yes');
        this.quotationStatusSelected = res[0].quoteStatus;
        this.editMefForm.get("quotationDate").setValue(res[0].quoteDate);
        if (res[0].quoteStatus == 'Others') {
          this.isShowQuotationOther = true;
          this.editMefForm.get("quotationOther").setValue(res[0].quoteOthers);
        }
      } else {
        this.editMefForm.get("quotation").setValue('no');
      }
      if (res[0].conversionDone == false) {
        this.isShowConversionDone = true;
        this.editMefForm.get("conversion").setValue('no');
        this.editMefForm.get("notReason").setValue(res[0].conversionNoReason);
      } else {
        this.editMefForm.get("conversion").setValue('yes');
      }
    });
  }

  getMgrEmployee(val) {
    this.userDetailsService.getUserInfoBasedOnDesignation(this.currentUser.companyId, val.designationLevel, [true]).subscribe(res => {
      console.log(res);
      this.userData = res;
    }, err => {
      console.log(err)
    });
  }

  getUserMappedAreas(userId) {
    if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 4) {
      this.editMefForm.patchValue({ jointWork: userId })
    }
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, userId).subscribe(res => {
      this.areas = res;
    });
  }

  getProvider(val) {
    this.editMefForm.patchValue({ area: val })
    this._providerService.getMEFProviderList(this.currentUser.companyId, [val]).subscribe(res => {
      this.providers = res;
    });
  }

  getFocusProduct(val) {
    this.editMefForm.patchValue({ provider: val })
    this._providerService.getFocusProductList(this.currentUser.companyId, val).subscribe(res => {
      this._productService.getFocusProductBasedOnProvider(this.currentUser.companyId, [res[0].focusProduct]).subscribe(res => {
        this.focusProducts = res;
      });
    });
  }

  setFocusProduct(val) {
    this.editMefForm.patchValue({ focusProduct: val })
  }

  getSolutionToHisQry(val) {
    this.editMefForm.patchValue({ haveInformed: undefined });
    this.editMefForm.patchValue({ srGivenSolution: undefined });
    if (val == 'yes') {
      this.isShowSpecified = true;
      this.isShowNeedFromSr = false;
      this.isShowInformedHim = false;
      this.isShowSrGivenSol = false;
      this.isShowSolutionGiven = false;
    } else {
      this.isShowSpecified = false;
      this.isShowNeedFromSr = true;
      this.isShowInformedHim = true;
      this.isShowSrGivenSol = false;
      this.isShowSolutionGiven = false;
    }
    this.editMefForm.patchValue({ solutionToHisQry: val });  
  }

  getSrGivenSolution(val) {
    if (val == 'yes') {
      this.isShowSolutionGiven = true;
    } else {
      this.isShowSolutionGiven = false;
    }
  }

  getHaveInformed(val) {
    if (val == 'yes') {
      this.isShowSrGivenSol = true;
    } else {
      this.isShowSrGivenSol = false;
      this.isShowSolutionGiven = false;
    }
  }

  getQuotationGiven(val) {
    if (val == 'yes') {
      this.isShowQuotationDate = true;
      this.isShowQuotationStatus = true;
    } else {
      this.isShowQuotationDate = false;
      this.isShowQuotationStatus = false;
    }
    this.editMefForm.patchValue({ quotation: val })
  }

  getconversion(val) {
    if (val == 'yes') {
      this.isShowConversionDone = false;
    } else {
      this.isShowConversionDone = true;
    }
    this.editMefForm.patchValue({ conversion: val })
  }

  getQuotationStatus(val) {
    this.isShowQuotationOther = false;
    if (val == 'Others') {
      this.isShowQuotationOther = true;
    }
  }

  getQuotationDate(val) {
    this.editMefForm.patchValue({ quotationDate: val })
  }

  modifyMefForm() {
    this._mefService.mefModify(this.editMefForm.value, this.currentUser.userInfo[0], this.currentUser.companyId).subscribe(res => {
      this.editForm = false;
      this.isShowSpecified = false;
      this.isShowNeedFromSr = false;
      this.isShowInformedHim = false;
      this.isShowSolutionGiven = false;
      this.isShowQuotationDate = false;
      this.isShowQuotationStatus = false;
      this.isShowConversionDone = false;
      this.isShowSrGivenSol = false;
      this.isShowQuotationOther = false;
      this.editMefFormMain.reset();
      this.editMefForm.reset();
      this._toastr.success("Form modify successfully !", "MEF Tracker Form Modify Successfully !!");
    }, err => {
      console.log(err);
    })
  }

}
