import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMefTrackerComponent } from './edit-mef-tracker.component';

describe('EditMefTrackerComponent', () => {
  let component: EditMefTrackerComponent;
  let fixture: ComponentFixture<EditMefTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMefTrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMefTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
