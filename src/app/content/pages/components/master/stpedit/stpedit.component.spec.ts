import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { STPEditComponent } from './stpedit.component';

describe('STPEditComponent', () => {
  let component: STPEditComponent;
  let fixture: ComponentFixture<STPEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ STPEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(STPEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
