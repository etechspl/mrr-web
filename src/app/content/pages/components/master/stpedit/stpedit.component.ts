import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreaComponent } from '../../filters/area/area.component';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { STPService } from '../../../../../core/services/stp.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'm-stpedit',
  templateUrl: './stpedit.component.html',
  styleUrls: ['./stpedit.component.scss']
})
export class STPEditComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<STPEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userAreaMapping: UserAreaMappingService,
    private stpService: STPService,
    private toaster: ToastrService) { }

  currentUser = JSON.parse(sessionStorage.currentUser);

  @ViewChild("areaComponent") areaComponent: AreaComponent;
  areas: any;

  ngOnInit() {
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, this.currentUser.id).subscribe(res => {
      this.areas = res;
    })
    this.stpEditFormCreation();
  }

  stpEditForm: FormGroup;

  stpEditFormCreation() {
    this.stpEditForm = this.fb.group({
      id: this.data.obj.id,
      fromArea: this.data.obj.fromArea.id,
      toArea: this.data.obj.toArea.id,
      type: this.data.obj.type,
      distance: this.data.obj.distance,
      freqVisit: this.data.obj.freqVisit
    })
    console.log("STP is exists ", this.stpEditForm.value);

  }

  updateSTP() {
    this.stpService.updateSTP(this.stpEditForm.value).subscribe(res => {
      if (res["count"] > 0) {
        this.toaster.success('','STP updated Successfully.')
      } else {
        this.toaster.error('','Records not updated.')
      }
    })

  }
  closeDialog() {
    this.dialogRef.close();
  }
}



