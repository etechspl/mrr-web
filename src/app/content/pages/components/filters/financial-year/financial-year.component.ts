import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { Console } from 'console';
import { MonthYearService } from '../../../../../core/services/month-year.service';

@Component({
  selector: 'm-financial-year',
  templateUrl: './financial-year.component.html',
  styleUrls: ['./financial-year.component.scss']
})
export class FinancialYearComponent implements OnInit {
  yearList;
  @Output() valueChange = new EventEmitter();

  constructor(private _monthYearservice : MonthYearService) { }

  ngOnInit() {
    let financialYear = this._monthYearservice.getFinancialYear();
    this.yearList = financialYear;
    console.log("Gourav=================================================================>", this.yearList)
  }

  changedValue(val) { 
    this.valueChange.emit(val);
  }

}
