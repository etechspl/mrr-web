import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { AreaModel } from '../../../_core/models/area.model';
import { AreaService } from '../../../../../core/services/Area/area.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';


@Component({
  selector: 'm-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  constructor(private _areaService: AreaService, private _userAreaMapping: UserAreaMappingService) { }
  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));

  areaData: AreaModel;

  ngOnInit() {
    this.getAreaList(this.currentUser.companyId, [this.currentUser.userInfo[0].districtId], this.currentUser.userInfo[0].designationLevel, [this.currentUser.userInfo[0].userId]);
  }

  getAreaList(companyId, districtIds, designationLevel, userIds) {
    this._areaService.getAreaList(companyId, districtIds, designationLevel, userIds).subscribe(res => {
      this.areaData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }

  changedValue(val) {
    this.valueChange.emit(val);
  }
}
