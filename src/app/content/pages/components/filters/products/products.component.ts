import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { ProductService } from '../../../../../core/services/Product/product.service';

@Component({
  selector: 'm-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean ;
  constructor(private _productService: ProductService) { }
  productData=[];
  selecteddata;

  ngOnInit() { 
  }

  getProductsData(companyId,stateIds){
    this._productService.getProducts(companyId,stateIds).subscribe(res => {
      this.productData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }

  getProductsDataForGroup(companyId,IS_DIVISION_EXIST,divisionId){
  let obj={
  "companyId":companyId,
  "IS_DIVISION_EXIST":IS_DIVISION_EXIST,
  "divisionId": divisionId
  }
    this._productService.getProductsDataForGroup(obj).subscribe(res => {
      this.productData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }


  changedValue(val) {
    this.valueChange.emit(val);
  } 
  setBlank(){
    this.selecteddata = [];
    this.productData.length = 0;
  }
}
