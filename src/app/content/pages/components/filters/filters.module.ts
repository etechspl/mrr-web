import { DistrictComponent } from './district/district.component';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { PartialsModule } from '../../../partials/partials.module';
// Core => Services (API)
import { CustomersService } from '../../_core/services';
// Core => Utils
import { HttpUtilsService } from '../../_core/utils/http-utils.service';
import { TypesUtilsService } from '../../_core/utils/types-utils.service';
import { LayoutUtilsService } from '../../_core/utils/layout-utils.service';
import { InterceptService } from '../../_core/utils/intercept.service';
// Shared
import { FiltersComponent } from './filters.component';
import { StateComponent } from './state/state.component';

import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule,
	MatSliderModule,
	MatFormFieldModule,
	MatListModule,
	MatSlideToggleModule

} from '@angular/material';
import { EmployeeComponent } from './employee/employee.component';
import { MonthComponent } from './month/month.component';
import { YearComponent } from './year/year.component';
import { DesignationComponent } from './designation/designation.component';
import { ReporttypeComponent } from './reporttype/reporttype.component';
import { FromdateComponent } from './fromdate/fromdate.component';
import { TypeComponent } from './type/type.component';
import { TodateComponent } from './todate/todate.component';
import { AreaComponent } from './area/area.component';
import { StockiestComponent } from './stockiest/stockiest.component';
import { MappedStockistComponent } from './mapped-stockist/mapped-stockist.component';
import { StatusComponent } from './status/status.component';
import { DivisionComponent } from './division/division.component';
import { FinancialYearComponent } from './financial-year/financial-year.component';
import { FiscalYearComponent } from './fiscal-year/fiscal-year.component';
import { GiftCreationComponent } from '../master/gift-creation/gift-creation.component';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';
import { DACategoryComponent } from './dacategory/dacategory.component';
import { CategoryComponent } from './category/category.component';
import { ProvidersComponent } from './providers/providers.component';
import { ProductsComponent } from './products/products.component';
import { ProductGroupFilterComponent } from './product-group-filter/product-group-filter.component';
import { ReusableTableComponent } from './reusable-table/reusable-table.component';

const routes: Routes = [
	{
		path: '',
		component: FiltersComponent,
		children: [
			{
				path: 'statefilter',
				component: StateComponent
			},
			{
				path: 'designationfilter',
				component: DesignationComponent
			}
		]
	}
];
@NgModule({
	imports: [
		MatDialogModule,
		CommonModule,
		HttpClientModule,
		PartialsModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		MatSliderModule,
		MatFormFieldModule,
		MatListModule,
		MatSlideToggleModule

	],
	providers: [
		InterceptService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'm-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		HttpUtilsService,
		CustomersService,
		TypesUtilsService,
		LayoutUtilsService
	],
	entryComponents: [
		//ActionNotificationComponent,
		//DeleteEntityDialogComponent,
		//FetchEntityDialogComponent,
		//UpdateStatusDialogComponent,
	],
	declarations: [
		FiltersComponent, StateComponent, DistrictComponent, EmployeeComponent, MonthComponent, YearComponent, DesignationComponent, ReporttypeComponent, FromdateComponent, TypeComponent, TodateComponent, StockiestComponent, MappedStockistComponent, DivisionComponent, AreaComponent, StatusComponent, GiftCreationComponent, FinancialYearComponent, FiscalYearComponent, EmployeeInfoComponent, DACategoryComponent, CategoryComponent, ProvidersComponent, ProductsComponent, ProductGroupFilterComponent, ReusableTableComponent],
	exports: [
		StateComponent,
		ReusableTableComponent,
		DistrictComponent,
		EmployeeComponent,
		EmployeeInfoComponent,
		MonthComponent,
		YearComponent,
		ReporttypeComponent,
		FromdateComponent,
		TodateComponent,
		TypeComponent,
		DesignationComponent,
		AreaComponent,
		StockiestComponent,
		MappedStockistComponent,
		DivisionComponent,
		StatusComponent,
		GiftCreationComponent,
		FinancialYearComponent,
		FiscalYearComponent,
		DACategoryComponent,
		CategoryComponent,
		ProvidersComponent,
		ProductsComponent,
		ProductGroupFilterComponent
	]
})
export class FiltersModule { }
