import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductService } from '../../../../../core/services/Product/product.service';

@Component({
  selector: 'm-product-group-filter',
  templateUrl: './product-group-filter.component.html',
  styleUrls: ['./product-group-filter.component.scss']
})
export class ProductGroupFilterComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean ;
  constructor(private _productService: ProductService) { }
  productGroupData=[];
  
  selecteddata;
  ngOnInit() {
  }

  getProductGroupData(companyId,isDivisionExist,obj){

    this._productService.getProductGroup(companyId,isDivisionExist,obj).subscribe(res => {
      this.productGroupData = res;
      console.log("this.productGroupData :",this.productGroupData);
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }

  changedValue(val) {
    this.valueChange.emit(val);
  } 
  setBlank(){ 
    this.selecteddata = [];
    this.productGroupData.length = 0;
  }
}
