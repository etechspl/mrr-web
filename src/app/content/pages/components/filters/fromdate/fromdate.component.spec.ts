import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromdateComponent } from './fromdate.component';

describe('DateselectionComponent', () => {
  let component: FromdateComponent;
  let fixture: ComponentFixture<FromdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
