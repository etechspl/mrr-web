import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS,  NativeDateAdapter } from '@angular/material/core';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';
import { FormControl } from '@angular/forms';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: {month: 'short', year: 'numeric', day: 'numeric'}
},
display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: 'input',
    monthYearLabel: {year: 'numeric', month: 'short'},
    dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'},
}
};

@Component({
  selector: 'm-fromdate',
  templateUrl: './fromdate.component.html',
  styleUrls: ['./fromdate.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: DateAdapter, useClass: FromdateComponent },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class FromdateComponent extends NativeDateAdapter implements OnInit {
  @Output() valueChange = new EventEmitter();
  //@Output() fromDate=new EventEmitter();
  @Input () required: false;
  date;

  ngOnInit() {
  }

  yourFunction(val) {
    let day = val.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    let month = val.getMonth() + 1;
    if (month < 10) {
      month = "0" + month
    }
    let year = val.getFullYear();

    this.valueChange.emit(year + "-" + month + "-" + day);
  }

  format(date: Date, displayFormat: Object): string {
    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return this._to2digit(day) + '-' + this._to2digit(month) + '-' + year;
    } else {
      return date.toDateString();
    }
  }

  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }
  setBlank(){
    this.date = null;
  }

}
