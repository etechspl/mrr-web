import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'm-reporttype',
  templateUrl: './reporttype.component.html',
  styleUrls: ['./reporttype.component.scss']
})
export class ReporttypeComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  changedValue(val) { 
    this.valueChange.emit(val);
  }
}
