import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappedStockistComponent } from './mapped-stockist.component';

describe('MappedStockistComponent', () => {
  let component: MappedStockistComponent;
  let fixture: ComponentFixture<MappedStockistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappedStockistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappedStockistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
