import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'm-mapped-stockist',
  templateUrl: './mapped-stockist.component.html',
  styleUrls: ['./mapped-stockist.component.scss']
})
export class MappedStockistComponent implements OnInit {

  @Output() valueChange = new EventEmitter();
  @Input() check: boolean ;
  constructor(private stockiestService: StockiestService) { }
  stockistData=[];
  selecteddata;
  ngOnInit() {

  }

  getMappedStockists(companyId,userId){
    this.stockiestService.getMappedStockist(companyId,userId).subscribe(res => {
      this.stockistData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }

  getMappedStockist(companyId,userId){
    this.stockiestService.getMappedStockists(companyId,userId).subscribe(res => {
      this.stockistData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }

  getHQWiseStockist(obj){
    this.stockiestService.getHQWiseStockist(obj).subscribe(res => {
      this.stockistData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }
  changedValue(val) {
    this.valueChange.emit(val);
  }
  setBlank(){
    this.selecteddata = [];
    this.stockistData.length = 0;
  }

}
