import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'm-fiscal-year',
  templateUrl: './fiscal-year.component.html',
  styleUrls: ['./fiscal-year.component.scss']
})
export class FiscalYearComponent implements OnInit {
  @Output() valueChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  changedValue(val) {
    this.valueChange.emit(val);
  }

}
