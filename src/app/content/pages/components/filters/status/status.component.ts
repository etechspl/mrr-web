import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'm-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  selectedValue = true;
  selectedVal = true;
  chosenProgram = 'act';

  // statusDetails = [{
  //   id: true,
  //   statusName: "Active"
  // }, {
  //   id: false,
  //   statusName: "InActive"
  // }]


  constructor() { }
  brandFont: any;
  defaultFont: any;
  selectedObjects : any;


  statusDetails : any = [
	  { id: true,  statusName: "Active" },
	  { id: false, statusName: "InActive"}
	]

  ngOnInit() {
  //   this.defaultFont = this.statusDetails[0];
	// this.brandFont = Object.assign(this.defaultFont.label);
	// this.changedValue(this.selectedValue);
  // this.selectedVal = this.selectedValue;

  this.selectedObjects = [{statusName: 'Active', id: 99}];

  }


  comparer(o1: any, o2: any): boolean {
       return o1 && o2 ? o1.name === o2.name : o2 === o2;
  }


  changedValue(val) {
	this.valueChange.emit(val);
  }

  onChange(val) {
  }

  setBlank() {
	this.selectedValue =  null;
 }



}
