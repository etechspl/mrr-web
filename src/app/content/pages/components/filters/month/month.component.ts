import { MonthYearService } from '../../../../../core/services/month-year.service';
import { Component, OnInit, Output, EventEmitter,ChangeDetectorRef, Input } from '@angular/core';

@Component({
  selector: 'm-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.scss']
})
export class MonthComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean = false;

  constructor(private _monthYearservice:MonthYearService,
	          private _changeDetectorRef: ChangeDetectorRef,) { }
  monthList;
  yearList;
  month;
  currentmonth = new Date().getMonth()+1;
  selectedVal = [];

  ngOnInit() {
	 let monthYear =  this._monthYearservice.getMonthAndCurrentYearList(2017);
	 this.monthList = monthYear[0].month;
	 this.yearList = monthYear[1].year;

	 this.monthList.forEach(i => {
		if(i.monthValue == this.currentmonth){
		  this.changedValue(this.currentmonth);
		  }
	  })

	  this.selectedVal = [this.currentmonth]

 }
 getMonthList(){
    return this._monthYearservice.getMonthAndCurrentYearList(2017);
  }
  changedValue(val) {
	this.valueChange.emit(val);
	(!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges(): null;
  }


}
