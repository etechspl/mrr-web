import { ToastrService } from 'ngx-toastr';
import { DesignationService } from '../../../../../core/services/Designation/designation.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'm-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.scss']
})
export class DesignationComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  @Input() required: boolean;
  currentUser = JSON.parse(sessionStorage.currentUser);

  constructor(private _designationService: DesignationService, private _toastrService: ToastrService) { }

  desgInfo: any;
  selectedValue;
  ngOnInit() {
    this._designationService.getLowerLevelDesignations(this.currentUser.companyId, this.currentUser.userInfo[0].designationLevel).subscribe(res => {
      this.desgInfo = res;
    }, err => {
      this._toastrService.info("Oooops! \nNo Record Found");
    });
  }

  getManagersDesignation() {
    let param = {
      "companyId": this.currentUser.companyId
    }
    this._designationService.getManagersDesignation(param).subscribe(
      res => {
        this.desgInfo = res;
      },
      error => {
        console.log("Error " + error)
      }
    )
  }

  //------------------------Praveen Kumar(11-04-2019)-------------------
  getUniqueDesignationOnStateOrDistrict(param) {
    this._designationService.getUniqueDesignationOnStateOrDistrict(param).subscribe(
      res => {
        this.desgInfo = res;
      },
      error => {
        console.log("Error " + error)
      });
  }

  //--------------------------------END---------------------------------

  //------------------------Praveen Kumar(09-08-2019)-------------------
  getDesignationBasedOnDivision(param) {
    this._designationService.getDesignationBasedOnDivision(param).subscribe(
      res => {
        this.desgInfo = res;
      },
      error => {
        console.log("Error " + error)
      });
  }

  //--------------------------------END---------------------------------

  getDesignationBasedOnDivisionOnlyManager(param) {
    this._designationService.getDesignationBasedOnDivision(param).subscribe(
      (res:any) => {
        let arr = []
        res.forEach(element => {
          if(element.designationLevel!=1){
            arr.push(element);
          }
        });
        this.desgInfo = arr;
      },
      error => {
        console.log("Error " + error)
      });
  }


  changedValue(val) {
    this.valueChange.emit(val);
  }
  setBlank() {
    this.desgInfo = [];
    this.selectedValue = null;
  }

  //------------------------Praveen Kumar(09-08-2019)-------------------
  getAllDesignationForMail(param) {
    this._designationService.getAllDesignationForMail(param).subscribe(
      res => {
        this.desgInfo = res;
      },
      error => {
        console.log("Error " + error)
      });
  }
  //------------------------------------------------

  getDesignation(companyId : string){
    this._designationService.getDesignations(companyId).subscribe(res => {
      console.log(res);
      this.desgInfo = res;
    },
    error=>{
      console.log('Error'+error);
    }
    );
  }


  getDesignations(companyId : string){
    this._designationService.getDesignations(companyId).subscribe(res => {
      console.log(res);
      let i;
      let j;
      let length = Object.keys(res).length      
      let desig=[]
      for(i=0;i<length;i++){
        for(j=i+1;j<length;j++){
          if(res[i].designationLevel > res[j].designationLevel){
            desig=res[i]
            res[i]=res[j];
            res[j]=desig;
          }
        }
      }      
      this.desgInfo = res;
    },
    error=>{
      console.log('Error'+error);
      
    }
    );
  }
}
