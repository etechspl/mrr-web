import { ToastrService } from 'ngx-toastr';
import { MefService } from '../../../../../core/services/Mef/mef.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
	selector: "m-category",
	templateUrl: "./category.component.html",
	styleUrls: ["./category.component.scss"]
})
export class CategoryComponent implements OnInit {
  
 @Input() categorySelected = [];
	@Output() valueChange = new EventEmitter();
  @Input() check: boolean;
	constructor(private _mefService: MefService, private _toastrService: ToastrService) { }

  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
  
	categoryInfo: any;
	categoryLength: any;

	ngOnInit() {
		this.categoryLength = this.currentUser.company.category.length;
	}

	changedValue(val) {
    this.categorySelected = val;
		this.valueChange.emit(val);
	}
  
	deselectAll() {
    this.changedValue([]);
  }
  
	selectAll() {
    this.changedValue(this.currentUser.company.category);
  }

 
}


