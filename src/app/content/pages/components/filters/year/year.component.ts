import { MonthYearService } from '../../../../../core/services/month-year.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'm-year',
  templateUrl: './year.component.html',
  styleUrls: ['./year.component.scss']
})
export class YearComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);
  selectedYear: number;

  currentYear = new Date().getFullYear();
  @Input() check: boolean = false;

  @Output() valueChange = new EventEmitter();
  constructor(private _monthYearservice: MonthYearService) { }
  yearList;
  monthYear

  ngOnInit() {
    this.monthYear = this._monthYearservice.getMonthAndCurrentYearList(new Date(this.currentUser.company.createdAt).getFullYear());
    //this.monthYear = this._monthYearservice.getFinancialYear();
    this.yearList = this.monthYear[1].year;
    this.selectedYear = this.currentYear;
    this.changedValue(this.selectedYear);
  }

  // getFinantialYear(){
  //   let finantialYear = this._monthYearservice.getFinancialYear();
  //   finantialYear
  // }

  changedValue(val) {
    this.valueChange.emit(val);
  }

  setBlank() {
    this.selectedYear = null;
  }

}
