import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'm-type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.scss']
})
export class TypeComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() selectedType: any;
  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));

  constructor() { }
  typeArray = [];

  Dz=['inTypeComponent']

  ngOnInit() {
  }
  changedValue(val) {
    this.valueChange.emit(val);
  }
  getTypesBasedOnReportingType(reportingType, type) {
    this.selectedType = null;
    if (this.currentUser.userInfo[0].designationLevel == 0) {
      if (reportingType == "Geographical") {
        console.log("type------- : ",type);
        if (type == "WithState") {
          this.typeArray = [{ key: "State", value: "State" }];
        } else if (type == "WithHeadquarter") {
          this.typeArray = [{ key: "State", value: "State" }, { key: this.currentUser.company.lables.hqLabel, value: "Headquarter" }];
        } else if (type == "WithArea") {
          this.typeArray = [{ key: "State", value: "State" }, { key: this.currentUser.company.lables.hqLabel, value: "Headquarter" }, { key: this.currentUser.company.lables.areaLabel, value: "Area" }];
        }else if (type == "WithStateHeadquarterParty") {
          this.typeArray = [{ key: "State Wise", value: "State Wise" }, { key: "Headquarter Wise", value: "Headquarter Wise" },{ key: "Party Wise", value: "Party Wise" }];
        }
      } else if(reportingType == "Hierarachy") {
        //this.typeArray=["Employeewise","Teamwise"];
        if (type == "empWise") {
          this.typeArray = [{key :"Employee Wise",value : "Employee Wise"}];
          
          //[{Key:"Employee Wise",value:"Employee Wise"}];

        } else if (type == "empAndTeamWise") {
          this.typeArray = [{ key: "Employee Wise", value: "Employee Wise" }, { key: "Team Wise", value: "Team Wise" }];

        }else if (type == "managerwise") {
          this.typeArray = [{ key: "Manager Wise", value: "Manager Wise" }];
        }

      }
    } else if (this.currentUser.userInfo[0].designationLevel > 1) {
      if (reportingType == "Geographical") {
        if (type == "WithState") {
          this.typeArray = [{ key: "State", value: "State" }];
        } else if (type == "WithHeadquarter") {
          this.typeArray = [{ key: "State", value: "State" }, { key: this.currentUser.company.lables.hqLabel, value: "Headquarter" }];
        } else if (type == "WithArea") {
          this.typeArray = [{ key: "State", value: "State" }, { key: this.currentUser.company.lables.hqLabel, value: "Headquarter" }, { key: this.currentUser.company.lables.areaLabel, value: "Area" }];
        }else if (type == "WithStateHeadquarterParty") {
          this.typeArray = [{ key: "State Wise", value: "State Wise" }, { key: "Headquarter Wise", value: "Headquarter Wise" },{ key: "Party Wise", value: "Party Wise" }];
        }
      } else  if(reportingType == "Hierarachy")  {
        if (type == "empWise") {
          this.typeArray = [{ key: "Self", value: "Self" }, { key: "Employee Wise", value: "Employee Wise" }];

        } else if (type == "empAndTeamWise") {
          this.typeArray = [{ key: "Self", value: "Self" }, { key: "Employee Wise", value: "Employee Wise" }, { key: "Team Wise", value: "Team Wise" }];

        } else if (type == "onlyEmployeeWise") {
          this.typeArray = [{ key: "Employee Wise", value: "Employee Wise" }];

        } else if (type == "onlyEmployeeWise") {
          this.typeArray = ["Employee Wise"];

        }else if (type == "managerwise") {
          this.typeArray = [{ key: "Manager Wise", value: "Manager Wise" }];
        }
      }
    } else if (this.currentUser.userInfo[0].designationLevel == 1) {
      if (reportingType == "Geographical") {
        if (type == "WithState") {
          this.typeArray = [{ key: "State", value: "State" }];
        } else if (type == "WithHeadquarter") {
          this.typeArray = [{ key: "State", value: "State" }, { key: this.currentUser.company.lables.hqLabel, value: "Headquarter" }];
        } else if (type == "WithArea") {
          this.typeArray = [{ key: "State", value: "State" }, { key: this.currentUser.company.lables.hqLabel, value: "Headquarter" }, { key: this.currentUser.company.lables.areaLabel, value: "Area" }];
        } else if (type == "onlyArea") {
          this.typeArray = [{ key: this.currentUser.company.lables.areaLabel, value: "Area" }];
        } else if (type == "onlyHeadquarter") {
          this.typeArray = [{ key: this.currentUser.company.lables.hqLabel, value: "Headquarter" }];
        } else if (type == "onlyState") {
          this.typeArray = [{ key: "State", value: "State" }];
        }
      } else if(reportingType == "Hierarachy") {
        //this.typeArray=["Employeewise","Teamwise"];
        if (type == "empWise") {
          this.typeArray = [{ key: "Self", value: "Self" }];
        }
      }
    }
  }
  setBlank(){
    this.selectedType = null;
  }
}