import { State } from '../../../_core/models/state.model';
import { StateService } from '../../../../../core/services/state.service';
import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { NgModel } from '@angular/forms';
import { MatOption } from '@angular/material';

/*interface State{
  id:string,
  stateName:string
}*/
@Component({
  selector: 'm-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss']

})
export class StateComponent implements OnInit {
  @ViewChild('allSelected') private allSelected: MatOption;
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  @Input() stateSelected: any;
  selected;
  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
  constructor(private _stateService: StateService) { }
  stateData: any;
  stateIds: any;
  stateIdsLength: number

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne === objTwo;
    }
  }

  selectAll(select: NgModel, values: any) {
    this.changedValue(values)
    select.update.emit(values);
  }

  deselectAll(select: NgModel) {
    this.changedValue([])
    select.update.emit([]);
  }
  ngOnInit() {
      if (this.currentUser.userInfo[0].rL == 0) {
        this._stateService.getStates(this.currentUser.companyId).subscribe(res => {          
          this.stateData = res[0].stateInfo;
          this.stateIds = res[0].stateIds;
          this.stateIdsLength = res[0].stateIds.length;

        }, err => {
          console.log(err)
          alert("Oooops! \nNo Record Found");
        });
      } else if (this.currentUser.userInfo[0].rL > 1) {
        this._stateService.getUniqueStateOnManager(this.currentUser.companyId, this.currentUser.userInfo[0].userId).subscribe(res => {
          res[0].stateObj.sort(function (a, b) {
            if (a.hasOwnProperty("stateName") === true && b.hasOwnProperty("stateName") === true) {
              var textA = a.stateName.toUpperCase();
              var textB = b.stateName.toUpperCase();
              return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            }

          });
          this.stateData = res[0].stateObj;
          this.stateIds = res[0].stateIds;
          this.stateIdsLength = res[0].stateObj.length;
        }, err => {
          console.log(err)
          alert("Oooops! \nNo Record Found");
        });
      }
    
  }


  getUniqueStateOnManager(companyId: string, userId: string) {
    this._stateService.getUniqueStateOnManager(this.currentUser.companyId, this.currentUser.userInfo[0].userId).subscribe(res => {
      res[0].stateObj.sort(function (a, b) {
        var textA = a.stateName.toUpperCase();
        var textB = b.stateName.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
      });
      this.stateData = res[0].stateObj;
      this.stateIds = res[0].stateIds;
      this.stateIdsLength = res[0].stateIds.length;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }
  //-------------------------------PK(2019-06-2019)---------------------------------------------

  getStateBasedOnDivision(divisionObject: any) {   
    divisionObject.companyId = this.currentUser.companyId;
    divisionObject.isDivisionExist = true;
    if (this.currentUser.userInfo[0].rL == 0) {     
      this._stateService.getStatesBasedOnDivision(divisionObject).subscribe(res => {
        if (res.length > 0) {
          this.stateData = res[0].stateInfo;
          this.stateIds = res[0].stateIds;
          this.stateIdsLength = res[0].stateIds.length;
        } else {
          this.stateData = [];
          this.stateIds = [];
          this.stateIdsLength = 0;
        }
      }, err => {
        console.log(err)

        alert("Oooops! \nNo Record Found");
      });
    } else if (this.currentUser.userInfo[0].rL > 1) {
      this._stateService.getUniqueStateOnManager(this.currentUser.companyId, this.currentUser.userInfo[0].userId).subscribe(res => {
        res[0].stateObj.sort(function (a, b) {
          if (a.hasOwnProperty("stateName") === true && b.hasOwnProperty("stateName") === true) {
            var textA = a.stateName.toUpperCase();
            var textB = b.stateName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          }
        });
        this.stateData = res[0].stateObj;
        this.stateIds = res[0].stateIds;
        this.stateIdsLength = res[0].stateObj.length;
      }, err => {
        console.log(err)
        alert("Oooops! \nNo Record Found");
      });
    }
  }

  getStateOnDivision(divisionObject: any) {   
    divisionObject.companyId = this.currentUser.companyId;
    divisionObject.isDivisionExist = true;
    if (this.currentUser.userInfo[0].rL == 0) {     
      this._stateService.getStatesOnDivision(divisionObject).subscribe(res => {
        if (res.length > 0) {
          this.stateData = res[0].stateInfo;
          this.stateIds = res[0].stateIds;
          this.stateIdsLength = res[0].stateIds.length;
        } else {
          this.stateData = [];
          this.stateIds = [];
          this.stateIdsLength = 0;
        }
      });
    } else if (this.currentUser.userInfo[0].rL > 1) {
      this._stateService.getUniqueStateOnManager(this.currentUser.companyId, this.currentUser.userInfo[0].userId).subscribe(res => {
        res[0].stateObj.sort(function (a, b) {
          if (a.hasOwnProperty("stateName") === true && b.hasOwnProperty("stateName") === true) {
            var textA = a.stateName.toUpperCase();
            var textB = b.stateName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          }
        });
        this.stateData = res[0].stateObj;
        this.stateIds = res[0].stateIds;
        this.stateIdsLength = res[0].stateObj.length;
      });
    }
  }

  getUniqueStateOnManagerBasedOnDivision(divisionObject: any) {
    this._stateService.getUniqueStateOnManagerBasedOnDivision(divisionObject).subscribe(res => {

      if (res.length > 0) {
        res[0].stateObj.sort(function (a, b) {
          var textA = a.stateName.toUpperCase();
          var textB = b.stateName.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        this.stateData = res[0].stateObj;
        this.stateIds = res[0].stateIds;
        this.stateIdsLength = res[0].stateIds.length;
      } else {
        this.stateData = [];
        this.stateIds = [];
        this.stateIdsLength = 0;
      }
    }, err => {
      console.log(err)
      this.stateData = [];
      this.stateIds = [];
      this.stateIdsLength = 0;
      alert("Oooops! \nNo Record Found");
    });
  }
  //------------------------------_END----------------------------------------------------------
  changedValue(val) {
    this.valueChange.emit(val);
  }

  setBlank() {
    this.selected = null;
  }
}
