import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {ProviderService} from '../../../../../core/services/Provider/provider.service'
@Component({
  selector: 'm-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean ;
  constructor(private _providerService: ProviderService) { }
  providerData=[];
  selecteddata;
  ngOnInit() {
  }

  getProvidersData(companyId,areaIds,providerType){
    this._providerService.getProviderListBasedOnArea(companyId,areaIds,providerType).subscribe(res => {
      this.providerData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }

  changedValue(val) {
    this.valueChange.emit(val);
  }
  setBlank(){
    this.selecteddata = [];
    this.providerData.length = 0;
  }

}
