import { NgModel } from '@angular/forms';
import { District } from '../../../_core/models/district.model';
import { DistrictService } from '../../../../../core/services/district.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { StateService } from '../../../../../core/services/state.service';

@Component({
  selector: 'm-district',
  templateUrl: './district.component.html',
  styleUrls: ['./district.component.scss'],

})
export class DistrictComponent implements OnInit {

  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  @Input() districtSelected: any;
  constructor(private _districtService: DistrictService, private _stateService: StateService) { }

  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));

  districtData: any;
  districtIds: any;
  districtIdsLength: number;
  // districtSelected: any;
  selected: any;
  ngOnInit() {


  }
  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne === objTwo;
    }
  }

  selectAll(select: NgModel, values: any) {
    this.changedValue(values)
    select.update.emit(values);
  }

  deselectAll(select: NgModel) {
    this.changedValue([])
    select.update.emit([]);
  }
  getDistricts(companyId, stateId, isContainStateFilterValue) {
    if (this.currentUser.userInfo[0].designationLevel == 0) {

      

      this._districtService.getDistrict(companyId, stateId).subscribe(res => {
        if (res.length == 0 || res[0].districtObj == undefined) {
          this.districtData = [];
          this.districtIds = [];
          this.districtIdsLength = 0;
        } else {
          res[0].districtObj.sort(function (a, b) {
            var textA = a.districtName.toUpperCase();
            var textB = b.districtName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
          this.districtData = res[0].districtObj;
          this.districtIds = res[0].districtIds;
          this.districtIdsLength = res[0].districtIds.length;
        }

      }, err => {
        console.log(err)
        alert("Oooops! \nNo Record Found");
      });
    } else if (this.currentUser.userInfo[0].designationLevel > 1) {
      this._stateService.getUniqueHqOnManager(companyId, this.currentUser.userInfo[0].userId, stateId, isContainStateFilterValue).subscribe(res => {

        if (res.length == 0 || res[0].districtObj == undefined) {
          this.districtData = [];
          this.districtIds = [];
          this.districtIdsLength = 0;
        } else {
          res[0].districtObj.sort(function (a, b) {
            var textA = a.districtName.toUpperCase();
            var textB = b.districtName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
          this.districtData = res[0].districtObj;
          this.districtIds = res[0].districtIds;
          this.districtIdsLength = res[0].districtIds.length;
        }

      }, err => {
        console.log(err)
        alert("Oooops! \nNo Record Found");
      });
    }
  }
  //-------------------------------PK(2019-06-2019)---------------------------------------------

  getDistrictsBasedOnDivision(object: any) {
    if (this.currentUser.userInfo[0].designationLevel == 0) {
      this._districtService.getDistrictsBasedOnDivision(object).subscribe(res => {
        if (res.length == 0 || res[0].districtObj == undefined) {
          this.districtData = [];
          this.districtIds = [];
          this.districtIdsLength = 0;
        } else {
          res[0].districtObj.sort(function (a, b) {
            var textA = a.districtName.toUpperCase();
            var textB = b.districtName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
          this.districtData = res[0].districtObj;
          this.districtIds = res[0].districtIds;
          this.districtIdsLength = res[0].districtIds.length;
        }

      }, err => {
        console.log(err)
        alert("Oooops! \nNo Record Found");
      });
    } else if (this.currentUser.userInfo[0].designationLevel > 1) {
      object.managerId = this.currentUser.userInfo[0].userId;
      this._districtService.getUniqueHqOnManagerBasedOnDivision(object).subscribe(res => {

        if (res.length == 0 || res[0].districtObj == undefined) {
          this.districtData = [];
          this.districtIds = [];
          this.districtIdsLength = 0;
        } else {
          res[0].districtObj.sort(function (a, b) {
            var textA = a.districtName.toUpperCase();
            var textB = b.districtName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
          this.districtData = res[0].districtObj;
          this.districtIds = res[0].districtIds;
          this.districtIdsLength = res[0].districtIds.length;
        }

      }, err => {
        console.log(err)
        alert("Oooops! \nNo Record Found");
      });
    }


  }
  //------------------------------------------END----------------------------------------------
  removeList(){
    this.districtData = null;
  }
  changedValue(val) {
    this.valueChange.emit(val);
  }
  setBlank() {
    this.selected = null;
    this.districtSelected = null;
  }

}

