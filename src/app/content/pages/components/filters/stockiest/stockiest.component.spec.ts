import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockiestComponent } from './stockiest.component';

describe('StockiestComponent', () => {
  let component: StockiestComponent;
  let fixture: ComponentFixture<StockiestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockiestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockiestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
