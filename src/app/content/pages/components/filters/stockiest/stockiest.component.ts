import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';

@Component({
  selector: 'm-stockiest',
  templateUrl: './stockiest.component.html',
  styleUrls: ['./stockiest.component.scss']
})
export class StockiestComponent implements OnInit {

  @Output() valueChange = new EventEmitter();
  @Input() check: boolean ;
  constructor(private _stockiestService:StockiestService) { }
  ngOnInit() {
  }
  stockiestData=[];
  getStockiests(companyId,districts){
    this._stockiestService.getStockiest(companyId,districts).subscribe(res => {
      this.stockiestData = res;
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
  }
  changedValue(val) { 
    this.valueChange.emit(val);
  }

}
