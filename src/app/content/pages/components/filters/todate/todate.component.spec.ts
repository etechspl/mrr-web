import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodateComponent } from './todate.component';

describe('TodateComponent', () => {
  let component: TodateComponent;
  let fixture: ComponentFixture<TodateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
