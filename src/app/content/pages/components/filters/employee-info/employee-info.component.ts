import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { UserDetailsService } from '../../../../../core/services/user-details.service';

@Component({
  selector: 'm-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.scss']
})
export class EmployeeInfoComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  constructor(private _userDetailservice: UserDetailsService) { }
  userData: any;
  ngOnInit() {
  }

  //method created by praveen singh for getting employee based on designation & stateId id or destrictId
  getEmployeeDesignationStatewiseOrDistrictWise(params: Object) {
    this._userDetailservice.getEmployeeDesignationStatewiseOrDistrictWise(params).subscribe(res => {
      this.userData = res;
    });
  }

  changedValue(val) {
    this.valueChange.emit(val);
  }
}
