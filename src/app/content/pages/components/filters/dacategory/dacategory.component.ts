import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { DailyallowanceService } from '../../../_core/services/dailyallowance.service';

@Component({
  selector: 'm-dacategory',
  templateUrl: './dacategory.component.html',
  styleUrls: ['./dacategory.component.scss']
})

export class DACategoryComponent implements OnInit {

  constructor(private dailyAllowanceService: DailyallowanceService) { }
 
  categories =[];

  ngOnInit() { 


   }

  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  @Input() placeholder:string;

  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
  isDivisionExist = this.currentUser.company.isDivisionExist;
  selected: any;
  companyId = this.currentUser.company.id;
  division = this.currentUser.company.divisionId;
  
  
  changedValue(val) {
      this.valueChange.emit(val);
  }



  getDACategory(companyId,divisionId){
    let param={
      companyId:companyId,
      divisionId:divisionId
    }

    this.dailyAllowanceService.getDaCategory(param).subscribe(res=>{
      
      this.categories=[];
      res.map(item =>{
        if(item.category){
          if(!this.categories.includes(item.category)){
            this.categories.push(item.category);
          }
        }
      });
      console.log("categories++++++",this.categories);
      console.log("result+++++",res);
    })
   }

  setBlank(){
    this.selected=null;
  }
}
