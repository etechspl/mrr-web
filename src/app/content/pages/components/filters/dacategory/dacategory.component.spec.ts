import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DACategoryComponent } from './dacategory.component';

describe('DACategoryComponent', () => {
  let component: DACategoryComponent;
  let fixture: ComponentFixture<DACategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DACategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DACategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
