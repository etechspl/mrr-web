import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DivisionService } from '../../../../../core/services/Division/division.service';

@Component({
  selector: 'm-division',
  templateUrl: './division.component.html',
  styleUrls: ['./division.component.scss']
})
export class DivisionComponent implements OnInit {

  @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  @Input() required: boolean = true;
  @Input() selected: any;

  currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
  constructor(private _divisionService: DivisionService) { }
  divisionData;
  ngOnInit() {

  }

  getDivisions(object: any) {
    console.log("selected=>",this.selected);
    
    this._divisionService.getDivisions(object).subscribe(res => {  
      console.log("res1--->>>>>>>>>>.",res)

      if (res.length > 0) {
        res[0].divisionObj.sort(function (a, b) {
          var textA = a.divisionName.toUpperCase();
          var textB = b.divisionName.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        this.divisionData = res[0].divisionObj;
      }
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found of Division ");
    });
  }


  getUserDivision(object: any) {
    this._divisionService.getUserDivision(object).subscribe(res => {
      console.log("res--->>>>>>>>>>.",res)
      if (res.length > 0) {
        this.divisionData = res[0].divisionObj;
      }
    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found of Division");
    });
  }

  changedValue(val) {
    this.valueChange.emit(val);
  }

  setBlank() {
    this.selected  = null;
  }

}
