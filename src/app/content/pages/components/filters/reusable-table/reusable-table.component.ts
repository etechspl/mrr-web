import { Component, OnInit,Input ,ViewChild,ElementRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort'; 
import { MatSlideToggle, MatSlideToggleChange ,MatTableDataSource} from '@angular/material';
import * as XLSX from 'xlsx';

@Component({
  selector: 'm-reusable-table',
  templateUrl: './reusable-table.component.html',
  styleUrls: ['./reusable-table.component.scss']
})
export class ReusableTableComponent implements OnInit {
 
  constructor() { }
  @ViewChild('TABLE',{ read: ElementRef }) table: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() tableData: any;
  @Input()dataSource: MatTableDataSource<any>;
  @Input() columnHeader: any;
  objectKeys = Object.keys;
  ngOnInit() {
    console.log(this.tableData);
    console.log(this.columnHeader);
    this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.sort = this.sort;
    }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }

// ---------------------------------------------excel file import -------------------------------------------------
  ExportTOExcel()
  {
    
    const  ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    
    XLSX.writeFile(wb,'SheetJS.xlsx');
    console.log("exported");

  }
 

  ngOnChanges() {
    console.log(this.tableData);
    this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
