import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	Input,
	ChangeDetectorRef,
} from "@angular/core";
import { UserDetailsService } from "../../../../../core/services/user-details.service";
import { HierarchyService } from "../../../../../core/services/hierarchy-service/hierarchy.service";
import { NgModel } from "@angular/forms";
import { MatOption } from "@angular/material";
import Swal from "sweetalert2";

@Component({
	selector: "m-employee",
	templateUrl: "./employee.component.html",
	styleUrls: ["./employee.component.scss"],
})
export class EmployeeComponent implements OnInit {
	@Output() valueChange = new EventEmitter();
	@Input() check: boolean;
	@Input() required: boolean = true;
	constructor(
		private _userDetailservice: UserDetailsService,
		private _hierarchyService: HierarchyService,
		private changeDetectorRef: ChangeDetectorRef
	) {}
	userData: any;
	@Input() stateEmployee: any;
	@Input() type: "";
	selectedValue;
	userIds;
	userIdsLength: number;

	equals(objOne, objTwo) {
		if (typeof objOne !== "undefined" && typeof objTwo !== "undefined") {
			return objOne === objTwo;
		}
	}

	selectAll(select: NgModel, values: any) {
		this.changedValue(values);
		select.update.emit(values);
	}

	deselectAll(select: NgModel) {
		this.changedValue([]);
		select.update.emit([]);
	}

	changedValue(val) {
		this.valueChange.emit(val);
	}

	setBlank() {
		this.selectedValue = null;
	}

	
	ngOnInit() {}

	getEmployeeList(companyId, designationLevel, status) {
		this._userDetailservice
			.getUserInfoBasedOnDesignation(companyId, designationLevel, status)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});

					this.changeDetectorRef.detectChanges();
				},
				(err) => {
					console.log(err);
				}
			);
	}


	getEmployeeListBasedOnState(companyId, designationLevel, status,stateIds) {
		console.log("stateIds",stateIds,designationLevel)
		let filter = {
			where:{
				companyId:companyId,
				stateId: stateIds[0],
				status:true,
				designationLevel:designationLevel[0].designationLevel,
				designation:designationLevel[0].designation
			}
		}
		console.log("filter",filter)
		this._userDetailservice.get(filter).subscribe(res=>{ 
			console.log("filter",res)
			if(res.length>0){
				this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});

					this.changeDetectorRef.detectChanges();
			}
		})
			
	}


	//----------------nipun(01-02-2020) start ---------------

	getEmployeeDesignatiuonWise(
		paramcompanyId,
		paramdistrictId,
		paramdesignationLevel,
		paramstatus
	) {
		let obj = {
			where: {},
		};
		if (
			typeof paramdistrictId == "object" &&
			typeof paramdesignationLevel == "object"
		) {
			obj.where = {
				companyId: paramcompanyId,
				districtId: { inq: paramdistrictId },
				designationLevel: { inq: paramdesignationLevel },
				// status: paramstatus,
			};
		} else {
			obj.where = {
				companyId: paramcompanyId,
				districtId: paramdistrictId,
				designationLevel: paramdesignationLevel,
				status: paramstatus,
			};
		}

		this._userDetailservice
			.getUserDetailsDesignationWise(obj)
			.subscribe((res) => {
				// setTimeout(()=>{this.userData = res;}, 1000);
				this.userData = res;
				this.userIds = res.map((item) => {
					return item.userId;
				});
			});
	}
	//----------------nipun(01-02-2020) end ---------------

	getMgrList(companyId, stateInfo, designationLevel, status, designation?) {
		this._userDetailservice
			.getMgrBasedOnStateAndDesignation(
				companyId,
				stateInfo,
				designationLevel,
				status,
				designation
			)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});
				},
				(err) => {
					console.log(err);
				}
			);
	}

	getEmployeeListOnDistricts(companyId, districts) {
		this._userDetailservice
			.getEmployeeLinkValue(companyId, districts)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});
				},
				(err) => {
					console.log(err);
					alert("Oooops! \nNo Record Found");
				}
			);
	}

	getEmployeeListOfStates(obj) {
		this._userDetailservice
			.getEmployeeListOfStates(obj)
			.subscribe((res) => {
				this.userData = res;
				this.userIds = res.map((item) => {
					return item.userId;
				});
				this.stateEmployee = this.userData;
			});
	}

	//----------------------Rahul For get Emps state/District------------------
	getEmployeeStateDistDesig(companyId, districts, designationLevel) {
		this._userDetailservice
			.getEmployeeStateDistDesig(companyId, districts, designationLevel)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});
				},
				(err) => {
					console.log(err);
					alert("Oooops! \nNo Record Found");
				}
			);
	}

	getEmployeeStateWiseOrDistrictWise(params) {
		this._userDetailservice
			.getEmployeeStateWiseOrDistrictWise(params)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});
				},
				(err) => {
					console.log(err);
					console.log("Errow while getting url");
				}
			);
	}
	getManagerStateWise(companyId, stateId) {
		this._userDetailservice
			.getManagerStatewise(companyId, stateId)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});
				},
				(err) => {
					console.log(err);
					console.log("Errow while getting employees");
				}
			);
	}

	//method created by praveen singh for getting employee based on designation & stateId id or destrictId
	getEmployeeDesignationStatewiseOrDistrictWise(params: Object) {
		this._userDetailservice
			.getEmployeeDesignationStatewiseOrDistrictWise(params)
			.subscribe((res) => {
				this.userData = res;
				this.userIds = res.map((item) => {
					return item.userId;
				});
			});
	}

	getManagerHierarchy(obj: object) {
		this._hierarchyService.getManagerHierarchy(obj).subscribe((res) => {
			this.userData = res;
			this.userIds = res.map((item) => {
				return item.userId;
			});
		});
	}

	getEmployeeListWithMgr(obj: object) {
		this._userDetailservice.getEmployeeListWithMgr(obj).subscribe((res) => {
			this.userData = res;
			this.userIds = res.map((item) => {
				return item.userId;
			});
		});
	}

	//------------------------PK(20-06-2019)------------------------------------------
	getEmployeeListBasedOnDivision(Object) {
		this._userDetailservice
			.getUserInfoBasedOnDesignationAndBasedOnDivision(Object)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});
				},
				(err) => {
					console.log(err);
				}
			);
	}

	//-------------------------------END----------------------------------------------

	

	removelist() {
		this.userData = [];
	}
	//--------------Preeti 24-01-2020------------------
	getFullEmpDetails(empId: string) {
		const data = this.userData.find((res) => res.userId == empId);
		return data;
	}
	//----------------------------

	// --------------------------- By Gourav for getting employee on division 10-06-2021  Start------------------------
	getEmployeeOnDiv(obj){
		console.log(obj);
		this._userDetailservice.getEmployeeOnDivision(obj).subscribe((res)=>{
			this.userData = res;
		})
	}
	// --------------------------- By Gourav for getting employee on division 10-06-2021  End------------------------

	getEmployeeBasedOnDesignation(obj){
		this._userDetailservice.get({where:obj}).subscribe(res=>{
			if(res.length>0){
				this.userData = res;
			}else{
				const Toast = (Swal as any).mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000,
					timerProgressBar: true,
					didOpen: (toast) => {
					  toast.addEventListener('mouseenter', Swal.stopTimer)
					  toast.addEventListener('mouseleave', Swal.resumeTimer)
					}
				  })
				  
				  Toast.fire({
					icon: 'error',
					title: 'No data found'
				  })
			}
		})
	}
}
