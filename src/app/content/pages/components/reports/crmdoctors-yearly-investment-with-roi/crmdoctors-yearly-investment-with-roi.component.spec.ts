import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CRMDoctorsYearlyInvestmentWithROIComponent } from './crmdoctors-yearly-investment-with-roi.component';

describe('CRMDoctorsYearlyInvestmentWithROIComponent', () => {
  let component: CRMDoctorsYearlyInvestmentWithROIComponent;
  let fixture: ComponentFixture<CRMDoctorsYearlyInvestmentWithROIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CRMDoctorsYearlyInvestmentWithROIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CRMDoctorsYearlyInvestmentWithROIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
