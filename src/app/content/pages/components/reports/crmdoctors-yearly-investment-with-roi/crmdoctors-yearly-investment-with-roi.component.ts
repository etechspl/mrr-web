import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { JAN, MatSlideToggleChange, MatTableDataSource } from '@angular/material';
import moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { CrmService } from '../../../../../core/services/CRM/crm.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';
import { TypeComponent } from '../../filters/type/type.component';

@Component({
  selector: 'm-crmdoctors-yearly-investment-with-roi',
  templateUrl: './crmdoctors-yearly-investment-with-roi.component.html',
  styleUrls: ['./crmdoctors-yearly-investment-with-roi.component.scss']
})
export class CRMDoctorsYearlyInvestmentWithROIComponent implements OnInit {

  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  showHeadquarter: boolean = false;
  employeeFilter: boolean = false;
  stateFilter: boolean = true;
  dataSource: MatTableDataSource<any>;
  heading = [];
  arr = [];
  displayedColumns1 = [
    'position',
    'hq',
    'invJan',//-----------------------------------
    'roiJan',
    'invFeb',
    'roiFeb',
    'invMar',
    'roiMar',
    'invApr',
    'roiApr',
    'invMay',
    'roiMay',
    'invJune',
    'roiJune',
    'invJuly',
    'roiJuly',
    'invAug',
    'roiAug',
    'invSep',
    'roiSep',
    'invOct',
    'roiOct',
    'invNov',
    'roiNov',
    'invDec',
    'roiDec' //---------------------------------------
  ];
  displayedColoums = [];
  jan : boolean =false;
  feb : boolean =false;
  mar : boolean =false;
  apr : boolean = false;
  may:boolean = false;
  jun : boolean =false;
  jul: boolean =false;
  aug : boolean =false;
  sep : boolean =false;
  oct :boolean =false;
  nov : boolean = false;
  dec: boolean = false;

  showProcessing: boolean = false;

  constructor(private _crmService: CrmService,
    private _changeDetectService: ChangeDetectorRef,
    private _tosterService: ToastrService) { }

  CRMDoctorYearInvestmentWithROI = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    state: new FormControl(null),
    district: new FormControl(null),
    designation: new FormControl(null),
    employee: new FormControl(null),
    month: new FormControl(null),
    year: new FormControl(null),
    status: new FormControl(true)
  })

  isToggled: boolean = true;
  currentUser = JSON.parse(sessionStorage.currentUser);
  showDivisionFilter: boolean = false;
  isDevisionExist: Boolean = false;

  ngOnInit() {

    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) {
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
    if (this.currentUser.company.isDivisionExist === true) {
      this.isDevisionExist = true;
      this.CRMDoctorYearInvestmentWithROI.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  getReportType(val) {
    if (val === "Geographical") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      if (this.isDevisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }

  }

  getTypeValue(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ type: val })
    if (val === 'State') {
      this.showHeadquarter = false;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callStateComponent.setBlank();
        this.callDivisionComponent.setBlank();
      }
    } else if (val === 'Headquarter') {
      this.showHeadquarter = true;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
        this.callDistrictComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDistrictComponent.setBlank();
      }
    } else if (val === 'Employee Wise') {
      this.stateFilter = false;
      this.showHeadquarter = false;
      this.employeeFilter = true;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;

        this.callEmployeeComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
      }
    }
  }

  getDivisionValue(val) {
    if (this.CRMDoctorYearInvestmentWithROI.value.type == "Employee Wise") {
      //=======================Clearing Filter===================
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    } else if (this.CRMDoctorYearInvestmentWithROI.value.type == "State" || this.CRMDoctorYearInvestmentWithROI.value.type == "Headquarter") {
      //=======================Clearing Filter===================
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      //===============================END=======================
      let divisionIds = {
        division: val
      };
      this.callStateComponent.getStateBasedOnDivision(divisionIds);
    }
  }

  getStateValue(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ state: val })
    if (this.CRMDoctorYearInvestmentWithROI.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.CRMDoctorYearInvestmentWithROI.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrict(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ district: val });
  }

  getDesignation(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ designation: val });
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.CRMDoctorYearInvestmentWithROI.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    }
  }

  getEmployee(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ employee: val })
  }

  getMonth(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ month: val });
  }

  getYear(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ year: val })
  }

  getStatusValue(val) {
    this.CRMDoctorYearInvestmentWithROI.patchValue({ status: val })
  }

  getRecords() {
    this.showProcessing = true;
    this.isToggled = false;
    let fromDate;
    let toDate;
    let obj = {
      companyId: this.currentUser.companyId,
      status: this.CRMDoctorYearInvestmentWithROI.value.status,
      year: this.CRMDoctorYearInvestmentWithROI.value.year,
      stateIds: this.CRMDoctorYearInvestmentWithROI.value.state,
      type: this.CRMDoctorYearInvestmentWithROI.value.type,
      districtIds: this.CRMDoctorYearInvestmentWithROI.value.district,
      userIds: this.CRMDoctorYearInvestmentWithROI.value.employee
    };

    if ((this.CRMDoctorYearInvestmentWithROI.value.month).length > 0) {
      obj['month'] = this.CRMDoctorYearInvestmentWithROI.value.month;
      let val = this.CRMDoctorYearInvestmentWithROI.value.month;
      let l = val.length
      fromDate = new Date(
        this.CRMDoctorYearInvestmentWithROI.value.year,
        val[0] - 1,
        1
      );
      toDate = new Date(
        this.CRMDoctorYearInvestmentWithROI.value.year,
        (val[l - 1]),
        0
      );
    } else {
      obj['month'] = [this.CRMDoctorYearInvestmentWithROI.value.month];
      let val = this.CRMDoctorYearInvestmentWithROI.value.month
      fromDate = new Date(
        this.CRMDoctorYearInvestmentWithROI.value.year,
        (val - 1),
        1
      );
      toDate = new Date(
        this.CRMDoctorYearInvestmentWithROI.value.year,
        val,
        0
      );
    }

    obj['fromDate'] = moment(fromDate).format("YYYY-MM-DD");
    obj['toDate'] = moment(toDate).format('YYYY-MM-DD');

    this._crmService.getCRMYearlyInvestmentWithROIDoctorWiseReport(obj).subscribe((res: any) => {
      if (res.length === 0 || res === null || res === undefined) {
        this.isToggled = true;
        this.showProcessing = false;
        this.alert("error", "No data found for the selection");
        // this._tosterService.error('No Data Found For This Selection');

      } else {
        this.heading = [];
        this.showProcessing = false;
        let result = [];
        let month = this.CRMDoctorYearInvestmentWithROI.value.month
        let year = this.CRMDoctorYearInvestmentWithROI.value.year;
   
        
        if ((month).length > 0) {
          this.arr = ['No', 'dist']
          this.displayedColoums = ['position', 'hq']
          month.forEach(element => {
            let data = new Date(year, element - 1, 1).getMonth()
            this.arr.push((moment().month(data).format('MMM')))
            this.displayedColoums.push('INV'+(moment().month(data).format('MMM')), 'ROI'+(moment().month(data).format('MMM')))
          });
        } else {
          this.arr = ['No', 'dist']
          this.displayedColoums = ['position', 'hq']
          let data = new Date(year, month - 1, 1).getMonth()
          this.arr.push((moment().month(data).format('MMM')))
          this.displayedColoums.push('INV'+(moment().month(data).format('MMM')), 'ROI'+(moment().month(data).format('MMM')))
        }

        res.forEach(async (element) => {
          let month = {}
          element.data.forEach(data => {
            if ((data.month == 1)) {
              month['jan'] = data;
              this.jan = true;
            } else
              if (data.month == 2) {
                month['feb'] = data;
                this.feb=true;
              } else
                if (data.month == 3) {
                  month['mar'] = data;
                  this.mar = true;
                } else
                  if (data.month == 4) {
                    month['apr'] = data;
                    this.apr = true;
                  } else
                    if (data.month == 5) {
                      month['may'] = data;
                      this.may = true;
                    } else
                      if (data.month == 6) {
                        month['june'] = data;
                        this.jun = true;
                      } else
                        if (data.month == 7) {
                          month['july'] = data;
                          this.jul = true;
                        } else
                          if (data.month == 8) {
                            month['aug'] = data;
                            this.aug = true;
                          } else if (data.month == 9) {
                            month['sep'] = data;
                            this.sep = true;
                          } else if (data.month == 10) {
                            month['oct'] = data;
                            this.oct = true;
                          } else if (data.month == 11) {
                            month['nov'] = data;
                            this.nov = true;                        
                          } else if (data.month == 12) {
                            month['dec'] = data;
                            this.dec=true;
                          }
            data['userName'] = element.userName;
            data['providerName'] = element.providerName;
            data['month'] = month
          });
        });

        res.forEach(element => {
          result.push(element.data)
        });
        this.dataSource = new MatTableDataSource(result);
        

        this._changeDetectService.detectChanges();
      }
    })
  }

  alert(icon, title) {
    const Toast = (Swal as any).mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      type: icon,
      title: title
    })
  }

}
