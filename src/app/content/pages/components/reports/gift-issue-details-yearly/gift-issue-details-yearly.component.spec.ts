import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftIssueDetailsYearlyComponent } from './gift-issue-details-yearly.component';

describe('GiftIssueDetailsYearlyComponent', () => {
  let component: GiftIssueDetailsYearlyComponent;
  let fixture: ComponentFixture<GiftIssueDetailsYearlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftIssueDetailsYearlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftIssueDetailsYearlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
