import { Component, OnInit, ViewChild, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import * as _moment from 'moment';
import { forkJoin } from 'rxjs';

import { StateService } from '../../../../../core/services/state.service';
import { District } from '../../../_core/models/district.model';
import { DistrictService } from '../../../../../core/services/district.service';

import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DesignationService } from '../../../../../core/services/Designation/designation.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';

import { ToastrService } from 'ngx-toastr';

import { ProductService } from '../../../../../core/services/Product/product.service';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { DivisionComponent } from '../../filters/division/division.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';

import { StateComponent } from '../../filters/state/state.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { URLService } from '../../../../../core/services/URL/url.service';
import { LatLongService } from '../../../../../core/services/LatLong/lat-long.service';
import { forEach } from '@angular/router/src/utils/collection';
import { SampleService } from '../../../../../core/services/sample/sample.service';
import { UtilsService } from '../../../../../core/services/utils.service';
declare var $;
@Component({
  selector: 'm-gift-issue-details-yearly',
  templateUrl: './gift-issue-details-yearly.component.html',
  styleUrls: ['./gift-issue-details-yearly.component.scss']
})
export class GiftIssueDetailsYearlyComponent implements OnInit {

  @ViewChild("userDataTable") table;

  userDataTable: any;
  userDTOptions: any;

  currentUser = JSON.parse(sessionStorage.currentUser);
  dynamicTableHeader: any = [];

  districtData: any;
  districtIds: any;
  districtIdsLength: number;
  designationData = [];
  giftData:any;
  data: any = [];

  chkArray = [];

  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  DA_TYPE = this.currentUser.company.expense.daType;
  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  isShowDivision = false;
  public showDivisionFilter: boolean = false;
  isShowUsersInfo = false;
  showProcessing: boolean = false;
  giftForm: FormGroup;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;

  constructor(public snackBar: MatSnackBar, private fb: FormBuilder,
    private districtService: DistrictService,
    private utilService: UtilsService,

    private productService: ProductService,
    public toasterService: ToastrService,
    private userDetailservice: UserDetailsService,
    private userAreaMappingService: UserAreaMappingService,
    private changeDetectedRef: ChangeDetectorRef,
    private _urlService: URLService,
    private _latLongService: LatLongService,
    private _samplesService:SampleService
  ) {
    this.giftForm = this.fb.group({
      companyId: null,
      division: new FormControl(null),
      designation: new FormControl(null),
      year:new FormControl(null),
      employeeId: new FormControl(null,Validators.required),

    })
  }

  headquarterData: District;
  isToggled = true; 
  dataTable: any;
  dtOptions: any;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }


  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.isShowDivision = true;
    }
    let object={
      where:{
        companyId:this.currentUser.companyId,
        status:true
      }
    }
    this._samplesService.getGiftDetailsCompanyWise(object)
      .subscribe(sample => {
        if(sample.length){
          this.giftData=sample
        }
               
      }, err => {
      })
  }



  getDivisionValue(val) {
    this.giftForm.patchValue({ division: val });
    if (this.currentUser.userInfo[0].designationLevel == 0) {
      //=======================Clearing Filter===================
      this.giftForm.patchValue({ designation: '' });
      this.giftForm.patchValue({ employeeId: '' });
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================

      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      //=======================Clearing Filter===================
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    }


  }
  getDesignation(val) {

    this.callEmployeeComponent.setBlank();
    this.giftForm.patchValue({ designation: val.designationLevel });
    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.giftForm.value.division
        }
        

        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel] //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.giftForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

    }
  }


  getFromDate(month: object) {
    this.giftForm.patchValue({ month: month })
  }

  getToDate(year: number) {
    this.giftForm.patchValue({ year: year })
  }

  getEmployeeValue(val) {
    this.giftForm.patchValue({ employeeId: val });
  }
  giftDetails(){
   
    let object={
      companyId:this.currentUser.companyId,
      userId:this.giftForm.value.employeeId,
      year:this.giftForm.value.year,
    }

    
    let fromDate =_moment(new Date(this.giftForm.value.year + "-01-01")).format("YYYY-MM-DD");
    let toDate =_moment(new Date(this.giftForm.value.year + "-12-31")).format("YYYY-MM-DD")
    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(fromDate,toDate);
   
    this.isShowUsersInfo = true;

    this._samplesService.getRIBOfGiftIssue(object).subscribe((result)=>{
      if(result.length>0){
        this.showProcessing = false;
        this.data = result;
        this.isShowUsersInfo = true;
        this.changeDetectedRef.detectChanges();

        
      }else{
        this.showProcessing = false;
        this.isShowUsersInfo = false;
        this.changeDetectedRef.detectChanges();
        console.log("error");
        
      }
      
    })
    
    
  }

}

