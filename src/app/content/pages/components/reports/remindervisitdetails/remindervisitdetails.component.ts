
import { ToastrService } from 'ngx-toastr';
import { DcrProviderVisitDetailsService } from './../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TypeComponent } from './../../filters/type/type.component';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { AreaService } from '../../../../../core/services/Area/area.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { CategoryComponent } from '../../filters/category/category.component';
import { URLService } from '../../../../../core/services/URL/url.service';
import { MatSlideToggleChange } from '@angular/material';
import * as XLSX from 'xlsx';
import * as _moment from 'moment';
declare var $;

@Component({
  selector: 'm-remindervisitdetails',
  templateUrl: './remindervisitdetails.component.html',
  styleUrls: ['./remindervisitdetails.component.scss']
})

export class RemindervisitdetailsComponent implements OnInit {

  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showAreaOnMR: boolean = false;
  public showDivisionFilter: boolean = false;

  //dcrForm: FormGroup;
  displayedColumns = ['stateName', 'districtName', 'blockName', 'userName', 'providerName', 'providerCode', 'category', 'address', 'phone', 'status', 'frequencyVisit', 'totalVisit', 'visitDates', 'givenProducts'];
  dataSource;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild('dataTable') table;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callCategoryComponent") callCategoryComponent: CategoryComponent;

  dataTable: any;
  dtOptions: any;
  dtTrigger: any;

  showProcessing: boolean = false;
  showAdminAndMGRLevelFilter = false;
  logoURL: any;
  constructor(private changeDetectorRef: ChangeDetectorRef,
    private _dcrProviderVisitService: DcrProviderVisitDetailsService,
    private _toastr: ToastrService,
    private _areaService: AreaService,
    private _urlService: URLService

  ) {
  }
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  @ViewChild('dataTable') tableRef: ElementRef;

  dcrForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    designation: new FormControl(null),
    toDate: new FormControl(null, Validators.required),
    fromDate: new FormControl(null, Validators.required),
    category: new FormControl(null, Validators.required),
    submitted: new FormControl(null, Validators.required),
  })
  check: boolean;
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
    this.logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }

    if (this.currentUser.company.isDivisionExist == true) {

      this.dcrForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  getReportType(val) {
    this.showReport = false;
    this.callDivisionComponent.setBlank();
    this.dcrForm.patchValue({type: ''});
    if (val === "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showEmployee = false;

      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = false;
      this.showDistrict = false;
      this.showDivisionFilter = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
      }
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = true;
      this.showDistrict = false;
      this.callStateComponent.setBlank();
      this.dcrForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");
      this.dcrForm.removeControl("districtInfo");
    } else if (val == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
      }
      this.callDivisionComponent.setBlank();
      this.callStateComponent.setBlank();
      this.dcrForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.dcrForm.setControl("districtInfo", new FormControl(null, Validators.required))
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");

      this.showState = true;
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.showEmployee = true;
      this.dcrForm.removeControl("stateInfo");
      this.dcrForm.removeControl("districtInfo");
      this.dcrForm.setControl("designation", new FormControl(null, Validators.required))
      this.dcrForm.setControl("employeeId", new FormControl(null, Validators.required))
    } else if (val == "Self") {
      this.showDivisionFilter = false;
      this.showEmployee = false;
    }

    this.dcrForm.patchValue({ type: val });

  }

  submittedStatus(event) {
    console.log('this.dcrForm.value.submitted',this.dcrForm.value.submitted);
	}

  getDivisionValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.dcrForm.patchValue({ division: val });

    if (this.currentUser.userInfo[0].designationLevel == 0) {
      if (this.dcrForm.value.type == "Employee Wise") {
        //=======================Clearing Filter===================

        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.dcrForm.value.type == "State" || this.dcrForm.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================


        let divisionIds = {
          division: val
        };
        this.callStateComponent.getStateBasedOnDivision(divisionIds);
      }
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      if (this.dcrForm.value.type == "Employee Wise") {
        //=======================Clearing Filter===================
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } 
      else if (this.dcrForm.value.type == "State" || this.dcrForm.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================

        let passingObj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: true,
          division: this.dcrForm.value.division,
          supervisorId: this.currentUser.id,
        }
        this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)

      }
    }

  }

  getStateValue(val) {
    console.log("val->",val);
    
    this.showReport = false;
    this.dcrForm.patchValue({ stateInfo: val });

    this.callDistrictComponent.setBlank();

    if (this.dcrForm.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.dcrForm.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ toDate: val });
  }
  getCategory(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.dcrForm.patchValue({ category: val });
  }

  getDesignation(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ designation: val });

    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.dcrForm.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    } else {
      let desig = [];
      for (let i = 0; i < val.length; i++) {
        desig.push(val[i].designationLevel);
      }
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: desig,
          isDivisionExist: true,
          division: this.dcrForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

      // this.callEmployeeComponent.getManagerHierarchy(this.currentUser.companyId, val[0].designationLevel, [true]);
    }
  }

  viewRecords() {
    this.isToggled = false;
    let showEmployeeColumn = true;
    if (this.currentUser.userInfo[0].designationLevel == 1) {
      showEmployeeColumn = false;
    }
    let userIds;
    if (this.dcrForm.value.type == "Self" || this.dcrForm.value.type == null) { //Null is for MR level
      userIds = [this.currentUser.id];
    } else {
      userIds = this.dcrForm.value.employeeId
    }

    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.dcrForm.value.division;
    }

    this.showReport = false;
    this.showProcessing = true;

    
    this._dcrProviderVisitService.getReminderProviderVisitDetails(this.currentUser.companyId, this.dcrForm.value.submitted, this.dcrForm.value.type, this.dcrForm.value.stateInfo, this.dcrForm.value.districtInfo, userIds, this.dcrForm.value.fromDate, this.dcrForm.value.toDate, ["RMP"], this.currentUser.company.validation.unlistedDocCall, this.currentUser.company.isDivisionExist, divisionIds, this.dcrForm.value.category).subscribe(res => {

      if (res == null || res.length == 0 || res == undefined) {
        this.showProcessing = false;
        this.showReport = false;
        this._toastr.info('For Selected Filter data not found', 'Data Not Found');
      } else {
        res.map((item, index) => {
          item['index'] = index;
      });
        this.dataSource = res;
     
        this.showReport = true;

		this.showProcessing = false;
		const PrintTableFunction = this.PrintTableFunction.bind(this);
        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: this.dataSource,

          columns: [
            {
              title: 'S. No.',
              data: 'index',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data + 1;
                }
              },
              defaultContent: '---'
            },
            {
              title: 'State Name',
              data: 'stateName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.hqLabel + ' Name',
              data: 'districtName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }
            ,
            {
              title: this.currentUser.company.lables.areaLabel + ' Name',
              data: 'blockName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'User Name',
              data: 'userName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Division',
              data: 'divisionName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.doctorLabel + ' Name',
              data: 'providerName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Code',
              data: 'providerCode',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Category',
              data: 'category',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },

            {
              title: 'Specialization',
              data: 'specialization',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Address',
              data: 'address',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Phone',
              data: 'phone',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Status',
              data: 'status',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Frequency Visit',
              data: 'frequencyVisit',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Total Visit',
              data: 'totalVisit',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Visit Dates',
              data: 'visitDates',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            { //*******************  Aakash Bist  ****  17-02-2020   *******************
              title: `<table style="width: 100%;">
              <thead><tr><th style=" border-bottom: transparent;" colspan="5" class="text-center">Product Given</th></tr>
              <tr>
                  <th style="width: 55%;  border-bottom: transparent;">Product Name</th>
                  <th style="width: 15%;  border-bottom: transparent;">POB</th>
                  <th style="width: 15%;  border-bottom: transparent;">Order Qty</th>
                  <th style="width: 15%;  border-bottom: transparent;">Sample Qty</th>
              </tr></thead></table>`,
              data: 'givenProducts',
              render: function (data, row) {
                if (data === '' || data == 'null' || data == undefined || data === [] ) {
                  return '---'
                } else {
                  let product = ``;
                  data.forEach(item => {
                    product += `
                    <tr style="border-style: hidden;background-color: transparent;" >
                    <td style="width: 30%;">${item.productName || '---'}</td>
                    <td style="width: 20%;">${item.pob || '---'}</td>
                    <td style="width: 15%;">${item.availableStock || '---'}</td>
                    <td style="width: 15%;">${item.productQuantity || '---'}</td>
                    </tr>
                   `
                  })
                  let table = `<table style="width: 100%;" > ${product}</table>`
                  return (table)
                }

              }
            },
            
            { //*******************  Rahul Saini  **** For Vygon 18-07-2020   *******************
              title: 'Product Name',
              data: 'givenProducts',
              render: function (data, row) {
                if (data === '' || data == 'null' || data == undefined || data === [] ) {
                  return '---'
                } else {
                  let product = ``;
                  data.forEach(item => {
                  product += ` <td style="width: 30%;">${item.productName || '---'}</td> </tr>`})
                  let table = `<table style="width: 100%;" > ${product}   </table>`
                  return (table)
                }

              }
            },

            { //*******************  Rahul Saini  **** For Vygon 18-07-2020   *******************
              title: 'POB',
              data: 'givenProducts',
              render: function (data, row) {
                if (data === '' || data == 'null' || data == undefined || data === [] ) {
                  return '---'
                } else {
                  let product = ``;
                  data.forEach(item => {
                  product += ` <td style="width: 30%;">${item.pob || '---'}</td> </tr>`})
                  let table = `<table style="width: 100%;" > ${product}   </table>`
                  return (table)
                }

              }
            },

            { //*******************  Rahul Saini  **** For Vygon 18-07-2020   *******************
              title: 'Order Qty',
              data: 'givenProducts',
              render: function (data, row) {
                if (data === '' || data == 'null' || data == undefined || data === [] ) {
                  return '---'
                } else {
                  let product = ``;
                  data.forEach(item => {
                  product += ` <td style="width: 30%;">${item.availableStock || '---'}</td> </tr>`})
                  let table = `<table style="width: 100%;" > ${product}   </table>`
                  return (table)
                }

              }
            },

            { //*******************  Rahul Saini  **** For Vygon 17-02-2020   *******************
              title: 'Sample Qty',
              data: 'givenProducts',
              render: function (data, row) {
                if (data === '' || data == 'null' || data == undefined || data === [] ) {
                  return '---'
                } else {
                  let product = ``;
                  data.forEach(item => {
                    product += `
                    
                    <td style="width: 30%;">${item.productQuantity || '---'}</td>
                  
                    </tr>
                   `
                  })
                  let table = `<table style="width: 100%;" > ${product}   </table>`
                  return (table)
                }

              }
            },
            {
              title: 'reminder',
              data: 'reminder',
              render: function (data) {
                if (data === '' || data == 'null' || data == undefined) {
                  return '---'
                } else {
                  return data
                }
              }
            },
             {
              title: 'Discussion',
              data: 'providerDiscussion',
              render: function (data) {
                if (data === '' || data == 'null' || data == undefined) {
                  return '---'
                } else {
                  return data
                }
              }
            }
          ],
          dom: 'Bfrtip',
          buttons: [
           
            {
              extend: 'excel',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'copy',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'csv',
              exportOptions: {
                columns: ':visible'
              }
            },
			{
				extend: 'print',
				title: function () {
				return 'State List';
				},
				exportOptions: {
				columns: ':visible'
				},
				action: function (e, dt, node, config) {
				PrintTableFunction(res);
			    	}
		     	}
          ]
		};
		this.changeDetectorRef.detectChanges()
        if (this.currentUser.company.isDivisionExist == false) {
          this.dtOptions.columns[5].visible = false;
        }

        if(this.currentUser.companyId=="5bea8423e7e95f19bc9bc417"){

          this.dtOptions.columns[16].visible = false;
      
        }
        // else{
        //   this.dtOptions.columns[16].visible = false;
        //   this.dtOptions.columns[19].visible = false;
        // }

        if(this.currentUser.companyId=="5e04bd1b51dc150ad1008e99"){
          this.dtOptions.columns[16].visible = false;
        }
        else{
          this.dtOptions.columns[17].visible = false;
          this.dtOptions.columns[18].visible = false;
          this.dtOptions.columns[19].visible = false;
          this.dtOptions.columns[20].visible = false;
        }
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
            }

      

      this.changeDetectorRef.detectChanges()
    }, err => {
      this.showReport = false;
      this.showProcessing = false;
      this._toastr.info('For Selected Filter data not found', 'Data Not Found');

    })

  }

  

  


PrintTableFunction(data?: any): void {
	// const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
	const currentDateAndTime = _moment(new Date()).format('DD/MM/YYYY, dddd , HH:mm A');
	const department = this.currentUser.section;
	const designation = this.currentUser.designation;
  const companyName = this.currentUser.companyName;
  
  let tablerowwstatename: any=[];
  let tablerowheadquartername: any=[];
  let tableusername: any=[];

	const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

  const tableRows: any = [];

  
  for(let i=0; i<1; i++){
  
    tablerowwstatename.push(`
    
    <td class="tg-oiyu">${data[i].stateName}</td>
    `) 
  
  }

  for(let i=0; i<1; i++){
  
    tablerowheadquartername.push(`
    
    <td class="tg-oiyu">${data[i].districtName}</td>
    `) 
  
  }

  for(let i=0; i<1; i++){
  
    tableusername.push(`
    
    <td class="tg-oiyu">${data[i].userName}</td>
    `) 
  
  }

  

	data.forEach(element => {

    let productnameArray=[];

    let productpobArray=[];

    let productavailableStock=[];

    let productproductQuantity=[];

    let product = ``;
    

    if(element.givenProducts.length>0){

      element.givenProducts.forEach(item => {
       
       
       productnameArray.push(item.productName);

       productpobArray.push(item.pob);

       productavailableStock.push(item.availableStock);

       productproductQuantity.push(item.productQuantity);
      

      })

      product += ` 
      
      <td class="tg-oiyu" >${productnameArray}</td>
      <td class="tg-oiyu">${productpobArray}</td>
      <td class="tg-oiyu">${productavailableStock }</td>
      <td class="tg-oiyu">${productproductQuantity }</td>
     
     `
     
    } else{
     
      product += ` <td class="tg-oiyu" >${'---'}</td>
      <td class="tg-oiyu">${ '---'}</td>
      <td class="tg-oiyu">${'---'}</td>
      <td class="tg-oiyu">${'---'}</td>
     `
  
     }


  tableRows.push(`<tr>
  <td class="tg-oiyu">${element.index}</td>
  <td class="tg-oiyu">${element.districtName}</td>
	<td class="tg-oiyu">${element.blockName}</td>
	<td class="tg-oiyu">${element.providerName}</td>
	<td class="tg-oiyu">${element.frequencyVisit}</td>
	<td class="tg-oiyu">${element.totalVisit}</td>
  <td class="tg-oiyu">${element.visitDates}</td>
   ${product}
	</tr>`)
	});

	let showHeaderAndTable: boolean = false;
	// this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
	let printContents, popupWin;
	popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
	popupWin.document.open();
	popupWin.document.write(`
	<html>
	<head>
	<style>
	img {
	float: left;
	}

	.flex-container {
	display: flex;
	justify-content: space-between;
	}

	.tg {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
  }
  
  .tb2 .tg-oikpyu {
    font-size: 10px;
    text-align: center;
    vertical-align: middle
    }

  
	.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg .tg-lboi {
	border-color: inherit;
	text-align: left;
	vertical-align: middle;

	}

	.tg .tg-u6fn {
	font-weight: bold;
	font-size: 18px;
	background-color: #efefef;
	color:black;
	border-color: inherit;
	text-align: left;
	vertical-align: top;
	padding-top: 0px;
	padding-bottom: 0px;
	}

	.tg .tg-yz93 {
	border-color: inherit;
	text-align: right;
	vertical-align: middle
	}
	.tb2 {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
  }
  
  

	.tb2 td {
	font-family: Arial, sans-serif;
	font-size: 12px;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 th {
	font-family: Arial, sans-serif;
	font-size: 12px;
	font-weight: normal;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 .tg-oiyu {
	font-size: 10px;
	text-align: left;
	vertical-align: middle
	}
	.tg-bckGnd {
	font-weight: bold;
	background-color: #efefef;
	}

	.tb2 .tg-3ppa {
	font-size: 10px;
	background-color: #efefef;
	color:black;
	text-align: left;
	vertical-align: middle;
	padding-top: 2px;
	padding-bottom: 2px;
	}

	.footer {
	position: sticky;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: #efefef;
	color: rgb(2, 2, 2);
	text-align: right
	}

	button {
	background-color: #008CBA;
	border: none;
	color: white;
	margin: 5px 0px;
	padding: 2px 10px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
	border-radius: 12px;
	cursor: pointer;
	outline: none;
	}
	@media print {
	@page { size: A4; margin: 5mm; }
	.footer {
	position: relative;
	}
	tr {
	page-break-inside: avoid;
	}
	button{
	display : none;
	}
	}
	</style>
	</head>
	<body onload="window.print();window.close()" style="padding-bottom:30px">
	<div class="flex-container">
	<div style="width: 40%; ">
	<img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	<h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

	</div>

  <h2 style="margin-top: 0px; margin-bottom: 5px " ; align="left"><u>${this.currentUser.company.lables.doctorLabel} Visit Report </u></h2>
	<div>
	<button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	<button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	</div>
	</div>
	</div>

	<div>
  ${/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/''}
  <table  class="tb2">
  <th class="tg-3ppa"><span style="font-weight:600">State Name </span>
    </th>
    ${tablerowwstatename.join("")}

    <th class="tg-3ppa"><span style="font-weight:600">Headquarter Name </span>
    </th>
    ${tablerowheadquartername.join("")}

    <th class="tg-3ppa"><span style="font-weight:600">User Name </span>
    </th>
    ${tableusername.join("")}

</table>
	<table class="tb2">
	<tr>
  <th class="tg-3ppa"><span style="font-weight:20">S.No.</span></th>
  <th class="tg-3ppa"><span style="font-weight:20">Headquarter Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">AreaName</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">${this.currentUser.company.lables.doctorLabel} Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">Frequency Visit</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">Total Visit</span></th>
  <th class="tg-3ppa"><span style="font-weight:300">Visit Dates</span></th>
  <th class="tg-3ppa">Product Name</th>
  <th class="tg-3ppa">POB</th>
  <th class="tg-3ppa">Order Qty</th>
  <th class="tg-3ppa">Sample Qty</th>
              
	</tr>
	${tableRows.join('')}
	</table>
	</div>
	
	</body>
	</html>
	`);

	// popupWin.document.close();

  }
  exportToExcel(){}
}
