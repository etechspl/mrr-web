import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemindervisitdetailsComponent } from './remindervisitdetails.component';

describe('RemindervisitdetailsComponent', () => {
  let component: RemindervisitdetailsComponent;
  let fixture: ComponentFixture<RemindervisitdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemindervisitdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemindervisitdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
