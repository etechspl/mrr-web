import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { StateComponent } from '../../filters/state/state.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DCRProviderVisitDetailsBackupService } from '../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service';
import { ExportAsService } from 'ngx-export-as';
import { FormGroup, FormControl } from '@angular/forms';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { MatDialog } from '@angular/material';
declare var $;
import * as moment from 'moment';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
@Component({
  selector: 'm-field-force-status-report',
  templateUrl: './field-force-status-report.component.html',
  styleUrls: ['./field-force-status-report.component.scss']
})
export class FieldForceStatusReportComponent implements OnInit {
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;

  showAdminAndMGRLevelFilter = false;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
  constructor(private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private providerService: ProviderService,
    private changeDetectedRef: ChangeDetectorRef,
    private dialog: MatDialog,private _userDetailService:UserDetailsService) { }
    isShowDivision = false;
    currentUser = JSON.parse(sessionStorage.currentUser);
    isDivisionExist = this.currentUser.company.isDivisionExist;
    designationLevel = this.currentUser.userInfo[0].designationLevel;
    companyId = this.currentUser.companyId;
    userForm = new FormGroup({
      reportType: new FormControl('Geographical'),
      type: new FormControl(null),
      //toDate: new FormControl(null),
     // fromDate: new FormControl(null)
    })
    check: boolean = false;
    getReportType(val) {
      
      if (val === "Geographical") {
        this.showState = true;
        this.showEmployee = false;
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      } else {
        this.showState = false;
        this.showDistrict = false;
        this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
      }
    }
    getTypeValue(val) {
      this.showReport = false;
      if (val == "State") {
        this.showDistrict = false;
        this.userForm.setControl("stateInfo", new FormControl())
        this.userForm.removeControl("employeeId");
        this.userForm.removeControl("designation");
      } else if (val == "Headquarter") {
        this.userForm.setControl("stateInfo", new FormControl())
        this.userForm.setControl("districtInfo", new FormControl())
        this.userForm.removeControl("employeeId");
        this.userForm.removeControl("designation");
        this.showDistrict = true;
      } else if (val == "Employee Wise") {
        this.showEmployee = true;
        this.userForm.removeControl("stateInfo");
        this.userForm.removeControl("districtInfo");
        this.userForm.setControl("designation", new FormControl());
        this.userForm.setControl("employeeId", new FormControl());
      } else if (val == "Self") {
        this.showEmployee = false;
      }
  
      if (this.isDivisionExist === true) {
        this.isShowDivision = true;
      } else {
        this.isShowDivision = false;
      }
  
      this.userForm.patchValue({ type: val });
      this._changeDetectorRef.detectChanges();
    }
    getStateValue(val) {
      this.userForm.patchValue({ stateInfo: val });
      if (this.isDivisionExist === true) {
        if (this.userForm.value.type == "Headquarter") {
          this.callDistrictComponent.getDistrictsBasedOnDivision({
            designationLevel: this.designationLevel,
            companyId: this.companyId,
            "stateId": this.userForm.value.stateInfo,
            isDivisionExist: this.isDivisionExist,
            division: this.userForm.value.divisionId
          })
  
          //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
        }
      } else {
        if (this.userForm.value.type == "Headquarter") {
          this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
        }
      }
    }
    getDistrictValue(val) {
      this.userForm.patchValue({ districtInfo: val });
    }
    getEmployeeValue(val) {
      this.userForm.patchValue({ employeeId: val });
    }
    getFromDate(val) {
      this.userForm.patchValue({ fromDate: val });
  
    }
    getToDate(val) {
      this.userForm.patchValue({ toDate: val });
    }
  
    getDesignation(val) {
      this.userForm.patchValue({ designation: val });
      if (this.isDivisionExist === true) {
        this.callEmployeeComponent.getEmployeeListBasedOnDivision({
          companyId: this.companyId,
          division: this.userForm.value.divisionId,
          status: [true],
          designationObject: this.userForm.value.designation
        })
      } else {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
  
    }
    //setting division into userForm
    getDivisionValue($event) {
      this.userForm.patchValue({ divisionId: $event });
      if (this.isDivisionExist === true) {
        if (this.userForm.value.type === "State" || this.userForm.value.type === "Headquarter") {
          this.callStateComponent.getStateBasedOnDivision({
            companyId: this.currentUser.companyId,
            isDivisionExist: this.isDivisionExist,
            division: this.userForm.value.divisionId,
            designationLevel: this.currentUser.userInfo[0].designationLevel
          })
        }
  
        if (this.userForm.value.type === "Employee Wise") {
          //getting employee if we change the division filter after select the designation
          if (this.userForm.value.divisionId != null && this.userForm.value.designation != null) {
            if (this.userForm.value.designation.length > 0) {
              this.callEmployeeComponent.getEmployeeListBasedOnDivision({
                companyId: this.companyId,
                division: this.userForm.value.divisionId,
                status: [true],
                designationObject: this.userForm.value.designation
              })
            }
          }
        }
      }
    }
    ngOnInit() {

      //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      if (this.isDivisionExist === true) {
        this.isShowDivision = true;
        this.userForm.setControl('divisionId', new FormControl());
        let obj = {
          companyId: this.currentUser.companyId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        };
  
        if (this.currentUser.userInfo[0].rL > 1) {
          obj["supervisorId"] = this.currentUser.id;
        }
        this.callDivisionComponent.getDivisions(obj);
  
      } else {
        this.isShowDivision = false;
      }
  
      if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
        this.showAdminAndMGRLevelFilter = true;
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      }
    
  
    }
    viewRecords(){
      this.showReport = false;
      this.showProcessing = true;
      if (this.userForm.value.employeeId == null) {
        this.userForm.value.employeeId = [this.currentUser.id];
      }
      
      let params = {};
      if (this.isDivisionExist === true) {
        params["division"] = this.userForm.value.divisionId;
      } else {
        params["division"] = [];
      }
      params["isDivisionExist"] = this.isDivisionExist;
      params["companyId"] = this.companyId;
      params["stateIds"] = this.userForm.value.stateInfo;
      params["districtIds"] = this.userForm.value.districtInfo;
      params["type"] = this.userForm.value.type;
      params["employeeId"] = this.userForm.value.employeeId;
      params["fromDate"] = this.userForm.value.fromDate;
      params["toDate"] = this.userForm.value.toDate;
      this.providerService.getFieldForceStatus(params).subscribe(res => {

        if (res == null || res == undefined) {
          this.showReport = false;
          this.showProcessing = false;
          this.toastr.info('Record is not found for selected filter.', 'No Record Found');
        } else {
          this.showReport = true;
          this.showProcessing = false;
          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            destroy: true,
            data:res,
            columns: [{
              title: 'Division',
              data: 'divisionName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'State Name',
              data: 'state',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.hqLabel,
              data: 'district',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Employee Name',
              data: 'userName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Active '+ this.currentUser.company.lables.doctorLabel,
              data: 'totalActiveRMP',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title:  'InActive '+ this.currentUser.company.lables.doctorLabel,
              data: 'totalInActiveRMP',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Unlisted '+ this.currentUser.company.lables.doctorLabel,
              data: 'unlistedRMP',
              render: function (data) {
                if (data === '') {

                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Active '+ this.currentUser.company.lables.vendorLabel,
              data: 'totalActiveDrug',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'InActive '+ this.currentUser.company.lables.vendorLabel,
              data: 'totalInActiveDrug',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Unlisted '+ this.currentUser.company.lables.vendorLabel,
              data: 'unlistedDrug',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title:  'Active '+ this.currentUser.company.lables.stockistLabel,
              data: 'totalActiveStockist',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'InActive '+ this.currentUser.company.lables.stockistLabel,
              data: 'totalInActiveStockist',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Unlisted '+ this.currentUser.company.lables.stockistLabel,
              data: 'unlistedStockist',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Active '+ this.currentUser.company.lables.areaLabel,
              data: 'activeArea',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }, {
              title: 'InActive '+ this.currentUser.company.lables.areaLabel,
              data: 'InactiveArea',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }
            ],
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search records",
            },
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'copy',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'print',
                exportOptions: {
                  columns: ':visible'
                }
              }
            ]
          };

          if (this.currentUser.company.isDivisionExist == false) {
            this.dtOptions.columns[0].visible = false;
          }
          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          this._changeDetectorRef.detectChanges();

        }
        
      })

    }

}
