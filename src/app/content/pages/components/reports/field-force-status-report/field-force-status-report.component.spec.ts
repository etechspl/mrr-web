import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldForceStatusReportComponent } from './field-force-status-report.component';

describe('FieldForceStatusReportComponent', () => {
  let component: FieldForceStatusReportComponent;
  let fixture: ComponentFixture<FieldForceStatusReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldForceStatusReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldForceStatusReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
