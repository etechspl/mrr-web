import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
	selector: 'mr-reports',
	templateUrl: './reports.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReportsComponent implements OnInit {
	constructor() { }

	ngOnInit() { }
}
