import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import { PrimaryAndSaleReturnService } from "./../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { StateComponent } from "./../../filters/state/state.component";
import { DivisionComponent } from "./../../filters/division/division.component";
import { TypeComponent } from "./../../filters/type/type.component";
import { MonthComponent } from "./../../filters/month/month.component";
import { EmployeeComponent } from "./../../filters/employee/employee.component";
import { DistrictComponent } from "./../../filters/district/district.component";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { MatSlideToggleChange } from "@angular/material";

@Component({
	selector: "m-secondary-sales-status",
	templateUrl: "./secondary-sales-status.component.html",
	styleUrls: ["./secondary-sales-status.component.scss"],
})
export class SecondarySalesStatusComponent implements OnInit {
	public showState: boolean = true;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	public showDateWiseDCRReport: boolean = false;
	public showProcessing: boolean = false;
	showAdminAndMGRLevelFilter = false;
	data=[];
	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId1", // the id of html/table element
	};

	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("dataTable") table;
	@ViewChild("dataTableForDCRDateWise") dataTableForDCRDateWise;

	constructor(
		private _changeDetectorRef: ChangeDetectorRef,
		private _salesStatus: PrimaryAndSaleReturnService,
		private _toastrService: ToastrService,
		private exportAsService: ExportAsService
	) {}

	isShowDivision = false;
	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	companyId = this.currentUser.companyId;
	salesStatusForm = new FormGroup({
		reportType: new FormControl("Geographical"),
		type: new FormControl(null),
		stateInfo: new FormControl(null),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		month: new FormControl(null, Validators.required),
		year: new FormControl(null, Validators.required),
		designation: new FormControl(null),
	});
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
	getReportType(val) {
		if (val === "Geographical") {
			this.showState = true;
			this.showEmployee = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			this.showState = false;
			this.showDistrict = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
	}

	getTypeValue(val) {
		this.showReport = false;
		if (val == "State") {
			this.showDistrict = false;
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.salesStatusForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.salesStatusForm.removeControl("employeeId");
			this.salesStatusForm.removeControl("designation");
		} else if (val == "Headquarter") {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.callStateComponent.setBlank();
			this.salesStatusForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.salesStatusForm.setControl(
				"districtInfo",
				new FormControl("", Validators.required)
			);
			this.salesStatusForm.removeControl("employeeId");
			this.salesStatusForm.removeControl("designation");
			this.showDistrict = true;
		} else if (val == "Employee Wise") {
			this.showEmployee = true;
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.salesStatusForm.removeControl("stateInfo");
			this.salesStatusForm.removeControl("districtInfo");
			this.salesStatusForm.setControl(
				"designation",
				new FormControl("", Validators.required)
			);
			this.salesStatusForm.setControl(
				"employeeId",
				new FormControl("", Validators.required)
			);
		} else if (val == "Self") {
			this.showEmployee = false;
		}

		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
		} else {
			this.isShowDivision = false;
		}

		this.salesStatusForm.patchValue({ type: val });
		this._changeDetectorRef.detectChanges();
	}

	getStateValue(val) {
		this.salesStatusForm.patchValue({ stateInfo: val });
		if (this.isDivisionExist === true) {
			if (this.salesStatusForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.designationLevel,
					companyId: this.companyId,
					stateId: this.salesStatusForm.value.stateInfo,
					isDivisionExist: this.isDivisionExist,
					division: this.salesStatusForm.value.divisionId,
				});
			}
		} else {
			if (this.salesStatusForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
	}

	getDistrictValue(val) {
		this.salesStatusForm.patchValue({ districtInfo: val });
	}
	getEmployeeValue(val) {
		this.salesStatusForm.patchValue({ employeeId: val });
	}
	getMonth(val) {
		this.showReport = false;
		this.salesStatusForm.patchValue({ month: val });
	}
	getYear(val) {
		this.showReport = false;
		this.salesStatusForm.patchValue({ year: val });
	}

	getDesignation(val) {
		this.salesStatusForm.patchValue({ designation: val });
		if (this.isDivisionExist === true) {
			this.callEmployeeComponent.getEmployeeListBasedOnDivision({
				companyId: this.companyId,
				division: this.salesStatusForm.value.divisionId,
				status: [true],
				designationObject: this.salesStatusForm.value.designation,
			});
		} else {
			this.callEmployeeComponent.getEmployeeList(
				this.currentUser.companyId,
				val,
				[true]
			);
		}
	}
	//setting division into dcrform
	getDivisionValue($event) {
		this.salesStatusForm.patchValue({ divisionId: $event });
		if (this.isDivisionExist === true) {
			if (
				this.salesStatusForm.value.type === "State" ||
				this.salesStatusForm.value.type === "Headquarter"
			) {
				this.callStateComponent.getStateBasedOnDivision({
					companyId: this.currentUser.companyId,
					isDivisionExist: this.isDivisionExist,
					division: this.salesStatusForm.value.divisionId,
					designationLevel: this.currentUser.userInfo[0]
						.designationLevel,
				});
			}

			if (this.salesStatusForm.value.type === "Employee Wise") {
				//getting employee if we change the division filter after select the designation
				if (
					this.salesStatusForm.value.divisionId != null &&
					this.salesStatusForm.value.designation != null
				) {
					if (this.salesStatusForm.value.designation.length > 0) {
						this.callEmployeeComponent.getEmployeeListBasedOnDivision(
							{
								companyId: this.companyId,
								division: this.salesStatusForm.value.divisionId,
								status: [true],
								designationObject: this.salesStatusForm.value
									.designation,
							}
						);
					}
				}
			}
		}
	}

	ngOnInit() {
		//this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.salesStatusForm.setControl("divisionId", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};

			if (this.currentUser.userInfo[0].rL > 1) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}

		if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			this.showAdminAndMGRLevelFilter = true;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		}
	}

	viewRecords() {
		this.isToggled = false;
		this.showProcessing = true;
		this.showReport = false;
		let params = {
			companyId: this.currentUser.companyId,
			stateId: this.salesStatusForm.value.stateInfo,
			districtId: this.salesStatusForm.value.districtInfo,
			userId: this.salesStatusForm.value.employeeId,
			month: this.salesStatusForm.value.month,
			year: this.salesStatusForm.value.year,
			type: this.salesStatusForm.value.type,
			designation: this.salesStatusForm.value.designation,
		};
		this._salesStatus.getSalesStatus(params).subscribe(
			(res) => {
				console.log("duplicate checking.....",res)
				if (!Boolean(res.length)) {
					this.showProcessing = false;
					this._toastrService.info(
						"For Selected Filter data not found",
						"Data Not Found"
					);
					this._changeDetectorRef.detectChanges();
				} else {
					this.showProcessing = false;
					this.showReport = true;
					this.data = res;
					this._changeDetectorRef.detectChanges();
				}
				this._changeDetectorRef.detectChanges();
			},
			(err) => {
				console.log(err);
			}
		);
		this._changeDetectorRef.detectChanges();
	}

	exporttoExcel() {
		// download the file using old school javascript method
		this.exportAsService
			.save(this.exportAsConfig, "Secondary Sales Status")
			.subscribe(() => {
				// save started
			});
	}
}
