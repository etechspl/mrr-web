import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondarySalesStatusComponent } from './secondary-sales-status.component';

describe('SecondarySalesStatusComponent', () => {
  let component: SecondarySalesStatusComponent;
  let fixture: ComponentFixture<SecondarySalesStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondarySalesStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondarySalesStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
