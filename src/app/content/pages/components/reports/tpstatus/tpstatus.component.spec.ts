import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TPStatusComponent } from './tpstatus.component';

describe('TPStatusComponent', () => {
  let component: TPStatusComponent;
  let fixture: ComponentFixture<TPStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TPStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TPStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
