import { ToastrService } from "ngx-toastr";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { StateService } from "../../../../../core/services/state.service";
import {
	FormGroup,
	FormControl,
	Validators,
	FormBuilder,
	NgModel,
} from "@angular/forms";
import { MonthYearService } from "../../../../../core/services/month-year.service";
import { DistrictService } from "../../../../../core/services/district.service";
import { TourProgramService } from "../../../../../core/services/TourProgram/tour-program.service";
import { DistrictComponent } from "../../filters/district/district.component";
import { DivisionComponent } from "../../filters/division/division.component";
import { StateComponent } from "../../filters/state/state.component";
import { MatSlideToggleChange } from "@angular/material";
import { URLService } from "../../../../../core/services/URL/url.service";
declare var $;
import * as _moment from "moment";

@Component({
	selector: "m-tpstatus",
	templateUrl: "./tpstatus.component.html",
	styleUrls: ["./tpstatus.component.scss"],
})
export class TPStatusComponent implements OnInit {
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;

	currentUser = JSON.parse(sessionStorage.currentUser);
	showTPDetails = false;
	check: boolean;
	public showCompleteTPDetails: boolean = false;
	public showProcessing: boolean = false;
	displayedColumnsForTPDetail = [
		"date",
		"submittedActivity",
		"apprvActivity",
		"submittedArea",
		"approvedArea",
		"submittedDoctor",
		"apprvDoctor",
		"submittedChemist",
		"approvedChemists",
		"submittedRemark",
		"apprvRemark",
	];
	dataSourceForTPInDetail = [];

	displayedColumns = [
		"sno",
		"stateName",
		"districtName",
		"name",
		"designation",
		"submitted",
		"submittedDate",
		"approved",
		"approvedDate",
		"userId",
	];
	dataSource = [];

	tpSubmittionDate: any;
	tpApprovalDate: any;
	tpApprovedBy: any;
	employeeName: any;
	tpFilter: FormGroup;

	@ViewChild("dataTable") table;
	@ViewChild("dataTableForTPInDetail") tableForTPInDetail;
	dataTable: any;
	dtOptions: any;
	dataTableForTPInDetail: any;
	dtOptionsForTPInDetail: any;

	public showDivisionFilter: boolean = false;

	isDivisionExist = this.currentUser.company.isDivisionExist;
	submittedFilter = [];

	constructor(
		private fb: FormBuilder,
		private stateService: StateService,
		private districtService: DistrictService,
		private monthYearService: MonthYearService,
		private _tpService: TourProgramService,
		private _changeDetectorRef: ChangeDetectorRef,
		private _toastr: ToastrService,
		private _urlService: URLService
	) {
		this.tpFilter = this.fb.group({
			stateInfo: new FormControl(null, Validators.required),
			districtInfo: new FormControl(null, Validators.required),
			type: "Headquarter", //We are using same method in TP Approval report that why we are taking 'Type' key also.
			month: new FormControl(true),
			year: new FormControl(true),
			submitted: new FormControl(null, Validators.required),
		});
	}
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		console.log("toggle", event.checked);
		this.isToggled = event.checked;
	}

	ngOnInit() {
		this.stateService
			.getERPStateDetail({
				userId: "1733",
			})
			.subscribe((res) => {
				console.log("Result : ", res);
			});

		if (this.currentUser.company.isDivisionExist == true) {
			this.tpFilter.setControl("division", new FormControl());

			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.showDivisionFilter = true;
		}
	}
	getDivisionValue(val) {
		this.showCompleteTPDetails = false;
		this.showTPDetails = false;

		this.tpFilter.patchValue({ division: val });
		//-------Clearing Filter--------------
		this.callStateComponent.setBlank();
		this.callDistrictComponent.setBlank();
		//--------------------END-----------------------------------------
		if (this.currentUser.userInfo[0].designationLevel == 0) {
			let divisionIds = {
				division: val,
			};
			this.callStateComponent.getStateBasedOnDivision(divisionIds);
		} else if (this.currentUser.userInfo[0].designationLevel >= 2) {
			let passingObj = {
				companyId: this.currentUser.companyId,
				isDivisionExist: true,
				division: val,
				supervisorId: this.currentUser.id,
			};
			this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(
				passingObj
			);
		}
	}

	getStateValue(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;
		this.tpFilter.patchValue({ stateInfo: val });

		this.callDistrictComponent.setBlank();

		if (this.currentUser.company.isDivisionExist == true) {
			let passingObj = {
				division: this.tpFilter.value.division,
				companyId: this.currentUser.companyId,
				stateId: val,
			};
			this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
		} else if (this.currentUser.company.isDivisionExist == false) {
			this.callDistrictComponent.getDistricts(
				this.currentUser.companyId,
				val,
				true
			);
		}
	}
	getDistrictValue(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;
		this.tpFilter.patchValue({ districtInfo: val });
	}
	getMonth(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;

		this.tpFilter.patchValue({ month: val });
	}
	getYear(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;
		this.tpFilter.patchValue({ year: val });
	}
	submittedStatus(event) {
		this.submittedFilter = event;
	}
	showReport() {
		this.isToggled = false;
		this.showProcessing = true;
		this.showCompleteTPDetails = false;

		let divisionIds = [];
		if (this.currentUser.company.isDivisionExist) {
			divisionIds = this.tpFilter.value.division;
		}

		if (this.tpFilter.valid) {
			this._tpService
				.getTPStatus(
					this.currentUser.companyId,
					this.tpFilter.value,
					this.currentUser.company.isDivisionExist,
					divisionIds
				)
				.subscribe(
					(res) => {
						console.log(res, 'test')

						this.dataSource = res.filter((tpRes) =>
							this.submittedFilter.includes(tpRes.submitted)
						);
						if (this.dataSource.length > 0) {
							this.showProcessing = false;
							this.showTPDetails = true;
							this.dataSource.map((item, index) => {
								item["index"] = index;
							});
							const PrintTableFunction = this.PrintTableFunction.bind(
								this
							);

							this.dtOptions = {
								pagingType: "full_numbers",
								paging: true,
								ordering: true,
								info: true,
								scrollY: 300,
								scrollX: true,
								fixedColumns: true,
								destroy: true,
								data: this.dataSource,
								responsive: true,
								columns: [
									{
										title: "S.No.",
										data: "index",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data + 1;
											}
										},
										defaultContent: "---",
									},
									{
										title: "Division",
										data: "divisionName",

										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title: "State Name",
										data: "stateName",

										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									
									{
										title:
											this.currentUser.company.lables
												.hqLabel + " Name",
										data: "districtName",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title: "Name",
										data: "name",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title:"Manager name",
										data:"managerName",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title: "Designation",
										data: "designation",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title: "Submitted",
										data: "submitted",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title: "Submittion Date",
										data: "submittedDate",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title: "Approved",
										data: "approvedDate",
										render: function (data) {
											if (data === "") {
												return "---";
											} else {
												return data;
											}
										},
										defaultContent: "---",
									},
									{
										title: "View TP Detail",
										defaultContent: `<button class='btn btn-warning' style='padding: 6px 8px;'><i class='la la-eye'></i>&nbsp;&nbsp;Show TP</button>`,
									},
								],
								rowCallback: (
									row: Node,
									data: any[] | Object,
									index: number
								) => {
									const self = this;
									// Unbind first in order to avoid any duplicate handler
									// (see https://github.com/l-lin/angular-datatables/issues/87)
									$("td", row).unbind("click");
									$("td button", row).bind("click", () => {
										//console.log("row : ", data);
										let rowObject = JSON.parse(
											JSON.stringify(data)
										);
										self.getTPInDetail(
											rowObject.userId,
											rowObject.name
										);
									});
									return row;
								},
								language: {
									search: "_INPUT_",
									searchPlaceholder: "Search records",
								},
								dom: "Bfrtip",
								buttons: [
									{
										extend: "csv",
										exportOptions: {
											columns: ":visible",
										},
									},
									{
										extend: "copy",
										exportOptions: {
											columns: ":visible",
										},
									},
									{
										extend: "print",
										title: function () {
											return "State List";
										},
										exportOptions: {
											columns: ":visible",
										},
										action: function (e, dt, node, config) {
											PrintTableFunction(res);
										},
									},
								],
							};
							this._changeDetectorRef.detectChanges();
							if (
								this.currentUser.company.isDivisionExist ==
								false
							) {
								this.dtOptions.columns[1].visible = false;
							}

							if (
								this.currentUser.company.isDivisionExist == true
							) {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [0, 1, 2, 3, 4, 5, 6, 7, 8],
									},
								});
							} else {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [0, 2, 3, 4, 5, 6, 7, 8],
									},
								});
							}

							this.dataTable = $(this.table.nativeElement);
							this.dataTable.DataTable(this.dtOptions);
							this._changeDetectorRef.detectChanges();
						} else {
							this.showTPDetails = false;
							this.showProcessing = false;
							this._toastr.info(
								"For Selected Filter data not found",
								"Data Not Found"
							);
						}
					},
					(err) => {
						console.log(err);

						this.showProcessing = false;
						this.showTPDetails = false;
						this._toastr.info(
							"For Selected Filter data not found",
							"Data Not Found"
						);
					}
				);
		}
	} //showReport() method end here.

	getTPInDetail(userId, userName) {
		this.employeeName = userName;
		this._tpService
			.viewTourProgram(
				this.currentUser.companyId,
				userId,
				this.tpFilter.value.month,
				this.tpFilter.value.year
			)
			.subscribe(
				(res) => {
					let array = [];
					array.push(res);
					//this.dataSourceForTPInDetail.push(res);
					if (array[0].length > 0) {
						console.log("........", array[0]);

						this.dataSourceForTPInDetail = array[0];
						this.showCompleteTPDetails = true;
						this.tpSubmittionDate = array[0][0].sendForApprovalDate;
						this.tpApprovalDate = array[0][0].approvedAt;
						this.tpApprovedBy = array[0][0].apporvedByName;

						this.dtOptionsForTPInDetail = {
							pagingType: "full_numbers",
							paging: true,
							ordering: true,
							info: true,
							scrollY: 300,
							scrollX: true,
							fixedColumns: true,
							destroy: true,
							data: array[0],
							responsive: true,
							columns: [
								{
									title: "Date",
									data: "date",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Submitted Activity",
									data: "submittedActivity",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Aprroved Activity",
									data: "apprvActivity",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										"Submitted " +
										this.currentUser.company.lables
											.areaLabel,
									data: "submittedArea",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										"Approved " +
										this.currentUser.company.lables
											.areaLabel,
									data: "approvedArea",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										"Submitted " +
										this.currentUser.company.lables
											.doctorLabel,
									data: "submittedDoctor",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										"Approved " +
										this.currentUser.company.lables
											.doctorLabel,
									data: "apprvDoctor",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										"Submitted " +
										this.currentUser.company.lables
											.vendorLabel,
									data: "submittedChemist",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										"Approved " +
										this.currentUser.company.lables
											.vendorLabel,
									data: "approvedChemists",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
							],
							rowCallback: (
								row: Node,
								data: any[] | Object,
								index: number
							) => {
								const self = this;
								// Unbind first in order to avoid any duplicate handler
								// (see https://github.com/l-lin/angular-datatables/issues/87)
								$("td", row).unbind("click");
								$("td button", row).bind("click", () => {
									//console.log("row : ", data);
									let rowObject = JSON.parse(
										JSON.stringify(data)
									);
									self.getTPInDetail(
										rowObject.userId,
										rowObject.name
									);
								});
								return row;
							},
							language: {
								search: "_INPUT_",
								searchPlaceholder: "Search Records",
							},
							dom: "Bfrtip",
							buttons: ["copy", "csv", "excel", "print"],
						};
						this._changeDetectorRef.detectChanges();
						this.dataTableForTPInDetail = $(
							this.tableForTPInDetail.nativeElement
						);
						this.dataTableForTPInDetail.DataTable(
							this.dtOptionsForTPInDetail
						);
						this._changeDetectorRef.detectChanges();
					} else {
						this.showCompleteTPDetails = false;
						this._toastr.info(
							"For Selected Filter data not found",
							"Data Not Found"
						);
					}
					this._changeDetectorRef.detectChanges();
				},
				(err) => {
					this.showCompleteTPDetails = false;
					this._toastr.info(
						"For Selected Filter data not found",
						"Data Not Found"
					);
				}
			);
	}

	/*****************************************UMESH 26-03-2020 *************/

	PrintTableFunction(data?: any): void {
		// const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
		const currentDateAndTime = _moment(new Date()).format(
			"DD/MM/YYYY, dddd , HH:mm A"
		);
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;

		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;

		const tableRows: any = [];
		data.forEach((element) => {
			tableRows.push(`<tr>
			${
				this.isDivisionExist
					? `<td class="tg-oiyu">${element.divisionName}</td>`
					: ""
			} 

	<td class="tg-oiyu">${element.stateName}</td>
	<td class="tg-oiyu">${element.districtName}</td>
	<td class="tg-oiyu">${element.name}</td>
	<td class="tg-oiyu">${element.designation}</td>
	<td class="tg-oiyu">${element.submitted}</td>
	<td class="tg-oiyu">${element.submittedDate}</td>
	<td class="tg-oiyu">${element.approvedDate}</td>
	</tr>`);
		});

		let showHeaderAndTable: boolean = false;
		// this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
		let printContents, popupWin;
		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
	<html>
	<head>
	<style>
	img {
	float: left;
	}

	.flex-container {
	display: flex;
	justify-content: space-between;
	}

	.tg {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg .tg-lboi {
	border-color: inherit;
	text-align: left;
	vertical-align: middle;

	}

	.tg .tg-u6fn {
	font-weight: bold;
	font-size: 18px;
	background-color: #efefef;
	color:black;
	border-color: inherit;
	text-align: left;
	vertical-align: top;
	padding-top: 0px;
	padding-bottom: 0px;
	}

	.tg .tg-yz93 {
	border-color: inherit;
	text-align: right;
	vertical-align: middle
	}
	.tb2 {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tb2 td {
	font-family: Arial, sans-serif;
	font-size: 12px;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 th {
	font-family: Arial, sans-serif;
	font-size: 12px;
	font-weight: normal;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 .tg-oiyu {
	font-size: 10px;
	text-align: left;
	vertical-align: middle
	}
	.tg-bckGnd {
	font-weight: bold;
	background-color: #efefef;
	}

	.tb2 .tg-3ppa {
	font-size: 10px;
	background-color: #efefef;
	color:black;
	text-align: left;
	vertical-align: middle;
	padding-top: 2px;
	padding-bottom: 2px;
	}

	.footer {
	position: sticky;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: #efefef;
	color: rgb(2, 2, 2);
	text-align: right
	}

	button {
	background-color: #008CBA;
	border: none;
	color: white;
	margin: 5px 0px;
	padding: 2px 10px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
	border-radius: 12px;
	cursor: pointer;
	outline: none;
	}
	@media print {
	@page { size: A4; margin: 5mm; }
	.footer {
	position: relative;
	}
	tr {
	page-break-inside: avoid;
	}
	button{
	display : none;
	}
	}
	</style>
	</head>
	<body onload="window.print();window.close()" style="padding-bottom:30px">
	<div class="flex-container">
	<div style="width: 40%; ">
	<img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	<h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

	</div>

	<div style="text-align: right;">
	<h2 style="margin-top: 0px; margin-bottom: 5px">Tour Program Details</h2>
	<div>
	<button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	<button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	</div>
	</div>
	</div>

	<div>
	${
		/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/ ""
	}
	<table class="tb2">
	<tr>
	${
		this.isDivisionExist
			? `<th class="tg-3ppa"><span style="font-weight:600">Division Name</span></th>`
			: ""
	} 
	<th class="tg-3ppa"><span style="font-weight:600">State Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Headquarter</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Designation</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Submitted</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Submitted Date</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Approved</span></th>
	</tr>
	${tableRows.join("")}
	</table>
	</div>
	<div class="footer flex-container">
	<p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>

	<p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
	</div>
	</body>
	</html>
	`);

		// popupWin.document.close();
	}
}
