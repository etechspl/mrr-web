import { ExpenseReportDumpComponent } from './expense-report-dump/expense-report-dump.component';
import { PobProductWiseComponent } from './pobproduct-wise/pobproduct-wise.component';
import { MgrInvestmentReportComponent } from './mgr-investment-report/mgr-investment-report.component';
import { DcrstatusComponent } from './dcrstatus/dcrstatus.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { FiltersModule } from './../filters/filters.module';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { PartialsModule } from '../../../partials/partials.module';
import { ReportsComponent } from './reports.component';


// Core => Services (API)
import { CustomersService } from '../../_core/services/index';
// Core => Utils
import { HttpUtilsService } from '../../_core/utils/http-utils.service';
import { TypesUtilsService } from '../../_core/utils/types-utils.service';
import { LayoutUtilsService } from '../../_core/utils/layout-utils.service';
import { InterceptService } from '../../_core/utils/intercept.service';
// Shared
import { ActionNotificationComponent } from '../../_shared/action-natification/action-notification.component';
import { DeleteEntityDialogComponent } from '../../_shared/delete-entity-dialog/delete-entity-dialog.component';
import { FetchEntityDialogComponent } from '../../_shared/fetch-entity-dialog/fetch-entity-dialog.component';
import { UpdateStatusDialogComponent } from '../../_shared/update-status-dialog/update-status-dialog.component';
import { AlertComponent } from '../../_shared/alert/alert.component';
// Report 1
import { Report1Component } from './report1/report1.component';
// Report 2
import { Report2Component } from './report2/report2.component';

import { TPStatusComponent } from './tpstatus/tpstatus.component';
// Material
import {
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatSelectModule,
    MatMenuModule,
    MatProgressBarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTabsModule,
    MatNativeDateModule,
    MatCardModule,
    MatRadioModule,
    MatIconModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MAT_DIALOG_DEFAULT_OPTIONS,
    MatSnackBarModule,
    MatTooltipModule,
    MatGridListModule,
    MatListModule,
    MatSlideToggleModule
} from '@angular/material';
import { AuthGuard } from '../../../../core/services/auth.guard';
import { DCRConsolidateSummaryComponent } from './dcrconsolidate-summary/dcrconsolidate-summary.component';
import { TeamsTPViewComponent } from './teams-tpview/teams-tpview.component';
import { UserDCRDetailComponent } from './user-dcrdetail/user-dcrdetail.component';
import { UserCompleteDCRInfoComponent } from './user-complete-dcrinfo/user-complete-dcrinfo.component';
import { ProvidervisitdetailComponent } from './providervisitdetail/providervisitdetail.component';
import { VendorvisitdetailComponent } from './vendorvisitdetail/vendorvisitdetail.component';
import { ProviderListComponent } from './provider-list/provider-list.component';
import { UntouchedProviderComponent } from './untouched-provider/untouched-provider.component';
import { UserStokistMappedComponent } from './user-stokist-mapped/user-stokist-mapped.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { SssViewComponent } from './sss-view/sss-view.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ConsolidateSssComponent } from './consolidate-sss/consolidate-sss.component';
import { MatTableExporterModule } from 'mat-table-exporter';

import { ProviderVisitAnalysisComponent } from './provider-visit-analysis/provider-visit-analysis.component';
import { MgrJointWorkComponent } from './mgr-joint-work/mgr-joint-work.component';
import { EdituserComponent } from './edituser/edituser.component';
import { TPApprovalComponent } from './tpapproval/tpapproval.component';
import { TpDeviationComponent } from './tp-deviation/tp-deviation.component';
import { MgrTeamPerformanceComponent } from './mgr-team-performance/mgr-team-performance.component';
import { SamplesIssueComponent } from '../master/samples-issue/samples-issue.component';
import { ViewSampleIssueComponent } from './view-sample-issue/view-sample-issue.component';

import { TargetViewComponent } from './target-view/target-view.component';
import { EmployeesAttendanceComponent } from './employees-attendance/employees-attendance.component';
import { ExpenseReportComponent } from './expense-report/expense-report.component';
import { FinalizeExpenseComponent } from './finalize-expense/finalize-expense.component';
import { ExpenseModifyComponent } from './expense-modify/expense-modify.component';
import { TargetVsAchievementStateAndDistrictWiseComponent } from './target-vs-achievement-state-and-district-wise/target-vs-achievement-state-and-district-wise.component';
import { SampleGiftIssueComponent } from './sample-gift-issue/sample-gift-issue.component';
import { ViewMefTrackerComponent } from './view-mef-tracker/view-mef-tracker.component';
import { YealrSponserDoctorsRoiComponent } from './yealr-sponser-doctors-roi/yealr-sponser-doctors-roi.component';
import { ConsolidatedTourProgramComponent } from './consolidated-tour-program/consolidated-tour-program.component';
import { ViewProviderComponent } from './view-provider/view-provider.component';
import { StationWiseWorkingComponent } from './station-wise-working/station-wise-working.component';
import { ExpenseConsolidatedReportComponent } from './expense-consolidated-report/expense-consolidated-report.component';
import { CallAverageAndWorkingDayComponent } from './call-average-and-working-day/call-average-and-working-day.component';
import { POBReportComponent } from './pobreport/./pobreport.component';;
import { MatRangeDatepickerModule, MatRangeNativeDateModule } from 'mat-range-datepicker';

import { DayPlanAttendanceComponent } from './day-plan-attendance/day-plan-attendance.component';
import { BlockUserComponent } from './block-user/block-user.component';
import { TestComponentComponent } from './test-component/test-component.component';
import { ResignReportComponent } from './resign-report/resign-report.component';
import { ResignedTPStatusComponent } from './resigned-tpstatus/resigned-tpstatus.component';
import { ViewDCRResignedUserComponent } from './view-dcrresigned-user/view-dcrresigned-user.component';
import { DCRViewReportDoctorWiseComponent } from './dcrview-report-doctor-wise/dcrview-report-doctor-wise.component';
import { SpecialtyWiseDoctorCountComponent } from './specialty-wise-doctor-count/specialty-wise-doctor-count.component';
import { HQWiseSSSReportComponent } from './hqwise-sssreport/hqwise-sssreport.component';
import { ManagerAnalysisComponent } from './manager-analysis/manager-analysis.component';

import { ConsolidatedExpenseComponent } from './consolidated-expense/consolidated-expense.component';
import { MRQtyValuewiseSecondarySaleComponent } from './mrqty-valuewise-secondary-sale/mrqty-valuewise-secondary-sale.component';
import { ProductStatewiseSaleComponent } from './product-statewise-sale/product-statewise-sale.component';
import { StockistWiseSaleReportComponent } from './stockist-wise-sale-report/stockist-wise-sale-report.component';
import { SecondarySalesStatusComponent } from './secondary-sales-status/secondary-sales-status.component';
import { ManagerwiseSSSReportComponent } from './managerwise-sssreport/managerwise-sssreport.component';
import { CategorywiseSaleComponent } from './categorywise-sale/categorywise-sale.component';
import { ProductStockistWiseQtyValueSaleComponent } from './product-stockist-wise-qty-value-sale/product-stockist-wise-qty-value-sale.component';
import { DCRStatusReportComponent } from './dcrstatus-report/dcrstatus-report.component';
import { TargetAndAchievementReportComponent } from './target-and-achievement-report/target-and-achievement-report.component';
import { JointFieldWorkReportComponent } from './joint-field-work-report/joint-field-work-report.component';
import { LeaveApprovalComponent } from './leave-approval/leave-approval.component';
import { FieldForceStatusReportComponent } from './field-force-status-report/field-force-status-report.component';
import { ERPStateWiseReportComponent } from './erpstate-wise-report/erpstate-wise-report.component';
import { EditLeaveApprovalComponent } from './edit-leave-approval/edit-leave-approval.component';
import { SponsorshipApprovalComponent } from './sponsorship-approval/sponsorship-approval.component';
import { HolidayReportComponent } from './holiday-report/holiday-report.component';
import { MEFReportComponent } from './mefreport/mefreport.component';
import { NewReportsComponent } from './new-reports/new-reports.component';
import { ExpenseUpdateComponent } from './expense-update/expense-update.component';
import { MasterDataReportComponent } from './master-data-report/master-data-report.component';
import { ProvideLocationOnOffSystemComponent } from './provide-location-on-off-system/provide-location-on-off-system.component';
import { TpLockOpenReportComponent } from './tp-lock-open-report/tp-lock-open-report.component';
import { StationdetailsComponent } from './stationdetails/stationdetails.component';
import { ProviderUpdatedLocationComponent } from './provider-updated-location/provider-updated-location.component';
import { GiftIssueDetailReportGiftWiseComponent } from './gift-issue-detail-report-gift-wise/gift-issue-detail-report-gift-wise.component';
import { EmployeedcrattendanceComponent } from './employeedcrattendance/employeedcrattendance.component';
import { GiftIssueReportDateWiseComponent } from './gift-issue-report-date-wise/gift-issue-report-date-wise.component';
import { GiftIssueDetailsYearlyComponent } from './gift-issue-details-yearly/gift-issue-details-yearly.component';
import { DayStartDayEndReportComponent } from './day-start-day-end-report/day-start-day-end-report.component';
import { PrimarySaleStateHqMgrWiseComponent } from './primary-sale-state-hq-mgr-wise/primary-sale-state-hq-mgr-wise.component';
import { EditAssignedProductGroupComponent } from '../master/edit-assigned-product-group/edit-assigned-product-group.component';
import { POBReportStockistReportComponent } from './pobreport-stockist-report/pobreport-stockist-report.component';
import { SummaryPOBReportComponent } from './summary-pobreport/summary-pobreport.component';
import { RCPAReportComponent } from './rcpareport/rcpareport.component';
import { SampleReportProviderClickWiseComponent } from './sample-report-provider-click-wise/sample-report-provider-click-wise.component';
import { SampleIssueReportDateWiseComponent } from './sample-issue-report-date-wise/sample-issue-report-date-wise.component';
import { SampleIssueDetailReportSampleWiseComponent } from './sample-issue-detail-report-sample-wise/sample-issue-detail-report-sample-wise.component';
import { EmploeeWiseCallAverageComponent } from './emploee-wise-call-average/emploee-wise-call-average.component';
import { AreavisitdetailsComponent } from './areavisitdetails/areavisitdetails.component';
import { SampleDistributedIssuedBalanceDetailsComponent } from './sample-distributed-issued-balance-details/sample-distributed-issued-balance-details.component';
import { RemindervisitdetailsComponent } from './remindervisitdetails/remindervisitdetails.component';
import { TargetVsAchivementERPReportComponent } from './target-vs-achivement-erpreport/target-vs-achivement-erpreport.component';
import { TaegetViewFinancialYearWiseComponent } from './taeget-view-financial-year-wise/taeget-view-financial-year-wise.component';
import { EmployeeLoginDetailsComponent } from './employee-login-details/employee-login-details.component';
import { SalesReportHqWiseComponent } from './sales-report-hq-wise/sales-report-hq-wise.component';
import { SssPerformanceWiseComponent } from './sss-performance-wise/sss-performance-wise.component';
import { SponsorshipViewComponent } from './sponsorship-view/sponsorship-view.component';
import { GenerateExcelOnSelectionComponent } from './generate-excel-on-selection/generate-excel-on-selection.component';
import { TargetVsAchievementHqWiseComponent } from './target-vs-achievement-hq-wise/target-vs-achievement-hq-wise.component';
import { StockAndSalesDumpComponent } from './stock-and-sales-dump/stock-and-sales-dump.component';
import { MissedProviderCountsComponent } from './missed-provider-counts/missed-provider-counts.component';
import { StokistVistReportComponent } from './stokist-vist-report/stokist-vist-report.component';
import { EmployeeLeaveDetailsComponent } from './employee-leave-details/employee-leave-details.component';
import { SalesReportMultiMonthComponent } from './sales-report-multi-month/sales-report-multi-month.component';
import { StpCreationComponent } from './stp-creation/stp-creation.component';
import { EditStpComponent } from '../master/edit-stp/edit-stp.component';
import { CRMDoctorsYearlyInvestmentWithROIComponent } from './crmdoctors-yearly-investment-with-roi/crmdoctors-yearly-investment-with-roi.component';
import { LeaveStatusComponent } from './leave-status/leave-status.component';
import { DCRStatusReportsComponent } from './dcrstatus-reports/dcrstatus-reports.component';
import { ProviderlocationviweandeditComponent } from './providerlocationviweandedit/providerlocationviweandedit.component';
import { DcrCallDetailsComponent } from './dcr-call-details/dcr-call-details.component';
import { UserVersionReportComponent } from './user-version-report/user-version-report.component';
import { EmploeeWisedayCallAverageComponent } from './emploee-wiseday-call-average/emploee-wiseday-call-average.component';
import { TargetvsachievementComponent } from './targetvsachievement/targetvsachievement.component';
import { CampdayreportComponent } from './campdayreport/campdayreport.component';
import { ChannelformdetailsComponent } from './channelformdetails/channelformdetails.component';



const routes: Routes = [
    {
        path: '',
        component: ReportsComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                redirectTo: 'report1',
                pathMatch: 'full'
            },
            {
                path: 'report1',
                component: Report1Component
            },
            {
                path: 'report2',
                component: Report2Component
            }, {
                path: 'tpstatus',
                component: TPStatusComponent
            }, {
                path: 'viewteamstp',
                component: TeamsTPViewComponent
            },
            {
                path: 'dcrsummary',
                component: DCRConsolidateSummaryComponent
            },
            {
                path: 'userdcrs',
                component: UserDCRDetailComponent
            },
            {
                path: 'usercompletedcr',
                component: UserCompleteDCRInfoComponent
            },
            {
                path: 'userView',
                component: UserDetailsComponent
            },
            {
                path: 'providervisitdetail',
                component: ProvidervisitdetailComponent
            },
            {
                path: 'remindervisitdetail',
                component: RemindervisitdetailsComponent
            },
            {
                path: 'areavisitdetail',
                component: AreavisitdetailsComponent
            },
            {
                path: 'vendorvisitdetail',
                component: VendorvisitdetailComponent
            },
            {
                path: 'viewproviderlist',
                component: ProviderListComponent
            },
            {
                path: 'untouchedProvider',
                component: UntouchedProviderComponent
            }, {
                path: 'userStockistMapped',
                component: UserStokistMappedComponent
            },
            {
                path: 'sssView',
                component: SssViewComponent
            },
            {
                path: 'consolidateSSS',
                component: ConsolidateSssComponent
            },
            {
                path: 'productView',
                component: ProductViewComponent
            },
            {
                path: 'providerVisitAnalysis',
                component: ProviderVisitAnalysisComponent
            }, {
                path: 'mgrJointWork',
                component: MgrJointWorkComponent
            }, {
                path: 'tpApproval',
                component: TPApprovalComponent
            }, {
                path: 'tpDeviation',
                component: TpDeviationComponent
            }, {
                path: 'mgrTeamPerformance',
                component: MgrTeamPerformanceComponent
            }, {
                path: 'userdetails',
                component: UserDetailsComponent
            }, {
                path: 'dcrstatus',
                component: DcrstatusComponent
            }, {
                path: 'viewSampleIssue',
                component: ViewSampleIssueComponent
            }, {
                path: 'excelOnSelection',
                component: GenerateExcelOnSelectionComponent
            }, {
                path: 'SampleDistributedIssuedBalanceDetailsComponent',
                component: SampleDistributedIssuedBalanceDetailsComponent
            }, {
                path: 'targetView',
                component: TargetViewComponent
            }, {
                path: 'EmployeeAttendance',
                component: EmployeesAttendanceComponent
            }, {
                path: 'DayPlanEmployeeAttendance',
                component: DayPlanAttendanceComponent
            }, {
                path: 'expenseReport',
                component: ExpenseReportComponent
            }, {
                path: 'finalizeExpense',
                component: FinalizeExpenseComponent
            }, {
                path: 'TargetVsAchievementStateAndDistrict',
                component: TargetVsAchievementStateAndDistrictWiseComponent
            }, {
                path: 'sampleGiftIssueReport',
                component: SampleGiftIssueComponent
            }, {
                path: 'viewMEFTracker',
                component: ViewMefTrackerComponent
            }, {
                path: 'sponserdoctorsroi',
                component: YealrSponserDoctorsRoiComponent
            }, {
                path: 'ConsolidatedTourProgram',
                component: ConsolidatedTourProgramComponent
            }, {
                path: 'viewProvider',
                component: ViewProviderComponent
            }, {
                path: 'viewStationWiseWorking',
                component: StationWiseWorkingComponent
            }, {
                path: 'viewConsolidatedExpense',
                component: ExpenseConsolidatedReportComponent
            }, {
                path: 'callAverageAndWorkingDay',
                component: CallAverageAndWorkingDayComponent
            }, {
                path: 'test',
                component: TestComponentComponent
            }, {
                path: 'ResignReport',
                component: ResignReportComponent
            }, {
                path: 'DCRReportDoctorWise',
                component: DCRViewReportDoctorWiseComponent
            }, {
                path: 'POBReport',
                component: POBReportComponent
            },
            {
                path: 'specialtyWiseDoctorCount',
                component: SpecialtyWiseDoctorCountComponent
            },
            {
                path: 'stokistWiseReport',
                component: StokistVistReportComponent
            },
            {
                path: 'manageranalysis',
                component: ManagerAnalysisComponent
            },
            {
                path: 'consolidatedexpense',
                component: ConsolidatedExpenseComponent
            },
            {
                path: 'MRQtyValuewiseSecondarySaleComponent',
                component: MRQtyValuewiseSecondarySaleComponent
            }, {
                path: 'productStatewiseSale',
                component: ProductStatewiseSaleComponent
            },
            {
                path: 'ConsolidatedSSSReport',
                component: HQWiseSSSReportComponent
            },
            {
                path: 'stockistWiseMgrReport',
                component: StockistWiseSaleReportComponent
            },
            {
                path: 'salesStatus',
                component: SecondarySalesStatusComponent
            }, {
                path: 'managerwiseSSS',
                component: ManagerwiseSSSReportComponent

            },
            {
                path: 'categorywiseSale',
                component: CategorywiseSaleComponent
            },
            {
                path: 'productStockistWiseQtyvalueSale',
                component: ProductStockistWiseQtyValueSaleComponent
            }, {
                path: 'dcrStatusReport',
                component: DCRStatusReportComponent
            }, {
                path: 'TargetVsAchievementReport',
                component: TargetAndAchievementReportComponent
            }, {
                path: 'targetViewFinancialYearWise',
                component: TaegetViewFinancialYearWiseComponent
            },
            {
                path: 'JointFieldWorkReport',
                component: JointFieldWorkReportComponent
            }, {
                path: 'FieldForceStatusReport',
                component: FieldForceStatusReportComponent
            }, {
                path: 'ERPStateWiseReport',
                component: ERPStateWiseReportComponent
            }, {
                path: 'targetVsAchivementERPReport',
                component: TargetVsAchivementERPReportComponent
            }, {
                path: 'targetVsAchievementERPHqWise',
                component: TargetVsAchievementHqWiseComponent
            },
            {
                path: 'leaveApproval',
                component: LeaveApprovalComponent
            },

            {
                path: 'EmployeeDCRAttandance',
                component: EmployeedcrattendanceComponent
            },
            {
                path: 'sponsorshipApproval',
                component: SponsorshipApprovalComponent
            }, {
                path: 'holidayReport',
                component: HolidayReportComponent
            }, {
                path: 'MEFReport',
                component: MEFReportComponent
            },
            {
                path: 'newReports',
                component: NewReportsComponent
            },
            {
                path: 'ExpenseUpdate',
                component: ExpenseUpdateComponent
            },
            {
                path: 'MasterData',
                component: MasterDataReportComponent
            },
            {
                path: 'StationDetails',
                component: StationdetailsComponent
            },
            {
                path: 'mgrInvestmentView',
                component: MgrInvestmentReportComponent
            },
            {
                path: 'tpLockOpenReport',
                component: TpLockOpenReportComponent
            },
            {
                path: 'ProvideLocationOnOffSystem',
                component: ProvideLocationOnOffSystemComponent
            },
            {
                path: 'GiftIssueDetailReportGiftWise',
                component: GiftIssueDetailReportGiftWiseComponent
            },

            {
                path: 'SampleIssueDetailReportSampleWise',
                component: SampleIssueDetailReportSampleWiseComponent
            }
            ,
            {
                path: 'GiftIssueReportDateWise',
                component: GiftIssueReportDateWiseComponent
            },
            {
                path: 'SampleIssueReportDateWise',
                component: SampleIssueReportDateWiseComponent
            },
            {
                path: 'GiftIssueDetailsYearly',
                component: GiftIssueDetailsYearlyComponent
            }, {
                path: 'DayStartDayEndReport',
                component: DayStartDayEndReportComponent
            }, {
                path: 'productWisePOB',
                component: PobProductWiseComponent
            },
            {
                path: 'PrimarySaleStateHqMgrWise',
                component: PrimarySaleStateHqMgrWiseComponent
            },
            {
                path: 'salesReportHqWise',
                component: SalesReportHqWiseComponent
            },
            {
                path: 'POBReportStockistReport',
                component: POBReportStockistReportComponent
            },
            {
                path: 'RCPA',
                component: RCPAReportComponent
            },
            {
                path: 'SummaryPOBReport',
                component: SummaryPOBReportComponent
            },
            {
                path: 'SampleReportProviderClickWise',
                component: SampleReportProviderClickWiseComponent
            }, {
                path: 'EmploeeWiseCallAverage',
                component: EmploeeWiseCallAverageComponent
            },

            {
                path: 'EmployeeLoginDetails',
                component: EmployeeLoginDetailsComponent
            },

            {
                path: 'sss-performance-wise',
                component: SssPerformanceWiseComponent
            },
            {
                path: 'crm-sponsorship',
                component: SponsorshipViewComponent
            },
            {
                path: 'sssDump',
                component: StockAndSalesDumpComponent
            },
            {
                path: 'missedProviderCount',
                component: MissedProviderCountsComponent
            }, {
                path: 'employeeLeaveDetails',
                component: EmployeeLeaveDetailsComponent
            }, {
                path: 'salesReportMultiMonth',
                component: SalesReportMultiMonthComponent
            },{
                path: 'createSTP',
                component: StpCreationComponent
            },
            {
                path: 'expenseDump',
                component: ExpenseReportDumpComponent
            },
            {
                path: 'CRMDoctorsYearlyInvestmentWithROI',
                component: CRMDoctorsYearlyInvestmentWithROIComponent
            },
            {
                path: 'leaveStatus',
                component: LeaveStatusComponent
            },
            {
                path:'dcrStatusReports',
                component: DCRStatusReportsComponent
            }
            ,
            {
                path:'Providerlocationviweandedit',
                component: ProviderlocationviweandeditComponent
            },
            {
                path:'dcrCallDetails',
                component: DcrCallDetailsComponent
            },
            {
                path:'UserVersionReport',
                component: UserVersionReportComponent
            },
            {
                path:'EmploeeWisedayCallAverage',
                component: EmploeeWisedayCallAverageComponent
            },{
                path:'Targetvsachievement',
                component: TargetvsachievementComponent
            },{
                path:'Campdayreport',
                component: CampdayreportComponent
            },
            {
                path:'channelform',
                component: ChannelformdetailsComponent
            }
        ]
    }];


@NgModule({
    imports: [
        MatDialogModule,
        CommonModule,
        HttpClientModule,
        PartialsModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        MatButtonModule,
        MatMenuModule,
        MatSelectModule,
        MatInputModule,
        MatTableModule,
        MatAutocompleteModule,
        MatRadioModule,
        MatIconModule,
        MatNativeDateModule,
        MatProgressBarModule,
        MatDatepickerModule,
        
        MatCardModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatTabsModule,
        MatTooltipModule,
        FiltersModule,
        MatGridListModule,
        MatListModule,
        MatTableExporterModule,
        MatRangeDatepickerModule,
        MatRangeNativeDateModule,
        MatSlideToggleModule

    ],
    providers: [
        InterceptService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptService,
            multi: true
        },
        {
            provide: MAT_DIALOG_DEFAULT_OPTIONS,
            useValue: {
                hasBackdrop: true,
                panelClass: 'm-mat-dialog-container__wrapper',
                height: 'auto',
                width: '900px'
            }
        },
        HttpUtilsService,
        CustomersService,
        TypesUtilsService,

        LayoutUtilsService
    ],
    entryComponents: [
        ActionNotificationComponent,
        DeleteEntityDialogComponent,
        FetchEntityDialogComponent,
        UpdateStatusDialogComponent,
        UserDetailsComponent,
        EdituserComponent,
        ResignedTPStatusComponent,
        EditProductComponent,
        ExpenseModifyComponent,
        BlockUserComponent,
        ViewDCRResignedUserComponent,
        EditLeaveApprovalComponent,
        EditAssignedProductGroupComponent
    ],
    declarations: [
        ReportsComponent,
        // Shared
        ActionNotificationComponent,
        DeleteEntityDialogComponent,
        FetchEntityDialogComponent,
        UpdateStatusDialogComponent,
        AlertComponent,
        // Report1
        Report1Component,
        // Report2
        Report2Component,
        TPStatusComponent,
        DCRConsolidateSummaryComponent,
        TeamsTPViewComponent,
        UserDCRDetailComponent,
        UserCompleteDCRInfoComponent,
        VendorvisitdetailComponent,
        ConsolidateSssComponent,
        ProvidervisitdetailComponent,
        RemindervisitdetailsComponent,
        ProductViewComponent,
        UntouchedProviderComponent,
        UserStokistMappedComponent,
        ProductViewComponent,
        UserStokistMappedComponent,
        ProviderListComponent,
        EditProductComponent,
        SssViewComponent,
        UserDetailsComponent,
        ProviderVisitAnalysisComponent,
        MgrJointWorkComponent,

        EdituserComponent,
        DayPlanAttendanceComponent,
        TPApprovalComponent,
        TpDeviationComponent,
        MgrTeamPerformanceComponent,
        DcrstatusComponent,
        ViewSampleIssueComponent,
        SampleDistributedIssuedBalanceDetailsComponent,
        TargetViewComponent,
        ExpenseReportComponent,
        FinalizeExpenseComponent,
        ExpenseModifyComponent,
        EmployeesAttendanceComponent,
        SampleGiftIssueComponent,
        ViewMefTrackerComponent,
        TargetVsAchievementStateAndDistrictWiseComponent,
        YealrSponserDoctorsRoiComponent,
        ConsolidatedTourProgramComponent,
        ViewProviderComponent,
        StationWiseWorkingComponent,
        ExpenseConsolidatedReportComponent,
        CallAverageAndWorkingDayComponent,
        BlockUserComponent,
        TestComponentComponent,
        ResignReportComponent,
        ResignedTPStatusComponent,
        ViewDCRResignedUserComponent,
        DCRViewReportDoctorWiseComponent,
        POBReportComponent,
        SpecialtyWiseDoctorCountComponent,
        ManagerAnalysisComponent,
        ConsolidatedExpenseComponent,
        HQWiseSSSReportComponent,
        MRQtyValuewiseSecondarySaleComponent,
        HolidayReportComponent,

        ManagerAnalysisComponent,
        ProductStatewiseSaleComponent,
        StockistWiseSaleReportComponent,
        SecondarySalesStatusComponent,
        ManagerwiseSSSReportComponent,
        CategorywiseSaleComponent,
        ProductStockistWiseQtyValueSaleComponent,
        DCRStatusReportComponent,
        TargetAndAchievementReportComponent,
        JointFieldWorkReportComponent,
        LeaveApprovalComponent,
        FieldForceStatusReportComponent,
        ERPStateWiseReportComponent,
        EditLeaveApprovalComponent,
        SponsorshipApprovalComponent,
        MEFReportComponent,
        NewReportsComponent,
        ExpenseUpdateComponent,
        MasterDataReportComponent,
        MgrInvestmentReportComponent,
        ProvideLocationOnOffSystemComponent,
        TpLockOpenReportComponent,
        StationdetailsComponent,
        ProviderUpdatedLocationComponent,
        EmployeedcrattendanceComponent,
        GiftIssueDetailReportGiftWiseComponent,
        GiftIssueReportDateWiseComponent,
        SampleIssueReportDateWiseComponent,
        GiftIssueDetailsYearlyComponent,
        DayStartDayEndReportComponent,
        PobProductWiseComponent,
        PrimarySaleStateHqMgrWiseComponent,
        EditAssignedProductGroupComponent,
        POBReportStockistReportComponent,
        SummaryPOBReportComponent,
        RCPAReportComponent,
        SampleReportProviderClickWiseComponent,
        SampleIssueDetailReportSampleWiseComponent,
        EmploeeWiseCallAverageComponent,
        EmployeeLoginDetailsComponent,
        AreavisitdetailsComponent,
        SampleDistributedIssuedBalanceDetailsComponent,
        RemindervisitdetailsComponent,
        TargetVsAchivementERPReportComponent,
        TaegetViewFinancialYearWiseComponent,
        EmployeeLoginDetailsComponent,
        SalesReportHqWiseComponent,
        SssPerformanceWiseComponent,
        SponsorshipViewComponent,
        GenerateExcelOnSelectionComponent,
        TargetVsAchievementHqWiseComponent,
        StockAndSalesDumpComponent,
        StokistVistReportComponent,
        MissedProviderCountsComponent,
        EmployeeLeaveDetailsComponent,
        SalesReportMultiMonthComponent,
        StpCreationComponent,
        ExpenseReportDumpComponent,
        CRMDoctorsYearlyInvestmentWithROIComponent,
        LeaveStatusComponent,
        DCRStatusReportsComponent,
        ProviderlocationviweandeditComponent,
        DcrCallDetailsComponent,
        UserVersionReportComponent,
        EmploeeWisedayCallAverageComponent,
        TargetvsachievementComponent,
        CampdayreportComponent,
        ChannelformdetailsComponent
        
    ],
    exports: [
        UserDCRDetailComponent,
        UserCompleteDCRInfoComponent,
        TpDeviationComponent
    ]
})
export class ReportsModule { }
