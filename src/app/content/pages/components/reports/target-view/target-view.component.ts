import { MonthComponent } from '../../filters/month/month.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { TargetService } from '../../../../../core/services/Target/target.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, NgModel } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef, Output, EventEmitter, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { YearComponent } from '../../filters/year/year.component';
import { MatSlideToggleChange } from '@angular/material';
import { URLService } from '../../../../../core/services/URL/url.service';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';

@Component({
  selector: 'm-target-view',
  templateUrl: './target-view.component.html',
  styleUrls: ['./target-view.component.scss']
})
export class TargetViewComponent implements OnInit {
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  exportAsConfig2: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element
  }

  dataSource = [];
  dataSourceAmtwise = [];
  showStateReportType = true;
  checkedVarEmp = false;
  checkedVarHq = false;
  checkedVarQuarterly = false;
  checkedVarMonthly = false;
  checkedVarYearly = false;
  checkedVarProduct = true;
  checkedVarAmount = false;
  showEmpReportType = true;
  showHqReportType = true;
  showYearFilter = false;
  stateFilterForStatewise = false;
  stateFilter = false;
  districtFilter = false;
  employeeFilter = false;
  yearlySubmitType = false;
  targetType = false;
  quarterly = false;
  showViewReport = false;
  showViewReportAmountwise = false;
  firstQuarter = false;
  secondQuarter = false;
  thirdQuarter = false;
  fourthQuarter = false;
  yearlyTarget = false;
  districtFilterForMulti = false;
  showStateName = false;
  showEmpName = false;
  showHQName = false;
  isToggled = true; 

  Quarters = [
    { value: '1', viewValue: 'Jan-Mar' },
    { value: '2', viewValue: 'Apr-June' },
    { value: '3', viewValue: 'July-Sep' },
    { value: '4', viewValue: 'Oct-Dec' }
  ];

  @ViewChild("callStateComponent") callStateComponent: DistrictComponent;
  @ViewChild("callStateComponent2") callStateComponent2: DistrictComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDistrictComponentAgainForMulti") callDistrictComponentAgainForMulti: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callYearComponent") callYearComponent: YearComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  valueChange: any;
  userIds: any;
  //@Output() valueChange = new EventEmitter();
  @Input() check: boolean = true;

  constructor(
    private fb: FormBuilder,
    private _toastr: ToastrService,
    private stockiestService: StockiestService,
    private _targetService: TargetService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _userDetailservice: UserDetailsService,
    private exportAsService: ExportAsService,
    private _urlService: URLService,
  ) { }

  ngOnInit() {

    if (this.currentUser.company.target.targetInfo == 'Employeewise' || this.currentUser.company.target.targetInfo == 'all') {
      this.createTargetFormEmployeewise(this.fb);
      this.checkedVarEmp = true;
      this.checkedVarHq = false;
      this.reportType('Employeewise');
    } else {
      this.createTargetFormHeadquarterwise(this.fb);
      this.checkedVarEmp = false;
      this.checkedVarHq = true;
      this.reportType(this.currentUser.company.target.targetInfo);
    }

    if (this.currentUser.company.target.targetDuration == 'Quarterly' || this.currentUser.company.target.targetDuration == 'all') {
      this.checkedVarQuarterly = true;
      this.checkedVarMonthly = false;
      this.checkedVarYearly = false;
      this.yearlyType('Quarterly');
    } else if (this.currentUser.company.target.targetDuration == 'Monthly') {
      this.checkedVarQuarterly = false;
      this.checkedVarMonthly = true;
      this.checkedVarYearly = false;
      this.yearlyType(this.currentUser.company.target.targetDuration);
    } else if (this.currentUser.company.target.targetDuration == 'Yearly') {
      this.checkedVarQuarterly = false;
      this.checkedVarMonthly = false;
      this.checkedVarYearly = true;
      this.yearlyType(this.currentUser.company.target.targetDuration);
    }

    if (this.currentUser.company.target.targetType == 'productwise' || this.currentUser.company.target.targetType == 'all') {
      this.checkedVarProduct = true;
      this.checkedVarAmount = false;
      this.targetSubmitType('productwise');
    } else {
      this.checkedVarProduct = false;
      this.checkedVarAmount = true;
      this.targetSubmitType(this.currentUser.company.target.targetType);
    }

  }

  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  currentUser = JSON.parse(sessionStorage.currentUser);
  public target: FormGroup;
  createTargetFormStatewise(fb: FormBuilder) {
    this.target = fb.group({
      reportType: new FormControl(null, Validators.required),
      yearlyType: new FormControl(null, Validators.required),
      targetSubmitType: new FormControl(null, Validators.required),
      stateInfo: new FormControl(null, Validators.required),
      districtInfo: new FormControl(null),
      userInfo: new FormControl(null),
      year: new FormControl(null, Validators.required),
      quarterType: new FormControl(null),
      groupName: new FormControl(null),
      selectedGroupName: new FormControl(null)
    })
  }
  createTargetFormEmployeewise(fb: FormBuilder) {
    this.target = fb.group({
      reportType: new FormControl(null, Validators.required),
      yearlyType: new FormControl(null, Validators.required),
      targetSubmitType: new FormControl(null, Validators.required),
      stateInfo: new FormControl(null, Validators.required),
      districtInfo: new FormControl(null, Validators.required),
      userInfo: new FormControl(null),
      year: new FormControl(null, Validators.required),
      quarterType: new FormControl(null),
      groupName: new FormControl(null),
      selectedGroupName: new FormControl(null)
    })
  }
  createTargetFormHeadquarterwise(fb: FormBuilder) {
    this.target = fb.group({
      reportType: new FormControl(null, Validators.required),
      yearlyType: new FormControl(null, Validators.required),
      targetSubmitType: new FormControl(null, Validators.required),
      stateInfo: new FormControl(null, Validators.required),
      districtInfo: new FormControl(null, Validators.required),
      userInfo: new FormControl(null),
      year: new FormControl(null, Validators.required),
      quarterType: new FormControl(null),
      groupName: new FormControl(null),
      selectedGroupName: new FormControl(null)
    })
  }
  reportType(val) {
    this.yearlyTarget = false;
    this.showStateName = false;
    this.showEmpName = false;
    this.showHQName = false;
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    this.target.patchValue({ reportType: val });
    this.target.patchValue({ stateInfo: null });
    this.target.patchValue({ districtInfo: null });
    this.target.patchValue({ userInfo: null });
    if (val === 'Employeewise') {
      this.target.get('districtInfo').setValidators([Validators.required]);
      this.target.get('userInfo').setValidators([Validators.required]);
      (this.callStateComponent)? this.callStateComponent.setBlank(): null;
      (this.callStateComponent2)? this.callStateComponent2.setBlank(): null;
      (this.callDistrictComponent)? this.callDistrictComponent.setBlank(): null;
      (this.callDistrictComponentAgainForMulti)? this.callDistrictComponentAgainForMulti.setBlank(): null;
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = true;
        this.employeeFilter = true;
        this.yearlySubmitType = true;
        this.showYearFilter = true;
        this.targetType = true;
        this.districtFilterForMulti = false;
        this.showStateName = false;
        this.showEmpName = true;
        this.showHQName = false;
        this.stateFilterForStatewise = false;
      } else if (this.currentUser.userInfo[0].rL > 1) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = true;
        this.yearlySubmitType = true;
        this.showYearFilter = true;
        this.targetType = true;
        this.districtFilterForMulti = false;
        this.showStateName = false;
        this.showEmpName = true;
        this.showHQName = false;
        this.stateFilterForStatewise = false;
      }
    } else if (val === 'Headquarterwise') {
      this.target.get('districtInfo').setValidators([Validators.required]);
      this.target.get('userInfo').clearValidators();
      (this.callStateComponent)? this.callStateComponent.setBlank(): null;
      (this.callStateComponent2)? this.callStateComponent2.setBlank(): null;
      (this.callDistrictComponent)? this.callDistrictComponent.setBlank(): null;
      (this.callDistrictComponentAgainForMulti)? this.callDistrictComponentAgainForMulti.setBlank(): null;
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.yearlySubmitType = true;
        this.showYearFilter = true;
        this.targetType = true;
        this.districtFilterForMulti = true;
        this.showStateName = false;
        this.showEmpName = false;
        this.showHQName = true;
        this.stateFilterForStatewise = false;
      } else if (this.currentUser.userInfo[0].rL > 1) {
        this.stateFilter = false;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.yearlySubmitType = true;
        this.showYearFilter = true;
        this.targetType = true;
        this.districtFilterForMulti = true;
        this.showStateName = false;
        this.showEmpName = false;
        this.showHQName = true;
        this.stateFilterForStatewise = false;
      }
    } else if (val === 'Statewise') {
      this.target.get('districtInfo').clearValidators();
      this.target.get('userInfo').clearValidators();
      (this.callStateComponent)? this.callStateComponent.setBlank(): null;
      (this.callStateComponent2)? this.callStateComponent2.setBlank(): null;
      (this.callDistrictComponent)? this.callDistrictComponent.setBlank(): null;
      (this.callDistrictComponentAgainForMulti)? this.callDistrictComponentAgainForMulti.setBlank(): null;
      // this.createTargetFormStatewise(this.fb);
      this.yearlyType(this.currentUser.company.target.targetDuration);
      this.targetSubmitType(this.currentUser.company.target.targetType);
      if (this.currentUser.userInfo[0].rL === 0) {
        this.stateFilterForStatewise = true;
        this.stateFilter = false;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.yearlySubmitType = true;
        this.showYearFilter = true;
        this.targetType = true;
        this.districtFilterForMulti = false;
        this.showStateName = true;
        this.showEmpName = false;
        this.showHQName = false;
      }
    }
    this.target.patchValue({ reportType: val })
  }

  setEmployeeDetail(val) {
    this.target.patchValue({ userInfo: val })
  }

  yearlyType(val) {
    this.showYearFilter = false;
    this.quarterly = false;
    this.firstQuarter = false;
    this.secondQuarter = false;
    this.thirdQuarter = false;
    this.fourthQuarter = false;
    this.yearlyTarget = false;
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    this.target.patchValue({ yearlyType: val });
    this.target.patchValue({ year: null });
    this.target.patchValue({ quarterType: null });
    this.callYearComponent.setBlank();
    if (val == 'Quarterly' || val == 'all') {
      this.showYearFilter = true;
      this.quarterly = true;
      this.yearlyTarget = false;
    } else if (val == 'Monthly') {
      this.showYearFilter = true;
      this.quarterly = false;
      this.firstQuarter = true;
      this.secondQuarter = true;
      this.thirdQuarter = true;
      this.fourthQuarter = true;
      this.yearlyTarget = false;
    } else if (val == 'Yearly') {
      this.showYearFilter = true;
      this.quarterly = false;
      this.firstQuarter = false;
      this.secondQuarter = false;
      this.thirdQuarter = false;
      this.fourthQuarter = false;
      this.yearlyTarget = true;
    }
  }

  targetSubmitType(val) {
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    this.showViewReportAmountwise = false;
    this.target.patchValue({ targetSubmitType: val })
  }

  getStateValue(val) {
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    let state = [];
    this.userData = [];
    this.target.patchValue({ userInfo: null });
    this.target.patchValue({ districtInfo: null });
    if (this.target.value.reportType == "Statewise") {
      this.target.patchValue({ stateInfo: val });
    } else {
      state.push(val);
      this.target.patchValue({ stateInfo: state });
    }
    if(this.callDistrictComponent){
      this.callDistrictComponent.setBlank();
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
    }
    if (this.target.value.reportType == 'Headquarterwise') {
      this.callDistrictComponentAgainForMulti.setBlank();
      this.callDistrictComponentAgainForMulti.getDistricts(this.currentUser.companyId, val, true);
    }
  }
  userData = [];
  getDistrictValue(val) {
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    let district;
    this.target.patchValue({ userInfo: null });
    if (this.target.value.reportType == 'Employeewise') {
      district=val;
      this.target.patchValue({ districtInfo: district });
    } else if (this.target.value.reportType == 'Headquarterwise') {
      for (let i = 0; i < val.length; i++) {
        district.push(val[i]);        
      }
      this.target.patchValue({ districtInfo: district });
    }
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, district);
    /* this._userDetailservice.getEmployeeLinkValue(this.currentUser.companyId, district).subscribe(res => {
      this.userData = res;
    }, err => {
      console.log(err)
    }); */
  }

  getMonth(val) {
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    this.target.patchValue({ month: val })
  }

  getYear(val) {
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    this.target.patchValue({ year: val })
  }

  getQuarter(val) {
    // console.log(val);
    this.firstQuarter = false;
    this.secondQuarter = false;
    this.thirdQuarter = false;
    this.fourthQuarter = false;
    this.showViewReport = false;
    this.showViewReportAmountwise = false;
    if (val == '1') {
      this.firstQuarter = true;
      this.secondQuarter = false;
      this.thirdQuarter = false;
      this.fourthQuarter = false;
    } else if (val == '2') {
      this.secondQuarter = true;
      this.firstQuarter = false;
      this.thirdQuarter = false;
      this.fourthQuarter = false;
    } else if (val == '3') {
      this.firstQuarter = false;
      this.secondQuarter = false;
      this.thirdQuarter = true;
      this.fourthQuarter = false;
    } else if (val == '4') {
      this.firstQuarter = false;
      this.secondQuarter = false;
      this.thirdQuarter = false;
      this.fourthQuarter = true;
    }
    this.target.patchValue({ quarterType: val })
  }

  viewTargetDetail() {
    if (this.target.value.targetSubmitType == 'productwise') {
      this._targetService.getTarget(this.currentUser.companyId, this.target.value)
      .subscribe(viewTarget => {
        if (viewTarget.status == 1) {
          this.dataSource = viewTarget;
          this.showViewReportAmountwise = false;
          this.showViewReport = true;
          this.isToggled = false;
          this._changeDetectorRef.detectChanges();
        } else {
          this.showViewReportAmountwise = false;
            this.showViewReport = false;
            this._changeDetectorRef.detectChanges();
            this._toastr.success("Please select another details", "No Target Details Found !!");
          }
        }, err => {
        })
    } else if (this.target.value.targetSubmitType == 'amountwise') {
      // console.log(this.target.value);
      this._targetService.getTarget(this.currentUser.companyId, this.target.value)
        .subscribe(viewTargetAmt => {
          if (viewTargetAmt.status == 1) {
            this.dataSourceAmtwise = viewTargetAmt;
            this.showViewReport = false;
            this.showViewReportAmountwise = true;
            this.isToggled = false;
            this._changeDetectorRef.detectChanges();
          } else {
            this.showViewReport = false;
            this.showViewReportAmountwise = false;
            this._changeDetectorRef.detectChanges();
            this._toastr.success("Please select another details", "No Target Details Found !!");
          }
        }, err => {
        })
    }
  }

  exporttoExcel() {
    this.exportAsService.save(this.exportAsConfig, 'Target report').subscribe(() => {


    });

  }


  exporttoAmountExcel() {
    this.exportAsService.save(this.exportAsConfig2, 'Target report').subscribe(() => {


    });

  }




  onPreviewAmountClick(){
   
    let printContents, popupWin;
  
    const department = this.currentUser.section;
    const designation = this.currentUser.designation;
    const companyName = this.currentUser.companyName;
    printContents = document.getElementById('tableId2').innerHTML;
    const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
  <head>
  <style>
  img {
  float: left;
  }

  .flex-container {
  display: flex;
  justify-content: space-between;
  }

  .tg {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tg td {
  font-family: Arial, sans-serif;
  font-size: 14px;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  table {
    border-collapse: collapse;
}

  tg.th {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg .tg-lboi {
  border-color: inherit;
  text-align: left;
  vertical-align: middle;

  }

  .tg .tg-u6fn {
  font-weight: bold;
  font-size: 18px;
  background-color: #efefef;
  color:black;
  border-color: inherit;
  text-align: left;
  vertical-align: top;
  padding-top: 0px;
  padding-bottom: 0px;
  }

  .tg .tg-yz93 {
  border-color: inherit;
  text-align: right;
  vertical-align: middle
  }

  .tb2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

   td {
  font-family: Arial, sans-serif;
  font-size: 12px;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  th {
    background-color:#AAB7B8;
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-weight: normal;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  
  }

  .tb2 .tg-oiyu {
  font-size: 10px;
  text-align: left;
  vertical-align: middle
  }
  .tg-bckGnd {
  font-weight: bold;
  background-color: #efefef;
  }


  .tb2 .tg-3ppa {
  font-size: 10px;
  background-color: #efefef;
  color:black;
  text-align: left;
  vertical-align: middle;
  padding-top: 2px;
  padding-bottom: 2px;
  }

  .footer {
  position: sticky;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #efefef;
  color: rgb(2, 2, 2);
  text-align: right
  }



  button {
  background-color: #008CBA;
  border: none;
  color: white;
  margin: 5px 0px;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  }

  @media print {
  @page { size: A4; margin: 5mm; }

  .footer {
  position: relative;
  }
  tr {
  page-break-inside: avoid;
  }

  button{
  display : none;
  }
  }
  </style>
  </head>
  <body onload="window.print();window.close()" style="padding-bottom:30px">
  <div class="flex-container">
  <div style="width: 40%; ">
  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

  </div>

  <div style="text-align: right;">
  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Basic Info </h2>
  <div>
  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
  </div>
  </div>
  </div>
  ${printContents}</body>
  </html>
  `
    );
   // popupWin.document.close();
 }



  onPreviewClick(){
   
    let printContents, popupWin;
  
    const department = this.currentUser.section;
    const designation = this.currentUser.designation;
    const companyName = this.currentUser.companyName;
    printContents = document.getElementById('tableid1').innerHTML;
    const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
  <head>
  <style>
  img {
  float: left;
  }

  .flex-container {
  display: flex;
  justify-content: space-between;
  }

  .tg {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tg td {
  font-family: Arial, sans-serif;
  font-size: 14px;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  table {
    border-collapse: collapse;
}

  tg.th {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg .tg-lboi {
  border-color: inherit;
  text-align: left;
  vertical-align: middle;

  }

  .tg .tg-u6fn {
  font-weight: bold;
  font-size: 18px;
  background-color: #efefef;
  color:black;
  border-color: inherit;
  text-align: left;
  vertical-align: top;
  padding-top: 0px;
  padding-bottom: 0px;
  }

  .tg .tg-yz93 {
  border-color: inherit;
  text-align: right;
  vertical-align: middle
  }

  .tb2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

   td {
  font-family: Arial, sans-serif;
  font-size: 12px;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  th {
    background-color:#AAB7B8;
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-weight: normal;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  
  }

  .tb2 .tg-oiyu {
  font-size: 10px;
  text-align: left;
  vertical-align: middle
  }
  .tg-bckGnd {
  font-weight: bold;
  background-color: #efefef;
  }


  .tb2 .tg-3ppa {
  font-size: 10px;
  background-color: #efefef;
  color:black;
  text-align: left;
  vertical-align: middle;
  padding-top: 2px;
  padding-bottom: 2px;
  }

  .footer {
  position: sticky;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #efefef;
  color: rgb(2, 2, 2);
  text-align: right
  }



  button {
  background-color: #008CBA;
  border: none;
  color: white;
  margin: 5px 0px;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  }

  @media print {
  @page { size: A4; margin: 5mm; }

  .footer {
  position: relative;
  }
  tr {
  page-break-inside: avoid;
  }

  button{
  display : none;
  }
  }
  </style>
  </head>
  <body onload="window.print();window.close()" style="padding-bottom:30px">
  <div class="flex-container">
  <div style="width: 40%; ">
  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

  </div>

  <div style="text-align: right;">
  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Basic Info </h2>
  <div>
  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
  </div>
  </div>
  </div>
  ${printContents}</body>
  </html>
  `
    );
   // popupWin.document.close();
 }


}
