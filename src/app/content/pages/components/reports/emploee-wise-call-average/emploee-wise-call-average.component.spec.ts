import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploeeWiseCallAverageComponent } from './emploee-wise-call-average.component';

describe('EmploeeWiseCallAverageComponent', () => {
  let component: EmploeeWiseCallAverageComponent;
  let fixture: ComponentFixture<EmploeeWiseCallAverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmploeeWiseCallAverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploeeWiseCallAverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
