import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { StateComponent } from '../../filters/state/state.component';
import { NativeDateAdapter, DateAdapter, MatDateFormats, MAT_DATE_FORMATS, MatStepper, MatSlideToggleChange, MatPaginator, MatTableDataSource, MatSort } from "@angular/material";
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';

import { URLService } from '../../../../../core/services/URL/url.service';
import * as _moment from 'moment';
import { UtilsService, isString } from '../../../../../core/services/utils.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import * as XLSX from 'xlsx';
import { ToastrService } from 'ngx-toastr';

import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
@Component({
  selector: 'm-emploee-wise-call-average',
  templateUrl: './emploee-wise-call-average.component.html',
  styleUrls: ['./emploee-wise-call-average.component.scss']
})
export class EmploeeWiseCallAverageComponent implements OnInit {
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('tableData') tableRef: ElementRef;
  @ViewChild('sort') sort: MatSort;

  currentMonth = new Date().getMonth();
  currentYear = new Date().getFullYear();
  lastDay = new Date(this.currentYear, this.currentMonth, 0).getDate();
  @ViewChild('paginator') paginator: MatPaginator;


  actualMonth = this.currentMonth + 1;

  currentMonthStartDate = this.currentYear + "-" + this.actualMonth + "-01";
  currentMonthLastDate =
    this.currentYear + "-" + this.actualMonth + "-" + this.lastDay;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  showProcessing: boolean = false;
  showReportType: boolean = true;
  showType: boolean = true;
  showState: boolean = false;
  showDistrict: boolean = false;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  };
  showReport: boolean = false;
  showEmployee: boolean = false;
  showDesignation: boolean = false;
  showDivisionFilter: boolean = false;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any;

  currentMonthStartDatefilter: any;
  currentMonthLastDatefilter: any;

  isShowTeamInfo: boolean = false;
  dynamicTableHeader: any = [];
  currentDate = new Date();
  public showDistrictWise: boolean = false;
  public showUserWise: boolean = false;
  expenseUpdateForm: FormGroup;
  constructor(private utilService: UtilsService,
    public hierarchyService: HierarchyService,
    private toastr: ToastrService,
    private _urlService: URLService,
    private exportAsService: ExportAsService,
    private _DCRReportService: DcrReportsService,
    private _fb: FormBuilder,
    private _ChangeDetectorRef: ChangeDetectorRef) {
    this.createForm(this._fb);
  }
  async ngOnInit() {


    let resultArr = [];

    let objForEmployeeAvg = {};
    if (this.currentUser.userInfo[0].rL === 1) {
      objForEmployeeAvg = {
        companyId: this.currentUser.companyId,
        fromDate: this.currentMonthStartDate,
        toDate: this.currentMonthLastDate,
        designation: this.currentUser.userInfo[0].designationLevel,
        userId: [this.currentUser.userInfo[0].userId],
        rL: this.currentUser.userInfo[0].rL

      }
    }

    else if (this.currentUser.userInfo[0].rL === 2) {

      let object = {
        companyId: this.currentUser.companyId,
        supervisorId: this.currentUser.userInfo[0].userId,
        type: 'lower'
      }

      const resArr = await this.getHierarchy(object)

      resArr.forEach(element => {
        resultArr.push(element.userId)
      });

      objForEmployeeAvg = {
        companyId: this.currentUser.companyId,
        fromDate: this.currentMonthStartDate,
        toDate: this.currentMonthLastDate,
        designation: this.currentUser.userInfo[0].designationLevel,
        rL: this.currentUser.userInfo[0].rL,
        userId: resultArr
      }


    } else {
      objForEmployeeAvg = {
        companyId: this.currentUser.companyId,
        fromDate: this.currentMonthStartDate,
        toDate: this.currentMonthLastDate,
        designation: this.currentUser.userInfo[0].designationLevel
      };



    }


    this._DCRReportService.getAllEmployeeCallAverage(objForEmployeeAvg).subscribe(res => {
      if (res.length > 0) {
        this.showUserWise = true;
        this.displayedColumns = ['sn', 'username', 'districtName', 'totalWorkingDay', 'drCall', 'drAvg', 'chemCalls', 'chemAvg', 'stkCalls', 'stkAvg', 'doctorPOB', 'vendorPOB', 'stockistPOB'];
        this._ChangeDetectorRef.detectChanges();
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 200);
        this._ChangeDetectorRef.detectChanges();
      } else {
        this.toastr.info("No Record Found.");
        this.showProcessing = false;
        this.showUserWise = false;

      }

    })





  }

  createForm(_fb: FormBuilder) {

    this.expenseUpdateForm = _fb.group({

      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })

  }



  getFromDate(month: object) {
    this.expenseUpdateForm.patchValue({ month: month })
  }

  getToDate(year: number) {
    this.expenseUpdateForm.patchValue({ year: year })
  }

  getHierarchy(obj): Promise<any> {
    let resultArr = []
    return new Promise((resolve, reject) => {
      this.hierarchyService.getManagerHierarchy(obj).subscribe(res => {
        if (!Boolean(res)) reject();
        else resolve(res);
      })
    })
  }

  async generateReport() {


    let resultArr = [];

    this.currentMonthStartDatefilter = this.expenseUpdateForm.value.year + "-" + this.expenseUpdateForm.value.month + "-01";
    this.currentMonthLastDatefilter =  this.expenseUpdateForm.value.year + "-" + this.expenseUpdateForm.value.month + "-" + "31";

console.log("currentMonthStartDatefilter", this.currentMonthStartDatefilter)
console.log("currentMonthLastDatefilter", this.currentMonthLastDatefilter)

    var obj11 = {};
    if (this.currentUser.userInfo[0].rL === 1){
      console.log("this.currentUser.userInfo[0].rL ", this.currentUser.userInfo[0].rL);
      obj11 = {
        companyId: this.currentUser.companyId,
        fromDate: this.currentMonthStartDate,
        toDate: this.currentMonthLastDate,
        designation: this.currentUser.userInfo[0].designationLevel,
        userId: this.currentUser.userInfo[0].userId,
        rL: this.currentUser.userInfo[0].rL
      }
    }

    else if (this.currentUser.userInfo[0].rL === 2) {
      console.log("this.currentUser.userInfo[0].rL ", this.currentUser.userInfo[0].rL);
      let object = {
        companyId: this.currentUser.companyId,
        supervisorId: this.currentUser.userInfo[0].userId,
        type: 'lower'
      }

      const resArr = await this.getHierarchy(object)

      resArr.forEach(element => {
        resultArr.push(element.userId)
      });

      obj11 = {
        companyId: this.currentUser.companyId,
        fromDate: this.currentMonthStartDate,
        toDate: this.currentMonthLastDate,
        designation: this.currentUser.userInfo[0].designationLevel,
        userId: resultArr

      }

    } else {
      obj11 = {
        companyId: this.currentUser.companyId,
        fromDate: this.currentMonthStartDate,
        toDate: this.currentMonthLastDate,
        designation: this.currentUser.userInfo[0].designationLevel,

      };

    }

    let obj = {
      companyId: this.currentUser.companyId,
      fromDate: this.currentMonthStartDatefilter,
      toDate: this.currentMonthLastDatefilter,
      designation: this.currentUser.userInfo[0].designationLevel,
      // userId: this.currentUser.userInfo[0].userId,
      // rL: this.currentUser.userInfo[0].rL

    }

console.log("mahender emp",obj)


    this._DCRReportService.getAllEmployeeCallAverage(obj).subscribe(res => {
      console.log("mahender res",res)

      if (res.length > 0) {
        this.showUserWise = true;
        this.displayedColumns = ['sn', 'username', 'districtName', 'totalWorkingDay', 'drCall', 'drAvg', 'chemCalls', 'chemAvg', 'stkCalls', 'stkAvg', 'doctorPOB', 'vendorPOB', 'stockistPOB'];
        this._ChangeDetectorRef.detectChanges();
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 200);
        this._ChangeDetectorRef.detectChanges();
      } else {
        this.toastr.info("No Record Found.");
        this.showProcessing = false;
        this.showUserWise = false;

      }

    })


  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue
  }


  exportToExcel() {

    // download the file using old school javascript method
    let dataToExport = [];
    dataToExport = this.dataSource.data.map(x => ({
      "Employee Name": x.emploeeName.toUpperCase(),
      "District Name": x.districtName,
      "Total Working Days": x.TotalWorkingDays,
      "Total Doctor Call": x.RMPCalls,
      "Doctor Call Avg": x.drCallAvg != null ? (x.drCallAvg).toFixed(2) : '---',
      "Chemist Call Avg" : x.venCallAvg != null?(x.venCallAvg).toFixed(2):'---',
      "Stokist Call Avg" : x.StockistCalls != null?(x.StockistCalls).toFixed(2):'---',
      "Doctor POB" : x.DoctorPOB,
      "Vendor POB" : x.VendorPOB,
      "Stokist POB" : x.stockistPOB
    }))
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Employee Call Average.xlsx");
  }



  onPreviewClick() {



    console.log("this.dataSource", this.dataSource);

    let printContents, popupWin;

    const department = this.currentUser.section;
    const designation = this.currentUser.designation;
    const companyName = this.currentUser.companyName;

    let tablerowdivisionname: any = [];


    for (let i = 0; i < this.dataSource.filteredData.length; i++) {

      tablerowdivisionname.push(`
  <tr>
  <td class="tg-oiyu" style="font-weight:600">${i + 1}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].emploeeName}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].districtName}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].TotalWorkingDays}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].RMPCalls}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].drCallAvg}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].chemistCalls}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].venCallAvg}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].StockistCalls}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].stkCallAvg}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].DoctorPOB}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].VendorPOB}</td>
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].stockistPOB}</td>
  </tr>
  `)

    }


    let monthname;


    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;
	}
	
	
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	 
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4 landscape;
		max-height:100%;
		max-width:100%}
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
   
    </div>
    
   
    
	  
	
	  <div style="text-align: center;">
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  
    
    <table class="tb2"> 
    <tr>
    <th class="tg-3ppa"><span style="font-weight:600">SN.</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Employee Name</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">District Name</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Total working Days</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Total Doctor Calls</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Doctor Call Avg.</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Chemist Call </span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Chemist Call Avg. </span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Stockist Call </span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Stckist Call Avg. </span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Doctor POB  </span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Vendor POB  </span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Stockist POB  </span></th>
    
     
</tr>
<td class="tg-oiyu">  ${tablerowdivisionname.join("")}</td>




    </table>
    </body>
	  </html>
	  `
    );
    // popupWin.document.close();
  }


}
