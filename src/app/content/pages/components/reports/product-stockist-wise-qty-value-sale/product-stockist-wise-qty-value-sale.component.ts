import { ToastrService, ToastrComponentlessModule } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTable, MatSlideToggleChange } from '@angular/material';
import { Designation } from '../../../_core/models/designation.model';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { UtilsService } from '../../../../../core/services/utils.service';
import { PrimaryAndSaleReturnService } from '../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service';
@Component({
  selector: 'm-product-stockist-wise-qty-value-sale',
  templateUrl: './product-stockist-wise-qty-value-sale.component.html',
  styleUrls: ['./product-stockist-wise-qty-value-sale.component.scss']
})
export class ProductStockistWiseQtyValueSaleComponent implements OnInit {
  public showReport: boolean = false;
  public showProcessing: boolean = false;
  public showProductFilter: boolean = false;
  public showReportProductWise:boolean=false;
  isStatus: boolean = true;

  count = 0;
  data;
  dataProductWise;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  stockistWiseSaleForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(private _toastrService: ToastrService,
    private _fb: FormBuilder,
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef, private utilService: UtilsService,
    private toastr: ToastrService, private exportAsService: ExportAsService,
    private _primarySaleReturn:PrimaryAndSaleReturnService) {
    this.createForm(this._fb);

  }
  public dynamicTableHeader: any = [];
  public dynamicTableStockist: any=[];

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("employeeComponent") employeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild('dataTable') table1;
  dataTable: any;
  dtOptions: any;
  dtTrigger: any;
  showDivisionFilter: boolean = false;
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {

      this.stockistWiseSaleForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  createForm(_fb: FormBuilder) {

    this.stockistWiseSaleForm = _fb.group({
      designation: new FormControl('', Validators.required),
      status: new FormControl(true),
      userId: new FormControl('', Validators.required),
      toDate: new FormControl('', Validators.required),
      fromDate: new FormControl('', Validators.required),
    })

  }
  getDivisionValue(val) {
    // this.showProcessing = false;
    this.stockistWiseSaleForm.patchValue({ division: val });
    //=======================Clearing Filter===================
    this.designationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.employeeComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.designationComponent.getDesignationBasedOnDivision(passingObj);
  }
  getDesignationList() {
    //this.designationComponent.getManagersDesignation();
  }

//   getDesignationLevel(designation: Designation) {
//         //=======================Clearing Filter===================
//         this.callStatusComponent.setBlank();
//         this.employeeComponent.setBlank();
//         //===============================END================================
//     this.stockistWiseSaleForm.patchValue({ designation: designation.designationLevel })
//   }


//********************************************* Umesh Kumar 15-05-2020 ************************* */
getDesignationLevel(designation: Designation) {
	//=======================Clearing Filter===================
	this.callStatusComponent.setBlank();
	this.employeeComponent.setBlank();
	//===============================END================================
this.stockistWiseSaleForm.patchValue({ designation: designation.designationLevel })

if (this.currentUser.userInfo[0].rL === 0) {
	if (this.currentUser.company.isDivisionExist == true) {
		let passingObj = {
			companyId: this.currentUser.companyId,
			designationObject: [designation],
			status: [true],
			division: this.stockistWiseSaleForm.value.division,
		};

		this.employeeComponent.getEmployeeListBasedOnDivision(
			passingObj
		);
	} else if (this.currentUser.company.isDivisionExist == false) {
		this.employeeComponent.getEmployeeList(
			this.currentUser.companyId,
			[designation],
			[true]
		);
	}
} else {
	let dynamicObj = {};
	if (this.currentUser.company.isDivisionExist == false) {
		dynamicObj = {
			supervisorId: this.currentUser.id,
			companyId: this.currentUser.companyId,
			status: true,
			type: "lower",
			designation: [designation.designationLevel], //desig
		};
		this.employeeComponent.getManagerHierarchy(dynamicObj);
	} else if (this.currentUser.company.isDivisionExist == true) {
		dynamicObj = {
			supervisorId: this.currentUser.id,
			companyId: this.currentUser.companyId,
			status: true,
			type: "lower",
			designation: [designation.designationLevel], //desig,
			isDivisionExist: true,
			division: this.stockistWiseSaleForm.value.division,
		};
		this.employeeComponent.getManagerHierarchy(dynamicObj);
	}
}
}

  getStatus(status: boolean) {
    this.showReport = false;
    this.showReportProductWise = false;
    this.stockistWiseSaleForm.patchValue({ status: status });
    //=======================Clearing Filter===================
    this.employeeComponent.setBlank();
    //===============================END================================
    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        designationObject: this.stockistWiseSaleForm.value.designation,
        status: status,
        division: this.stockistWiseSaleForm.value.division
      }
      this.employeeComponent.getEmployeeListBasedOnDivision(passingObj);
    } else {
      this.employeeComponent.getEmployeeList(this.currentUser.companyId, this.stockistWiseSaleForm.value.designation, status);
    }
  }

  getEmployee(emp: string) {
    this.stockistWiseSaleForm.patchValue({ userId: emp })
    this.showReport = false;
    this.showReportProductWise = false;
  }

  getFromDate(fromDate: object) {
    this.stockistWiseSaleForm.patchValue({ fromDate: fromDate })
  }

  getToDate(toDate: number) {
    this.stockistWiseSaleForm.patchValue({ toDate: toDate })
  }


  intializeForm() {
    this.stockistWiseSaleForm.setValue({
      designation: '',
      status: '',
      userId: '',
      fromDate: '',
      toDate: '',
    })
  }
  exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Product Stockist Wise Qty Value Report').subscribe(() => {
      // save started
    });
  }
  monthsName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  getCelData(date, stockist, data,key){

    const _date = date.split(' ')
    const month = this.monthsName.findIndex(i=>i.toLowerCase() === _date[0].toLowerCase()) + 1
    const year = _date[1]
    const _cellData = data.find(i=>i.month == month && i.year == year && i.partyName == stockist)

    // console.log("data = ",_cellData);

    return _cellData ? _cellData[key] : 0

  }


  generateReport() {
    this.isToggled = false;
    this.showProcessing = true;
    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.stockistWiseSaleForm.value.fromDate, this.stockistWiseSaleForm.value.toDate);
    let where={
      "where":{
        companyId: this.currentUser.companyId,
        status: true,
        userId: this.stockistWiseSaleForm.value.userId,
        providerType:"Stockist"
      }
    }
    this._primarySaleReturn.getStockists(where).subscribe(res => {
      
      this.dynamicTableStockist = []
      for(var i=0;i<res.length;i++){
        this.dynamicTableStockist.push(res[i].providerName)
      }
      this._changeDetectorRef.detectChanges();
      
    })
    if (!this.stockistWiseSaleForm.valid) {
      this._toastrService.error("Select the required field to generate the report", "All Field required");
      return;
    }
    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.stockistWiseSaleForm.value.division;
    }
    let params = {
      companyId: this.currentUser.companyId,
      status: this.stockistWiseSaleForm.value.status,
      userId: this.stockistWiseSaleForm.value.userId,
      fromDate: this.stockistWiseSaleForm.value.fromDate,
      toDate: this.stockistWiseSaleForm.value.toDate,
      isDivisionExist: this.currentUser.company.isDivisionExist,
      division: divisionIds,
      designation: this.stockistWiseSaleForm.value.designation,
      type: "lowerWithMgr"
      
    }
    this._primarySaleReturn.stockistwiseSales(params).subscribe(res => {
      this.data=[];
      if(res.length>0){
        this.data = res;
        this.showReport = true;
        this.showProcessing = false;
        this._changeDetectorRef.detectChanges();
      } else{
        this.showReport = false;
        this.showProcessing = false;
        this.toastr.info("No Record Found.")
        this._changeDetectorRef.detectChanges();    
      }
      this._changeDetectorRef.detectChanges();
      
    }, err => {
      this.showReport = false;
      this.showProcessing = false;
      this.toastr.info("No Record Found.")
    })
    this._changeDetectorRef.detectChanges();
  }
  
}
