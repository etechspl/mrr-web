import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductStockistWiseQtyValueSaleComponent } from './product-stockist-wise-qty-value-sale.component';

describe('ProductStockistWiseQtyValueSaleComponent', () => {
  let component: ProductStockistWiseQtyValueSaleComponent;
  let fixture: ComponentFixture<ProductStockistWiseQtyValueSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductStockistWiseQtyValueSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductStockistWiseQtyValueSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
