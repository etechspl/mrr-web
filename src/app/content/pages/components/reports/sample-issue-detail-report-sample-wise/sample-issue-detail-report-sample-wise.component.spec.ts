import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleIssueDetailReportSampleWiseComponent } from './sample-issue-detail-report-sample-wise.component';

describe('SampleIssueDetailReportSampleWiseComponent', () => {
  let component: SampleIssueDetailReportSampleWiseComponent;
  let fixture: ComponentFixture<SampleIssueDetailReportSampleWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleIssueDetailReportSampleWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleIssueDetailReportSampleWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
