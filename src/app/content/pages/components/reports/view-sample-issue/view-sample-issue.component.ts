import { URLService } from './../../../../../core/services/URL/url.service';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { Designation } from '../../../_core/models/designation.model';
import { DesignationComponent } from '../../master/designation/designation.component';
import { SampleService } from '../../../../../core/services/sample/sample.service';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'm-view-sample-issue',
  templateUrl: './view-sample-issue.component.html',
  styleUrls: ['./view-sample-issue.component.scss']
})
export class ViewSampleIssueComponent implements OnInit {
  public showReport = false;
  showProductsStateWise = false;
  designationData: any;
  displayedColumns = ['state', 'headquarter', 'employeeName', 'productName', 'productCode', 'issuedDate', 'openingStock', 'receipt', 'totalStock'];
  dataSource;



  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  currentUser = JSON.parse(sessionStorage.currentUser);
  viewsampleIssue : FormGroup;
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private userDetailsService: UserDetailsService,
    private _samplesService:SampleService,
		private _urlService: URLService,    
    private fb: FormBuilder,
  )
  
  {
    this.viewsampleIssue = this.fb.group({

      stateInfo: new FormControl(null, Validators.required),
      designation: new FormControl(null, Validators.required),
      employeeId: new FormControl(null, Validators.required),
      month: new FormControl(null, Validators.required),
      year: new FormControl(null, Validators.required),

    })
  }



  ngOnInit() {
  }

  getStateValue(val) {
    this.viewsampleIssue.patchValue({ stateInfo: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);

  }
  getMonth(val) {

    this.viewsampleIssue.patchValue({ month: val });
  }
  getYear(val) {
    this.viewsampleIssue.patchValue({ year: val });
  }

  getDesignation(val) {
    this.viewsampleIssue.patchValue({ designation: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
  }
  getEmployeeValue(val) {
    this.viewsampleIssue.patchValue({ employeeId: val });
  }


  get() {
	  this.showProductsStateWise = true;
	  this._samplesService.getSampleGiftIssueDetail(this.currentUser.companyId,this.viewsampleIssue.value,1).subscribe(res => {
		  console.log('dataSource',res);
		  this.dataSource=res;
		}, err => {
			this.showReport = false;
			//this.showProcessing = false;
			// this._toastr.info('For Selected Filter data not found', 'Data Not Found');
			
		})
		this.changeDetectorRef.detectChanges();
	}


  onPreviewClick(){
   
		let printContents, popupWin;
		const companyName = this.currentUser.companyName;
		printContents = document.getElementById('tableId1').innerHTML;
		const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
	
		popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		popupWin.document.open();
		popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;
	}
	
	  tg.th {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  font-weight: normal;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4; margin: 5mm; }
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
	  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
	
	  </div>
	
	  <div style="text-align: right;">
	  <h2 style="margin-top: 0px; margin-bottom: 5px">Sample Or Gift Issue Details </h2>
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  ${printContents}</body>
	  </html>
	  `
		);
	   // popupWin.document.close();
	 }

}
