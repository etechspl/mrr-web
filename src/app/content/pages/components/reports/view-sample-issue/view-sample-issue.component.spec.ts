import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSampleIssueComponent } from './view-sample-issue.component';

describe('ViewSampleIssueComponent', () => {
  let component: ViewSampleIssueComponent;
  let fixture: ComponentFixture<ViewSampleIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSampleIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSampleIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
