import { StockiestService } from './../../../../../core/services/Stockiest/stockiest.service';
import { MonthComponent } from '../../filters/month/month.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { MappedStockistComponent } from '../../filters/mapped-stockist/mapped-stockist.component';
import { TypeComponent } from '../../filters/type/type.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatSlideToggleChange } from '@angular/material';
import { StateComponent } from '../../filters/state/state.component';
import { DivisionComponent } from '../../filters/division/division.component';

declare var $;

@Component({
  selector: 'm-consolidate-sss',
  templateUrl: './consolidate-sss.component.html',
  styleUrls: ['./consolidate-sss.component.scss']
})
export class ConsolidateSssComponent implements OnInit {
  showGeoReportType = false;
  showEmpReportType = false;
  showStokistReportType = false;
  typeFilter = false;
  stateFilter = false;
  districtFilter = false;
  employeeFilter = false;
  stokistFilter = false;
  public showReport = false;
  public showState: boolean = true;
  public showDistrict: boolean = false;
  showProcessing: boolean = false;
  showStockistDoctorInTable = false;

  displayedColumns = ['productName', 'productRate', 'opening', 'totalPrimaryQty', 'totalPrimaryAmt', 'totalSaleReturnQty', 'totalSaleReturnAmt', 'secondarySaleQty', 'secondarySaleValue', 'totalStock', 'totalStockValue', 'closing', 'closingvalue'];
  dataSource;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callMappedStockistComponent") callMappedStockistComponent: MappedStockistComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;

  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  stockistData: any;

  constructor(private stockiestService: StockiestService, private _changeDetectorRef: ChangeDetectorRef, private _ToastrService: ToastrService) { }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
    this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.currentUser.userInfo[0].rL === 1) {
      this.showGeoReportType = false;
      this.showEmpReportType = false;
      this.showStokistReportType = true;
      this.getStockists([this.currentUser.id]);
      this.getStateValue([this.currentUser.userInfo[0].stateId]);
      this.getEmployeeValue([this.currentUser.userInfo[0].districtId]);
    } else if (this.currentUser.userInfo[0].rL === 0 || this.currentUser.userInfo[0].rL === 2) {
      this.showGeoReportType = true;
      this.showEmpReportType = true;
      this.showStokistReportType = true;
    }
    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
    }
  }

  currentUser = JSON.parse(sessionStorage.currentUser);
  sssConsolidate = new FormGroup({
    reportType: new FormControl(null),
    type: new FormControl(null),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    userInfo: new FormControl(null),
    stokistInfo: new FormControl(null),
    month: new FormControl(null, Validators.required),
    year: new FormControl(null, Validators.required),
    division: new FormControl(null)
  })


  isDivision: boolean = false


  reportType(val) {
    this.showProcessing = false;
    this.showReport = false;



    // this.sssConsolidate.patchValue({ reportType: val });
    if (val === 'Geographical') {
      this.typeFilter = true;
      this.stateFilter = false;
      this.districtFilter = false;
      this.employeeFilter = false;
      this.stokistFilter = false;
      if (this.currentUser.company.isDivisionExist === true) {
        this.isDivision = true;
      } else {
        this.isDivision = false;
      }
      this.callStateComponent.setBlank();
      this.callTypeComponent.setBlank();
      this.sssConsolidate.setControl('type', new FormControl('', Validators.required))
    } else if (val === 'Employeewise') {
      if (this.currentUser.company.isDivisionExist === true) {
        this.isDivision = true;
      } else {
        this.isDivision = false;
      }
      this.callStateComponent.setBlank();
      this.sssConsolidate.removeControl('stokistInfo');
      this.sssConsolidate.removeControl('type');
      this.sssConsolidate.setControl('districtInfo', new FormControl('', Validators.required))
      this.sssConsolidate.setControl('userInfo', new FormControl('', Validators.required))
      if (this.currentUser.userInfo[0].rL === 0 || this.currentUser.userInfo[0].rL === 2) {
        this.typeFilter = false;
        this.stateFilter = true;
        this.districtFilter = true;
        this.employeeFilter = true;
        this.stokistFilter = false;


      } else if (this.currentUser.userInfo[0].rL === 1) {
        if (this.currentUser.company.isDivision === true) {
          this.isDivision = true;
        } else {
          this.isDivision = false;
        }
        this.typeFilter = false;
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = true;
        this.stokistFilter = false;
      }

    } else if (val === 'Stokistwise') {

      if (this.currentUser.company.isDivisionExist === true) {
        this.isDivision = true;
      } else {
        this.isDivision = false;
      }
      this.callStateComponent.setBlank();

      this.sssConsolidate.removeControl('type');
      // this.sssConsolidate.setControl('districtInfo', new FormControl('', Validators.required))
      // this.sssConsolidate.setControl('userInfo', new FormControl('', Validators.required))
      this.sssConsolidate.setControl('stokistInfo', new FormControl('', Validators.required))
      if (this.currentUser.userInfo[0].rL === 0 || this.currentUser.userInfo[0].rL === 2) {
        this.typeFilter = false;
        this.stateFilter = true;
        this.districtFilter = true;
        this.employeeFilter = true;
        this.stokistFilter = true;

      } else if (this.currentUser.userInfo[0].rL === 1) {
        this.typeFilter = false;
        this.stateFilter = false;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.stokistFilter = true;
      }

    }
    this.sssConsolidate.patchValue({ reportType: val })
  }

  getTypeValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    if (val == "Headquarter") {
      this.stateFilter = true;
      this.districtFilter = true;
      this.callStateComponent.setBlank();
      this.sssConsolidate.removeControl('userInfo');
      this.sssConsolidate.removeControl('stokistInfo');
      this.sssConsolidate.setControl('districtInfo', new FormControl('', Validators.required))
    } else {
      this.callStateComponent.setBlank();
      this.sssConsolidate.removeControl('districtInfo');
      this.sssConsolidate.removeControl('userInfo');
      this.sssConsolidate.removeControl('stokistInfo');
      this.sssConsolidate.setControl('stateInfo', new FormControl('', Validators.required))
      this.stateFilter = true;
      this.districtFilter = false;
    }
    this.sssConsolidate.patchValue({ type: val });
  }

  getDivisionValue(val) {
    let obj = {
      companyId: this.currentUser.companyId,
      isDivisionExist: true,
      division: val
    }
    this.callStateComponent.getStateBasedOnDivision(obj);


    this.sssConsolidate.patchValue({ division: val })
  }

  getStateValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.sssConsolidate.patchValue({ stateInfo: val });

    this.callDistrictComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    if (this.isDivision == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        stateId: val,
        isDivisionExist: true,
        division: this.sssConsolidate.value.division
      }
      this.callDistrictComponent.getDistrictsBasedOnDivision(obj);
    } else
      if (this.sssConsolidate.value.type != "State") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
  }
  getEmployeeValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.sssConsolidate.patchValue({ districtInfo: val });
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, val);
  }

  getStockists(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.sssConsolidate.patchValue({ userInfo: val });
    this.stockiestService.getMappedStockists(this.currentUser.companyId, val).subscribe(res => {
      this.stockistData = res;
    })
  }

  setStockistValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    let obj = {
      stkId: val.id,
      stkCode: val.providerCode,
      stkName: val.providerName
    }
    this.sssConsolidate.patchValue({ stokistInfo: val });
  }

  getMonth(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.sssConsolidate.patchValue({ month: val })
  }

  getYear(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.sssConsolidate.patchValue({ year: val })
  }

  // viewRecords() {
  //   this.showProcessing = true;
  //   this.showReport = false;
  //   let matchId;
  //   if (this.sssConsolidate.value.type == "State") {
  //     matchId = this.sssConsolidate.value.stateInfo;
  //   } else if (this.sssConsolidate.value.type == "Headquarter") {
  //     matchId = this.sssConsolidate.value.districtInfo;
  //   }
  //   this.sssConsolidate.value.stateInfo = [this.sssConsolidate.value.stateInfo];
  //  //console.log(this.sssConsolidate.value)
  //   this.stockiestService.getSubmittedSaleStateAndHQwise(this.currentUser.companyId, this.sssConsolidate.value, this.currentUser.company.sssType).subscribe(res => {
  //     console.log("result",res)

  //     if (res.length == 0 || res == undefined) {
  //       this.showProcessing = false;
  //       this.showReport = true;

  // this._ToastrService.error("No Record Found");
  //   } else {
  //       console.log(res);
  //       this.dataSource = res;
  //       this.showProcessing = false;
  //       this.showReport = true;
  //     }
  //     this._changeDetectorRef.detectChanges();

  //   }, err => {
  //     this.showProcessing = false;
  //     this.showReport = false;
  //     console.log(err)
  //             this._ToastrService.error("No Record Found");

  //     this._changeDetectorRef.detectChanges();

  //   })

  // }
  viewRecords() {

    this.isToggled = false;
    this.showProcessing = true;
    this.showReport = false;
    if (this.sssConsolidate.value.type == "Headquarter") {
      this.sssConsolidate.value.districtInfo;
    }
    // this.sssConsolidate.value.stateInfo = [this.sssConsolidate.value.stateInfo];
    let stokistInfo = []
    if (this.sssConsolidate.value.reportType === 'Stokistwise') {
      this.sssConsolidate.value.stokistInfo.forEach(element => {
        stokistInfo.push({
          stkId: element.id,
          stkName: element.providerName,
          stkCode: element.providerCode
        })
      });
      this.sssConsolidate.patchValue({ stokistInfo: stokistInfo });
    }

    this.stockiestService.getSubmittedSaleStateAndHQwise(this.currentUser.companyId, this.sssConsolidate.value, this.currentUser.company.sssType).subscribe(res => {
      this.dataSource = res;
      this._changeDetectorRef.detectChanges();
      this.dataSource.forEach(i => {
        if (i.partyNameDoctor || i.partyName) {
          this.showStockistDoctorInTable = true;
        }
        i["openingValue"] = i.opening * i.productRate
      });

      if (this.dataSource.length > 0) {
        this.showProcessing = false;
        this.showReport = true;
        this._changeDetectorRef.detectChanges();

        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,

          "fixedColumns": {
            leftColumns: 1,
            rightColumns: 17
          },
          destroy: true,
          data: this.dataSource,
          responsive: true,
          columns: [
            {
              title: 'Product Name',
              data: 'productName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return '<div style="text-align: left">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Doctor',
              data: 'partyNameDoctor',
              visible: this.showStockistDoctorInTable,
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return '<div style="text-align: left">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Stockist',
              data: 'partyName',
              visible: this.showStockistDoctorInTable,
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return '<div style="text-align: left">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Product Rate',
              data: 'productRate',
              render: function (data) {
                if (data === '') {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: "Opening",
              data: 'openingQty',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: "Opening Value",
              data: "opening",
              render: function (data) {
                if (data === '') {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Primary Sale',
              data: 'totalPrimaryQty',
              render: function (data) {
                if (data === '') {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Primary Value',
              data: 'totalPrimaryAmt',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Return',
              data: 'totalSaleReturnQty',
              render: function (data) {
                if (data === '') {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            }, {
              title: 'Return Value',
              data: 'totalSaleReturnAmt',
              render: function (data) {
                if (data === '') {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Secondary Sale',
              data: 'secondarySaleQty',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: ' Secondary Value',
              data: 'secondarySaleValue',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            },

            {
              title: 'Closing',
              data: 'closing',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Closing Value',
              data: 'closingvalue',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Expiry',
              data: 'expiryQty',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Expiry Value',
              data: 'expiry',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Breakage',
              data: 'breakageQty',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Breakage Value',
              data: 'breakage',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Batch Recall',
              data: 'batchRecallQty',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + data + '</div>';
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Batch Recall Value',
              data: 'batchRecall',
              render: function (data) {
                if (!Boolean(data)) {
                  return '<div style="text-align: right">' + 0 + '</div>';
                } else {
                  return '<div style="text-align: right">' + (data).toFixed(2) + '</div>';
                }
              },
              defaultContent: '---'
            }

          ],

          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
          },
          dom: 'Bfrtip',
          // buttons: [
          //   'copy', 'csv', 'excel', 'print'
          // ],
          buttons: [
            { extend: 'copy', footer: true },
            { extend: 'csv', footer: true },
            { extend: 'excel', footer: true },
            { extend: 'print', footer: true }
          ],

          footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
            var intVal = function (i) {
              return typeof i === 'string' ?
                parseFloat(i.replace(/[\$,]/g, '')) * 1 :
                typeof i === 'number' ?
                  i : 0;

            };

            if (this.showStockistDoctorInTable) {
              //Product Rate
              const colTotal_2 = api
                .column(3)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(3).footer()).html(
                Math.round(colTotal_2)
              );

              //Opening qty
              const colTotal_3 = api
                .column(4)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(4).footer()).html(
                Math.round(colTotal_3)
              );

              //Opening Value
              const colTotal_4 = api
                .column(5)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(5).footer()).html(
                Math.round(colTotal_4)
              );

              //Primary Sale
              const colTotal_5 = api
                .column(6)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(6).footer()).html(
                Math.round(colTotal_5)
              );

              //Primary Value
              const colTotal_6 = api
                .column(7)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(7).footer()).html(
                Math.round(colTotal_6)
              );

              //Return
              const colTotal_7 = api
                .column(8)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(8).footer()).html(
                Math.round(colTotal_7)
              );

              //Return Value
              const colTotal_8 = api
                .column(9)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(9).footer()).html(
                Math.round(colTotal_8)
              );

              //Secondary Sale
              const colTotal_9 = api
                .column(10)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(10).footer()).html(
                Math.round(colTotal_9)
              );

              //Secondary Value
              const colTotal_10 = api
                .column(11)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(11).footer()).html(
                Math.round(colTotal_10)
              );

              //Closing
              const colTotal_11 = api
                .column(12)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(12).footer()).html(
                Math.round(colTotal_11)
              );

              //Closing Value
              const colTotal_12 = api
                .column(13)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(13).footer()).html(
                Math.round(colTotal_12)
              );

              // Expiry Qty
              const colTotal_13 = api
                .column(14)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(14).footer()).html(
                Math.round(colTotal_13)
              );

              // Expiry Value
              const colTotal_14 = api
                .column(15)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(15).footer()).html(
                Math.round(colTotal_14)
              );

              // Breakage Qty
              const colTotal_15 = api
                .column(16)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(16).footer()).html(
                Math.round(colTotal_15)
              );

              // Breakage Value
              const colTotal_16 = api
                .column(17)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(17).footer()).html(
                Math.round(colTotal_16)
              );

              // Batch Recall Qty
              const colTotal_17 = api
                .column(18)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(18).footer()).html(
                Math.round(colTotal_17)
              );

              // Batch Recall Value
              const colTotal_18 = api
                .column(19)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(19).footer()).html(
                Math.round(colTotal_18)
              );

            } else {
              //Product Rate
              const colTotal_2 = api
                .column(3)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(3).footer()).html(
                Math.round(colTotal_2)
              );

              //Opening qty
              const colTotal_3 = api
                .column(4)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(4).footer()).html(
                Math.round(colTotal_3)
              );

              //Opening Value
              const colTotal_4 = api
                .column(5)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(5).footer()).html(
                Math.round(colTotal_4)
              );

              //Primary Sale
              const colTotal_5 = api
                .column(6)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(6).footer()).html(
                Math.round(colTotal_5)
              );

              //Primary Value
              const colTotal_6 = api
                .column(7)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(7).footer()).html(
                Math.round(colTotal_6)
              );

              //Return
              const colTotal_7 = api
                .column(8)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(8).footer()).html(
                Math.round(colTotal_7)
              );

              //Return Value
              const colTotal_8 = api
                .column(9)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(9).footer()).html(
                Math.round(colTotal_8)
              );

              //Secondary Sale
              const colTotal_9 = api
                .column(10)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(10).footer()).html(
                Math.round(colTotal_9)
              );

              //Secondary Value
              const colTotal_10 = api
                .column(11)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(11).footer()).html(
                Math.round(colTotal_10)
              );

              //Closing
              const colTotal_11 = api
                .column(12)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(12).footer()).html(
                Math.round(colTotal_11)
              );

              //Closing Value
              const colTotal_12 = api
                .column(13)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(13).footer()).html(
                Math.round(colTotal_12)
              );

              // Expiry Qty
              const colTotal_13 = api
                .column(14)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(14).footer()).html(
                Math.round(colTotal_13)
              );

              // Expiry Value
              const colTotal_14 = api
                .column(15)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(15).footer()).html(
                Math.round(colTotal_14)
              );

              // Breakage Qty
              const colTotal_15 = api
                .column(16)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(16).footer()).html(
                Math.round(colTotal_15)
              );

              // Breakage Value
              const colTotal_16 = api
                .column(17)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(17).footer()).html(
                Math.round(colTotal_16)
              );

              // Batch Recall Qty
              const colTotal_17 = api
                .column(18)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(18).footer()).html(
                Math.round(colTotal_17)
              );

              // Batch Recall Value
              const colTotal_18 = api
                .column(19)
                .data()
                .reduce(function (a, b) {
                  return intVal(a) + intVal(b);
                }, 0);
              $(api.column(19).footer()).html(
                Math.round(colTotal_18)
              );

            }
          }
        }
        // this.dtOptions.columns[7].visible = false;
        // this.dtOptions.columns[8].visible = false;
        // this.dtOptions.columns[12].visible = false;
        // this.dtOptions.columns[14].visible = false;

        // if(this.showStockistDoctorInTable){
        //   this.dtOptions.columns.push();
        //   this.dtOptions.columns.push();
        // }
        // this.dtOptions.columns.unshift();


        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
        this._changeDetectorRef.detectChanges();


      } else {
        this._ToastrService.error("No Record Found");
        //  this.dataSource = res;
        this.showProcessing = false;
        this.showReport = false;
      }

      this._changeDetectorRef.detectChanges();

    }, err => {
      this.showProcessing = false;
      this.showReport = false;
      console.log(err)
      this._ToastrService.error("No Record Found");
      this._changeDetectorRef.detectChanges();

    })

  }

}