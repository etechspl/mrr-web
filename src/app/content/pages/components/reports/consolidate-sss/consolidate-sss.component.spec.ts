import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidateSssComponent } from './consolidate-sss.component';

describe('ConsolidateSssComponent', () => {
  let component: ConsolidateSssComponent;
  let fixture: ComponentFixture<ConsolidateSssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolidateSssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolidateSssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
