import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
export interface matRangeDatepickerRangeValue<D> {
  begin: D | null;
  end: D | null;
}

import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { BlockUserService } from '../../../../../core/services/BlockUser/block-user.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'm-block-user',
  templateUrl: './block-user.component.html',
  styleUrls: ['./block-user.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class BlockUserComponent implements OnInit {
  blockForm: FormGroup;
  blockFormForManager: FormArray;
  managerTeam: any;
  onlyManager: any;
  currentUser = JSON.parse(sessionStorage.currentUser);
  header: string;
  buttonLabel: string;
  constructor(
    public dialogRef: MatDialogRef<BlockUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _hierarchyService: HierarchyService,
    private _blockUserService: BlockUserService) {
    //console.log("Selected Row : ", data);
    this.header = `Enter ${data.name}'s Block Detail`;
    this.buttonLabel = 'Block'
    if (data.designationLevel == 1) {
      this.blockForm = new FormGroup({
        blockReason: new FormControl(null, Validators.required),
        blockDate: new FormControl(null, Validators.required),
        releaseDate: new FormControl(null)
      });
    } else if (data.callingFor == 'Block' && data.designationLevel > 1) {
      this.blockForm = new FormGroup({
        blockReason: new FormControl(null, Validators.required),
        blockDate: new FormControl(null, Validators.required),
        releaseDate: new FormControl(null),
        teamData: new FormArray([])
      });

      const obj = {
        supervisorId: data.id,
        companyId: data.companyId,
        status: true,
        type: "lowerWithUpper"
      }
      this._hierarchyService.getManagerHierarchy(obj).subscribe((res: any) => {
        this.managerTeam = res.filter(res => res.hierarchy === 'lower' && res.designationLevel !== 0);
        this.onlyManager = res.filter(res => res.hierarchy === 'upper' && res.designationLevel > 1);

        //console.log("Team managerTeam: ", this.managerTeam);
        //console.log("onlyManager : ", this.onlyManager);

        for (let object of this.managerTeam) {
          object = { ...object, newManagerDetails: { userId: this.onlyManager[0].userId } }
          this.addTeamMemberGroupToForm(object);
        }

      })
    } else if (data.callingFor == 'Unblock' && data.designationLevel > 1) {
      this.buttonLabel = 'Unblock';
      this.header = `Enter ${data.name}'s Unblock Detail`
      //console.log("Selected Row : ", data);
      this.blockForm = new FormGroup({
        //blockReason: new FormControl(null, Validators.required),
        //blockDate: new FormControl(null, Validators.required),
        releaseDate: new FormControl(new Date()),
        teamData: new FormArray([])
      });

      const obj = {
        supervisorId: data.id,
        companyId: data.companyId,
        status: true,
        type: "upperWithMgr"
      }
      this._hierarchyService.getManagerHierarchy(obj).subscribe((res: any) => {
        this.onlyManager = res
      })

      const passindObjectForBlockUserDetails = {
        where: {
          userId: data.id
        },
        order: 'createdAt DESC',
        limit: 1
      }
      this._blockUserService.getBlockUserDetails(passindObjectForBlockUserDetails).subscribe((res: any) => {
        //console.log(res);
        this.blockForm.addControl('userBlockRowId', new FormControl(res[0].id));
        this.blockForm.addControl('previousTeamAndManager', new FormControl(res[0].transferHierarchyTo));
        
        for (let object of res[0].transferHierarchyTo) {
          object = { ...object, userId: object.teamMemberId, newManagerDetails: { userId: data.id } }
          this.addTeamMemberGroupToForm(object);
        }
      });
    }

  }

  addTeamMemberGroupToForm(data: object): void {
    (<FormArray>this.blockForm.controls.teamData).push(this.addTeamMember(data));
  }
  addTeamMember(data: any): FormGroup {
    //console.log(data);

    return new FormGroup({
      stateName: new FormControl(data.stateName),
      districtName: new FormControl(data.districtName),
      name: new FormControl(data.name),
      designation: new FormControl(data.designation),
      designationLevel: new FormControl(data.designationLevel),
      teamMemberId: new FormControl(data.userId),
      newManagerId: new FormControl(data.newManagerDetails.userId, Validators.required)
    });
  }

  ngOnInit() {
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
  submitForm(): void {
    if('teamData' in this.blockForm.value){ 
      //For Manager Only
      const passindData = this.blockForm.value.teamData.map((ele: any) => {
        const newManagerDetails = this.onlyManager.find(res => res.userId == ele.newManagerId);
        ele = { ...ele, newManagerDetails }
        return ele
      })
      // console.log("Data : ", passindData);
      this.blockForm.value.teamData = passindData;
    }
    
    this.dialogRef.close(this.blockForm.value);
  }

}
