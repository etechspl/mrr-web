import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMefTrackerComponent } from './view-mef-tracker.component';

describe('ViewMefTrackerComponent', () => {
  let component: ViewMefTrackerComponent;
  let fixture: ComponentFixture<ViewMefTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMefTrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMefTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
