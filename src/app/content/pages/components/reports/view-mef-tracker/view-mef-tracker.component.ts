import { EmployeeComponent } from '../../filters/employee/employee.component';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { MefService } from '../../../../../core/services/Mef/mef.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'm-view-mef-tracker',
  templateUrl: './view-mef-tracker.component.html',
  styleUrls: ['./view-mef-tracker.component.scss']
})
export class ViewMefTrackerComponent implements OnInit {



  showStateReportType = true;
  showEmpReportType = true;
  showHqReportType = true;
  designationFilterForUserwiseOnly = false;
  employeeFilter = false;
  designationFilter = false;
  designationFilterForMgr = false;
  showMgrSelfReportType = false;
  designationData = [];
  isShowMefTracker = false;

  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;

  constructor(
    private fb: FormBuilder,
    private _toastr: ToastrService,
    private userDetailsService: UserDetailsService,
    private _mefService: MefService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { this.createForm(fb);}

  ngOnInit() {
    this.reportType('Employeewise');
    if (this.currentUser.userInfo[0].rL === 1) {
      this.setEmployeeDetail([this.currentUser.userInfo[0].userId]);
    }
  }

  displayedColumns = ['sno', 'userName', 'areaName', 'providerName','mefDate' ,'focusProduct', 'visitDates', 'competitorProduct',
    'productDiscussed', 'drProductProblem', 'problemSolnToDr', 'doctorFeedback', 'isQuerySolnGiven',
    'querySoln', 'seniorHelp', 'isInformToSenior', 'isSolnFromSenior', 'solnGivenBySenior',
    'quoteGiven', 'quoteDate', 'quoteStatus', 'quoteOthers', 'conversionDone', 'conversionNoReason'];

  currentUser = JSON.parse(sessionStorage.currentUser);
  public viewMEF:FormGroup;
  createForm(fb: FormBuilder){
    this.viewMEF = new FormGroup({
      reportType: new FormControl(null),
      userInfo: new FormControl(null, Validators.required),
      designation: new FormControl(null),
      fromDate: new FormControl(null, Validators.required),
      toDate: new FormControl(null, Validators.required),
    })
  }
 

  reportType(val) {
    this.isShowMefTracker = false;
    this.createForm(this.fb);
    if (this.currentUser.userInfo[0].rL == 1) {
      this.showStateReportType = false;
      this.showEmpReportType = false;
      this.showHqReportType = false;
      this.employeeFilter = false;
      this.designationFilter = false;
      this.designationFilterForUserwiseOnly = false;
      this.designationFilterForMgr = false;
      this.showMgrSelfReportType = false;
    } else if (this.currentUser.userInfo[0].rL > 1) {
      this.designationFilterForMgr = true;
      this.showMgrSelfReportType = true;
      this.showStateReportType = false;
      this.showEmpReportType = true;
      this.showHqReportType = false;
      this.employeeFilter = true;
      this.designationFilter = false;
      this.designationFilterForUserwiseOnly = false;
    }
    this.viewMEF.patchValue({ reportType: val })
  }

  setEmployeeDetail(val) {
    this.isShowMefTracker = false;
    this.viewMEF.patchValue({ userInfo: val })
  }

  getEmployeeValue(val) {
    this.isShowMefTracker = false;
    let district = [];
    if (this.viewMEF.value.reportType == 'Employeewise') {
      this.viewMEF.patchValue({ designation: val });
      this.callEmployeeComponent.getMgrList(this.currentUser.companyId, this.viewMEF.value.stateInfo, val, [true]);
    }
  }

  getMgrEmployee(val) {
    this.isShowMefTracker = false;
    this.viewMEF.patchValue({ designation: val.designationLevel });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, [true]);
  }

  mgrSelf(val) {
    this.isShowMefTracker = false;
    this.designationFilterForMgr = false;
    this.employeeFilter = false;
    this.setEmployeeDetail([this.currentUser.userInfo[0].userId]);
  }

  getFromDate(val) {
    this.isShowMefTracker = false;
    this.viewMEF.patchValue({ fromDate: val });
  }
  getToDate(val) {
    this.isShowMefTracker = false;
    this.viewMEF.patchValue({ toDate: val });
  }

  dataSource = [];
  viewMEFDetail() {
    this._mefService.getMefDetail(this.currentUser.companyId, this.viewMEF.value).subscribe(getMef => {
      if (getMef.length > 0) {
        this.dataSource = getMef;
        this.isShowMefTracker = true;
        this._changeDetectorRef.detectChanges();
      } else {
        this._changeDetectorRef.detectChanges();
        this._toastr.success("Please select another details", "No MEF Found for selected Details !!");
      }
    }, err => {
    });
  }


}
