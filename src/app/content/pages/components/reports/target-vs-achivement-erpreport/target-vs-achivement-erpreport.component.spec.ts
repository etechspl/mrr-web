import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetVsAchivementERPReportComponent } from './target-vs-achivement-erpreport.component';

describe('TargetVsAchivementERPReportComponent', () => {
  let component: TargetVsAchivementERPReportComponent;
  let fixture: ComponentFixture<TargetVsAchivementERPReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetVsAchivementERPReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetVsAchivementERPReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
