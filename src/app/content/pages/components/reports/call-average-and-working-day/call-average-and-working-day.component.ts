import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StateComponent } from '../../filters/state/state.component';
import { NativeDateAdapter, DateAdapter, MatDateFormats, MAT_DATE_FORMATS, MatStepper, MatSlideToggleChange } from "@angular/material";
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import * as _moment from 'moment';
import { UtilsService, isString } from '../../../../../core/services/utils.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import * as XLSX from 'xlsx';
export interface matRangeDatepickerRangeValue<D> {
  begin: D | null;
  end: D | null;
}
export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      let day: string = date.getDate().toString();
      day = +day < 10 ? '0' + day : day;
      let month: string = (date.getMonth() + 1).toString();
      month = +month < 10 ? '0' + month : month;
      let year = date.getFullYear();
      return `${day}-${month}-${year}`;
    }
    return date.toDateString();
  }
}
export const APP_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
  },
  display: {
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'numeric' },
    dateA11yLabel: {
      year: 'numeric', month: 'long', day: 'numeric'
    },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};
@Component({
  selector: 'm-call-average-and-working-day',
  templateUrl: './call-average-and-working-day.component.html',
  styleUrls: ['./call-average-and-working-day.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})


export class CallAverageAndWorkingDayComponent implements OnInit {
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('tableData') tableRef: ElementRef;

  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  showProcessing: boolean = false;
  showReportType: boolean = true;
  showType: boolean = true;
  showState: boolean = false;
  showDistrict: boolean = false;
  showReport: boolean = false;
  showEmployee: boolean = false;
  showDesignation: boolean = false;
  showDivisionFilter: boolean = false;
  isShowTeamInfo: boolean = false;
  dynamicTableHeader: any = [];
  currentDate = new Date();

  constructor(private utilService: UtilsService, private _DCRReportService: DcrReportsService, private _ChangeDetectorRef: ChangeDetectorRef) { }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }

    this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.currentUser.userInfo[0].rL == 1) {
      this.showReportType = false;
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showType = false;
      this.showDivisionFilter = false;
    }
  }

  form = new FormGroup({
    companyId: new FormControl(this.currentUser.companyId),
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    division: new FormControl(null),
    stateIds: new FormControl(null),
    districtIds: new FormControl(null),
    employeeIds: new FormControl(null),
    designation: new FormControl(null),
    status: new FormControl(true),
    dateRange: new FormControl(null, Validators.required)

  });

  getReportType(val) {
    this.showReport = false;
    this.showProcessing = false;
    this.showReport = false;
    if (this.isDivisionExist === true) {
      this.callDivisionComponent.setBlank();
    }
    if (val === "Geographical") {
      this.showState = false;
      this.showDesignation = false;
      this.showEmployee = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else {
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showEmployee = false;
      this.showDivisionFilter = false;
      this.form.patchValue({type: ''});
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }

  }

  getTypeValue(val) {

    this.showProcessing = false;
    this.showReport = false;
    this.form.patchValue({ type: val });

    if (val == "Headquarter" && (this.currentUser.userInfo[0].rL > 1 || this.currentUser.userInfo[0].rL == 0)) {
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = true;
      this.showDistrict = true;
      this.form.removeControl("designation");
      this.form.removeControl("employeeIds");
      this.form.setControl("stateIds", new FormControl('',Validators.required));
      this.form.setControl("districtIds", new FormControl('', Validators.required));
    } else {
      this.showDistrict = false;
    }

    if (this.currentUser.userInfo[0].rL === 0 && val === 'State') {
      this.showState = true;
      this.callStateComponent.setBlank();
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.form.removeControl("districtIds");
      this.form.removeControl("employeeIds");
      this.form.removeControl("designation");
      this.form.setControl("stateIds", new FormControl('', Validators.required))
    } else {
      if (this.form.value.type === 'Self') {
        this.showState = false;
        this.isShowTeamInfo = false;
        this.showDesignation = false;
        this.showEmployee = false;
        this.showDivisionFilter = false;
      } else if (this.form.value.type === 'Employee Wise') {
        this.showState = false;
        this.isShowTeamInfo = true;
        this.showDesignation = true;
        this.form.removeControl("districtIds");
        this.form.removeControl("stateIds");
        this.form.setControl("employeeIds", new FormControl('', Validators.required));
        this.form.setControl("designation", new FormControl('', Validators.required));
        this.showEmployee = true;
        if (this.currentUser.company.isDivisionExist == false) {
          this.showDivisionFilter = false;
        } else if (this.currentUser.company.isDivisionExist == true) {
          this.showDivisionFilter = true;
        }
      } else if (this.form.value.type === 'State' && this.currentUser.company.isDivisionExist == false) {
        this.showState = true;
        this.callStateComponent.getUniqueStateOnManager(this.currentUser.companyId, this.currentUser.id)
      } else if (this.form.value.type === 'State' && this.currentUser.company.isDivisionExist == true) {
        this.showState = true;
      }
    }
  }

  getDivisionValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.form.patchValue({ division: val });

    if (this.currentUser.userInfo[0].designationLevel == 0) {
      if (this.form.value.type == "Employee Wise") {
        //=======================Clearing Filter===================

        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.form.value.type == "State" || this.form.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================


        let divisionIds = {
          division: val
        };
        this.callStateComponent.getStateBasedOnDivision(divisionIds);
      }
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      if (this.form.value.type == "Employee Wise") {
        //=======================Clearing Filter===================
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================
        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }
        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.form.value.type == "State" || this.form.value.type == "Headquarter") {
        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================
        let passingObj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: true,
          division: this.form.value.division,
          supervisorId: this.currentUser.id,
        }
        this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)
      }
    }

  }
  getStateValue(val) {
    this.showProcessing = false;
    this.showReport = false;

    this.form.patchValue({ stateIds: val });
    this.callDistrictComponent.setBlank();

    if (this.form.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.form.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.form.patchValue({ districtIds: val });
  }
  getEmployeeValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.form.patchValue({ employeeIds: val });
  }
  getStatusValue(event: any) {
    this.form.patchValue({ status: event });
  }

  getDesignation(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.form.patchValue({ designation: val.designationLevel });

    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.form.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel] //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.form.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

    }
  }

  hideTable():void{
    this.showReport = false;
    this.showProcessing = false;
  }
  data: any = [];

  viewRecords(): void {
    this.showReport = false;
    this.isToggled= false;
    //let fromDate = _moment(new Date(this.form.value.dateRange.begin)).format("YYYY-MM-DD");
   // let toDate = _moment(new Date(this.form.value.dateRange.end)).format("YYYY-MM-DD");

    const fromDate =  _moment(new Date(new Date(this.form.value.dateRange.begin).setHours(0,0,0,0))).format("YYYY-MM-DD");
    const toDate = _moment(new Date(new Date(this.form.value.dateRange.end).setHours(0,0,0,0))).format("YYYY-MM-DD");

    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(fromDate, toDate);
    
    this.showProcessing = true;
    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.form.value.division;
    }

    let params = {
     // ...this.form.value,
       companyId:this.currentUser.companyId,
       dateRange: {begin: fromDate, end: toDate},
       type:this.form.value.type,
       "reportType":this.form.value.reportType,
       stateIds:this.form.value.stateIds,
       districtIds:this.form.value.districtIds,
        status:this.form.value.status,
       employeeIds:this.form.value.employeeIds,
       designation:this.form.value.designation,
       unlistedValidations: {
        unlistedDocCall: this.currentUser.company.validation.unlistedDocCall,
        unlistedVenCall: this.currentUser.company.validation.unlistedVenCall,
        unlistedDocAvg: this.currentUser.company.validation.unlistedDocAvg,
        unlistedVenAvg: this.currentUser.company.validation.unlistedVenAvg,
      },
      isDivisionExist: this.currentUser.company.isDivisionExist,
      division: divisionIds
    }
    

    this._DCRReportService.getDoctorCallAverage(params).subscribe(res => {
      this.showProcessing = false;
      this.data = res;
      console.log("this.data ++++++++",this.data);
      this.showReport = true;
      this._ChangeDetectorRef.detectChanges();
    }, err => {
      this.showReport = false;
      this._ChangeDetectorRef.detectChanges();
    })
    
  }

  calculateTotal(index, dataIndex) {
    const data: any[] = this.data[index].docVenData
    let total = data.reduce((accumulator, currentValue) => {
      return parseInt(accumulator) + parseInt(currentValue.data[dataIndex])
    }, 0)
    return total
  }
  exportToExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tableRef.nativeElement );
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, 'Call Average Report.xlsx');
  }
}



