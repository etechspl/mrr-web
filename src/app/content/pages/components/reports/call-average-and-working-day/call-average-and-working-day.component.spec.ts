import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallAverageAndWorkingDayComponent } from './call-average-and-working-day.component';

describe('CallAverageAndWorkingDayComponent', () => {
  let component: CallAverageAndWorkingDayComponent;
  let fixture: ComponentFixture<CallAverageAndWorkingDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallAverageAndWorkingDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallAverageAndWorkingDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
