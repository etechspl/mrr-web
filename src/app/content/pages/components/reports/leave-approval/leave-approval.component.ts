import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { LeaveApprovalService } from '../../../../../core/services/leave-approval.service';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { MatDialog, MatTable, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { EditLeaveApprovalComponent } from '../edit-leave-approval/edit-leave-approval.component';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
declare var $;


@Component({
  selector: 'm-leave-approval',
  templateUrl: './leave-approval.component.html',
  styleUrls: ['./leave-approval.component.scss']
})
export class LeaveApprovalComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);
  @ViewChild('dataTable') table;
  @ViewChild('dataTable2') table2;
  @ViewChild('dataTable3') table3;
  @ViewChild('MatTable') matTable: MatTable<any>;
  @ViewChild('sort') sort: MatSort;
  @ViewChild('paginator3') paginator3: MatPaginator;

  displayedColumns: any;
  displayedColumns3 = ['select', 'sn', 'requestDate', 'appStatus', 'leaveType', 'leaveReason'];
  dataSource;
  dataSource2;
  dataSource3;
  dataSource4: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);


  dataTable: any;
  dataTable2: any;
  dataTable3: any;
  dtOptions: any;
  dtOptions2: any;
  dtOptions3: any;
  public showProcessing: boolean = false;
  public showDistrictWise: boolean = false;
  public showUserWise: boolean = false;
  districtId: any;
  leaveTypes = [
    { value: 'CL', viewValue: 'CL' },
    { value: 'PL', viewValue: 'PL' },
    { value: 'SL', viewValue: 'SL' }
  ];
  selectedValue = [];
  UserWiseRowObject: any;
  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _LeaveApprovalService: LeaveApprovalService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private _toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getLeaveApproval();
  }

  // <><><><><>*****   AAKASH 18-03-2020  *****<><><><>
  getLeaveApproval() {
    this.showProcessing = true;
    this.showUserWise = false;
    this.showDistrictWise = false;
    if (this.currentUser.userInfo[0].rL === 0) {
      let params = {
        companyId: this.currentUser.companyId,
        type: 'districtCount'
      }
      this._LeaveApprovalService.getLeavesForApproval(params).subscribe(res => {

        if (res.length == 0 || res == undefined) {
          // this.showLeavesTable = false;
          this.toastr.info("No Record Found.");
          this.showProcessing = false;
          this.showDistrictWise = false;
          this.showUserWise = false;
        } else {
          this.showProcessing = false;
          this.dataSource = [];
          this.dataSource = res;
          this.displayedColumns = ['stateName', 'districtName', 'districtLeaveCount'];
          // this.showLeavesTable = true;

          this.dtOptions = {
            "pagingType": 'full_numbers',
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "scrollCollapse": true,
            "paging": false,
            destroy: true,
            data: res,
            responsive: true,
            columns: [
              {
                title: 'State Name',
                data: 'stateName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'District Name',
                data: 'districtName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return `<a class='getDistrictWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
                  }
                },
                defaultContent: '---'
              },
              {
                title: '# Leave Counts',
                data: 'districtLeaveCount',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }

            ],
            rowCallback: (row: Node, data: any[] | Object, index: number) => {
              const self = this;

              $('td', row).unbind('click');
              $('td .getDistrictWiseReport', row).bind('click', () => {
                let rowObject = JSON.parse(JSON.stringify(data));
                this.getDistrictWise(rowObject.districtId);
                this._changeDetectorRef.detectChanges();
              });
              return row;
            },
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search Records",
            },
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'print',
                exportOptions: {
                  columns: ':visible'
                }
              },
            ]
          };
          this._changeDetectorRef.detectChanges();
          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          this._changeDetectorRef.detectChanges();

        }
      })
    }
    this._changeDetectorRef.detectChanges();

  }
  // <><><><><>*****   AAKASH 18-03-2020  *****<><><><>
  getDistrictWise(val) {
    this.districtId = val;
    this.showDistrictWise = true;
    this.showUserWise = false;
    this.showProcessing = true;
    let params = {
      companyId: this.currentUser.companyId,
      districtId: val,
      type: 'districtWise'
    }
    this._LeaveApprovalService.getLeavesForApproval(params).subscribe(res => {
      if (res.length == 0 || res == undefined) {
        // this.showLeavesTable = false;
        this.toastr.info("No Record Found.")
        this.showProcessing = false;
        this.showDistrictWise = false;
        this.showUserWise = false;
      } else {
        this.showProcessing = false;
        this.dataSource2 = res;
        this.displayedColumns = ['name', 'designation', 'districtName', 'leaveCount'];

        this.dtOptions2 = {
          "pagingType": 'full_numbers',
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "scrollCollapse": true,
          "paging": false,
          destroy: true,
          data: res,
          responsive: true,
          columns: [
            {
              title: 'Employee Name',
              data: 'userDetails.name',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return `<a class='getUserWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
                }
              },
              defaultContent: '---'
            }, {
              title: 'Designation',
              data: 'userDetails.designation',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'District Name',
              data: 'district',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: '# Leave Counts',
              data: 'leaveCount',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }

          ],
          rowCallback: (row: Node, data: any[] | Object, index: number) => {
            const self = this;

            $('td', row).unbind('click');
            $('td .getUserWiseReport', row).bind('click', () => {
              let rowObject = JSON.parse(JSON.stringify(data));
              // self.openDialog(rowObject);
              this.getUserWiseLeaves(rowObject);
              this._changeDetectorRef.detectChanges();
            });
            return row;
          },
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Records",
          },
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'excel',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'csv',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'print',
              exportOptions: {
                columns: ':visible'
              }
            },
          ]
        };
        this._changeDetectorRef.detectChanges();
        this.dataTable2 = $(this.table2.nativeElement);
        this.dataTable2.DataTable(this.dtOptions2);
        this._changeDetectorRef.detectChanges();

      }
    })
    this._changeDetectorRef.detectChanges();
  }

  //creating LEAVE approval Form

  leaveApproveForm: FormGroup = this.fb.group({
    formArray: this.fb.array([]),
  });
  get formArray() {
    return this.leaveApproveForm.get('formArray') as FormArray;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource4.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.dataSource4.data.forEach((row, index) => {
        this.formArray.controls[index].patchValue({ checked: false });
      })
    } else {
      this.dataSource4.data.forEach((row, index) => {
        this.formArray.controls[index].patchValue({ checked: true });
        this.selection.select(row);
      })

    }

  }


  getSelectionValue($event, i: number, row: object) {
    this.selectedValue.push(row);
  }


  getUserWiseLeaves(obj) {
    this.UserWiseRowObject = obj;
    this.selectedValue = [];
    let params = {
      type: 'userLeaveList',
      companyId: this.currentUser.companyId,
      userId: obj.userDetails.userId
    }

    this._LeaveApprovalService.getLeavesForApproval(params).subscribe(res => {
      this.leaveApproveForm = this.fb.group({
        formArray: this.fb.array([])
      });
      if (res.length == 0 || res == undefined) {
        // this.showLeavesTable = false;
        this.toastr.info("No Record Found.")
        this.showProcessing = false;
        this.showUserWise = false;
      } else {
        this.showUserWise = true;
        this.showProcessing = false;
        this.dataSource3 = res;
        // this.displayedColumns3 = ['select', 'sn', 'requestDate', 'appStatus', 'status', 'leaveType', 'leaveReason'];
        let array = res.map(item => {
          this.formArray.push(this.fb.group({
            id: new FormControl(item._id),
            userId: new FormControl(item.userId),
            companyId: new FormControl(item.companyId),
            leaveType: new FormControl(item.leaveType),
            appStatus: new FormControl(item.appStatus),
            requestDate:new FormControl(item.requestDate),
            stateId:new FormControl(item.stateId),
            districtId:new FormControl(item.districtId),
            checked: new FormControl(false)
          }));
          return {
            ...item
          };
        });
        this._changeDetectorRef.detectChanges();
        this.dataSource4 = new MatTableDataSource(array);
        this.dataSource4.sort = this.sort;
        this.dataSource4.paginator = this.paginator3;
        this._changeDetectorRef.detectChanges();
      }
    })



  }
  approveLeave() {
    let formArrayData = this.leaveApproveForm.value.formArray
      .filter(i => i.checked)
      .map(i => { delete i.checked; return i });
    if (formArrayData.length === 0) {
      this._toastr.error('', 'Please Select the Checkbox')
    } else {
      let obj = {
        rl: this.currentUser.userInfo[0].rL,
        companyId: this.currentUser.companyId,
      }
      let noOfLeavesToBeUpdated = 0;
      for (let i = 0; i < formArrayData.length; i++) {
        obj['id'] = formArrayData[i].id;
        obj['companyId'] = formArrayData[i].companyId;
        obj['userId'] = formArrayData[i].userId;
        obj['leaveType'] = formArrayData[i].leaveType;
        obj['requestDate'] = formArrayData[i].requestDate;
        obj['stateId'] = formArrayData[i].stateId;
        obj['districtId'] = formArrayData[i].districtId;
        noOfLeavesToBeUpdated += 1;        
        this._LeaveApprovalService.approveLeave(obj).subscribe(res => {
          //this._LeaveApprovalService.updateLeaveInDCRMaster(obj).subscribe(dcrRes=>{
            this.getLeaveApproval();
            this.getDistrictWise(this.districtId);
            this.getUserWiseLeaves(this.UserWiseRowObject);
            this.showUserWise = false;
          //})
          this._changeDetectorRef.detectChanges();
        })
        this._changeDetectorRef.detectChanges();
      }
      this._toastr.success('', `Leave Approved Successfully`);
      this._changeDetectorRef.detectChanges();
      // this.showDistrictWise = false;
      this._changeDetectorRef.detectChanges();
    }
    this.showUserWise = false;
  }


  // openDialog(obj: object): void {
  //   const dialogRef = this.dialog.open(EditLeaveApprovalComponent, {
  //     height: '300px',
  //     data: { obj , dateSource:this.dataSource, dataSource2:this.dataSource2 }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     this.getDistrictWise(this.districtId);
  //   });

  // }

}
