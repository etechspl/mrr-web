import { URLService } from './../../../../../core/services/URL/url.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { DesignationService } from '../../../../../core/services/designation.service';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { MatTable, MatPaginator, MatTableDataSource, MatSlideToggleChange } from '@angular/material';
@Component({
  selector: 'm-tp-deviation',
  templateUrl: './tp-deviation.component.html',
  styleUrls: ['./tp-deviation.component.scss']
})

export class TpDeviationComponent implements OnInit {
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatPaginator)
  set paginator(value: MatPaginator) {
    this.dataSource.paginator = value;
  }
  currentUser = JSON.parse(sessionStorage.currentUser);

  tpDeviationFilter: FormGroup;

  isDesignationFilterView = false;

  displayedColumns = ['reportDate', 'dcrSubmissionDate','Headquarter', 'plannedActivity', 'plannedArea','areaHqData', 'actualVisitedArea', 'workWith', 'actualActivity', 'visitedDoctor', 'visitedChemist', 'visitedStockist', 'tpDeviationStatus'];
  dataSource;
  showTPDeviationDetails = false;

  totalDoctorVisited: number = 0;
  totalVendorVisited: number = 0;
  totalStockistVisited: number = 0;
  public showDivisionFilter: boolean = false;
  public showProcessing: boolean = false;
  DeviatedCount: number = 0;
  NoDeviationCount: number = 0;
  AdditionalVisitedCount: number = 0;

  constructor(
    private fb: FormBuilder,
    private monthYearService: MonthYearService,
    private _designation: DesignationService,
    private _tourProgramService: TourProgramService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _toastr: ToastrService,
		private _urlService: URLService
  ) {
    this.tpDeviationFilter = this.fb.group({
      viewFor: new FormControl('', Validators.required),
      designation: new FormControl('', Validators.required),
      employeeId: new FormControl('', Validators.required),
      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {

    if (this.currentUser.company.isDivisionExist == true) {

      this.tpDeviationFilter.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
    }
  }
  getDivisionValue(val) {
    this.showTPDeviationDetails = false;
    this.tpDeviationFilter.patchValue({ division: val });


    //=======================Clearing Employee Filter===================
    this.callDesignationComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: [val]
    }

    this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);



  }
  getDesignation(val) {
    this.showTPDeviationDetails = false;
    this.tpDeviationFilter.patchValue({ designation: val });

    this.callEmployeeComponent.setBlank();

    if (this.currentUser.company.isDivisionExist == true) {
      if (this.currentUser.company.isDivisionExist == true && this.tpDeviationFilter.value.division != null) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val.designationLevel,
          status: [true],
          division: [this.tpDeviationFilter.value.division]
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      }
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, [true]);
    }
  }
  getEmployees(val) {
    this.showTPDeviationDetails = false;
    this.tpDeviationFilter.patchValue({ employeeId: val });
  }
  showEmployeeFilter(event) {
    this.showTPDeviationDetails = false;
    if (event == "teamTPDeviation") {
      if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;

      }
      this.isDesignationFilterView = true;
    } else if (event == "self") {
      this.showDivisionFilter = false;
      this.isDesignationFilterView = false;
      this.tpDeviationFilter.patchValue({ employeeId: this.currentUser.userInfo[0].userId, designation: this.currentUser.userInfo[0].designationLevel });

    }
  }
  getMonth(val) {
    this.showTPDeviationDetails = false;
    this.tpDeviationFilter.patchValue({ month: val });
  }
  getYear(val) {
    this.showTPDeviationDetails = false;
    this.tpDeviationFilter.patchValue({ year: val });
  }

  showReport() {
    this.isToggled = false;
    this.showProcessing = true;
    this._tourProgramService.getTourProgramDeviation(this.currentUser.companyId, this.tpDeviationFilter.value).subscribe(res => {
     // console.log("result....",res)
      let array = [];
      array.push(res)
      //this.dataSourceForTPInDetail.push(res);
      if (array[0].length > 0) {
                                        //******************Umesh Kumar 11-05-2020 *************
		this.totalDoctorVisited = 0;
        this.totalVendorVisited = 0;
		this.totalStockistVisited = 0;

			array[0].forEach((item)=> {
    		if (item.tpDeviationStatus == "Deviated") {
          this.DeviatedCount++
         }
		    else if(item.tpDeviationStatus == "No Deviation") {
			  	this.NoDeviationCount++
			  }
			  else if(item.tpDeviationStatus == "Additional Visited") {
				 this.AdditionalVisitedCount++;
			}
			this.totalDoctorVisited += item.visitedDoctor;
			this.totalVendorVisited += item.visitedChemist;
			this.totalStockistVisited += item.visitedStockist;
		 })

        this.showProcessing = false;
        this.dataSource = new MatTableDataSource(array[0]);
        this.dataSource.paginator = this.paginator;
        this.showTPDeviationDetails = true;


        // for (let i = 0; i < this.dataSource.length; i++) {
		//   this.totalDoctorVisited = this.totalDoctorVisited + this.dataSource[i].visitedDoctor;
        //   this.totalVendorVisited = this.totalVendorVisited + this.dataSource[i].visitedChemist;
        //   this.totalStockistVisited = this.totalStockistVisited + this.dataSource[i].visitedStockist;

        // }

      } else {
        this._toastr.info('For Selected Filter data not found', 'Data Not Found');
        this.showTPDeviationDetails = false;
        this.showProcessing = false;
      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      console.log(err)
    })
  }

 
	onPreviewClick(){
   
		let printContents, popupWin;
	  
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;
		printContents = document.getElementById('tableId1').innerHTML;
		const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
	
		popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		popupWin.document.open();
		popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;
	}
	
	  tg.th {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  font-weight: normal;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4; margin: 5mm; }
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
	  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
	
	  </div>
	
	  <div style="text-align: right;">
	  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Basic Info </h2>
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  ${printContents}</body>
	  </html>
	  `
		);
	   // popupWin.document.close();
	 }

}
