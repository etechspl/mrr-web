import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpDeviationComponent } from './tp-deviation.component';

describe('TpDeviationComponent', () => {
  let component: TpDeviationComponent;
  let fixture: ComponentFixture<TpDeviationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpDeviationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpDeviationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
