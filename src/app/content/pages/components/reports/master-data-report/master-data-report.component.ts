import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import * as _moment from 'moment';

import Swal from "sweetalert2";
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StatusComponent } from '../../filters/status/status.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { StateComponent } from '../../filters/state/state.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
declare var $;
@Component({
  selector: 'm-master-data-report',
  templateUrl: './master-data-report.component.html',
  styleUrls: ['./master-data-report.component.scss']
})
export class MasterDataReportComponent implements OnInit {
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;
  public showDetails:boolean=false;
  logoURL: any;
  count = 0;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("setStatus") setStatus: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @ViewChild('dataTable') table;
  showTotalMasterData: boolean = false;

  constructor(private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private exportAsService: ExportAsService,
    private _hierarchyService: HierarchyService,
    private _urlService: URLService, private providerService:ProviderService) { } 
    isShowDivision = false;
    currentUser = JSON.parse(sessionStorage.currentUser);
    colspanValue;
    isDivisionExist = this.currentUser.company.isDivisionExist;
    designationLevel = this.currentUser.userInfo[0].designationLevel;
    companyId = this.currentUser.companyId;
    // dataSource: any;
    dataTable: any;
    dtOptions: any;
  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.masterDataForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);



    } else {
      this.isShowDivision = false;
    }
    this.getReportType('Geographical');
  }
  check: boolean = false;

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  masterDataForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    status: new FormControl(true),
    stateInfo: new FormControl(null),
    employeeId: new FormControl(null),
    designation: new FormControl(null),

  })
  getReportType(val) { 
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {

      this.showState = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }
  getTypeValue(val) {
    if (val == "State") {
      this.showDistrict = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.masterDataForm.removeControl("employeeId");
      this.masterDataForm.removeControl("designation");
      this.masterDataForm.setControl("stateInfo", new FormControl('', Validators.required))
    } else if (val == "Headquarter") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.masterDataForm.removeControl("employeeId");
      this.masterDataForm.removeControl("designation");
      this.masterDataForm.setControl("stateInfo", new FormControl('', Validators.required))
      this.masterDataForm.setControl("districtInfo", new FormControl('', Validators.required))
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.masterDataForm.removeControl("stateInfo");
      this.masterDataForm.removeControl("districtInfo");
      this.masterDataForm.setControl("designation", new FormControl('', Validators.required))
      this.masterDataForm.setControl("employeeId", new FormControl('', Validators.required))
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.masterDataForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }
  getStateValue(val) {
    this.masterDataForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.masterDataForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.masterDataForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.masterDataForm.value.divisionId
        })

        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.masterDataForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getStatusValue(event){
    this.masterDataForm.patchValue({ status: event });
  }
  getDistrictValue(val) {
    this.masterDataForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.masterDataForm.patchValue({ employeeId: val });
  }
  getDesignation(val) {
    this.masterDataForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.masterDataForm.value.divisionId,
        status: [true],
        designationObject: this.masterDataForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  //setting division into masterDataForm
  getDivisionValue($event) {
    this.masterDataForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
     if (this.masterDataForm.value.type === "State" || this.masterDataForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.masterDataForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
     }

      if (this.masterDataForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.masterDataForm.value.divisionId != null && this.masterDataForm.value.designation != null) {
          if (this.masterDataForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.masterDataForm.value.divisionId,
              status: [true],
              designationObject: this.masterDataForm.value.designation
            })
          }
        }
      }
    }
  }
  viewRecords(){
    this.isToggled = false;
    this.showReport = false;
    this.showProcessing = true;
    this.showTotalMasterData = false;
    this.showDetails=true;
    if (this.masterDataForm.value.employeeId == null) {
      this.masterDataForm.value.employeeId = [this.currentUser.id];
    }
    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.masterDataForm.value.divisionId;
    } else {
      params["division"] = [];
    }

    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.masterDataForm.value.stateInfo;
    params["districtIds"] = this.masterDataForm.value.districtInfo;
    params["type"] = this.masterDataForm.value.type;
    params["employeeId"] = this.masterDataForm.value.employeeId;
    params["status"] = this.masterDataForm.value.status;
    this.providerService.getMasterData(params).subscribe(res=>{
      if (res.length == 0 || res == undefined) {
        this.showProcessing = false;
        this.showTotalMasterData = false;
        (Swal as any).fire({
          title: "No Records Found",
          type: "info",
        });
      } else {
        this.showProcessing = false;
        this.showTotalMasterData = true;
        res.forEach((item, i) => {
          item["index"] = i + 1;
        });


        const PrintTableFunction = this.PrintTableFunction.bind(this);
        this.dtOptions = {
          pagingType: "full_numbers",
          ordering: true,
          info: true,
          scrollY: 300,
          scrollX: true,
          scrollCollapse: true,
          paging: false,
          destroy: true,
          data: res,
          responsive: true,
          columns: [
            {
              title: "S No.",
              data: "index",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "User",
              data: "user",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "State",
              data: "state",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "District",
              data: "district",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: `Total ${this.currentUser.company.lables.vendorLabel}`,
              data: "totalDrug",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: `Total ${this.currentUser.company.lables.stockistLabel}`,
              data: "totalStockist",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: `Total ${this.currentUser.company.lables.doctorLabel}`,
              data: "totalRMP",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: `Total ${this.currentUser.company.lables.areaLabel}`,
              data: "totalArea",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
     
          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Records",
          },
          dom: "Bfrtip",
          buttons: [
            {
              extend: "excel",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: "csv",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: 'print',
              title: function () {
              return 'State List';
              },
              exportOptions: {
              columns: ':visible'
              },
              action: function (e, dt, node, config) {
              PrintTableFunction(res);
                  }
                 }
          ],
        };
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
      }
    })
    
    
  }

  
  PrintTableFunction(data?: any): void {
    const companyName = this.currentUser.companyName;
  
    const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
  
    const tableRows: any = [];
  
  
    data.forEach(element => {
  
    tableRows.push(`<tr>
    <td class="tg-oiyu">${element.index}</td>
    <td class="tg-oiyu">${element.user}</td>
    <td class="tg-oiyu">${element.state}</td>
    <td class="tg-oiyu">${element.district}</td>
    <td class="tg-oiyu">${element.totalDrug}</td>
    <td class="tg-oiyu">${element.totalStockist}</td>
    <td class="tg-oiyu">${element.totalRMP}</td>
    <td class="tg-oiyu">${element.totalArea}</td>
    </tr>`)
    });
  
    let showHeaderAndTable: boolean = false;
    // this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
    let printContents, popupWin;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
    <head>
    <style>
    img {
    float: left;
    }
  
    .flex-container {
    display: flex;
    justify-content: space-between;
    }
  
    .tg {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }
  
    .tg td {
    font-family: Arial, sans-serif;
    font-size: 14px;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
    
    .tb2 .tg-oikpyu {
      font-size: 10px;
      text-align: center;
      vertical-align: middle
      }
  
    
    .tg th {
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tg .tg-lboi {
    border-color: inherit;
    text-align: left;
    vertical-align: middle;
  
    }
  
    .tg .tg-u6fn {
    font-weight: bold;
    font-size: 18px;
    background-color: #efefef;
    color:black;
    border-color: inherit;
    text-align: left;
    vertical-align: top;
    padding-top: 0px;
    padding-bottom: 0px;
    }
  
    .tg .tg-yz93 {
    border-color: inherit;
    text-align: right;
    vertical-align: middle
    }
    .tb2 {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }
    
    
  
    .tb2 td {
    font-family: Arial, sans-serif;
    font-size: 12px;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 th {
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-weight: normal;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 .tg-oiyu {
    font-size: 10px;
    text-align: left;
    vertical-align: middle
    }
    .tg-bckGnd {
    font-weight: bold;
    background-color: #efefef;
    }
  
    .tb2 .tg-3ppa {
    font-size: 10px;
    background-color: #efefef;
    color:black;
    text-align: left;
    vertical-align: middle;
    padding-top: 2px;
    padding-bottom: 2px;
    }
  
    .footer {
    position: sticky;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #efefef;
    color: rgb(2, 2, 2);
    text-align: right
    }
  
    button {
    background-color: #008CBA;
    border: none;
    color: white;
    margin: 5px 0px;
    padding: 2px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    border-radius: 12px;
    cursor: pointer;
    outline: none;
    }
    @media print {
    @page { size: A4; margin: 5mm; }
    .footer {
    position: relative;
    }
    tr {
    page-break-inside: avoid;
    }
    button{
    display : none;
    }
    }
    </style>
    </head>
    <body onload="window.print();window.close()" style="padding-bottom:30px">
    <div class="flex-container">
    <div style="width: 40%; ">
    <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
    <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
  
    </div>
  
    <h2 style="margin-top: 0px; margin-bottom: 5px " ; align="left"><u>Master Data </u></h2>
    <div>
    <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
    <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
    </div>
    </div>
    </div>
  
    <div>
    
    <table class="tb2">
    <tr>
    <th class="tg-3ppa"><span style="font-weight:20">S.No.</span></th>
    <th class="tg-3ppa"><span style="font-weight:20">Employee Name</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">State</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">District</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Total Chemist</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Total Stockist</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Total Doctors</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Total Area</span></th>
                
    </tr>
    ${tableRows.join('')}
    </table>
    </div>
    
    </body>
    </html>
    `);
  
    // popupWin.document.close();
  
    }
  

}
