import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDataReportComponent } from './master-data-report.component';

describe('MasterDataReportComponent', () => {
  let component: MasterDataReportComponent;
  let fixture: ComponentFixture<MasterDataReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDataReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDataReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
