import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { CrmService } from "../../../../../core/services/CRM/crm.service";
import Swal from "sweetalert2";
import { FormGroup, FormBuilder, FormArray, FormControl } from "@angular/forms";
import { SelectionModel } from "@angular/cdk/collections";
import {
	MatTableDataSource,
	MatPaginator,
	MatSort,
	Sort,
} from "@angular/material";
declare var $;
import * as moment from "moment";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { HierarchyService } from "../../../../../core/services/hierarchy-service/hierarchy.service";
@Component({
	selector: "m-sponsorship-approval",
	templateUrl: "./sponsorship-approval.component.html",
	styleUrls: ["./sponsorship-approval.component.scss"],
})
export class SponsorshipApprovalComponent implements OnInit {
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	currentUser = JSON.parse(sessionStorage.currentUser);
	rl = this.currentUser.userInfo[0].rL;
	@ViewChild("dataTable") table;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	dataSource: any;
	dataTable: any;
	dtOptions: any;
	displayedColumns: any;

	selectedValue = [];
	selection = new SelectionModel<any>(true, []);
	showSponsorshipDetails: boolean = false;
	showCountWiseDetails: boolean = false;
	providerInfo: any;
	kindOfCRM = [
		{ value: "clinicalTrial", viewValue: "Clinical Trial" },
		{ value: "cash", viewValue: "Cash" },
		{ value: "conference", viewValue: "Conference" },
		{ value: "travel", viewValue: "Travel" },
		{ value: "stay", viewValue: "Stay" },
		{ value: "reimbursement", viewValue: "Reimbursement" },
		{ value: "conveyance", viewValue: "Conveyance" },
		{ value: "pleasureTrip", viewValue: "Pleasure Trip" },
		{ value: "cme", viewValue: "CME" },
		{ value: "other", viewValue: "Other" },
	];
	modeOfPayment = [
		{ value: "cash", viewValue: "Cash" },
		{ value: "cheque", viewValue: "Cheque" },
		{ value: "NEFT", viewValue: "NEFT" },
		{ value: "IMPS", viewValue: "IMPS" },
		{ value: "RTGS", viewValue: "RTGS" },
	];
	userData: any;
	userIds = [];
	showProcessing: boolean = false;
	constructor(
		private _CrmService: CrmService,
		private _changeDetectorRef: ChangeDetectorRef,
		private fb: FormBuilder,
		private _hierarchyService: HierarchyService
	) {}

	ngOnInit() {
		this.getAllSponsorshipApprovalsCountWise();
	}

	sponsorshipForm: FormGroup = this.fb.group({
		sponsorshipArray: this.fb.array([]),
	});

	get sponsorshipArray() {
		return this.sponsorshipForm.get("sponsorshipArray") as FormArray;
	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
			this.dataSource.data.forEach((row, index) => {
				this.sponsorshipArray.controls[index].patchValue({
					checked: false,
				});
			});
		} else {
			this.dataSource.data.forEach((row, index) => {
				this.sponsorshipArray.controls[index].patchValue({
					checked: true,
				});
				this.selection.select(row);
			});
		}
	}

	getSelectionValue($event, i: number, row: object) {
		this.selectedValue.push(row);
	}
	getAllSponsorshipApprovalsCountWise() {
		this.showProcessing = true;
		let object = {};
		if (this.rl == 2) {
			object = {
				supervisorId: this.currentUser.id,
				companyId: this.currentUser.companyId,
				type: "lower",
			};
			this._hierarchyService
				.getManagerHierarchy(object)
				.subscribe((res) => {
					this.userData = res;
					let userIds = [];
					this.userData.forEach((user) => {
						userIds.push(user.userId);
						!this._changeDetectorRef["destroyed"]
							? this._changeDetectorRef.detectChanges()
							: null;
					});
					this.getCountTable(userIds);
				});
		} else if (this.rl == 0) {
			let query = {
				where: {
					companyId: this.currentUser.companyId,
				},
			};
			this._CrmService
				.getRequestDataManagerHeirarchy(query)
				.subscribe((res: any) => {
					let userIds = [];
					res.forEach((element) => {
						userIds.push(element.userId);
						!this._changeDetectorRef["destroyed"]
							? this._changeDetectorRef.detectChanges()
							: null;
					});
					this.getCountTable(userIds);
					
				});
				
				
		}
	}
	getCountTable(data) {
		let obj = {
			companyId: this.currentUser.companyId,
			userIds: data,
			status:false
		};		
		this._CrmService
			.getRequestUserwise(obj)
			.subscribe((userWiseRes: any) => {
				if (userWiseRes.length == 0 || userWiseRes == undefined) {
					this.showCountWiseDetails = false;
					this.showProcessing = false;
					this._changeDetectorRef.detectChanges();
					(Swal as any).fire({
						title: "No Records Found",
						type: "info",
					});
					
				} else {
					this.showProcessing = false;

					this.showCountWiseDetails = true;

					userWiseRes.forEach((item, i) => {
						item["index"] = i + 1;
					});
					this.dtOptions = {
						pagingType: "full_numbers",
						ordering: true,
						info: true,
						scrollY: 300,
						scrollX: true,
						scrollCollapse: true,
						paging: false,
						destroy: true,
						data: userWiseRes,
						responsive: true,
						columns: [
							{
								title: "S No.",
								data: "index",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Name",
								data: "_id.name",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "District Name",
								data: "_id.district",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "# Sponsorship Counts",
								data: "requestCount",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return `<a class='getUserWiseReport btn btn-outline-success btn-sm' style="color: #3f51b5; cursor:pointer;font-weight: 600;    line-height: .8 !important;
						  font-size: 1.19rem;">${data}</a>`;
									}
								},
								defaultContent: "---",
							},
						],
						rowCallback: (
							row: Node,
							data: any[] | Object,
							index: number
						) => {
							const self = this;

							$("td", row).unbind("click");
							$("td .getUserWiseReport", row).bind(
								"click",
								() => {
									let rowObject = JSON.parse(
										JSON.stringify(data)
									);
									this.providerInfo = rowObject._id;
									self.getUserWise(rowObject._id.userId);
									!this._changeDetectorRef["destroyed"]
										? this._changeDetectorRef.detectChanges()
										: null;
								}
							);
							return row;
						},
						language: {
							search: "_INPUT_",
							searchPlaceholder: "Search Records",
						},
						dom: "Bfrtip",
						buttons: [
							{
								extend: "excel",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "csv",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "print",
								exportOptions: {
									columns: ":visible",
								},
							},
						],
					};
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
					this.dataTable = $(this.table.nativeElement);
					this.dataTable.DataTable(this.dtOptions);
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				}
			});
	}
	getMonthName(number) {
		return moment(`${number}`, "MM").format("MMMM");
	}
	getUserWise(userId) {
		this.showProcessing = true;
		this.showSponsorshipDetails = false;
		this.dataSource = null;
		this.sponsorshipForm = this.fb.group({
			sponsorshipArray: this.fb.array([]),
		});
		let query = {
			where: {
				userId: userId,
			},
			include: [
				{
					relation: "managerInfo",
					scope: {
						fields: {
							name: true,
							designation: true,
							designationLevel: true,
						},
					},
				},
				{
					relation: "providerInfo",
				},
			],
		};
		this._CrmService
			.getRequestDataManagerHeirarchy(query)
			.subscribe((sponsorshipRes: any) => {
				if (sponsorshipRes.length == 0) {
					(Swal as any).fire({
						title: `No Sponsorship requests found !!`,
						type: "info",
					});
				} else {
					sponsorshipRes.forEach((element, i) => {
						element["index"] = i;
					});
			

					if(this.currentUser.company.validation.hasOwnProperty("CRMModuleProposalWise")){
						if(this.currentUser.company.validation.hasOwnProperty("CRMModuleProposalWise")==true){
							if (this.rl == 2) {
								this.displayedColumns = [
									"select",
									"sn",
									"providerName",
									"category",
									"specialization",
									"qualification",
									"codeOfActivity",
									"typeOfActivity",
									"dateOfProposal",
									"dateOfActivity",
									"noOfDoctorsInTheActivity",
									"expenseAmount",
									"month",
									"year",
									"approvalStatusByMgr",
									"approvalStatusByAdmin",
								];
							} else if (this.rl == 0) {
								this.displayedColumns = [
									"select",
									"sn",
									"providerName",
									"category",
									"specialization",
									"qualification",
									"typeOfActivity",
									"enteredActivity",
									"codeOfActivity",
									"dateOfProposal",
									"dateOfActivity",
									"noOfDoctorsInTheActivity",
									"expenseAmount",
									"month",
									"year",
									"appAndDisAppByMgr",
									"appAndDisAppMgrDate",
									"approvalStatusByMgr",
									"approvalStatusByAdmin",
									"remarks",
								];
							}
						}
                         
						  
					}else{
						if (this.rl == 2) {
							this.displayedColumns = [
								"select",
								"sn",
								"providerName",
								"specialization",
								"degree",
								"investmentType",
								"investmentAmount",
								"expectedBusiness",
								// "lastYearInvestment",
								"month",
								"year",
								"investmentMonth",
								"monthOfNextInvestment",
								"dateOfProposal",
								"approvalStatusByMgr",
								"approvalStatusByAdmin",
								// "status",
							];
						} else if (this.rl == 0) {
							this.displayedColumns = [
								"select",
								"sn",
								"providerName",
								"appAndDisAppByMgr",
								"dateOfProposal",
								"appAndDisAppMgrDate",
								"kindOfCRM",
								"modeOfPayment",
								"specialization",
								"degree",
								"investmentType",
								"investmentAmount",
								"expectedBusiness",
								// "lastYearInvestment",
								"month",
								"year",
								"investmentMonth",
								"monthOfNextInvestment",
								"paidTo",
								"billNum",
								"approvalStatusByMgr",
								"approvalStatusByAdmin",
								// "status",
								"remarks",
							];
						}
					}
				
					this.showSponsorshipDetails = true;
					let array = sponsorshipRes.map((item, i) => {
						this.sponsorshipArray.push(
							this.fb.group({
								id: new FormControl(item.id),
								activity: new FormControl(
									item.activity
								),
								investmentType: new FormControl(
									item.investmentType
								),
								dateOfActivity: new FormControl(
									item.dateOfActivity
								),
								expense: new FormControl(
									item.expense
								),
								noOfDoctors: new FormControl(
									item.noOfDoctors
								),
								codeOfActivity: new FormControl(
									item.uniqueCode
								),
								investmentAmount: new FormControl(
									item.investmentAmount
								),
								expectedBusiness: new FormControl(
									item.expectedBusiness
								),
								lastYearInvestment: new FormControl(
									item.lastYearInvestment
								),
								month: new FormControl(item.month),
								year: new FormControl(item.year),
								investmentMonth: new FormControl(
									item.investmentMonth
								),
								monthOfNextInvestment: new FormControl(
									item.monthOfNextInvestment
								),
								providerName: new FormControl(
									item.providerInfo?item.providerInfo.providerName:''
								),
								specialization: new FormControl(
									item.providerInfo?item.providerInfo.specialization:''
								),
								// providerCode: new FormControl(item.providerCode),
								degree: new FormControl(item.providerInfo?item.providerInfo.degree:''),
								// crmStatus: new FormControl(item.crmStatus),
								approvalStatusByAdmin: new FormControl(
									item.approvalStatusByAdmin
								),
								approvalStatusByMgr: new FormControl(
									item.approvalStatusByMgr
								),
								status: new FormControl(item.status),
								appAndDisAppByMgr: new FormControl(""),
								kindOfCRM: new FormControl(""),
								modeOfPayment: new FormControl(""),
								checked: new FormControl(false),
								index: new FormControl(item.index),
								remarks: new FormControl(""),
								paidTo: new FormControl(""),
								billNum: new FormControl(""),
							})
						);
						return { ...item };
					});
					this.showProcessing = false;
					let resultData=[]
					array.forEach(element => {
						if(element.approvalStatusByAdmin === 'pending' && element.approvalStatusByMgr === 'approved'){
							resultData.push(element)
						}
					});

					this.dataSource = new MatTableDataSource(resultData);
					console.log('Gourav',this.dataSource);
					
					setTimeout(() => {
						this.dataSource.paginator = this.paginator;
					}, 100);
					setTimeout(() => {
						this.dataSource.sort = this.sort;
					}, 100);
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				}
			});
	}
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	approveSponsorshipRequest() {
		this.selection.clear();
		let issueExist = false;
		let sponsorshipArrayData = this.sponsorshipForm.value.sponsorshipArray
			.filter((i) => i.checked)
			.map((i) => {
				delete i.checked;
				return i;
			});

		let selectedProviders = [];
		sponsorshipArrayData.forEach((data) => {
			
			if (this.rl == 0) {				
				if (!this.currentUser.company.validation.hasOwnProperty("CRMModuleProposalWise") && data.kindOfCRM == "") {
					console.log("in if");
					issueExist = true;
					(Swal as any).fire({
						title: `Please select Kind of CRM for &nbsp; <b> ${data.providerName} </b>`,
						type: "error",
					});
				} else if (!this.currentUser.company.validation.hasOwnProperty("CRMModuleProposalWise") && data.modeOfPayment == "") {
					issueExist = true;
					(Swal as any).fire({
						title: `Please select Mode Of Payment for &nbsp; <b> ${data.providerName} </b>`,
						type: "error",
					});
				} else if (!this.currentUser.company.validation.hasOwnProperty("CRMModuleProposalWise") && data.paidTo == "") {
					issueExist = true;
					(Swal as any).fire({
						title: `Please select Paid To for &nbsp; <b> ${data.providerName} </b>`,
						type: "error",
					});
				} else if (!this.currentUser.company.validation.hasOwnProperty("CRMModuleProposalWise") && data.billNum == "") {
					issueExist = true;
					(Swal as any).fire({
						title: `Please select Bill No. for &nbsp; <b> ${data.providerName} </b>`,
						type: "error",
					});
				} else if (!this.currentUser.company.validation.hasOwnProperty("CRMModuleProposalWise") && data.remarks == "") {
					issueExist = true;
					(Swal as any).fire({
						title: `Please select Remarks for &nbsp; <b> ${data.providerName} </b>`,
						type: "error",
					});
				} else {					
					selectedProviders.push(data.id);
				}
			} else {
				selectedProviders.push(data.id);
			}
		});
		if (selectedProviders.length == 0 && issueExist) {
			(Swal as any).fire({
				title: "Please select the providers to Approve",
				type: "error",
			});
		} else {
			let query = {
				requestIds: [],
				setData: {},
				data: [],
				appAndDisAppBy: "",
			};
			if (this.rl == 0) {
				(query.requestIds = selectedProviders),
					(query.setData = {
						appByAdm: this.currentUser.id,
						approvalStatusByAdmin: "approved",
						appAdmDate: new Date(),
						status:true
					}),
					(query.data = sponsorshipArrayData),
					(query.appAndDisAppBy = "admin");
			} else if (this.rl == 2) {
				(query.requestIds = selectedProviders),
					(query.appAndDisAppBy = "manager"),
					(query.setData = {
						appAndDisAppByMgrLevel: this.currentUser
							.designationLevel,
							status:true,
						appAndDisAppByMgr: this.currentUser.id,
						approvalStatusByMgr: "approved",
						appAndDisAppMgrDate: new Date(),
					});
			}

			this._CrmService
				.getApproveAndDisapproveCrmRequest(query)
				.subscribe((approvalRes: any) => {
					this.getAllSponsorshipApprovalsCountWise();
					this.showSponsorshipDetails = false;
					(Swal as any).fire({
						title: `Sponsorship request has been Approved !!`,
						type: "success",
					});
				});
		}
	}

	denySponsorshipRequest() {
		let sponsorshipArrayData = this.sponsorshipForm.value.sponsorshipArray
			.filter((i) => i.checked)
			.map((i) => {
				delete i.checked;
				return i;
			});

		let selectedProviders = [];
		sponsorshipArrayData.forEach((data) => {
			selectedProviders.push(data.id);
		});
		if (selectedProviders.length == 0) {
			(Swal as any).fire({
				title:
					"Please select the providers for Denying the Sponsorship",
				type: "error",
			});
		} else {
			(Swal as any)
				.fire({
					title:
						"Please enter a reason for the denial of Sponsorship",
					type: "info",
					input: "textarea",
					inputPlaceholder: "Type here...",
					allowOutsideClick: false,
					allowEscapeKey: false,
					showCancelButton: true,
					confirmButtonColor: "#16a184",
					cancelButtonColor: "#f4516c",
					confirmButtonText: "Submit",
				})
				.then((res) => {
					if (res.value) {
						let query = {
							requestIds: [],
							setData: {},
							appAndDisAppBy: "",
						};
						if (this.rl == 0) {
							(query.requestIds = selectedProviders),
								(query.setData = {
									approvalStatusByAdmin: "denied",
									appAndDisAppMgrDate: new Date(),
									remarksAdmin: res.value,
								}),
								(query.appAndDisAppBy = "adminDeny");
						} else if (this.rl == 2) {
							(query.requestIds = selectedProviders),
								(query.setData = {
									appAndDisAppByMgrLevel: this.currentUser
										.designationLevel,
									appAndDisAppByMgr: this.currentUser.id,
									approvalStatusByMgr: "denied",
									appAndDisAppMgrDate: new Date(),
									remarksMgr: res.value,
								}),
								(query.appAndDisAppBy = "manager");
						}
						this._CrmService
							.getApproveAndDisapproveCrmRequest(query)
							.subscribe((deniedRes: any) => {
								this.showSponsorshipDetails = false;
								this.getAllSponsorshipApprovalsCountWise();
								!this._changeDetectorRef["destroyed"]
									? this._changeDetectorRef.detectChanges()
									: null;
								(Swal as any).fire({
									title: `Sponsorship request has been Denied !!`,
									type: "success",
								});
							});
					}
				});
		}
	}
}
