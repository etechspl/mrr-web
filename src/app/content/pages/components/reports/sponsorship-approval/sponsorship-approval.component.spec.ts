import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SponsorshipApprovalComponent } from './sponsorship-approval.component';

describe('SponsorshipApprovalComponent', () => {
  let component: SponsorshipApprovalComponent;
  let fixture: ComponentFixture<SponsorshipApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SponsorshipApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SponsorshipApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
