import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvideLocationOnOffSystemComponent } from './provide-location-on-off-system.component';

describe('ProvideLocationOnOffSystemComponent', () => {
  let component: ProvideLocationOnOffSystemComponent;
  let fixture: ComponentFixture<ProvideLocationOnOffSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvideLocationOnOffSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvideLocationOnOffSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
