import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import * as _moment from 'moment';

import Swal from "sweetalert2";
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StatusComponent } from '../../filters/status/status.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { StateComponent } from '../../filters/state/state.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { SelectionModel } from '@angular/cdk/collections';
declare var $;

@Component({
  selector: 'm-provide-location-on-off-system',
  templateUrl: './provide-location-on-off-system.component.html',
  styleUrls: ['./provide-location-on-off-system.component.scss']
})
export class ProvideLocationOnOffSystemComponent implements OnInit {
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;
  public showDetails:boolean=false;
  logoURL: any;
  dataSource2;
  selection = new SelectionModel(true, []);
  displayedColumns = ['select','stateName', 'districtName', 'userName','giveAccess'];


  count = 0;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("setStatus") setStatus: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @ViewChild('dataTable') table;
  showTotalMasterData: boolean = false;

  constructor(private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private exportAsService: ExportAsService,
    private _hierarchyService: HierarchyService,
    private userInfo:UserDetailsService,
    private _urlService: URLService, private providerService:ProviderService) { } 
    isShowDivision = false;
    currentUser = JSON.parse(sessionStorage.currentUser);
    colspanValue;
    isDivisionExist = this.currentUser.company.isDivisionExist;
    designationLevel = this.currentUser.userInfo[0].designationLevel;
    companyId = this.currentUser.companyId;
    dataTable: any;
    dtOptions: any;
  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.locationForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);



    } else {
      this.isShowDivision = false;
    }
  }
  check: boolean = false;

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  locationForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    status: new FormControl([true]),
    stateInfo: new FormControl(null),
    employeeId: new FormControl(null),
    designation: new FormControl(null),

  })
  getReportType(val) { 
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }

   // final approving and deleting area
   changeStatus(obj) {
     let filter=[]
     obj.forEach(item => {
       filter.push({
         companyId:item.companyId,
         userId:item.id,
         isUpdateGeoLoc:item.isUpdateGeoLoc,
         stateId:item.stateId,
         districtId:item.districtId
       })
     });
     this.userInfo.updateStatus(filter).subscribe(res=>{

     })
     
  
  }

  getTypeValue(val) {
    if (val == "State") {
      this.showDistrict = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.locationForm.removeControl("employeeId");
      this.locationForm.removeControl("designation");
      this.locationForm.setControl("stateInfo", new FormControl('', Validators.required))
    } else if (val == "Headquarter") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.locationForm.removeControl("employeeId");
      this.locationForm.removeControl("designation");
      this.locationForm.setControl("stateInfo", new FormControl('', Validators.required))
      this.locationForm.setControl("districtInfo", new FormControl('', Validators.required))
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.locationForm.removeControl("stateInfo");
      this.locationForm.removeControl("districtInfo");
      this.locationForm.setControl("designation", new FormControl('', Validators.required))
      this.locationForm.setControl("employeeId", new FormControl('', Validators.required))
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.locationForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }
  getStatusValue(val){}
  getStateValue(val) {
    this.locationForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.locationForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.locationForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.locationForm.value.divisionId
        })

        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.locationForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.locationForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.locationForm.patchValue({ employeeId: val });
  }
  getDesignation(val) {
    this.locationForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.locationForm.value.divisionId,
        status: [true],
        designationObject: this.locationForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  //setting division into locationForm
  getDivisionValue($event) {
    this.locationForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
     if (this.locationForm.value.type === "State" || this.locationForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.locationForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
     }

      if (this.locationForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.locationForm.value.divisionId != null && this.locationForm.value.designation != null) {
          if (this.locationForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.locationForm.value.divisionId,
              status: [true],
              designationObject: this.locationForm.value.designation
            })
          }
        }
      }
    }
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource2.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource2.forEach(element => {
        this.selection.select(element)
      });
  }
  viewRecords(){
    this.isToggled = false;
    this.showReport = false;
    this.showProcessing = true;
    this.showDetails=true;
    if (this.locationForm.value.employeeId == null) {
      this.locationForm.value.employeeId = [this.currentUser.id];
    }
    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.locationForm.value.divisionId;
    } else {
      params["division"] = [];
    }

    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.locationForm.value.stateInfo;
    params["districtIds"] = this.locationForm.value.districtInfo;
    params["type"] = this.locationForm.value.type;
    params["employeeId"] = this.locationForm.value.employeeId;
    params["status"] = this.locationForm.value.status;
    this.userInfo.getUsers(params).subscribe(res=>{
      if (res.length == 0 || res == undefined) {
        this.showProcessing = false;
        this.showTotalMasterData = false;
        (Swal as any).fire({
          title: "No Records Found",
          type: "info",
        });
      } else {
        this._changeDetectorRef.detectChanges();
        this.dataSource2 = res;
        this.showProcessing = false;
        this.showTotalMasterData = true;
        this._changeDetectorRef.detectChanges();

        res.forEach((item, i) => {
          item["index"] = i + 1;
        });

      }
    
    })
    
  }

}
