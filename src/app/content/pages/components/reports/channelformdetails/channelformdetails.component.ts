import { toDate } from "@angular/common/src/i18n/format_date";
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
	MatSlideToggle,
	MatSlideToggleChange,
	MatTableDataSource,
} from "@angular/material";
import { ChannelFormService } from "../../../../../core/services/ChannelForm/channel-form.service";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { TypeComponent } from "../../filters/type/type.component";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { ExportAsConfig, ExportAsService } from "ngx-export-as";
import * as XLSX from "xlsx";


@Component({
	selector: "m-channelformdetails",
	templateUrl: "./channelformdetails.component.html",
	styleUrls: ["./channelformdetails.component.scss"],
})
export class ChannelformdetailsComponent implements OnInit {
	isToggle: boolean;
	showEmployee: boolean;
	showformReport: boolean;
	showState: boolean;
	showDistrict: boolean;
	showProcessing: boolean = false;

	showFilter = false;

	dataSource: MatTableDataSource<any>;
	displayedColumns = [
		"SNo",
		"NameofEmployee",
		"NameofProspective",
		"CCLocationResponse",
		"SdAmount",
		"RTGSNo",
		"Feedback",
		"Submitted Date"
	];

	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId2", // the id of html/table element

		
	};
	@ViewChild('table1') table1: ElementRef;
	@ViewChild("callTypeComponent")
	callTypeComponent: TypeComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	created_at: any;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	arr: Array<any>;

	constructor(
		private _channelformservice: ChannelFormService,
		private toastr: ToastrService,
		private _cdr: ChangeDetectorRef,
		private exportAsService: ExportAsService,

	){}


	ChannelFormGroup = new FormGroup({
		designation: new FormControl(null, Validators.required),
		employee: new FormControl(null),
		toDate: new FormControl(null),
		fromDate: new FormControl(null),
	});

	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	companyId = this.currentUser.companyId;

	ngOnInit() {
		if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			this.showFilter = true;
		}
	}

	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}

	getDesignation(val) {
		this.ChannelFormGroup.patchValue({ designation: val });
		let passingObj = {
			designationLevel: val[0].designationLevel,
			companyId: this.companyId,
			status: true,
		};
		if (this.currentUser.company.isDivisionExist === true) {
			passingObj["divisionId"] = this.ChannelFormGroup.value.division;
		}
		this.callEmployeeComponent.getEmployeeBasedOnDesignation(passingObj);
	}
	getEmployeeValue(val) {
		this.ChannelFormGroup.patchValue({ employee: val });
	}
	exportToExcel() {
		const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table1.nativeElement);
		const wb: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
		XLSX.writeFile(wb, 'Channel Form.xlsx');
	  }
	

	getFromDate(val) {
		this.ChannelFormGroup.patchValue({ fromDate: val });
	}

	getToDate(val) {
		this.ChannelFormGroup.patchValue({ toDate: val });
	}
	viewRecords() {
		console.log(this.ChannelFormGroup.value);
		// console.log(new Date(this.ChannelFormGroup.value.fromDate).getTime())
		// console.log(new Date(this.ChannelFormGroup.value.toDate).getTime())
    this.showformReport=false;
    this.showProcessing=true;
		const obj = {
			where: {
				companyId: this.companyId,
				userId: { inq: this.ChannelFormGroup.value.employee },
				createdAt: {
					gte: new Date(
						this.ChannelFormGroup.value.fromDate
					).setHours(0, 0, 0, 0),
                    lte: new Date(this.ChannelFormGroup.value.toDate).setHours(
						23,
						59,
						59,
						0
					),
				},
			},include:"user"
		};
		// console.log(obj);
		this._channelformservice.get(obj).subscribe((res) => {

// console.log(res);
			if (res.length == 0 || res == undefined) {
				this.showProcessing =false;
				this.showformReport = false;
        this._cdr.detectChanges();

				this.toastr.info("No Record Found.");
			
			} else {
				this.showProcessing = false;
				this.showformReport = true;
				this.dataSource = new MatTableDataSource(res);
				this.dataSource.paginator = this.paginator;
				this._cdr.detectChanges();
			}
		});
	}
}
