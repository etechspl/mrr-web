import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelformdetailsComponent } from './channelformdetails.component';

describe('ChannelformdetailsComponent', () => {
  let component: ChannelformdetailsComponent;
  let fixture: ComponentFixture<ChannelformdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelformdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelformdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
