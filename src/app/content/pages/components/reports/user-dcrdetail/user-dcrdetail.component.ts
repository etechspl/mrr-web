import { Router } from '@angular/router';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'm-user-dcrdetail',
  templateUrl: './user-dcrdetail.component.html',
  styleUrls: ['./user-dcrdetail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserDCRDetailComponent implements OnInit {
  displayedColumns = ['dcrDate','workingStatus','visitedBlock', 'jointWork', 'RMPCount','DrugStoreCount','remarks','dcrId'];
  displayColumns = ['from','to'];
  displayColumn=['Name'];
  dataSource:any=[];
 // persons: Person[] = [];
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(
    private _dcrReportService :DcrReportsService,
    private router: Router,
    private _changeDetectorRef:ChangeDetectorRef
  
  ) { }
  ngOnInit() {
 
  }
  getEmployeeDCRDetail(userId,fromDate,toDate){
   
    this._dcrReportService.getUserDateWiseDCRDetail(userId,fromDate,toDate).subscribe(res => {
      this._changeDetectorRef.detectChanges();
      console.log(res);
       this.dataSource = res;

    }, err => {
      console.log(err)
      alert("Oooops! \nNo Record Found");
    });
   this._changeDetectorRef.detectChanges();

  }
  getDCRInfo(val){
    this.router.navigate(['/reports/usercompletedcr'],{ queryParams: { dcrId: val } });
  }
 

}
