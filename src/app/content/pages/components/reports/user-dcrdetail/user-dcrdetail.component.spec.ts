import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDCRDetailComponent } from './user-dcrdetail.component';

describe('UserDCRDetailComponent', () => {
  let component: UserDCRDetailComponent;
  let fixture: ComponentFixture<UserDCRDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDCRDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDCRDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
