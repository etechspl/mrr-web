import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftIssueDetailReportGiftWiseComponent } from './gift-issue-detail-report-gift-wise.component';

describe('GiftIssueDetailReportGiftWiseComponent', () => {
  let component: GiftIssueDetailReportGiftWiseComponent;
  let fixture: ComponentFixture<GiftIssueDetailReportGiftWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftIssueDetailReportGiftWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftIssueDetailReportGiftWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
