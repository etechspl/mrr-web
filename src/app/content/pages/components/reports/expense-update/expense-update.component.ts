import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTable, MatSlideToggleChange } from '@angular/material';
import { Designation } from '../../../_core/models/designation.model';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from "../../filters/state/state.component";
import { state } from '@angular/animations';

declare var $;
@Component({
  selector: 'm-expense-update',
  templateUrl: './expense-update.component.html',
  styleUrls: ['./expense-update.component.scss']
})
export class ExpenseUpdateComponent implements OnInit {
  public showState: boolean = true;
  expenseUpdateForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(
    private _toastrService: ToastrService,
    private _fb: FormBuilder,
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toasterService: ToastrService,

  ) {
    this.createForm(this._fb);
  }
  isDivisionExist = this.currentUser.company.isDivisionExist;

  check: boolean = false;
  isStatus: boolean = true;
  isMultipleEmp: boolean = false;
  data: any;
  isShowReport: boolean = false;
  //, 'workedWith', 'district', 'area', 'visits'
  displayedColumns = ['sn', 'district', 'name', 'designation', 'workingDCR', 'otherDCR', 'pendingDCR', 'incompleteDCR', 'tpSubmitted', 'tpApproved', 'totalDoctor', 'totalDoctorTimePeriod', 'totalChemist', 'drCall', 'drCallAvg', 'listedDoctorCall', 'listedDoctorCallAvg', 'listedDoctorCoverage', 'listedActiveDoctorCoverage', 'listedVendorCall', 'listedVendorCallAvg', 'listedVendorCoverage', 'untouchedProvider', 'untouchedProvider1'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;

  @ViewChild('dataTable') table1;
  dataTable: any;
  dtOptions: any;
  dtTrigger: any;
  showDivisionFilter: boolean = false;

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    console.log('toggle', event.checked);
    this.isToggled = event.checked;
  }
  ngOnInit() {

    if (this.currentUser.company.isDivisionExist == true) {

      this.expenseUpdateForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel,
        state:this.expenseUpdateForm.value.stateInfo
      }
      console.log(obj,"=------------------------------------------")
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  createForm(_fb: FormBuilder) {

    this.expenseUpdateForm = _fb.group({
      stateInfo: new FormControl(null),
      designation: new FormControl('', Validators.required),
      status: new FormControl(true),
      userId: new FormControl('', Validators.required),
      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })

  }
  getDivisionValue(val) {
    this.expenseUpdateForm.patchValue({ division: val });
    console.log("division",this.expenseUpdateForm.patchValue({ division: val }))
    //=======================Clearing Filter===================
    this.designationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    this._changeDetectorRef.detectChanges();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.designationComponent.getDesignationBasedOnDivision(passingObj);
  }

  getDesignationList() {
    //this.designationComponent.getManagersDesignation();
  }

  //   getDesignationLevel(designation: Designation) {
  //     this.expenseUpdateForm.patchValue({ designation: designation.designationLevel })
  //   }

  getDesignation(val) {
    this.expenseUpdateForm.patchValue({ designation: val.designationLevel });

    if (this.currentUser.userInfo[0].rL === 0) {
      console.log("-0")

      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
           stateIds:this.expenseUpdateForm.value.stateInfo,
          division: this.expenseUpdateForm.value.division,
        }
        console.log("-1",passingObj)

        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        console.log("-2")

        this.callEmployeeComponent.getEmployeeListBasedOnState(this.currentUser.companyId, [val], [true],this.expenseUpdateForm.value.stateInfo);
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          stateInfo:this.expenseUpdateForm.value.state,
          designation: [val.designationLevel] //desig
        };

        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.expenseUpdateForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

    }
    this.callEmployeeComponent.setBlank();

  }


  getStatusValue(val) {
    this.expenseUpdateForm.patchValue({ status: val });
  }

  getEmployee(emp: string) {
    this.expenseUpdateForm.patchValue({ userId: emp })
    console.log()
  }

  getFromDate(month: object) {
    this.expenseUpdateForm.patchValue({ month: month })
  }

  getToDate(year: number) {
    this.expenseUpdateForm.patchValue({ year: year })
  }

  generateReport() {
    this.isToggled = false;
    this.isShowReport = false;
    if (!this.expenseUpdateForm.valid) {
      this._toastrService.error("Select the required field to generate the report", "All Field required");
      return;
    }
    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.expenseUpdateForm.value.division;
    }
    let params = {
      companyId: this.currentUser.companyId,
      status: this.expenseUpdateForm.value.status,
      userId: [this.expenseUpdateForm.value.userId],
      designation: this.expenseUpdateForm.value.designation,
      month: this.expenseUpdateForm.value.month,
      year: this.expenseUpdateForm.value.year,
    }
    this._dcrReportsService.expenseUpdated(params).subscribe(result => {

      if (result) {
        this.toasterService.success(result);
        this.isToggled = true;
      } else {
        this.toasterService.error("There is some problem in expense updation");
        this.isToggled = true;
      }

    })
  }
  intializeForm() {
    this.expenseUpdateForm.setValue({
      type: '',
      designation: '',
      status: '',
      userId: '',
      fromDate: '',
      toDate: ''
    })
  }


  getStateValue(val) {
    console.log(val)
    this.expenseUpdateForm.patchValue({ stateInfo : val });
    this.callEmployeeComponent.setBlank();

  }

}
