import { Router } from "@angular/router";
import { UserCompleteDCRInfoComponent } from "./../user-complete-dcrinfo/user-complete-dcrinfo.component";
import { TypeComponent } from "../../filters/type/type.component";
import { StateComponent } from "../../filters/state/state.component";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";
import { MonthComponent } from "../../filters/month/month.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { DistrictComponent } from "../../filters/district/district.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { forkJoin } from 'rxjs';
import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  ElementRef,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { DivisionComponent } from "../../filters/division/division.component";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import * as _moment from "moment";
declare var $;
import Swal from "sweetalert2";
import { DCRBackupService } from "../../../../../core/services/DCRBackup/dcrbackup.service";
import { DCRProviderVisitDetailsBackupService } from "../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service";
import { StatusComponent } from "../../filters/status/status.component";
import { HierarchyService } from "../../../../../core/services/hierarchy-service/hierarchy.service";
import { ActivatedRoute } from "@angular/router";
import { URLService } from "../../../../../core/services/URL/url.service";
import { MatOption, MatSelect, MatSlideToggleChange } from "@angular/material";
import { FromdateComponent } from "../../filters/fromdate/fromdate.component";
import { ExpenseClaimedService } from "../../../../../core/services/expense-claimed.service";
import { ERPServiceService } from "../../../../../core/services/erpservice.service";

@Component({
  selector: 'm-primary-sale-state-hq-mgr-wise',
  templateUrl: './primary-sale-state-hq-mgr-wise.component.html',
  styleUrls: ['./primary-sale-state-hq-mgr-wise.component.scss']
})
export class PrimarySaleStateHqMgrWiseComponent implements OnInit {
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("dataTable") table;
  @ViewChild("dataTableProdPrimary") tableForProdPrimary;
  @ViewChild("dataTableHq") tableHq;
  @ViewChild("dataTableParty") tableParty;
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showProcessing: boolean = false;
  public showProcessingHq: boolean = false;
  public showProcessingParty: boolean = false;
  public productWiseReportShowMgrState: boolean = false;
  public stockistWiseReportShowMgrState: boolean = false;
  public productWiseReportShowMgrHq: boolean = false;
  public stockistWiseReportShowMgrHq: boolean = false;
  public productWiseReportShowMgrParty: boolean = false;
  public stockistWiseReportShowMgrParty: boolean = false;
  public productWiseReportShowMgeWise: boolean = false;
  public stockistWiseReportShowMgr: boolean = false;
  public productWiseReportShowAdminState: boolean = false;
  public stockistWiseReportShowAdminState: boolean = false;
  public productWiseReportShowAdminHq: boolean = false;
  public stockistWiseReportShowAdminHq: boolean = false;
  public productWiseReportShowAdminParty: boolean = false;
  public stockistWiseReportShowAdminParty: boolean = false;

  public productWiseReportShowProductAdminMgeWise: boolean = false;
  public productWiseReportShowStateAmountMgr: boolean = false;
  public productWiseReportShowAdminStateAmount: boolean = false;
  //public productWiseReportShowMgrStateAmount: boolean = false;
  

  
  logoURL: any;
  count = 0;
  exportAsConfig: ExportAsConfig = {
    type: "xlsx", // the type you want to download
    elementId: "tableId1", // the id of html/table element
  };
  //dcrForm: FormGroup;
  isDataSources1Empty = false;
  statuscheck = 1;
  managerTeam;
  onlyManager;
  showAdminAndMGRLevelFilter = false;
  isShowHqClick = false;
  isShowPartyClick = false;
  
  dataTable: any;
  dataTableProdPrimary: any;
  dataTableHq: any;
  dataTableParty: any;
  dtOptions: any;
  dtOptionsProdPrimary: any;
  dtOptionsHq: any;
  dtOptionsParty: any;
  //dataTableForDCRDateWise: any;
  dtOptionsForDCRDateWise: any;
  stateData: any;
  districtData: any;
  desigData: any;
  managerData: any;
  multiple;
  multipleTrue;
  saleTypeArr: any = [{ value: "Primary" }, { value: "Collection" }, { value: "Outstanding" }, { value: "SalesReturn" }];
  wiseType : any = [{ value: "Amount Wise" }, { value: "Product Wise" }];
  productwise:any;
  stockistwise:any;

  // select all States
  allSelected=false;
  allSelectedHQ=false;
  @ViewChild('select') select: MatSelect;
  @ViewChild('selectHQ') selectHQ: MatSelect;

  constructor(
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private _dcrReportsServiceBackup: DCRBackupService,
    private DCRProviderVisitDetailsBackupService: DCRProviderVisitDetailsBackupService,
    private exportAsService: ExportAsService,
    private _hierarchyService: HierarchyService,
    private activateRoute: ActivatedRoute,
    private _urlService: URLService,
    private _expenseClaimedService: ExpenseClaimedService,
    private ERPServiceService: ERPServiceService,
  ) { }

  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  otherTypeLablesExist = this.currentUser.company.lables.otherType
    ? true
    : false;
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  dcrForm = new FormGroup({
    reportType: new FormControl("Geographical"),
    type: new FormControl(null, Validators.required),
    status: new FormControl([true]),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    month: new FormControl(null),
    year: new FormControl(null),
    designation: new FormControl(null),
    saleType: new FormControl(null, Validators.required),
    wiseType: new FormControl(null, Validators.required),
    toDate: new FormControl(null, Validators.required),
    fromDate: new FormControl(null, Validators.required),
  });
  check: boolean = false;
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  getSaleArray(val){    
    if(val=="Product Wise"){
      this.multipleTrue=false;
      this.multiple=true;
    }else{
      this.multipleTrue=true;
      this.multiple=false;
    }

  }
  getReportType(val) {
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Geographical",
        "WithStateHeadquarterParty"
      );
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Hierarachy",
        "managerwise"
      );
    }
  }
  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    
    if (this.allSelected) {
      this.select.options.forEach( (item : MatOption) => item.select());
    } else {
      this.select.options.forEach( (item : MatOption) => {item.deselect()});
    }

  }

  toggleAllSelectionForHQ() {
    this.allSelectedHQ = !this.allSelectedHQ;  // to control select-unselect
    
    if (this.allSelectedHQ) {
      this.selectHQ.options.forEach( (item : MatOption) => item.select());
    } else {
      this.selectHQ.options.forEach( (item : MatOption) => {item.deselect()});
    }

  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State Wise") {
      this.getERPStatesData();
      this.showState = true;
      this.showDistrict = false;
      this.showReport=false;
      this.isShowHqClick=false;
      this.isShowPartyClick=false;
      this.dcrForm.setControl("stateInfo", new FormControl(null, Validators.required));
      this.dcrForm.removeControl("reportType");
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");
      this.dcrForm.removeControl("districtInfo");
    } else if (val == "Headquarter Wise") {
      this.getERPStatesData();
      this.showState = true;
      this.showDistrict = false;
      this.showReport=false;
      this.isShowHqClick=false;
      this.isShowPartyClick=false;
      this.dcrForm.setControl("stateInfo", new FormControl(null, Validators.required));
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");
      this.dcrForm.removeControl("districtInfo");
    } else if (val == "Party Wise") {
      this.showState = true;
      this.showReport=false;
      this.isShowHqClick=false;
      this.isShowPartyClick=false;
      this.getERPStatesData();
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.setControl("stateInfo", new FormControl(null, Validators.required));
      this.dcrForm.setControl("districtInfo", new FormControl(null, Validators.required));
      this.dcrForm.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Manager Wise") {
      this.showEmployee = true; 
      this.showState = false;
      this.showDistrict = false;
      this.getDesignation(val);
      this.dcrForm.removeControl("stateInfo");
      this.dcrForm.removeControl("districtInfo");
      this.dcrForm.setControl("reportType", new FormControl("Hierarachy", Validators.required));
      this.dcrForm.setControl("designation", new FormControl(null, Validators.required));
      this.dcrForm.setControl("employeeId", new FormControl(null, Validators.required));
    } else if (val == "Self") {
      this.showEmployee = false;
    }
    this.isShowDivision = false;
    this.dcrForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();
  }
  getStateValue(val) {
    this.dcrForm.patchValue({ stateInfo: val });
    if (this.dcrForm.value.type == "Party Wise") {
      this.districtData = [];
      this.dcrForm.setControl("districtInfo", new FormControl(null, Validators.required));
      this.getERPHeadquartersData(this.dcrForm.value.stateInfo);
    }
  }
  getDistrictValue(val) {
    this.dcrForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.dcrForm.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.dcrForm.patchValue({ fromDate: val });
  }
  getToDate(val) {
    this.dcrForm.patchValue({ toDate: val });
  }
  getDesignation(val) {
    this.dcrForm.patchValue({ designation: val });
    this.getDesignationsData(val);
  }
  //setting division into dcrform
  ngOnInit() {
    this.isShowDivision = false;
    if (
      this.currentUser.userInfo[0].designationLevel == 0 ||
      this.currentUser.userInfo[0].designationLevel > 1
    ) {
      //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Geographical",
        "WithStateHeadquarterParty"
      );
    }
  }

  getERPStatesData() {
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, }
    this.ERPServiceService.getERPStateData(params).subscribe((stateDataResult) => {
      this.stateData = stateDataResult;
    })
  }

  getERPHeadquartersData(val) {
    let stateCode = (val.filter(x => x)).toString();
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, stateId: stateCode   }
    this.ERPServiceService.getERPHeadquarterData(params).subscribe((districtDataResult) => {
      this.districtData = districtDataResult;
    })
  }

  getDesignationsData(val) {
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint }
    this.ERPServiceService.getDesignationsData(params).subscribe((desigResult) => {
      this.desigData = desigResult;
    })
    this.dcrForm.patchValue({ designation: val });
    this.getManagersData(val);
    
  }

  getManagersData(val) {
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint, Designationid: val }
    this.ERPServiceService.getManagerData(params).subscribe((managerResult) => {
      this.managerData = managerResult;
    })
  }
  test;
  viewRecords() {
    //this.isToggled = true;
    this.showReport = false;
    this.isShowHqClick = false;
    this.isShowPartyClick = false;
    this.showProcessing = true;
    if (this.dcrForm.value.employeeId == null) {
      this.dcrForm.value.employeeId = [this.currentUser.id];
    }
   
    let params = {};
    let type = this.dcrForm.value.type;
    let forkAPIArray = [];
    let forkAPIResponseArray = [];
    let stateCode;
    params = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.employeeCode,
      APIEndPoint: this.currentUser.company.APIEndPoint,
    }
    if (type == "Headquarter Wise") {
      params['stateCode'] = this.dcrForm.value.stateInfo;
    } else if (type == "Party Wise") {
      const headquaterCode = (this.dcrForm.value.districtInfo.filter(x => x)).toString();
      params['headquaterCode'] = headquaterCode;

    }  else if (type == "Manager Wise") {
      params['userId'] = this.dcrForm.value.employeeId; 
    } else if (type == "State Wise") {
      let stateIds = this.dcrForm.value.stateInfo;
      stateCode = (stateIds.filter(x => x)).toString();
        } 

     let stateIds = this.dcrForm.value.stateInfo;
     if(stateIds=="undefined" ||stateIds==undefined){
      stateCode = "";
     }else{
     stateCode = (stateIds.filter(x => x)).toString();
        } 
      
    if(this.dcrForm.value.wiseType === "Amount Wise"){
      let priStatus = this.dcrForm.value.saleType.find(obj => obj == "Primary");
      let collStatus = this.dcrForm.value.saleType.find(obj => obj == "Collection");
      let outStatus = this.dcrForm.value.saleType.find(obj => obj == "Outstanding");
      let returnStatus = this.dcrForm.value.saleType.find(obj => obj == "SalesReturn");
    this.productWiseReportShowAdminState = false;
     
      if (priStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
       
        params['type'] = priStatus;
        params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
        params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
        //if (type == "State Wise" || type == "Manager Wise") {
          if (type == "State Wise" || type == "Manager Wise") {
          forkAPIArray.push(this.ERPServiceService.getStateWiseDetails(params));
        } else if (type == "Headquarter Wise") {
          forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
        } else if (type == "Party Wise") {
          forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
        }
        forkAPIResponseArray.push("primarySaleResult");
      }
      if (collStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
        params['type'] = collStatus;
        params['month'] = _moment(this.dcrForm.value.toDate).format("MM");
        params['year'] = _moment(this.dcrForm.value.toDate).format("YYYY");
        if (type == "State Wise" || type == "Manager Wise") {
          forkAPIArray.push(this.ERPServiceService.getStateWiseDetails(params));
        } else if (type == "Headquarter Wise") {
          forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
        } else if (type == "Party Wise") {
          forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
        }
        forkAPIResponseArray.push("collectionSaleResult");
      }
      
      if (outStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
        params['type'] = outStatus;
        if (type == "State Wise" || type == "Manager Wise") {
          forkAPIArray.push(this.ERPServiceService.getStateWiseDetails(params));
        } else if (type == "Headquarter Wise") {
          forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
        } else if (type == "Party Wise") {
          forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
        }
        forkAPIResponseArray.push("outstandingSaleResult");
      }
      if (returnStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
        params['type'] = returnStatus;
        params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
        params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
        
        if (type == "State Wise" || type == "Manager Wise") {
          forkAPIArray.push(this.ERPServiceService.getStateWiseDetails(params));
        } else if (type == "Headquarter Wise") {
          forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
          forkAPIResponseArray.push("returnSaleResult");
        } else if (type == "Party Wise") {
          forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
        }
      }
      forkJoin(forkAPIArray).subscribe((forkAPIResponseArray) => {
        let final = [];
        let ddoption = [];
        let loopLengthVal = 0;
        let index = 0;
        for (let i = 0; i < forkAPIArray.length; i++) {
          if (forkAPIResponseArray[i]) {
            if (forkAPIResponseArray[i].length > loopLengthVal) {
              loopLengthVal = forkAPIResponseArray[i].length;
              index = i;
            }
          }
        }
  
        if (priStatus) {
          const primData = {
            title: "Primary Sale",
            data: "primary",
            render: function (data) {
              if (data === "") {
                return "---";
              } else {
                return data;
              }
            },
            defaultContent: "---",
          }
          ddoption.push(primData)
        }
        if (collStatus) {
          const collData = {
            title: "Collection Sale",
            data: "collection",
            render: function (data) {
              if (data === "") {
                return "---";
              } else {
                return data;
              }
            },
            defaultContent: "---",
          }
          ddoption.push(collData)
        }
        if (outStatus) {
          const outData = {
            title: "Outstanding Sale",
            data: "outstanding",
            render: function (data) {
              if (data === "") {
                return "---";
              } else {
                return data;
              }
            },
            defaultContent: "---",
          }
          ddoption.push(outData)
        }
        if (returnStatus) {
          const returnData = {
            title: "SalesReturn",
            data: "return",
            render: function (data) {
              if (data === "") {
                return "---";
              } else {
                return data;
              }
            },
            defaultContent: "---",
          }
          ddoption.push(returnData)
        }
        if (type == "State Wise" || type == "Manager Wise") {
          if (forkAPIResponseArray[index]) {
            forkAPIResponseArray[index].forEach(element => {
              let temp = {
                stateName: '',
                stateCode: '',
                primary: 0,
                collection: 0,
                outstanding: 0,
                return: 0
              }
              if (priStatus) {
                if (forkAPIResponseArray[0]) {
                  let primaryFound = forkAPIResponseArray[0].filter(res => {
                    return element.StateCode == res.StateCode
                  });
  
                  if (primaryFound.length > 0) {
                    temp.stateName = primaryFound[0].StateName;
                    temp.stateCode = primaryFound[0].StateCode;
                    temp.primary = primaryFound[0].amount;
                  }
                }
              }
              if (collStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let collFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.StateCode) == (res.StateCode)
                  });
                  if (collFound.length > 0) {
                    temp.stateName = collFound[0].StateName;
                    temp.stateCode = collFound[0].StateCode;
                    temp.collection = collFound[0].AMOUNT;
                  }
                }
              }
              if (outStatus) { 
                let indexNum = 0;
                if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let outFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.StateCode) == (res.StateCode)
                  });
                  if (outFound.length > 0) {
                    temp.stateName = outFound[0].StateName;
                    temp.stateCode = outFound[0].StateCode;
                    temp.outstanding = outFound[0].amount;
                  }
                }
              }
              if (returnStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let returnFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.StateCode) == (res.StateCode)
                  });
                  if (returnFound.length > 0) {
                    temp.stateName = returnFound[0].StateName;
                    temp.stateCode = returnFound[0].StateCode;
                    temp.return = returnFound[0].Amount;
                  }
                }
              }
              final.push(temp);
            });
            final.map((item, index) => {
              item["index"] = index;
              item["stateName"] = item.stateName;
            });
            this.test=final;
            this.dtOptions = {
              pagingType: "full_numbers",
              ordering: true,
              info: true,
              scrollY: 300,
              scrollX: true,
              scrollCollapse: true,
              paging: false,
              destroy: true,
              data: final,
              responsive: true,
              columns: [
                {
                  title: "S. No.",
                  data: "index",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data + 1;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title: "State",
                  data: "stateName",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return "<button id='viewHQSale' class='btn btn-warning btn-sm'><i class='fa fa-check'></i>&nbsp;&nbsp;" + data + "</button>"
                    }
                  },
                  defaultContent: "---",
                },
                ...ddoption
              ],
              rowCallback: (
                row: Node,
                data: any[] | Object,
                index: number
              ) => {
                const self = this;
                $('td', row).unbind('click');
                $('td button', row).bind('click', (event) => {
                  let rowObject = JSON.parse(JSON.stringify(data));
                  if (event.target.id === "viewHQSale") {
                    self.getHqwiseSale(rowObject, 'Headquarter Wise');
                  }
                });
              },
              language: {
                search: "_INPUT_",
                searchPlaceholder: "Search Records",
              },
              dom: "Bfrtip",
              buttons: [
                {
                  extend: 'excel',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'csv',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'copy',
                  exportOptions: {
                    columns: ':visible'
                  }
                }],
            };
            this.showProcessing = false;
            this.showReport = true;
          } else {
            this.showProcessing = false;
            this.showReport = false;
            alert('No records found !!!');
          }
        } else if (type == "Headquarter Wise") {

          this.productWiseReportShowAdminHq = false;
          this.productWiseReportShowMgrState = false;
          if (forkAPIResponseArray[index]) {
            forkAPIResponseArray[index].forEach(element => {
              let temp = {
                stateName: '',
                headquater: '',
                headquarterCode: '',
                primary: 0,
                collection: 0,
                outstanding: 0,
                return: 0
              }
              if (priStatus) {
                if (forkAPIResponseArray[0]) {
                  let primaryFound = forkAPIResponseArray[0].filter(res => {
                    return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HeadquaterCode)
                  });
  
                  if (primaryFound.length > 0) {
                    temp.stateName = primaryFound[0].StateName;
                    temp.headquater = primaryFound[0].Headquater;
                    temp.headquarterCode = primaryFound[0].HeadquaterCode;
                    temp.primary = primaryFound[0].primSales;
                  }
                }
              }
              if (collStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let collFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HQCode)
                  });
                  if (collFound.length > 0) {
                    temp.stateName = collFound[0].StateName;
                    temp.headquater = collFound[0].HQ;
                    temp.headquarterCode = collFound[0].HQCode;
                    temp.collection = collFound[0].AMOUNT;
                  }
                }
              }
              if (outStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let outFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.headquarterCode)
                  });
                  if (outFound.length > 0) {
                    temp.stateName = outFound[0].StateName;
                    temp.headquater = outFound[0].headquarter;
                    temp.headquarterCode = outFound[0].headquarterCode;
                    temp.outstanding = outFound[0].amount;
                  }
                }
              }
              if (returnStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let returnFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HQCode)
                  });
                  if (returnFound.length > 0) {
                    temp.headquater = returnFound[0].HQ;
                    temp.headquarterCode = returnFound[0].HQCode;
                    temp.return = returnFound[0].Amount;
                  }
                }
              }
              final.push(temp);
            });
            final.map((item, index) => {
              item["index"] = index;
              item["stateName"] = item.stateName;
              item["headquater"] = item.headquater;
            });
            this.dtOptions = {
              pagingType: "full_numbers",
              ordering: true,
              info: true,
              scrollY: 300,
              scrollX: true,
              scrollCollapse: true,
              paging: false,
              destroy: true,
              data: final,
              responsive: true,
              columns: [
                {
                  title: "S. No.",
                  data: "index",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data + 1;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title: "State",
                  data: "stateName",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title: "Headquarter",
                  data: "headquater",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return "<button id='viewPartySale' class='btn btn-warning btn-sm'><i class='fa fa-check'></i>&nbsp;&nbsp;" + data + "</button>"
                    }
                  },
                  defaultContent: "---",
                },
                ...ddoption
              ],
              rowCallback: (
                row: Node,
                data: any[] | Object,
                index: number
              ) => {
                const self = this;
                $('td', row).unbind('click');
                $('td button', row).bind('click', (event) => {
                  let rowObject = JSON.parse(JSON.stringify(data));
                  if (event.target.id === "viewPartySale") {
                    self.getHqwiseSale(rowObject, 'Party Wise');
                  }
                });
              },
              language: {
                search: "_INPUT_",
                searchPlaceholder: "Search Records",
              },
              dom: "Bfrtip",
              buttons: [
                {
                  extend: 'excel',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'csv',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'copy',
                  exportOptions: {
                    columns: ':visible'
                  }
                }],
            };
            this.showProcessing = false;
            this.showReport = true;
          } else {
            this.showProcessing = false;
            this.showReport = false;
            alert('No records found !!!');
          }
        } else if (type == "Party Wise") {
          this.productWiseReportShowMgrHq=false;
          this.productWiseReportShowAdminHq = false;
          this.productWiseReportShowAdminStateAmount = false;
          this.productWiseReportShowStateAmountMgr = false;
          if (forkAPIResponseArray[index]) {
            forkAPIResponseArray[index].forEach(element => {
              let temp = {
                headquater: '',
                party: '',
                primary: 0,
                collection: 0,
                outstanding: 0,
                return : 0,
              }
              if (priStatus) {
                if (forkAPIResponseArray[0]) {
                  let primaryFound = forkAPIResponseArray[0].filter(res => {
                    return (element.PartyCode) == (res.PartyCode)
                  });
  
                  if (primaryFound.length > 0) {
                    temp.headquater = primaryFound[0].Headquater;
                    temp.party = primaryFound[0].PartyName;
                    temp.primary = primaryFound[0].primSales;
                  }
                }
              }
              if (collStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let collFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.PartyCode) == (res.PartyCode)
                  });
                  if (collFound.length > 0) {
                    temp.headquater = collFound[0].HQ;
                    temp.party = collFound[0].PartyName;
                    temp.collection = collFound[0].AMOUNT;
                  }
                }
              }
              if (outStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let outFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.PartyCode) == res.PartyCode
                  });
                  if (outFound.length > 0) {
                    temp.headquater = outFound[0].headquarter;
                    temp.party = outFound[0].PartyName;
                    temp.outstanding = outFound[0].amount;
                  }
                }
              } if (returnStatus) {
                let indexNum = 0;
                if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
                if (forkAPIResponseArray[indexNum]) {
                  let returnFound = forkAPIResponseArray[indexNum].filter(res => {
                    return (element.PartyCode) == res.PartyCode
                  });
                  if (returnFound.length > 0) {
                    temp.headquater = returnFound[0].HQ;
                    temp.party = returnFound[0].PartyName;
                    temp.return = returnFound[0].Amount;
                  }
                }
              }
              
              final.push(temp);
            });
            final.map((item, index) => {
              item["index"] = index;
              item["headquater"] = item.headquater;
              item["party"] = item.party;
            });
            this.dtOptions = {
              pagingType: "full_numbers",
              ordering: true,
              info: true,
              scrollY: 300,
              scrollX: true,
              scrollCollapse: true,
              paging: false,
              destroy: true,
              data: final,
              responsive: true,
              columns: [
                {
                  title: "S. No.",
                  data: "index",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data + 1;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title: "Headquater",
                  data: "headquater",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title: "Party",
                  data: "party",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
                ...ddoption
              ],
              rowCallback: (
                row: Node,
                data: any[] | Object,
                index: number
              ) => {
                const self = this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $("td", row).unbind("click");
                $("td .getDateWiseReport", row).bind(
                  "click",
                  () => {
                    //console.log("row : ", data);
                    let rowObject = JSON.parse(
                      JSON.stringify(data)
                    );
                    //self.getDateWise(rowObject.userId);
                    this._changeDetectorRef.detectChanges();
                  }
                );
                $("td button", row).bind("click", () => {
                  //console.log("row : ", data);
                  let rowObject = JSON.parse(
                    JSON.stringify(data)
                  );
                });
                return row;
              },
              language: {
                search: "_INPUT_",
                searchPlaceholder: "Search Records",
              },
              dom: "Bfrtip",
              buttons: [
                {
                  extend: 'excel',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'csv',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'copy',
                  exportOptions: {
                    columns: ':visible'
                  }
                }],
            };
            this.showProcessing = false;
            this.showReport = true;
          } else {
            this.showProcessing = false;
            this.showReport = false;
            alert('No records found !!!');
          }
        }
        this._changeDetectorRef.detectChanges();
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
        this._changeDetectorRef.detectChanges();
      });



//--------------------------Managerwise Amount wise Admin

// if(this.currentUser.userInfo[0].designationLevel==0){
//   if(type === "Manager Wise"){

//   this.showProcessing = false;
//   //this.productWiseReportShowAdminStateAmount = true;
//   this.productWiseReportShowAdminHq = false;
//   this.productWiseReportShowMgrStateAmount=false;
//   this.productWiseReportShowAdminState = false;  
//    this.productWiseReportShowAdminParty = false;
//    this.productWiseReportShowProductAdminMgeWise = false;
//   }
// }else if(this.currentUser.userInfo[0].designationLevel!=0||this.currentUser.userInfo[0].designationLevel!=1){

// if(type === "Manager Wise"){
//   this.showProcessing = false;
//   this.productWiseReportShowMgrState = false;
//   this.productWiseReportShowAdminStateAmount = false;
//   this.productWiseReportShowMgrStateAmount=true;
//   this.productWiseReportShowProductAdminMgeWise = false;
//   this.productWiseReportShowMgeWise = false;
//   this.productWiseReportShowMgrHq=false;
// }
//  // this.productWiseReportShowStateAmountMgr = true;
// }




    }else if(this.dcrForm.value.wiseType === "Product Wise"){
      let collStatus = "";
      let outStatus = "";
      let returnStatus = "";
      let priStatus = "";
      if(this.dcrForm.value.saleType == "Primary"){
         priStatus = "Primary"
      }
      else if(this.dcrForm.value.saleType == "Collection"){
        collStatus = "Collection"
      }
      else if(this.dcrForm.value.saleType == "Outstanding"){
        outStatus = "Outstanding"
      }
      else if(this.dcrForm.value.saleType == "SalesReturn"){
        returnStatus = "SalesReturn"
      }
     
      if (priStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
        params['type'] = priStatus;
        params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
        params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
        if (type == "State Wise" || type == "Manager Wise") {
         this.ERPServiceService.getStateWiseProductDetails(params).subscribe(primSales=>{

          if(primSales.length>0){
            this.productWiseReportShowAdminState=true;
            var index=0;
            primSales.forEach((element,i) => {
              index++;
              element["index"]=index;
              element["creditValue"]=element.qty*element.creditvalue;
            });
            this.dtOptionsProdPrimary = {
              pagingType: "full_numbers",
              ordering: true,
              info: true,
              scrollY: 300,
              scrollX: true,
              scrollCollapse: true,
              paging: false,
              destroy: true,
              data: primSales,
              responsive: true,
              columns: [
                {
                  title: "S No.",
                  data: "index",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title:"State Name",
                  data: "StateName",
                  render: function (data) {
                    
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
                  {
                  title: "Product Name",
                  data: "ProductName",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title: "Qty",
                  data: "qty",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
                {
                  title: "Amount",
                  data: "creditValue",
                  render: function (data) {
                    if (data === "") {
                      return "---";
                    } else {
                      return data;
                    }
                  },
                  defaultContent: "---",
                },
              ],
              language: {
                search: "_INPUT_",
                searchPlaceholder: "Search Records",
              },
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "excel",
                  exportOptions: {
                    columns: ":visible",
                  },
                },
                {
                  extend: "csv",
                  exportOptions: {
                    columns: ":visible",
                  },
                },
                {
                  extend: 'copy',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: "print",
                  exportOptions: {
                    columns: ":visible",
                  },
                },
              ],
            };
            !this._changeDetectorRef["destroyed"]
              ? this._changeDetectorRef.detectChanges()
              : null;
            this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
            this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
            !this._changeDetectorRef["destroyed"]
              ? this._changeDetectorRef.detectChanges()
              : null;  
          }  else{
            this.toastr.error("No Record Found");
          }        
     
         });
        } else if (type == "Headquarter Wise") {
          this.ERPServiceService.getHQWiseProductDetails(params).subscribe(primSales=>{

            if(primSales.length>0){
              this.productWiseReportShowAdminState=true;
              var index=0;
              primSales.forEach((element,i) => {
                index++;
                element["index"]=index;
                element["creditValue"]=element.qty*element.creditvalue;
              });
              this.dtOptionsProdPrimary = {
                pagingType: "full_numbers",
                ordering: true,
                info: true,
                scrollY: 300,
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                destroy: true,
                data: primSales,
                responsive: true,
                columns: [
                  {
                    title: "S No.",
                    data: "index",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title:"State Name",
                    data: "StateName",
                    render: function (data) {
                      
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title:"Headquarter Name",
                    data: "Headquater",
                    render: function (data) {
                      
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                    {
                    title: "Product Name",
                    data: "ProductName",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title: "Qty",
                    data: "qty",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title: "Amount",
                    data: "creditValue",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                ],
                language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Search Records",
                },
                dom: "Bfrtip",
                buttons: [
                  {
                    extend: "excel",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                  {
                    extend: "csv",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                  {
                    extend: 'copy',
                    exportOptions: {
                      columns: ':visible'
                    }
                  },
                  {
                    extend: "print",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                ],
              };
              !this._changeDetectorRef["destroyed"]
                ? this._changeDetectorRef.detectChanges()
                : null;
              this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
              this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
              !this._changeDetectorRef["destroyed"]
                ? this._changeDetectorRef.detectChanges()
                : null;  
            }  else{
              this.toastr.error("No Record Found");
            }        
       
           });
        
        } else if (type == "Party Wise") {
          this.ERPServiceService.getPartyWiseProductDetails(params).subscribe(primSales=>{
            if(primSales.length>0){
              this.productWiseReportShowAdminState=true;
              var index=0;
              primSales.forEach((element,i) => {
                index++;
                element["index"]=index;
                element["creditValue"]=element.qty*element.creditvalue;
              });
              this.dtOptionsProdPrimary = {
                pagingType: "full_numbers",
                ordering: true,
                info: true,
                scrollY: 300,
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                destroy: true,
                data: primSales,
                responsive: true,
                columns: [
                  {
                    title: "S No.",
                    data: "index",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title:"State Name",
                    data: "StateName",
                    render: function (data) {
                      
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title:"HeadQuarter Name",
                    data: "Headquater",
                    render: function (data) {
                      
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title:"Party Name",
                    data: "PartyName",
                    render: function (data) {
                      
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                    {
                    title: "Product Name",
                    data: "ProductName",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title: "Qty",
                    data: "qty",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                  {
                    title: "Amount",
                    data: "creditValue",
                    render: function (data) {
                      if (data === "") {
                        return "---";
                      } else {
                        return data;
                      }
                    },
                    defaultContent: "---",
                  },
                ],
                language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Search Records",
                },
                dom: "Bfrtip",
                buttons: [
                  {
                    extend: "excel",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                  {
                    extend: "csv",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                  {
                    extend: 'copy',
                    exportOptions: {
                      columns: ':visible'
                    }
                  },
                  {
                    extend: "print",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                ],
              };
              !this._changeDetectorRef["destroyed"]
                ? this._changeDetectorRef.detectChanges()
                : null;
              this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
              this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
              !this._changeDetectorRef["destroyed"]
                ? this._changeDetectorRef.detectChanges()
                : null;  
            }  else{
              this.toastr.error("No Record Found");
            }        
       
           });
        
        }
      }
      if (collStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
        params['type'] = collStatus;
        params['month'] = _moment(this.dcrForm.value.toDate).format("MM");
        params['year'] = _moment(this.dcrForm.value.toDate).format("YYYY");
        if (type == "State Wise" || type == "Manager Wise") {
          this.ERPServiceService.getStateWiseProductDetails(params).subscribe(primSales=>{
 
           if(primSales.length>0){
             this.productWiseReportShowAdminState=true;
             var index=0;
             primSales.forEach((element,i) => {
               index++;
               element["index"]=index;
               element["creditValue"]=element.qty*element.creditvalue;
             });
             this.dtOptionsProdPrimary = {
               pagingType: "full_numbers",
               ordering: true,
               info: true,
               scrollY: 300,
               scrollX: true,
               scrollCollapse: true,
               paging: false,
               destroy: true,
               data: primSales,
               responsive: true,
               columns: [
                 {
                   title: "S No.",
                   data: "index",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                 {
                   title:"State Name",
                   data: "StateName",
                   render: function (data) {
                     
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                 {
                   title: "Amount",
                   data: "creditValue",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
               ],
               language: {
                 search: "_INPUT_",
                 searchPlaceholder: "Search Records",
               },
               dom: "Bfrtip",
               buttons: [
                 {
                   extend: "excel",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
                 {
                   extend: "csv",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
                 {
                   extend: 'copy',
                   exportOptions: {
                     columns: ':visible'
                   }
                 },
                 {
                   extend: "print",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
               ],
             };
             !this._changeDetectorRef["destroyed"]
               ? this._changeDetectorRef.detectChanges()
               : null;
             this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
             this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
             !this._changeDetectorRef["destroyed"]
               ? this._changeDetectorRef.detectChanges()
               : null;  
           }  else{
             this.toastr.error("No Record Found");
           }        
      
          });
         } else if (type == "Headquarter Wise") {
           this.ERPServiceService.getHQWiseProductDetails(params).subscribe(primSales=>{
 
             if(primSales.length>0){
               this.productWiseReportShowAdminState=true;
               var index=0;
               primSales.forEach((element,i) => {
                 index++;
                 element["index"]=index;
                 element["creditValue"]=element.qty*element.creditvalue;
               });
               this.dtOptionsProdPrimary = {
                 pagingType: "full_numbers",
                 ordering: true,
                 info: true,
                 scrollY: 300,
                 scrollX: true,
                 scrollCollapse: true,
                 paging: false,
                 destroy: true,
                 data: primSales,
                 responsive: true,
                 columns: [
                   {
                     title: "S No.",
                     data: "index",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"State Name",
                     data: "StateName",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"Headquarter Name",
                     data: "HQ",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Amount",
                     data: "creditValue",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                 ],
                 language: {
                   search: "_INPUT_",
                   searchPlaceholder: "Search Records",
                 },
                 dom: "Bfrtip",
                 buttons: [
                   {
                     extend: "excel",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: "csv",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: 'copy',
                     exportOptions: {
                       columns: ':visible'
                     }
                   },
                   {
                     extend: "print",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                 ],
               };
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;
               this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
               this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;  
             }  else{
               this.toastr.error("No Record Found");
             }        
        
            });
         
         } else if (type == "Party Wise") {
           this.ERPServiceService.getPartyWiseProductDetails(params).subscribe(primSales=>{
             if(primSales.length>0){
               this.productWiseReportShowAdminState=true;
               var index=0;
               primSales.forEach((element,i) => {
                 index++;
                 element["index"]=index;
                 element["creditValue"]=element.qty*element.creditvalue;
               });
               this.dtOptionsProdPrimary = {
                 pagingType: "full_numbers",
                 ordering: true,
                 info: true,
                 scrollY: 300,
                 scrollX: true,
                 scrollCollapse: true,
                 paging: false,
                 destroy: true,
                 data: primSales,
                 responsive: true,
                 columns: [
                   {
                     title: "S No.",
                     data: "index",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"HeadQuarter Name",
                     data: "HQ",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Amount",
                     data: "creditValue",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                 ],
                 language: {
                   search: "_INPUT_",
                   searchPlaceholder: "Search Records",
                 },
                 dom: "Bfrtip",
                 buttons: [
                   {
                     extend: "excel",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: "csv",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: 'copy',
                     exportOptions: {
                       columns: ':visible'
                     }
                   },
                   {
                     extend: "print",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                 ],
               };
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;
               this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
               this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;  
             }  else{
               this.toastr.error("No Record Found");
             }        
        
            });
         
         }
      }
      if (outStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
        params['type'] = outStatus;
        if (type == "State Wise" || type == "Manager Wise") {
          this.ERPServiceService.getStateWiseProductDetails(params).subscribe(primSales=>{
           if(primSales.length>0){
             this.productWiseReportShowAdminState=true;
             var index=0;
             primSales.forEach((element,i) => {
               index++;
               element["index"]=index;
               element["creditValue"]=element.qty*element.creditvalue;
             });
             this.dtOptionsProdPrimary = {
               pagingType: "full_numbers",
               ordering: true,
               info: true,
               scrollY: 300,
               scrollX: true,
               scrollCollapse: true,
               paging: false,
               destroy: true,
               data: primSales,
               responsive: true,
               columns: [
                 {
                   title: "S No.",
                   data: "index",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                 {
                   title:"State Name",
                   data: "StateName",
                   render: function (data) {
                     
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                 {
                   title: "Amount",
                   data: "creditValue",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
               ],
               language: {
                 search: "_INPUT_",
                 searchPlaceholder: "Search Records",
               },
               dom: "Bfrtip",
               buttons: [
                 {
                   extend: "excel",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
                 {
                   extend: "csv",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
                 {
                   extend: 'copy',
                   exportOptions: {
                     columns: ':visible'
                   }
                 },
                 {
                   extend: "print",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
               ],
             };
             !this._changeDetectorRef["destroyed"]
               ? this._changeDetectorRef.detectChanges()
               : null;
             this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
             this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
             !this._changeDetectorRef["destroyed"]
               ? this._changeDetectorRef.detectChanges()
               : null;  
           }  else{
             this.toastr.error("No Record Found");
           }        
          });
         } else if (type == "Headquarter Wise") {
           this.ERPServiceService.getHQWiseProductDetails(params).subscribe(primSales=>{
 
             if(primSales.length>0){
               this.productWiseReportShowAdminState=true;
               var index=0;
               primSales.forEach((element,i) => {
                 index++;
                 element["index"]=index;
                 element["creditValue"]=element.qty*element.creditvalue;
               });
               this.dtOptionsProdPrimary = {
                 pagingType: "full_numbers",
                 ordering: true,
                 info: true,
                 scrollY: 300,
                 scrollX: true,
                 scrollCollapse: true,
                 paging: false,
                 destroy: true,
                 data: primSales,
                 responsive: true,
                 columns: [
                   {
                     title: "S No.",
                     data: "index",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"State Name",
                     data: "StateName",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"Headquarter Name",
                     data: "headquarter",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Amount",
                     data: "creditValue",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                 ],
                 language: {
                   search: "_INPUT_",
                   searchPlaceholder: "Search Records",
                 },
                 dom: "Bfrtip",
                 buttons: [
                   {
                     extend: "excel",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: "csv",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: 'copy',
                     exportOptions: {
                       columns: ':visible'
                     }
                   },
                   {
                     extend: "print",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                 ],
               };
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;
               this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
               this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;  
             }  else{
               this.toastr.error("No Record Found");
             }        
        
            });
         
         } else if (type == "Party Wise") {
           this.ERPServiceService.getPartyWiseProductDetails(params).subscribe(primSales=>{
             if(primSales.length>0){
               this.productWiseReportShowAdminState=true;
               var index=0;
               primSales.forEach((element,i) => {
                 index++;
                 element["index"]=index;
                 element["creditValue"]=element.qty*element.creditvalue;
                 
               });
               this.dtOptionsProdPrimary = {
                 pagingType: "full_numbers",
                 ordering: true,
                 info: true,
                 scrollY: 300,
                 scrollX: true,
                 scrollCollapse: true,
                 paging: false,
                 destroy: true,
                 data: primSales,
                 responsive: true,
                 columns: [
                   {
                     title: "S No.",
                     data: "index",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"HeadQuarter Name",
                     data: "headquarter",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"Party Name",
                     data: "PartyName",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Amount",
                     data: "creditValue",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                 ],
                 language: {
                   search: "_INPUT_",
                   searchPlaceholder: "Search Records",
                 },
                 dom: "Bfrtip",
                 buttons: [
                   {
                     extend: "excel",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: "csv",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: 'copy',
                     exportOptions: {
                       columns: ':visible'
                     }
                   },
                   {
                     extend: "print",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                 ],
               };
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;
               this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
               this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;  
             }  else{
               this.toastr.error("No Record Found");
             }        
        
            });
         
         }
      }
      if (returnStatus) {
        if( type == "Manager Wise"){
          params['stateCode'] = '';
        }else{
          params['stateCode'] = stateCode;
        }
        params['type'] = returnStatus;
        params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
        params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
        
        if (type == "State Wise" || type == "Manager Wise") {
          this.ERPServiceService.getStateWiseProductDetails(params).subscribe(primSales=>{
           if(primSales !=undefined &&primSales.length>0){
             this.productWiseReportShowAdminState=true;
             var index=0;
             primSales.forEach((element,i) => {
               index++;
               element["index"]=index;
               element["creditValue"]=element.qty*element.creditvalue;
             });
             this.dtOptionsProdPrimary = {
               pagingType: "full_numbers",
               ordering: true,
               info: true,
               scrollY: 300,
               scrollX: true,
               scrollCollapse: true,
               paging: false,
               destroy: true,
               data: primSales,
               responsive: true,
               columns: [
                 {
                   title: "S No.",
                   data: "index",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                 {
                   title:"State Name",
                   data: "StateName",
                   render: function (data) {
                     
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                   {
                   title: "Product Name",
                   data: "ProductName",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                 {
                   title: "Qty",
                   data: "qty",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
                 {
                   title: "Amount",
                   data: "creditValue",
                   render: function (data) {
                     if (data === "") {
                       return "---";
                     } else {
                       return data;
                     }
                   },
                   defaultContent: "---",
                 },
               ],
               language: {
                 search: "_INPUT_",
                 searchPlaceholder: "Search Records",
               },
               dom: "Bfrtip",
               buttons: [
                 {
                   extend: "excel",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
                 {
                   extend: "csv",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
                 {
                   extend: 'copy',
                   exportOptions: {
                     columns: ':visible'
                   }
                 },
                 {
                   extend: "print",
                   exportOptions: {
                     columns: ":visible",
                   },
                 },
               ],
             };
             !this._changeDetectorRef["destroyed"]
               ? this._changeDetectorRef.detectChanges()
               : null;
             this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
             this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
             !this._changeDetectorRef["destroyed"]
               ? this._changeDetectorRef.detectChanges()
               : null;  
           }  else{
            this.productWiseReportShowAdminState=false;
             this.toastr.error("No Record Found");
           }        
      
          });
         } else if (type == "Headquarter Wise") {
           this.ERPServiceService.getHQWiseProductDetails(params).subscribe(primSales=>{
 
             if(primSales !=undefined && primSales.length>0){
               this.productWiseReportShowAdminState=true;
               var index=0;
               primSales.forEach((element,i) => {
                 index++;
                 element["index"]=index;
                 element["creditValue"]=element.qty*element.creditvalue;
               });
               this.dtOptionsProdPrimary = {
                 pagingType: "full_numbers",
                 ordering: true,
                 info: true,
                 scrollY: 300,
                 scrollX: true,
                 scrollCollapse: true,
                 paging: false,
                 destroy: true,
                 data: primSales,
                 responsive: true,
                 columns: [
                   {
                     title: "S No.",
                     data: "index",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"Headquarter Name",
                     data: "HQ",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                     {
                     title: "Product Name",
                     data: "ProductName",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Qty",
                     data: "qty",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Amount",
                     data: "creditValue",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                 ],
                 language: {
                   search: "_INPUT_",
                   searchPlaceholder: "Search Records",
                 },
                 dom: "Bfrtip",
                 buttons: [
                   {
                     extend: "excel",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: "csv",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: 'copy',
                     exportOptions: {
                       columns: ':visible'
                     }
                   },
                   {
                     extend: "print",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                 ],
               };
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;
               this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
               this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;  
             }  else{
              this.productWiseReportShowAdminState=false;
               this.toastr.error("No Record Found");
             }        
        
            });
         
         } else if (type == "Party Wise") {
           this.ERPServiceService.getPartyWiseProductDetails(params).subscribe(primSales=>{
             if(primSales !=undefined &&  primSales.length>0){
               this.productWiseReportShowAdminState=true;
               var index=0;
               primSales.forEach((element,i) => {
                 index++;
                 element["index"]=index;
                 element["creditValue"]=element.qty*element.creditvalue;
               });
               this.dtOptionsProdPrimary = {
                 pagingType: "full_numbers",
                 ordering: true,
                 info: true,
                 scrollY: 300,
                 scrollX: true,
                 scrollCollapse: true,
                 paging: false,
                 destroy: true,
                 data: primSales,
                 responsive: true,
                 columns: [
                   {
                     title: "S No.",
                     data: "index",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"HeadQuarter Name",
                     data: "HQ",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title:"Party Name",
                     data: "PartyName",
                     render: function (data) {
                       
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                     {
                     title: "Product Name",
                     data: "ProductName",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Qty",
                     data: "qty",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                   {
                     title: "Amount",
                     data: "creditValue",
                     render: function (data) {
                       if (data === "") {
                         return "---";
                       } else {
                         return data;
                       }
                     },
                     defaultContent: "---",
                   },
                 ],
                 language: {
                   search: "_INPUT_",
                   searchPlaceholder: "Search Records",
                 },
                 dom: "Bfrtip",
                 buttons: [
                   {
                     extend: "excel",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: "csv",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                   {
                     extend: 'copy',
                     exportOptions: {
                       columns: ':visible'
                     }
                   },
                   {
                     extend: "print",
                     exportOptions: {
                       columns: ":visible",
                     },
                   },
                 ],
               };
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;
               this.dataTableProdPrimary = $(this.tableForProdPrimary.nativeElement);
               this.dataTableProdPrimary.DataTable(this.dtOptionsProdPrimary);
               !this._changeDetectorRef["destroyed"]
                 ? this._changeDetectorRef.detectChanges()
                 : null;  
             }  else{
              this.productWiseReportShowAdminState=false;
               this.toastr.error("No Record Found");
             }        
        
            });
         
         }
      }
     
    //   if (priStatus) {
    //     params['stateCode'] = stateCode;
    //     params['type'] = priStatus;
    //     params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
    //     params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
    //     //if (type == "State Wise" || type == "Manager Wise") {
    //       console.log(" priStatus params : ",params);
    //       if (type == "State Wise" ) {
    //       forkAPIArray.push(this.ERPServiceService.getStateWiseProductDetails(params));
    //     } else if (type == "Headquarter Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getHqWiseProductDetails(params));
    //     } else if (type == "Party Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getPartyProductWiseDetails(params));
    //     }
  
    //     forkAPIResponseArray.push("primarySaleResult");
    //   }
    //   if (collStatus) {
    //     params['type'] = collStatus;
    //     params['month'] = _moment(this.dcrForm.value.toDate).format("MM");
    //     params['year'] = _moment(this.dcrForm.value.toDate).format("YYYY");
    //     console.log(" collStatus params : ",params);
       
    //     if (type == "State Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getStateWiseProductDetails(params));
    //     } else if (type == "Headquarter Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getHqWiseProductDetails(params));
    //     }  else if (type == "Party Wise") {e
    //       forkAPIArray.push(this.ERPServiceService.getPartyProductWiseDetails(params));
    //     }
        
    //     forkAPIResponseArray.push("collectionSaleResult");
    //   }
    //   if (outStatus) {
    //     params['type'] = outStatus;
    //     console.log(" outStatus params : ",params);
       
    //     if (type == "State Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getStateWiseProductDetails(params));
    //     } else if (type == "Headquarter Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getHqWiseProductDetails(params));
    //     } else if (type == "Party Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getPartyProductWiseDetails(params));
    //     }
    //     forkAPIResponseArray.push("outstandingSaleResult");
    //   }
    //   if (returnStatus) {
    //     params['type'] = returnStatus;
    //     params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
    //     params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
    //     console.log(" returnStatus params : ",params);
    //     if (type == "State Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getStateWiseProductDetails(params));
    //     } else if (type == "Headquarter Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getHqWiseProductDetails(params));
    //     } else if (type == "Party Wise") {
    //       forkAPIArray.push(this.ERPServiceService.getPartyProductWiseDetails(params));
    //     } 
    //     forkAPIResponseArray.push("returnSaleResult");
    //   }
      
    //   forkJoin(forkAPIArray).subscribe((forkAPIResponseArray) => {
    //     let final = [];
    //     let ddoption = [];
    //     let loopLengthVal = 0;
    //     let index = 0;
    //     for (let i = 0; i < forkAPIArray.length; i++) {
    //       if (forkAPIResponseArray[i]) {
    //         if (forkAPIResponseArray[i].length > loopLengthVal) {
    //           loopLengthVal = forkAPIResponseArray[i].length;
    //           index = i;
    //         }
    //       }
    //     }
  
    //     if (priStatus) {
    //       const primData = {
    //         title: "Primary Sale",
    //         data: "primary",
    //         render: function (data) {
    //           if (data === "") {
    //             return "---";
    //           } else {
    //             return data;
    //           }
    //         },
    //         defaultContent: "---",
    //       }
    //       ddoption.push(primData)
    //     }
    //     if (collStatus) {
    //       const collData = {
    //         title: "Collection Sale",
    //         data: "collection",
    //         render: function (data) {
    //           if (data === "") {
    //             return "---";
    //           } else {
    //             return data;
    //           }
    //         },
    //         defaultContent: "---",
    //       }
    //       ddoption.push(collData)
    //     }
    //     if (outStatus) {
    //       const outData = {
    //         title: "Outstanding Sale",
    //         data: "outstanding",
    //         render: function (data) {
    //           if (data === "") {
    //             return "---";
    //           } else {
    //             return data;
    //           }
    //         },
    //         defaultContent: "---",
    //       }
    //       ddoption.push(outData)
    //     }
    //     if (returnStatus) {
    //       const returnData = {
    //         title: "SalesReturn",
    //         data: "return",
    //         render: function (data) {
    //           if (data === "") {
    //             return "---";
    //           } else {
    //             return data;
    //           }
    //         },
    //         defaultContent: "---",
    //       }
    //       ddoption.push(returnData)
    //     }
    //     if (type == "State Wise") {
    //       if (forkAPIResponseArray[index]) {
    //         forkAPIResponseArray[index].forEach(element => {
    //           let temp = {
    //             stateName: '',
    //             stateCode: '',
    //             productCode : '',
    //             productName:'',
    //             primary: 0,
    //             collection: 0,
    //             outstanding: 0,
    //             return: 0
    //           }
    //           if (priStatus) {
    //             if (forkAPIResponseArray[0]) {
    //               let primaryFound = forkAPIResponseArray[0].filter(res => {
    //                 return element.StateCode == res.StateCode
    //               });
  
    //               if (primaryFound.length > 0) {
    //                 temp.stateName = primaryFound[0].StateName;
    //                 temp.stateCode = primaryFound[0].StateCode;
    //                 temp.productCode = primaryFound[0].productcode;
    //                 temp.productName = primaryFound[0].productname;
    //                 temp.primary = primaryFound[0].amount;
    //               }
    //             }
    //           }
    //           if (collStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let collFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.StateCode) == (res.StateCode)
    //               });
    //               if (collFound.length > 0) {
    //                 temp.stateName = collFound[0].StateName;
    //                 temp.stateCode = collFound[0].StateCode;
    //                 temp.productCode = collFound[0].productcode;
    //                 temp.productName = collFound[0].productname;
    //                 temp.collection = collFound[0].AMOUNT;
    //               }
    //             }
    //           }
    //           if (outStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let outFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.StateCode) == (res.StateCode)
    //               });
    //               if (outFound.length > 0) {
    //                 temp.stateName = outFound[0].StateName;
    //                 temp.stateCode = outFound[0].StateCode;
    //                 temp.productCode = outFound[0].productcode;
    //                 temp.productName = outFound[0].productname;
    //                 temp.outstanding = outFound[0].amount;
    //               }
    //             }
    //           }
    //           if (returnStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let returnFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.StateCode) == (res.StateCode)
    //               });
    //               if (returnFound.length > 0) {
    //                 temp.stateName = returnFound[0].StateName;
    //                 temp.stateCode = returnFound[0].StateCode;
    //                 temp.productCode = returnFound[0].productcode;
    //                 temp.productName = returnFound[0].productname;
    //                 temp.return = returnFound[0].Amount;
    //               }
    //             }
    //           }
    //           final.push(temp);
    //         });
    //         final.map((item, index) => {
    //           item["index"] = index;
    //           item["stateName"] = item.stateName;
    //           item["productName"] = item.productName;
    //           item["productCode"] = item.productCode;
    //         });
    //         this.test=final;
    //         this.dtOptions = {
    //           pagingType: "full_numbers",
    //           ordering: true,
    //           info: true,
    //           scrollY: 300,
    //           scrollX: true,
    //           scrollCollapse: true,
    //           paging: false,
    //           destroy: true,
    //           data: final,
    //           responsive: true,
    //           columns: [
    //             {
    //               title: "S. No.",
    //               data: "index",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data + 1;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "State",
    //               data: "stateName",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data
    //                  // "<button id='viewHQSale' class='btn btn-warning btn-sm'><i class='fa fa-check'></i>&nbsp;&nbsp;" + data + "</button>"
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "Product",
    //               data: "productName",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "ProductCode",
    //               data: "productCode",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },

    //             ...ddoption
    //           ],
    //           rowCallback: (
    //             row: Node,
    //             data: any[] | Object,
    //             index: number
    //           ) => {
    //             const self = this;
    //             $('td', row).unbind('click');
    //             $('td button', row).bind('click', (event) => {
    //               let rowObject = JSON.parse(JSON.stringify(data));
    //               if (event.target.id === "viewHQSale") {
    //                 self.getHqwiseSale(rowObject, 'Headquarter Wise');
    //               }
    //             });
    //           },
    //           language: {
    //             search: "_INPUT_",
    //             searchPlaceholder: "Search Records",
    //           },
    //           dom: "Bfrtip",
    //           buttons: [
    //             {
    //               extend: 'excel',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             },
    //             {
    //               extend: 'csv',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             },
    //             {
    //               extend: 'copy',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             }],
    //         };
    //         this.showProcessing = false;
    //         this.showReport = true;
    //       } else {
    //         this.showProcessing = false;
    //         this.showReport = false;
    //         alert('No records found !!!');
    //       }
    //     } else if(type == "Headquarter Wise"){
    //       if (forkAPIResponseArray[index]) {
    //         forkAPIResponseArray[index].forEach(element => {
    //           let temp = {
    //             stateName: '',
    //             headquater: '',
    //             headquarterCode: '',
    //             productCode : '',
    //             productName:'',
    //             primary: 0,
    //             collection: 0,
    //             outstanding: 0,
    //             return: 0
    //           }
    //           if (priStatus) {
    //             if (forkAPIResponseArray[0]) {
    //               let primaryFound = forkAPIResponseArray[0].filter(res => {
    //                 return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HeadquaterCode)
    //               });
  
    //               if (primaryFound.length > 0) {
    //                 temp.stateName = primaryFound[0].StateName;
    //                 temp.headquater = primaryFound[0].Headquater;
    //                 temp.headquarterCode = primaryFound[0].HeadquaterCode;
    //                 temp.productCode = primaryFound[0].productcode;
    //                 temp.productName = primaryFound[0].productname;
    //                 temp.primary = primaryFound[0].primSales;
    //               }
    //             }
    //           }
    //           if (collStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let collFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HQCode)
    //               });
    //               if (collFound.length > 0) {
    //                 temp.stateName = collFound[0].StateName;
    //                 temp.headquater = collFound[0].HQ;
    //                 temp.headquarterCode = collFound[0].HQCode;
    //                 temp.productCode = collFound[0].productcode;
    //                 temp.productName = collFound[0].productname;
    //                 temp.collection = collFound[0].AMOUNT;
    //               }
    //             }
    //           }
    //           if (outStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let outFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.headquarterCode)
    //               });
    //               if (outFound.length > 0) {
    //                 temp.stateName = outFound[0].StateName;
    //                 temp.headquater = outFound[0].headquarter;
    //                 temp.headquarterCode = outFound[0].headquarterCode;
    //                 temp.productCode = outFound[0].productcode;
    //                 temp.productName = outFound[0].productname;
    //                 temp.outstanding = outFound[0].amount;
    //               }
    //             }
    //           }
    //           if (returnStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let returnFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HQCode)
    //               });
    //               if (returnFound.length > 0) {
    //                 temp.headquater = returnFound[0].HQ;
    //                 temp.headquarterCode = returnFound[0].HQCode;
    //                 temp.productCode = returnFound[0].productcode;
    //                 temp.productName = returnFound[0].productname;
    //                 temp.return = returnFound[0].Amount;
    //               }
    //             }
    //           }
    //           final.push(temp);
    //         });
    //         final.map((item, index) => {
    //           item["index"] = index;
    //           item["stateName"] = item.stateName;
    //           item["headquater"] = item.headquater;
    //           item["productName"] = item.productName;
    //           item["productCode"] = item.productCode;
    //         });
    //         this.dtOptions = {
    //           pagingType: "full_numbers",
    //           ordering: true,
    //           info: true,
    //           scrollY: 300,
    //           scrollX: true,
    //           scrollCollapse: true,
    //           paging: false,
    //           destroy: true,
    //           data: final,
    //           responsive: true,
    //           columns: [
    //             {
    //               title: "S. No.",
    //               data: "index",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data + 1;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "State",
    //               data: "stateName",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "Headquarter",
    //               data: "headquater",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data
    //                 //"<button id='viewPartySale' class='btn btn-warning btn-sm'><i class='fa fa-check'></i>&nbsp;&nbsp;" + data + "</button>"
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "Product",
    //               data: "productName",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "ProductCode",
    //               data: "productCode",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             ...ddoption
    //           ],
    //           rowCallback: (
    //             row: Node,
    //             data: any[] | Object,
    //             index: number
    //           ) => {
    //             const self = this;
    //             $('td', row).unbind('click');
    //             $('td button', row).bind('click', (event) => {
    //               let rowObject = JSON.parse(JSON.stringify(data));
    //               if (event.target.id === "viewPartySale") {
    //                 self.getHqwiseSale(rowObject, 'Party Wise');
    //               }
    //             });
    //           },
    //           language: {
    //             search: "_INPUT_",
    //             searchPlaceholder: "Search Records",
    //           },
    //           dom: "Bfrtip",
    //           buttons: [
    //             {
    //               extend: 'excel',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             },
    //             {
    //               extend: 'csv',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             },
    //             {
    //               extend: 'copy',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             }],
    //         };
    //         this.showProcessing = false;
    //         this.showReport = true;
    //       } else {
    //         this.showProcessing = false;
    //         this.showReport = false;
    //         alert('No records found !!!');
    //       }
    //     } else if(type == "Party Wise"){

    //       if (forkAPIResponseArray[index]) {
    //         forkAPIResponseArray[index].forEach(element => {
    //           let temp = {
    //             headquater: '',
    //             party: '',
    //             productCode : '',
    //             productName:'',
    //             primary: 0,
    //             collection: 0,
    //             outstanding: 0,
    //             return : 0,
    //           }
    //           if (priStatus) {
    //             if (forkAPIResponseArray[0]) {
    //               let primaryFound = forkAPIResponseArray[0].filter(res => {
    //                 return (element.PartyCode) == (res.PartyCode)
    //               });
  
    //               if (primaryFound.length > 0) {
    //                 temp.headquater = primaryFound[0].Headquater;
    //                 temp.party = primaryFound[0].PartyName;
    //                 temp.productCode = primaryFound[0].productcode;
    //                 temp.productName = primaryFound[0].productname;
    //                 temp.primary = primaryFound[0].primSales;
    //               }
    //             }
    //           }
    //           if (collStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let collFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.PartyCode) == (res.PartyCode)
    //               });
    //               if (collFound.length > 0) {
    //                 temp.headquater = collFound[0].HQ;
    //                 temp.party = collFound[0].PartyName;
    //                 temp.productCode = collFound[0].productcode;
    //                 temp.productName = collFound[0].productname;
    //                 temp.collection = collFound[0].AMOUNT;
    //               }
    //             }
    //           }
    //           if (outStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let outFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.PartyCode) == res.PartyCode
    //               });
    //               if (outFound.length > 0) {
    //                 temp.headquater = outFound[0].headquarter;
    //                 temp.party = outFound[0].PartyName;
    //                 temp.productCode = outFound[0].productcode;
    //                 temp.productName = outFound[0].productname;
    //                 temp.outstanding = outFound[0].amount;
    //               }
    //             }
    //           } if (returnStatus) {
    //             let indexNum = 0;
    //             if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
    //             if (forkAPIResponseArray[indexNum]) {
    //               let returnFound = forkAPIResponseArray[indexNum].filter(res => {
    //                 return (element.PartyCode) == res.PartyCode
    //               });
    //               if (returnFound.length > 0) {
    //                 temp.headquater = returnFound[0].HQ;
    //                 temp.party = returnFound[0].PartyName;
    //                 temp.productCode = returnFound[0].productcode;
    //                 temp.productName = returnFound[0].productname;
    //                 temp.return = returnFound[0].Amount;
    //               }
    //             }
    //           }
              
    //           final.push(temp);
    //         });
    //         final.map((item, index) => {
    //           item["index"] = index;
    //           item["headquater"] = item.headquater;
    //           item["party"] = item.party;
    //           item["productName"] = item.productName;
    //           item["productCode"] = item.productCode;
    //         });
    //         this.dtOptions = {
    //           pagingType: "full_numbers",
    //           ordering: true,
    //           info: true,
    //           scrollY: 300,
    //           scrollX: true,
    //           scrollCollapse: true,
    //           paging: false,
    //           destroy: true,
    //           data: final,
    //           responsive: true,
    //           columns: [
    //             {
    //               title: "S. No.",
    //               data: "index",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data + 1;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "Headquater",
    //               data: "headquater",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "Party",
    //               data: "party",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "Product",
    //               data: "productName",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               },
    //               defaultContent: "---",
    //             },
    //             {
    //               title: "ProductCode",
    //               data: "productCode",
    //               render: function (data) {
    //                 if (data === "") {
    //                   return "---";
    //                 } else {
    //                   return data;
    //                 }
    //               }
    //             },
    //             ...ddoption
    //           ],
    //           rowCallback: (
    //             row: Node,
    //             data: any[] | Object,
    //             index: number
    //           ) => {
    //             const self = this;
    //             // Unbind first in order to avoid any duplicate handler
    //             // (see https://github.com/l-lin/angular-datatables/issues/87)
    //             $("td", row).unbind("click");
    //             $("td .getDateWiseReport", row).bind(
    //               "click",
    //               () => {
    //                 //console.log("row : ", data);
    //                 let rowObject = JSON.parse(
    //                   JSON.stringify(data)
    //                 );
    //                 //self.getDateWise(rowObject.userId);
    //                 this._changeDetectorRef.detectChanges();
    //               }
    //             );
    //             $("td button", row).bind("click", () => {
    //               //console.log("row : ", data);
    //               let rowObject = JSON.parse(
    //                 JSON.stringify(data)
    //               );
    //             });
    //             return row;
    //           },
    //           language: {
    //             search: "_INPUT_",
    //             searchPlaceholder: "Search Records",
    //           },
    //           dom: "Bfrtip",
    //           buttons: [
    //             {
    //               extend: 'excel',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             },
    //             {
    //               extend: 'csv',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             },
    //             {
    //               extend: 'copy',
    //               exportOptions: {
    //                 columns: ':visible'
    //               }
    //             }],
    //         };
    //         this.showProcessing = false;
    //         this.showReport = true;
    //       } else {
    //         this.showProcessing = false;
    //         this.showReport = false;
    //         alert('No records found !!!');
    //       }


    //     }
  
    //     this._changeDetectorRef.detectChanges();
    //     this.dataTable = $(this.table.nativeElement);
    //     this.dataTable.DataTable(this.dtOptions);
    //     this._changeDetectorRef.detectChanges();
      
    // });
    this.showReport=false;
    if(this.currentUser.userInfo[0].designationLevel==0){
      if(type === "State Wise"){
     this.productWiseReportShowAdminHq = false;
   this.productWiseReportShowAdminState = true;  
    this.productWiseReportShowAdminParty = false;
    this.productWiseReportShowAdminStateAmount = false;
    this.productWiseReportShowProductAdminMgeWise = false;
      }else if(type === "Headquarter Wise"){
        this.productWiseReportShowAdminHq = true;
      this.productWiseReportShowAdminState = false; 
      this.productWiseReportShowAdminParty = false;
      this.productWiseReportShowAdminStateAmount = false;
      this.productWiseReportShowProductAdminMgeWise = false;
      }else if(type === "Party Wise"){
        this.productWiseReportShowAdminHq = false;
        this.productWiseReportShowAdminState = false;  
         this.productWiseReportShowAdminParty = true;
         this.productWiseReportShowAdminStateAmount = false;
         this.productWiseReportShowProductAdminMgeWise = false;
      }else if(type === "Manager Wise"){
        this.productWiseReportShowAdminHq = false;
        this.productWiseReportShowAdminState = false;  
         this.productWiseReportShowAdminParty = false; 
         this.productWiseReportShowProductAdminMgeWise = true;
         this.productWiseReportShowAdminStateAmount = false;

      }
      
    }else if(this.currentUser.userInfo[0].designationLevel!=0||this.currentUser.userInfo[0].designationLevel!=1){
      if(type === "State Wise"){
        this.productWiseReportShowMgrState = true;
        this.productWiseReportShowMgrHq = false;
        this.stockistWiseReportShowMgrState = false;
        this.productWiseReportShowMgeWise = false;
      }else if(type === "Headquarter Wise"){
        this.productWiseReportShowMgrHq = true;
        this.productWiseReportShowMgrState = false;
        this.stockistWiseReportShowMgrHq = false;
        this.productWiseReportShowMgeWise = false;
      }else if(type === "Party Wise"){
        this.productWiseReportShowMgrHq = false;
        this.productWiseReportShowMgrState = false;
        this.stockistWiseReportShowMgrHq = false;
        this.productWiseReportShowMgeWise = false;
      }else if(type === "Manager Wise"){
        this.productWiseReportShowMgeWise = true;
        
  
        this.productWiseReportShowMgrHq = false;
        this.productWiseReportShowMgrState = false;
        this.stockistWiseReportShowMgr = false;
      }
    }
    this.showReport = false;
this.isShowHqClick = false;
this.isShowPartyClick = false;
this.showProcessing = false;
  }
  
  }

  getHqwiseSale(object, type) {
    let params = {};
    //let type = this.dcrForm.value.type;
    let forkAPIArray = [];
    let forkAPIResponseArray = [];
    let priStatus = this.dcrForm.value.saleType.find(obj => obj == "Primary");
    let collStatus = this.dcrForm.value.saleType.find(obj => obj == "Collection");
    let outStatus = this.dcrForm.value.saleType.find(obj => obj == "Outstanding");
    let returnStatus = this.dcrForm.value.saleType.find(obj => obj == "SalesReturn");
    params = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.employeeCode,
      APIEndPoint: this.currentUser.company.APIEndPoint,
    }
    
    if (type == "Headquarter Wise") { 
      this.showProcessingHq=true;
      params['stateCode'] = object['stateCode'];
    } else if (type == "Party Wise") {
      this.showProcessingParty=true;
      params['headquaterCode'] = object['headquarterCode'];
    }   


    if(this.dcrForm.value.type=="Manager Wise"){
  params['userId'] = this.dcrForm.value.employeeId;
}
    
   

    if (priStatus) {
      params['type'] = priStatus;
      params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
      params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
      if (type == "Headquarter Wise") {
        forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
      } else if (type == "Party Wise") {
        forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
      }
      forkAPIResponseArray.push("primarySaleResult");
    }
    if (collStatus) {
      params['type'] = collStatus;
      params['month'] = _moment(this.dcrForm.value.toDate).format("MM");
      params['year'] = _moment(this.dcrForm.value.toDate).format("YYYY");
      if (type == "Headquarter Wise") {
        forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
      } else if (type == "Party Wise") {
        forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
      }
      forkAPIResponseArray.push("collectionSaleResult");
    }
    if (outStatus) {
      params['type'] = outStatus;
      if (type == "Headquarter Wise") {
        forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
      } else if (type == "Party Wise") {
        forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
      }
      forkAPIResponseArray.push("outstandingSaleResult");
    }
    if (returnStatus) {
      params['type'] = returnStatus;
      params['fromDate'] = _moment(this.dcrForm.value.fromDate).format("DD-MM-YYYY");
      params['toDate'] = _moment(this.dcrForm.value.toDate).format("DD-MM-YYYY");
      if (type == "Headquarter Wise") {
        forkAPIArray.push(this.ERPServiceService.getHQWiseDetails(params));
      }else if (type == "Party Wise") {
        forkAPIArray.push(this.ERPServiceService.getPartyWiseDetails(params));
      }
      forkAPIResponseArray.push("returnSaleResult");
    }
    forkJoin(forkAPIArray).subscribe((forkAPIResponseArray) => {
      let final = [];
      let ddoption = [];
      let loopLengthVal = 0;
      let index = 0;
      for (let i = 0; i < forkAPIArray.length; i++) {
        if (forkAPIResponseArray[i]) {
          if (forkAPIResponseArray[i].length > loopLengthVal) {
            loopLengthVal = forkAPIResponseArray[i].length;
            index = i;
          }
        }
      }

      if (priStatus) {
        const primData = {
          title: "Primary Sale",
          data: "primary",
          render: function (data) {
            if (data === "") {
              return "---";
            } else {
              return data;
            }
          },
          defaultContent: "---",
        }
        ddoption.push(primData)
      }
      if (collStatus) {
        const collData = {
          title: "Collection Sale",
          data: "collection",
          render: function (data) {
            if (data === "") {
              return "---";
            } else {
              return data;
            }
          },
          defaultContent: "---",
        }
        ddoption.push(collData)
      }
      if (outStatus) {
        const outData = {
          title: "Outstanding Sale",
          data: "outstanding",
          render: function (data) {
            if (data === "") {
              return "---";
            } else {
              return data;
            }
          },
          defaultContent: "---",
        }
        ddoption.push(outData)
      }
      if (returnStatus) {
        const returnData = {
          title: "SalesReturn",
          data: "return",
          render: function (data) {
            if (data === "") {
              return "---";
            } else {
              return data;
            }
          },
          defaultContent: "---",
        }
        ddoption.push(returnData)
      }
      if (type == "Headquarter Wise") {
        if (forkAPIResponseArray[index]) {
          forkAPIResponseArray[index].forEach(element => {
            let temp = {
              stateName: '',
              headquater: '',
              headquarterCode: '',
              primary: 0,
              collection: 0,
              outstanding: 0,
              return: 0
            }
            if (priStatus) {
              if (forkAPIResponseArray[0]) {
                let primaryFound = forkAPIResponseArray[0].filter(res => {
                  return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HeadquaterCode)
                });

                if (primaryFound.length > 0) {
                  temp.stateName = primaryFound[0].StateName;
                  temp.headquater = primaryFound[0].Headquater;
                  temp.headquarterCode = primaryFound[0].HeadquaterCode;
                  temp.primary = primaryFound[0].primSales;
                }
              }
            }
            if (collStatus) {
              let indexNum = 0;
              if (priStatus) { indexNum++ };
              if (forkAPIResponseArray[indexNum]) {
                let collFound = forkAPIResponseArray[indexNum].filter(res => {
                  return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HQCode)
                });
                if (collFound.length > 0) {
                  temp.stateName = collFound[0].StateName;
                  temp.headquater = collFound[0].HQ;
                  temp.headquarterCode = collFound[0].HQCode;
                  temp.collection = collFound[0].AMOUNT;
                }
              }
            }
            if (outStatus) {
              let indexNum = 0;
              if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
              if (forkAPIResponseArray[indexNum]) {
                let outFound = forkAPIResponseArray[indexNum].filter(res => {
                  return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.headquarterCode)
                });
                if (outFound.length > 0) {
                  temp.stateName = outFound[0].StateName;
                  temp.headquater = outFound[0].headquarter;
                  temp.headquarterCode = outFound[0].headquarterCode;
                  temp.outstanding = outFound[0].amount;
                }
              }
            }
            if (returnStatus) {
              let indexNum = 0;
              if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
              if (forkAPIResponseArray[indexNum]) {
          
                let returnFound = forkAPIResponseArray[indexNum].filter(res => {
                  return (element.HeadquaterCode || element.headquarterCode || element.HQCode) == (res.HQCode)
                });
              
                if (returnFound.length > 0) {
                  temp.headquater = returnFound[0].HQ;
                  temp.headquarterCode = returnFound[0].HQCode;
                  temp.return = returnFound[0].Amount;
                }
              }
            }
            final.push(temp);
          });
          final.map((item, index) => {
            item["index"] = index;
            item["stateName"] = item.stateName;
            item["headquater"] = item.headquater;
          });
          this.dtOptionsHq = {
            pagingType: "full_numbers",
            ordering: true,
            info: true,
            scrollY: 300,
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            destroy: true,
            data: final,
            responsive: true,
            columns: [
              {
                title: "S. No.",
                data: "index",
                render: function (data) {
                  if (data === "") {
                    return "---";
                  } else {
                    return data + 1;
                  }
                },
                defaultContent: "---",
              },
              {
                title: "State",
                data: "stateName",
                render: function (data) {
                  if (data === "") {
                    return "---";
                  } else {
                    return data;
                  }
                },
                defaultContent: "---",
              },
              {
                title: "Headquarter",
                data: "headquater",
                render: function (data) {
                  if (data === "") {
                    return "---";
                  } else {
                    return "<button id='viewPartySale' class='btn btn-warning btn-sm'><i class='fa fa-check'></i>&nbsp;&nbsp;" + data + "</button>"
                  }
                },
                defaultContent: "---",
              },
              ...ddoption
            ],
            rowCallback: (
              row: Node,
              data: any[] | Object,
              index: number
            ) => {
              const self = this;
              $('td', row).unbind('click');
              $('td button', row).bind('click', (event) => {
                let rowObject = JSON.parse(JSON.stringify(data));
                if (event.target.id === "viewPartySale") {
                  self.getHqwiseSale(rowObject, 'Party Wise');
                }
              });
            },
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search Records",
            },
            dom: "Bfrtip",
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'copy',
                exportOptions: {
                  columns: ':visible'
                }
              }],
          };
          this.showProcessingHq=false;
          this.showProcessing = false;
          this.showReport = true;
          this.isShowHqClick = true;
          this.isShowPartyClick = false;
        } else {
          this.showProcessingHq=false;
          this.showProcessing = false;
          this.showReport = true;
          this.isShowHqClick = false;
          this.isShowPartyClick = false;
          alert('No records found !!!');
        }
        this._changeDetectorRef.detectChanges();
        this.dataTableHq = $(this.tableHq.nativeElement);
        this.dataTableHq.DataTable(this.dtOptionsHq);
        this._changeDetectorRef.detectChanges();
      } else if (type == "Party Wise") {
        if (forkAPIResponseArray[index]) {
          forkAPIResponseArray[index].forEach(element => {
            let temp = {
              headquater: '',
              party: '',
              primary: 0,
              collection: 0,
              outstanding: 0,
              return: 0
            }
            if (priStatus) {
              if (forkAPIResponseArray[0]) {
                let primaryFound = forkAPIResponseArray[0].filter(res => {
                  return (element.PartyCode) == (res.PartyCode)
                });

                if (primaryFound.length > 0) {
                  temp.headquater = primaryFound[0].Headquater;
                  temp.party = primaryFound[0].PartyName;
                  temp.primary = primaryFound[0].primSales;
                }
              }
            }
            if (collStatus) {
              let indexNum = 0;
              if (priStatus) { indexNum++ };
              if (forkAPIResponseArray[indexNum]) {
                let collFound = forkAPIResponseArray[indexNum].filter(res => {
                  return (element.PartyCode) == (res.PartyCode)
                });
                if (collFound.length > 0) {
                  temp.headquater = collFound[0].HQ;
                  temp.party = collFound[0].PartyName;
                  temp.collection = collFound[0].AMOUNT;
                }
              }
            }
            if (outStatus) {
              let indexNum = 0;
              if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ };
              if (forkAPIResponseArray[indexNum]) {
                let outFound = forkAPIResponseArray[indexNum].filter(res => {
                  return (element.PartyCode) == res.PartyCode
                });
                
                if (outFound.length > 0) {
                  temp.headquater = outFound[0].headquarter;
                  temp.party = outFound[0].PartyName;
                  temp.outstanding = outFound[0].amount;
                }
              }
            }


            if (returnStatus) {
              let indexNum = 0;
              if (priStatus) { indexNum++ }; if (collStatus) { indexNum++ } if (outStatus) { indexNum++ };
              if (forkAPIResponseArray[indexNum]) {
          
                let returnFound = forkAPIResponseArray[indexNum].filter(res => {
                 
                  return (element.PartyCode) == (res.PartyCode)
                });
               
                if (returnFound.length > 0) {
                  temp.headquater = returnFound[0].HQ;
                  temp.party = returnFound[0].PartyName;
                  temp.return = returnFound[0].Amount;
                }
              }
            }
            final.push(temp);
          });
          final.map((item, index) => {
            item["index"] = index;
            item["headquater"] = item.headquater;
            item["party"] = item.party;
          });
          this.dtOptionsParty = {
            pagingType: "full_numbers",
            ordering: true,
            info: true,
            scrollY: 300,
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            destroy: true,
            data: final,
            responsive: true,
            columns: [
              {
                title: "S. No.",
                data: "index",
                render: function (data) {
                  if (data === "") {
                    return "---";
                  } else {
                    return data + 1;
                  }
                },
                defaultContent: "---",
              },
              {
                title: "Headquater",
                data: "headquater",
                render: function (data) {
                  if (data === "") {
                    return "---";
                  } else {
                    return data;
                  }
                },
                defaultContent: "---",
              },
              {
                title: "Party",
                data: "party",
                render: function (data) {
                  if (data === "") {
                    return "---";
                  } else {
                    return data;
                  }
                },
                defaultContent: "---",
              },
              ...ddoption
            ],
            rowCallback: (
              row: Node,
              data: any[] | Object,
              index: number
            ) => {
              const self = this;
              // Unbind first in order to avoid any duplicate handler
              // (see https://github.com/l-lin/angular-datatables/issues/87)
              $("td", row).unbind("click");
              $("td .getDateWiseReport", row).bind(
                "click",
                () => {
                  //console.log("row : ", data);
                  let rowObject = JSON.parse(
                    JSON.stringify(data)
                  );
                  //self.getDateWise(rowObject.userId);
                  this._changeDetectorRef.detectChanges();
                }
              );
              $("td button", row).bind("click", () => {
                //console.log("row : ", data);
                let rowObject = JSON.parse(
                  JSON.stringify(data)
                );
              });
              return row;
            },
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search Records",
            },
            dom: "Bfrtip",
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'copy',
                exportOptions: {
                  columns: ':visible'
                }
              }],
          };
          this.showProcessingParty=false;
          this.showProcessing = false;
          this.showReport = true;
          this.isShowHqClick = true;
          this.isShowPartyClick = true;
        } else {
          this.showProcessingParty=false;
          this.showProcessing = false;
          this.showReport = true;
          this.isShowHqClick = true;
          this.isShowPartyClick = false;
          alert('No records found !!!');
        }
        this._changeDetectorRef.detectChanges();
        this.dataTableParty = $(this.tableParty.nativeElement);
        this.dataTableParty.DataTable(this.dtOptionsParty);
        this._changeDetectorRef.detectChanges();
      }
    });
  }

  exporttoExcel() {
    this.exportAsService
      .save(this.exportAsConfig, "Primary Sale")
      .subscribe(() => {
        // save started
      });
  }
  scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: "smooth" });
  }


}
