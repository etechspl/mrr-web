import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimarySaleStateHqMgrWiseComponent } from './primary-sale-state-hq-mgr-wise.component';

describe('PrimarySaleStateHqMgrWiseComponent', () => {
  let component: PrimarySaleStateHqMgrWiseComponent;
  let fixture: ComponentFixture<PrimarySaleStateHqMgrWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimarySaleStateHqMgrWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimarySaleStateHqMgrWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
