import { ActivatedRoute, Params } from '@angular/router';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
//import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import Swal from 'sweetalert2';
import { LatLongService } from '../../../../../core/services/LatLong/lat-long.service';
import { } from 'googlemaps';
import { FileHandlingService } from '../../../../../core/services/FileHandling/file-handling.service';
import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import * as jsPDF from 'jspdf';
import { URLService } from '../../../../../core/services/URL/url.service';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { ConsolidatedExpenseComponent } from '../consolidated-expense/consolidated-expense.component';
@Component({
  selector: 'm-user-complete-dcrinfo',
  templateUrl: './user-complete-dcrinfo.component.html',
  styleUrls: ['./user-complete-dcrinfo.component.scss']
})

export class UserCompleteDCRInfoComponent implements OnInit {
  dataSource;
  mockLocation;
  imageToShow;
  apiResponseFound = []



  @ViewChild('TABLE') TABLE: ElementRef;
  @ViewChild('table1') table1: ElementRef;
  status: boolean = false;

  constructor(private _latLongService: LatLongService,
    private _productService: ProductService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _dcrReportsService: DcrReportsService,
    private activatedRoute: ActivatedRoute,
    private userDetailService: UserDetailsService,
    private _fileHandlingService: FileHandlingService,
    private exportAsService: ExportAsService,
    private _urlService: URLService
  ) { }
  currentUser = JSON.parse(sessionStorage.currentUser);
  otherTypeLablesExist = (this.currentUser.company.lables.otherType) ? true : false;


  ngOnInit() {
    //this.activatedRoute.queryParams.subscribe((params: Params) => {
    // let dcrId = params['dcrId'];
    // this.getUserCompleteDCRDetails(dcrId);
    //});

  }
  showGeoReportType = false;
  getUserCompleteDCRDetails(dcrId: string) {
    this.apiResponseFound = [];
    this._dcrReportsService.getUserCompleteDCRDetails(dcrId).subscribe(res => {
      if (res == undefined || res == 0) {
        //1 this.dataSource = res;
        this.showGeoReportType = false;
      }
      else {
        //------------Preeti Arora (19-11-2020)------------------------------------
        if (res[0].hasOwnProperty('info')) {
          for (let i = 0; i < res[0].info.length; i++) {
            if (res[0].info[i].hasOwnProperty("signId")) {
              this._fileHandlingService.getImageDetailBasedOnId(res[0].info[i].signId).subscribe(fileResult => {
                if (fileResult.length) {
                  res[0].info[i]["modifiedFileName"] = fileResult[0].modifiedFileName
                }
              }, err => {
                console.log(err);
              });
            }

          }
        }
        //-------------------------------------------------------------

        //-----------------------Praveen Kumar(22-08-2019)-------------------------
        if (res[0].dcrExpense != null && res[0].dcrExpense != undefined) {
          for (let i = 0; i < res[0].dcrExpense.length; i++) {
            this._fileHandlingService.getImageDetailBasedOnId(res[0].dcrExpense[i].fileId).subscribe(fileResult => {
              if (fileResult.length) {
                res[0].dcrExpense[i]["modifiedFileName"] = fileResult[0].modifiedFileName
              }
            }, err => {
              console.log(err);
            });
          }
        }
        //---------------------------------END-------------------------------------
        if (res[0].dcrMeetings != null && res[0].dcrMeetings != undefined) {
          for (let i = 0; i < res[0].dcrMeetings.length; i++) {
            this.userDetailService.getEmployees(this.currentUser.companyId, res[0].dcrMeetings[i].meetingWith).subscribe(empRes => {

              res[0].dcrMeetings[i].meetingWithName = empRes[0].name
            }, err => {
              console.log(err);
            })
          }
        }
        if (this.currentUser.company.geoDistance != 0 && res[0].info != null && res[0].info != undefined) {
          if (res[0].hasOwnProperty('info')) {
            if (this.currentUser.company.validation.hasOwnProperty('isShowOnlyLatLong') && this.currentUser.company.validation.isShowOnlyLatLong == true) {
              // this.status=true;
              res[0].info.forEach(element => {
                let data = "";
                //console.log('Gourav',element);
                if (element.geoLocation.length > 0) {
                  data = "Lat: " + element.geoLocation[0].lat + ", Long: " + element.geoLocation[0].long
                }

                if (element.hasOwnProperty('geoLocation')) {
                  if (element.geoLocation[0].hasOwnProperty('mocked') && element.geoLocation[0].mocked === true) {
                    console.log('if')

                    element.geoLocation[0]['mockLocation'] = 'Yes';
                    this.mockLocation = 'Yes'
                  }else{
                    console.log('else')
                    element.geoLocation[0]['mockLocation'] = 'No';
                    this.mockLocation = 'No';
                  }
                } else {
                  // res[0].info[i].geoLocation[0]['mockLocation'] = 'No';
                  
                  // res[0].info[i]['locAddress']= ".......";
                  // this.apiResponseFound.push("")
                }

                element["locAddress"] = data;
              });

            } else {
              console.log('this')
              console.log('else--2')

              for (let i = 0; i < res[0].info.length; i++) {
                console.log('else--2  for',res[0].info[i])

                res[0].info[i].locAddress = ".......";
                if (res[0].info[i].geoLocation && (res[0].info[i].geoLocation[0]&& res[0].info[i].geoLocation[0].mocked==true)) {
                  //res[0].info[i].geoLocation[0]['mockLocation'] = 'Yes';
                  res[0].info[i].geoLocation.push({mockLocation : 'Yes'})
                  console.log('else--2 if')

                  this.mockLocation = 'Yes'
                } else {
                  if(res[0].geoLocation){
                    res[0].geoLocation.push({mockLocation : 'No'})
                    this.mockLocation = 'No'
                   
                  }else{
                    console.log('else--2 else')
                    console.log('res---------------------->',res)
                    //res[0].info[i].geoLocation[0]['mockLocation'] = 'No';
                    res[0].info[i].geoLocation.push({mockLocation : 'No'})
                    this.mockLocation = 'No'
                    console.log(res[0].info[i].geoLocation[0])
                  }
                }
                // if (res[0].info[i].hasOwnProperty('geoLocation')) {
                //   if (res[0].info[i].geoLocation[0].hasOwnProperty('mocked') && res[0].info[i].geoLocation[0].mocked === true) {
                //     res[0].info[i].geoLocation[0]['mockLocation'] = 'Yes';
                //     this.mockLocation = 'Yes'
                //   }
                // } else {
                //   res[0].info[i].geoLocation[0]['mockLocation'] = 'No';
                //   this.mockLocation = 'No';
                //   // res[0].info[i]['locAddress']= ".......";
                //   // this.apiResponseFound.push("")
                // }

                if (!res[0].info[i].hasOwnProperty('geoLocation')) {
                  res[0].info[i].locAddress = '.......'
                } else {
                  let count = 0;

                  this._latLongService.getAddressOnLongLatBasis(res[0].info[i].geoLocation[0]).subscribe(locRes => {


                    if (locRes.length != 0) {
                      this.apiResponseFound.push("")
                      res[0].info[i].locAddress = locRes[0].address;
                      this._changeDetectorRef.detectChanges();
                    } else {
                      var geocoder = geocoder = new google.maps.Geocoder();
                      var latlng1 = { lat: parseFloat(res[0].info[i].geoLocation[0].lat), lng: parseFloat(res[0].info[i].geoLocation[0].long) };
                      setTimeout(() => {

                        geocoder.geocode({ 'location': latlng1 }, (results, status) => {
                          console.log('hello');
                          count++;
                          this.apiResponseFound.push("")
                          if (status == google.maps.GeocoderStatus.OK) {

                            if (results[0]) {

                              res[0].info[i].locAddress = results[0].formatted_address;
                              this._changeDetectorRef.detectChanges();

                            } else {
                              res[0].info[i].locAddress = ".......";
                            }
                          } else {
                            res[0].info[i].locAddress = ".......";
                          }
                        });
                      }, 1500)
                    }
                  }, err => {
                    console.log(err);
                    res[0].info[i].locAddress = ".......";
                    this._changeDetectorRef.detectChanges();
                  })
                }
              }
            }

          }
        }


        this.dataSource = res;
        console.log('tabele Data',this.dataSource)
        this.dataSource[0].info.forEach(i => {
          if (i.mefAnswers && i.mefAnswers.productDiscussed) {
            let query = {
              where: {
                companyId: this.currentUser.companyId,
                id: { inq: i.mefAnswers.productDiscussed }
              }
            }
            this._productService.getAllProducts(query).subscribe(res => {
              i.productDetails = res;
              (!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
            });
          }
        }
        );

        this.showGeoReportType = true;
        this._changeDetectorRef.detectChanges();
      }

    }, err => {
      console.log(err)

    })

  }

  getProductDetails(ids) {
    let query = {
      where: {
        companyId: this.currentUser.companyId,
        id: { inq: ids }
      }
    }
    // this._productService.getAllProducts(query).subscribe(res => {
    //   (!this._changeDetectorRef['destroyed'])? this._changeDetectorRef.detectChanges(): null;
    //   return res;
    // });
  }



  filterRpmData(dataSource) {
    return dataSource.filter(i => i.providerType == 'RMP')
  }
  filterDrugData(dataSource) {

    return dataSource.filter(i => i.providerType == 'Drug')
  }
  filterStockistData(dataSource) {

    return dataSource.filter(i => i.providerType == 'Stockist')
  }
  showImage(fileDetall) {
    let fileName = "";
    if (typeof (fileDetall) == "object") {
      fileName = JSON.parse(JSON.stringify(fileDetall)).modifiedFileName;
    } else {
      fileName = fileDetall;
    }
    let url = 'http://103.11.84.144:3015/api/containers/DCRImage/download/' + fileName;
    window.open(url, "Title but its not working", 'toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500');
  }
  showSignature(fileDetall) {

    let fileName = "";
    if (typeof (fileDetall) == "object") {
      fileName = JSON.parse(JSON.stringify(fileDetall));
    } else {
      fileName = fileDetall;
    }

    let url = 'http://103.11.84.144:3015/api/containers/Signature/download/' + fileName;


    window.open(url, "Title but its not working", 'toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500');

  }

  // exporttoExcel() {
  //   // download the file using old school javascript method
  //   this.exportAsService.save(this.exportAsConfig, 'Sales Status Report').subscribe(() => {
  //     // save started
  //   });
  // }

  onPreviewClick() {

    let printContents, popupWin;

    const department = this.currentUser.section;
    const designation = this.currentUser.designation;
    const companyName = this.currentUser.companyName;
    printContents = document.getElementById('tableid1').innerHTML;
    const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
  <head>
  <style>
  img {
  float: left;
  }

  .flex-container {
  display: flex;
  justify-content: space-between;
  }

  .tg {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tg td {
  font-family: Arial, sans-serif;
  font-size: 14px;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  table {
    border-collapse: collapse;
}

  tg.th {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg .tg-lboi {
  border-color: inherit;
  text-align: left;
  vertical-align: middle;

  }

  .tg .tg-u6fn {
  font-weight: bold;
  font-size: 18px;
  background-color: #efefef;
  color:black;
  border-color: inherit;
  text-align: left;
  vertical-align: top;
  padding-top: 0px;
  padding-bottom: 0px;
  }

  .tg .tg-yz93 {
  border-color: inherit;
  text-align: right;
  vertical-align: middle
  }

  .tb2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

   td {
  font-family: Arial, sans-serif;
  font-size: 12px;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  th {
    background-color:#AAB7B8;
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-weight: normal;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  
  }

  .tb2 .tg-oiyu {
  font-size: 10px;
  text-align: left;
  vertical-align: middle
  }
  .tg-bckGnd {
  font-weight: bold;
  background-color: #efefef;
  }


  .tb2 .tg-3ppa {
  font-size: 10px;
  background-color: #efefef;
  color:black;
  text-align: left;
  vertical-align: middle;
  padding-top: 2px;
  padding-bottom: 2px;
  }

  .footer {
  position: sticky;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #efefef;
  color: rgb(2, 2, 2);
  text-align: right
  }



  button {
  background-color: #008CBA;
  border: none;
  color: white;
  margin: 5px 0px;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  }

  @media print {
  @page { size: A4; margin: 5mm; }

  .footer {
  position: relative;
  }
  tr {
  page-break-inside: avoid;
  }

  button{
  display : none;
  }
  }
  </style>
  </head>
  <body onload="window.print();window.close()" style="padding-bottom:30px">
  <div class="flex-container">
  <div style="width: 40%; ">
  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

  </div>

  <div style="text-align: right;">
  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Basic Info </h2>
  <div>
  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
  </div>
  </div>
  </div>
  ${printContents}</body>
  </html>
  `
    );
    // popupWin.document.close();
  }



  exportToExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.TABLE.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, 'DCR Provider Visit.xlsx');
  }
  exportAll() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table1.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, 'DCR Provider Visit.xlsx');
  }

}
