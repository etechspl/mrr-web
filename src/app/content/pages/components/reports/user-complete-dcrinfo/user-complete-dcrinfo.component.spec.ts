import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCompleteDCRInfoComponent } from './user-complete-dcrinfo.component';

describe('UserCompleteDCRInfoComponent', () => {
  let component: UserCompleteDCRInfoComponent;
  let fixture: ComponentFixture<UserCompleteDCRInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCompleteDCRInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCompleteDCRInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
