import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { StateComponent } from '../../filters/state/state.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import * as XLSX from "xlsx";
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { MatSlideToggleChange } from "@angular/material";

@Component({
  selector: 'm-dcr-call-details',
  templateUrl: './dcr-call-details.component.html',
  styleUrls: ['./dcr-call-details.component.scss']
})
export class DcrCallDetailsComponent implements OnInit {

  constructor(
    private _dcrReportService : DcrReportsService,
    private exportAsService: ExportAsService

  ) { }
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementId: 'tableId1', // the id of html/table element
	}

  show = false;
  showProcessing: boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist: boolean = false;
  showDivisionFilter: boolean = false;
  isToggled: boolean = false;
  selection = new SelectionModel(true, []);
  selection1 = new SelectionModel(true, []);
  primaryContact: any;
  showHeadquarter: boolean = false;
  employeeFilter: boolean = false;
  stateFilter: boolean = true;
  dataSource: MatTableDataSource<any>;
  // columnHeader = {'studentID': 'ID', 'fname': 'First Name', 'lname': 'Last Name', 'weight': 'Weight', 'symbol': 'Code'};
  tableData: any = [];
  // tableData=[];
  displayedColumns: string[] = [ 'category', 'districtName','name', 'address', 'weight', 'user'];
  // columnHeader = { 'designation': 'designation', 'employeeName': 'employeeName', 'districtName': 'districtName', 'stateName': 'stateName', 'dcrDate': 'dcrDate', 'dcrNotSubmitted': 'dcrNotSubmitted', 'dcrSubmitted': 'dcrSubmitted' };

  dcrStatus = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    state: new FormControl(null),
    district: new FormControl(null),
    designation: new FormControl(null),
    employee: new FormControl(null),
    fromDate: new FormControl(null),
    toDate: new FormControl(null),

  })



  ngOnInit() {
    console.log('currentUser=> ', this.currentUser);


    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) {
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }

    if (this.currentUser.company.isDivisionExist === true) {
      this.isDivisionExist = true;
      this.dcrStatus.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
   getReportType(val) {
    if (val === "Geographical") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
    this.dcrStatus.patchValue({ reportType: val })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }
  toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
  // exporttoExcel() {
		
	// 		const lastCol = this.displayedColumns.pop();
	// 		// download the file using old school javascript method
	// 		setTimeout(() => {
	// 			this.exportAsService
	// 				.save(this.exportAsConfig, "state wise da")
	// 				.subscribe(() => {
	// 					// save started
	// 					this.displayedColumns.push(lastCol);
	// 				});
	// 		}, 300);
	// 	//}
	// }
  exporttoExcel(){
  
    let dataToExport=[];
    dataToExport=this.dataSource.data.map((x)=>({
      "userName" : x.userName,
      "productiveCalls" : x.productiveCalls,
      "totalPOB" : x.totalPOB,
      "totalCalls"  : x.totalCalls,
      "state" : x.state,
      "district" : x.district
    }));
    dataToExport.push(
      {
      "Name" :'------',
      "productiveCalls" : '------',
      "totalPOB" : '------',
      "totalCalls"  : '------',
      "state" : '------',
      "district" : '------'
      }
    )

    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "dcrCallDetails.xlsx");
  }

  getTypeValue(val) {
    console.log(val);
    this.dcrStatus.patchValue({ type: val })
    let { } = this.currentUser;
    if (val === 'State') {

      this.showHeadquarter = false;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callStateComponent.setBlank();
        this.callDivisionComponent.setBlank();
      }
    } else if (val === 'Headquarter') {
      this.showHeadquarter = true;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
        this.callDistrictComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDistrictComponent.setBlank();
      }
    } else if (val === 'Employee Wise') {
      this.stateFilter = false;
      this.showHeadquarter = false;
      this.employeeFilter = true;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;

        this.callEmployeeComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
      }
    }
  }

  getDivisionValue(val) {
    this.dcrStatus.patchValue({ division: val })
    if (this.dcrStatus.value.type == "Employee Wise") {
      //=======================Clearing Filter===================
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    } else if (this.dcrStatus.value.type == "State" || this.dcrStatus.value.type == "Headquarter") {
      //=======================Clearing Filter===================
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      //===============================END=======================
      let divisionIds = {
        division: val
      };
      this.callStateComponent.getStateBasedOnDivision(divisionIds);
    }
  }

  getStateValue(val) {
    this.dcrStatus.patchValue({ state: val })
    if (this.dcrStatus.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.dcrStatus.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrict(val) {
    this.dcrStatus.patchValue({ district: val })
  }

  getDesignation(val) {
    this.dcrStatus.patchValue({ designation: val })
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.dcrStatus.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    }
  }

  getEmployee(val) {
    this.dcrStatus.patchValue({ employee: val })
  }
  getRecords() {
    console.log('data', this.dcrStatus.value)
    this.showProcessing = true;
    this.isToggled = false;
    let params = {
      companyId: this.currentUser.companyId,
      //type:this.dcrStatus.value.type
    }

    if (this.dcrStatus.value.type === 'State') {
      params['companyId'] = this.currentUser.companyId;
      params['fromDate'] = this.dcrStatus.value.fromDate;
      params['toDate'] = this.dcrStatus.value.toDate;
      params['stateIds'] = this.dcrStatus.value.state;
      params['type'] = 'State'

    } else if (this.dcrStatus.value.type === 'Headquarter') {
       params['type'] = 'Headquarter'
       params['companyId'] = this.currentUser.companyId;
      params['districtIds'] = this.dcrStatus.value.district ;
      params['fromDate'] = this.dcrStatus.value.fromDate;
      params['toDate'] = this.dcrStatus.value.toDate;
      params['stateId'] =this.dcrStatus.value.state ;
    }
    else if (this.dcrStatus.value.type === 'Employee Wise') {
       params['type'] = 'Employee'
       params['companyId'] = this.currentUser.companyId;  
       params['fromDate'] = this.dcrStatus.value.fromDate;
       params['toDate'] = this.dcrStatus.value.toDate;
       params['userIds'] =  this.dcrStatus.value.employee ;
    }


    //----------------------------------mahender sharma--------------------------------------------- 
    console.log("params :--------/////------------------- ", params);
    this._dcrReportService.dcrcallreport(params).subscribe(res=>{
      console.log(res);

      this.showProcessing = false;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.show = true;
    })
  }

}
