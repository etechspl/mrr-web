import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcrCallDetailsComponent } from './dcr-call-details.component';

describe('DcrCallDetailsComponent', () => {
  let component: DcrCallDetailsComponent;
  let fixture: ComponentFixture<DcrCallDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcrCallDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcrCallDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
