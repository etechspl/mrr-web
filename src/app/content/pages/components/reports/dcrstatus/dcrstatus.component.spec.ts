import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcrstatusComponent } from './dcrstatus.component';

describe('DcrstatusComponent', () => {
  let component: DcrstatusComponent;
  let fixture: ComponentFixture<DcrstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcrstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcrstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
