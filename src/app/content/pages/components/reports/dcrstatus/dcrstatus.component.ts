import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { StateService } from '../../../../../core/services/state.service';
import { DistrictService } from '../../../../../core/services/district.service';
import { MonthYearService } from '../../../../../core/services/month-year.service';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'm-dcrstatus',
  templateUrl: './dcrstatus.component.html',
  styleUrls: ['./dcrstatus.component.scss']
})

export class DcrstatusComponent implements OnInit {
  stateModel = new FormControl('', [Validators.required]);
  headquarterModel = new FormControl('', [Validators.required]);
  monthModel = new FormControl('', [Validators.required]);
  yearModel = new FormControl('', [Validators.required]);

  //currentYear = new Date().getFullYear();

  //yearList = [];
  monthList;
  yearList;
  stateList;
  districtList;

  displayedColumns = ['name', 'designation', 'submit', 'submitDate', 'apprv', 'apprvDate'];
  dataSource = ELEMENT_DATA;

  constructor(private stateService: StateService, private districtService: DistrictService, private monthYearService: MonthYearService, private userDetailService: UserDetailsService) { }

  ngOnInit() {


    let monthYear = this.monthYearService.getMonthAndCurrentYearList(2017);
    this.monthList = monthYear[0].month;
    this.yearList = monthYear[1].year;


  }

  getEmployees(state) {
    this.userDetailService.getEmployees("5b2e22083225df01ba7d6059", state.id).subscribe(res => {
      this.districtList = res;
    }, err => {
      console.log(err)
      alert("No User Found");
    });
  }

}
export interface PeriodicElement {
  name: string;
  designation: string;
  submit: string;
  submitDate: string;
  apprv: string;
  apprvDate: string;
}


const ELEMENT_DATA: PeriodicElement[] = [
  { name: 'Anajan Rana', designation: "MR", submit: 'Yes', submitDate: "15-Sept-2018", apprv: 'Yes', apprvDate: "17-Sept-2018" },
  { name: 'Praveen Singh', designation: "MR", submit: 'Yes', submitDate: "18-Sept-2018", apprv: 'Yes', apprvDate: "20-Sept-2018" },
  { name: 'Divya Kumari', designation: "MR", submit: 'No', submitDate: "------", apprv: 'No', apprvDate: "-------" },
  { name: 'Praveen Kumar', designation: "MR", submit: 'No', submitDate: "------", apprv: 'N0', apprvDate: "-------" },
  { name: 'Ravindra Singh', designation: "MR", submit: 'Yes', submitDate: "22-Sept-2018", apprv: 'Yes', apprvDate: "23-Sept-2018" },
  { name: 'Rahul Kumar', designation: "MR", submit: 'No', submitDate: "-------", apprv: 'No', apprvDate: "-------" },
  { name: 'Preeti', designation: "MR", submit: 'No', submitDate: "-------", apprv: 'No', apprvDate: "-------" },
  { name: 'Chaman', designation: "MR", submit: 'Yes', submitDate: "15-Sept-2018", apprv: 'No', apprvDate: "-------" },
  { name: 'Provas Das', designation: "MR", submit: 'Yes', submitDate: "24-Sept-2018", apprv: 'Yes', apprvDate: "27-Sept-2018" },
  { name: 'jyoti Kaushik', designation: "MR", submit: 'Yes', submitDate: "27-Sept-2018", apprv: 'Yes', apprvDate: "29-Sept-2018" },
];
