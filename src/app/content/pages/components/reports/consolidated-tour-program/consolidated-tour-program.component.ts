import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TypeComponent } from '../../filters/type/type.component';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl } from '@angular/forms';
import { DistrictComponent } from '../../filters/district/district.component';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
declare var $;

@Component({
  selector: 'm-consolidated-tour-program',
  templateUrl: './consolidated-tour-program.component.html',
  styleUrls: ['./consolidated-tour-program.component.scss']
})
export class ConsolidatedTourProgramComponent implements OnInit {

  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;

  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('dataTable') table;

  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showTPDetails: boolean = false;
  public showEmployee: boolean = false;
  showAdminAndMGRLevelFilter = false;
  showProcessing: boolean = false;
  dataSource;
  dataTable: any;
  dtOptions: any;
  dtTrigger: any;
  constructor(
    private _tpService: TourProgramService,
    private changeDetectorRef: ChangeDetectorRef,
    private _toastr: ToastrService,
  ) { }

  currentUser = JSON.parse(sessionStorage.currentUser);
  tpFilter = new FormGroup({
    companyId: new FormControl(this.currentUser.companyId),
    reportType: new FormControl('Geographical'),
    type: new FormControl(null),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    status: new FormControl(true),
    designation: new FormControl(null),
    employeeIds: new FormControl(null),
    month: new FormControl(null),
    year: new FormControl(null)
  })

  ngOnInit() {
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }

  getReportType(val) {
    this.showTPDetails = false;
    if (val === "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    this.showTPDetails = false;
    if (val == "State") {
      this.showState = true;
      this.showDistrict = false;
    } else if (val == "Headquarter") {
      this.showState = true;
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    this.tpFilter.patchValue({ type: val });

  }
  getStateValue(val) {
    this.showTPDetails = false;
    this.tpFilter.patchValue({ stateInfo: val });

    if (this.tpFilter.value.type == "Headquarter") {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);
    }
  }
  getDistrictValue(val) {
    this.showTPDetails = false;
    this.tpFilter.patchValue({ districtInfo: val });
  }
  getDesignation(val) {
    this.showTPDetails = false;
    this.showProcessing = false;
    this.tpFilter.patchValue({ designation: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, this.tpFilter.value.status);

  }
  getStatusValue(val) {
    this.showTPDetails = false;
    this.showProcessing = false;
    this.tpFilter.patchValue({ status: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.tpFilter.value.designation, val);

  }

  getEmployeeValue(val) {
    this.showTPDetails = false;
    this.tpFilter.patchValue({ employeeIds: val });
  }
  getMonth(val) {
    this.showTPDetails = false;
    this.tpFilter.patchValue({ month: val });
  }
  getYear(val) {
    this.showTPDetails = false;
    this.tpFilter.patchValue({ year: val });
  }


  viewRecords() {
	  let showEmployeeColumn = true;
    if (this.currentUser.userInfo[0].designationLevel == 1) {
      showEmployeeColumn = false;
    }
    let userIds;
    if (this.tpFilter.value.type == "Self" || this.tpFilter.value.type == null) { //Null is for MR level
      userIds = [this.currentUser.id];
    } else {
      userIds = this.tpFilter.value.employeeId
    }

    this._tpService.getConsolidatedTPDetail(this.tpFilter.value).subscribe(res => {
		console.log("result1234...",res)
      let tpResult = [];
      tpResult.push(res[0])
      console.log(tpResult);


      if (tpResult == null || tpResult == undefined || tpResult.length == 0) {
        this.showTPDetails = false;
        this._toastr.info('For Selected Filter data not found', 'Data Not Found');

      } else {
        let test = [];
        test = [
          {
            title: 'State Name',
            data: 'stateName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: this.currentUser.company.lables.hqLabel + 'Name',
            data: 'Headquarter',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }
          ,
          {
            title: 'Employee Name',
            data: 'empName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Designation',
            data: 'designation',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }

          , {
            title: 'Approval Status',
            data: 'approvalStatus',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }
        ]

        let count = 1;
        for (let i = 0; i < tpResult[0].area.length; i++) {
          test.push({
            title: "" + count,
            data: 'area.' + i + ".area",
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          })
          count++;
        }


        this.showProcessing = true;
        this.dataSource = res;
        this.showTPDetails = true;
        this.showProcessing = false;

        this.dtOptions = {
          data: this.dataSource,
          destroy: true,

          columns: test,
          dom: 'Bfrtip',
          buttons: [
            'copy', 'csv', 'excel', 'print'
          ]
        };
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);

      }
      this.changeDetectorRef.detectChanges()
    }, err => {
      console.log(err)
      this.showTPDetails = false;
      this.showProcessing = false;
      this._toastr.info('For Selected Filter data not found', 'Data Not Found');

    })

  }
}
