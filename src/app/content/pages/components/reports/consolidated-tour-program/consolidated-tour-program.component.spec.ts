import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidatedTourProgramComponent } from './consolidated-tour-program.component';

describe('ConsolidatedTourProgramComponent', () => {
  let component: ConsolidatedTourProgramComponent;
  let fixture: ComponentFixture<ConsolidatedTourProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolidatedTourProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolidatedTourProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
