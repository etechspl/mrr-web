import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MRQtyValuewiseSecondarySaleComponent } from './mrqty-valuewise-secondary-sale.component';

describe('MRQtyValuewiseSecondarySaleComponent', () => {
  let component: MRQtyValuewiseSecondarySaleComponent;
  let fixture: ComponentFixture<MRQtyValuewiseSecondarySaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MRQtyValuewiseSecondarySaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MRQtyValuewiseSecondarySaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
