import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UtilsService } from '../../../../../core/services/utils.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StatusComponent } from '../../filters/status/status.component';
import { Designation } from '../../../_core/models/designation.model';
import { DesignationComponent } from '../../filters/designation/designation.component';
import * as _moment from 'moment';
import { PrimaryAndSaleReturnService } from '../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service';
import { MatSlideToggleChange } from '@angular/material';
export interface matRangeDatepickerRangeValue<D> {
  begin: D | null;
  end: D | null;
}

@Component({
  selector: 'm-mrqty-valuewise-secondary-sale',
  templateUrl: './mrqty-valuewise-secondary-sale.component.html',
  styleUrls: ['./mrqty-valuewise-secondary-sale.component.scss']
})
export class MRQtyValuewiseSecondarySaleComponent implements OnInit {
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("employeeComponent") employeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;

  currentUser = JSON.parse(sessionStorage.currentUser);
  showDivisionFilter: boolean = false;
  mgrAnalysisForm: FormGroup;
  showProcessing: boolean = false;
  dynamicTableHeader: any = [];
  data: any = [];
  showReport: boolean = false;
  isMultipleEmp: boolean = false;
  currentDate = new Date();

  constructor(private _toastrService: ToastrService,
    private utilService: UtilsService,
    private _PrimaryAndSaleReturnService: PrimaryAndSaleReturnService,
    private _ChangeDetectorRef: ChangeDetectorRef) {
    this.mgrAnalysisForm = new FormGroup({
      designation: new FormControl('', Validators.required),
      status: new FormControl(true),
      userId: new FormControl('', Validators.required),
      dateRange: new FormControl(null, Validators.required)
    });
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      this.mgrAnalysisForm.setControl('division', new FormControl('', Validators.required));
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  getDivisionValue(val) {
    this.showReport = false;

    this.showProcessing = false;
    this.mgrAnalysisForm.patchValue({ division: val });
    //=======================Clearing Filter===================
    this.designationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.employeeComponent.setBlank();
    //===============================END================================
    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.designationComponent.getDesignationBasedOnDivisionOnlyManager(passingObj);
  }

//   getDesignationLevel(designation: Designation) {
//     this.showReport = false;
//     //=======================Clearing Filter===================
//     this.employeeComponent.setBlank();
//     this.callStatusComponent.setBlank();
//     //===============================END================================
//     this.mgrAnalysisForm.patchValue({ designation: designation.designationLevel })
//   }

//********************************************* Umesh Kumar 15-05-2020 ************************* */
  getDesignationLevel(designation: Designation) {
    if(designation.designation === "MR"){
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: this.mgrAnalysisForm.value.division
      }
      this.designationComponent.getDesignationBasedOnDivision(passingObj);
      this._toastrService.error("This report is for Managers only", "Can't Select MR");
      this.designationComponent.setBlank();
    }else{
      this.showReport = false;
      //=======================Clearing Filter===================
      this.employeeComponent.setBlank();
      this.callStatusComponent.setBlank();
      //===============================END================================
      this.mgrAnalysisForm.patchValue({ designation: designation.designationLevel })
  
  if (this.currentUser.userInfo[0].rL === 0) {
    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        designationObject: [designation],
        status: [true],
        division: this.mgrAnalysisForm.value.division,
      };
  
      this.employeeComponent.getEmployeeListBasedOnDivision(
        passingObj
      );
    } else if (this.currentUser.company.isDivisionExist == false) {
      this.employeeComponent.getEmployeeList(
        this.currentUser.companyId,
        [designation],
        [true]
      );
    }
  } else {
    let dynamicObj = {};
    if (this.currentUser.company.isDivisionExist == false) {
      dynamicObj = {
        supervisorId: this.currentUser.id,
        companyId: this.currentUser.companyId,
        status: true,
        type: "lower",
        designation: [designation.designationLevel], //desig
      };
      this.employeeComponent.getManagerHierarchy(dynamicObj);
    } else if (this.currentUser.company.isDivisionExist == true) {
      dynamicObj = {
        supervisorId: this.currentUser.id,
        companyId: this.currentUser.companyId,
        status: true,
        type: "lower",
        designation: [designation.designationLevel], //desig,
        isDivisionExist: true,
        division: this.mgrAnalysisForm.value.division,
      };
      this.employeeComponent.getManagerHierarchy(dynamicObj);
    }
  }
    }

}

  getStatus(status: boolean) {
    this.showReport = false;

    this.mgrAnalysisForm.patchValue({ status: status });
    //=======================Clearing Filter===================
    this.employeeComponent.setBlank();
    //===============================END================================
    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        designationObject: this.mgrAnalysisForm.value.designation,
        status: status,
        division: this.mgrAnalysisForm.value.division
      }
      this.employeeComponent.getEmployeeListBasedOnDivision(passingObj);
    } else {
      this.employeeComponent.getEmployeeList(this.currentUser.companyId, this.mgrAnalysisForm.value.designation, status);
    }
  }

//   getStatus(status: boolean) {
//     this.showReport = false;

//     this.mgrAnalysisForm.patchValue({ status: status });
//   }




  getEmployee(emp: string) {
    this.mgrAnalysisForm.patchValue({ userId: emp })
  }


  generateReport() {
	this.isToggled = false;
	    // if (!this.mgrAnalysisForm.valid) {
    //   this._toastrService.error("Select the required field to generate the report", "All Field required");
    //   return;
    // }
    this.showReport = false;

    this.showProcessing = true;

    let params = {
	  companyId: this.currentUser.companyId,
	  supervisorId: this.mgrAnalysisForm.value.userId,
	  fromDate: _moment(new Date(this.mgrAnalysisForm.value.dateRange.begin)).format("YYYY-MM-DD"),
      toDate: _moment(new Date(this.mgrAnalysisForm.value.dateRange.end)).format("YYYY-MM-DD")
    }

    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);

    console.log("Method Called...", this.dynamicTableHeader);
    this._PrimaryAndSaleReturnService.getMRQtySecondarySale(params).subscribe(res => {
      this.showProcessing = false;
      this.data = res;
      this.showReport = true;
      this._ChangeDetectorRef.detectChanges();
      console.log(res);
    }, err => {
      this.showProcessing = false;
      this.showReport = false;
      this._ChangeDetectorRef.detectChanges();
    });

  }

  hideTable(): void {
    this.showReport = false;
    this.showProcessing = false;
  }
}
