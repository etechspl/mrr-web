import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DCRStatusReportsComponent } from './dcrstatus-reports.component';

describe('DCRStatusReportsComponent', () => {
  let component: DCRStatusReportsComponent;
  let fixture: ComponentFixture<DCRStatusReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DCRStatusReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DCRStatusReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
