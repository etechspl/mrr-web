import { ChangeDetectorRef, Component, OnInit, ViewChild ,ElementRef} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggle, MatSlideToggleChange ,MatTableDataSource} from '@angular/material';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { ReusableTableComponent} from '../../filters/reusable-table/reusable-table.component';

import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';
import { TypeComponent } from '../../filters/type/type.component';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import * as XLSX from 'xlsx';

@Component({
  selector: 'm-dcrstatus-reports',
  templateUrl: './dcrstatus-reports.component.html',
  styleUrls: ['./dcrstatus-reports.component.scss']
})

export class DCRStatusReportsComponent implements OnInit {
  

  constructor(
    private _cdr : ChangeDetectorRef,
    private _dcrReportService : DcrReportsService
  ) { }
  @ViewChild("callReusableTableComponent") callReusableTableComponent: ReusableTableComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('TABLE',{ read: ElementRef }) table: ElementRef;

  show = false;
  showProcessing : boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist : boolean = false;
  showDivisionFilter:boolean = false;
  isToggled : boolean = false;
  showHeadquarter: boolean = false;
  employeeFilter: boolean = false;
  stateFilter: boolean = true;
   dataSource: MatTableDataSource<any>;
  // columnHeader = {'studentID': 'ID', 'fname': 'First Name', 'lname': 'Last Name', 'weight': 'Weight', 'symbol': 'Code'};
  tableData:any =[];
  // tableData=[];
  columnHeader = {'designation':'designation','employeeName':'employeeName','districtName': 'districtName',  'stateName':'stateName','dcrDate':'dcrDate','dcrNotSubmitted':'dcrNotSubmitted','dcrSubmitted':'dcrSubmitted'};
   
    dcrStatus = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    state: new FormControl(null),
    district: new FormControl(null),
    designation: new FormControl(null),
    employee: new FormControl(null),
    fromDate : new FormControl(null,Validators.required),
    toDate:new FormControl(null,Validators.required)
    
  })
  // ---------------------------mahender sharma Start code --------------------------------------------------

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase(); 
  //   this.dataSource.filter = filterValue;
  // }

  // ExportTOExcel()
  // {
  //   let data =  this.dataSource.data;
  //   console.log(data);
  //   const  ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
  //   const wb: XLSX.WorkBook = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  //   XLSX.writeFile(wb,'SheetJS.xlsx');
  //   console.log("exported");

  // }

  
  // --------------------------------------End Mahender sharma -------------------------------------------------------
  ngOnInit() {
    console.log('currentUser=> ',this.currentUser);
    

    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) {
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }

    if (this.currentUser.company.isDivisionExist === true) {
      this.isDivisionExist = true;
      this.dcrStatus.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  toggleFilter(event : MatSlideToggleChange){
    this.isToggled = event.checked
  }

  getReportType(val){
    if (val === "Geographical") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
    this.dcrStatus.patchValue({ reportType : val})
  }

  getTypeValue(val){
    console.log(val);
    this.dcrStatus.patchValue({type:val})
    if (val === 'State') {
      this.showHeadquarter = false;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callStateComponent.setBlank();
        this.callDivisionComponent.setBlank();
      }
    } else if (val === 'Headquarter') {
      this.showHeadquarter = true;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
        this.callDistrictComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDistrictComponent.setBlank();
      }
    } else if (val === 'Employee Wise') {
      this.stateFilter = false;
      this.showHeadquarter = false;
      this.employeeFilter = true;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;

        this.callEmployeeComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
      }
    }
  }

  getDivisionValue(val){
    this.dcrStatus.patchValue({division:val})
    if (this.dcrStatus.value.type == "Employee Wise") {
      //=======================Clearing Filter===================
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    } else if (this.dcrStatus.value.type == "State" || this.dcrStatus.value.type == "Headquarter") {
      //=======================Clearing Filter===================
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      //===============================END=======================
      let divisionIds = {
        division: val
      };
      this.callStateComponent.getStateBasedOnDivision(divisionIds);
    }
  }

  getStateValue(val){
    this.dcrStatus.patchValue({state:val})
    if (this.dcrStatus.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.dcrStatus.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrict(val){
    this.dcrStatus.patchValue({district:val})
  }

  getDesignation(val){
    this.dcrStatus.patchValue({designation:val})
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.dcrStatus.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    }
  }

  getEmployee(val){
    this.dcrStatus.patchValue({employee:val})
  }

  getFromDate(val){
    this.dcrStatus.patchValue({fromDate:val})
  }

  getToDate(val){
    this.dcrStatus.patchValue({toDate:val})
  }

  getRecords(){
    console.log('data',this.dcrStatus.value)
    this.showProcessing = true;
    this.isToggled = false;
    let param = {
      companyId:this.currentUser.companyId,
      fromDate:this.dcrStatus.value.fromDate,
      toDate:this.dcrStatus.value.toDate,
      //type:this.dcrStatus.value.type
    }

    if(this.dcrStatus.value.type === 'State'){
      param['stateId'] = this.dcrStatus.value.state;
      param['type'] = 'state'
    }else if(this.dcrStatus.value.type === 'Headquarter'){
      param['type'] = 'Headquarter'
      param['districtId']= this.dcrStatus.value.district;
      param['stateId'] = this.dcrStatus.value.state;
    }
    else if(this.dcrStatus.value.type === 'Employee Wise'){
      param['type'] = 'Employee'
      param['employeeId'] = this.dcrStatus.value.employee;
      param['designation'] = this.dcrStatus.value.designation;
    }

    if(this.currentUser.company.isDivisinExist === true){
      param['divisionId'] = this.dcrStatus.value.division;
    }
      // console.log(JSON.stringify(param));
      
//----------------------------------mahender sharma--------------------------------------------- 
      this._dcrReportService.getDcrStatusSummery(param).subscribe(res=>{
       this.show = true;
       this.tableData = res;    
       this._cdr.detectChanges()
    })
    

  }

}
