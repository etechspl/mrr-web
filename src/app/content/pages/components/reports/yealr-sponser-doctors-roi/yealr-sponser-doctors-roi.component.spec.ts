import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YealrSponserDoctorsRoiComponent } from './yealr-sponser-doctors-roi.component';

describe('YealrSponserDoctorsRoiComponent', () => {
  let component: YealrSponserDoctorsRoiComponent;
  let fixture: ComponentFixture<YealrSponserDoctorsRoiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YealrSponserDoctorsRoiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YealrSponserDoctorsRoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
