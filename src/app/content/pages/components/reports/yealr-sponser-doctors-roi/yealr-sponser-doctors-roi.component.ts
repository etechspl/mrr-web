import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { MonthYearService } from '../../../../../core/services/month-year.service';
declare var $;
@Component({
  selector: 'm-yealr-sponser-doctors-roi',
  templateUrl: './yealr-sponser-doctors-roi.component.html',
  styleUrls: ['./yealr-sponser-doctors-roi.component.scss']
})
export class YealrSponserDoctorsRoiComponent implements OnInit {

  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  currentUser = JSON.parse(sessionStorage.currentUser);
  SponserDoctorROIFilterForm: FormGroup;
  
  dataSourceYearly: any;
  dataSourceFinancly: any;



  public dataViewFor: string = this.currentUser.company.lables.doctorLabel;
  public showSponserDoctorsROIDetailsYearly: boolean = false;
  public showSponserDoctorsROIDetailsFinanceYearly: boolean = false;
  public showYearFilter: boolean = true;
  public showReport: boolean=false;
  public showFinancialFilter: boolean = false;
  public showDoctorColumn: boolean = true;
  showProcessing: boolean = false;

  constructor(private fb: FormBuilder,
    private _providerService: ProviderService,
    private _toastr: ToastrService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _monthYearService: MonthYearService
  ) {
    this.SponserDoctorROIFilterForm = this.fb.group({
      fiscalType: new FormControl('yearly'),
      designation: new FormControl('', Validators.required),
      status: new FormControl(null, Validators.required),
      employeeId: new FormControl(null, Validators.required),
      year: new FormControl(null, Validators.required),
      viewFor: new FormControl('RMP'),
    });
  }

  ngOnInit() {
  }
  getFiscalType(val) {
    this.showProcessing = false;
    this.showReport = false;

    this.showSponserDoctorsROIDetailsYearly = false;
    this.showSponserDoctorsROIDetailsFinanceYearly = false;
    if (val == "yearly") {
      this.showYearFilter = true;
      this.showReport = false;

      this.showFinancialFilter = false;
    } else if (val == "financialYearly") {
      this.showYearFilter = false;
      this.showReport = false;
      this.showFinancialFilter = true;
    }
    let testObj = {}
    this.SponserDoctorROIFilterForm.patchValue({ fiscalType: val });
    this.SponserDoctorROIFilterForm.patchValue({ year: null });
  }
  getDesignation(val) {
    this.showProcessing = false;

    this.showSponserDoctorsROIDetailsYearly = false;
    this.showSponserDoctorsROIDetailsFinanceYearly = false;
    this.SponserDoctorROIFilterForm.patchValue({ designation: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, this.SponserDoctorROIFilterForm.value.status);
  }
  getStatusValue(val) {
    this.showProcessing = false;

    this.showSponserDoctorsROIDetailsYearly = false;
    this.showSponserDoctorsROIDetailsFinanceYearly = false;
    this.SponserDoctorROIFilterForm.patchValue({ status: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.SponserDoctorROIFilterForm.value.designation.designationLevel, val);
  }

  getEmployeeValue(val) {
    this.showProcessing = false;

    this.showSponserDoctorsROIDetailsYearly = false;
    this.showSponserDoctorsROIDetailsFinanceYearly = false;
    this.SponserDoctorROIFilterForm.patchValue({ employeeId: val });
  }

  setTableHeading(obj) {
    this.showProcessing = false;

    this.showSponserDoctorsROIDetailsYearly = false;
    this.showSponserDoctorsROIDetailsFinanceYearly = false;
  
      this.showReport=false;

      this.showDoctorColumn = true;
  
      this.dataViewFor = this.currentUser.company.lables.doctorLabel;
    
  }
  getYear(val) {
    this.showProcessing = false;
    this.showSponserDoctorsROIDetailsYearly = false;
    this.showSponserDoctorsROIDetailsFinanceYearly = false;
    this.SponserDoctorROIFilterForm.patchValue({ year: val });
  }
  viewRecords() {
    this.showProcessing = true;
    this.showReport=false;
    this.showSponserDoctorsROIDetailsYearly = false;
    this.showSponserDoctorsROIDetailsFinanceYearly = false;

    this._providerService.getSponserDoctorsROI({'companyId':this.currentUser.companyId, 'userId':this.SponserDoctorROIFilterForm.value.employeeId, 'fiscalYear':this.SponserDoctorROIFilterForm.value.fiscalType, 'year':this.SponserDoctorROIFilterForm.value.year,'designationLevel':this.SponserDoctorROIFilterForm.value.designation.designationLevel}).subscribe(res => {
      if (res.length == 0 || res == undefined) {
        this.showProcessing = false;
        this.showSponserDoctorsROIDetailsYearly = false;
        this.showSponserDoctorsROIDetailsFinanceYearly = false;
        this._toastr.info('For Selected Filter data not found', 'Data Not Found');
      } else {
        if (this.SponserDoctorROIFilterForm.value.fiscalType == "financialYearly") {
          this.dataSourceFinancly = res;
          this.showProcessing = false;
          this.showReport=true;
          let ProStatus=true;
        
          this.dtOptions = {
            destroy: true,
            data: this.dataSourceFinancly,
            columns: [
              {
                title: 'State Name',
                data: 'stateName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.hqLabel,
                data: 'districtName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.areaLabel,
                data: 'areaName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              // {
              //   title: this.currentUser.company.lables.areaLabel+" Current Status",
              //   data: 'areaStatus',
              //   render: function (data) {
              //     if (data === '' || data == "null") {
              //       return '---'
              //     } else {
              //       return data
              //     }
              //   },
              //   defaultContent: '---'
              // },
              {
                title: this.dataViewFor+" Name",
                data: 'providerName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              // {
              //   title: this.dataViewFor+" Code",
              //   data: 'providerCode',
              //   render: function (data) {

              //     if (data === '' || data == "null") {
              //       return '---'
              //     } else {
              //       return data
              //     }
              //   },
              //   defaultContent: '---'
              // },
              {
                title: 'category',
                data: 'Category',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'specialization',
                data: 'Specialization',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'degree',
                data: 'Degree',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.dataViewFor+' Current Status',
                data: 'status',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'April',
                data: 'aprData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'May',
                data: 'mayData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'June',
                data: 'junData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'July',
                data: 'julData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'August',
                data: 'augData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'September',
                data: 'septData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'October',
                data: 'octData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'November',
                data: 'novData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'December',
                data:'decData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Jan',
                data:'janData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Feb',
                data: 'febData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'March',
                data: 'marData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Total',
                data: 'tot',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }
            ],
            footer:[120],
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'print'
            ]
          };
          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);

        } else if (this.SponserDoctorROIFilterForm.value.fiscalType == "yearly") {
          this.dataSourceYearly = res;
          this.showProcessing = false;
          this.showReport=true;
          this.dtOptions = {
            destroy: true,
            data: this.dataSourceYearly,
            columns: [
              {
                title: 'State Name',
                data: 'stateName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.hqLabel,
                data: 'districtName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.areaLabel,
                data: 'areaName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              // {
              //   title: this.currentUser.company.lables.areaLabel + " Current Status",
              //   data: 'areaStatus',
              //   render: function (data) {
              //     if (data === '' || data == "null") {
              //       return '---'
              //     } else {
              //       return data
              //     }
              //   },
              //   defaultContent: '---'
              // },
              {
                title: this.dataViewFor+" Name",
                data: 'providerName',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              // {
              //   title: this.dataViewFor+" Code",
              //   data: 'providerCode',
              //   render: function (data) {
              //     if (data === '' || data == "null") {
              //       return '---'
              //     } else {
              //       return data
              //     }
              //   },
              //   defaultContent: '---'
              // },
              {
                title: 'category',
                data: 'Category',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'specialization',
                data: 'Specialization',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'degree',
                data: 'Degree',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.dataViewFor+' Current Status',
                data: 'status',
                render: function (data) {
                  if (data === '' || data == "null") {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Jan',
                data: 'janData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Feb',
                data: 'febData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'March',
                data: 'marData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'April',
                data: 'aprData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
               
              },
              {
                title: 'May',
                data: 'mayData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'June',
                data: 'junData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'July',
                data: 'julData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'August',
                data: 'augData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'September',
                data:'septData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'October',
                data:'octData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'November',
                data: 'novData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'December',
                data: 'decData',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Total',
                data: 'tot',
                render: function (data) {
                  if (data.length ==='' || data == "null") {
                    return '0'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }
            ],
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'print'
            ]
          };
          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
        }

       
      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      console.log(err)
    });
  }
}
