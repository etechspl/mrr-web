import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftIssueReportDateWiseComponent } from './gift-issue-report-date-wise.component';

describe('GiftIssueReportDateWiseComponent', () => {
  let component: GiftIssueReportDateWiseComponent;
  let fixture: ComponentFixture<GiftIssueReportDateWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftIssueReportDateWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftIssueReportDateWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
