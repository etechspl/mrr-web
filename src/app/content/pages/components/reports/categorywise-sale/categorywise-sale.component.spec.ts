import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorywiseSaleComponent } from './categorywise-sale.component';

describe('CategorywiseSaleComponent', () => {
  let component: CategorywiseSaleComponent;
  let fixture: ComponentFixture<CategorywiseSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorywiseSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorywiseSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
