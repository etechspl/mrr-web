import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DCRConsolidateSummaryComponent } from './dcrconsolidate-summary.component';

describe('DCRConsolidateSummaryComponent', () => {
  let component: DCRConsolidateSummaryComponent;
  let fixture: ComponentFixture<DCRConsolidateSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DCRConsolidateSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DCRConsolidateSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
