import { Router } from "@angular/router";
import { UserCompleteDCRInfoComponent } from "./../user-complete-dcrinfo/user-complete-dcrinfo.component";
import { TypeComponent } from "../../filters/type/type.component";
import { StateComponent } from "../../filters/state/state.component";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";
import { MonthComponent } from "../../filters/month/month.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { DistrictComponent } from "../../filters/district/district.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
	Component,
	OnInit,
	ViewChild,
	ChangeDetectorRef,
	ChangeDetectionStrategy,
	ElementRef,
	SimpleChange,
	SimpleChanges,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { DivisionComponent } from "../../filters/division/division.component";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import * as _moment from "moment";
declare var $;
import Swal from "sweetalert2";
import { DCRBackupService } from "../../../../../core/services/DCRBackup/dcrbackup.service";
import { DCRProviderVisitDetailsBackupService } from "../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service";
import { StatusComponent } from "../../filters/status/status.component";
import { HierarchyService } from "../../../../../core/services/hierarchy-service/hierarchy.service";
import { ActivatedRoute } from "@angular/router";
import { URLService } from "../../../../../core/services/URL/url.service";
import { MatSlideToggleChange } from "@angular/material";
import { FromdateComponent } from "../../filters/fromdate/fromdate.component";
import { ExpenseClaimedService } from "../../../../../core/services/expense-claimed.service";
import { Session } from "protractor";
import { url } from "inspector";

@Component({
	selector: "m-dcrconsolidate-summary",
	templateUrl: "./dcrconsolidate-summary.component.html",
	styleUrls: ["./dcrconsolidate-summary.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DCRConsolidateSummaryComponent implements OnInit {
	public showState: boolean = true;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	public showDateWiseDCRReport: boolean = false;
	public showProcessing: boolean = false;

	logoURL: any;
	count = 0;
	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId1", // the id of html/table element
	};
	//dcrForm: FormGroup;
	displayedColumns = [
		"UserName",
		"UserDesignation",
		"totalDCR",
		"RMPCount",
		"doctorPOB",
		"DrugStoreCount",
		"vendorPOB",
		"userId",
	];
	dataSource;
	dataSources;
	dataSources1;
	isDataSources1Empty = false;
	statuscheck = 1;
	managerTeam;
	onlyManager;

	showAdminAndMGRLevelFilter = false;

	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("setStatus") setStatus: StatusComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callUserCompleteDCR")
	callUserCompleteDCR: UserCompleteDCRInfoComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;

	@ViewChild("dataTable") table;
	@ViewChild("dataTableForDCRDateWise") dataTableForDCRDateWise;
	dataTable: any;
	dtOptions: any;
	//dataTableForDCRDateWise: any;
	dtOptionsForDCRDateWise: any;

	constructor(
		private _dcrReportsService: DcrReportsService,
		private _changeDetectorRef: ChangeDetectorRef,
		private toastr: ToastrService,
		private router: Router,
		private _dcrReportsServiceBackup: DCRBackupService,
		private DCRProviderVisitDetailsBackupService: DCRProviderVisitDetailsBackupService,
		private exportAsService: ExportAsService,
		private _hierarchyService: HierarchyService,
		private activateRoute: ActivatedRoute,
		private _urlService: URLService,
		private _expenseClaimedService: ExpenseClaimedService
	) {}

	isShowDivision = false;
	currentUser = JSON.parse(sessionStorage.currentUser);
	otherTypeLablesExist = this.currentUser.company.lables.otherType
		? true
		: false;
	isPOBExist = this.currentUser.company.validation.pob;
	colspanValue;
	isDivisionExist = this.currentUser.company.isDivisionExist;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	companyId = this.currentUser.companyId;
	dcrForm = new FormGroup({
		reportType: new FormControl("Geographical"),
		type: new FormControl(null, Validators.required),
		status: new FormControl([true]),
		stateInfo: new FormControl(null),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		month: new FormControl(null),
		year: new FormControl(null),
		designation: new FormControl(null),
		toDate: new FormControl(null, Validators.required),
		fromDate: new FormControl(null, Validators.required),
	});
	check: boolean = false;
    noOfDays=0;
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}

	getReportType(val) {		
		if (val === "Geographical") {
			this.showState = true;
			this.showEmployee = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			this.showState = false;
			this.showDistrict = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
	}
	getTypeValue(val) {
		this.showReport = false;
		if (val == "State") {
			this.showDistrict = false;
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.callStateComponent.setBlank();
			this.dcrForm.setControl(
				"stateInfo",
				new FormControl(null, Validators.required)
			);
			this.dcrForm.removeControl("employeeId");
			this.dcrForm.removeControl("designation");
			this.dcrForm.removeControl("districtInfo");
		} else if (val == "Headquarter") {
			this.callStateComponent.setBlank();
			this.dcrForm.removeControl("employeeId");
			this.dcrForm.setControl(
				"stateInfo",
				new FormControl(null, Validators.required)
			);
			this.dcrForm.setControl(
				"districtInfo",
				new FormControl(null, Validators.required)
			);
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.dcrForm.removeControl("designation");
			this.showDistrict = true;
		} else if (val == "Employee Wise") {
			this.showEmployee = true;
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.dcrForm.removeControl("stateInfo");
			this.dcrForm.removeControl("districtInfo");
			this.dcrForm.setControl(
				"designation",
				new FormControl(null, Validators.required)
			);
			this.dcrForm.setControl(
				"employeeId",
				new FormControl(null, Validators.required)
			);
		} else if (val == "Self") {
			this.showEmployee = false;
		}

		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
		} else {
			this.isShowDivision = false;
		}
		this.dcrForm.patchValue({ type: val });
		this._changeDetectorRef.detectChanges();
	}
	getStateValue(val) {
		this.dcrForm.patchValue({ stateInfo: val });
		console.log(this.dcrForm.patchValue({ stateInfo: val }))
		if (this.isDivisionExist === true) {
			if (this.dcrForm.value.type == "Headquarter") { 
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.designationLevel,
					companyId: this.companyId,
					stateId: this.dcrForm.value.stateInfo,
					isDivisionExist: this.isDivisionExist,
					division: this.dcrForm.value.divisionId,
				});

				//this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
			}
		} else {
			if (this.dcrForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
	}
	getDistrictValue(val) {
		this.dcrForm.patchValue({ districtInfo: val });
	}
	getEmployeeValue(val) {
		this.dcrForm.patchValue({ employeeId: val });
	}
	getFromDate(val) {
		this.dcrForm.patchValue({ fromDate: val });
	}
	getToDate(val) {
		this.dcrForm.patchValue({ toDate: val });
	}

	getDesignation(val) {
		this.dcrForm.patchValue({ designation: val });
		if (this.isDivisionExist === true) {
			this.callEmployeeComponent.getEmployeeListBasedOnDivision({
				companyId: this.companyId,
				division: this.dcrForm.value.divisionId,
				status: [true],
				designationObject: this.dcrForm.value.designation,
			});
		} else {
			this.callEmployeeComponent.getEmployeeList(
				this.currentUser.companyId,
				val,
				[true]
			);
		}
	}
	//setting division into dcrform
	getDivisionValue($event) {
		this.dcrForm.patchValue({ divisionId: $event });
		if (this.isDivisionExist === true) {
			if (
				this.dcrForm.value.type === "State" ||
				this.dcrForm.value.type === "Headquarter"
			) {
				this.callStateComponent.getStateBasedOnDivision({
					companyId: this.currentUser.companyId,
					isDivisionExist: this.isDivisionExist,
					division: this.dcrForm.value.divisionId,
					designationLevel: this.currentUser.userInfo[0]
						.designationLevel,
				});
			}

			if (this.dcrForm.value.type === "Employee Wise") {
				//getting employee if we change the division filter after select the designation
				if (
					this.dcrForm.value.divisionId != null &&
					this.dcrForm.value.designation != null
				) {
					if (this.dcrForm.value.designation.length > 0) {
						this.callEmployeeComponent.getEmployeeListBasedOnDivision(
							{
								companyId: this.companyId,
								division: this.dcrForm.value.divisionId,
								status: [true],
								designationObject: this.dcrForm.value
									.designation,
							}
						);
					}
				}
			}
		}
	}
	getSumByKey(key: string) {
		const total = (this.dataSources || []).reduce(
			(accumulator, currentValue) => {
				return accumulator + currentValue[key];
			},
			0
		);

		return total;
	}
	getCountByStatus(status: string) {
		const total = (this.dataSources || []).reduce(
			(accumulator, currentValue) => {
				if(status=="Working"){
				if(currentValue.act=="camp" ||currentValue.act=="saturday" ||currentValue.act=="WHFD" ){
					this.noOfDays += 0.5
				}
			}else{
				this.noOfDays=0;
			}
				return currentValue.workingStatus == status
					? accumulator + 1
					: accumulator;
			},
			0
		);
		return total+this.noOfDays;
	}

	getJointWork() {
		const dcvVisited = (this.dataSources || []).reduce(
			(accumulator, currentValue) => {
				if (currentValue.jointWork.length)
					accumulator.push(
						currentValue.jointWork.map((i) => ({
							...i,
							date: currentValue.dcrDate,
						}))
					);
				return accumulator;
			},
			[]
		);
		let workers = []
			.concat(...dcvVisited)
			.reduce((accumulator, currentValue) => {
				const worker = accumulator.find(
					(w) => w.Name == currentValue.Name
				);
				if (worker) {
					worker.date.push(currentValue.date);
					// TODO: no. of calls
					// worker.NoOfcalls.push(currentValue.date)
				} else {
					accumulator.push({
						...currentValue,
						date: [currentValue.date],
						// Todo: no. of calls
					});
				}
				return accumulator;
			}, []);
		// const jointWorkDate=[].concat(dcvVisited.dcrDate)
		const avg = workers.length / (dcvVisited.length || 1);
		return { dcvVisited: dcvVisited.length, workers, avg }; //total
	}

	getJointWorkdate() {
		const dcvVisited = (this.dataSources || []).reduce(
			(accumulator, currentValue) => {
				if (currentValue.jointWork.length)
					accumulator.push(currentValue.dcrDate);
				return accumulator;
			},
			[]
		);
		return dcvVisited;
	}

	getkeysCount(status: string) {
		const total = (this.dataSources || []).reduce(
			(accumulator, currentValue) => {
				return currentValue.hasOwnProperty("deviateReason") == true
					? accumulator + 1
					: accumulator;
			},
			0
		);
		return total;
	}
	getAverage(status: string, dcrCount: number) {
		var count=0
		const total = (this.dataSources || []).reduce(
			(accumulator, currentValue) => {
				
				if(currentValue.DCRStatus==1 && !currentValue.hasOwnProperty('act') ){
				  this.noOfDays += parseInt('' + currentValue.DCRStatus);
				}else if(currentValue.act=="camp" ||currentValue.act=="saturday" ||currentValue.act=="WHFD"  ){
					this.noOfDays += 0.5
				}
				else if(currentValue.act=="working"  ){
					this.noOfDays += parseInt('' + currentValue.DCRStatus);
				}

				return accumulator + currentValue[status];

				
			},
			this.noOfDays=0 
		);
		
		// total/dcrCount;
		return (total / this.noOfDays).toFixed(2);
	}
	getStatusValue(event: any) {
		this.dcrForm.patchValue({ status: event });
	}
	peterSergical:boolean=false;
	ngOnInit() {
		if(this.currentUser.companyId == '5e1d5b216dede22577e5046d'){
			this.peterSergical=true;
		}
		this.logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;
		//this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
		this.colspanValue = 9;
		// this.colspanValue = (this.isPOBExist === false )? 8 : 7;
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.dcrForm.setControl("divisionId", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			if (this.currentUser.userInfo[0].rL > 1) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}

		if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			this.showAdminAndMGRLevelFilter = true;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		}
	}

	showDCRSummary = true;

	hideColumn = false;

	viewRecords() {
		
		this.isToggled = false;
		this.showReport = false;
		this.showDateWiseDCRReport = false;
		this.showDCRSummary = true;
		this.showProcessing = true;
		if (this.dcrForm.value.employeeId == null) {
			this.dcrForm.value.employeeId = [this.currentUser.id];
		}
		//this.currentUser.companyId, this.dcrForm.value.type, this.dcrForm.value.stateInfo, this.dcrForm.value.districtInfo,
		//this.dcrForm.value.employeeId, this.dcrForm.value.fromDate, this.dcrForm.value.toDate
		let params = {};
		if (this.isDivisionExist === true) {
			params["division"] = this.dcrForm.value.divisionId;
		} else {
			params["division"] = [];
		}

		params["isDivisionExist"] = this.isDivisionExist;
		params["companyId"] = this.companyId;
		params["stateIds"] = this.dcrForm.value.stateInfo;
		params["districtIds"] = this.dcrForm.value.districtInfo;
		params["type"] = this.dcrForm.value.type;
		params["employeeId"] = this.dcrForm.value.employeeId;
		params["fromDate"] = this.dcrForm.value.fromDate;
		params["toDate"] = this.dcrForm.value.toDate;
		params["status"] = this.dcrForm.value.status;

		this._dcrReportsService.getEmployeeWiseDCRDetails(params).subscribe(
			(res) => {
				if (res.length == 0 || res == undefined) {
					this.showReport = false;
					this.showDateWiseDCRReport = false;

					this.showProcessing = false;
					this.toastr.info("No Record Found.");
				} else {
					this.dataSource = res;
					this.showProcessing = false;
					this.showReport = true;
					let row = 0;
				
					res.map((item, index) => {
						item["index"] = index;
						item["totalPOB"] = item.drugPOB+ item.doctorPOB;
						item["InPersonRMPCount"] = item.RMPCount-(item.phoneOrderRMPCount+item.digitalMarketingRMPCount);
					});

					const PrintTableFunction = this.PrintTableFunction.bind(
						this
					);
					let otherArray = [
						{
							title: "Default Col",
							data: "key",
							render: function (data) {
								if (data === "") {
									return "---";
								} else {
									return data;
								}
							},
							defaultContent: "---",
						},
					];

					if (this.otherTypeLablesExist) {
						// otherArray.pop();
						this.currentUser.company.lables.otherType.forEach(
							(element) => {
								otherArray.push({
									title: element.label + " Met Count",
									data: element.key,
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								});
							}
						);
					}

					this.dtOptions = {
						pagingType: "full_numbers",
						ordering: true,
						info: true,
						scrollY: 300,
						scrollX: true,
						scrollCollapse: true,
						paging: false,
						fixedColumns: {
							leftColumns: 0,
							rightColumns: 1,
						},
						destroy: true,
						data: res,
						responsive: true,

						columns: [
							{
								title: "S. No.",
								data: "index",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data + 1;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Division",
								data: "divisionName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Headquarter",
								data: "headquarter",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Employee Code",
								data: "employeeCode",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Employee Name ",
								data: "UserName",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Manager Name ",
								data: "managerName",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							
							{
								title: "Designation",
								data: "UserDesignation",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Date Of Joining",
								data: "dateOfJoining",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return _moment(data).format(
											"ddd, MMM DD, YYYY"
										);
									}
								},
								defaultContent: "---",
							},
							{
								title: "Mobile",
								data: "mobile",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Last Submitted DCR",
								data: "lastDCRDate",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return _moment(data).format(
											"ddd, MMM DD, YYYY"
										);
									}
								},
								defaultContent: "---",
							},
							{
								title: "Total DCR",
								data: "totalDCR",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title:
									this.currentUser.company.lables
										.doctorLabel + " Met Count",
								data: "RMPCount",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							
							{
								title:
								" Digital Meeting "+	this.currentUser.company.lables
										.doctorLabel + " Count",
								data: "phoneOrderRMPCount",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title:
								" Over the Call "+	this.currentUser.company.lables
										.doctorLabel  +"Meeting Count",
								data: "digitalMarketingRMPCount",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							
							{
								title:
								" In Person "+	this.currentUser.company.lables
										.doctorLabel  +" Call Count",
								data: "InPersonRMPCount",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title:
									this.currentUser.company.lables
										.doctorLabel + " POB",
								data: "doctorPOB",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title:
									this.currentUser.company.lables
										.vendorLabel + " Met Count",
								data: "DrugStoreCount",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title:
									this.currentUser.company.lables
										.stockistLabel + " Met Count",
								data: "stockistCount",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							...otherArray,

							{
								title:
									this.currentUser.company.lables
										.doctorLabel +
									" / " +
									this.currentUser.company.lables
										.vendorLabel +
									" POB",
								data: "totalPOB",
							
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							
							
							{
								title: "View DCR Detail",
								defaultContent:
									"<button class='btn' style='padding: 1px 2px; background: transparent;color: #f44336;font-weight: 600;'><i class='la la-eye'></i>&nbsp;&nbsp;View DCR Detail</button>",
							},
						],
						rowCallback: (
							row: Node,
							data: any[] | Object,
							index: number
						) => {
							const self = this;
							// Unbind first in order to avoid any duplicate handler
							// (see https://github.com/l-lin/angular-datatables/issues/87)
							$("td", row).unbind("click");
							$("td .getDateWiseReport", row).bind(
								"click",
								() => {
									//console.log("row : ", data);
									let rowObject = JSON.parse(
										JSON.stringify(data)
									);
									self.getDateWise(rowObject.userId);
									this._changeDetectorRef.detectChanges();
								}
							);
							$("td button", row).bind("click", () => {
								//console.log("row : ", data);
								let rowObject = JSON.parse(
									JSON.stringify(data)
								);
								self.getDateWise(rowObject.userId);
							});
							return row;
						},
						language: {
							search: "_INPUT_",
							searchPlaceholder: "Search Records",
						},
						dom: "Bfrtip",
						buttons: [
							{
								extend: "csv",
								exportOptions: {
									columns: ":visible",
								},
							},

							//********************* UMESH 19-03-2020 ************************ */
							{
								extend: "print",
								title: function () {
									return "State List";
								},
								exportOptions: {
									columns: ":visible",
								},
								action: function (e, dt, node, config) {
									PrintTableFunction(res);
								},
							},

							// {
							//             extend: 'print',
							//             exportOptions: {
							//               stripHtml: true,
							//               columns: ':visible'
							//             },
							//             customize: (win) => {
							//               const currentDateAndTime = _moment(new Date()).format('DD/MM/YYYY, dddd , HH:mm A');
							//               const designation = this.currentUser.designation;
							//               var css = '@page { size: landscape; }',
							//                 head = win.document.head || win.document.getElementsByTagName('head')[0],
							//                 style = win.document.createElement('style');
							//               style.type = 'text/css';
							//               style.media = 'print';

							//               if (style.styleSheet) {
							//                 style.styleSheet.cssText = css;
							//               }
							//               else {
							//                 style.appendChild(win.document.createTextNode(css));
							//               }
							//               head.appendChild(style);
							//               $(win.document.body)
							//                 .css('font-size', '8px')
							//                 .prepend(
							//                   `
							//                   <div>
							//                   <img src='${this.logoURL}' style="float:left; width:200px;height:50px;margin-right:5px;" />;
							//                    <h2 style="float:right; margin-right:5px">User Details</h2>
							//                   </div>

							//                   <div style="text-align:right; display: flex;  justify-content: space-between;  position: relative; ">
							//                   <p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>
							//                   <p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
							//                   </div>
							//                   `
							//                 );
							//               $(win.document.body).find('table')
							//                 .addClass('compact')
							//                 .css('font-size', 'inherit', 'direction', 'rtl');
							//             }
							//           }
							//********************End ********/
						],
					};

					if (this.isDivisionExist === false) {
						this.dtOptions.columns[1].visible = false;
					}
					if(this.peterSergical==false){
						this.dtOptions.columns[11].visible = false;
						this.dtOptions.columns[12].visible = false;//-----Hiding from company other than Peter Sergical
						this.dtOptions.columns[13].visible = false;
					}

					// if (this.isPOBExist == false) {
					//   this.dtOptions.columns[15].visible = false;
					// }
				

					// ========= Rahul Saini 17-05-2020 Prevent buttons to print in excel ==========
					if (this.isDivisionExist === true) {
						if (!this.otherTypeLablesExist) {
							if (this.isPOBExist == false) {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											1,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
											14,
											15,
											16,
											17,
											18
										],
									},
								});
							} else {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											1,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
											
											14,
											15,
											16,
											17,
											18
										],
									},
								});
							}
						} else {
							if (this.isPOBExist == false) {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											1,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
											14,
											15,
											
											16,
											17,
											18
										],
									},
								});
							} else {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											1,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
											14,
											15,
											
											16,
											17,
											18
										],
									},
								});
							}
						}
					} else {
						if (!this.otherTypeLablesExist) {
							if (this.isPOBExist == false) {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
											14,
											15,
											16,
											17,
											18
										],
									},
								});
							} else {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
										
											14,
											15,
											16,
											17,
											18
										],
									},
								});
							}
						} else {
							if (this.isPOBExist == false) {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
											14,
											15,
											16,
											17,
											18
										],
									},
								});
							} else {
								this.dtOptions.buttons.push({
									extend: "excel",
									exportOptions: {
										columns: [
											0,
											2,
											3,
											4,
											5,
											6,
											7,
											8,
											9,
											10,
											11,
											12,
											13,
											
											14,
											15,
											16,
											17,
											18
										],
									},
								});
							}
						}
					}
					// ========= End ============================================================

					this._changeDetectorRef.detectChanges();

					this.dataTable = $(this.table.nativeElement);
					this.dataTable.DataTable(this.dtOptions);
					this._changeDetectorRef.detectChanges();
					// this.showDateWiseDCRReport=true;
				}
				this._changeDetectorRef.detectChanges();
			},
			(err) => {
				this.showReport = false;
				this.showDateWiseDCRReport = false;
				this.showProcessing = false;
				this.toastr.info("No Record Found.");
				this._changeDetectorRef.detectChanges();
			}
		);
	}

	getDateWise(val) {
		this.getEmployeeDCRDetail(
			val,
			this.dcrForm.value.fromDate,
			this.dcrForm.value.toDate
		);
	}
	//----delete dcr by preeti arora 12-10-2019----------------

	deleteDCR(val) {
		let msg = "The DCR Will Be Deleted.";
		let buttonMsg = "Yes, Delete it !!!";
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: "btn btn-success",
				cancelButton: "btn btn-danger",
			},
			buttonsStyling: false,
		});
		swalWithBootstrapButtons
			.fire({
				title: "Are you sure?",
				text: msg,
				type: "warning",
				showCancelButton: true,
				confirmButtonText: buttonMsg,
				cancelButtonText: "No, cancel!",
				reverseButtons: true,
				allowOutsideClick: false,
			})
			.then((result) => {
				if (result.value) {
					let where = {
						dcrId: val.dcrId,
					};
					this._expenseClaimedService
						.getExpenseClaimedByDcrId(where)
						.subscribe((expenseClaimedRes) => {
							if (expenseClaimedRes.length > 0) {
								this._expenseClaimedService
									.deleteExpenseClaimed(
										expenseClaimedRes[0].id
									)
									.subscribe((deleteRes) => {
										this._dcrReportsService
											.getDCRCopy(val.dcrId)
											.subscribe(
												(copyDcrObj) => {
													let copyObj = {
														companyId:
															copyDcrObj[0]
																.companyId,
														dcrDate:
															copyDcrObj[0]
																.dcrDate,
														stateId:
															copyDcrObj[0]
																.stateId,
														districtId:
															copyDcrObj[0]
																.districtId,
														visitedBlock:
															copyDcrObj[0]
																.visitedBlock,
														workingStatus:
															copyDcrObj[0]
																.workingStatus,
														jointWork:
															copyDcrObj[0]
																.jointWork,
														remarks:
															copyDcrObj[0]
																.remarks,
														timeIn:
															copyDcrObj[0]
																.timeIn,
														timeOut:
															copyDcrObj[0]
																.timeOut,
														createdAt:
															copyDcrObj[0]
																.createdAt,
														updatedAt:
															copyDcrObj[0]
																.updatedAt,
														submitBy:
															copyDcrObj[0]
																.submitBy,
														reportedFrom:
															copyDcrObj[0]
																.reportedFrom,
														geoLocation:
															copyDcrObj[0]
																.geoLocation,
														dcrSubmissionDate:
															copyDcrObj[0]
																.dcrSubmissionDate,
														callModifiedDate:
															copyDcrObj[0]
																.callModifiedDate,
														DCRStatus:
															copyDcrObj[0]
																.DCRStatus,
														dcrVersion:
															copyDcrObj[0]
																.dcrVersion,
														dcrExpense:
															copyDcrObj[0]
																.dcrExpense,
														isDayPlan:
															copyDcrObj[0]
																.isDayPlan,
														ipAddress:
															copyDcrObj[0]
																.ipAddress,
														dcrId: copyDcrObj[0].id,
														deletedAt: new Date(),
													};
													this._dcrReportsServiceBackup
														.submitDCRCopy(copyObj)
														.subscribe(
															(result) => {
																this.DCRProviderVisitDetailsBackupService.getDCRProviderVisitCopy(
																	val.dcrId
																).subscribe(
																	(
																		DCRCopyResult
																	) => {
																		this.DCRProviderVisitDetailsBackupService.submitDCRProviderVisitCopy(
																			DCRCopyResult
																		).subscribe(
																			(
																				result
																			) => {
																				this._dcrReportsService
																					.deleteDCR(
																						val.dcrId
																					)
																					.subscribe(
																						(
																							result
																						) => {
																							this.getDateWise(
																								val.userId
																							);
																							this.toastr.success(
																								"DCR has been Deleted Successfully !!!"
																							);
																						},
																						(
																							err
																						) => {
																							console.log(
																								err
																							);
																							this.showDateWiseDCRReport = false;
																							this.showProcessing = false;
																							this.toastr.error(
																								"There is some issue in DCR 1 !!"
																							);
																							// alert("Oooops! \nNo Record Found");
																							this._changeDetectorRef.detectChanges();
																						}
																					);
																			},
																			(
																				err
																			) => {
																				console.log(
																					err
																				);
																				this.showDateWiseDCRReport = false;
																				this.showProcessing = false;
																				this.toastr.error(
																					"There is some issue in DCR  2!!"
																				);
																				// alert("Oooops! \nNo Record Found");
																				this._changeDetectorRef.detectChanges();
																			}
																		);
																	},
																	(err) => {
																		console.log(
																			err
																		);
																		this.showDateWiseDCRReport = false;
																		this.showProcessing = false;
																		this.toastr.error(
																			"There is some issue in DCR 3 !!"
																		);
																		// alert("Oooops! \nNo Record Found");
																		this._changeDetectorRef.detectChanges();
																	}
																);
															},
															(err) => {
																console.log(
																	err
																);
																this.showDateWiseDCRReport = false;
																this.showProcessing = false;
																this.toastr.error(
																	"There is some issue in DCR 4 !!"
																);
																// alert("Oooops! \nNo Record Found");
																this._changeDetectorRef.detectChanges();
															}
														);
												},
												(err) => {
													console.log(err);
													this.showDateWiseDCRReport = false;
													this.showProcessing = false;
													this.toastr.error(
														"There is some issue in DCR 5!!"
													);
													// alert("Oooops! \nNo Record Found");
													this._changeDetectorRef.detectChanges();
												}
											);
									});
							} else {
								this._dcrReportsService
									.getDCRCopy(val.dcrId)
									.subscribe(
										(copyDcrObj) => {
											let copyObj = {
												companyId:
													copyDcrObj[0].companyId,
												dcrDate: copyDcrObj[0].dcrDate,
												stateId: copyDcrObj[0].stateId,
												districtId:
													copyDcrObj[0].districtId,
												visitedBlock:
													copyDcrObj[0].visitedBlock,
												workingStatus:
													copyDcrObj[0].workingStatus,
												jointWork:
													copyDcrObj[0].jointWork,
												remarks: copyDcrObj[0].remarks,
												timeIn: copyDcrObj[0].timeIn,
												timeOut: copyDcrObj[0].timeOut,
												createdAt:
													copyDcrObj[0].createdAt,
												updatedAt:
													copyDcrObj[0].updatedAt,
												submitBy:
													copyDcrObj[0].submitBy,
												reportedFrom:
													copyDcrObj[0].reportedFrom,
												geoLocation:
													copyDcrObj[0].geoLocation,
												dcrSubmissionDate:
													copyDcrObj[0]
														.dcrSubmissionDate,
												callModifiedDate:
													copyDcrObj[0]
														.callModifiedDate,
												DCRStatus:
													copyDcrObj[0].DCRStatus,
												dcrVersion:
													copyDcrObj[0].dcrVersion,
												dcrExpense:
													copyDcrObj[0].dcrExpense,
												isDayPlan:
													copyDcrObj[0].isDayPlan,
												ipAddress:
													copyDcrObj[0].ipAddress,
												dcrId: copyDcrObj[0].id,
												deletedAt: new Date(),
											};
											this._dcrReportsServiceBackup
												.submitDCRCopy(copyObj)
												.subscribe(
													(result) => {
														this.DCRProviderVisitDetailsBackupService.getDCRProviderVisitCopy(
															val.dcrId
														).subscribe(
															(DCRCopyResult) => {
																this.DCRProviderVisitDetailsBackupService.submitDCRProviderVisitCopy(
																	DCRCopyResult
																).subscribe(
																	(
																		result
																	) => {
																		this._dcrReportsService
																			.deleteDCR(
																				val.dcrId
																			)
																			.subscribe(
																				(
																					result
																				) => {
																					this.getDateWise(
																						val.userId
																					);
																					this.toastr.success(
																						"DCR has been Deleted Successfully !!!"
																					);
																				},
																				(
																					err
																				) => {
																					console.log(
																						err
																					);
																					this.showDateWiseDCRReport = false;
																					this.showProcessing = false;
																					this.toastr.error(
																						"There is some issue in DCR 1 !!"
																					);
																					// alert("Oooops! \nNo Record Found");
																					this._changeDetectorRef.detectChanges();
																				}
																			);
																	},
																	(err) => {
																		console.log(
																			err
																		);
																		this.showDateWiseDCRReport = false;
																		this.showProcessing = false;
																		this.toastr.error(
																			"There is some issue in DCR  2!!"
																		);
																		// alert("Oooops! \nNo Record Found");
																		this._changeDetectorRef.detectChanges();
																	}
																);
															},
															(err) => {
																console.log(
																	err
																);
																this.showDateWiseDCRReport = false;
																this.showProcessing = false;
																this.toastr.error(
																	"There is some issue in DCR 3 !!"
																);
																// alert("Oooops! \nNo Record Found");
																this._changeDetectorRef.detectChanges();
															}
														);
													},
													(err) => {
														console.log(err);
														this.showDateWiseDCRReport = false;
														this.showProcessing = false;
														this.toastr.error(
															"There is some issue in DCR 4 !!"
														);
														// alert("Oooops! \nNo Record Found");
														this._changeDetectorRef.detectChanges();
													}
												);
										},
										(err) => {
											console.log(err);
											this.showDateWiseDCRReport = false;
											this.showProcessing = false;
											this.toastr.error(
												"There is some issue in DCR 5!!"
											);
											// alert("Oooops! \nNo Record Found");
											this._changeDetectorRef.detectChanges();
										}
									);
							}
						});
				}
			});
	}
	//------------------end--------------------

	inpersonVisit: any;
	getEmployeeDCRDetail(userId, fromDate, toDate) {
		this.showDateWiseDCRReport = false;
		this.showDCRSummary = true;
		this.showProcessing = true;
		this._dcrReportsService
			.getUserDateWiseDCRDetail(userId, fromDate, toDate)
			.subscribe(
				(result) => {
					

					
					const obj = {
						supervisorId: userId,
						companyId: this.currentUser.companyId,
						status: true,
						type: "upper",
					};

					let sum=0;
					let i;
					for(i=0; i<result.length; i++){
						sum = sum + (result[i].RMPCount - (result[i].digitalMarketingRMPCount + result[i].phoneOrderRMPCount));
					}
					this.inpersonVisit=sum;
					
					//-----------------------------get joint working details by preeti----------
					this._dcrReportsService
						.getUserJointworkDetails(userId, fromDate, toDate)
						.subscribe(
							(JointWorkResult) => {
								this.dataSources1 = JointWorkResult;
								this.isDataSources1Empty = this.dataSources1
									.length
									? true
									: false;
							},
							(err) => {
								console.log(err);
								this._changeDetectorRef.detectChanges();
							}
						);

					//-----------------------end-----------------------------------------//
					if (result == undefined) {
						this.showDateWiseDCRReport = false;
						this.showProcessing = false;
						this.toastr.info("No Record Found.");
					} else {
						this._hierarchyService
							.getManagerHierarchy(obj)
							.subscribe((res: any) => {
								let maxdesignation = 100;
								let currentManager = { name: "---" };

								res.forEach((item) => {
									if (
										item.designationLevel < maxdesignation
									) {
										maxdesignation = item.designationLevel;
										currentManager = item;
									}
								});

								result.forEach((i) => {
									i["managerName"] = currentManager.name;
								});

								this.dataSources = result;
								this.showProcessing = false;
								this.showDateWiseDCRReport = true;
								this._changeDetectorRef.detectChanges();								
							});

						//--------------get locking dcrs--by preeti ------------------------------------//
						//   this._dcrReportsService.getLockedDCR(userId, fromDate, toDate).subscribe(lockedDetails => {

						//  }, err => {
						//  console.log(err)
						// });

						//------------------end-----------------//
					}
				},
				(err) => {
					console.log(err);
					this.showDateWiseDCRReport = false;
					this.showProcessing = false;
					this.toastr.info("No Record Found.");
					// alert("Oooops! \nNo Record Found");
					this._changeDetectorRef.detectChanges();
				}
			);
			
	}

	getDCRInfo(val) {
		if (val != undefined) {
			this.showDCRSummary = false;
			this.callUserCompleteDCR.getUserCompleteDCRDetails(val);

			//s this.callUserCompleteDCR.getUserCompleteDCRDetails(val)
		} else {
			this.showDCRSummary = true;
		}
		//this.router.navigate(['/reports/usercompletedcr'],{ queryParams: { dcrId: val } });
	}

	exporttoExcel() {
		this.exportAsService
			.save(this.exportAsConfig, "DCR Consolidate Date Wise")
			.subscribe(() => {
				// save started
			});
	}
	scroll(el: HTMLElement) {
		el.scrollIntoView({ behavior: "smooth" });
	}

	///*****************************************UMESH 19-03-2020 *************/

	PrintTableFunction(data?: any): void {
		//const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
		const currentDateAndTime = _moment(new Date()).format(
			"DD/MM/YYYY, dddd , HH:mm A"
		);
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;

		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;

		let tableRows: any = [];
		data.forEach((element) => {
			tableRows.push(`<tr>
			${ this.isDivisionExist ? `<td class="tg-oiyu">${element.divisionName}</td>`: "" } 
  
  <td class="tg-oiyu">${element.headquarter}</td>
  <td class="tg-oiyu">${element.UserName}</td>
  <td class="tg-oiyu">${element.UserDesignation}</td>
  <td class="tg-oiyu">${element.dateOfJoining}</td>
  <td class="tg-oiyu">${element.mobile}</td>
  <td class="tg-oiyu">${element.employeeCode}</td>
  <td class="tg-oiyu">${element.lastDCRDate}</td>
  <td class="tg-oiyu">${element.totalDCR}</td>
  <td class="tg-oiyu">${element.RMPCount}</td>
  <td class="tg-oiyu">${element.doctorPOB}</td>
  <td class="tg-oiyu">${element.DrugStoreCount}</td>
  <td class="tg-oiyu">${element.stockistCount}</td>
  <td class="tg-oiyu">${element.vendorPOB}</td>
  </tr>`);
		});

		let showHeaderAndTable: boolean = false;
		//this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
		let printContents, popupWin;
		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
  <html>
  <head>
  <style>
  img {
  float: left;
  }

  .flex-container {
  display: flex;
  justify-content: space-between;
  }

  .tg {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tg td {
  font-family: Arial, sans-serif;
  font-size: 14px;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg th {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg .tg-lboi {
  border-color: inherit;
  text-align: left;
  vertical-align: middle;

  }

  .tg .tg-u6fn {
  font-weight: bold;
  font-size: 18px;
  background-color: #efefef;
  color:black;
  border-color: inherit;
  text-align: left;
  vertical-align: top;
  padding-top: 0px;
  padding-bottom: 0px;
  }

  .tg .tg-yz93 {
  border-color: inherit;
  text-align: right;
  vertical-align: middle
  }

  .tb2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tb2 td {
  font-family: Arial, sans-serif;
  font-size: 12px;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tb2 th {
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-weight: normal;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tb2 .tg-oiyu {
  font-size: 10px;
  text-align: left;
  vertical-align: middle
  }
  .tg-bckGnd {
  font-weight: bold;
  background-color: #efefef;
  }


  .tb2 .tg-3ppa {
  font-size: 10px;
  background-color: #efefef;
  color:black;
  text-align: left;
  vertical-align: middle;
  padding-top: 2px;
  padding-bottom: 2px;
  }

  .footer {
  position: sticky;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #efefef;
  color: rgb(2, 2, 2);
  text-align: right
  }



  button {
  background-color: #008CBA;
  border: none;
  color: white;
  margin: 5px 0px;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  }

  @media print {
  @page { size: A4; margin: 5mm; }

  .footer {
  position: relative;
  }
  tr {
  page-break-inside: avoid;
  }

  button{
  display : none;
  }
  }
  </style>
  </head>
  <body onload="window.print();window.close()" style="padding-bottom:30px">
  <div class="flex-container">
  <div style="width: 40%; ">
  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

  </div>

  <div style="text-align: right;">
  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Consolidate Report</h2>
  <div>
  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
  </div>
  </div>
  </div>

  <div>
  ${
		/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/ ""
  }
  <table class="tb2">
  <tr>
  ${
	this.isDivisionExist
		? `<th class="tg-3ppa"><span style="font-weight:600">Division</span></th>`
		: ""
} 
  
  <th class="tg-3ppa"><span style="font-weight:600">Headquarter</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Employee Name</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Designation</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Date Of Joining</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Mobile</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Employee Code</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Last Submitted DCR</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Total DCR</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.doctorLabel} Met Count</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.doctorLabel} POB</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.vendorLabel}/${this.currentUser.company.lables.stockistLabel} Met Count</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.stockistLabel} Met Count</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.vendorLabel}/${this.currentUser.company.lables.stockistLabel} POB</span></th>
  </tr>
  ${tableRows.join("")}
  </table>
  </div>
  <div class="footer flex-container">
  <p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>

  <p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
  </div>
  </body>
  </html>
  `);

		//popupWin.document.close();
	}


	onPreviewClick(){
   
		let printContents, popupWin;
	  
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;
		printContents = document.getElementById('tableId1').innerHTML;
		const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
	
		popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		popupWin.document.open();
		popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;
	}
	
	  tg.th {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  font-weight: normal;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4; margin: 5mm; }
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
	  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
	
	  </div>
	
	  <div style="text-align: right;">
	  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Basic Info </h2>
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  ${printContents}</body>
	  </html>
	  `
		);
	   // popupWin.document.close();
	 }
}
