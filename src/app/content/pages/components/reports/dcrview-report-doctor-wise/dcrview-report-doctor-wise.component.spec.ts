import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DCRViewReportDoctorWiseComponent } from './dcrview-report-doctor-wise.component';

describe('DCRViewReportDoctorWiseComponent', () => {
  let component: DCRViewReportDoctorWiseComponent;
  let fixture: ComponentFixture<DCRViewReportDoctorWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DCRViewReportDoctorWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DCRViewReportDoctorWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
