import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { DivisionComponent } from "../../filters/division/division.component";
import { DesignationComponent } from "../../filters/designation/designation.component";
import {
	FormBuilder,
	FormControl,
	Validators,
	FormGroup,
} from "@angular/forms";
import { MonthYearService } from "../../../../../core/services/month-year.service";
import { DesignationService } from "../../../../../core/services/designation.service";
import { TourProgramService } from "../../../../../core/services/TourProgram/tour-program.service";
import { ToastrService } from "ngx-toastr";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";
import { DCRProviderVisitDetailsBackupService } from "../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service";
import { FileHandlingService } from "../../../../../core/services/FileHandling/file-handling.service";
import { UserDetailsService } from "../../../../../core/services/user-details.service";
import { LatLongService } from "../../../../../core/services/LatLong/lat-long.service";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import { MatSlideToggleChange } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { URLService } from "../../../../../core/services/URL/url.service";
import * as _moment from "moment";
// var _moment = require('moment');

declare var $;
@Component({
	selector: "m-dcrview-report-doctor-wise",
	templateUrl: "./dcrview-report-doctor-wise.component.html",
	styleUrls: ["./dcrview-report-doctor-wise.component.scss"],
})
export class DCRViewReportDoctorWiseComponent implements OnInit {
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;
	excelData:[];
	data=false;
	count = 0;
	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId12", // the id of html/table element
	};
	isToggled = true;
	userData: any;
	userIds: any;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
	currentUser = JSON.parse(sessionStorage.currentUser);
	
	isDivisionExist = this.currentUser.company.isDivisionExist;
	companyId = this.currentUser.companyId;
	public showDivisionFilter: boolean = false;
	dataSource = [];
	showGeoReportType = false;
	
	showProcessing = false;
	DCRViewDOctorWiseForm: FormGroup;
	constructor(
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private _designation: DesignationService,
		private _changeDetectorRef: ChangeDetectorRef,
		private _toastr: ToastrService,
		private _dcrReportsService: DcrReportsService,
		private DCRProviderVisitDetailsBackupService: DCRProviderVisitDetailsBackupService,
		private _fileHandlingService: FileHandlingService,
		private userDetailService: UserDetailsService,
		private _latLongService: LatLongService,
		private exportAsService: ExportAsService,
		private toastr: ToastrService,
		private _urlService: URLService
	) {
		this.DCRViewDOctorWiseForm = this.fb.group({
			designation: new FormControl("", Validators.required),
			employeeId: new FormControl(null, Validators.required),
			toDate: new FormControl("", Validators.required),
			fromDate: new FormControl("", Validators.required),
		});
	}
	getFromDate(val) {
		this.DCRViewDOctorWiseForm.patchValue({ fromDate: val });
	}
	getToDate(val) {
		this.DCRViewDOctorWiseForm.patchValue({ toDate: val });
	}
	getDivisionValue(val) {
		//this.showTPDeviationDetails = false;
		this.DCRViewDOctorWiseForm.patchValue({ division: val });
		//=======================Clearing Employee Filter===================
		this.callDesignationComponent.setBlank();
		//===============================END================================
		let passingObj = {
			companyId: this.currentUser.companyId,
			userId: this.currentUser.id,
			userLevel: this.currentUser.userInfo[0].designationLevel,
			division: [val],
		};
		this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
	}
	getDesignation(val) {
		//  this.showTPDeviationDetails = false;
		this.DCRViewDOctorWiseForm.patchValue({ designation: val });

		if (this.currentUser.company.isDivisionExist == true) {
			if (
				this.currentUser.company.isDivisionExist == true &&
				this.DCRViewDOctorWiseForm.value.division != null
			) {
				let passingObj = {
					companyId: this.currentUser.companyId,
					designationObject: val.designationLevel,
					status: [true],
					division: [this.DCRViewDOctorWiseForm.value.division],
				};
				this.userDetailService
			.getUserInfoBasedOnDesignationAndBasedOnDivision(passingObj)
			.subscribe(
				(res) => {
					this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});
				},
				(err) => {
					console.log(err);
				}
			);
				
			}
		} else {
			this.userDetailService.getUserInfoBasedOnDesignation(
				this.currentUser.companyId,
				val.designationLevel,
				[true]
			).subscribe(res=>{
				this.userData = res;
					this.userIds = res.map((item) => {
						return item.userId;
					});

					this._changeDetectorRef.detectChanges();
				},
				(err) => {
					console.log(err);
			})
		}
	}


	
	getEmployees(val) {
		this.DCRViewDOctorWiseForm.patchValue({ employeeId: val });
	}
	exporttoExcel() {
		console.log("after excel",this.excelData)
		// download the file using old school javascript method
		this.exportAsService
			.save(this.exportAsConfig, "DCR Detail Doctor Wise")
			.subscribe(() => {
				// save started
			});
	}

	/* typeDataLabel(data){
		if(data == 'RMP'){
			 return this.currentUser.company.lables.doctorLabel; 
		}else if(data == 'Drug'){
			return this.currentUser.company.lables.vendorLabel; 
		}
	} */

	onPreviewClick() {
		let printContents, popupWin;

		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;
		printContents = document.getElementById("tableId12").innerHTML;
		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;
		let tableRows: any = [];
		
		this.dataSource.forEach((element) => {
			var mydate = new Date(element.dcrDate);
			
			for (let i = 0; i < element.info.length; i++) {
				tableRows.push(`
				<tr>
      				<td class="tg-oiyu"  >${mydate.getDate()}-${mydate.getMonth() + 1
					}-${mydate.getFullYear()}</td>
    
					<td class="tg-oiyu"  >${element.userInfo.name}</td>
      				<td class="tg-oiyu"  >${element.info[i].block.areaName}</td>
        			<td class="tg-oiyu"  >${element.info[i].providerinfo.providerName}</td>
        			<td class="tg-oiyu"  >${element.info[i].providerNameFull ? element.info[i].providerNameFull : '---'}</td>
        			<td class="tg-oiyu"  >${element.info[i].workingAddress}</td>
        			<td class="tg-oiyu"  >${element.info[i].productPob}</td>
        			<td class="tg-oiyu"  >${element.info[i].totalSession.timeIn}</td>
       				<td class="tg-oiyu"  >${element.info[i].totalSession.timeOut}</td>
        			<td class="tg-oiyu"  >${element.info[i].remarks}</td>
				</tr>`);
			}
		});

		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
      <html>
      <head>
      <style>
      img {
      float: left;
      }
    
      .flex-container {
      display: flex;
      justify-content: space-between;
      }
    
      .tg {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      margin-top: 10px;
      }
    
      .tg td {
      font-family: Arial, sans-serif;
      font-size: 14px;
      padding: 6px 2px;
      border-style: solid;
      border-width: 1px;
      overflow: hidden;
      word-break: normal;
      border-color: black;
      }
    
      table {
      border-collapse: collapse;
    }
    
    
    
      .tg .tg-lboi {
      border-color: inherit;
      text-align: left;
      vertical-align: middle;
    
      }
    
      .tg .tg-u6fn {
      font-weight: bold;
      font-size: 18px;
      background-color: #efefef;
      color:black;
      border-color: inherit;
      text-align: left;
      vertical-align: top;
      padding-top: 0px;
      padding-bottom: 0px;
      }
    
      .tg .tg-yz93 {
      border-color: inherit;
      text-align: right;
      vertical-align: middle
      }
    
      .tb2 {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      margin-top: 10px;
      }
    
       td {
      font-family: Arial, sans-serif;
      font-size: 12px;
      padding: 2px;
      border-style: solid;
      border-width: 1px;
      overflow: hidden;
      word-break: normal;
      border-color: black;
      
      }
    
      th {
      background-color:#AAB7B8;
      font-family: Arial, sans-serif;
      font-size: 12px;
      font-weight: normal;
      padding: 2px;
      border-style: solid;
      border-width: 1px;
      overflow: hidden;
      word-break: normal;
     
      
      }
    
      .tb2 .tg-oiyu {
      font-size: 10px;
      text-align: left;
      vertical-align: middle
      }
      .tg-bckGnd {
      font-weight: bold;
      background-color: #efefef;
      }
    
    
      .tb2 .tg-3ppa {
      font-size: 10px;
      background-color: #efefef;
      color:black;
      text-align: left;
      vertical-align: middle;
      padding-top: 2px;
      padding-bottom: 2px;
      }
    
      .footer {
      position: sticky;
      left: 0;
      bottom: 0;
      width: 100%;
      background-color: #efefef;
      color: rgb(2, 2, 2);
      text-align: right
      }
    
    
    
      button {
      background-color: #008CBA;
      border: none;
      color: white;
      margin: 5px 0px;
      padding: 2px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 15px;
      border-radius: 12px;
      cursor: pointer;
      outline: none;
      }
    
      @media print {
      @page { size: A4 landscape;
      max-height:100%;
      max-width:100%}
    
      .footer {
      position: relative;
      }
      tr {
      page-break-inside: avoid;
      }
    
      button{
      display : none;
      }
      }
      </style>
      </head>
      <body onload="window.print();window.close()" style="padding-bottom:30px">
      <div class="flex-container">
      <div style="width: 40%; ">
      <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
      <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
    
      </div>
    
      <div style="text-align: right;">
      <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Report Doctorwise Report</h2>
      <div>
      <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
      <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
      </div>
      </div>
      </div>
      <table class="tb2"> 
    <tr>
    <th class="tg-3ppa"><span style="font-weight:600">Date</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Employee Name</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Area Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Customer Name</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Customer Type</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Working Address</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">POB</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Time In</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Time Out</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Remarks</span></th>
    <tr>
    ${tableRows.join("")}
    </table>
     </body>
      </html>
      `);
		// popupWin.document.close();
	}

	showReport() {
		this._changeDetectorRef.detectChanges();
		this.showGeoReportType = false;
		this.showProcessing = true;
		let userId = this.DCRViewDOctorWiseForm.value.employeeId;
		let fromDate = this.DCRViewDOctorWiseForm.value.fromDate;
		let toDate = this.DCRViewDOctorWiseForm.value.toDate;
		let dcrIdArr = [];
		this._dcrReportsService
			.getUserconsolidateDateWiseDCRDetail(userId, fromDate, toDate)
			.subscribe(
				(result) => {
					//console.log("this result",result);
					this.excelData=result;
					for (var i = 0; i < result.length; i++) {
						dcrIdArr.push(result[i].dcrId);
					}
					let obj = {
						dcrIds: dcrIdArr,
						companyId: this.companyId,
					};
					this._dcrReportsService
						.getUserCompleteDCRDoctorDetails(obj)
						.subscribe((res) => {
							if (res == undefined || res == 0) {
								this.showGeoReportType = false;
								this.showProcessing = false;
								this.toastr.info("No Record Found.");
							} else {
								//-----------------------Preeti Arora 3-Dec-2019------------------------
								this.showProcessing = false;
								this.isToggled = false;

								this.showGeoReportType = true;
								this.dataSource = res;

								this.dataSource.forEach((item, i) => {
									item.info.forEach((info) => {
										if (
											info.geoLocation.length <= 0 ||
											info.geoLocation.length ==
											undefined ||
											info.geoLocation == null ||
											info.geoLocation == []
										) {
											info["workingAddress"] = "---";
										} else {
											this._latLongService
												.getAddressOnLongLatBasis(
													info.geoLocation[0]
												)
												.subscribe((res) => {
													if (res.length != 0) {
														info["workingAddress"] =
															res[0].address;
														this._changeDetectorRef.detectChanges();
													} else {
														var geocoder = (geocoder = new google.maps.Geocoder());
														var latlng1 = {
															lat: parseFloat(
																info
																	.geoLocation[0]
																	.lat
															),
															lng: parseFloat(
																info
																	.geoLocation[0]
																	.long
															),
														};
														geocoder.geocode(
															{
																location: latlng1,
															},
															(
																results,
																status
															) => {
																if (
																	status ==
																	google.maps
																		.GeocoderStatus
																		.OK
																) {
																	if (
																		results[0]
																	) {
																		info[
																			"workingAddress"
																		] =
																			results[0].formatted_address;
																		this._changeDetectorRef.detectChanges();
																	} else {
																		info[
																			"workingAddress"
																		] =
																			"---";
																	}
																} else {
																	info[
																		"workingAddress"
																	] = "---";
																}
															}
														);
													}
												}),
												(err) => {
													console.log(err);
												};
										}

										if(info.providerType === 'RMP'){
											info['providerNameFull']=this.currentUser.company.lables.doctorLabel;
										}else if(info.providerType === 'Drug'){
											info['providerNameFull']=this.currentUser.company.lables.vendorLabel;
										}else if(info.providerType === 'Stokist'){
											info['providerNameFull'] = this.currentUser.company.lables.stockistLabel;
										}
									});
								});
								
								//console.log("this.excelData", this.excelData);

								this._changeDetectorRef.detectChanges();

								//---------------------------------END-------------------------------------
							}
						});
				},
				(err) => {
					console.log(err);
					this._changeDetectorRef.detectChanges();
				}
			);
	}

	filterDoctorInfor(dataSource) {
		const data = dataSource.filter((i) => i.providerType === "RMP");
		data.forEach((i) => {
			if (i.jointWorkWithId.length == 0) {
				i.jointWorkWithId[0] = { Name: "---" };
			}
		});
		return data;
	}

	filterDrugInfor(dataSource) {
		const data = dataSource.filter((i) => i.providerType === "Drug");
		data.forEach((i) => {
			if (i.jointWorkWithId.length == 0) {
				i.jointWorkWithId[0] = { Name: "---" };
			}
		});

		return data;
	}
	filterStockistInfor(dataSource) {
		const data = dataSource.filter((i) => i.providerType == "Stockist");
		data.forEach((i) => {
			if (i.jointWorkWithId.length == 0) {
				i.jointWorkWithId[0] = { Name: "---" };
			}
		});
	}
	ngOnInit() {
		console.log("User Information",this.currentUser);
		console.log(this.currentUser.company.lables.doctorLabel);
		if (this.currentUser.company.isDivisionExist == true) {
			this.DCRViewDOctorWiseForm.setControl(
				"division",
				new FormControl()
			);

			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.showDivisionFilter = true;
		}
		this.route.queryParams.subscribe((res) => {
			if (res.toAndFromDate) {
				let date = _moment(new Date(res.toAndFromDate)).format(
					"YYYY-MM-DD"
				);
				this.DCRViewDOctorWiseForm.patchValue({ fromDate: date });
				this.DCRViewDOctorWiseForm.patchValue({ toDate: date });
				this.DCRViewDOctorWiseForm.patchValue({
					employeeId: res.userId,
				});
				this.showReport();
			}
		});
		this.route.queryParams = null;
	}
}
