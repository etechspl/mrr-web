import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleIssueReportDateWiseComponent } from './sample-issue-report-date-wise.component';

describe('SampleIssueReportDateWiseComponent', () => {
  let component: SampleIssueReportDateWiseComponent;
  let fixture: ComponentFixture<SampleIssueReportDateWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleIssueReportDateWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleIssueReportDateWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
