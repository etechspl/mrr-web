import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpLockOpenReportComponent } from './tp-lock-open-report.component';

describe('TpLockOpenReportComponent', () => {
  let component: TpLockOpenReportComponent;
  let fixture: ComponentFixture<TpLockOpenReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpLockOpenReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpLockOpenReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
