import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseConsolidatedReportComponent } from './expense-consolidated-report.component';

describe('ExpenseConsolidatedReportComponent', () => {
  let component: ExpenseConsolidatedReportComponent;
  let fixture: ComponentFixture<ExpenseConsolidatedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenseConsolidatedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseConsolidatedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
