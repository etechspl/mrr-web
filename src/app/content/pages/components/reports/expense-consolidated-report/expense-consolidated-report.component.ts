import { Component, OnInit, ChangeDetectorRef, ViewChild, EventEmitter, Output, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatTable, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { ExpenseClaimedService } from '../../../../../core/services/expense-claimed.service';
import { DailyallowanceService } from '../../../_core/services/dailyallowance.service';
import { ToastrService } from 'ngx-toastr';
import { TypeComponent } from '../../filters/type/type.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { ExpenseModifyComponent } from '../expense-modify/expense-modify.component';
import * as _moment from 'moment';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
declare var $;

@Component({
  selector: 'm-expense-consolidated-report',
  templateUrl: './expense-consolidated-report.component.html',
  styleUrls: ['./expense-consolidated-report.component.scss']
})
export class ExpenseConsolidatedReportComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
    private expenseClaimedService: ExpenseClaimedService,
    private changeDetectorRef: ChangeDetectorRef,
    private dailyallowanceService:DailyallowanceService,
    private toastr: ToastrService,
    private _changeDetectorRef: ChangeDetectorRef,

    ) { }

  public isDisableButton: boolean;
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showDesignation: boolean = false;
  public showEmployee: boolean = false;
  public showStatus: boolean = false;
  public showDivisionFilter: boolean=false;
  public showProcessing: boolean=false;
  public showType: boolean = true;

  public color: string;
  public isShowReport: boolean = false;
  isShowDivision = false;

  @ViewChild('dataTable') table;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: any;
  dataSource2: any;
  consolidatedExpenseForm: FormGroup;
  showAdminAndMGRLevelFilter = false;

  @ViewChild('dataTable') Table;
  dataTable: any;
  dtOptions: any;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @Output() reRender: EventEmitter<null> = new EventEmitter();
  @HostListener('click')
  click() {
    this.reRender.emit();
  }
  isShowColumn: boolean = false;

  obj = {};
  isShowReportExpense = false;

  currentUser = JSON.parse(sessionStorage.currentUser);

  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  DA_TYPE=this.currentUser.company.expense.daType;
  IS_DIVISION_EXIST=this.currentUser.company.isDivisionExist;

  ngOnInit() {

    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
    this.createForm();
    this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithArea");
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }
  getReportType(reportType: string) {

    if (reportType === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }
  getTypeValue(val) {
    this.isShowReportExpense = false;
    this.consolidatedExpenseForm.patchValue({ type: val });

    if (val == "State") {
      this.showDistrict = false;
      this.consolidatedExpenseForm.setControl("stateInfo", new FormControl())
      this.consolidatedExpenseForm.removeControl("employeeId");
      this.consolidatedExpenseForm.removeControl("designation");
    } else if (val == "Headquarter") {
      this.consolidatedExpenseForm.setControl("stateInfo", new FormControl())
      this.consolidatedExpenseForm.setControl("districtInfo", new FormControl())
      this.consolidatedExpenseForm.removeControl("employeeId");
      this.consolidatedExpenseForm.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      this.consolidatedExpenseForm.removeControl("stateInfo");
      this.consolidatedExpenseForm.removeControl("districtInfo");
      this.consolidatedExpenseForm.setControl("designation", new FormControl());
      this.consolidatedExpenseForm.setControl("employeeId", new FormControl());
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if(this.IS_DIVISION_EXIST===true){
      this.isShowDivision = true;
    }else{
      this.isShowDivision = false;
    }


    this._changeDetectorRef.detectChanges();
  }

  createForm() {
    this.consolidatedExpenseForm = this.fb.group({
      //"reportType": new FormControl('Geographical',),
     "type": new FormControl(null),
    //  "designation": new FormControl('', Validators.required),
  //    "status": new FormControl('', Validators.required),
        "employeeIds": new FormControl(null),
        divisionId:new FormControl(null),
        stateInfo:new FormControl(null),

      "month": new FormControl('', Validators.required),
      "year": new FormControl('', Validators.required)
    })

  }

  getStateValue(val) {
    this.consolidatedExpenseForm.patchValue({ stateInfo: val });
    if (this.IS_DIVISION_EXIST === true) {
      if (this.consolidatedExpenseForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.consolidatedExpenseForm.value.stateInfo,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.consolidatedExpenseForm.value.divisionId
        })

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.consolidatedExpenseForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(districtInfo: any) {
    this.consolidatedExpenseForm.patchValue({ districtInfo: districtInfo });

  }

  getDesignation(val) {
    this.consolidatedExpenseForm.patchValue({ designation: val });
    if (this.IS_DIVISION_EXIST === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.consolidatedExpenseForm.value.divisionId,
        status: [true],
        designationObject: this.consolidatedExpenseForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  getStatusValue(status: any) {
    this.consolidatedExpenseForm.patchValue({ status: status });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.consolidatedExpenseForm.value.designation.designationLevel, status);

  }
  getDivisionValue($event) {
    this.consolidatedExpenseForm.patchValue({ divisionId: $event });
    if (this.IS_DIVISION_EXIST === true) {
      if (this.consolidatedExpenseForm.value.type === "State" || this.consolidatedExpenseForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.consolidatedExpenseForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.consolidatedExpenseForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.consolidatedExpenseForm.value.divisionId != null && this.consolidatedExpenseForm.value.designation != null) {
          if (this.consolidatedExpenseForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.consolidatedExpenseForm.value.divisionId,
              status: [true],
              designationObject: this.consolidatedExpenseForm.value.designation
            })
          }
        }
      }
    }
  }
  getEmployeeValue(employeeId: any) {

    this.consolidatedExpenseForm.patchValue({ employeeIds: employeeId });

  }
  getMonth(month: any) {
    this.consolidatedExpenseForm.patchValue({ month: month });
  }
  getYear(year: any) {
    this.consolidatedExpenseForm.patchValue({ year: year });
  }

  approvedByMgr: string;
  approvedByAdmin: string;
  checkApprovalStatus(item: any) {
    //for manager
    if (item.hasOwnProperty("appByMgr") === true) {
      this.approvedByMgr = "Yes";
    } else {
      this.approvedByMgr = "No";
    }

    //for admin
    if (item.hasOwnProperty("appByAdm") === true) {
      this.approvedByAdmin = "Yes";
    } else {
      this.approvedByAdmin = "No";
    }
  }

  //generate Report
  generatereport() {
    this.isShowReport = false;
    if (this.consolidatedExpenseForm.valid) {
      let month = this.consolidatedExpenseForm.value.month;
      if (month < 10) {
        month = "0" + month
      }
      let date = this.consolidatedExpenseForm.value.year + '-' + month + '-01';
      let startDate = _moment.utc(new Date(date)).startOf('month').format('YYYY-MM-DD');
      let endDate = _moment.utc(new Date(date)).endOf('month').format('YYYY-MM-DD');

      this.obj = {
        companyId: this.currentUser.companyId,
        startDate: startDate,
        endDate: endDate,
        type: this.consolidatedExpenseForm.value.type,
        rL: this.currentUser.userInfo[0].rL
      }
      if (this.IS_DIVISION_EXIST === true) {
        this.obj["division"] = this.consolidatedExpenseForm.value.divisionId;
      } else {
        this.obj["division"] = [];
      }

      if(this.currentUser.userInfo[0].rL>1){
        this.obj["supervisorId"] = [this.currentUser.id];
      }
      if (this.consolidatedExpenseForm.value.type === 'State') {
        this.obj["stateId"] = this.consolidatedExpenseForm.value.stateInfo;
      } else if (this.consolidatedExpenseForm.value.type === 'Headquarter') {
        this.obj["stateId"] = this.consolidatedExpenseForm.value.stateInfo;
        this.obj["districtIds"] = this.consolidatedExpenseForm.value.districtInfo;
      } else if (this.consolidatedExpenseForm.value.type === 'Employee Wise') {
        this.obj["userId"] = this.consolidatedExpenseForm.value.employeeIds;
      }
      this.showProcessing=true;

      this.expenseClaimedService.getConsolidatedExpense(this.obj).subscribe(res => {

        if (res.length > 0) {
          this.showProcessing=false;
          this.isShowReportExpense = true;
          this.dataSource2=res;
          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            destroy: true,
            data: this.dataSource2,
            columns: [
              {
                title: 'State Name',
                data: 'state',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.hqLabel,
              data: 'district',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
          },
          {
            title: 'Name',
            data: 'name',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
        },
        {
          title: 'Claimed Expense',
          data: 'claimedExpense',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
      },
      {
        title: 'Approved Expense By Mgr',
        data: 'approvedExpenseByMgr',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
    },
    {
      title: 'Approved Expense By Admin',
      data: 'approvedExpenseByAdmin',
      render: function (data) {
        if (data === '') {
          return '---'
        } else {
          return data
        }
      },
      defaultContent: '---'
  },
  {
    title: 'Amount Deduction',
    data: 'AmountDeduction',
    render: function (data) {
      if (data === '') {
        return '---'
      } else {
        return data
      }
    },
    defaultContent: '---'
}
          ],
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search records",
            },
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'copy',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'print',
                exportOptions: {
                  columns: ':visible'
                }
              }
            ]
          }
          if (this.currentUser.company.isDivisionExist == false) {
            this.dtOptions.columns[0].visible = false;
          }
          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);

        } else {
          this.showProcessing=false;
          this.toastr.info("Expense is not submitted for the selected  month & year", "Expense not Submitted");
          this.isShowReport = false;
          this.isShowReportExpense = false;
        }
        this.changeDetectorRef.detectChanges();
      })
    }
  }





}
