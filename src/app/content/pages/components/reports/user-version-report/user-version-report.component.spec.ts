import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserVersionReportComponent } from './user-version-report.component';

describe('UserVersionReportComponent', () => {
  let component: UserVersionReportComponent;
  let fixture: ComponentFixture<UserVersionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserVersionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserVersionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
