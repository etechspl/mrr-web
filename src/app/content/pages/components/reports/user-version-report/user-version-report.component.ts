import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { StateComponent } from '../../filters/state/state.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import * as XLSX from "xlsx";
import { MatSlideToggleChange } from "@angular/material";

@Component({
  selector: 'm-user-version-report',
  templateUrl: './user-version-report.component.html',
  styleUrls: ['./user-version-report.component.scss']
})
export class UserVersionReportComponent implements OnInit {

  constructor(
    private _dcrProviderVisitService: ProviderService,

  ) { }
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementId: 'tableId1', // the id of html/table element
	}
  show = false;
  showProcessing: boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist: boolean = true;
  showDivisionFilter: boolean = false;
  isToggled: boolean = true;
  selection = new SelectionModel(true, []);
  selection1 = new SelectionModel(true, []);
  primaryContact: any;
  showHeadquarter: boolean = false;
  employeeFilter: boolean = true;
  stateFilter: boolean = true;
  dataSource: MatTableDataSource<any>;
  tableData: any = [];
  displayedColumns: string[] = [ 'address','districtName','category','name' , 'user', 'weight'];
  
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
  dcrStatus = new FormGroup({
        
    state: new FormControl(null),
    district: new FormControl(null),
    designation: new FormControl(null),
    employee: new FormControl(null, Validators.required),

  })
  ngOnInit() {
    console.log('currentUser=> ', this.currentUser);

    if (this.currentUser.company.isDivisionExist === true) {
      this.isDivisionExist = true;
      this.dcrStatus.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  

  getTypeValue(val) {
    console.log(val);
    this.dcrStatus.patchValue({ type: val })
    let { } = this.currentUser;
   
      this.stateFilter = false;
      this.showHeadquarter = false;
      this.employeeFilter = true;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;

        this.callEmployeeComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
      }
    
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }
  getDivisionValue(val) {
    this.dcrStatus.patchValue({ division: val })
    if (this.dcrStatus.value.type == "Employee Wise") {
      //=======================Clearing Filter===================
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    }
  }

  getDistrict(val) {
    this.dcrStatus.patchValue({ district: val })
  }
 exporttoExcel(){
  
    let dataToExport=[];
    dataToExport=this.dataSource.data.map((x)=>({
      "stateName" : x.stateName,
      "appCurrentVersion" : x.appCurrentVersion,
      "model" : x.model,
      "divisionName"  : x.divisionName,
      "districtName" : x.districtName
    }));
    dataToExport.push(
      {
      "Name" :'------',
      "productiveCalls" : '------',
      "totalPOB" : '------',
      "totalCalls"  : '------',
      "state" : '------'
      }
    )

    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "userVersion.xlsx");
  }
  getDesignation(val) {
    this.dcrStatus.patchValue({ designation: val })
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.dcrStatus.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    }
  }

  getEmployee(val) {
    this.dcrStatus.patchValue({ employee: val })
  }

  getRecords() {
    this.selection.clear();
    console.log('data', this.dcrStatus.value)
    this.showProcessing = true;
    this.isToggled = false;
    let param = {
    }
      param['userId'] =  this.dcrStatus.value.employee ;
    console.log("params :--------------------------- ", param);
    this._dcrProviderVisitService.userversioncheck(param).subscribe(res => {
      console.log(res);

      this.showProcessing = false;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.show = true;
    }
    )
  }
}
