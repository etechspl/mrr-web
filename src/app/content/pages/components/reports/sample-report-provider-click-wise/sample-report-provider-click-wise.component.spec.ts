import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleReportProviderClickWiseComponent } from './sample-report-provider-click-wise.component';

describe('SampleReportProviderClickWiseComponent', () => {
  let component: SampleReportProviderClickWiseComponent;
  let fixture: ComponentFixture<SampleReportProviderClickWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleReportProviderClickWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleReportProviderClickWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
