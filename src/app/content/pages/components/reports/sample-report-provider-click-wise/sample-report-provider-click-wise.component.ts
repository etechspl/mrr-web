import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { StateComponent } from '../../filters/state/state.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DCRBackupService } from '../../../../../core/services/DCRBackup/dcrbackup.service';
import { DCRProviderVisitDetailsBackupService } from '../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { MatDialog, MatPaginator, MatSlideToggleChange, MatTableDataSource } from '@angular/material';
import { EdituserComponent } from '../edituser/edituser.component';
import { ResignedTPStatusComponent } from '../resigned-tpstatus/resigned-tpstatus.component';
import { ViewDCRResignedUserComponent } from '../view-dcrresigned-user/view-dcrresigned-user.component';
declare var $;

import { URLService } from '../../../../../core/services/URL/url.service';
import * as _moment from 'moment';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { DcrProviderVisitDetailsService } from '../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
@Component({
  selector: 'm-sample-report-provider-click-wise',
  templateUrl: './sample-report-provider-click-wise.component.html',
  styleUrls: ['./sample-report-provider-click-wise.component.scss']
})
export class SampleReportProviderClickWiseComponent implements OnInit {
  @ViewChild('paginator') paginator: MatPaginator;

  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;
  isShowReport = false;
  isShowProviderTable=false;
  showAdminAndMGRLevelFilter = false;
  @ViewChild("userDataTable") dataTable1;
  userDataTable: any;
  userDTOptions: any;
  data;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;


  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
	dataSource: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  constructor( private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private _providerService: ProviderService,
    private _dcrProviderVisitService: DcrProviderVisitDetailsService,
    private DCRProviderVisitDetailsBackupService: DCRProviderVisitDetailsBackupService,
    private exportAsService: ExportAsService,
    private dialog: MatDialog,
    ) { }
    isShowDivision = false;
    currentUser = JSON.parse(sessionStorage.currentUser);
    isDivisionExist = this.currentUser.company.isDivisionExist;
    designationLevel = this.currentUser.userInfo[0].designationLevel;
    companyId = this.currentUser.companyId;

    sampleForm = new FormGroup({
      reportType: new FormControl('Geographical'),
      type: new FormControl(null, Validators.required),
      stateInfo: new FormControl(null),
      districtInfo: new FormControl(null),
      employeeId: new FormControl(null),
      designation: new FormControl(null),
      toDate: new FormControl(null, Validators.required),
      fromDate: new FormControl(null, Validators.required)
    })
    exportAsConfig: ExportAsConfig = {
      type: "xlsx", // the type you want to download
      elementId: "tableId1", // the id of html/table element
    };
    displayedColumns = [
      "sn",
      "state",
      "hq",
      "userName",
      "providerName",
      "area",
      "category",
      "visit",
      "visitDate",
      "pob",
     
    ];
    displayedColumns2 = [
      "providerName",
      "visit",
      "noOfMgrVisit",
      "pob",
      "givenProducts",
      "noOfProducts",
      "qty",
      "gift",
      "giftQty",
    ];

    check: boolean = false;
    isToggled = true;
    toggleFilter(event: MatSlideToggleChange) {
      this.isToggled = event.checked;
    }
    getReportType(val) {
      if (val === "Geographical") {
        this.showState = true;
        this.showEmployee = false;
        this. callDivisionComponent.setBlank();
        this.sampleForm.patchValue({type: ''});
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      } else {
        this.showState = false;
        this.showDistrict = false;
        this. callDivisionComponent.setBlank();
        this.sampleForm.patchValue({divisionId: ''});
        this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
      }
    }
    getTypeValue(val) {
      if (val == "State") {
        this.showDistrict = false;
        if (this.isDivisionExist === true) {
          this.callDivisionComponent.setBlank();
        }
        this.callStateComponent.setBlank();
        this.sampleForm.removeControl("employeeId");
        this.sampleForm.removeControl("designation");
        this.sampleForm.setControl("stateInfo", new FormControl('', Validators.required))
      } else if (val == "Headquarter") {
        if (this.isDivisionExist === true) {
          this.callDivisionComponent.setBlank();
        }
        this.callStateComponent.setBlank();
        this.sampleForm.removeControl("employeeId");
        this.sampleForm.removeControl("designation");
        this.sampleForm.setControl("stateInfo", new FormControl('', Validators.required))
        this.sampleForm.setControl("districtInfo", new FormControl('', Validators.required))
        this.showDistrict = true;
      } else if (val == "Employee Wise") {
        this.showEmployee = true;
        if (this.isDivisionExist === true) {
          this.callDivisionComponent.setBlank();
        }
        this.sampleForm.removeControl("stateInfo");
        this.sampleForm.removeControl("districtInfo");
        this.sampleForm.setControl("designation", new FormControl('', Validators.required))
        this.sampleForm.setControl("employeeId", new FormControl('', Validators.required))
      } else if (val == "Self") {
        this.showEmployee = false;
      }
  
      if (this.isDivisionExist === true) {
        this.isShowDivision = true;
      } else {
        this.isShowDivision = false;
      }
  
      this.sampleForm.patchValue({ type: val });
      this._changeDetectorRef.detectChanges();
  
  
    }
    getStateValue(val) {
      this.sampleForm.patchValue({ stateInfo: val });
      if (this.isDivisionExist === true) {
        if (this.sampleForm.value.type == "Headquarter") {
          this.callDistrictComponent.getDistrictsBasedOnDivision({
            designationLevel: this.designationLevel,
            companyId: this.companyId,
            "stateId": this.sampleForm.value.stateInfo,
            isDivisionExist: this.isDivisionExist,
            division: this.sampleForm.value.divisionId
          })
  
          //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
        }
      } else {
        if (this.sampleForm.value.type == "Headquarter") {
          this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
        }
      }
    }
    getDistrictValue(val) {
      this.sampleForm.patchValue({ districtInfo: val });
    }
    getEmployeeValue(val) {
      this.sampleForm.patchValue({ employeeId: val });
    }
    getFromDate(val) {
      this.sampleForm.patchValue({ fromDate: val });
  
    }
    getToDate(val) {
      this.sampleForm.patchValue({ toDate: val });
    }
  
    getDesignation(val) {
      this.sampleForm.patchValue({ designation: val });
      if (this.isDivisionExist === true) {
        this.callEmployeeComponent.getEmployeeListBasedOnDivision({
          companyId: this.companyId,
          division: this.sampleForm.value.divisionId,
          status: [false],
          designationObject: this.sampleForm.value.designation
        })
      } else {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
  
    }
    //setting division into sampleForm
    getDivisionValue($event) {
      this.sampleForm.patchValue({ divisionId: $event });
      if (this.isDivisionExist === true) {
        if (this.sampleForm.value.type === "State" || this.sampleForm.value.type === "Headquarter") {
          this.callStateComponent.getStateBasedOnDivision({
            companyId: this.currentUser.companyId,
            isDivisionExist: this.isDivisionExist,
            division: this.sampleForm.value.divisionId,
            designationLevel: this.currentUser.userInfo[0].designationLevel
          })
        }
  
        if (this.sampleForm.value.type === "Employee Wise") {
          //getting employee if we change the division filter after select the designation
          if (this.sampleForm.value.divisionId != null && this.sampleForm.value.designation != null) {
            if (this.sampleForm.value.designation.length > 0) {
              this.callEmployeeComponent.getEmployeeListBasedOnDivision({
                companyId: this.companyId,
                division: this.sampleForm.value.divisionId,
                status: [true],
                designationObject: this.sampleForm.value.designation
              })
            }
          }
        }
      }
    }
    ngOnInit() {
  
      if (this.isDivisionExist === true) {
        this.isShowDivision = true;
        this.sampleForm.setControl('divisionId', new FormControl('', Validators.required));
        let obj = {
          companyId: this.currentUser.companyId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        };
  
        if (this.currentUser.userInfo[0].rL > 1) {
          obj["supervisorId"] = this.currentUser.id;
        }
        this.callDivisionComponent.getDivisions(obj);
  
      } else {
        this.isShowDivision = false;
      }
  
      if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
        this.showAdminAndMGRLevelFilter = true;
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      }
  
  
    }
    getProviderDetail(data){     
      let gifts=[];
      let products=[];    
      let mgrVisit=0;         

      let obj={
        companyId:this.currentUser.companyId,
        submitBy:data._id.userId,
        fromDate:this.sampleForm.value.fromDate,
        toDate:this.sampleForm.value.toDate,
        providerId:data._id.providerId
      }
      this._dcrProviderVisitService.getProviderSampleAndGiftDetails(obj).subscribe(res=>{
        if (res == null || res.length == 0 || res == undefined) {
          this.showProcessing = false;
          this.showReport = false;
          this.toastr.info('For Selected Filter data not found', 'Data Not Found');
        } else {   
          res[0].productDetails.forEach(prod => {        
            if(prod.length>0){
               prod.forEach((proRes,i) => {
                let obj=products.filter(obj=>{
                  return obj.productId.toString() == proRes.productId.toString();
                })
                if(obj.length>0){
                  obj[0].productQuantity+=proRes.productQuantity
                }else{
                  products.push({
                    productId:proRes.productId,
                    productName:proRes.productName,
                    productQuantity:proRes.productQuantity,
                    productPrice:proRes.productPrice
                  })
                }
               });
               
            }
          });
          res[0].giftDetails.forEach(element => {
          if(element.length>0){
            element.forEach(gift => {              
              let obj=gifts.filter(obj=>{
                return obj.giftId.toString() == gift.giftId.toString();
              })
              if(obj.length>0){
                obj[0].giftQty+=gift.giftQty
              }else{
                gifts.push({
                  giftId:gift.giftId,
                  giftName:gift.giftName,
                  price:gift.giftPrice,
                  giftQty:gift.giftQty
                })
              }
            
            });
          }        
          });
          mgrVisit=res[0].mgrData.length
         

        }
      })
    

      data["gifts"]=gifts;
      data["products"]=products; 
      data["mgrVisit"]=mgrVisit; 

       this.isShowProviderTable=true;
       this.dataSource2 = new MatTableDataSource([data]);
       setTimeout(() => {
         this.dataSource.paginator = this.paginator;
       }, 100);
       !this._changeDetectorRef["destroyed"]
         ? this._changeDetectorRef.detectChanges()
         : null;
       
    }
    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    exporttoExcel() {
      // download the file using old school javascript method
      this.exportAsService
        .save(this.exportAsConfig, "Sample Details")
        .subscribe(() => {
          // save started
        });
    }

    viewRecords(){
      this.isToggled = false;
      this.isShowReport = false;
      this.isShowProviderTable=false;
      this.showProcessing = true;
      let userIds;
      if (this.sampleForm.value.type == "Self" || this.sampleForm.value.type == null) { //Null is for MR level
        userIds = [this.currentUser.id];
      } else {
        userIds = this.sampleForm.value.employeeId
      }
      let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.sampleForm.value.division;
    }
      
      this._dcrProviderVisitService.getProviderVisitDetails(this.currentUser.companyId, this.sampleForm.value.type, this.sampleForm.value.stateInfo, this.sampleForm.value.districtInfo, userIds, this.sampleForm.value.fromDate, this.sampleForm.value.toDate, ["RMP","Drug","Stockist"], this.currentUser.company.validation.unlistedDocCall, this.currentUser.company.isDivisionExist, divisionIds,[]).subscribe(res=>{
        if (res == null || res.length == 0 || res == undefined) {
          this.showProcessing = false;
          this.showReport = false;
          this.toastr.info('For Selected Filter data not found', 'Data Not Found');
        } else {
          this.showReport=true;	
          this.showProcessing = false;
				
          !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
        this.dataSource = new MatTableDataSource(res);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 100);
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
          
        }
      })
    }

}
