import { ToastrService } from 'ngx-toastr';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef, EventEmitter, Output,Input } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator, MatSlideToggleChange } from '@angular/material';
import { EdituserComponent } from '../edituser/edituser.component';
declare var $;
import Swal from 'sweetalert2'
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { BlockUserComponent } from '../block-user/block-user.component';
import * as _moment from 'moment';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { BlockUserService } from '../../../../../core/services/BlockUser/block-user.service';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StateService } from '../../../../../core/services/state.service';
import { FileHandlingService } from '../../../../../core/services/FileHandling/file-handling.service';
import { StatusComponent } from '../../filters/status/status.component';

@Component({
  selector: 'm-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  @Input() headerLogo: any = '';
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent
  @ViewChild("userDataTable") dataTable1;
  userDataTable: any;
  userDTOptions: any;
  @Output() valueChange = new EventEmitter();
  stateList;
  clicks = 0;
  check: boolean;
  search: FormGroup;
  isShowUsersInfo : boolean = false;
  showProcessing : boolean= false;
  displayedColumns = ['stateName', 'districtName', 'name', 'designation','deactivationDate','dateOfJoining', 'lockingPeriod', 'email', 'mobile', 'aadhaar', 'pan', 'username', 'password', 'action', 'action1'];
  dataSource = new MatTableDataSource<any>();
  showDivisionFilter: boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  isShowDivision = false;
  showAdminAndMGRLevelFilter = false;
  isDivisionExist = this.currentUser.company.isDivisionExist;
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showEmployee: boolean = false;
  public hidecoloumn : boolean =true;
  obj = JSON.parse(sessionStorage.currentUser);
  maxAllowedUsers;
  currentActiveUsers;
  logoURL: any;
  queryObject;
  currentInactiveUsers;
  queryInactiveObject;
  modifyButtonCount:number = 0;
  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private changeDetectedRef: ChangeDetectorRef,
    private userDetailsService: UserDetailsService,
    private _hierarchyService: HierarchyService,
    private _blockUserService: BlockUserService,
    private stateService: StateService,
    private _urlService: URLService,
    private _fileHandlingService: FileHandlingService,

  ) {
  }
  changedValue(val) {
    this.valueChange.emit(val);
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
   // this.logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
    this.queryObject = {
      'where': {
        companyId: this.obj.companyId,
        status: true
      }
    };
    this.queryInactiveObject = {
      'where': {
        companyId: this.obj.companyId,
        status: false
      }
    };
    this.maxAllowedUsers = this.obj.company.maxUser;
    this.getCurrentActiveUsers();
    this.getCurrentInActiveUsers()
    if (this.currentUser.company.isDivisionExist == true) {
      this.userForm.setControl('division', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
      this.showAdminAndMGRLevelFilter = true;
    }
    else {
      this.isShowDivision = false;
    } if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }

  }
  getCurrentActiveUsers() {
  
    this.userDetailsService.getUsersList(this.queryObject).subscribe(res => {

      this.currentActiveUsers = res.length;
    })
  }
  getCurrentInActiveUsers() {
    this.userDetailsService.getInactiveUsersList(this.queryInactiveObject).subscribe(res => {

      this.currentInactiveUsers = res.length;
    })
  }
  userForm = new FormGroup({
    reporttype: new FormControl(null),
    type: new FormControl(null, Validators.required),
    divisionId: new FormControl(null),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employees: new FormControl(null),
    designation: new FormControl(null),
    status: new FormControl([true]),
  })

  getDivisionValue(val) {
    this.isShowUsersInfo = false;

    this.userForm.patchValue({ division: val });

    if (this.currentUser.userInfo[0].designationLevel == 0) {
      //=======================Clearing Filter===================
	  this.callStateComponent.setBlank();
      //===============================END=======================
      let divisionIds = {
        division: val
      };
      this.callStateComponent.getStateBasedOnDivision(divisionIds);
    }

  }
  getStateValue(val) {
    this.userForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.userForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.currentUser.companyId,
          "stateId": this.userForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.userForm.value.division,
        })
        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.userForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getReportType(val) {
    if (val === "Geographical") {
      this.userForm.reset();
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.userForm.reset();
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }

  }
  getTypeValue(val) {
    this.isShowUsersInfo = false;
    if (val == "State") {
      this.userForm.reset();
      this.showState = true;
      this.showDistrict = false;
      this.showEmployee = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
	  this.userForm.setControl("stateInfo", new FormControl('', Validators.required))
	  this.userForm.setControl("status", new FormControl([true]))
      this.userForm.removeControl("employees");
      this.userForm.removeControl("designation");
    } else if (val == "Headquarter") {
      this.userForm.reset();
      this.showEmployee = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.userForm.setControl("stateInfo", new FormControl('', Validators.required))
	  this.userForm.setControl("districtInfo", new FormControl('', Validators.required))
	  this.userForm.setControl("status", new FormControl([true]))
      this.userForm.removeControl("employees");
      this.userForm.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.userForm.reset();
      this.showEmployee = true;
      this.showState = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.userForm.removeControl("stateInfo");
	  this.userForm.removeControl("districtInfo");
	  this.userForm.setControl("status", new FormControl([true]))
      this.userForm.setControl("designation", new FormControl('', Validators.required));
      this.userForm.setControl("employees", new FormControl('', Validators.required));
    } else if (val == "Self") {
      this.userForm.reset();
      this.showEmployee = false;
    } if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }
    this.userForm.patchValue({ type: val });
    this.changeDetectedRef.detectChanges();
  }

  getDistrictValue(val) {
    this.userForm.patchValue({ districtInfo: val });
  }


  getDesignation(val) {
	this.callStatusComponent.setBlank();
    this.userForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.currentUser.companyId,
        division: this.userForm.value.division,
        status: [true],
        designationObject: this.userForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
	}
  }

  getEmployeeValue(val) {
    this.userForm.patchValue({ employees: val });
  }

//**************************************************************Umesh Kumar 13-05-2020**************************************************
  getStatusValue(status: boolean) {
	this.userForm.patchValue({ status: status });
  }

  getUsers() {
	this.isToggled = false;
    this.isShowUsersInfo = false;
    this.showProcessing=true;
    this.stateService.getStates("5b2e22083225df01ba7d6059").subscribe(res => {
      
      this.stateList = res;
     
      res.map((item, index) => {
        this.showProcessing=false;
        
        item['index'] = index;
	  });
      

    }, err => {
      console.log(err)
      this.showProcessing=false;

      alert("No State Found");
    });

let row=0;
    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.userForm.value.division;
    }

    let object = {
      "companyId": this.currentUser.companyId,
      "userId": this.userForm.value.employees,
      "type": this.userForm.value.type,
      "stateIds": this.userForm.value.stateInfo,
      "isDivisionExist": this.currentUser.company.isDivisionExist,
      "division": divisionIds,
      "district": this.userForm.value.districtInfo,
      "status": this.userForm.value.status,
	}

    this.userDetailsService.getUsers(object).subscribe(res => {
      console.log("This--------------------------->",res);
      this.isShowUsersInfo = true;
      res.forEach((item, index) => {
        item['index'] = index;
        let immediateManager = null;
        item.manager.forEach((i, indexi)=> {

          if(!immediateManager){
            if(i.status === true){
              immediateManager = i;
            }
          } else {
            if(immediateManager.supervisorDesignationLevel > i.supervisorDesignationLevel && i.status === true){
              immediateManager = i;
            }
          }

          // if(indexi == 0){
          //   immediateManager = i;
          // } else{
          //   if(immediateManager.supervisorDesignationLevel > i.supervisorDesignationLevel && i.status === true){
          //     immediateManager = i;
          //   }
          // }
          (indexi === item.manager.length-1)? item['immediateManager'] = immediateManager : null ;
        });
	    });
    const PrintTableFunction = this.PrintTableFunction.bind(this);
      this.userDTOptions = {
        "pagingType": 'full_numbers',
        "paging": true,
        "ordering": true,
        "info": true,
        "scrollY": 300,
        "scrollX": true,
        "fixedColumns": true,
        destroy: true,
        data: res,
        responsive: true,
        columns: [
          {
            title: 'S. No.',
            data: 'index',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data + 1;
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Division',
            data: 'divisionName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'State Name',
            data: 'stateName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: this.currentUser.company.lables.hqLabel + " Name",
            data: 'districtName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
		      },
          // {
          //   title: "DaCategory",
          //   data: 'daCategory',
          //   render: function (data) {
          //     if (data === '') {
          //       return '---'
          //     } else {
          //       return data
          //     }
          //   },
          //   defaultContent: '---'
          // },
		      {
            title: "Employee Code",
            data: 'employeeCode',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "DACategory",
            data: 'daCategory',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Name",
            data: { 'name': 'name', 'blockedStatus': 'blockedStatus' },
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data.name
              }
            },
            defaultContent: '---'
		      },
		      {
            title: "Designation",
            data: 'designation',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },

        

          {
            title: "Locking Period",
            data: 'lockingPeriod',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "Deactivation Date",
            data: 'deactivationDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
               return _moment(data).format("DD-MM-YYYY")
              }
            },
            defaultContent: '---'
          },
          {
            title: "Date Of Joining",
            data: 'dateOfJoining',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
               return _moment(data).format("DD-MM-YYYY")
              }
            },
            defaultContent: '---'
          },
          {
            title: "Date Of Birth",
            data: 'dateOfBirth',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
               return _moment(data).format("DD-MM-YYYY")
              }
            },
            defaultContent: '---'
          },
          {
            title: "Email",
            data: 'email',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "Mobile",
            data: 'mobile',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
         
          
          {
            title: "Aadhaar",
            data: 'aadhaar',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data 
              }
            },
            defaultContent: '---'
          }, {
            title: "PAN",
            data: 'pan',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Manager",
            data:  "immediateManager",
            render: function (data) {

              if(data){
                return data.supervisorName
              } else {
                return '---'
              }
              // let designationlev = 1000;
              // let manager;
              // if(data.length){
              //   for(let i = 0; i < data.length; i++){
              //     if(data[i].supervisorDesignationLevel){
              //       if(data[i].supervisorDesignationLevel < designationlev){
              //         manager = data[i].supervisorName;
              //       }
              //     }
              //   }
              //   return manager;
              // }
            },
            defaultContent: '---'
          },
          {
            title: "Username",
            data: 'username',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "Password",
            data: 'password',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "Status",
            data: 'status',
            render: function (data) {
              if (data === '' || data === undefined) {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Bank Name",
            data: 'bankName',
            render: function (data) {
              if (data === '' || data === undefined) {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Bank Account No.",
            data: 'bankAccountNumber',
            render: function (data) {
              if (data === '' || data === undefined) {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Bank IFSC",
            data: 'ifsc',
            render: function (data) {
              if (data === '' || data === undefined) {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Deactivate Reason",
            data: 'statusReason',
            render: function (data) {
              if (data === '' || data === undefined) {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Block Reason",
            data: 'blockReason',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "Block Date",
            data: 'blockDate',
            render: function (data) {
              if (data === '' || data === undefined) {
                return '---'
              } else {
                return _moment(data).format("DD-MMM-YYYY");
              }
            },
            defaultContent: '---'
          }, {
            title: "Release Date",
            data: 'releaseDate',
            render: function (data) {
              if (data === '' || data === undefined || data === null) {
                return '---'
              } else {
                return _moment(data).format("DD-MMM-YYYY");
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Edit',
            defaultContent: "<button id='modifyId' class='btn btn-warning btn-sm'><i class='la la-edit'></i>&nbsp;&nbsp;Modify</button>"
          },
          {
            title: 'Change Status',

            data: 'status',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                let status = "Deactivate";
                if (data == true) {
                  return "<button id='inActiveButtonId' class='btn btn-warning btn-sm'><i class='la la-check'></i>&nbsp;&nbsp;" + status + "</button>"
                } else if (data == false) {
                  status = "Activate";
                  return "<button id='activeButotonId' class='btn btn-warning btn-sm'><i class='la la-check'></i>&nbsp;&nbsp;&nbsp;&nbsp" + status + "</button>"
                }
              }
            }
          },
          {
            title: 'Block Statuss',
            data: 'blockedStatus',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                let status = "UnBlock";
                if (data == true) {
                  return "<button id='unblockedButotonId' class='btn btn-warning btn-sm'><i class='fa fa-unlock'></i>&nbsp;&nbsp;" + status + "</button>"
                } else if (data == false) {
                  status = "Block";
                  return "<button id='blockedButtonId' class='btn btn-warning btn-sm'><i class='fa fa-lock'></i>&nbsp;&nbsp;&nbsp;&nbsp" + status + "</button>"
                }
              }
            }
          }
          // ,
          // {
          //   title: 'Deactivate',
          //   defaultContent: "<button id='deleteId' class='btn btn-warning's><i class='la la-trash-o'></i>&nbsp;&nbsp;Deactivate</button>"
          // }

        ],
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;

          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td', row).unbind('click');

          $('td button', row).bind('click', (event) => {

            //console.log("row : ", data);
            //console.log(event.target.id);
            let rowObject = JSON.parse(JSON.stringify(data));
            if (event.target.id == "modifyId") {
              if(this.modifyButtonCount === 0){
                this.modifyButtonCount++;  //For Stopping multiple click events.
                self.editUser(rowObject);
              }
            } else if (event.target.id == "inActiveButtonId") {
              //Making 
              self.updatedUserStatus(rowObject, false);
            } else if (event.target.id == "activeButotonId") {
              //Making Active
              if (this.currentActiveUsers < this.maxAllowedUsers) {
                self.updatedUserStatus(rowObject, true);
              } else {
                //--- Nipun 07-01-2020------
                this.toastr.error(`At a time only ${this.maxAllowedUsers} active users are allowed `);
              }

            } else if (event.target.id == "blockedButtonId") {
              //Making Block
              self.Block_Unblock_User(rowObject, false);
            } else if (event.target.id == "unblockedButotonId") {
              //Making Unlock
              self.Block_Unblock_User(rowObject, true);
            }
          });
          this.getCurrentActiveUsers();
          this.getCurrentInActiveUsers();
          return row;
        },
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search Records",
        },
        dom: 'Bfrtip',
        buttons: [

          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }

          },
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible'
            }
          },

//********************* UMESH 17-03-2020 ************************ */

{
  extend: "print",
  title: function () {
  return "State List";
  },
  exportOptions: {
  columns: ":visible"
  },
  action: function (e, dt, node, config) {
  PrintTableFunction(res);
  }
  },



// {
          //   extend: 'print',
          //   exportOptions: {
          //     stripHtml: true,
          //     columns: ':visible'
          //   },
          //   customize: (win) => {
          //     const designation = this.currentUser.designation;
          //     var css = '@page { size: landscape; }',
          //       head = win.document.head || win.document.getElementsByTagName('head')[0],
          //       style = win.document.createElement('style');
          //     style.type = 'text/css';
          //     style.media = 'print';

          //     if (style.styleSheet) {
          //       style.styleSheet.cssText = css;
          //     }
          //     else {
          //       style.appendChild(win.document.createTextNode(css));
          //     }
          //     head.appendChild(style);
          //     $(win.document.body)
          //       .css('font-size', '8px')
          //       .prepend(
          //         `<img src='${this.logoURL}' style="width:200px;height:50px;margin-right:5px;" />;
          //         <div>
          //          <h2 style="float:left; margin-right:7px"> &nbsp;${designation}</h2>
          //          <h2 style="float:right; margin-right:5px">User Details</h2>
          //         </div>
          //         `
          //       );
          //     $(win.document.body).find('table')
          //       .addClass('compact')
          //       .css('font-size', 'inherit', 'direction', 'rtl');
          //   }
          // }
          //********************End ********/

        ]
      };

      if (this.currentUser.company.isDivisionExist == false) {
        this.userDTOptions.columns[1].visible = false;
        this.userDTOptions.columns[5].visible = false;
      }
      
      if(this.currentUser.companyId=="5cd162dd50ce3f0f80f64e14"){
        this.userDTOptions.columns[5].visible = true;
      }

      if(this.userForm.value.status[0]==true){
        
        this.userDTOptions.columns[22].visible = false;
      }

      if (this.currentUser.company.validation.isUserBlockCondition == false) {
        this.userDTOptions.columns[23].visible = false;
        this.userDTOptions.columns[24].visible = false;
        this.userDTOptions.columns[25].visible = false;
        this.userDTOptions.columns[28].visible = false;
      }else{
        this.userDTOptions.columns[23].visible = true;
        this.userDTOptions.columns[24].visible = true;
        this.userDTOptions.columns[25].visible = true;
        this.userDTOptions.columns[28].visible = true;
      }

      // ========= Rahul Saini 15-05-2020 Prevent buttons to print in excel ==========
      if (this.currentUser.company.isDivisionExist == true) {

        if(this.userForm.value.status[0]==true){
          this.userDTOptions.buttons.push({
            extend: 'excel',
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20]
            }
          })
        }
        else{
          this.userDTOptions.buttons.push({
            extend: 'excel',
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
            }
          })

        }

    } else {
      if(this.userForm.value.status[0]==true){
        this.userDTOptions.buttons.push({
          extend: 'excel',
          exportOptions: {
            columns: [0,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20]
          }
        })
      }
      else{
        this.userDTOptions.buttons.push({
          extend: 'excel',
          exportOptions: {
            columns: [0,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
            
            
            ]
          }
        })

      }
    }
    // ========= End ============================================================

      this.changeDetectedRef.detectChanges();
      this.userDataTable = $(this.dataTable1.nativeElement);
      this.userDataTable.DataTable(this.userDTOptions);
      this.changeDetectedRef.detectChanges();

    },
      err => {
        console.log(err);
      });
  }
  //---------------------edit user------------------------------------
  editUser(obj) {
    const dialogRef = this.dialog.open(EdituserComponent, {
      data: { obj }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.modifyButtonCount = 0; // Resetting count : For blocking multiple click event on modify button.
      this.getUsers();
    });
  }


  //----------Delete user--------------------------
  updatedUserStatus(obj, changeTo) {
    let msg = "Enter the Inactivated Reason";
    let buttonMsg = "Yes, Activate it !!!";
    if (changeTo == false) {
      msg = "The User Will Be Deactivated.";
      buttonMsg = "Yes, Deactivate it !!!";
    }
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })
    // Swal.fire({
    //   title: 'Are you sure?',
    //   text: 'Enter TP Rejection Message',
    //   input: 'text',
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Yes, Reject it!'
    // }).then((result) => {})

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: 'Enter the User Inactivation Reason',
      input: 'text',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Reject it!'
    }).then((result) => {
      if (result.value) {
        console.log("result value=>",result.value);
        
        this.userDetailsService.updatedUserStatus(obj.id, this.currentUser.company.id, this.currentUser.id, changeTo,result.value).subscribe(userDelResponse => {
          this.getUsers();
          this.toastr.success('User has been Updated Successfully !!!');
        }, err => {
          this.toastr.error('Error in User Updation!!');
        });
      }
    });
    this.getCurrentActiveUsers();

  }


  Block_Unblock_User(obj: any, changeTo) {
    //console.log(obj);

    let msg = "The User Will Be Unblocked.";
    let buttonMsg = "Yes, Unblock it !!!";
    if (changeTo == false) {
      msg = "The User Will Be Blocked.";
      buttonMsg = "Yes, Block it !!!";
    }
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success btn-sm',
        cancelButton: 'btn btn-danger btn-sm'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: msg,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: buttonMsg,
      cancelButtonText: 'No, cancel',
      reverseButtons: true,
      allowOutsideClick: false,
      width: '280px'
    }).then((result) => {
      if (result.value) {
        if (changeTo) {
          //Unblock Here
          if (obj.designationLevel > 1) {
            obj = { ...obj, 'callingFor': 'Unblock' }

            const passingObject = {
              data: obj, //optional
              width: '70%',
              height: '55%'
            }
            const dialogRef = this.dialog.open(BlockUserComponent, passingObject);
            dialogRef.afterClosed().subscribe(result => {
              
              result.previousTeamAndManager.forEach(element => {
                const objectForUpdateHierarchy = {
                  where: {
                    userId: element.teamMemberId,
                    supervisorId: element.newManagerDetails.userId,
                    blockedFrom: 'UserBlockModule'
                  },
                  dataToBeUpdate: {
                    status: false,
                    updatedAt: new Date(),
                    blockReason: result.blockReason,
                    blockDate: result.blockDate,
                    releaseDate: result.releaseDate,
                    blockedBy: this.currentUser.id
                  }
                }
                this._hierarchyService.updateHierarchy(
                  objectForUpdateHierarchy.where, 
                  objectForUpdateHierarchy.dataToBeUpdate).
                  subscribe(blockUserResult => {
                
                });

              });


              const passingObject = {
                where: {
                  companyId: obj.companyId,
                  id: result.userBlockRowId
                },
                dataToBeUpdate: {
                  releaseDate: new Date(),
                  updatedAt: new Date(),
                  blockReason: result.blockReason,
                  blockDate: result.blockDate,
                  blockedBy: this.currentUser.id
                }
              }
              this._blockUserService.updateBlockUserInfo(
                passingObject.where,
                passingObject.dataToBeUpdate).
                subscribe(blockUserResult => {
                const createNewManagerHierarchy: Array<Object> = [];
                result.teamData.forEach(element => {

                  createNewManagerHierarchy.push({
                    "userId": element.teamMemberId,
                    "companyId": this.currentUser.companyId,
                    "userName": element.name,
                    "userDesignation": element.designation,
                    "userDesignationLevel": element.designationLevel,
                    "supervisorId": element.newManagerId,
                    "supervisorName": element.newManagerDetails.name,
                    "supervisorDesignation": element.newManagerDetails.designation,
                    "supervisorDesignationLevel": element.newManagerDetails.designationLevel,
                    "immediateSupervisorId": element.newManagerId,
                    "immediateSupervisorName": element.newManagerDetails.name,
                    "status": true
                  });
                });
                this._hierarchyService.createHierarchyNew(createNewManagerHierarchy).subscribe(hierarchyResult => {
                });
                //------------------------Update UserInfo-----------------------------
                const whereForUserInfo = {
                  companyId: obj.companyId,
                  userId: obj.id
                }
                const updateObjForUserInfo = {
                  userBlockId: '', //Clear User Blocked Id
                  UpdatedAt: new Date(),
                  name:obj.name
                }
                this.userDetailsService.updateUserInfo(whereForUserInfo, updateObjForUserInfo).subscribe(userInfoResult => {
                  this.getUsers();
                  this.toastr.success('User has been Unblocked Successfully !!!');

                });
              });
            });
          } else {
            const whereForUserInfo = {
              companyId: obj.companyId,
              userId: obj.id
            }
            const updateObjForUserInfo = {
              userBlockId: '', //Clear User Blocked Id
              UpdatedAt: new Date(),
              name:obj.name
            }
            this.userDetailsService.updateUserInfo(whereForUserInfo, updateObjForUserInfo).subscribe(userInfoResult => {

              const passingObject = {
                where: {
                  companyId: obj.companyId,
                  id: obj.userBlockRowId
                },
                dataToBeUpdate: {
                  releaseDate: new Date(),
                  updatedAt: new Date(), 
                  blockDate: new Date(),
                 
                  blockedBy: this.currentUser.id
                }
              }
              this._blockUserService.updateBlockUserInfo(passingObject.where, passingObject.dataToBeUpdate).subscribe(blockUserResult => {
                this.getUsers();
                this.toastr.success('User has been Unblocked Successfully !!!');
              });

            });


          }
        } else {
          //Block Here
          obj = { ...obj, 'callingFor': 'Block' }
          let passingObject = {
            data: obj, //optional
            width: 'auto',
            height: '30%'
          }
          if (obj.designationLevel > 1) {
            passingObject = {
              data: obj, //optional
              width: '70%',
              height: '55%'
            }
          }
          const dialogRef = this.dialog.open(BlockUserComponent, passingObject);
          dialogRef.afterClosed().subscribe(result => {
            let passingObject = {};
            if (obj.designationLevel > 1) {
              const passingObject = {
                userId: obj.id,
                companyId: obj.companyId,
                blockReason: result.blockReason,
                blockDate: result.blockDate,
                releaseDate: result.releaseDate,
                transferHierarchyTo: result.teamData,
                blockedBy: this.currentUser.id
              }

              const createNewManagerHierarchy: Array<Object> = [];
              result.teamData.forEach(element => {
                createNewManagerHierarchy.push({
                  "userId": element.teamMemberId,
                  "companyId": this.currentUser.companyId,
                  "userName": element.name,
                  "userDesignation": element.designation,
                  "userDesignationLevel": element.designationLevel,
                  "supervisorId": element.newManagerId,
                  "supervisorName": element.newManagerDetails.name,
                  "supervisorDesignation": element.newManagerDetails.designation,
                  "supervisorDesignationLevel": element.newManagerDetails.designationLevel,
                  "immediateSupervisorId": element.newManagerId,
                  "immediateSupervisorName": element.newManagerDetails.name,
                  "status": true,
                  "blockedFrom": 'UserBlockModule'
                });
              });
              this._blockUserService.createBlockUser(passingObject).subscribe(blockUserResult => {
                const whereForHierarchyToBeFalse = {
                  supervisorId: obj.id
                }
                this._hierarchyService.updateHierarchy(whereForHierarchyToBeFalse, { status: false, UpdatedAt: new Date() }).subscribe(userInfoResult => {
                  this._hierarchyService.createHierarchyNew(createNewManagerHierarchy).subscribe(hierarchyResult => {
                    
                  });
                });
                //------------------------Update UserInfo-----------------------------
                const whereForUserInfo = {
                  companyId: obj.companyId,
                  userId: obj.id
                }
                const updateObjForUserInfo = {
                  userBlockId: blockUserResult.id,
                  UpdatedAt: new Date(),
                  name:obj.name+' (BLOCK)'
                }
                this.userDetailsService.updateUserInfo(whereForUserInfo, updateObjForUserInfo).subscribe(userInfoResult => {
                  this.getUsers();
                  this.toastr.success('User has been Blocked Successfully !!!');
                });
              });
            } else {

              //---------------------------------------- Create Block User Data --------------------------------------------
              const passingObject = {
                userId: obj.id,
                companyId: obj.companyId,
                blockReason: result.blockReason,
                blockDate: result.blockDate,
                releaseDate: result.releaseDate,
                blockedBy: this.currentUser.id
              }
              this._blockUserService.createBlockUser(passingObject).subscribe(blockUserResult => {
                
                //------------------------Update UserInfo-----------------------------
                const whereForUserInfo = {
                  companyId: obj.companyId,
                  userId: obj.id
                }
                const updateObjForUserInfo = {
                  userBlockId: blockUserResult.id,
                  updatedAt: new Date(),
                  name:obj.name +' (BLOCK)'
                }
                this.userDetailsService.updateUserInfo(whereForUserInfo, updateObjForUserInfo).subscribe(userInfoResult => {
                  this.getUsers();
                  this.toastr.success('User has been Blocked Successfully !!!');
                });
              });
            }


          });
        }
      }
    });


  }

///*****************************************UMESH 18-03-2020 */


PrintTableFunction(data?: any): void {
  //const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
  const currentDateAndTime = _moment(new Date()).format('DD/MM/YYYY, dddd , HH:mm A');
  const department = this.currentUser.section;
  const designation = this.currentUser.userInfo[0].designation;
  const companyName = this.currentUser.companyName;

  const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

  let tableRows: any = [];
  data.forEach(element => {
  tableRows.push(`<tr>
  ${ this.isDivisionExist ? `<td class="tg-oiyu">${element.divisionName}</td>`: "" } 
  <td class="tg-oiyu">${element.stateName}</td>
  <td class="tg-oiyu">${element.districtName}</td>
  <td class="tg-oiyu">${element.name}</td>
  <td class="tg-oiyu">${element.designation}</td>
  <td class="tg-oiyu">${element.lockingPeriod}</td>
  <td class="tg-oiyu">${element.email == undefined?"":element.email}</td>
  <td class="tg-oiyu">${element.mobile}</td>
  <td class="tg-oiyu">${element.aadhar == undefined?"":element.aadhaar}</td>
  <td class="tg-oiyu">${element.pan == undefined ?"":element.pan}</td>
  <td class="tg-oiyu">${element.username}</td>
  <td class="tg-oiyu">${element.password}</td>
  <td class="tg-oiyu">${element.status}</td>
  </tr>`)
  });

  let showHeaderAndTable: boolean = false;
  //this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;

  let printContents, popupWin;
  popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  popupWin.document.open();
  popupWin.document.write(`
  <html>
  <head>
  <style>
  img {
  float: left;
  }

  .flex-container {
  display: flex;
  justify-content: space-between;
  }

  .tg {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tg td {
  font-family: Arial, sans-serif;
  font-size: 14px;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg th {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg .tg-lboi {
  border-color: inherit;
  text-align: left;
  vertical-align: middle;

  }

  .tg .tg-u6fn {
  font-weight: bold;
  font-size: 18px;
  background-color: #efefef;
  color:black;
  border-color: inherit;
  text-align: left;
  vertical-align: top;
  padding-top: 0px;
  padding-bottom: 0px;
  }

  .tg .tg-yz93 {
  border-color: inherit;
  text-align: right;
  vertical-align: middle
  }

  .tb2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tb2 td {
  font-family: Arial, sans-serif;
  font-size: 12px;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tb2 th {
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-weight: normal;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tb2 .tg-oiyu {
  font-size: 10px;
  text-align: left;
  vertical-align: middle
  }
  .tg-bckGnd {
  font-weight: bold;
  background-color: #efefef;
  }


  .tb2 .tg-3ppa {
  font-size: 10px;
  background-color: #efefef;
  color:black;
  text-align: left;
  vertical-align: middle;
  padding-top: 2px;
  padding-bottom: 2px;
  }

  .footer {
  position: sticky;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #efefef;
  color: rgb(2, 2, 2);
  text-align: right
  }



  button {
  background-color: #008CBA;
  border: none;
  color: white;
  margin: 5px 0px;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  }

  @media print {
  @page { size: A4; margin: 5mm; }

  .footer {
  position: relative;
  }
  tr {
  page-break-inside: avoid;
  }

  button{
  display : none;
  }
  }
  </style>
  </head>
  <body onload="window.print();window.close()" style="padding-bottom:30px">
  <div class="flex-container">
  <div style="width: 40%; ">
  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

  </div>

  <div style="text-align: right;">
  <h2 style="margin-top: 0px; margin-bottom: 5px">User Details</h2>
  <div>
  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
  </div>
  </div>
  </div>

  <div>
  ${/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/''}
  <table class="tb2">
  <tr>
  ${
		this.isDivisionExist
			? `<th class="tg-3ppa"><span style="font-weight:600">Division Name</span></th>`
			: ""
	} 
  <th class="tg-3ppa"><span style="font-weight:600">State Name</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Headquarter Name</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Name</span></th>z
  <th class="tg-3ppa"><span style="font-weight:600">Designation</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Locking Period</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Email</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Mobile</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Aadhaar</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">PAN</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Username</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Password</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Status</span></th>
  </tr>
  ${tableRows.join("")}
  </table>
  </div>
  <div class="footer flex-container">
  <p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>
  <p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
  </div>
  </body>
  </html>
  `);

  //popupWin.document.close();

  }



}
