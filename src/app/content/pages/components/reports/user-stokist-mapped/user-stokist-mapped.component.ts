import { SelectionModel } from '@angular/cdk/collections';
import { DistrictComponent } from '../../filters/district/district.component';
import { URLService } from '../../../../../core/services/URL/url.service';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { StockiestComponent } from '../../filters/stockiest/stockiest.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatSnackBar, MatSlideToggleChange, MatPaginator, MatTableDataSource } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import Swal from "sweetalert2";
@Component({
  selector: 'm-user-stokist-mapped',
  templateUrl: './user-stokist-mapped.component.html',
  styleUrls: ['./user-stokist-mapped.component.scss']
})
export class UserStokistMappedComponent implements OnInit {
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
	@ViewChild(MatPaginator) paginator: MatPaginator;

  check: boolean;

  selection = new SelectionModel(true, []);
  displayedColumns = ['select', 'districtName', 'stockistName', 'userName'];

  dataSource;
  isShowUserStokistMapped = false;

  constructor(private _ToastrService: ToastrService, private snackBar: MatSnackBar, private urlService: URLService, private stockiestService: StockiestService, private userDetailService: UserDetailsService, private changedetectorref: ChangeDetectorRef) { }
  currentUser = JSON.parse(sessionStorage.currentUser);
  viewForm = new FormGroup({
    stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null, Validators.required),
    employeeId: new FormControl(null, Validators.required),
  })

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  getStateValue(val) {
    this.viewForm.patchValue({ stateInfo: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
  }

  ngOnInit() {
  }

  getEmployeeListDistrictsBasis(val) {
    this.viewForm.patchValue({ districtInfo: val });
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, val);
  }
  getEmployeeValue(val) {
    this.viewForm.patchValue({ employeeId: val });
  }

  viewRecords() {
    this.isToggled = false;
    this.stockiestService.getUserStockistMappedDetail(this.currentUser.companyId, this.viewForm.value.districtInfo, this.viewForm.value.employeeId).subscribe(res => {
      if (res.length == 0 || res == undefined) {
        this.isShowUserStokistMapped = false;
        this._ToastrService.error('No Record Found');
      } else {
        this.isShowUserStokistMapped = true;
        this.dataSource = new MatTableDataSource(res);
        setTimeout(()=>{
          this.dataSource.paginator = this.paginator;
        }, 100);
        (!this.changedetectorref['destroyed']) ? this.changedetectorref.detectChanges() : null;
      }
    }, err => {
      console.log(err)
      this._ToastrService.error('No Record Found');
    })

  }

  //mapping stockiest
  delete(selectedStockiest) {
    this.stockiestService.deleteStockists(selectedStockiest)
      .subscribe(mapStokist => {
        this.isShowUserStokistMapped = false;
        // this.openSnackBar('Stockiest Deleted Successfully.', '');
        (Swal as any).fire({
          title: "Stockist Deleted Successfully !!",
          type: "success",
        });
        this.selection.clear();
        this.viewRecords();
      }, err => {
        this.isShowUserStokistMapped = false;
        // this.openSnackBar('Stockist Deleted Successfully.', '');
        (Swal as any).fire({
          title: "Something went wrong !!",
          type: "error",
        });
        console.log(err)
      });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(element => {
        this.selection.select(element)
      });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
