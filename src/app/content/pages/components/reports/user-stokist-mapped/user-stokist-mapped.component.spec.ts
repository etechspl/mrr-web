import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserStokistMappedComponent } from './user-stokist-mapped.component';

describe('UserStokistMappedComponent', () => {
  let component: UserStokistMappedComponent;
  let fixture: ComponentFixture<UserStokistMappedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserStokistMappedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserStokistMappedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
