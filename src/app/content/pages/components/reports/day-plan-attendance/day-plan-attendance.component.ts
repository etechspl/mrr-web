import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TypeComponent } from '../../filters/type/type.component';
import { ActivityService } from '../../../../../core/services/Activity/activity.service';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { MatSlideToggleChange } from '@angular/material';
import { promise } from 'protractor';
import { resolve } from 'path';
import { reject, values } from 'lodash';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { data } from 'jquery';
import { arrayify } from 'tslint/lib/utils';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';

@Component({
  selector: 'm-day-plan-attendance',
  templateUrl: './day-plan-attendance.component.html',
  styleUrls: ['./day-plan-attendance.component.scss']
})
export class DayPlanAttendanceComponent implements OnInit {

  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  resultprovider:any;
  dataSource: any;
  columns = [];
  public showDCRSummary: boolean = false;
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  showProcessing: boolean = false;
  showAdminAndMGRLevelFilter = false;
  workingType = [];
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  constructor(private changeDetectorRef: ChangeDetectorRef,
    private _tpService: TourProgramService,
    private _toastr: ToastrService,
    private _activityService: ActivityService,
    private changedetectorref: ChangeDetectorRef,
    private exportAsService: ExportAsService,
    private _userDetailsService: UserDetailsService,
    private _providerService : ProviderService
  ) { }


  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;


  attendanceForm = new FormGroup({
    companyId: new FormControl(this.currentUser.companyId),
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    isDivisionExist: new FormControl(this.isDivisionExist),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    month: new FormControl(null, Validators.required),
    year: new FormControl(null, Validators.required),
    designation: new FormControl(null),
    selectedWorkingTypes: new FormControl([]),
    totalWorkingTypes: new FormControl([])

  });
  ngOnInit() {

    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.attendanceForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }


    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
    this._activityService.getActivities(this.currentUser.companyId).subscribe(res => {
      this.workingType = res[0].activitiesForWeb;
      this.attendanceForm.patchValue({ totalWorkingTypes: res[0].activitiesForWeb });
      this.changedetectorref.detectChanges();
    }, err => {
      console.log(err)
    });
  }


  getReportType(val) {
    this.showReport = false;
    if (val === "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else {
      this.showState = false;
      this.showDistrict = false;
      //this.isShowDivision = true; /*Gourav*/ 
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }

      this.showState = true;
      this.showDistrict = false;
      this.attendanceForm.setControl("stateInfo", new FormControl('', Validators.required))
      this.attendanceForm.removeControl("employeeId");
      this.attendanceForm.removeControl("designation");

    } else if (val == "Headquarter") {
      this.showState = true;
      this.showDistrict = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.attendanceForm.setControl("stateInfo", new FormControl('', Validators.required))
      this.attendanceForm.setControl("districtInfo", new FormControl('', Validators.required))
      this.attendanceForm.removeControl("employeeId");
      this.attendanceForm.removeControl("designation");

    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.attendanceForm.removeControl("stateInfo");
      this.attendanceForm.removeControl("districtInfo");
      this.attendanceForm.setControl("designation", new FormControl('', Validators.required));
      this.attendanceForm.setControl("employeeId", new FormControl('', Validators.required));

    } else if (val == "Self") {
      this.showEmployee = false;
      this.attendanceForm.removeControl("stateInfo");
      this.attendanceForm.removeControl("districtInfo");
      this.attendanceForm.removeControl("designation");
      this.attendanceForm.setControl("employeeId", new FormControl());

    }
    // show division on change of the type
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.attendanceForm.patchValue({ type: val });

  }


  getDivisionValue($event) {
    this.attendanceForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.attendanceForm.value.type === "State" || this.attendanceForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.attendanceForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.attendanceForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.attendanceForm.value.divisionId != null && this.attendanceForm.value.designation != null) {
          if (this.attendanceForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.attendanceForm.value.divisionId,
              status: [true],
              designationObject: this.attendanceForm.value.designation
            })
          }
        }
      }
    }
  }

  getStateValue(val) {
    this.showReport = false;

    this.attendanceForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.attendanceForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.attendanceForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.attendanceForm.value.divisionId
        })
        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.attendanceForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }


  getDistrictValue(val) {
    this.showReport = false;
    this.attendanceForm.patchValue({ districtInfo: val });
  }


  getDesignation(val) {
    this.showReport = false;

    this.attendanceForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.attendanceForm.value.divisionId,
        status: [true],
        designationObject: this.attendanceForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }

  getEmployeeValue(val) {
    this.showReport = false;
    this.attendanceForm.patchValue({ employeeId: val });
  }
  getMonth(val) {
    this.showReport = false;
    this.attendanceForm.patchValue({ month: val });
  }
  getYear(val) {
    this.showReport = false;
    this.attendanceForm.patchValue({ year: val });
  }




  viewRecords() {
    this.isToggled = false;
    this.showProcessing = true;

    if (this.attendanceForm.value.type == null || this.attendanceForm.value.type == 'Self') {
      this.attendanceForm.patchValue({ employeeId: [this.currentUser.id] });
    }
    this._tpService.getTPDayPlan(this.attendanceForm.value).subscribe((res: any) => {
      this.columns = [];
      if (res.length > 0) {
        this.dataSource = JSON.parse(JSON.stringify(res));
        console.log("hello", this.dataSource);
        let date = 1;
        for (let i = 0; i < this.dataSource[0].dateArray.length; i++) {
          this.columns.push(date);
          date++;
        }
        this.showProcessing = false;
        this.showReport = true;

        this.changeDetectorRef.detectChanges();

      } else {
        this._toastr.info("Data not available")
        this.showProcessing = false;
        this.changeDetectorRef.detectChanges();
        this.showReport = false;
      }

    }, err => {
      this.showReport = false;
      this.showProcessing = false;
      this.changeDetectorRef.detectChanges();
      console.log(err);
    });
    this.changeDetectorRef.detectChanges();


  }
  async getUserattendence(val,date) {

    //console.log("val", val,'-----',date)
    this.showDCRSummary = true;
    let userData = await this.getUserData(val)
    //console.log("Dz",this.attendanceForm.value)
    let filter = {
      where: {
        createdBy: userData[0].userId,
        approvalStatus: true,
        companyId: this.currentUser.companyId,
        month: this.attendanceForm.value.month,
        year:this.attendanceForm.value.year
      }
      }
      this._tpService.getTPExistOfDay(filter).subscribe( async (res) => {
        
      console.log(res[date.day - 1]);
      let dataresponse = res[date.day - 1];
      let response = []
      // dataresponse.plannedArea.forEach(element => {
      //   response.push(element)
      // });

      dataresponse.plannedChemists.forEach(element => {
        response.push(element)
      });
      dataresponse.plannedDoctors.forEach(element => {
        response.push(element)
      });

      const providerdetail = await this.getProvider(response,dataresponse.date)
      console.log("providerdetail",providerdetail)
      this.resultprovider = providerdetail;
      this.changeDetectorRef.detectChanges();

        console.log("resultprovider",this.resultprovider)
      })
    let Obj = {
      createdBy:userData[0].userId,
      companyId: this.currentUser.companyId,
    }
  }
  getProvider(value,date): Promise<any>{
    return new Promise((resolve,reject)=>{
      let filter ={
        where:{
          
          
        },
        include:["block"]
      }
      value.forEach(element => {
        filter.where['id']=element.id

        this._providerService.getProviderCategory(filter).subscribe((res)=>{
          element['providerType'] = res[0].providerType
          element['areaName'] = res[0].block.areaName
          element['date'] = date
        })

      });
      resolve(value)
    })
  }

  getUserData(value): Promise<any> {
    return new Promise((resolve, reject) => {
      let filter = {
        where: {
          name: value.name,
          status: true,
          companyId: this.currentUser.companyId
        }

      }
      this._userDetailsService.getUserDetailsDesignationWise(filter).subscribe((res) => {
        if (res.length > 0) {
          resolve(res)
        } else {
          reject
        }
      })

    })
  }

  exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Day Plan Attendance').subscribe(() => {
      // save started
    });
  }
}
