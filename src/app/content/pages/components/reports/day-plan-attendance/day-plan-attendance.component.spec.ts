import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayPlanAttendanceComponent } from './day-plan-attendance.component';

describe('DayPlanAttendanceComponent', () => {
  let component: DayPlanAttendanceComponent;
  let fixture: ComponentFixture<DayPlanAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DayPlanAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayPlanAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
