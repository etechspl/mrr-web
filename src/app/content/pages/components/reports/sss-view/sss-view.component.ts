import { EmployeeComponent } from './../../filters/employee/employee.component';
import { SelectionModel } from '@angular/cdk/collections';
import { MappedStockistComponent } from './../../filters/mapped-stockist/mapped-stockist.component';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { DistrictComponent } from './../../filters/district/district.component';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators, ControlContainer } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'm-sss-view',
  templateUrl: './sss-view.component.html',
  styleUrls: ['./sss-view.component.scss']
})
export class SssViewComponent implements OnInit {
  showState=false;
  showDistrict=false;
  showEmployee=false;
  isShowSSSForm = false;
  isShowPriColumn = false;
  isShowReturnColumn = false;
  isShowEditSSSForm = false;
  exampleDatabase: StockiestService | null;
  constructor(private fb: FormBuilder, private monthYearService: MonthYearService, private snackBar: MatSnackBar, private stockiestService: StockiestService) { }

  ngOnInit() {
    if (this.currentUser.userInfo[0].rL === 1) {
      this.getStockists(this.currentUser.id);
      this.getStateValue(this.currentUser.userInfo[0].stateId);
      this.getDistrictValue(this.currentUser.userInfo[0].districtId);
    }else if(this.currentUser.userInfo[0].rL === 0){
      this.showState=true;
      this.showDistrict=true;
      this.showEmployee=true;
    }

  }

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMappedStockistComponent") callMappedStockistComponent: MappedStockistComponent;
  check: boolean;
  selection = new SelectionModel(true, []);
  displayedColumns = [];
  displayedColumnsForEdit = [];
  index: number;
  id: number;
  dataSource;

  currentUser = JSON.parse(sessionStorage.currentUser);
  sssViewForm = new FormGroup({
    stateId: new FormControl(null, Validators.required),
    districtId: new FormControl(null, Validators.required),
    userId: new FormControl(null, Validators.required),
    stockistId: new FormControl(null, Validators.required),
    month: new FormControl(null, Validators.required),
    year: new FormControl(null, Validators.required),
    saleType: new FormControl(null, Validators.required),
  })
  //sssEditForm = this.formBuilder.group({
  //ProductInfo: this.formBuilder.array([])
  //});
  myform = new FormGroup({
    productName: new FormControl(Validators.required),
    netRate: new FormControl(null, Validators.required),
    splRate: new FormControl(),
    opening: new FormControl(null, Validators.required),
    primarySale: new FormControl(null, Validators.required),
    returnSale: new FormControl(null, Validators.required),
    resultList: new FormArray([])
  });


  productList = [];
  createForm(productArr) {
    this.productList = productArr;
    this.myform = this.fb.group({
      productName: [[Validators.required, Validators.minLength(4)]],
      netRate: [],
      splRate: [],
      opening: [],
      primarySale: [],
      returnSale: [],
      breakage: [],
      saleable: [],
      other: [],
      resultList: new FormArray([])
    });


    const productFGs = this.productList.map(product => {
      return this.fb.group({
        productName: [product.productName],
        netRate: [product.netRate],
        splRate: [product.splRate],
        opening: [product.opening],
        primarySale: [product.primarySale],
        returnSale: [product.returnSale],
        productId: [product.productId],
        invoiceNo: [product.invoiceNo],
        invoiceDate: [product.invoiceDate],
        breakage: [product.breakage],
        saleable: [product.saleable],
        other: [product.other]
      })
    });
    const productFormArray: FormArray = this.fb.array(productFGs);
    this.myform.setControl('resultList', productFormArray);
  }



  getStateValue(val) {
    this.isShowSSSForm = false;
    this.sssViewForm.patchValue({ stateId: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);

  }

  getDistrictValue(val) {
    let districtArr = [];
    this.isShowSSSForm = false;
    this.sssViewForm.patchValue({ districtId: val });
    districtArr.push(val);
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr);
  }

  getStockists(val) {
    let userArr = [];
    this.isShowSSSForm = false;
    this.sssViewForm.patchValue({ userId: val });
    userArr.push(val);
    this.callMappedStockistComponent.getMappedStockists(this.currentUser.companyId, userArr)
  }
  setStockistValue(val) {
    this.isShowSSSForm = false;
    this.sssViewForm.patchValue({ stockistId: val });
  }

  getMonth(val) {
    this.isShowSSSForm = false;
    this.sssViewForm.patchValue({ month: val })
  }

  getYear(val) {
    this.isShowSSSForm = false;
    this.sssViewForm.patchValue({ year: val })
  }


  saleType(val) {
    this.isShowPriColumn = false;
    this.isShowReturnColumn = false;
    this.isShowEditSSSForm = false;
    if (val === 'primarySale') {
      this.isShowPriColumn = true;
      this.displayedColumns = ['sn', 'productName', 'partyName', 'month', 'year', 'opening', 'totalPrimaryQty', 'totalPrimaryAmt', 'delActions'];
      this.displayedColumnsForEdit = ['select', 'sn', 'productName', 'netRate', 'splRate', 'opening', 'totalPrimaryQty', 'delActions'];
    } else if (val === 'returnSale') {
      this.isShowReturnColumn = true;
      this.displayedColumns = ['sn', 'productName', 'partyName', 'month', 'year', 'totalSaleReturnQty', 'totalSaleReturnAmt',/* 'breakage', 'saleable', 'other', */'delActions'];
      this.displayedColumnsForEdit = ['select', 'sn', 'productName', 'netRate', 'splRate', 'totalSaleReturnQty', 'breakage', 'saleable', 'other', 'delActions'];
    }
    this.isShowSSSForm = false;
    this.sssViewForm.patchValue({ saleType: val })
  }

  getSubmittedSSSDetail() {
    this.stockiestService.getSubmittedSSSDetail(this.currentUser.companyId, this.sssViewForm.value).subscribe(res => {
      if (res.length == 0 || res == undefined) {
        this.isShowSSSForm = false;
        alert("No Record Found");
      } else {
        this.isShowEditSSSForm = false;
        this.isShowSSSForm = true;
        this.dataSource = res;
      }
    }, err => {
      console.log(err);
      alert("No Record Found");
    })
  }
  editSSSDetail() {
    this.stockiestService.checkForClosingSubmitted(this.currentUser.companyId, this.sssViewForm.value).subscribe(res => {
      if (res === '' || res.length === 0 || res[0].closing === 0) {
        this.stockiestService.getSubmittedSSSDetailForEditModule(this.currentUser.companyId, this.sssViewForm.value).subscribe(res => {
          if (res.length == 0 || res == undefined) {
            alert("No Record Found");
          } else {
            this.dataSource = res;
            this.createForm(this.dataSource);
            this.isShowSSSForm = false;
            this.isShowEditSSSForm = true;
          }
        }, err => {
          console.log(err)
          alert("No Record Found");
        })
      } else {
        this.openSnackBar('You can not edit SSS details', 'Because Closing is Alreay submitted !!');
      }
    });

  }

  editSSS() {
    this.stockiestService.editSubmittedSSSDetails(this.currentUser.companyId, this.sssViewForm.value, this.myform.value.resultList).subscribe(res => {
      if (res.length == 0 || res == undefined) {
        alert("No Record Found");
      } else {
        if (res[0].status === 'Ok') {
          this.openSnackBar('SSS Edited Successfully.', '');
          this.getSubmittedSSSDetail();
        }
      }
    }, err => {
      console.log(err)
      alert("No Record Found");
    })
  }

  deleteProduct(productInfo) {
    this.stockiestService.deleteProducts(productInfo, this.sssViewForm.value)
      .subscribe(delProduct => {
        this.openSnackBar('Product Deleted Successfully.', '');
        this.getSubmittedSSSDetail();
      }, err => {
        this.openSnackBar('Product Deleted Successfully.', '');
        this.getSubmittedSSSDetail();
        console.log(err)
      })
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(element => {
        this.selection.select(element)
      });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}

