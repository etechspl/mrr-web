import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SssViewComponent } from './sss-view.component';

describe('SssViewComponent', () => {
  let component: SssViewComponent;
  let fixture: ComponentFixture<SssViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SssViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SssViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
