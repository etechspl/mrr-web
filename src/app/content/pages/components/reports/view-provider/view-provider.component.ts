import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import { SelectionModel } from '@angular/cdk/collections';
import { MatOption, MatPaginator, MatSelect, MatTableDataSource } from '@angular/material';
import { Console } from 'console';

@Component({
  selector: 'm-view-provider',
  templateUrl: './view-provider.component.html',
  styleUrls: ['./view-provider.component.scss']
})
export class ViewProviderComponent implements OnInit {

  allSelected=false;
  allSelectedHQ=false;

  constructor(
    public fb: FormBuilder,
    private _toastr: ToastrService,
    private userAreaMapping: UserAreaMappingService,
    private _providerService: ProviderService,
    private _productService: ProductService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { }

  currentUser = JSON.parse(sessionStorage.currentUser);
  areas = [];
  dataSource;
  dataSourceT : MatTableDataSource<any>;
  dataSource01 : MatTableDataSource<any>;
  dataSource02 : MatTableDataSource<any>;
  dataSource03 : MatTableDataSource<any>;
  dataSource1 :any = [];
  dataSource2 :any = [];
  dataSource3 :any = [];
  showDoctorForm = false;
  showVendorForm = false;
  isShowProvider = false;
  isShowDoctorForApprove = false;
  isShowDoctorForDelete = false;
  isShowVendorForApprove = true;
  isShowVendorForDelete = true;
  isShowPendingForApprovalAndDeletionForDoctor = false;
  isShowPendingForApprovalAndDeletionForVendor = false;
  showProcessing = false;
  showApproveButton = false;
  showDeletetButton = false;
  displayedColumns = [];
  displayedColumns1 = [];
  viewStatus = [{
    id: true,
    value: "Active"
  }, {
    id: 'Pending For Approval',
    value: "Pending For Approval"
  }, {
    id: 'Pending For Deletion',
    value: "Pending For Deletion"
  }, {
    id: false,
    value: "InActive"
  }]



  ngOnInit() {
    this.reportType('doctor');
    if (this.currentUser.userInfo[0].rL == 1) {
      this.getUserMappedAreas(this.currentUser.id);
    } else if (this.currentUser.userInfo[0].rL > 1 && this.currentUser.userInfo[0].rL < 6) {
      //this.reportType('doctor');
      this.getUserMappedAreas(this.currentUser.id);
    }
  }

  selection = new SelectionModel(true, []);
  selection1 = new SelectionModel(true, []);
  selection2 = new SelectionModel(true, []);
  selection3 = new SelectionModel(true, []);
  viewProvider = new FormGroup({
    reportTypeName: new FormControl(null, [Validators.required]),
    area: new FormControl(null, [Validators.required]),
    viewStatus: new FormControl(null, [Validators.required]),
    stateId: new FormControl(null),
    districtId: new FormControl(null),
    userId: new FormControl(null)
  });

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('selectArea') selectArea: MatSelect;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  getStateValue(val) {
    this.isShowPendingForApprovalAndDeletionForDoctor = false;
    this.isShowPendingForApprovalAndDeletionForVendor = false;
    this.viewProvider.patchValue({ stateId: val });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);

  }
 
  getDistrictValue(val) {
    this.isShowPendingForApprovalAndDeletionForDoctor = false;
    this.isShowPendingForApprovalAndDeletionForVendor = false;
    this.viewProvider.patchValue({ districtId: val });
    let districtArr = [];
    districtArr.push(val);
    this.callEmployeeComponent.getEmployeeListOnDistricts(this.currentUser.companyId, districtArr)
  }

  getEmployees(val) {
    this.isShowPendingForApprovalAndDeletionForDoctor = false;
    this.isShowPendingForApprovalAndDeletionForVendor = false;
    this.getUserMappedAreas(val)
  }
  

  setArea(val) {
    this.showProcessing = false;
    this.isShowProvider = false;
    this.isShowDoctorForApprove = false;
    this.isShowDoctorForDelete = false;
    this.isShowVendorForApprove = true;
    this.isShowVendorForDelete = true;
    this.isShowPendingForApprovalAndDeletionForDoctor = false;
    this.isShowPendingForApprovalAndDeletionForVendor = false;
  }

  reportType(val) {
    this.showProcessing = false;
    this.isShowProvider = false;
    this.isShowDoctorForApprove = false;
    this.isShowDoctorForDelete = false;
    this.isShowVendorForApprove = true;
    this.isShowVendorForDelete = true;
    this.isShowPendingForApprovalAndDeletionForDoctor = false;
    this.isShowPendingForApprovalAndDeletionForVendor = false;
    if (val == 'doctor') {
      this.showDoctorForm = true;
      this.showVendorForm = false;
    } else if (val == 'vendor') {
      this.showDoctorForm = false;
      this.showVendorForm = true;
    }
    this.viewProvider.patchValue({ reportTypeName: val });
  }

  getUserMappedAreas(userId) {
    this.viewProvider.patchValue({ userId: userId });

    console.log("userId",userId);
    
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, userId).subscribe(res => {
      this.areas = res;
    });
  }

  getProviderDetails(val) {
    this.isShowProvider = false;
    this.isShowDoctorForApprove = false;
    this.isShowDoctorForDelete = false;
    this.isShowVendorForApprove = true;
    this.isShowVendorForDelete = true;
    this.isShowPendingForApprovalAndDeletionForDoctor = false;
    this.isShowPendingForApprovalAndDeletionForVendor = false;
    this.showProcessing = true;
    if (this.viewProvider.value.area == null || this.viewProvider.value.area == undefined || this.viewProvider.value.area == "") {
      this._toastr.error("Selected information is incorret !", "Please select " + this.currentUser.company.lables.areaLabel + " Name first !!");
      this._changeDetectorRef.detectChanges();
    } else {
      let whereObj = {
        "companyId": this.currentUser.companyId,
        "blockId": this.viewProvider.value.area,
        //"userId":this.currentUser.userInfo[0].userId
      };
      let showStatus = '';
      if (this.viewProvider.value.reportTypeName == "doctor") {
        whereObj['providerType'] = ["RMP"];
        this.isShowDoctorForApprove = true;
        this.isShowDoctorForDelete = true;
        this.displayedColumns = ['select', 'sno', 'blockName','dateOfBirth',  'providerName', 'providerCode', 'category', 'degree', 'specialization'];
      } else if (this.viewProvider.value.reportTypeName == "vendor") {
        whereObj['providerType'] = ['Drug', 'Chemist', 'Stockist'];
        this.isShowVendorForApprove = true;
        this.isShowVendorForDelete = true;
        this.displayedColumns1 = ['select', 'sno', 'blockName','dateOfBirth',  'providerName', 'providerCode', 'category'];
      }

      if (this.viewProvider.value.viewStatus == "true") {
        whereObj['appStatus'] = "approved";
        whereObj['status'] = true;
        this.isShowPendingForApprovalAndDeletionForDoctor = false;
        this.isShowPendingForApprovalAndDeletionForVendor = false;
        if (this.viewProvider.value.reportTypeName == "doctor") {
          this.isShowDoctorForApprove = false;
          this.isShowDoctorForDelete = true;
          this.isShowVendorForApprove = false;
          this.isShowVendorForDelete = false;
        } else {
          this.isShowDoctorForApprove = false;
          this.isShowDoctorForDelete = false;
          this.isShowVendorForApprove = false;
          this.isShowVendorForDelete = true;
        }

      } else if (this.viewProvider.value.viewStatus == "Pending For Approval") {
        whereObj['appStatus'] = "pending";
        whereObj['status'] = false;
        if (this.viewProvider.value.reportTypeName == "doctor") {
          this.isShowPendingForApprovalAndDeletionForDoctor = true;
          this.isShowPendingForApprovalAndDeletionForVendor = false;
          this.displayedColumns = ['sno', 'blockName', 'providerName','dateOfBirth',  'providerCode', 'category', 'degree', 'specialization'];
        } else {
          this.isShowPendingForApprovalAndDeletionForVendor = true;
          this.isShowPendingForApprovalAndDeletionForDoctor = false;
          this.displayedColumns1 = ['sno', 'blockName','dateOfBirth', 'providerName', 'providerCode', 'category'];
        }

        this.isShowDoctorForApprove = false;
        this.isShowDoctorForDelete = false;
        this.isShowVendorForApprove = false;
        this.isShowVendorForDelete = false;
      } else if (this.viewProvider.value.viewStatus == "Pending For Deletion") {
        whereObj['delStatus'] = "pending";
        if (this.viewProvider.value.reportTypeName == "doctor") {
          this.isShowPendingForApprovalAndDeletionForDoctor = true;
          this.isShowPendingForApprovalAndDeletionForVendor = false;
          this.displayedColumns = ['sno', 'blockName', 'providerName', 'dateOfBirth', 'providerCode', 'category', 'degree', 'specialization'];
        } else {
          this.isShowPendingForApprovalAndDeletionForVendor = true;
          this.isShowPendingForApprovalAndDeletionForDoctor = false;
          this.displayedColumns1 = ['sno', 'blockName', 'providerName','dateOfBirth',  'providerCode', 'category'];
        }

        this.isShowDoctorForApprove = false;
        this.isShowDoctorForDelete = false;
        this.isShowVendorForApprove = false;
        this.isShowVendorForDelete = false;
        //whereObj['status']=true;
      } else if (this.viewProvider.value.viewStatus == "false") {
        whereObj['delStatus'] = "approved";
        whereObj['status'] = false;
        this.isShowPendingForApprovalAndDeletionForDoctor = false;
        this.isShowPendingForApprovalAndDeletionForVendor = false;
        if (this.viewProvider.value.reportTypeName == "doctor") {
          this.isShowDoctorForApprove = true;
          this.isShowDoctorForDelete = false;
          this.isShowVendorForApprove = false;
          this.isShowVendorForDelete = false;
        } else {
          this.isShowDoctorForApprove = false;
          this.isShowDoctorForDelete = false;
          this.isShowVendorForApprove = true;
          this.isShowVendorForDelete = false;
        }

      }

      this._providerService.getProviderDetails(whereObj).subscribe(getProvider => {
        console.log("get pro--",getProvider);
        
        if (getProvider.length > 0) {
          this.dataSourceT = new MatTableDataSource(getProvider);
          this.dataSource01 = new MatTableDataSource(getProvider);
          this.dataSource02 = new MatTableDataSource(getProvider);
          this.dataSource03 = new MatTableDataSource(getProvider);
          this.dataSource = getProvider;
          this.dataSource1 = getProvider;
          this.dataSource2 = getProvider;
          this.dataSource3 = getProvider;
          this.isShowProvider = true;
          this.showProcessing = false;
          this._changeDetectorRef.detectChanges();
        } else {
          this.showProcessing = false;
          this._toastr.error("Selected information is incorret !", "No Data Found !!");
          this._changeDetectorRef.detectChanges();
        }
      }, err => {
      })
    }
  }

  deleteProvider(formInfo) {
    let provideIds = [];
    if (formInfo == null || formInfo == undefined) {
      this._toastr.error("Please select atleast one provider !", "Provider not selected !!");
    } else {
      for (let i = 0; i < formInfo.length; i++) {
        provideIds.push({ "providerId": formInfo[i]._id });
      }
    }
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })

    swalWithBootstrapButtons.fire({
      title: 'Are you sure want to delete?',
      text: 'Enter Provider Deletion Reason',
      input: 'text',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true,
      allowOutsideClick: false
    }).then((result) => {
      console.log("result",result)
      if (result.value) {
        this._providerService.approveDeleteProvidersForMrMgr(provideIds, 'delete', this.currentUser.companyId, this.currentUser.userInfo[0].userId, this.currentUser.company.validation.approvalUpto, this.currentUser.userInfo[0].rL, result.value).subscribe(response => {
          swalWithBootstrapButtons.fire({
            title: 'Provider Deleted Successfully!!!',
            text: 'Your provider has been deleted successfully!',
            type: 'success',
            allowOutsideClick: false
          }).then((result2) => {
            this.selection.clear();
            this.selection1.clear();
            this.selection2.clear();
            this.selection3.clear();
            this.dataSource=[];
            this.dataSource1=[];
            this.dataSource3=[];
            this.dataSource3=[];
            this.getProviderDetails('abc');
            this._changeDetectorRef.detectChanges();
          });
        }, err => {
          Swal.fire('Oops...', err.error.error.message, 'error');
          console.log("Error while creating mapping");
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Provider was not deleted.',
          'error'
        )
      }
    });
  }

  approveProvider(formInfo) {
    let provideIds1 = [];
    if (formInfo == null || formInfo == undefined) {
      this._toastr.error("Please select atleast one provider !", "Provider not selected !!");
    } else {
      for (let i = 0; i < formInfo.length; i++) {
        provideIds1.push({ "providerId": formInfo[i]._id });
      }
    }
    const swalWithBootstrapButtons1 = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons1.fire({
      title: 'Are you sure want to active?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Active it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true,
      allowOutsideClick: false
    }).then((result) => {

      if (result.value) {
        this._providerService.approveDeleteProvidersForMrMgr(provideIds1, 'approve', this.currentUser.companyId, this.currentUser.userInfo[0].userId, this.currentUser.company.validation.approvalUpto, this.currentUser.userInfo[0].rL, result.value).subscribe(response1 => {
          swalWithBootstrapButtons1.fire({
            title: 'Provider request has been sent for active !!!',
            text: 'Your provider request has been sent successfully! Thank you',
            type: 'success',
            allowOutsideClick: false
          }).then((result2) => {
            this.selection.clear();
            this.selection1.clear();
            this.selection2.clear();
            this.selection3.clear();
            this.dataSource=[];
            this.dataSource1=[];
            this.dataSource3=[];
            this.dataSource3=[];
            this.getProviderDetails('abc');
            this._changeDetectorRef.detectChanges();
          });
        }, err => {
          Swal.fire('Oops...', err.error.error.message, 'error');
          console.log("Error while creating mapping");
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons1.fire(
          'Cancelled',
          'Provider was not active.',
          'error'
        )
      }
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceT.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceT.data.forEach(element => {
        this.selection.select(element)
      });
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected1() {
    const numSelected1 = this.selection1.selected.length;
    const numRows1 = this.dataSource01.data.length;
    return numSelected1 === numRows1;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle1() {
    this.isAllSelected1() ?
      this.selection1.clear() :
      this.dataSource01.data.forEach(element => {
        this.selection1.select(element)
      });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected2() {
    const numSelected2 = this.selection2.selected.length;
    const numRows2 = this.dataSource02.data.length;
    return numSelected2 === numRows2;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle2() {
    this.isAllSelected2() ?
      this.selection2.clear() :
      this.dataSource02.data.forEach(element => {
        this.selection2.select(element)
      });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected3() {
    const numSelected3 = this.selection3.selected.length;
    const numRows3 = this.dataSource03.data.length;
    return numSelected3 === numRows3;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle3() {
    this.isAllSelected3() ?
      this.selection3.clear() :
      this.dataSource03.data.forEach(element => {
        this.selection3.select(element)
      });
  }

  applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSourceT.filter = filterValue;
    this.dataSource01.filter = filterValue;
    this.dataSource02.filter = filterValue;
    this.dataSource03.filter = filterValue;
  }

  toggleAllSelectionForHQ() {
    this.allSelectedHQ = !this.allSelectedHQ;  // to control select-unselect
    
    if (this.allSelectedHQ) {
      this.selectArea.options.forEach( (item : MatOption) => item.select());
    } else {
      this.selectArea.options.forEach( (item : MatOption) => {item.deselect()});
    }

  }
}
