import { Component, OnInit, ViewChild } from '@angular/core';
import { TypeComponent } from './../../filters/type/type.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl } from '@angular/forms';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { ChangeDetectorRef, ElementRef } from '@angular/core';

declare var $;


@Component({
  selector: 'm-station-wise-working',
  templateUrl: './station-wise-working.component.html',
  styleUrls: ['./station-wise-working.component.scss']
})
export class StationWiseWorkingComponent implements OnInit {
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showAreaOnMR: boolean = false;
  showAdminAndMGRLevelFilter = false;
  dataSource;
  showProcessing: boolean = false;

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  dtTrigger: any;
  constructor(    private _toastr: ToastrService,   
     private monthYearService: MonthYearService,
     private _dcrReportsService: DcrReportsService,
     private changedetectorref: ChangeDetectorRef

    ) { }
  currentUser = JSON.parse(sessionStorage.currentUser);
  dcrForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null),
    stateInfo: new FormControl(null),
    //stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    month: new FormControl(null),
    year: new FormControl(null),
    designation: new FormControl(null),
    toDate: new FormControl(null),
    fromDate: new FormControl(null),

  })
  check: boolean;
  ngOnInit() {
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }
  getReportType(val) {
    this.showReport = false;
    if (val === "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      this.showState = true;
      this.showDistrict = false;
    } else if (val == "Headquarter") {
      this.showState = true;
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
    } else if (val == "Self") {
      this.showEmployee = false;
    }
    this.dcrForm.patchValue({ type: val });
  }
  getStateValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ stateInfo: val });

    if (this.dcrForm.value.type == "Headquarter") {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);
    }
  }
  getDistrictValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ employeeId: val });
  }
  getMonth(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ month: val });

  }
  getYear(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ year: val });
  }

  getDesignation(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ designation: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
  }
  viewRecords() {
    this.showProcessing = true;
    this.changedetectorref.detectChanges();
    let userIds;
    console.log("DCR Form Value-",this.dcrForm.value)
    if (this.dcrForm.value.type == "Self" || this.dcrForm.value.type == null) { //Null is for MR level
      userIds = [this.currentUser.id];
    } else {
      userIds = this.dcrForm.value.employeeId
    }
   this._dcrReportsService.stationWiseWorking(this.currentUser.companyId,this.dcrForm.value,userIds).subscribe(res =>
   {    
    this.changedetectorref.detectChanges();
    this.showProcessing = false;
     this.dataSource=res;
     this.showReport= true;
     this.dtOptions = {
      destroy: true,
      data: res,
      columns: [
        {
          title: 'State Name',
          data: 'stateName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },{
          title: 'District Name',
          data: 'districtName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },{
          title: 'Employee Name',
          data: 'userName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },{
          title: 'HQ Visit',
          data: 'hQCount',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },{
          title: 'EX Visit',
          data: 'EXCount',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },{
          title: 'OUT Visit',
          data: 'OUTCount',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        }
      ],
      dom: 'Bfrtip',
      buttons: [
        'copy', 'csv', 'excel', 'print'
      ]
    };
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
    this.changedetectorref.detectChanges();

   }, err => {
    console.log(err)
    this.showProcessing = false;

    this._toastr.info('For Selected Filter data not found', 'Data Not Found');

  })
  }
}
