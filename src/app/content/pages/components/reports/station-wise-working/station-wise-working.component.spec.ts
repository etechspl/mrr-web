import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationWiseWorkingComponent } from './station-wise-working.component';

describe('StationWiseWorkingComponent', () => {
  let component: StationWiseWorkingComponent;
  let fixture: ComponentFixture<StationWiseWorkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationWiseWorkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationWiseWorkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
