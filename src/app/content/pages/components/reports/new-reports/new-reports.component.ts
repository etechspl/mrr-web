import { Component, OnInit } from "@angular/core";
import { URLService } from "../../../../../core/services/URL/url.service";

@Component({
	selector: "m-new-reports",
	templateUrl: "./new-reports.component.html",
	styleUrls: ["./new-reports.component.scss"],
})
export class NewReportsComponent implements OnInit {
  baseUrl;
  constructor(
    private _URLService: URLService
  ) {}
	dataSource = [
		{
			title: "CRM Module",
			description: `Technology-supported customer management can bring your business to the next level. 
      Modern CRM module allows your company to build a strong and trusting relationship with your clients.`,
			createdAt: new Date("2020-07-20").toISOString(),
			routerLink: `/crm/linkProviders`,
		},
		// {
		// 	title: "Upload Data",
		// 	description: `Uploading data has now become so easy and interesting. It's a hassle free process where you can just select any option for which you want to upload the data.`,
		// 	createdAt: new Date("2020-07-22").toISOString(),
		// 	routerLink: `/master/uploadData`,
		// },
		{
			title: "Battery Percentage while Adding DCR",
			description: `This is an exclusive feature where you can see the battery percentage (%) of user's device at the time of DCR submission.`,
			createdAt: new Date("2020-07-22").toISOString(),
			routerLink: `/reports/dcrsummary`,
		},
	];

	ngOnInit() {
    this.baseUrl = this._URLService.API_ENDPOINT;
  }
}
