import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SponsorshipViewComponent } from './sponsorship-view.component';

describe('SponsorshipViewComponent', () => {
  let component: SponsorshipViewComponent;
  let fixture: ComponentFixture<SponsorshipViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SponsorshipViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SponsorshipViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
