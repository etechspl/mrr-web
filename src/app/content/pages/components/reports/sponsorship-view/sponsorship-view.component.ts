import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material';
import Swal from "sweetalert2";

import { CrmService } from '../../../../../core/services/CRM/crm.service';
import { FileHandlingService } from '../../../../../core/services/FileHandling/file-handling.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { StateComponent } from '../../filters/state/state.component';
import { StatusComponent } from '../../filters/status/status.component';
import { TypeComponent } from '../../filters/type/type.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
declare var $;

@Component({
  selector: 'm-sponsorship-view',
  templateUrl: './sponsorship-view.component.html',
  styleUrls: ['./sponsorship-view.component.scss']
})
export class SponsorshipViewComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  sponsorshipForm: FormGroup;
  logoURL: any;
  showState: boolean = true;
  showDistrict: boolean = false;
  showReport: boolean = false;
  showEmployee: boolean = false;
  @ViewChild("callDistrictComponent")
  callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent")
  callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("setStatus") setStatus: StatusComponent;
  @ViewChild("callDivisionComponent")
  callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR")
  callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  isShowDivision: boolean = false;
  showProcessing: boolean = false;
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;

  constructor(
    private _cdr: ChangeDetectorRef,
    private _urlService: URLService,
    private _crmService: CrmService,
    private _fileHandlingService: FileHandlingService,


  ) {
    this.createForm();
  }
  createForm() {
    this.sponsorshipForm = new FormGroup({
      reportType: new FormControl("Geographical"),
      type: new FormControl(null, Validators.required),
      status: new FormControl([true]),
      stateInfo: new FormControl(null),
      districtInfo: new FormControl(null),
      employeeId: new FormControl(null),
      month: new FormControl(null, Validators.required),
      year: new FormControl(null, Validators.required),
      designation: new FormControl(null)
    });
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {

    this.logoURL =
      this._urlService.API_ENDPOINT_CONTAINER +
      "/" +
      this.currentUser.company.logo.container +
      "/download/" +
      this.currentUser.company.logo.modifiedFileName;

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.sponsorshipForm.setControl("divisionId", new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel,
      };
      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);
    } else {
      this.isShowDivision = false;
    }

    if (
      this.currentUser.userInfo[0].designationLevel == 0 ||
      this.currentUser.userInfo[0].designationLevel > 1
    ) {
      //For Admin And MGR
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Geographical",
        "WithHeadquarter"
      );
    }
  }



  getReportType(val) {
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Geographical",
        "WithHeadquarter"
      );
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Hierarachy",
        "empWise"
      );
    }
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      this.showDistrict = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.sponsorshipForm.setControl(
        "stateInfo",
        new FormControl(null, Validators.required)
      );
      this.sponsorshipForm.removeControl("employeeId");
      this.sponsorshipForm.removeControl("designation");
      this.sponsorshipForm.removeControl("districtInfo");
    } else if (val == "Headquarter") {
      this.callStateComponent.setBlank();
      this.sponsorshipForm.removeControl("employeeId");
      this.sponsorshipForm.setControl(
        "stateInfo",
        new FormControl(null, Validators.required)
      );
      this.sponsorshipForm.setControl(
        "districtInfo",
        new FormControl(null, Validators.required)
      );
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.sponsorshipForm.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.sponsorshipForm.removeControl("stateInfo");
      this.sponsorshipForm.removeControl("districtInfo");
      this.sponsorshipForm.setControl(
        "designation",
        new FormControl(null, Validators.required)
      );
      this.sponsorshipForm.setControl(
        "employeeId",
        new FormControl(null, Validators.required)
      );
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }
    this.sponsorshipForm.patchValue({ type: val });
    this._cdr.detectChanges();
  }
  getStateValue(val) {
    this.sponsorshipForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.sponsorshipForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          stateId: this.sponsorshipForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.sponsorshipForm.value.divisionId,
        });

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.sponsorshipForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(
          this.currentUser.companyId,
          val,
          true
        );
      }
    }
  }
  getDistrictValue(val) {
    this.sponsorshipForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.sponsorshipForm.patchValue({ employeeId: val });
  }
  getDesignation(val) {
    this.sponsorshipForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.sponsorshipForm.value.divisionId,
        status: [true],
        designationObject: this.sponsorshipForm.value.designation,
      });
    } else {
      this.callEmployeeComponent.getEmployeeList(
        this.currentUser.companyId,
        val,
        [true]
      );
    }
  }
  //setting division into sponsorshipForm
  getDivisionValue($event) {
    this.sponsorshipForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (
        this.sponsorshipForm.value.type === "State" ||
        this.sponsorshipForm.value.type === "Headquarter"
      ) {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.sponsorshipForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0]
            .designationLevel,
        });
      }

      if (this.sponsorshipForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (
          this.sponsorshipForm.value.divisionId != null &&
          this.sponsorshipForm.value.designation != null
        ) {
          if (this.sponsorshipForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision(
              {
                companyId: this.companyId,
                division: this.sponsorshipForm.value.divisionId,
                status: [true],
                designationObject: this.sponsorshipForm.value
                  .designation,
              }
            );
          }
        }
      }
    }
  }
  getMonth(val) {
    this.sponsorshipForm.patchValue({ month: val });
  }
  getYear(val) {
    this.sponsorshipForm.patchValue({ year: val });
  }

  showImage(fileDetail) {
    console.log(fileDetail);
    this._fileHandlingService.getImageDetailBasedOnId(fileDetail.fileId).subscribe(fileResult => {
      if (fileResult.length > 0) {
        let fileName = "";
        fileName = JSON.parse(JSON.stringify(fileResult[0])).modifiedFileName;
        let url = 'http://103.11.84.144:3015/api/containers/CRMImage/download/' + fileName;
        window.open(url, "Title but its not working", 'toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500');
      } else {
        (Swal as any).fire({
          title: `Bill is not updated yet!!`,
          type: "error",
        });
      }
    }, err => {
      console.log(err);
    });

  }

  viewRecords() {
    this.showProcessing = true;
    const formData = this.sponsorshipForm.value;

    const query = {
      where: {
        month: formData.month,
        year: formData.year
      },
      include: [
        {
          relation: "user"
        },
        {
          relation: "managerInfo"
        },
        {
          relation: "providerInfo"
        },
        {
          relation: "block"
        },
      ]
    }
    if (formData.type === "State") {
      query.where["stateId"] = { inq: formData.stateInfo }
    }
    else if (formData.type === "Employee Wise") {
      query.where["userId"] = { inq: formData.employeeId };
    }
    else if (formData.type === "Headquarter") {
      query.where["districtId"] = { inq: formData.districtInfo }
    }


    this._crmService.getCRMSponsorshipData(query).subscribe((crmRes: any) => {
      if (crmRes.length > 0) {
        this.showProcessing = false;
        this.isToggled = false;
        const finalData = crmRes.map(item => {
          const obj = {
            id: item.id,
            userId: item.userId,
            userName: item.user.name,
            designation: item.user.designation,
            block: item.block.areaName,
            provider: item.providerInfo.providerName,
            providerCategory: item.providerInfo.category,
            uniqueCode: item.uniqueCode,
            activity: item.activity,
            enterActivity: item.enterActivity,
            dateOfProposal: item.dateOfProposal,
            submissionDate: item.submissionDate,
            dateOfActivity: item.dateOfActivity,
            expense: item.expense,
            noOfDoctors: item.noOfDoctors,
            approvalStatusByMgr: item.approvalStatusByMgr,
            appAndDisAppMgrDate: item.appAndDisAppMgrDate,
            approvalStatusByAdmin: item.approvalStatusByAdmin,
            fileId: item.fileId,
            appAdmDate: item.appAdmDate,
            champainStatus: item.fileId ?"Close":"Open",
            remarksAdmin: item.remarksAdmin || ""
          }
          return obj;
        });


        if (finalData.length > 0) {
          this.showProcessing = false;
          this.showReport = true;
          this._cdr.detectChanges();

          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            destroy: true,
            data: finalData,
            responsive: true,
            columns: [
              {
                title: 'User Name',
                data: 'userName',
                render: function (data) {
                  if (!Boolean(data)) {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Designation',
                data: 'designation',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Block',
                data: 'block',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Provider',
                data: 'provider',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Provider Category',
                data: 'providerCategory',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Unique Code',
                data: 'uniqueCode',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Activity',
                data: 'activity',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Entered Activity',
                data: 'enterActivity',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Date Of Proposal',
                data: 'dateOfProposal',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return new DatePipe('en-US').transform(new Date(data), 'dd-MM-yyyy');
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Date Of Activity',
                data: 'dateOfActivity',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return new DatePipe('en-US').transform(new Date(data), 'dd-MM-yyyy');
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Selected Submission Date',
                data: 'submissionDate',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return new DatePipe('en-US').transform(new Date(data), 'dd-MM-yyyy');
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Expense',
                data: 'expense',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'No. of Doctors',
                data: 'noOfDoctors',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Approval Status By Mgr',
                data: 'approvalStatusByMgr',
                render: function (data) {
                  if (!Boolean(data)) {
                    return ""
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Approval Date of Manager',
                data: 'appAndDisAppMgrDate',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return new DatePipe('en-US').transform(new Date(data), 'dd-MM-yyyy');
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Approval Status By Admin',
                data: 'approvalStatusByAdmin',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Approval Date of Admin',
                data: 'appAdmDate',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return new DatePipe('en-US').transform(new Date(data), 'dd-MM-yyyy');
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Remarks Admin',
                data: 'remarksAdmin',
                render: function (data) {
                  if (!Boolean(data)) {
                    return "---"
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Champain Status',
                data: 'champainStatus',
                render: function (data) {
                  if (data) {
                    return data
                  }
                },
                defaultContent: '---'
              }, 
              {
                title: 'Bill Copy',
                defaultContent: "<button id='billId' class='btn btn-warning btn-sm'><i class='fa fa-folder'></i>&nbsp;&nbsp;&nbsp;&nbsp View Bill</button>"
              }
            ],
            rowCallback: (row: Node, data: any[] | Object, index: number) => {
              const self = this;
              $('td', row).unbind('click');
              $('td button', row).bind('click', (event) => {
                let rowObject = JSON.parse(JSON.stringify(data));
                if (event.target.id == "billId") {
                  console.log(data);
                  self.showImage(rowObject);
                }
              });

              return row;
            },

            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search records",
            },
            dom: 'Bfrtip',
            // buttons: [
            //   'copy', 'csv', 'excel', 'print'
            // ],
            buttons: [
              { extend: 'copy', footer: true },
              { extend: 'csv', footer: true },
              { extend: 'excel', footer: true },
              { extend: 'print', footer: true }
            ],


          }

          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          this.showReport = true;
          this._cdr.detectChanges();


        }



      }
    })

  }
}
