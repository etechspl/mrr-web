import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockistWiseSaleReportComponent } from './stockist-wise-sale-report.component';

describe('StockistWiseSaleReportComponent', () => {
  let component: StockistWiseSaleReportComponent;
  let fixture: ComponentFixture<StockistWiseSaleReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockistWiseSaleReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockistWiseSaleReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
