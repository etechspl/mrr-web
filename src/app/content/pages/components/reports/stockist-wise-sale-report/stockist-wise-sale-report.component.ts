import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTable, MatSlideToggleChange, MatSelect, MatOption } from '@angular/material';
import { Designation } from '../../../_core/models/designation.model';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { UtilsService } from '../../../../../core/services/utils.service';
declare var $;
@Component({
  selector: 'm-stockist-wise-sale-report',
  templateUrl: './stockist-wise-sale-report.component.html',
  styleUrls: ['./stockist-wise-sale-report.component.scss']
})

export class StockistWiseSaleReportComponent implements OnInit {
  public showReport: boolean = false;
  public showProcessing: boolean = false;
  public showProductFilter: boolean = false;
  public showReportProductWise: boolean = false;
  isStatus: boolean = false;

  count = 0;
  data;
  dataProductWise;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  stockistWiseSaleForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  allSelected: boolean = false;

  constructor(private _toastrService: ToastrService,
    private _fb: FormBuilder,
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef, private utilService: UtilsService,
    private toastr: ToastrService, private exportAsService: ExportAsService) {
    // this.createForm(this._fb);
    this.stockistWiseSaleForm = new FormGroup({
      designation: new FormControl(null, Validators.required),
      status: new FormControl(true),
      userId: new FormControl(null, Validators.required),
      toDate: new FormControl(null, Validators.required),
      fromDate: new FormControl(null, Validators.required),
      type: new FormControl('StockistWise'),
      product : new FormControl(null)
    })
  }
  public dynamicTableHeader: any = [];
  public dynamicTableProduct: any = [];

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('select') select: MatSelect;

  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("employeeComponent") employeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild('dataTable') table1;
  dataTable: any;
  dtOptions: any;
  dtTrigger: any;
  showDivisionFilter: boolean = false;
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      this.stockistWiseSaleForm.setControl('division', new FormControl('', Validators.required))
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  getDivisionValue(val) {
    // this.showProcessing = false;
    this.stockistWiseSaleForm.patchValue({ division: val });
    //=======================Clearing Filter===================
    this.designationComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: [val]
    }

    this.designationComponent.getDesignationBasedOnDivision(passingObj);
  }
  getDesignationList() {
    //this.designationComponent.getManagersDesignation();
  }

//   getDesignationLevel(designation: Designation) {
//     this.callStatusComponent.setBlank();
//     this.employeeComponent.setBlank();
//     this.stockistWiseSaleForm.patchValue({ designation: designation.designationLevel })
//   }

//****************************Umesh Kumar 15-05-2020 ******************************************** */

	getDesignationLevel(designation: Designation) {
		    this.callStatusComponent.setBlank();
		    this.employeeComponent.setBlank();
		    this.stockistWiseSaleForm.patchValue({ designation: designation.designationLevel })

		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					companyId: this.currentUser.companyId,
					designationObject: [designation],
					status: [true],
					division: this.stockistWiseSaleForm.value.division,
				};

				this.employeeComponent.getEmployeeListBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.employeeComponent.getEmployeeList(
					this.currentUser.companyId,
					[designation],
					[true]
				);
			}
		} else {
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [designation.designationLevel], //desig
				};
				this.employeeComponent.getManagerHierarchy(dynamicObj);
			} else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [designation.designationLevel], //desig,
					isDivisionExist: true,
					division: this.stockistWiseSaleForm.value.division,
				};
				this.employeeComponent.getManagerHierarchy(dynamicObj);
			}
		}
	}


  getStatus(status: boolean) {
    this.showReport = false;
    this.showReportProductWise = false;
    this.stockistWiseSaleForm.patchValue({ status: status });
    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        designationObject: this.stockistWiseSaleForm.value.designation,
        status: status,
        division: [this.stockistWiseSaleForm.value.division]
      }
      this.employeeComponent.getEmployeeListBasedOnDivision(passingObj);
    } else {
      this.employeeComponent.getEmployeeList(this.currentUser.companyId, this.stockistWiseSaleForm.value.designation, status);
    }
  }

//   getStatus(status: boolean) {

//     this.showReport = false;
//     this.showReportProductWise = false;
// 	this.stockistWiseSaleForm.patchValue({ status: status });
//   }


  getEmployee(emp: string) {
	this.stockistWiseSaleForm.patchValue({ userId: emp })
    this.showReport = false;
    this.showReportProductWise = false;
    if (this.stockistWiseSaleForm.value.type == "ProductWise") {
    this.stockistWiseSaleForm.setControl('product', new FormControl('', Validators.required))
      this.getProductOnSelectedUserStateWise(emp)
    }
  }

  getFromDate(fromDate: object) {
    this.stockistWiseSaleForm.patchValue({ fromDate: fromDate })
  }

  getToDate(toDate: number) {
    this.stockistWiseSaleForm.patchValue({ toDate: toDate })
  }

  getProduct(product) {
	this.stockistWiseSaleForm.patchValue({ product: product })
  }

  getProductOnSelectedUserStateWise(userId) {
    this._dcrReportsService.getUserInfo(userId).subscribe(res => {
      this._changeDetectorRef.detectChanges();
      this.showProcessing = false;
      if (res.length > 0) {
        let obj = {
          userId: res[0].userId,
          companyId: res[0].companyId,
          assignedStates: res[0].stateId,
          divisionId: "",
          status: true,
          sampleIssue: false,
          isDivisionExist: this.currentUser.company.isDivisionExist
        }
        if (this.currentUser.company.isDivisionExist == true) {
          obj.divisionId = this.stockistWiseSaleForm.value.division;
        }
        this._dcrReportsService.getProductsWithSampleQty(obj).subscribe(res => {
          this._changeDetectorRef.detectChanges();
          this.showProcessing = false;
          if (res.length > 0) {
            this.data = res;
          }
        }, err => {
        })

      }

    }, err => {
      this.showReport = false;
      this.showProcessing = false;
      this.toastr.info("No Record Found.")
      this._changeDetectorRef.detectChanges();
    })

  }

  // intializeForm() {
  //   this.stockistWiseSaleForm.setValue({
  //     type: '',
  //     designation: '',
  //     status: '',
  //     userId: '',
  //     fromDate: '',
  //     toDate: '',
  //   })
  // }
  getType(val) {
    this.stockistWiseSaleForm.patchValue({ type: val })
    this.callDivisionComponent.setBlank();
    this.stockistWiseSaleForm.patchValue({ 'division': '' });
    this.designationComponent.setBlank();
    this.stockistWiseSaleForm.patchValue({ 'designation': '' });
    this.stockistWiseSaleForm.patchValue({ 'userId': '' });
    this.callStatusComponent.setBlank();
    this.stockistWiseSaleForm.patchValue({ 'status': '' });
    if (val == "ProductWise") {
      this.showProductFilter = true;
    }
    else if (val == "StockistWise") {
      this.stockistWiseSaleForm.patchValue({ 'product': '' });
      this.stockistWiseSaleForm.removeControl('product');
      this.showProductFilter = false;
    }
  }

  exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'POB Details').subscribe(() => {
      // save started
    });
  }
  monthsName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  getCelData(date, product, data,i) {
    
    
    const _date = date.split(' ')
    const month = this.monthsName.findIndex(i => i.toLowerCase() === _date[0].toLowerCase()) + 1
    const year = _date[1]
    

    console.log("datat---", data);

    const _cellData = data.find(i => i.month == month && i.year == year && i.productName == product )
   
    
    return _cellData ? _cellData.total:(data[0].total[i]).toFixed(2);

  }

  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  generateReport() {
     this.isToggled = false;
    this.showProcessing = true;
    this._changeDetectorRef.detectChanges();
    if (!this.stockistWiseSaleForm.valid) {
      this._toastrService.error("Select the required field to generate the report", "All Field required");
      return;
    }
    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds.push(this.stockistWiseSaleForm.value.division)
    }
    if (this.stockistWiseSaleForm.value.type == "ProductWise") {
      let productId = [];
      let productName = [];


      for (var i = 0; i < this.stockistWiseSaleForm.value.product.length; i++) {
        productId.push(this.stockistWiseSaleForm.value.product[i].id)
        productName.push(this.stockistWiseSaleForm.value.product[i].productName)

      }
      let params = {
        companyId: this.currentUser.companyId,
        status: this.stockistWiseSaleForm.value.status,
        userId: this.stockistWiseSaleForm.value.userId,
        fromDate: this.stockistWiseSaleForm.value.fromDate,
        toDate: this.stockistWiseSaleForm.value.toDate,
        isDivisionExist: this.currentUser.company.isDivisionExist,
        division: divisionIds,
        designation: this.stockistWiseSaleForm.value.designation,
        productId: productId,
        type: "lowerWithMgr"
      }
      this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.stockistWiseSaleForm.value.fromDate, this.stockistWiseSaleForm.value.toDate);
      this.dynamicTableProduct = productName;
     
      
      this._dcrReportsService.productWisesale(params).subscribe(res => {
        this.showProcessing = false;
        if (res.length > 0) {
          this.showReportProductWise = true;
          this.showReport = false;
          this.dataProductWise = res;
          this._changeDetectorRef.detectChanges();          
        } else {
          this.showReportProductWise = false;
          this.showReport = false;
          this.toastr.info("No Record Found.")
          this._changeDetectorRef.detectChanges();
        }

      }, err => {
        this.showReport = false;
        this.showProcessing = false;
        this.showReport = false;
        this.toastr.info("No Record Found.")
        this._changeDetectorRef.detectChanges();
      })

    } else {
      let params = {
        companyId: this.currentUser.companyId,
        status: this.stockistWiseSaleForm.value.status,
        supervisorId: this.stockistWiseSaleForm.value.userId,
        designation: this.stockistWiseSaleForm.value.designation,
        fromDate: this.stockistWiseSaleForm.value.fromDate,
        toDate: this.stockistWiseSaleForm.value.toDate,
        type: "lowerWithMgr",
        isDivisionExist: this.currentUser.company.isDivisionExist,
        division: [divisionIds]
      }
      this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.stockistWiseSaleForm.value.fromDate, this.stockistWiseSaleForm.value.toDate);
      this._dcrReportsService.stkWiseMgrReport(params).subscribe(res => {
        this._changeDetectorRef.detectChanges();
        this.showProcessing = false;

        if (res.length > 0) {
          this._changeDetectorRef.detectChanges();
          this.showReport = true;
          this.showReportProductWise = false;
          this.data = res;
        } else {
          this.showReport = false;
          this.showProcessing = false;
          this.showReportProductWise = false;

          this.toastr.info("No Record Found.")

          this._changeDetectorRef.detectChanges();
		}
		this._changeDetectorRef.detectChanges();
      }, err => {
        this.showReport = false;
        this.showProcessing = false;
        this.showReportProductWise = false;

        this.toastr.info("No Record Found.")

        this._changeDetectorRef.detectChanges();
      })
      
    }
  }

}
