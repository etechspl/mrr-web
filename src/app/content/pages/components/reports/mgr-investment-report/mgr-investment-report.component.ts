import { MGRInvestmentService } from './../../../../../core/services/MGRInvestment/mgrinvestment.service';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { ToastrService } from 'ngx-toastr';
import { MatSlideToggleChange } from '@angular/material';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { URLService } from './../../../../../core/services/URL/url.service';
import { HierarchyService } from './../../../../../core/services/hierarchy-service/hierarchy.service';
import { StateComponent } from './../../filters/state/state.component';
import { DivisionComponent } from './../../filters/division/division.component';
import { StatusComponent } from './../../filters/status/status.component';
import { TypeComponent } from './../../filters/type/type.component';
import { MonthComponent } from './../../filters/month/month.component';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import * as _moment from 'moment';
declare var $;

@Component({
  selector: 'm-mgr-investment-report',
  templateUrl: './mgr-investment-report.component.html',
  styleUrls: ['./mgr-investment-report.component.scss']
})
export class MgrInvestmentReportComponent implements OnInit {
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;

  showAdminAndMGRLevelFilter = false;
  dataSource;
  dataSources;
  dataSources1;
  isDataSources1Empty = false;
  statuscheck = 1;
  managerTeam;
  onlyManager
  logoURL: any;
  count = 0;
  FinalView;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("setStatus") setStatus: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
  dtOptionsForDCRDateWise: any;
  
  constructor(
    private _mgrInvestmentservice: MGRInvestmentService, 
    private _changeDetectorRef: ChangeDetectorRef,
    private _hierarchyService: HierarchyService,
    private _urlService: URLService,
    private toastr: ToastrService,
    private exportAsService: ExportAsService,

  ) { }

  
  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  otherTypeLablesExist = (this.currentUser.company.lables.otherType) ? true : false;
  isPOBExist = this.currentUser.company.validation.pob;
  colspanValue;
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  investmentForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    status: new FormControl([true]),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    month: new FormControl(null),
    year: new FormControl(null),
    designation: new FormControl(null),
    toDate: new FormControl(null, Validators.required),
    fromDate: new FormControl(null, Validators.required)
  })

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  getReportType(val) {
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {

      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }

  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      this.showDistrict = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.investmentForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.investmentForm.removeControl("employeeId");
      this.investmentForm.removeControl("designation");
      this.investmentForm.removeControl("districtInfo");

    } else if (val == "Headquarter") {
      this.callStateComponent.setBlank();
      this.investmentForm.removeControl("employeeId");
      this.investmentForm.setControl("stateInfo", new FormControl(null,Validators.required))
      this.investmentForm.setControl("districtInfo", new FormControl(null,Validators.required))
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.investmentForm.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.investmentForm.removeControl("stateInfo");
      this.investmentForm.removeControl("districtInfo");
      this.investmentForm.setControl("designation", new FormControl(null,Validators.required))
      this.investmentForm.setControl("employeeId", new FormControl(null,Validators.required))
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.investmentForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();
  }

  getStateValue(val) {
    this.investmentForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.investmentForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.investmentForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.investmentForm.value.divisionId
        })

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.investmentForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.investmentForm.patchValue({ districtInfo: val });
  }
  getStatusValue(event){
  }
  getEmployeeValue(val) {
    this.investmentForm.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.investmentForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.investmentForm.patchValue({ toDate: val });
  }

  getDesignation(val) {
    this.investmentForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.investmentForm.value.divisionId,
        status: [true],
        designationObject: this.investmentForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  //setting division into dcrform
  getDivisionValue($event) {
    this.investmentForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.investmentForm.value.type === "State" || this.investmentForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.investmentForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.investmentForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.investmentForm.value.divisionId != null && this.investmentForm.value.designation != null) {
          if (this.investmentForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.investmentForm.value.divisionId,
              status: [true],
              designationObject: this.investmentForm.value.designation
            })
          }
        }
      }
    }
  }

  ngOnInit() {

    this.logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    this.colspanValue = 9;
    // this.colspanValue = (this.isPOBExist === false )? 8 : 7;
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.investmentForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };
      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }

    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }

  showDCRSummary = true;

  viewRecords() {
    this.isToggled = false;
    this.FinalView=false;
    this.showReport = false;
    this.showDateWiseDCRReport = false;
    this.showDCRSummary = true;
    this.showProcessing = true;
    if (this.investmentForm.value.employeeId == null) {
      this.investmentForm.value.employeeId = [this.currentUser.id];
    }
    
    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.investmentForm.value.divisionId;
    } else {
      params["division"] = [];
    }

    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.investmentForm.value.stateInfo;
    params["districtIds"] = this.investmentForm.value.districtInfo;
    params["type"] = this.investmentForm.value.type;
    params["employeeId"] = this.investmentForm.value.employeeId;
    params["fromDate"] = this.investmentForm.value.fromDate;
    params["toDate"] = this.investmentForm.value.toDate;
    params["status"] = this.investmentForm.value.status;



    this._mgrInvestmentservice.getInvestmentDetails(params).subscribe(res => {

      if (res.length == 0 || res == undefined) {
        this.showReport = false;
        this.showDateWiseDCRReport = false;

        this.showProcessing = false;
        this.toastr.info("No Record Found.")

      } else {
        this.dataSource = res;
        this.showProcessing = false;
        this.showReport = true;
        let row=0;
        res.map((item, index) => {
          item['index'] = index;
      });

      let otherArray=[{
        title:  'Default Col',
        data: "key",
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      }]

      if(this.otherTypeLablesExist){
        // otherArray.pop();
          this.currentUser.company.lables.otherType.forEach(element => {
            otherArray.push({
             title: element.label + ' Met Count',
             data: element.key,
             render: function (data) {
               if (data === '') {
                 return '---'
               } else {
                 return data
               }
             },
             defaultContent: '---'
           })
          });
       }  
       this.dtOptions = {
        "pagingType": 'full_numbers',
        "ordering": true,
        "info": true,
        "scrollY": 300,
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false,
        fixedColumns: {
          leftColumns: 0,
          rightColumns: 1
        },
        destroy: true,
        data: res,
        responsive: true,

        columns: [
          {
            title: 'S. No.',
            data: 'index',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data + 1;
              }
            },
            defaultContent: '---'
          },
          {
          title: 'Division',
          data: 'divisionName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        }, {
          title: 'Headquarter',
          data: 'headquarter',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        }
        , {
          title: 'Employee Code',
          data: 'employeeCode',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        }, {
          title: 'Employee Name ',
          data: 'UserName',

          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Designation',
          data: 'UserDesignation',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Date Of Joining',
          data: 'dateOfJoining',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return _moment(data).format("MMM DD, YYYY")
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Mobile',
          data: 'mobile',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        }, {
          title: 'Last Submission',
          data: 'lastsubmissionDate',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return _moment(data).format("MMM DD, YYYY")
            }
          },
          defaultContent: '---'
        },
        {
          title: "Total Reports",
          data: 'totalInvestmentdate',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: this.currentUser.company.lables.doctorLabel + ' Met Count',
          data: 'RMPCount',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: this.currentUser.company.lables.doctorLabel + ' POB',
          data: 'doctorPOB',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return Math.round(data)
            }
          },
          defaultContent: '---'
        }, {
          title: this.currentUser.company.lables.vendorLabel+ ' Met Count',
          data: 'DrugStoreCount',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: this.currentUser.company.lables.stockistLabel + ' Met Count',
          data: 'stockistCount',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
       
        {
          title: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel + ' POB',
          data: 'vendorPOB',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return Math.round(data)
            }
          },
          defaultContent: '---'
        },
        {
          title: 'View Investment Detail',
          defaultContent: "<button class='btn' style='padding: 1px 2px; background: transparent;color: #f44336;font-weight: 600;'><i class='la la-eye'></i>&nbsp;&nbsp;View Investment Detail</button>"
        } ,


        ],
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td', row).unbind('click');
          $('td .getDateWiseReport', row).bind('click', () => {
            //console.log("row : ", data);
            let rowObject = JSON.parse(JSON.stringify(data));
            self.getDateWise(rowObject.userId);
            this._changeDetectorRef.detectChanges();
          });
          $('td button', row).bind('click', () => {
            //console.log("row : ", data);
            let rowObject = JSON.parse(JSON.stringify(data));
            self.getDateWise(rowObject.userId);
          });
          return row;
        },
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search Records",
        },
        dom: 'Bfrtip',
        buttons: [

          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
        ]
      };

      if (this.isDivisionExist === false) {
        this.dtOptions.columns[1].visible = false;
      }

      if (this.otherTypeLablesExist) {
        
        this.dtOptions.columns[14].visible = false;
      }
      

   // ========= Rahul Saini 17-05-2020 Prevent buttons to print in excel ==========
   if (this.isDivisionExist === true) {

    if(!this.otherTypeLablesExist){

      if (this.isPOBExist == false) {
      this.dtOptions.buttons.push({
        extend: 'excel',
        exportOptions: {
          columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
        }
      })
    }
    else{

 this.dtOptions.buttons.push({
        extend: 'excel',
        exportOptions: {
          columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,15]
        }
      })
    }
    }
    else{
      if (this.isPOBExist == false) {
      this.dtOptions.buttons.push({
        extend: 'excel',
        exportOptions: {
          columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        }
      })
    }
    else{
      this.dtOptions.buttons.push({
        extend: 'excel',
        exportOptions: {
          columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        }
      })

    }
    }

} else {

  if(!this.otherTypeLablesExist){
    if (this.isPOBExist == false) {
    this.dtOptions.buttons.push({
      extend: 'excel',
      exportOptions: {
        columns: [0,2,3,4,5,6,7,8,9,10,11,12,13]
      }
    })
  }
  else{
    this.dtOptions.buttons.push({
      extend: 'excel',
      exportOptions: {
        columns: [0,2,3,4,5,6,7,8,9,10,11,12,13,15]
      }
    })

  }
  }
  else{
    if (this.isPOBExist == false) {
    this.dtOptions.buttons.push({
      extend: 'excel',
      exportOptions: {
        columns: [0,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
      }
    })
  }
  else{
    this.dtOptions.buttons.push({
      extend: 'excel',
      exportOptions: {
        columns: [0,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
      }
    })

  }
  }
}
// ========= End ============================================================


        this._changeDetectorRef.detectChanges();


        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
        this._changeDetectorRef.detectChanges();
}
        this._changeDetectorRef.detectChanges();
    }, err => {
      this.showReport = false;
      this.showDateWiseDCRReport = false;
      this.showProcessing = false;
      this.FinalView=false;
      this.toastr.info("No Record Found.")
      this._changeDetectorRef.detectChanges();
    });
  }

  getDateWise(val) {

    this.getEmployeeDCRDetail(val, this.investmentForm.value.fromDate, this.investmentForm.value.toDate);
  }
//------------------end--------------------

getEmployeeDCRDetail(userId, fromDate, toDate) {
  this.showDateWiseDCRReport = false;
  this.showDCRSummary = true;
  this.showProcessing = true;
  this._mgrInvestmentservice.getUserInvestmentDetail(userId, fromDate, toDate).subscribe(result => {
    const obj = {
      supervisorId: userId,
      companyId: this.currentUser.companyId,
      status: true,
      type: "upper"
    }

    //-----------------------------get joint working details by preeti----------
  this._mgrInvestmentservice.getUserJointworkDetails(userId, fromDate, toDate).subscribe(JointWorkResult => {
      this.dataSources1 = JointWorkResult;
      this.isDataSources1Empty = (this.dataSources1.length) ? true : false;
    }, err => {

      console.log(err)
      this._changeDetectorRef.detectChanges();
    });
    //-----------------------end-----------------------------------------//
    if (result == undefined) {
      this.showDateWiseDCRReport = false;
      this.showProcessing = false;
      this.toastr.info("No Record Found.")

    } else {
      this._hierarchyService.getManagerHierarchy(obj).subscribe((res: any) => {
        let maxdesignation = 100;
        let currentManager = { name: '---' };

        res.forEach(item => {
          if (item.designationLevel < maxdesignation) {
            maxdesignation = item.designationLevel;
            currentManager = item;
          }
        });

        result.forEach(i => {
          i['managerName'] = currentManager.name;
        });

        this.dataSources = result;
        this.showProcessing = false;
        this.FinalView=false;
        this.showDateWiseDCRReport = true;
        this._changeDetectorRef.detectChanges();
      })
    }

  }, err => {
    console.log(err)
    this.showDateWiseDCRReport = false;
    this.showProcessing = false;
    this.toastr.info("No Record Found.")
    this._changeDetectorRef.detectChanges();
  });
}


invstmentdataSource = {};

getInfo(investmentId){
  this._mgrInvestmentservice.getUserCompleteInvestmentDetails(investmentId).subscribe(res=>{
    if(res == 0 || res==undefined){
      alert("No Record Found");
    }else{
      this.FinalView=true;
      this.invstmentdataSource == res[0];
      this._changeDetectorRef.detectChanges();
    }
   },err=>{
    console.log(err)
  })
} 


exporttoExcel() {
  this.exportAsService.save(this.exportAsConfig, 'Investment Details').subscribe(() => {
    // save started

  });

}
} 