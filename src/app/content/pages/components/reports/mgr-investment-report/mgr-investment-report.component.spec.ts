import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MgrInvestmentReportComponent } from './mgr-investment-report.component';

describe('MgrInvestmentReportComponent', () => {
  let component: MgrInvestmentReportComponent;
  let fixture: ComponentFixture<MgrInvestmentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MgrInvestmentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MgrInvestmentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
