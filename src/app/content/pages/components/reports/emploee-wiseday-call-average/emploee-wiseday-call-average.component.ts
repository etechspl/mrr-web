import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { StateComponent } from '../../filters/state/state.component';
import { NativeDateAdapter, DateAdapter, MatDateFormats, MAT_DATE_FORMATS, MatStepper, MatSlideToggleChange, MatPaginator, MatTableDataSource, MatSort } from "@angular/material";
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import * as _moment from 'moment';
import { UtilsService, isString } from '../../../../../core/services/utils.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import * as XLSX from 'xlsx';
import { ToastrService } from 'ngx-toastr';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';

@Component({
  selector: 'm-emploee-wiseday-call-average',
  templateUrl: './emploee-wiseday-call-average.component.html',
  styleUrls: ['./emploee-wiseday-call-average.component.scss']
})
export class EmploeeWisedayCallAverageComponent implements OnInit {
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('tableData') tableRef: ElementRef;
  @ViewChild('sort') sort: MatSort;

  currentMonth = new Date().getMonth();
  currentYear = new Date().getFullYear();
  lastDay = new Date(this.currentYear, this.currentMonth, 0).getDate();
  @ViewChild('paginator') paginator: MatPaginator;
  actualMonth = this.currentMonth + 1;
  currentMonthLastDate =    this.currentYear + "-" + this.actualMonth + "-" + this.lastDay;
  currentMonthStartDate = this.currentYear + "-" + this.actualMonth + "-01";
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  showProcessing: boolean = false;
  showReportType: boolean = true;
  showType: boolean = true;
  showState: boolean = false;
  showDistrict: boolean = false;
  showReport: boolean = false;
  showEmployee: boolean = false;
  showDesignation: boolean = false;
  showDivisionFilter: boolean = false;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any;
  currentMonthStartDatefilter: any;
  currentMonthLastDatefilter: any;

  isShowTeamInfo: boolean = false;
  dynamicTableHeader: any = [];
  currentDate = new Date();
  public showDistrictWise: boolean = false;
  public showUserWise: boolean = false;
  expenseUpdateForm: FormGroup;
  constructor(
    private utilService: UtilsService,
    public hierarchyService: HierarchyService,
    private toastr: ToastrService
    , private _DCRReportService: DcrReportsService, private _fb: FormBuilder, private _ChangeDetectorRef: ChangeDetectorRef
  ) { }

  async ngOnInit() {
    let resultArr = [];

		let objForEmployeeAvg = {};
		if (this.currentUser.userInfo[0].rL === 1) {
		  objForEmployeeAvg = {
			companyId: this.currentUser.companyId,
			designation: this.currentUser.userInfo[0].designationLevel,
			userId:[this.currentUser.userInfo[0].userId],
			rL: this.currentUser.userInfo[0].rL
	
		  }
		}
	
		else if (this.currentUser.userInfo[0].rL === 2) {
		  
		  let object = {
			companyId: this.currentUser.companyId,
			supervisorId: this.currentUser.userInfo[0].userId,
			type: 'lower'
		  }
	
		  const resArr = await this.getHierarchy(object)
		  console.log("resArr",resArr)

		  resArr.forEach(element => {
			resultArr.push(element.userId)
		  });
	
		  objForEmployeeAvg = {
			companyId: this.currentUser.companyId,
			designation: this.currentUser.userInfo[0].designationLevel,
			rL: this.currentUser.userInfo[0].rL,
			userId: resultArr
		  }
	
	
		} else {
		  objForEmployeeAvg = {
			companyId: this.currentUser.companyId,
			designation: this.currentUser.userInfo[0].designationLevel
		  };
	
	    }
		this._DCRReportService.getAlldayPobEmployeeCallAverage(objForEmployeeAvg).subscribe(res => {
		  if (res.length > 0) {
        this.showUserWise = true;
        this.displayedColumns = ['sn', 'username', 'districtName', 'totalWorkingDay', 'drCall',  'chemCalls',  'stkCalls', 'doctorPOB', 'vendorPOB', 'stockistPOB'];
        this._ChangeDetectorRef.detectChanges();
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 200);
        this._ChangeDetectorRef.detectChanges();
      } else {
        this.toastr.info("No Record Found.");
        this.showProcessing = false;
        this.showUserWise = false;

      }
	});

  }
getHierarchy(obj):Promise<any>{
		let resultArr = []
		return new Promise((resolve,reject)=>{
		  this.hierarchyService.getManagerHierarchy(obj).subscribe(res => {
			if(!Boolean(res)) reject();
			else resolve(res);
		  })
		})
	}
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue
  }
}
