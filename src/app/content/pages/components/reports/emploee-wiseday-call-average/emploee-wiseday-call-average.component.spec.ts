import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploeeWisedayCallAverageComponent } from './emploee-wiseday-call-average.component';

describe('EmploeeWisedayCallAverageComponent', () => {
  let component: EmploeeWisedayCallAverageComponent;
  let fixture: ComponentFixture<EmploeeWisedayCallAverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmploeeWisedayCallAverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploeeWisedayCallAverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
