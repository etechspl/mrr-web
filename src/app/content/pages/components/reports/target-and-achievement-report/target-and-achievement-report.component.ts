import { MonthComponent } from '../../filters/month/month.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { MappedStockistComponent } from '../../filters/mapped-stockist/mapped-stockist.component';
import { TypeComponent } from '../../filters/type/type.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { StateComponent } from '../../filters/state/state.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { PrimaryAndSaleReturnService } from '../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service';
import { UtilsService } from '../../../../../core/services/utils.service';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';

declare var $;
@Component({
  selector: 'm-target-and-achievement-report',
  templateUrl: './target-and-achievement-report.component.html',
  styleUrls: ['./target-and-achievement-report.component.scss']
})
export class TargetAndAchievementReportComponent implements OnInit {
  showGeoReportType = false;
  showEmpReportType = false;
  showStokistReportType = false;
  typeFilter = false;
  stateFilter = false;
  districtFilter = false;
  employeeFilter = false;
  stokistFilter = false;
  public showReport = false;
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showAdminAndMGRLevelFilter:boolean=false;
  showProcessing: boolean = false;
  dataSource;
  data;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMappedStockistComponent") callMappedStockistComponent: MappedStockistComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild('dataTable') table;
  
  dataTable: any;
  dtOptions: any;
  public dynamicTableHeader: any = [];

  constructor(private stockiestService: StockiestService, 
    private _changeDetectorRef: ChangeDetectorRef,private utilService: UtilsService,
    private _PrimaryAndSaleReturnService: PrimaryAndSaleReturnService,    private exportAsService: ExportAsService
    ) { }
  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  sssConsolidate = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null),
    toDate: new FormControl(null),
    fromDate: new FormControl(null)
  })
  check: boolean = false;
  getReportType(val) {

    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      this.showDistrict = false;
      this.sssConsolidate.setControl("stateInfo", new FormControl())
      this.sssConsolidate.removeControl("employeeId");
      this.sssConsolidate.removeControl("designation");
    } else if (val == "Headquarter") {
      this.sssConsolidate.setControl("stateInfo", new FormControl())
      this.sssConsolidate.setControl("districtInfo", new FormControl())
      this.sssConsolidate.removeControl("employeeId");
      this.sssConsolidate.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      this.sssConsolidate.removeControl("stateInfo");
      this.sssConsolidate.removeControl("districtInfo");
      this.sssConsolidate.setControl("designation", new FormControl());
      this.sssConsolidate.setControl("employeeId", new FormControl());
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.sssConsolidate.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }
  exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Target Vs Achivement Details').subscribe(() => {
      // save started
    });
  }
  getStateValue(val) {
    this.sssConsolidate.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.sssConsolidate.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.sssConsolidate.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.sssConsolidate.value.divisionId
        })

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.sssConsolidate.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.sssConsolidate.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.sssConsolidate.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.sssConsolidate.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.sssConsolidate.patchValue({ toDate: val });
  }

  getDesignation(val) {
    this.sssConsolidate.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.sssConsolidate.value.divisionId,
        status: [true],
        designationObject: this.sssConsolidate.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  //setting division into dcrform
  getDivisionValue($event) {
    this.sssConsolidate.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.sssConsolidate.value.type === "State" || this.sssConsolidate.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.sssConsolidate.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.sssConsolidate.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.sssConsolidate.value.divisionId != null && this.sssConsolidate.value.designation != null) {
          if (this.sssConsolidate.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.sssConsolidate.value.divisionId,
              status: [true],
              designationObject: this.sssConsolidate.value.designation
            })
          }
        }
      }
    }
  }
  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.sssConsolidate.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }

    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }
  viewRecords(){
   
    this.showReport = false;
    this.showProcessing = true;
    if (this.sssConsolidate.value.employeeId == null) {
      this.sssConsolidate.value.employeeId = [this.currentUser.id];
    }
  
    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.sssConsolidate.value.divisionId;
    } else {
      params["division"] = [];
    }
    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.sssConsolidate.value.stateInfo;
    params["districtIds"] = this.sssConsolidate.value.districtInfo;
    params["type"] = this.sssConsolidate.value.type;
    params["employeeId"] = this.sssConsolidate.value.employeeId;
    params["fromDate"] = this.sssConsolidate.value.fromDate;
    params["toDate"] = this.sssConsolidate.value.toDate;
    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.sssConsolidate.value.fromDate, this.sssConsolidate.value.toDate);
    console.log("params: ------",params);
    this._PrimaryAndSaleReturnService.getTragetVsAchievementData(params).subscribe(res => {

      if(res.length>0){
        this.data = res;
        this.showReport=true;
        this.showProcessing=false;
      }else{
        alert('NO record Found')
      }
      this._changeDetectorRef.detectChanges();

    })

  }

}
