import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetAndAchievementReportComponent } from './target-and-achievement-report.component';

describe('TargetAndAchievementReportComponent', () => {
  let component: TargetAndAchievementReportComponent;
  let fixture: ComponentFixture<TargetAndAchievementReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetAndAchievementReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetAndAchievementReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
