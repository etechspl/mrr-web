import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { UtilsService } from './../../../../../core/services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { CategoryComponent } from '../../filters/category/category.component';
import { MatSlideToggleChange, MatTableDataSource } from '@angular/material';
import * as XLSX from 'xlsx';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
declare var $;

@Component({
  selector: 'm-provider-visit-analysis',
  templateUrl: './provider-visit-analysis.component.html',
  styleUrls: ['./provider-visit-analysis.component.scss']
})
export class ProviderVisitAnalysisComponent implements OnInit {
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('dataTable') table;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callCategoryComponent") callCategoryComponent: CategoryComponent;
  
  dataTable: any;
  dtOptions: any;
  hierarchyResult: any;

  currentUser = JSON.parse(sessionStorage.currentUser);
  ProviderVisitAnalysisFilterForm: FormGroup;
  showForMGROnly=false;
  displayedColumns: string[] = ['State', 'Headquater', 'Area', 'AreaCurrent','Doctor','DoctorCode','DoctorType','Category','Specialization','degree','doctorCurrentStutas','april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december', 'january', 'february', 'march'];
  displayedColumnsFinancly = ['stateName', 'districtName', 'areaName', 'providerName', 'providerCode', 'category', 'specialization', 'degree', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december', 'january', 'february', 'march'];
  displayedColumnsYearly = ['stateName', 'districtName', 'areaName', 'providerName', 'providerCode', 'category', 'specialization', 'degree', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
  displayedColumnsHirearchy = ['stateName', 'districtName', 'areaName', 'providerName', 'providerCode', 'category', 'specialization', 'degree', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

  dataSource:any;
  dataSourceYearly: any;
  dataSourceFinancly: any;
  dataSourceHierarchyWise: any;
  showForTeamSelectionOnly=true;
  data: any;
  otherTypeLablesExist = (this.currentUser.company.lables.otherType) ? true : false;

  viewForArray = [];
  selectedCategory = [];

  public dataViewFor: string = this.currentUser.company.lables.doctorLabel;
  public showProviderVisitAnalysisDetailsYearly: boolean = false;
  public showProviderVisitAnalysisDetailsFinanceYearly: boolean = false;
  public showYearFilter: boolean = true;
  public showReport: boolean = false;
  public showFinancialFilter: boolean = false;
  public showDoctorColumn: boolean = true;
  public showProcessing: boolean = false;
  public showDivisionFilter: boolean = false;
  public showCategory: boolean = false;
  public showFromDate: boolean = false;
  public showToDate: boolean = false;
  public dynamicTableHeader: any = [];
  public showReportHirearchyWise: boolean = false;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }


  constructor(private fb: FormBuilder,
    private _providerService: ProviderService,
    private _toastr: ToastrService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _monthYearService: MonthYearService,
    private utilService: UtilsService,
    private exportAsService: ExportAsService,
    private _hierarchy: HierarchyService
  ) {
    this.ProviderVisitAnalysisFilterForm = this.fb.group({
      fiscalType: new FormControl('yearly'),
      designation: new FormControl(''),
      status: new FormControl(true),
      team:new FormControl(""),
      employeeId: new FormControl(null),
      year: new FormControl(null),
      viewFor: new FormControl('RMP'),
      category: new FormControl(null),
      toDate: new FormControl(null),
      fromDate: new FormControl(null)
    });
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {

      this.ProviderVisitAnalysisFilterForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
if (this.currentUser.company.lables.other) {
      this.viewForArray = ["Drug", "Stockist"];
      this.currentUser.company.lables.otherType.forEach(i => {
        this.viewForArray.push(i.key);
      });
    } else {
      this.viewForArray = ["Drug", "Stockist"];
    }

    
  }


  getDivisionValue(val) {
    this.showProcessing = false;
    this.showReportHirearchyWise = false;
    this.showReport = false;
    this.showCategory = true;
    this.ProviderVisitAnalysisFilterForm.patchValue({ division: val });

    this.callDesignationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);


  }
  getFiscalType(val) {
    this.showProcessing = false;
    this.showReport = false;
    // this.showCategory= true;
    this.showFromDate = false;
    this.showToDate = false;
    this.showReportHirearchyWise = false;
    this.showProviderVisitAnalysisDetailsYearly = false;
    this.showProviderVisitAnalysisDetailsFinanceYearly = false;
    if (val === "yearly") {

      this.ProviderVisitAnalysisFilterForm.patchValue({fiscalType: 'yearly'});
      this.showYearFilter = true;
      this.showReport = false;
      this.showFromDate = false;
      this.showToDate = false;
      this.showFinancialFilter = false;
      this.showReportHirearchyWise = false;
      this.ProviderVisitAnalysisFilterForm.get('year').setValidators([Validators.required])
      this.ProviderVisitAnalysisFilterForm.get('year').updateValueAndValidity();
      this.ProviderVisitAnalysisFilterForm.patchValue({toDate: ''});

      this.ProviderVisitAnalysisFilterForm.removeControl("toDate");
      this.ProviderVisitAnalysisFilterForm.patchValue({fromDate: ''});
      this.ProviderVisitAnalysisFilterForm.removeControl("fromDate");
      this.ProviderVisitAnalysisFilterForm.setControl("year", new FormControl('', Validators.required))

    } else if (val === "financialYearly") {
      this.showYearFilter = false;
      this.showReport = false;
      this.showFinancialFilter = true;
      // this.showCategory= true;
      this.showFromDate = false;
      this.showToDate = false;
      this.showReportHirearchyWise = false;
      this.ProviderVisitAnalysisFilterForm.patchValue({fiscalType: 'financialYearly'});
      this.ProviderVisitAnalysisFilterForm.get('year').setValidators([Validators.required])
      this.ProviderVisitAnalysisFilterForm.get('year').updateValueAndValidity();
      this.ProviderVisitAnalysisFilterForm.setControl("year", new FormControl('', Validators.required))
      this.ProviderVisitAnalysisFilterForm.patchValue({toDate: ''});
      this.ProviderVisitAnalysisFilterForm.removeControl("toDate");
      this.ProviderVisitAnalysisFilterForm.patchValue({fromDate: ''});
      this.ProviderVisitAnalysisFilterForm.removeControl("fromDate");
    } else if (val === "hierarchyWise") {
      this.showYearFilter = false;
      this.showReport = false;
      this.showFinancialFilter = false;
      // this.showCategory= true;
      this.showFromDate = true;
      this.showToDate = true;
      this.showReportHirearchyWise = false;
      this.ProviderVisitAnalysisFilterForm.patchValue({fiscalType: 'hierarchyWise'});
      this.ProviderVisitAnalysisFilterForm.removeControl("year")
      this.ProviderVisitAnalysisFilterForm.get('year').clearValidators();
      this.ProviderVisitAnalysisFilterForm.get('year').updateValueAndValidity();
      this.ProviderVisitAnalysisFilterForm.patchValue({year: ''});
      this.ProviderVisitAnalysisFilterForm.setControl("fromDate", new FormControl('', Validators.required))
      this.ProviderVisitAnalysisFilterForm.setControl("toDate", new FormControl('', Validators.required))

    }


    let testObj = {}
    this.ProviderVisitAnalysisFilterForm.patchValue({ fiscalType: val });
    this.ProviderVisitAnalysisFilterForm.patchValue({ year: null });
  }
  // getDesignation(val) {
  //   this.showReportHirearchyWise = false;
  //   this.showProcessing = false;
  //   this.showProviderVisitAnalysisDetailsYearly = false;
  //   this.showProviderVisitAnalysisDetailsFinanceYearly = false;
  //   this.showCategory = true;
  //   this.ProviderVisitAnalysisFilterForm.patchValue({ designation: val });

  //   this.callEmployeeComponent.setBlank();

  //   if (this.currentUser.company.isDivisionExist == true) {
  //     if (this.currentUser.company.isDivisionExist == true && this.ProviderVisitAnalysisFilterForm.value.division != null && this.ProviderVisitAnalysisFilterForm.value.status != null) {
  //       let passingObj = {
  //         companyId: this.currentUser.companyId,
  //         designationObject: val.designationLevel,
  //         status: this.ProviderVisitAnalysisFilterForm.value.status,
  //         division: this.ProviderVisitAnalysisFilterForm.value.division
  //  }
    
  //       this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
  //     }
  //   } else {
  //     this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, this.ProviderVisitAnalysisFilterForm.value.status);
  //   }
  // }

  getDesignation(val) {
    
    this.showReportHirearchyWise = false;
    this.showProcessing = false;
    this.showProviderVisitAnalysisDetailsYearly = false;
    this.showProviderVisitAnalysisDetailsFinanceYearly = false;
    this.showCategory = true;
    this.ProviderVisitAnalysisFilterForm.patchValue({ designation: val });
    if(val.designationLevel>1){
      this.showForMGROnly=true;
    }else{
      this.showForMGROnly=false;
    }
    
    

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.ProviderVisitAnalysisFilterForm.value.division,
        };

        this.callEmployeeComponent.getEmployeeListBasedOnDivision(
          passingObj
        );
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(
          this.currentUser.companyId,
          [val],
          [true]
        );
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.ProviderVisitAnalysisFilterForm.value.division,
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
      }
    }
  }
  getTeamType(val){
     if(val=="withTeam"){
      this.showForTeamSelectionOnly=true
     }else{
      this.showForTeamSelectionOnly=false;
     }
     
  }
  getStatusValue(val) {
    this.showReportHirearchyWise = false;
    this.showProcessing = false;
    this.showCategory = true;
    this.showProviderVisitAnalysisDetailsYearly = false;
    this.showProviderVisitAnalysisDetailsFinanceYearly = false;
    this.ProviderVisitAnalysisFilterForm.patchValue({ status: val });

    if (this.currentUser.company.isDivisionExist == true) {
      if (this.currentUser.company.isDivisionExist == true && this.ProviderVisitAnalysisFilterForm.value.division != null && this.ProviderVisitAnalysisFilterForm.value.status != null) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: this.ProviderVisitAnalysisFilterForm.value.designation.designationLevel,
          status: val,
          division: this.ProviderVisitAnalysisFilterForm.value.division
    }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      }
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.ProviderVisitAnalysisFilterForm.value.designation.designationLevel, val);
    }
  }

  // getEmployeeValue(val) {
  //   this.showReportHirearchyWise = false;
  //   this.showProcessing = false;
  //   this.showCategory = true;

  //   this.showProviderVisitAnalysisDetailsYearly = false;
  //   this.showProviderVisitAnalysisDetailsFinanceYearly = false;
  //   this.ProviderVisitAnalysisFilterForm.patchValue({ employeeId: val });
  // }

  getEmployeeValue(emp: string) { 
    this.showReport = false;
    this.showReportHirearchyWise = false;
    this.ProviderVisitAnalysisFilterForm.patchValue({ employeeId: emp });
    const filter = { companyId: this.currentUser.companyId, supervisorId: emp, type: "lowerWithUpper" };    
      this._hierarchy.getManagerHierarchy(filter).subscribe(hierachyRes => {
      this.hierarchyResult=hierachyRes
    })

  }
  


  setTableHeading(obj) {
    this.showProcessing = false;

    this.showProviderVisitAnalysisDetailsYearly = false;
    this.showProviderVisitAnalysisDetailsFinanceYearly = false;
    if (obj.value == "RMP") {
      this.showReport = false;
      this.showCategory = true;

      this.showDoctorColumn = true;
      this.displayedColumnsFinancly = ['stateName', 'districtName', 'areaName', 'providerName', 'providerCode', 'category', 'specialization', 'degree', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december', 'january', 'february', 'march'];
      this.displayedColumnsYearly = ['stateName', 'districtName', 'areaName', 'providerName', 'providerCode', 'category', 'specialization', 'degree', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

      this.dataViewFor = this.currentUser.company.lables.doctorLabel;
    } else if (obj.value == "Drug") {
      this.ProviderVisitAnalysisFilterForm.patchValue({ category: '' });
      this.selectedCategory = [];
      this.showReport = false;
      this.showDoctorColumn = false;
      this.showCategory = false;
      this.displayedColumnsFinancly = ['stateName', 'districtName', 'areaName', 'providerName', 'providerCode', 'category', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december', 'january', 'february', 'march'];
      this.displayedColumnsYearly = ['stateName', 'districtName', 'areaName', 'providerName', 'providerCode', 'category', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

      this.dataViewFor = (this.otherTypeLablesExist == true) ? this.currentUser.company.lables.other : this.currentUser.company.lables.vendorLabel + '/' + this.currentUser.company.lables.stockistLabel;
    }
  }

  getCategory(val) {
    this.showReportHirearchyWise = false;
    this.showProcessing = false;
    this.showReport = false;
    this.selectedCategory = val;
    this.ProviderVisitAnalysisFilterForm.patchValue({ category: val });
  }

  getYear(val) {
    this.showReportHirearchyWise = false;
    this.showProcessing = false;
    this.showProviderVisitAnalysisDetailsYearly = false;
    this.showProviderVisitAnalysisDetailsFinanceYearly = false;
    this.ProviderVisitAnalysisFilterForm.patchValue({ year: val });
  }
  getFromDate(val) {
    this.showReportHirearchyWise = false;
    this.ProviderVisitAnalysisFilterForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.showReportHirearchyWise = false;
    this.ProviderVisitAnalysisFilterForm.patchValue({ toDate: val });
  }
  getHierachyData(val){
    let userIds = val.map((user)=>{
      return user.userId
    })
    this.ProviderVisitAnalysisFilterForm.patchValue({team: userIds})
    }


  viewRecords() {
    this.isToggled = false;
    this.showProcessing = true;
    this.showReport = false;
    this.showReportHirearchyWise = false;
    this.showProviderVisitAnalysisDetailsYearly = false;
    this.showProviderVisitAnalysisDetailsFinanceYearly = false;

    if(this.ProviderVisitAnalysisFilterForm.value.viewFor == "RMP"){
      this.viewForArray = ["RMP"];
    }else if(this.ProviderVisitAnalysisFilterForm.value.viewFor == "Drug"){
      this.viewForArray = ["Drug", "Stockist"];
    }

    if (this.ProviderVisitAnalysisFilterForm.value.fiscalType == "hierarchyWise") {
      this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.ProviderVisitAnalysisFilterForm.value.fromDate, this.ProviderVisitAnalysisFilterForm.value.toDate);
    
      this._providerService.getDoctorDisparityDetails(this.currentUser.companyId, this.ProviderVisitAnalysisFilterForm.value.employeeId, this.viewForArray, this.ProviderVisitAnalysisFilterForm.value.fiscalType, this.ProviderVisitAnalysisFilterForm.value.year, this.ProviderVisitAnalysisFilterForm.value.designation.designationLevel, this.ProviderVisitAnalysisFilterForm.value.category, this.ProviderVisitAnalysisFilterForm.value.fromDate, this.ProviderVisitAnalysisFilterForm.value.toDate).subscribe(res => {

        if (res.length == 0 || res == undefined) {
          this.showProcessing = false;
          this._toastr.info('For Selected Filter data not found', 'Data Not Found');
        } else {
          this.showProcessing = false;
          this.showReportHirearchyWise = true;
          res.sort(function (a, b) {
            let textA = a.userName.toUpperCase();
            var textB = b.userName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });

          // ----------AAKASH (24-02-2020)
          res.map((a, index) => {
            this.currentUser.company.type.map(i => {
              if (a.providerType == i.id) {
                res[index]['providerTypeUpdated'] = i.name;
              }
            });
          })
          // ----------AAKASH (24-02-2020)
          this.data = res;
          this._toastr.info('Data Found..');

        }
        this._changeDetectorRef.detectChanges();
      }, err => {
        console.log(err)
      });


    } else {

      this._providerService.getProviderVisitAnalysis(this.currentUser.companyId, this.ProviderVisitAnalysisFilterForm.value.employeeId, this.viewForArray, this.ProviderVisitAnalysisFilterForm.value.fiscalType, this.ProviderVisitAnalysisFilterForm.value.year, this.ProviderVisitAnalysisFilterForm.value.designation.designationLevel, this.ProviderVisitAnalysisFilterForm.value.category,this.ProviderVisitAnalysisFilterForm.value.team).subscribe(res => {
        if (res.length == 0 || res == undefined) {
          console.log("1-------------->");
          this.showProcessing = false;
          this.showProviderVisitAnalysisDetailsYearly = false;
          this.showProviderVisitAnalysisDetailsFinanceYearly = false;
          this._toastr.info('For Selected Filter data not found', 'Data Not Found');
        } else {
          if (this.ProviderVisitAnalysisFilterForm.value.fiscalType == "financialYearly") {
            // ----------AAKASH (24-02-2020)
            res.map((a, index) => {
              this.currentUser.company.type.map(i => {
                if (a.providerType == i.id) {
                  res[index]['providerTypeUpdated'] = i.name;
                }
              });
            })

            console.log("2-------------->");

            // ----------AAKASH (24-02-2020)
            this.dataSource = new MatTableDataSource(res);
            // this.dataSourceFinancly = res;
            this.showProcessing = false;
            this.showReport = true;
            let ProStatus = true;
            this._changeDetectorRef.detectChanges();
            console.log("3-------------->");

            // this.dtOptions = {
            //   "pagingType": 'full_numbers',
            //   "paging": true,
            //   "ordering": true,
            //   "info": true,
            //   "scrollY": 300,
            //   "scrollX": true,
            //   "fixedColumns": true,
            //   destroy: true,
            //   responsive: true,
            //   data: this.dataSourceFinancly,
            //   columns: [
            //     {
            //       title: 'State Name',
            //       data: 'stateName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.currentUser.company.lables.hqLabel,
            //       data: 'districtName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.currentUser.company.lables.areaLabel,
            //       data: 'areaName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.currentUser.company.lables.areaLabel + " Current Status",
            //       data: 'areaStatus',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + " Name",
            //       data: 'providerName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + " Code",
            //       data: 'providerCode',
            //       render: function (data) {

            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + " Type",
            //       data: 'providerTypeUpdated',
            //       render: function (data) {

            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'category',
            //       data: 'category',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'specialization',
            //       data: 'specialization',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'degree',
            //       data: 'degree',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + ' Current Status',
            //       data: 'providerStatus',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'April',
            //       data: 'data.' + 0,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'May',
            //       data: 'data.' + 1,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'June',
            //       data: 'data.' + 2,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'July',
            //       data: 'data.' + 3,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'August',
            //       data: 'data.' + 4,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'September',
            //       data: 'data.' + 5,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'October',
            //       data: 'data.' + 6,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'November',
            //       data: 'data.' + 7,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'December',
            //       data: 'data.' + 8,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'January',
            //       data: 'data.' + 9,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'February',
            //       data: 'data.' + 10,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'March',
            //       data: 'data.' + 11,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     }
            //   ],
            //   dom: 'Bfrtip',
            //   buttons: [
            //     'copy', 'csv', 'excel'
            //   ]
            // };
            // this.dataTable = $(this.table.nativeElement);
            // this.dataTable.DataTable(this.dtOptions);

          } else if (this.ProviderVisitAnalysisFilterForm.value.fiscalType == "yearly") {
            console.log("1.yearly-------------->");

                        // ----------AAKASH (24-02-2020)
                        res.map((a, index) => {
                          this.currentUser.company.type.map(i => {
                            if (a.providerType == i.id) {
                              res[index]['providerTypeUpdated'] = i.name;
                            }
                          });
                        })

                        // ----------AAKASH (24-02-2020)
            this.dataSource = new MatTableDataSource(res);
            this._changeDetectorRef.detectChanges();
            // this.dataSourceYearly = res;
            this.showProcessing = false;
            this.showReport = true;
            // this.dtOptions = {
            //   "pagingType": 'full_numbers',
            //   "paging": true,
            //   "ordering": true,
            //   "info": true,
            //   "scrollY": 300,
            //   "scrollX": true,
            //   "fixedColumns": true,
            //   destroy: true,
            //   responsive: true,
            //   data: this.dataSourceYearly,
            //   columns: [
            //     {
            //       title: 'State Name',
            //       data: 'stateName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.currentUser.company.lables.hqLabel,
            //       data: 'districtName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.currentUser.company.lables.areaLabel,
            //       data: 'areaName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.currentUser.company.lables.areaLabel + " Current Status",
            //       data: 'areaStatus',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + " Name",
            //       data: 'providerName',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + " Code",
            //       data: 'providerCode',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + " Type",
            //       data: 'providerTypeUpdated',
            //       render: function (data) {

            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'category',
            //       data: 'category',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'specialization',
            //       data: 'specialization',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'degree',
            //       data: 'degree',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: this.dataViewFor + ' Current Status',
            //       data: 'providerStatus',
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'Jan',
            //       data: 'data.' + 0,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'Feb',
            //       data: 'data.' + 1,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'March',
            //       data: 'data.' + 2,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'April',
            //       data: 'data.' + 3,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'

            //     },
            //     {
            //       title: 'May',
            //       data: 'data.' + 4,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'June',
            //       data: 'data.' + 5,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'July',
            //       data: 'data.' + 6,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'August',
            //       data: 'data.' + 7,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'September',
            //       data: 'data.' + 8,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'October',
            //       data: 'data.' + 9,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'November',
            //       data: 'data.' + 10,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     },
            //     {
            //       title: 'December',
            //       data: 'data.' + 11,
            //       render: function (data) {
            //         if (data === '' || data == "null") {
            //           return '---'
            //         } else {
            //           return data
            //         }
            //       },
            //       defaultContent: '---'
            //     }
            //   ],
            //   dom: 'Bfrtip',
            //   buttons: [
            //     {
            //       extend: "excel",
            //       exportOptions: {
            //         columns: ":visible",
            //       },
            //     },
            //     {
            //       extend: "copy",
            //       exportOptions: {
            //         columns: ":visible",
            //       },
            //     },
            //     {
            //       extend: "csv",
            //       exportOptions: {
            //         columns: ":visible",
            //       },
            //     },
            //     {
            //       extend: "print",
            //       title: function () {
            //         return "State List";
            //       },
            //       exportOptions: {
            //         columns: ":visible",
            //       },
            //     },
            //   ]
            // };
            // this.dataTable = $(this.table.nativeElement);
            // this.dataTable.DataTable(this.dtOptions);
          }
        }
        this._changeDetectorRef.detectChanges();
      }, err => {
        console.log(err)
      });
    }
  }
  export(){
  
    let dataToExport=[];
    dataToExport= this.dataSource.data.map((x)=>({
      "State Name" : x.stateName,
      "districtName" : x.districtName,
      "areaName" : x.areaName,
      "areaStatus"  : x.areaStatus,
      "providerName" : x.providerName,
      "providerCode" : x.providerCode,
      "providerTypeUpdated":x.providerTypeUpdated,
      "category ":x.category,
      "specialization" : x.specialization,
      "degree" : x.degree,
      "April" : x.data[0],
      "May" : x.data[1],
      "June" : x.data[2],
      "July" : x.data[3],
      "August" : x.data[4],
      "September" : x.data[5],
       "October" : x.data[6], 
       "November" : x.data[7],
       "December" : x.data[8],
       "January" : x.data[9],
       "February" : x.data[10],
       "March" : x.data[11],
    }));
    

    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Provider Visit Analysis.xlsx");
  }
  exporttoExcels() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Provider Visit Analysis Report').subscribe(() => {
      // save started
    });
  }

}
