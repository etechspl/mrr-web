import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderVisitAnalysisComponent } from './provider-visit-analysis.component';

describe('DoctorVisitAnalysisComponent', () => {
  let component: ProviderVisitAnalysisComponent;
  let fixture: ComponentFixture<ProviderVisitAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderVisitAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderVisitAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
