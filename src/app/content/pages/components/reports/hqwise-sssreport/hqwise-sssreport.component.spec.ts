import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HQWiseSSSReportComponent } from './hqwise-sssreport.component';

describe('HQWiseSSSReportComponent', () => {
  let component: HQWiseSSSReportComponent;
  let fixture: ComponentFixture<HQWiseSSSReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HQWiseSSSReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HQWiseSSSReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
