import { Router } from '@angular/router';
import { UserCompleteDCRInfoComponent } from './../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StateComponent } from '../../filters/state/state.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { MonthComponent } from '../../filters/month/month.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DivisionComponent } from '../../filters/division/division.component';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import * as _moment from 'moment';
declare var $;
import Swal from 'sweetalert2'
import { DCRBackupService } from '../../../../../core/services/DCRBackup/dcrbackup.service';
import { DCRProviderVisitDetailsBackupService } from '../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service';
import { UtilsService } from '../../../../../core/services/utils.service';
import { MatSlideToggleChange } from '@angular/material';
import { sum } from 'lodash';


@Component({
  selector: 'm-hqwise-sssreport',
  templateUrl: './hqwise-sssreport.component.html',
  styleUrls: ['./hqwise-sssreport.component.scss']
})


export class HQWiseSSSReportComponent implements OnInit {
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;

  count = 0;
  totalSum: any = 0;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  showAdminAndMGRLevelFilter = false;

  public dynamicTableHeader: any = [];
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
data;

  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
  //dataTableForDCRDateWise: any;
  dtOptionsForDCRDateWise: any;

  constructor(private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private _dcrReportsServiceBackup: DCRBackupService,
    private DCRProviderVisitDetailsBackupService: DCRProviderVisitDetailsBackupService,
    private exportAsService: ExportAsService,    private utilService: UtilsService,
    ) {

     }
    isShowDivision = false;
    currentUser = JSON.parse(sessionStorage.currentUser);
    isDivisionExist = this.currentUser.company.isDivisionExist;
    designationLevel = this.currentUser.userInfo[0].designationLevel;
    companyId = this.currentUser.companyId;
    sssForm = new FormGroup({
      reportType: new FormControl('Geographical'),
      type: new FormControl(null),
  
      stateInfo: new FormControl(null),  
      districtInfo: new FormControl(null),
      employeeId: new FormControl(null),
      month: new FormControl(null),
      year: new FormControl(null),
      designation: new FormControl(null),
      toDate: new FormControl(null, Validators.required),
      fromDate: new FormControl(null, Validators.required)
    })
    check: boolean = false;

    isToggled = true;
    toggleFilter(event: MatSlideToggleChange) {
      this.isToggled = event.checked;
    }
  getReportType(val) {

    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      this.showDistrict = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.sssForm.removeControl("employeeId");
      this.sssForm.removeControl("designation");
      this.sssForm.setControl("stateInfo", new FormControl('',Validators.required))
    } else if (val == "Headquarter") {
      this.showDistrict = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.sssForm.removeControl("employeeId");
      this.sssForm.removeControl("designation");
      this.sssForm.setControl("stateInfo", new FormControl('',Validators.required))
      this.sssForm.setControl("districtInfo", new FormControl('',Validators.required))
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.sssForm.removeControl("stateInfo");
      this.sssForm.removeControl("districtInfo");
      this.sssForm.setControl("designation", new FormControl('',Validators.required));
      this.sssForm.setControl("employeeId", new FormControl('',Validators.required));
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.sssForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }
  getStateValue(val) {
    this.sssForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.sssForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.sssForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.sssForm.value.divisionId
        })

      }
    } else {
      if (this.sssForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.sssForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.sssForm.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.sssForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.sssForm.patchValue({ toDate: val });
  }

  getDesignation(val) {
    this.sssForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.sssForm.value.divisionId,
        status: [true],
        designationObject: this.sssForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  //setting division into dcrform
  getDivisionValue($event) {
    this.sssForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.sssForm.value.type === "State" || this.sssForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.sssForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.sssForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.sssForm.value.divisionId != null && this.sssForm.value.designation != null) {
          if (this.sssForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.sssForm.value.divisionId,
              status: [true],
              designationObject: this.sssForm.value.designation
            })
          }
        }
      }
    }
  }
   exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'HQ Wise Stock and Sales').subscribe(() => {
      // save started
    });
  }
  viewRecords() {
    this.isToggled = false;
    this.showProcessing = true;

    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.sssForm.value.divisionId;
    } else {
      params["division"] = [];
    }
    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.sssForm.value.stateInfo;
    params["districtIds"] = this.sssForm.value.districtInfo;
    params["type"] = this.sssForm.value.type;
    params["employeeId"] = this.sssForm.value.employeeId;
    params["fromDate"] = this.sssForm.value.fromDate;
    params["toDate"] = this.sssForm.value.toDate;
    let totalsum=0;
    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.sssForm.value.fromDate, this.sssForm.value.toDate);
    this._dcrReportsService.HqWiseSecondaryReport(params).subscribe(res => {
      
      if(res.length>0){
        this.showProcessing = false;
        this.showReport = true;
        res.forEach(element => {
          totalsum=totalsum + parseFloat(element.totalAmt);
        });
        console.log(totalsum);
        
        this.totalSum = totalsum.toFixed(2);
        this.data = res;
        
        this._changeDetectorRef.detectChanges();
      }else{
        this.showReport = false;
      this.showDateWiseDCRReport = false;
      this.showProcessing = false;
      this.toastr.info("No Record Found.")
      this._changeDetectorRef.detectChanges();
      }
       
    }, err => {
      this.showReport = false;
      this.showDateWiseDCRReport = false;
      this.showProcessing = false;
      this.toastr.info("No Record Found.")
      this._changeDetectorRef.detectChanges();
    })
    this._changeDetectorRef.detectChanges();
  }

  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.sssForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;

      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }

}
