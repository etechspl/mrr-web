import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DesignationService } from '../../../../../core/services/Designation/designation.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { AreaComponent } from '../../filters/area/area.component';
import { StatusComponent } from '../../filters/status/status.component';
import { AreaService } from '../../../../../core/services/Area/area.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { CategoryComponent } from '../../filters/category/category.component';
import { MatSlideToggleChange } from '@angular/material';
import * as _moment from 'moment';
import { URLService } from '../../../../../core/services/URL/url.service';
declare var $;
@Component({
  selector: 'm-untouched-provider',
  templateUrl: './untouched-provider.component.html',
  styleUrls: ['./untouched-provider.component.scss']
})
export class UntouchedProviderComponent implements OnInit {
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callAreaComponent") callAreaComponent: AreaComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callCategoryComponent") callCategoryComponent: CategoryComponent;


  displayedColumns = ['sn', 'stateName', 'districtName', 'areaName', 'areaStatus', 'userName', 'providerName', 'providerCode', 'category', 'specialization', 'degree', 'phone', 'providerStatus'];
  dataSource: any
  untouchedProviderFilter: FormGroup;
  areaListOfMR: any;
  currentUser = JSON.parse(sessionStorage.currentUser);

  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  selectedCategory = [];
  otherLabel;
  otherlableArr;
  viewForArray = ["RMP"];

  public showType: boolean = false;
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showArea: boolean = false;
  public showAreaOnMR: boolean = false;
  public showUntouchedProviderDetails: boolean = false;
  public showDesignation: boolean = false;
  public showStatus: boolean = false;
  public showEmployee: boolean = false;
  public showReport: boolean = false;
  public dataViewFor: string = this.currentUser.company.lables.doctorLabel;
  showProcessing: boolean = false;
  public showCategory: boolean = false;
  isShowDivision: boolean;

  @ViewChild('untouchedDataTable') table1;
  untouchedDataTable: any;
  untouchedDataTableOptions: any;

  constructor(private fb: FormBuilder,
    private _designation: DesignationService,
    private _dcrReportsService: DcrReportsService,
    private _areaService: AreaService,
    private _changeDetectorRef: ChangeDetectorRef,
	private _toastr: ToastrService,
	private _urlService: URLService

  ) {
    this.untouchedProviderFilter = this.fb.group({
      reportType: new FormControl('Geographical'),
      type: new FormControl(null, Validators.required),

      //stateInfo: new FormControl(null),
      //districtInfo: new FormControl(null),
      //areaInfo: new FormControl(null),
      //areaOnMR: new FormControl(null),
      //designation: new FormControl(''),
	  status: new FormControl(true),
      employeeId: new FormControl(null),
	  viewFor: new FormControl('RMP', Validators.required),
      toDate: new FormControl(null,Validators.required),
      fromDate: new FormControl(null,Validators.required)
    })
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
    if (this.isDivisionExist === true) {
      if (this.designationLevel === 1) {
        this.isShowDivision = false;
        this.showCategory = true;
        this.untouchedProviderFilter.setControl("areaInfo", new FormControl());
      } else {
        this.isShowDivision = true;
        this.showCategory = true;
      }


      this.untouchedProviderFilter.setControl('divisionId', new FormControl());
      this.untouchedProviderFilter.setControl('isDivisionExist', new FormControl(true));
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;

      this.untouchedProviderFilter.removeControl('divisionId');
      this.untouchedProviderFilter.removeControl('isDivisionExist');
    }
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin and Manger
      this.showType = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithArea");

    } else if (this.currentUser.userInfo[0].designationLevel == 1) { //For MR
      //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "onlyArea");
      this.showArea = true;
      //this.callAreaComponent.getAreaList(this.currentUser.companyId, [], this.currentUser.userInfo[0].rL, [this.currentUser.userInfo[0].userId]);
    }
    if (this.isDivisionExist === true) {
      this.isShowDivision === true;
    } else {
      this.isShowDivision = false;
    }

    if(this.currentUser.company.lables.other){
      this.otherLabel = this.currentUser.company.lables.other;
      this.otherlableArr = ["Drug", "Stockist"];
      this.currentUser.company.lables.otherType.forEach(i=>{
        this.otherlableArr.push(i.key);
      });
    } else {
      this.otherlableArr = ["Drug", "Stockist"];
    }


  }
  getReportType(val) {

    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    this.untouchedProviderFilter.patchValue({type: ''});
    this.untouchedProviderFilter.patchValue({category: ''});
    this.selectedCategory =[];
    this.callDivisionComponent.setBlank();
    if (val === "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showArea = false;
      this.showAreaOnMR = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.showCategory = true;

      if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithArea");
        this.showCategory = true;
      } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithArea");
        this.showCategory = true;
      } else if (this.currentUser.userInfo[0].designationLevel == 1) { //For MR
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "onlyArea");
        this.showCategory = true;
      }
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.showArea = false;
      this.showAreaOnMR = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.showCategory = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }
  getTypeValue(val) {
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    if (val == "State") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.showState = true;
      this.showDistrict = false;
      this.showArea = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.showCategory = true;


      this.untouchedProviderFilter.setControl("stateInfo", new FormControl('', Validators.required))
      this.untouchedProviderFilter.setControl("category", new FormControl(''));

      this.untouchedProviderFilter.removeControl("areaInfo");
      this.untouchedProviderFilter.removeControl("employeeId");
      this.untouchedProviderFilter.removeControl("designation");
      this.untouchedProviderFilter.removeControl("districtInfo");
    } else if (val == "Headquarter") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.showState = true;
      this.showDistrict = true;
      this.showArea = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.showCategory = true;

      this.untouchedProviderFilter.setControl("stateInfo", new FormControl('',Validators.required))
      this.untouchedProviderFilter.setControl("districtInfo", new FormControl('',Validators.required))
      this.untouchedProviderFilter.setControl("category", new FormControl(''));

      this.untouchedProviderFilter.removeControl("areaInfo");
      this.untouchedProviderFilter.removeControl("employeeId");
      this.untouchedProviderFilter.removeControl("designation");

    } else if (val == "Area") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }

      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.untouchedProviderFilter.setControl("stateInfo", new FormControl(''))
      this.untouchedProviderFilter.setControl("districtInfo", new FormControl(''))
      this.untouchedProviderFilter.setControl("areaInfo", new FormControl(''))
      this.untouchedProviderFilter.setControl("category", new FormControl(''));
      this.untouchedProviderFilter.removeControl("employeeId");
      this.untouchedProviderFilter.removeControl("designation");

      if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
        this.showState = true;
        this.showDistrict = true;
        this.showArea = true;
        this.showDesignation = false;
        this.showStatus = false;
        this.showEmployee = false;
        this.showCategory = true;
      } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
        this.showState = true;
        this.showDistrict = true;
        this.showArea = true;
        this.showDesignation = false;
        this.showStatus = false;
        this.showEmployee = false;
        this.showCategory = true;
      } else if (this.currentUser.userInfo[0].designationLevel == 1) {  //For MR
        this.showState = false;
        this.showDistrict = false;
        this.showArea = false;
        this.showAreaOnMR = true;
        this.showDesignation = false;
        this.showStatus = false;
        this.showEmployee = false;
        this.showCategory = true;

        //basis of districtId and we are using for designation level 1 so here we not need to pass 2nd param.
        this._areaService.getAreaList(this.currentUser.companyId, [], this.currentUser.userInfo[0].designationLevel, [this.currentUser.userInfo[0].userId]).subscribe(res => {
          this.areaListOfMR = res;
        }, err => {
          console.log(err);
        })
      }
    } else if (val == "Employee Wise") {
      this.showState = false;
      this.showDistrict = false;
      this.showArea = false;
      this.showAreaOnMR = false;
      this.showDesignation = true;
      this.showStatus = true;
      this.showEmployee = true;
      this.showCategory = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }

      this.untouchedProviderFilter.removeControl("stateInfo");
      this.untouchedProviderFilter.removeControl("districtInfo");
      this.untouchedProviderFilter.removeControl("areaInfo");
      this.untouchedProviderFilter.setControl("employeeId", new FormControl('', Validators.required));
      this.untouchedProviderFilter.setControl("status", new FormControl(true));
      this.untouchedProviderFilter.setControl("designation", new FormControl('', Validators.required));
      this.untouchedProviderFilter.setControl("category", new FormControl(''));
    } else {
    }
    this.untouchedProviderFilter.patchValue({ type: val });

  }

  //setting division into dcrform
  getDivisionValue(val) {
    this.untouchedProviderFilter.patchValue({ divisionId: val });
    if (this.currentUser.userInfo[0].designationLevel == 0) {
      if (this.untouchedProviderFilter.value.type == "Employee Wise") {
        //=======================Clearing Filter===================
        // let passingObjForEmp = {
        //   companyId: this.currentUser.companyId,
        //   designationObject: 0,
        //   status: [true],
        //   division: []
        // }
        // this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObjForEmp);


        this.callDesignationComponent.setBlank();
        this.callStatusComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================



        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.untouchedProviderFilter.value.type == "State" || this.untouchedProviderFilter.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================


        let divisionIds = {
          division: val
        };
        this.callStateComponent.getStateBasedOnDivision(divisionIds);
      }
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      if (this.untouchedProviderFilter.value.type == "Employee Wise") {
        console.log("in if--");
        
        //=======================Clearing Filter===================
        this.callDesignationComponent.setBlank();
        this.callStatusComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.untouchedProviderFilter.value.type == "State" || this.untouchedProviderFilter.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================

        let passingObj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: true,
          division: this.untouchedProviderFilter.value.divisionId,
          supervisorId: this.currentUser.id,
        }
        this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)

      }
    }

  }

  getStateValue(val) {
    this.untouchedProviderFilter.patchValue({ stateInfo: val });
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;

    if (this.isDivisionExist === true) {
      if (this.untouchedProviderFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.untouchedProviderFilter.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.untouchedProviderFilter.value.divisionId
        })

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.untouchedProviderFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }


  // getStateValue(val) {
  //   this.showUntouchedProviderDetails = false;
  //   this.showProcessing = false;
  //   this.untouchedProviderFilter.patchValue({ stateInfo: val });

  //   if (this.untouchedProviderFilter.value.type == "Headquarter" || this.untouchedProviderFilter.value.type == "Area") {
  //     this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
  //   }
  // }
  getDistrictValue(val) {
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    this.untouchedProviderFilter.patchValue({ districtInfo: val });
    //areaInfo()
    //throwing error when selecting type headquarter wise. now updated the condition if
    //showArea is true then only it should execute
    if (this.showArea) {
      this.callAreaComponent.getAreaList(this.currentUser.companyId, val[0], this.currentUser.userInfo[0].designationLevel, [this.currentUser.userInfo[0].userId]);
    }


  }
  getAreaValue(val) {
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    this.untouchedProviderFilter.patchValue({ areaInfo: val });
  }

//   getDesignation(val) {
//     this.showUntouchedProviderDetails = false;
//     this.showProcessing = false;
//     this.untouchedProviderFilter.patchValue({ designation: val });
//   }


getDesignation(val) {
  this.showUntouchedProviderDetails = false;
  this.showProcessing = false;
	this.untouchedProviderFilter.patchValue({ designation: val.designationLevel });
	 this.callEmployeeComponent.setBlank();
	 if (this.currentUser.userInfo[0].rL === 0) {
	   if (this.currentUser.company.isDivisionExist == true) {
		 let passingObj = {
		   companyId: this.currentUser.companyId,
		   designationObject: [val],
		   status: [true],
		   division: this.untouchedProviderFilter.value.divisionId
		 }
		this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);

	   } else if (this.currentUser.company.isDivisionExist == false) {
		 this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
	   }
	 } else {
	   let dynamicObj = {};
	   if (this.currentUser.company.isDivisionExist == false) {
		 dynamicObj = {
		   supervisorId: this.currentUser.id,
		   companyId: this.currentUser.companyId,
		   status: true,
		   type: "lower",
		   designation: [val.designationLevel] //desig
		 };
		 this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
	   } else if (this.currentUser.company.isDivisionExist == true) {
		 dynamicObj = {
		   supervisorId: this.currentUser.id,
		   companyId: this.currentUser.companyId,
		   status: true,
		   type: "lower",
		   designation: [val.designationLevel], //desig,
		   isDivisionExist: true,
		   division: this.untouchedProviderFilter.value.divisionId
     };
     
		 this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
	   }

	 }
   }



  getStatusValue(val) {
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
   // this.callEmployeeComponent.setBlank();
    this.untouchedProviderFilter.patchValue({ status: val });

    // if (this.isDivisionExist === true) {
    //   this.callEmployeeComponent.getEmployeeListBasedOnDivision({
    //     companyId: this.companyId,
    //     division: this.untouchedProviderFilter.value.divisionId,
    //     status: val,
    //     designationObject: [this.untouchedProviderFilter.value.designation]
    //   })
    // } else {
    //   this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.untouchedProviderFilter.value.designation.designationLevel, val);
    // }
  }

  getEmployeeValue(val) {
	this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    this.showCategory = true;
    this.untouchedProviderFilter.patchValue({ employeeId: val });
  }

//   getEmployee(emp: string) {
//     this.untouchedProviderFilter.patchValue({ userId: emp })
//   }

  getFromDate(val) {
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    this.showCategory = true;
    this.untouchedProviderFilter.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    this.showCategory = true;
    this.untouchedProviderFilter.patchValue({ toDate: val });
  }

  getCategory(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.selectedCategory = val;
    this.untouchedProviderFilter.patchValue({ category: val });
  }


  setTableHeading(obj) {
    this.showUntouchedProviderDetails = false;
    this.showProcessing = false;
    if (obj.value == "RMP") {
      this.dataViewFor = this.currentUser.company.lables.doctorLabel;
      this.viewForArray = ["RMP"];
      this.showCategory = true;
      this.untouchedProviderFilter.get('category').setValidators([Validators.required])
      this.untouchedProviderFilter.get('category').updateValueAndValidity();
    } else if (obj.value == "Drug") {
      // this.untouchedProviderFilter.patchValue({ category: "" });
      this.selectedCategory = [];
      this.untouchedProviderFilter.get('category').clearValidators()
      this.untouchedProviderFilter.get('category').updateValueAndValidity();
      this.untouchedProviderFilter.patchValue({ category: [] });
      this.viewForArray = this.otherlableArr;
      this.showCategory = false;
      if(this.otherLabel){
        // this.dataViewFor = this.currentUser.company.lables.vendorLabel;
        this.untouchedProviderFilter.get('category').clearValidators()
        this.untouchedProviderFilter.get('category').updateValueAndValidity();
        this.dataViewFor = this.otherLabel;
      } else {
        this.dataViewFor = this.currentUser.company.lables.vendorLabel+`/`+this.currentUser.company.lables.stockistLabel;
      }
    }
  }

  viewRecords() {
    this.isToggled = false;
    this.showUntouchedProviderDetails = false;
    this.showProcessing = true;

    let employeeIds = this.untouchedProviderFilter.value.employeeId; // By default for ADMIN level.

    let designationLevel;

    if (this.untouchedProviderFilter.value.type == "Self" || this.untouchedProviderFilter.value.type == null) { //null is for MR level
      if (this.untouchedProviderFilter.value.type === null) {
        this.untouchedProviderFilter.patchValue({ divisionId: this.currentUser.userInfo[0].divisionId });
      }
      employeeIds = [this.currentUser.userInfo[0].userId]; //Passing manager id in array;
      designationLevel = this.currentUser.userInfo[0].designationLevel;
    } else if (this.untouchedProviderFilter.value.type == "Employee Wise") {
       designationLevel = this.untouchedProviderFilter.value.designation;
    }
    let selectedAreas = this.untouchedProviderFilter.value.areaInfo; // For manager and admin

    let typeValue = this.untouchedProviderFilter.value.type;
    if (this.currentUser.userInfo[0].designationLevel == 1) {
      typeValue = "Area";
    }
    let divisionIds = [];
    if (this.isDivisionExist === true) {
      divisionIds = this.untouchedProviderFilter.value.divisionId;
    }

    this._dcrReportsService.getUntouchedDotorDetail(this.currentUser.companyId,
      typeValue,
      this.untouchedProviderFilter.value.stateInfo,
      this.untouchedProviderFilter.value.districtInfo,
      selectedAreas,//this.untouchedDoctorFilter.value.areaInfo,
      designationLevel,
      employeeIds,
      this.viewForArray,
      this.untouchedProviderFilter.value.fromDate,
      this.untouchedProviderFilter.value.toDate,
      this.isDivisionExist,
      this.untouchedProviderFilter.value.status,
      divisionIds,
      this.untouchedProviderFilter.value.category,
    ).subscribe(res => {
      
      if (res.untouchedProvider.length == 0 || res == undefined) {
        this.showUntouchedProviderDetails = false;
        this.showProcessing = false;

        this._toastr.info('For Selected Filter data not found', 'Data Not Found');

      } else {
        res.untouchedProvider.map((item, index) => {
          item['index'] = index;
      });
        this.dataSource = res.untouchedProvider;
        this.showProcessing = false;
        this.showUntouchedProviderDetails = true;
    this.showReport = true;
    

		const PrintTableFunction = this.PrintTableFunction.bind(this);
        this.untouchedDataTableOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: res.untouchedProvider,
          responsive: true,
          columns: [
            {
              title: 'S. No.',
              data: 'index',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data + 1;
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Division',
              data: 'divisionName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }
            ,
            {
              title: 'State',
              data: 'stateName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.hqLabel,
              data: 'districtName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.areaLabel,
              data: 'areaName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Address',
              data: 'address',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
           
            {
              title: this.currentUser.company.lables.areaLabel + ' Current Status',
              data: 'areaStatus',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Employee Name',
              data: 'userName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.dataViewFor + ' Name',
              data: 'providerName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.dataViewFor + ' Code',
              data: 'providerCode',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'category',
              data: 'category',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Specialization',
              data: 'specialization',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Degree',
              data: 'degree',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            //  {
            //   title: 'Frequency Visit',
            //   data: 'frequencyVisit',
            //   render: function (data) {
            //     if (data === '') {
            //       return '---'
            //     } else {
            //       return data
            //     }
            //   },
            //   defaultContent: '---'
            // },
            {
              title: 'Mobile Number',
              data: 'phone',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.dataViewFor + ' Current Status',
              data: 'providerStatus',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }

          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
          },
          dom: 'Bfrtip',
           buttons: [
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
			extend: 'print',
			title: function () {
			return 'State List';
			},
			exportOptions: {
			columns: ':visible'
			},
			action: function (e, dt, node, config) {
			PrintTableFunction(res);
				}
			 }

        ]
        };

        if (this.isDivisionExist === false) {
          this.untouchedDataTableOptions.columns[1].visible = false;
        }
        this._changeDetectorRef.detectChanges();
        this.untouchedDataTable = $(this.table1.nativeElement);
        this.untouchedDataTable.DataTable(this.untouchedDataTableOptions);
        this._changeDetectorRef.detectChanges();

      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      console.log(err)
      this._toastr.info('', 'No Record Found');
      //alert("No Record Found");
    })


  }



PrintTableFunction(data?: any): void {
	// const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
	const currentDateAndTime = _moment(new Date()).format('DD/MM/YYYY, dddd , HH:mm A');
	const department = this.currentUser.section;
	const designation = this.currentUser.designation;
	const companyName = this.currentUser.companyName;

	const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

	const tableRows: any = [];
	data.untouchedProvider.forEach(element => {
	tableRows.push(`<tr>
  ${ this.isDivisionExist ? `<td class="tg-oiyu">${element.divisionName}</td>`: "" } 
	<td class="tg-oiyu">${element.stateName}</td>
	<td class="tg-oiyu">${element.districtName}</td>
	<td class="tg-oiyu">${element.areaName}</td>
	<td class="tg-oiyu">${element.areaStatus}</td>
	<td class="tg-oiyu">${element.userName}</td>
	<td class="tg-oiyu">${element.providerName}</td>
	<td class="tg-oiyu">${element.providerCode}</td>
	<td class="tg-oiyu">${element.specialization}</td>
	<td class="tg-oiyu">${element.degree}</td>
	<td class="tg-oiyu">${element.frequencyVisit}</td>
	<td class="tg-oiyu">${element.phone}</td>
	<td class="tg-oiyu">${element.providerStatus}</td>
	</tr>`)
	});

	let showHeaderAndTable: boolean = false;
	// this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
	let printContents, popupWin;
	popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
	popupWin.document.open();
	popupWin.document.write(`
	<html>
	<head>
	<style>
	img {
	float: left;
	}

	.flex-container {
	display: flex;
	justify-content: space-between;
	}

	.tg {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg .tg-lboi {
	border-color: inherit;
	text-align: left;
	vertical-align: middle;

	}

	.tg .tg-u6fn {
	font-weight: bold;
	font-size: 18px;
	background-color: #efefef;
	color:black;
	border-color: inherit;
	text-align: left;
	vertical-align: top;
	padding-top: 0px;
	padding-bottom: 0px;
	}

	.tg .tg-yz93 {
	border-color: inherit;
	text-align: right;
	vertical-align: middle
	}
	.tb2 {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tb2 td {
	font-family: Arial, sans-serif;
	font-size: 12px;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 th {
	font-family: Arial, sans-serif;
	font-size: 12px;
	font-weight: normal;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 .tg-oiyu {
	font-size: 10px;
	text-align: left;
	vertical-align: middle
	}
	.tg-bckGnd {
	font-weight: bold;
	background-color: #efefef;
	}

	.tb2 .tg-3ppa {
	font-size: 10px;
	background-color: #efefef;
	color:black;
	text-align: left;
	vertical-align: middle;
	padding-top: 2px;
	padding-bottom: 2px;
	}

	.footer {
	position: sticky;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: #efefef;
	color: rgb(2, 2, 2);
	text-align: right
	}

	button {
	background-color: #008CBA;
	border: none;
	color: white;
	margin: 5px 0px;
	padding: 2px 10px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
	border-radius: 12px;
	cursor: pointer;
	outline: none;
	}
	@media print {
	@page { size: A4; margin: 5mm; }
	.footer {
	position: relative;
	}
	tr {
	page-break-inside: avoid;
	}
	button{
	display : none;
	}
	}
	</style>
	</head>
	<body onload="window.print();window.close()" style="padding-bottom:30px">
	<div class="flex-container">
	<div style="width: 40%; ">
	<img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	<h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

	</div>

	<div style="text-align: right;">
	<h2 style="margin-top: 0px; margin-bottom: 5px">Missed Doctor Detail</h2>
	<div>
	<button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	<button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	</div>
	</div>
	</div>

	<div>
	${/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/''}
	<table class="tb2">
  <tr>
  ${
		this.isDivisionExist
			? `<th class="tg-3ppa"><span style="font-weight:600">Division Name</span></th>`
			: ""
	} 

	<th class="tg-3ppa"><span style="font-weight:600">State</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Headquarter</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Area</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Area Current Status</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Employee Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.doctorLabel} Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.doctorLabel} Code</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Specialization</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Degree</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Frequency Visit</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Mobile Number</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.doctorLabel} Current Status</span></th>
	</tr>
	${tableRows.join('')}
	</table>
	</div>
	<div class="footer flex-container">
	<p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>


	<p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
	</div>
	</body>
	</html>
	`);

	// popupWin.document.close();

	}

}
