import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UntouchedProviderComponent } from './untouched-provider.component';

describe('UntouchedProviderComponent', () => {
  let component: UntouchedProviderComponent;
  let fixture: ComponentFixture<UntouchedProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UntouchedProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UntouchedProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
