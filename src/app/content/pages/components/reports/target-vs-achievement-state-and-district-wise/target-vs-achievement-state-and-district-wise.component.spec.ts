import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetVsAchievementStateAndDistrictWiseComponent } from './target-vs-achievement-state-and-district-wise.component';

describe('TargetVsAchievementStateAndDistrictWiseComponent', () => {
  let component: TargetVsAchievementStateAndDistrictWiseComponent;
  let fixture: ComponentFixture<TargetVsAchievementStateAndDistrictWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetVsAchievementStateAndDistrictWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetVsAchievementStateAndDistrictWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
