import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PrimaryAndSaleReturnService } from '../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service';
import Swal from 'sweetalert2'
import { DistrictComponent } from '../../filters/district/district.component';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { StateService } from '../../../../../core/services/state.service';

@Component({
  selector: 'm-target-vs-achievement-state-and-district-wise',
  templateUrl: './target-vs-achievement-state-and-district-wise.component.html',
  styleUrls: ['./target-vs-achievement-state-and-district-wise.component.scss']
})
export class TargetVsAchievementStateAndDistrictWiseComponent implements OnInit {
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild('dataTable') table;
  currentUser = JSON.parse(sessionStorage.currentUser);
  TvsAFilter: FormGroup;
  displayedColumns = ['districtName', 'target', 'primary','perAchAmt', 'secondary','clossing'];
  dataSource;
  showTable = false;
  showProcessing = false;
  public hideState: boolean = false;
  public hideDistrict: boolean = true;
  stateCode;
  totalTarget: number = 0;
  totalPrimary: number = 0;
  totalSecondary: number = 0;
  targetAchivPercentage: number = 0;
  totalClossing: number = 0;
  totalachienemetPercentage: number = 0;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  constructor(
    private fb: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private _primaryAndSaleReturnService: PrimaryAndSaleReturnService,
    private exportAsService: ExportAsService,
    private _stateService: StateService


  ) {
    this.TvsAFilter = fb.group({
      state: new FormControl(null, Validators.required),
      district: new FormControl(null),
      month: new FormControl(null, Validators.required),
      year: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
    
    if (this.currentUser.userInfo[0].designationLevel >= 2) {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, "",false);
      this.hideState = true;
      this.hideDistrict = false;
      this.TvsAFilter.get('state').setValidators(null)
      this.TvsAFilter.get('district').setValidators(Validators.required)
    }
  }

  getStateValue(val) {
    this.showTable = false;
    this.showProcessing = false;
    this.TvsAFilter.patchValue({ state: val });
    this.getStateCodeForERP(val);
  }
  getDistrictValue(val) {
    this.showTable = false;
    this.showProcessing = false;
    this.TvsAFilter.patchValue({ district: val });
  }
  getMonth(val) {
    this.showTable = false;
    this.showProcessing = false;
    this.TvsAFilter.patchValue({ month: val });
  }
  getYear(val) {
    this.showTable = false;
    this.showProcessing = false;
    this.TvsAFilter.patchValue({ year: val });
  }
//---------------------Get ERP state code-------
  getStateCodeForERP(state){
      this._stateService.getStatesCode(this.currentUser.companyId,state).subscribe(res => {  
        console.log("res:  ",res);        
          this.stateCode = res[0];console.log("this.stateCode : ",this.stateCode);
        }, err => {
          console.log(err)
          alert("Oooops! \nNo Record Found");
        });
  }

  showReport() {
    this.showProcessing = true;
    let passingObjectForTargetGraphForHq = {};
    if (this.currentUser.userInfo[0].designationLevel == 0) {
      passingObjectForTargetGraphForHq = {
        companyId: this.currentUser.companyId,
        type: "headquarter",
        lookingFor: "all",
        stateId: this.TvsAFilter.value.state,
        stateCode:this.stateCode.erpStateCode,
        //userId:"1786",
        userId:this.currentUser.userInfo[0].employeeCode,
        month: this.TvsAFilter.value.month,
        year: this.TvsAFilter.value.year,
        APIEndPoint: this.currentUser.company.APIEndPoint
      };
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      passingObjectForTargetGraphForHq = {
        companyId: this.currentUser.companyId,
        type: "headquarter",
        lookingFor: "selected", 
        stateCode:this.stateCode.erpStateCode,
        userId:this.currentUser.userInfo[0].employeeCode,
        headquarterId: this.TvsAFilter.value.district,
        month: this.TvsAFilter.value.month,
        year: this.TvsAFilter.value.year,
        APIEndPoint: this.currentUser.company.APIEndPoint
      };
    }

    this._primaryAndSaleReturnService.getTargetVsAchievementDataForHq(passingObjectForTargetGraphForHq).subscribe(res => {
      this.dataSource = res;
    
      this.showProcessing = false;
      this.showTable = true;

      this.totalTarget = res.map(t => t.target).reduce((acc, value) => Math.round(parseInt(acc) + parseInt(value)).toFixed(2), 0);
      
      this.totalPrimary = res.map(t => t.primary).reduce((acc, value) => Math.round(parseInt(acc) + parseInt(value)).toFixed(2), 0);
      this.totalSecondary = res.map(t => t.secondary).reduce((acc, value) => Math.round(parseInt(acc) + parseInt(value)).toFixed(2), 0);
      this.totalachienemetPercentage = Math.round(((this.totalPrimary / this.totalTarget) * 100));
      this.totalClossing = res.map(t => t.clossing).reduce((acc, value) => Math.round(parseInt(acc) + parseInt(value)).toFixed(2), 0);

      this._changeDetectorRef.detectChanges();
    }, err => { 
      this.showProcessing = false;
      this.showTable = false;
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Data Not Found!'
      })
      this._changeDetectorRef.detectChanges();
    });

  }

  exporttoExcel() {                         
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Employee Attendance Report').subscribe(() => {
      // save started
    });
  }
}
