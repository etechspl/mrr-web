import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayStartDayEndReportComponent } from './day-start-day-end-report.component';

describe('DayStartDayEndReportComponent', () => {
  let component: DayStartDayEndReportComponent;
  let fixture: ComponentFixture<DayStartDayEndReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DayStartDayEndReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayStartDayEndReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
