import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTable, MatSlideToggleChange } from '@angular/material';
import { Designation } from '../../../_core/models/designation.model';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DivisionComponent } from '../../filters/division/division.component';
import * as _moment from 'moment';
import { LatLongService } from '../../../../../core/services/LatLong/lat-long.service';
import { ExpenseClaimedService } from '../../../../../core/services/expense-claimed.service';
declare var $;

@Component({
  selector: 'm-day-start-day-end-report',
  templateUrl: './day-start-day-end-report.component.html',
  styleUrls: ['./day-start-day-end-report.component.scss']
})
export class DayStartDayEndReportComponent implements OnInit {

  loginDetailForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(

    private _toastrService: ToastrService,
    private _fb: FormBuilder,
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _latLongService: LatLongService,
    private expenseClaimedService: ExpenseClaimedService

  ) {
    this.createForm(this._fb);
  }

  check: boolean = false;
  isStatus: boolean = true;
  showProcessing: boolean = false;
  isMultipleEmp: boolean = false;
  data: any;
  isShowReport: boolean = false;

  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild('dataTable') table1;

  dataTable: any;
  dtOptions: any;
  dtTrigger: any;
  showDivisionFilter: boolean = false;

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {

    if (this.currentUser.company.isDivisionExist == true) {

      this.loginDetailForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  createForm(_fb: FormBuilder) {

    this.loginDetailForm = _fb.group({
      designation: new FormControl('', Validators.required),
      status: new FormControl(true),
      userId: new FormControl('', Validators.required),
      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })

  }
  getDivisionValue(val) {
    this.showProcessing = false;
    this.loginDetailForm.patchValue({ division: val });
    //=======================Clearing Filter===================

    this.designationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.designationComponent.getDesignationBasedOnDivision(passingObj);
  }


  getDesignation(val) {

    this.loginDetailForm.patchValue({ designation: val.designationLevel });
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.loginDetailForm.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel] //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } 
      else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.loginDetailForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

    }
  }


  getStatusValue(val) {
    this.loginDetailForm.patchValue({ status: val });
  }

  getEmployee(emp: string) {
    this.loginDetailForm.patchValue({ userId: emp })
  }

  getFromDate(month: object) {
    this.loginDetailForm.patchValue({ month: month })
  }

  getToDate(year: number) {
    this.loginDetailForm.patchValue({ year: year })
  }
  generateReport() {
    this.isToggled = false;
    if (!this.loginDetailForm.valid) {
      this._toastrService.error("Select the required field to generate the report", "All Field required");
      return;
    }
    this.showProcessing = true;
    let params = {
      companyId: this.currentUser.companyId,
      status: this.loginDetailForm.value.status,
      userId: this.loginDetailForm.value.userId,
      designation: this.loginDetailForm.value.designation,
      month: this.loginDetailForm.value.month,
      year: this.loginDetailForm.value.year,
    }

    this._dcrReportsService.getDayPlanLoginDayEndOut(params).subscribe((res) => { 
      if (res.length > 0) {
        res.forEach((item,i,arr) => {
          let where = {
            dcrId: item.dcrId, 
            companyId: item.companyId,
            submitBy: item.userId
          }
          this.expenseClaimedService.getVisitedAreaAsPerProviderVisit(where).subscribe(visitedArea => {
            item["visitedAreaAsPerProviderVisit"] = visitedArea;
            
            if(Object.is(arr.length - 1, i)){
              this.showProcessing = false;
              this.isShowReport = true;               
      
              this.dtOptions = {
                "pagingType": 'full_numbers',
                "paging": true,
                "ordering": true,
                "info": true,
                "scrollY": 300,
                "scrollX": true,
                "fixedColumns": true,
                destroy: true,
                data: res,
               
                columns: [
                  {
                    title: 'State Name',
                    data: 'state',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: this.currentUser.company.lables.hqLabel + " Name",
                    data: 'district',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: "Name",
                    data: 'userName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  { //*******************  Preeti Arora 17-11-2020  *******************
                    title: `<table style="width: 100%;">
                    <tr>
                        <th style="width: 55%;  border-bottom: transparent;">From Area</th>
                        <th style="width: 15%;  border-bottom: transparent;">To Area</th>
                    </tr></thead></table>`,
                    data: 'visitedAreaAsPerProviderVisit',
                    render: function (data, row) {
                      if (data === '' || data == 'null' || data == undefined || data === [] ) {                
                        return '---'
                      } else {
                        let area = ``;
                        data.forEach(item => {                  
                          area = area + `
                          <tr style="border-style: hidden;background-color: transparent;" >
                          <td style="width: 30%;">${item.fromArea}</td>
                          <td style="width: 20%;">${item.toArea}</td>
                          </tr>
                         `
                        })
                        let table = `<table style="width: 100%;" > ${area}</table>`
                        return (table)
                      }
        
                    }
                  },
                  {
                    title: "DCR Submission Date",
                    data: 'dcrDate',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        if(data=="---"){
                          return '---'
                        }else{
                          return  _moment(data).format(
                            "DD-MM-YYYY"
                          );
                        }
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: "Working Status",
                    data: 'workingStatus',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: "Day Start Location",
                    data: 'dayStartLocation',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: "Day Start Time",
                    data: 'dayStartTime',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return  _moment(data).format(
                          "DD-MM-YYYY HH:MM"
                        );
                      }
                    },
                    defaultContent: '---'
                  },
                   {
                    title: "Day End Location",
                    data: 'dayEndLocation',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                          return data
                      }
                    },
                    defaultContent: '---'
                  }, {
                    title: "Day End Time",
                    data: 'dayEndTime',
                    render: function (data) {  
                      if (data === '') {
                        return '---'
                      } else {
                        if(data=="---"){
                          return '---'
                        }else{
                          return  _moment(data).format(
                            "DD-MM-YYYY HH:MM"
                          );
                        }
                      }
                    },
                    defaultContent: '---'
                  }
      
      
                ],
                language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Search Records",
                },
                dom: "Bfrtip",
                // buttons: [
                //   'copy', 'csv', 'excel', 'print'
                // ]
      
                buttons: [
                  {
                    extend: "copy",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                  {
                    extend: "csv",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                  {
                    extend: "excel",
                    exportOptions: {
                      columns: ":visible",
                    },
                  },
                  {
                    extend: "print",
                    title: function () {
                      return "State List";
                    },
                    exportOptions: {
                      columns: ":visible",
                    },
                    action: function (e, dt, node, config) {
                      // PrintTableFunction(res);
                    },
                  },
                ],
              };
              this._changeDetectorRef.detectChanges();
              this.dataTable = $(this.table1.nativeElement);
              this.dataTable.DataTable(this.dtOptions);
            }

            
          })
        });
       
            
      
          }



    })

  }


}
