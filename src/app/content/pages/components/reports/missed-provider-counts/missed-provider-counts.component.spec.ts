import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissedProviderCountsComponent } from './missed-provider-counts.component';

describe('MissedProviderCountsComponent', () => {
  let component: MissedProviderCountsComponent;
  let fixture: ComponentFixture<MissedProviderCountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissedProviderCountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissedProviderCountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
