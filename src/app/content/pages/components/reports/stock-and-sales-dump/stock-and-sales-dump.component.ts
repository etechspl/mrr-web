import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption, MatPaginator, MatSelect, MatSort } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { URLService } from '../../../../../core/services/URL/url.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import * as XLSX from 'xlsx';
import { ExportAsConfig } from 'ngx-export-as';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { ERPServiceService } from '../../../../../core/services/erpservice.service';
import * as _moment from "moment";
import { StateService } from '../../../../../core/services/state.service';
import { DistrictService } from '../../../../../core/services/district.service';
import { findIndex } from 'lodash';
@Component({
  selector: 'm-stock-and-sales-dump',
  templateUrl: './stock-and-sales-dump.component.html',
  styleUrls: ['./stock-and-sales-dump.component.scss']
})
export class StockAndSalesDumpComponent implements OnInit {

  isShowDivision = false;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  showTable = false;
  div = true;
  month = true;
  isDisabledBtn = false;
  showProcessing = false;
  allSelected: Boolean = false;
  allSelectedHq: Boolean = false;
  dataSource = [];
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  rl = this.currentUser.userInfo[0].rL;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild('paginator1') paginator1: MatPaginator;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild('selectHQ') selectHQ: MatSelect;
  @ViewChild('select') select: MatSelect;
  progressValue: number = 0;
  constructor(
    private _toastrService: ToastrService,
    private _urlService: URLService,
    private fb: FormBuilder,
    private stockiestService: StockiestService,
    private _changeDetectorRef: ChangeDetectorRef,
    private ERPServiceService: ERPServiceService,
    private _stateService: StateService,
    private _districtService: DistrictService
  ) {

  }

  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.DumpPrimaryandSecondary.setControl(
        "divisionId",
        new FormControl()
      );
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel,
      };
      this.callDivisionComponent.getDivisions(obj);
    } else {
      this.isShowDivision = false;
    }
    this.getStates();
  }

  DumpPrimaryandSecondary = new FormGroup({
    divisionId: new FormControl("", Validators.required),
    stateInfo: new FormControl("", Validators.required),
    districtInfo: new FormControl("", Validators.required),
    month: new FormControl("", Validators.required),
    year: new FormControl("", Validators.required),
  });

  getDivisionValue(val) {
    this.div = false;
    this.showTable = false;
    this.DumpPrimaryandSecondary.patchValue({ divisionId: val })
  }

  stateData: any;
  stateIdArr = [];
  getStates() {
    this._stateService.getStates(this.currentUser.companyId).subscribe(res => {
      this.stateData = res[0].stateInfo;
    })
  }
  HqData: any;
  getStateValue(val) {
    this.stateIdArr = [];
    this.DumpPrimaryandSecondary.patchValue({ stateInfo: val })
    val.forEach(element => {
      this.stateIdArr.push(element.id);
    });
    this.HqData = [];
    this._districtService.getDistrictsForTarget(this.currentUser.companyId, this.stateIdArr).subscribe(res => {
      this.HqData = res;
    })
  }

  getDistrictValue(val) {
    this.DumpPrimaryandSecondary.patchValue({ districtInfo: val })
  }


  getMonth(val) {
    this.month = false;
    this.showTable = false;
    this.DumpPrimaryandSecondary.patchValue({ month: val })

  }

  getYear(val) {
    this.showTable = false;
    this.DumpPrimaryandSecondary.patchValue({ year: val })
  }

  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  toggleAllSelectionForHQ() {
    this.allSelectedHq = !this.allSelectedHq;  // to control select-unselect
    if (this.allSelectedHq) {
      this.selectHQ.options.forEach((item: MatOption) => item.select());
    } else {
      this.selectHQ.options.forEach((item: MatOption) => { item.deselect() });
    }
  }


  // async getsssDetails11() {
  //   this.showProcessing = true;
  //   this.showTable = false;
  //   this.isDisabledBtn = true;
  //   let params = {};
  //   let fromDate = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).startOf('month').format("YYYY-MM-DD");
  //   let toDate = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).endOf('month').format("YYYY-MM-DD");
  //   let fromDatePri = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).startOf('month').format("DD-MM-YYYY");
  //   let toDatePri = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).endOf('month').format("DD-MM-YYYY");
  //   params = {
  //     divisionId: this.DumpPrimaryandSecondary.value.divisionId,
  //     companyId: this.currentUser.companyId,
  //     APIEndPoint: this.currentUser.company.APIEndPoint,
  //   }

  //   params['fromDate'] = fromDate;
  //   params['toDate'] = toDate;
  //   params['fromPrimDate'] = fromDatePri;
  //   params['toPrimDate'] = toDatePri;
  //   let selectedHqDetails = [];
  //   this.DumpPrimaryandSecondary.value.districtInfo.forEach(element => {
  //     if (element.districtName && element.erpHqCode && element._id) {
  //       selectedHqDetails.push({
  //         "districtName": element.districtName,
  //         "erpHqCode": element.erpHqCode,
  //         "districtId": element._id,
  //         "isPoolDistrict": element.isPoolDistrict
  //       });
  //     }
  //   });
  //   params['selectedHqDetails'] = selectedHqDetails;
  //   console.log(params);
  //   this.stockiestService.getSSSDump(params).subscribe(res => {
  //     if (res.length == 0 || res == undefined) {
  //       alert("No Record Found");
  //       this.isDisabledBtn = false;
  //       this.showProcessing = false;
  //     } else {
  //       this.dataSource = res;
  //       if (this.dataSource) {
  //         this.showProcessing = false;
  //         this.showTable = true;
  //         this.isDisabledBtn = false;
  //       }
  //     }
  //     this._changeDetectorRef.detectChanges();
  //   }, err => {
  //     console.log(err)
  //     alert("No Record Found");
  //     this.isDisabledBtn = false;
  //     this.showProcessing = false;
  //     this.showTable = false;
  //     this._changeDetectorRef.detectChanges();
  //   })
  // }
  workSheet;
  workBook;
  exportTable() {
    this.workBook = XLSX.utils.book_new();
    let allHqProductData = [];
    let totalTargetQty = 0, totalTargetAmt = 0.0, totalOpenQty = 0, totalOpenAmt = 0.0, totalActualPriQty = 0, totalActualPriAmt = 0.0,
      totalPriFreeQty = 0, totalPriFreeAmt = 0.0, totalSecQty = 0, totalSecAmt = 0.0, totalReturnQty = 0, totalReturnAmt = 0.0, totalReturnFreeQty = 0, totalReturnFreeAmt = 0.0,
      totalExpiryQty = 0, totalExpiryAmt = 0.0, totalBreQty = 0, totalBreAmt = 0.0, totalClosingQty = 0, totalClosingAmt = 0.0
    this.dataSource.forEach((elem) => {
      if (elem.district != "NA") {
        elem.data.forEach((x) => {
          let dataToExportAll = {
            "Headquarter": x.districtName,
            "Product Name": x.productName,
            "Product Rate": x.productRate,
            "Target (Qty)": parseFloat(x.targetObj.targetQty),
            "Opening (Qty)": parseFloat(x.sssObj.openingQty),
            "Primary Actual (Qty)": parseFloat(x.primaryObj.primaryQty),
            "Primary Free (Qty)": parseFloat(x.sssObj.primaryFreeQty),
            "Secondary (Qty)": parseFloat(x.sssObj.secondaryQty),
            "Ach % (Qty)": parseFloat(x.achObj.perAchQty),
            "Return (Qty)": parseFloat(x.returnObj.salereturnqty),
            "Return Free (Qty)": parseFloat(x.sssObj.returnFreeSale),
            "Expiry (Qty)": parseFloat(x.returnObj.expiryqty),
            "Breakage (Qty)": parseFloat(x.returnObj.breakageqty),
            "Closing (Qty)": parseFloat(x.sssObj.closingQty),
            "Target (Value)": parseFloat(x.targetObj.targetAmt),
            "Opening (Value)": parseFloat(x.sssObj.openingAmt),
            "Primary Actual (Value)": parseFloat(x.primaryObj.primaryAmt),
            "Primary Free (Value)": parseFloat(x.sssObj.primaryFreeAmt),
            "Secondary (Value)": parseFloat(x.sssObj.totalSecondrySaleAmt),
            "Ach % (Value)": parseFloat(x.achObj.PerAchAmt),
            "Return (Value)": parseFloat(x.returnObj.salereturnamount),
            "Return Free (Value)": parseFloat(x.sssObj.returnFreeAmt),
            "Expiry (Value)": parseFloat(x.returnObj.expiryamount),
            "Breakage (Value)": parseFloat(x.returnObj.breakageamount),
            "Closing Value": parseFloat(x.sssObj.closingAmt)
          }
          allHqProductData.push(dataToExportAll);
          if (x.districtName != "TOTAL") {
            totalTargetQty = totalTargetQty + parseFloat(x.targetObj.targetQty);
            totalTargetAmt = totalTargetAmt + parseFloat(x.targetObj.targetAmt);
            totalOpenQty = totalOpenQty + parseFloat(x.sssObj.openingQty);
            totalOpenAmt = totalOpenAmt + parseFloat(x.sssObj.openingAmt);
            totalActualPriQty = totalActualPriQty + parseFloat(x.primaryObj.primaryQty);
            totalActualPriAmt = totalActualPriAmt + parseFloat(x.primaryObj.primaryAmt);
            totalPriFreeQty = totalPriFreeQty + parseFloat(x.sssObj.primaryFreeQty);
            totalPriFreeAmt = totalPriFreeAmt + parseFloat(x.sssObj.primaryFreeAmt);
            totalSecQty = totalSecQty + parseFloat(x.sssObj.secondaryQty);
            totalSecAmt = totalSecAmt + parseFloat(x.sssObj.totalSecondrySaleAmt);
            totalReturnQty = totalReturnQty + parseFloat(x.returnObj.salereturnqty);
            totalReturnAmt = totalReturnAmt + parseFloat(x.returnObj.salereturnamount);
            totalReturnFreeQty = totalReturnFreeQty + parseFloat(x.sssObj.returnFreeSale);
            totalReturnFreeAmt = totalReturnFreeAmt + parseFloat(x.sssObj.returnFreeAmt);
            totalExpiryQty = totalExpiryQty + parseFloat(x.returnObj.expiryqty);
            totalExpiryAmt = totalExpiryAmt + parseFloat(x.returnObj.expiryamount);
            totalBreQty = totalBreQty + parseFloat(x.returnObj.breakageqty);
            totalBreAmt = totalBreAmt + parseFloat(x.returnObj.breakageamount);
            totalClosingQty = totalClosingQty + parseFloat(x.sssObj.closingQty);
            totalClosingAmt = totalClosingAmt + parseFloat(x.sssObj.closingAmt);
          }
        });
      }
    });
    let grandTotalObj = {
      "Headquarter": 'GRAND TOTAL',
      "Product Name": '----',
      "Product Rate": '----',
      "Target (Qty)": totalTargetQty,
      "Opening (Qty)": totalOpenQty,
      "Primary Actual (Qty)": totalActualPriQty,
      "Primary Free (Qty)": totalPriFreeQty,
      "Secondary (Qty)": totalSecQty,
      "Ach % (Qty)": '----',
      "Return (Qty)": totalReturnQty,
      "Return Free (Qty)": totalReturnFreeQty,
      "Expiry (Qty)": totalExpiryQty,
      "Breakage (Qty)": totalBreQty,
      "Closing (Qty)": totalClosingQty,
      "Target (Value)": totalTargetAmt,
      "Opening (Value)": totalOpenAmt,
      "Primary Actual (Value)": totalActualPriAmt,
      "Primary Free (Value)": totalPriFreeAmt,
      "Secondary (Value)": totalSecAmt,
      "Ach % (Value)": '----',
      "Return (Value)": totalReturnAmt,
      "Return Free (Value)": totalReturnFreeAmt,
      "Expiry (Value)": totalExpiryAmt,
      "Breakage (Value)": totalBreAmt,
      "Closing Value": totalClosingAmt
    }
    allHqProductData.push(grandTotalObj);
    this.workSheet = XLSX.utils.json_to_sheet(allHqProductData);
    XLSX.utils.book_append_sheet(this.workBook, this.workSheet, 'All Headquarters');
    this.dataSource.forEach((elem) => {
      if (elem.district != "NA") {
        const dataToExport = elem.data.map((x) => ({
          "Headquarter": x.districtName,
          "Product Name": x.productName,
          "Product Rate": x.productRate,
          "Target (Qty)": parseFloat(x.targetObj.targetQty),
          "Opening (Qty)": parseFloat(x.sssObj.openingQty),
          "Primary Actual (Qty)": parseFloat(x.primaryObj.primaryQty),
          "Primary Free (Qty)": parseFloat(x.sssObj.primaryFreeQty),
          "Secondary (Qty)": parseFloat(x.sssObj.secondaryQty),
          "Ach % (Qty)": parseFloat(x.achObj.perAchQty),
          "Return (Qty)": parseFloat(x.returnObj.salereturnqty),
          "Return Free (Qty)": parseFloat(x.sssObj.returnFreeSale),
          "Expiry (Qty)": parseFloat(x.returnObj.expiryqty),
          "Breakage (Qty)": parseFloat(x.returnObj.breakageqty),
          "Closing (Qty)": parseFloat(x.sssObj.closingQty),
          "Target (Value)": parseFloat(x.targetObj.targetAmt),
          "Opening (Value)": parseFloat(x.sssObj.openingAmt),
          "Primary Actual (Value)": parseFloat(x.primaryObj.primaryAmt),
          "Primary Free (Value)": parseFloat(x.sssObj.primaryFreeAmt),
          "Secondary (Value)": parseFloat(x.sssObj.totalSecondrySaleAmt),
          "Ach % (Value)": parseFloat(x.achObj.PerAchAmt),
          "Return (Value)": parseFloat(x.returnObj.salereturnamount),
          "Return Free (Value)": parseFloat(x.sssObj.returnFreeAmt),
          "Expiry (Value)": parseFloat(x.returnObj.expiryamount),
          "Breakage (Value)": parseFloat(x.returnObj.breakageamount),
          "Closing Value": parseFloat(x.sssObj.closingAmt)
        }));
        this.workSheet = XLSX.utils.json_to_sheet(dataToExport);
        XLSX.utils.book_append_sheet(this.workBook, this.workSheet, elem.district);
      }
    })
    XLSX.writeFile(this.workBook, "Sales Report(" + this.DumpPrimaryandSecondary.value.month + "-" + this.DumpPrimaryandSecondary.value.year + ").xlsx");
    this._changeDetectorRef.detectChanges();
  }

  async getsssDetails() {
    this.progressValue = 0;
    this.dataSource.length = 0;
    this.showTable = false;
    this.showProcessing = true;
    const states = this.DumpPrimaryandSecondary.value.stateInfo;
    const districts = this.DumpPrimaryandSecondary.value.districtInfo.filter(item => item);

    let params = {};
    let fromDate = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).startOf('month').format("YYYY-MM-DD");
    let toDate = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).endOf('month').format("YYYY-MM-DD");
    let fromDatePri = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).startOf('month').format("DD-MM-YYYY");
    let toDatePri = _moment(this.DumpPrimaryandSecondary.value.year + "-" + this.DumpPrimaryandSecondary.value.month).endOf('month').format("DD-MM-YYYY");
    params = {
      divisionId: this.DumpPrimaryandSecondary.value.divisionId,
      companyId: this.currentUser.companyId,
      APIEndPoint: this.currentUser.company.APIEndPoint,
    }

    params['fromDate'] = fromDate;
    params['toDate'] = toDate;
    params['fromPrimDate'] = fromDatePri;
    params['toPrimDate'] = toDatePri;

    for (let i = 0; i < states.length; i++) {
      const stateSpecificDistricts = districts.filter((district) => district.stateId.toString() === states[i].id.toString()).map(data => {
        const obj = {
          "districtName": data.districtName,
          "erpHqCode": data.erpHqCode,
          "districtId": data._id,
          "isPoolDistrict": data.isPoolDistrict
        }
        return obj;
      });

      params['selectedHqDetails'] = stateSpecificDistricts;
      const data = await this.getSingleStateData({ ...params });
      if (data) this.dataSource.push(...data)
      this.progressValue = Math.floor((i + 1) / states.length * 100)
      this._changeDetectorRef.detectChanges();
    }

    if (this.dataSource) {
      this.showProcessing = false;
      this.showTable = true;
      this.isDisabledBtn = false;
    }
    this._changeDetectorRef.detectChanges();
  }

  getSingleStateData(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this.stockiestService.getSSSDump(params).subscribe(res => {
        if (res) {
          resolve(res);
        }
      }, err => reject(err))
    })
  }

}
