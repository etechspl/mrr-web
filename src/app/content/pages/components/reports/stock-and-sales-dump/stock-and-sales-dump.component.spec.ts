import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAndSalesDumpComponent } from './stock-and-sales-dump.component';

describe('StockAndSalesDumpComponent', () => {
  let component: StockAndSalesDumpComponent;
  let fixture: ComponentFixture<StockAndSalesDumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockAndSalesDumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAndSalesDumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
