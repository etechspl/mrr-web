import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreavisitdetailsComponent } from './areavisitdetails.component';

describe('AreavisitdetailsComponent', () => {
  let component: AreavisitdetailsComponent;
  let fixture: ComponentFixture<AreavisitdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreavisitdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreavisitdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
