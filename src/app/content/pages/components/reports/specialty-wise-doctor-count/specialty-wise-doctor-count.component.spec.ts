import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialtyWiseDoctorCountComponent } from './specialty-wise-doctor-count.component';

describe('SpecialtyWiseDoctorCountComponent', () => {
  let component: SpecialtyWiseDoctorCountComponent;
  let fixture: ComponentFixture<SpecialtyWiseDoctorCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialtyWiseDoctorCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialtyWiseDoctorCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
