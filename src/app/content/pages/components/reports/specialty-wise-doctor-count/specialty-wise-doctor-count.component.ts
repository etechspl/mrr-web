import { Component, OnInit, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { UserCompleteDCRInfoComponent } from './../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { UserDCRDetailComponent } from '../user-dcrdetail/user-dcrdetail.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StateComponent } from '../../filters/state/state.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { MonthComponent } from '../../filters/month/month.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { Title } from '@angular/platform-browser';
import { DivisionComponent } from '../../filters/division/division.component';
import { MatSlideToggleChange } from '@angular/material';
declare var $;

export interface Special {
  value: string;
  viewValue: string;
}

export interface SpecialtyWise {
  viewValue: string;
}

@Component({
  selector: 'm-specialty-wise-doctor-count',
  templateUrl: './specialty-wise-doctor-count.component.html',
  styleUrls: ['./specialty-wise-doctor-count.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class SpecialtyWiseDoctorCountComponent implements OnInit {

  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;
  public showSpecialty: boolean = false;
  public showCategory: boolean = false;
  public showQualification: boolean = false;

  count = 0;
  //swdcForm: FormGroup;
  displayedColumns = ['companyId', 'UserName', 'UserDesignation', 'totalDCR', 'RMPCount', 'doctorPOB', 'DrugStoreCount', 'vendorPOB', 'userId'];
  dataSource;
  dataSources;
  showAdminAndMGRLevelFilter = false;
  isShowDivision = false;

  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  // @ViewChild("callUserDetailDCR") callUserDetailDCR:UserDCRDetailComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;


  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
  //dataTableForDCRDateWise: any;
  dtOptionsForDCRDateWise: any;
  data = [];
  data1: any;


  constructor(
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private _providerService: ProviderService

  ) { }
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  swdcForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null),
    stateInfo: new FormControl(null),
    companyId: new FormControl(null),
    //stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    month: new FormControl(null),
    year: new FormControl(null),
    designation: new FormControl(null),

    providerType: new FormControl(null, Validators.required),
    specialtyDatavalue: new FormControl(null),
    //category:new FormControl(null),
    //degree:new FormControl(null)
    // toDate: new FormControl(null),
    // fromDate: new FormControl(null)
  })

  check: boolean = false;

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    console.log('toggle', event.checked);
    this.isToggled = event.checked;
  }

  getReportType(val) {

    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }

  getDivisionValue($event) {

    this.swdcForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.swdcForm.value.type === "State" || this.swdcForm.value.type === "Headquarter") {


        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.swdcForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.swdcForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.swdcForm.value.divisionId != null && this.swdcForm.value.designation != null) {
          if (this.swdcForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.swdcForm.value.divisionId,
              status: [true],
              designationObject: this.swdcForm.value.designation
            })
          }
        }
      }
    }
  }

  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      this.showDistrict = false;
    //  this.callStateComponent.setBlank();
      this.swdcForm.setControl("stateInfo", new FormControl('',Validators.required))
      this.swdcForm.removeControl("employeeId");
      this.swdcForm.removeControl("designation");
    } else if (val == "Headquarter") {
      this.swdcForm.setControl("stateInfo", new FormControl('',Validators.required))
      this.swdcForm.setControl("districtInfo", new FormControl('',Validators.required))
      this.swdcForm.removeControl("employeeId");
      this.swdcForm.removeControl("designation");
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      this.swdcForm.removeControl("stateInfo");
      this.swdcForm.removeControl("districtInfo");
      this.swdcForm.setControl("designation", new FormControl('',Validators.required));
      this.swdcForm.setControl("employeeId", new FormControl('',Validators.required));
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.swdcForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }
  getStateValue(val) {
    this.swdcForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.swdcForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.swdcForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.swdcForm.value.divisionId
        })
      }
    } else {
      if (this.swdcForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }

    }
  }

  getDistrictValue(val) {
    this.swdcForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.swdcForm.patchValue({ employeeId: val });

  }


  specializations: Special[] = [
    { value: 'specialization', viewValue: 'Specialization' },
    { value: 'Category', viewValue: 'Category' },
    { value: 'Degree', viewValue: 'Degree' }
  ];
  getSpecialty(val) {
    if (val == "specialization") {
      this.data = this.currentUser.company.specialization;
      this.data1 = "Specialization Wise";
    } else if (val == "Category") {
      this.data = this.currentUser.company.category;
      this.data1 = "Category Wise";
    }
    else if (val == "Degree") {
      this.data = this.currentUser.company.degree;
      this.data1 = "Degree Wise";

    }
  }


  getDesignation(val) {
    this.swdcForm.patchValue({ designation: val });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
  }

  ngOnInit() {
    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.swdcForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }

    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }

  }
  showDCRSummary = true;

  viewRecords() {
    this.isToggled= false;
    const self = this
    this.showProcessing = true;
    this.showReport = false;

    let obj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.userId,
      Type: this.swdcForm.value.type,
      // specialization: this.swdcForm.value.specialization,
      specialtyDatavalue: this.swdcForm.value.specialtyDatavalue,
      stateInfo: this.swdcForm.value.stateInfo,
      districtInfo: this.swdcForm.value.districtInfo,
      providerType: this.swdcForm.value.providerType,
    }

    this._providerService.getSpecialityWiseDoctorCount(obj).subscribe(res => {

      this._changeDetectorRef.detectChanges();

      if (res[0].data[0].hasOwnProperty('specialization')) {
        this.dataSource = this.data1;
      } else if (res[0].data[0].hasOwnProperty('category')) {
        this.dataSource = this.data1;
      }
      // --------------nipun (10-01-2020) start ------------
      res.map((item)=>{
       item["total"] = 0;
       item.data.map(itemi=>{
        //  if(itemi.specialization === "Physician" || itemi.specialization==="Ortho"){
        //   item.total += itemi.tot;
        //  }
        //  if(itemi.specialization === "A" || itemi.specialization==="B"|| itemi.specialization==="C"){
        //   item.total += itemi.tot;
        //  }
        //  if(itemi.specialization === "MD" || itemi.specialization==="MBBS"){
        //   item.total += itemi.tot;
        //  }
        item.total += itemi.tot;
       });
      });
     // --------------nipun (10-01-2020) end ------------
      if (this.dataSource.length > 0) {
        this.showProcessing = false;
        this.showReport = true;
        if (this.dataTable) {
          this.dataTable.destroy();
          $(this.table.nativeElement).empty()
        }

        $(this.table.nativeElement).html('')
        $(this.table.nativeElement).html(`<tfoot><tr><th>Total:</th><th></th><th></th>${
          this.data.map(i => `<th></th>`).join('')
          }</tr></tfoot>`)


        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: res,
          responsive: true,
          columns: [
            {
              title: 'State',
              data: 'state',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'HeadQuater',
              data: 'district',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: "Employee",
              data: 'user',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },



            ...this.data.map(category => {
              return {
                title: category,
                data: 'data',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    const index = data.findIndex(i => i.specialization == category)
                    if (index > -1) {
                      return data[index].tot
                    }

                    return '--'
                  }
                },

                defaultContent: '---'
              }
            }),
            // --------------nipun (10-01-2020) start ------------
            {
              title:"Total",
              data: 'total',
              render: function (data) {
                if (data === 0) {
                  return 0
                } else {
                  return data
                }
              },
              defaultContent: 0
            },
            // --------------nipun (10-01-2020) end ------------

            // ...this.currentUser.company.specialization.map(specialization => {
            //   return {
            //     title: specialization,
            //     data: 'data',RSummary = true;

            //     render: function (data) {
            //       if (data === '') {
            //         return '---'
            //       } else {
            //         const index = data.findIndex(i => i.specialization == specialization)
            //         if (index > -1) {
            //           return data[index].tot
            //         }
            //         return '--'
            //       }
            //     },
            //     defaultContent: '---'
            //   }
            // }),


            //    ...this.currentUser.company.degree.map(degree=>{
            //     return {
            //       title:degree,
            //     data: 'data',
            //     render: function (data) {
            //       if (data === '') {
            //         return '---'
            //       } else {
            //         const index = data.findIndex(i=>i.specialization == degree)
            //         if(index > -1) {
            //           return data[index].tot
            //         }
            //         return '--'
            //       }
            //     },
            //     defaultContent: '---'
            //   }
            // }),

          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Records",
          },

          dom: 'Bfrtip',
          // --------------nipun (10-01-2020) start ------------
          buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
            // 'copy', 'csv', 'excel', 'print'
          ],
          // --------------nipun (10-01-2020) end ------------

          footerCallback: function (row, data, start, end, display) {
            var api = this.api();
            // Remove the formatting to get integer data for summation
            var intVal = function (i: any) {
              return typeof i === 'string' ?
                (i as any).replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                  i : 0;
            };

            const staticFied = 3
            self.data.forEach((e, index) => {
              const colIndex = staticFied + index
              var col6_total = api
                .column(colIndex)
                .data()
                .reduce(function (a, b) {

                  const key = $(api.column(colIndex).header()).text()
                  // .html()

                  const index = b.findIndex(i => i.specialization == key)
                  if (index > -1) {
                    return intVal(a) + intVal(b[index].tot);
                  } else {
                    return intVal(a)
                  }

                }, 0);
              $(api.column(colIndex).footer()).html(col6_total);

            });
            //  this._changeDetectorRef.detectChanges();

          }


        };
        this._changeDetectorRef.detectChanges();

      }

      this.dataTable = $(this.table.nativeElement).DataTable(this.dtOptions);
      this._changeDetectorRef.detectChanges();

    },

      err => {
        console.log(err);

      })

  }



}




















































































  // viewRecords() {
  //   this.showReport = false;
  //   this.showDateWiseDCRReport = false;
  //   this.showDCRSummary = true;
  //   this.showProcessing = true;
  //   if (this.swdcForm.value.employeeId == null) {
  //     this.swdcForm.value.employeeId = [this.currentUser.id];
  //   }
  //   this._dcrReportsService.getEmployeeWiseDCRDetails(this.currentUser.companyId, this.swdcForm.value.type, this.swdcForm.value.stateInfo, this.swdcForm.value.districtInfo, this.swdcForm.value.employeeId, this.swdcForm.value.fromDate, this.swdcForm.value.toDate).subscribe(res => {
  //     if (res.length == 0 || res == undefined) {
  //       this.showReport = false;
  //       this.showDateWiseDCRReport = false;
  //       this.showProcessing = false;
  //       this.toastr.info("No Record Found.")

  //     } else {
  //       this.dataSource = res;
  //       this.showProcessing = false;
  //       this.showReport = true;

  //       this.dtOptions = {
  //         "pagingType": 'full_numbers',
  //         "paging": true,
  //         "ordering": true,
  //         "info": true,
  //         "scrollY": 300,
  //         "scrollX": true,
  //         "fixedColumns": true,
  //         destroy: true,
  //         data: res,
  //         responsive: true,
  //         columns: [
  //           {
  //             title: 'Employee Name',
  //             data: 'UserName',

  //             render: function (data) {
  //               if (data === '') {
  //                 return '---'
  //               } else {
  //                 return data
  //               }
  //             },
  //             defaultContent: '---'
  //           },
  //           {
  //             title: 'Employee Designation',
  //             data: 'UserDesignation',
  //             render: function (data) {
  //               if (data === '') {
  //                 return '---'
  //               } else {
  //                 return data
  //               }
  //             },
  //             defaultContent: '---'
  //           },
  //           {
  //             title: "Total DCR",
  //             data: 'totalDCR',
  //             render: function (data) {
  //               if (data === '') {
  //                 return '---'
  //               } else {
  //                 return data
  //               }
  //             },
  //             defaultContent: '---'
  //           },
  //           {
  //             title: this.currentUser.company.lables.doctorLabel + ' Met Count',
  //             data: 'RMPCount',
  //             render: function (data) {
  //               if (data === '') {
  //                 return '---'
  //               } else {
  //                 return data
  //               }
  //             },
  //             defaultContent: '---'
  //           },
  //           {
  //             title: this.currentUser.company.lables.doctorLabel + ' POB',
  //             data: 'doctorPOB',
  //             render: function (data) {
  //               if (data === '') {
  //                 return '---'
  //               } else {
  //                 return data
  //               }
  //             },
  //             defaultContent: '---'
  //           }, {
  //             title: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel + ' Met Count',
  //             data: 'DrugStoreCount',
  //             render: function (data) {
  //               if (data === '') {
  //                 return '---'
  //               } else {
  //                 return data
  //               }
  //             },
  //             defaultContent: '---'
  //           },
  //           {
  //             title: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel + ' POB',
  //             data: 'vendorPOB',
  //             render: function (data) {
  //               if (data === '') {
  //                 return '---'
  //               } else {
  //                 return data
  //               }
  //             },
  //             defaultContent: '---'
  //           },
  //           {
  //             title: 'View TP Detail',
  //             defaultContent: "<button class='btn btn-warning's><i class='la la-eye'></i>&nbsp;&nbsp;View DCR Detail</button>"
  //           }

  //         ],
  //         rowCallback: (row: Node, data: any[] | Object, index: number) => {
  //           const self = this;
  //           // Unbind first in order to avoid any duplicate handler
  //           // (see https://github.com/l-lin/angular-datatables/issues/87)
  //           $('td', row).unbind('click');
  //           $('td button', row).bind('click', () => {
  //             //console.log("row : ", data);
  //             let rowObject = JSON.parse(JSON.stringify(data));
  //             self.getDateWise(rowObject.userId);
  //           });
  //           return row;
  //         },
  //         language: {
  //           search: "_INPUT_",
  //           searchPlaceholder: "Search Records",
  //         },
  //         dom: 'Bfrtip',
  //         buttons: [
  //           'copy', 'csv', 'excel', 'print'
  //         ]
  //       };
  //       this._changeDetectorRef.detectChanges();
  //       this.dataTable = $(this.table.nativeElement);
  //       this.dataTable.DataTable(this.dtOptions);
  //       this._changeDetectorRef.detectChanges();
  //       // this.showDateWiseDCRReport=true;

  //     }
  //     this._changeDetectorRef.detectChanges();
  //   }, err => {
  //     this.showReport = false;
  //     this.showDateWiseDCRReport = false;
  //     this.showProcessing = false;
  //     this.toastr.info("No Record Found.")
  //     this._changeDetectorRef.detectChanges();
  //   })

  // }

  // getDateWise(val) {
  //   this.getEmployeeDCRDetail(val, this.swdcForm.value.fromDate, this.swdcForm.value.toDate);
  // }

  // getEmployeeDCRDetail(userId, fromDate, toDate) {
  //   this.showDateWiseDCRReport = false;
  //   this.showDCRSummary = true;
  //   this.showProcessing = true;
  //   this._dcrReportsService.getUserDateWiseDCRDetail(userId, fromDate, toDate).subscribe(result => {

  //     if (result == undefined) {
  //       this.showDateWiseDCRReport = false;

  //       this.showProcessing = false;
  //       this.toastr.info("No Record Found.")

  //     } else {
  //       this.dataSources = result;
  //       this.showProcessing = false;
  //       this.showDateWiseDCRReport = true;
  //       // this.showDateWiseDCRReport=true;
  //       this._changeDetectorRef.detectChanges();
  //     }

  //   }, err => {
  //     console.log(err)
  //     this.showDateWiseDCRReport = false;
  //     this.showProcessing = false;
  //     this.toastr.info("No Record Found.")
  //     // alert("Oooops! \nNo Record Found");
  //     this._changeDetectorRef.detectChanges();
  //   });

  // }


  // getDCRInfo(val) {
  //   if (val != undefined) {
  //     this.showDCRSummary = false;
  //     this.callUserCompleteDCR.getUserCompleteDCRDetails(val);
  //     //s this.callUserCompleteDCR.getUserCompleteDCRDetails(val)

  //   } else {
  //     this.showDCRSummary = true;
  //   }
  //   //this.router.navigate(['/reports/usercompletedcr'],{ queryParams: { dcrId: val } });
  // }


