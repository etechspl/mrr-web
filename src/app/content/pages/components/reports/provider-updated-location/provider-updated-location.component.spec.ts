import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderUpdatedLocationComponent } from './provider-updated-location.component';

describe('ProviderUpdatedLocationComponent', () => {
  let component: ProviderUpdatedLocationComponent;
  let fixture: ComponentFixture<ProviderUpdatedLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderUpdatedLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderUpdatedLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
