import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'm-provider-updated-location',
  templateUrl: './provider-updated-location.component.html',
  styleUrls: ['./provider-updated-location.component.scss']
})
export class ProviderUpdatedLocationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
