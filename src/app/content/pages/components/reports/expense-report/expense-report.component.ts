import { DivisionComponent } from './../../filters/division/division.component';
import { StateComponent } from './../../filters/state/state.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ExpenseClaimedService } from '../../../../../core/services/expense-claimed.service';
import * as _moment from 'moment';
declare var $;
import { ToastrService } from 'ngx-toastr';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DailyallowanceService } from '../../../_core/services/dailyallowance.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { hasOwnProperty } from 'tslint/lib/utils';


@Component({
  selector: 'm-expense-report',
  templateUrl: './expense-report.component.html',
  styleUrls: ['./expense-report.component.scss']
})
export class ExpenseReportComponent implements OnInit {

  @ViewChild("dataTable") table: ElementRef;
  dataTable: any;
  dtOptions: any;

  dataTableFixed: any;
  dtOptionsFixed: any;

  approveByAdmin = "";
  approveByMgr = "";
  constructor(private fb: FormBuilder,
    private expenseClaimedService: ExpenseClaimedService,
    private userDetailsService: UserDetailsService,
    private changeDetectorRef: ChangeDetectorRef,
    private dailyallowanceService: DailyallowanceService,
    private _toastr: ToastrService) { }
  expenseFilterForm: FormGroup;
  check: boolean = false;
  isShowDivision = false;
  color: string = "";
  dataSource: any;
  dataSourceFixed: any;

  isShowReport: boolean = false;
  isLoading: boolean = false;
  //@ViewChild("MatTable") table: MatTable<any>;
  //@ViewChild(MatSort) sort: MatSort;
  //@ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild("employeeComponent") employeeComponent: EmployeeComponent;
  @ViewChild("districtComponent") districtComponent: DistrictComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  displayedColumns = ['dcrDate', 'plannedBlock', 'visitedBlock', 'type', 'dailyAllownce', 'dailyAllownceMgr', 'dailyAllownceAdm', 'distance', 'fare', 'miscExpense', 'totalExpense', 'remarks', 'doctorCalls', 'chemistCalls', 'jointWork'];
  currentUser = JSON.parse(sessionStorage.currentUser);


  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.currentUser);
    this.COMPANY_ID = this.currentUser.companyId;
    this.DA_TYPE = this.currentUser.company.expense.daType;
    this.IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
    this.createExpenseFilterForm();
    
    if (this.IS_DIVISION_EXIST === true) {
      this.isShowDivision = true;
      this.expenseFilterForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);



    } else {
      this.isShowDivision = false;
    }

  }

  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  COMPANY_ID = this.currentUser.companyId;
  DA_CATEGORY = this.currentUser.userInfo[0].daCategory;
  DA_TYPE = this.currentUser.company.expense.daType;
  DIVISION_ID = this.currentUser.userInfo[0].divisionId[0];
 
  createExpenseFilterForm(){
    this.expenseFilterForm = this.fb.group({
      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })
    if (this.currentUser.userInfo[0].rL === 0) {
      this.expenseFilterForm.addControl('stateId', new FormControl('', Validators.required));
      this.expenseFilterForm.addControl('districtId', new FormControl('', Validators.required));
      this.expenseFilterForm.addControl('userId', new FormControl('', Validators.required));
    } else if (this.currentUser.userInfo[0].rL > 1) {
      this.expenseFilterForm.addControl('type', new FormControl('', Validators.required));
    }
  }
  

  getType($event: any) {
    if (this.currentUser.userInfo[0].rL > 1) {
      if ($event.value === 'Team') {
        this.expenseFilterForm.addControl('stateId', new FormControl('', Validators.required));
        this.expenseFilterForm.addControl('districtId', new FormControl('', Validators.required));
        this.expenseFilterForm.addControl('userId', new FormControl('', Validators.required));
      } else {
        this.expenseFilterForm.removeControl('stateId');
        this.expenseFilterForm.removeControl('districtId');
        this.expenseFilterForm.removeControl('userId');
      }
    }

  }


  getDivisionValue($event) {
    
    this.expenseFilterForm.patchValue({ divisionId: $event });
    //-------Clearing Filter--------------
    this.callStateComponent.setBlank();
    this.districtComponent.setBlank();
    //--------------------END-----------------------------------------
    if (this.currentUser.userInfo[0].designationLevel == 0) {

      let divisionIds = {
        division: this.expenseFilterForm.value.divisionId,
      };
      this.callStateComponent.getStateBasedOnDivision(divisionIds);


    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        isDivisionExist: true,
        division: this.expenseFilterForm.value.divisionId,
        supervisorId: this.currentUser.id,
      }
      this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)
    }

  }


  getStateValue(stateId: string) {
    this.expenseFilterForm.patchValue({ stateId: stateId, districtId: '', userId: '' });
    this.districtComponent.getDistricts(this.currentUser.companyId, stateId, true);
  }
  //get District Value
  getDistrictValue(districtId: string) {
    this.expenseFilterForm.patchValue({ districtId: districtId, userId: '' });
    let obj = {
      companyId: this.currentUser.companyId,
      isDivisionExist: true,
      division: this.expenseFilterForm.value.divisionId,
      stateId: this.expenseFilterForm.value.stateId,
      districtId: this.expenseFilterForm.value.districtId
    }
    
    this.employeeComponent.getEmployeeListWithMgr(obj);
  }
  getUserId(userId: string) {
    this.expenseFilterForm.patchValue({ userId: userId })
  }

  getMonthValue(month: number) {
    this.expenseFilterForm.patchValue({ month: month })
  }

  getYearValue(yearValue: number) {
    this.expenseFilterForm.patchValue({ year: yearValue })
  }

  total:number=0;
  generateReport() {
	  	  this.total=0;
    if (this.expenseFilterForm.valid) {
      let month = this.expenseFilterForm.value.month;

      if (month < 10) {
        month = "0" + month
      }
      let date = this.expenseFilterForm.value.year + '-' + month + '-01';

      let startDate = _moment.utc(date).startOf('month').format('YYYY-MM-DDT00:00:00.000Z');
      let endDate = _moment.utc(date).endOf('month').format('YYYY-MM-DDT00:00:00.000Z');

      let obj = {
        companyId: this.currentUser.companyId,
        startDate: startDate,
        endDate: endDate
      }



      if (this.currentUser.userInfo[0].rL === 1) {
        obj["userId"] = this.currentUser.id
      } else if (this.currentUser.userInfo[0].rL > 1) {
        if (this.expenseFilterForm.value.type === 'Self') {
          obj["userId"] = this.currentUser.id
        } else {
          obj["userId"] = this.expenseFilterForm.value.userId
        }
      } else {
        obj["userId"] = this.expenseFilterForm.value.userId
      }

      
      this.expenseClaimedService.getExpenseDetails(obj).subscribe(res => {

        let array = res.map(item => {
          //if item modified by both mgr & admin then different color
          if (item.hasOwnProperty("modifiedByMgr") === true && item.modifiedByMgr === true && item.hasOwnProperty("modifiedByAdm") === true && item.modifiedByAdm === true) {
            this.color = "4eabf7";
          } else if (item.hasOwnProperty("modifiedByMgr") === true && item.modifiedByMgr) {
            this.color = "b959b9";
          } else if (item.hasOwnProperty("modifiedByAdm") === true && item.modifiedByAdm === true) {
            this.color = "cc6039";
          }
          // by nipun => for now (temporarily) we are calculating total expense at frontend 03-JUN--2020
          // this.total = this.total+=item.totalExpense
          this.total+=(item.da + item.fare + item.miscExpense)
          // by nipun  03-JUN--2020 END

          return {
            color: this.color,
            ...item
          };
        });
        
        if (array.length > 0) {
          this.dataSource = JSON.parse(JSON.stringify(array));
          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "destroy": true,
            "scrollY": 400,
            "scrollX": true,
            "scrollCollapse": true,
            fixedColumns: {
              leftColumns: 2
            },


            dom: 'Bfrtip',
            buttons: [
              'excel'
            ],
            data: this.dataSource,
            columns: [
              {
                title: 'Date',
                data: 'dcrDate',
                render: function (data) {
                  return _moment(data).format('DD-MM-YYYY');
                }
              },
              {
                title: "Planned Area ---- Planned To",
                data: 'visitedBlock',
                render: function (data) {
                  if (data === ''|| data.length === 0) {
                    return '---'
                  } else {
                    let info = [];
                    for (var i = 0; i < data.length; i++) {
                      info.push(
                        (data[i]["from"]) && (data[i]["to"])? data[i]["from"]+' --- '+data[i]["to"] :'----'  + "<br>");
                    }
                    let dd = info.toString().replace(/,/g, '');
                    return dd;
                  }
                },
                defaultContent: '---'

              },
              {
                title: "Visited Area ---- Visited To",
                data: 'visitedBlock',
                render: function (data) {
                  if (data === '' || data.length === 0) {
                    return '---'
                  } else {
                    let info = [];
                    for (var i = 0; i < data.length; i++) {
                      info.push(
                        (data[i]["from"]) && (data[i]["to"]) ? data[i]["from"]+' --- '+data[i]["to"] :'----'  + "<br/>");
                    }
                    let dd = info.toString().replace(/,/g, '');
                    return dd;
                  }
                },
                defaultContent: '---'

              },
              {
                title: 'Type',
                data: 'type',
                render: function(data){
                  if(data === ''||data === undefined){
                    return '---'
                  }else{
                    return data
                  }
                }
              },
              {
                title: 'Distance',
                data: 'distance'
              },
              {
                title: 'DA',
                data: 'da'
              },
              {
                title: 'DA Approved By Mgr',
                data: 'daMgr',
                visible: false,
                render: function (data) {
                  return data;
                }
              },
              {
                title: 'DA Approved By Admin',
                visible: false,
                data: 'daAdmin'
              },
              {
                title: 'Fare',
                data: 'fare'
              },
              {
                title: 'Fare Approved By Mgr',
                visible: false,
                data: 'fareMgr'
              },
              {
                title: 'Fare Approved By Admin',
                visible: false,
                data: 'fareAdmin'
              },
              {
                title: 'Misc Expense',
                data: 'miscExpense'
              },
              {
                title: 'Misc Expense Approved By Mgr',
                visible: false,
                data: 'miscExpenseMgr'
              },
              {
                title: 'Misc Expense Approved By Admin',
                visible: false,
                data: 'miscExpenseAdmin'
              },

              // by nipun => for now (temporarily) we are calculating total expense at frontend 03-JUN--2020
              {
                title: 'Total Expense',
                // data: 'totalExpense'
                data: {da: 'da', fare: 'fare', miscExpense: 'miscExpense'},
                render: (data)=>{
                  if(data){
                    return (data.da + data.fare + data.miscExpense)
                  } else return '--';
                }
              },
              {
                title: 'Total Expense Approved By Mgr',
                visible: false,
                // data: 'totalExpenseMgr'
                data: {daMgr: 'daMgr', fareMgr: 'fareMgr', miscExpenseMgr: 'miscExpenseMgr'},
                render: (data)=>{
                  if(data){
                    return (data.daMgr + data.fareMgr + data.miscExpenseMgr)
                  } else return '--';
                }
              },
              {
                title: 'Total Expense Approved By Admin',
                visible: false,
                // data: 'totalExpenseAdmin'
                data: {da: 'daAdmin', fare: 'fareAdmin', miscExpense: 'miscExpenseAdmin'},
                render: (data)=>{
                  if(data){
                    return (data.daAdmin + data.fareAdmin + data.miscExpenseAdmin)
                  } else return '--';
                }
              },
              // by nipun 03-JUN--2020  END
              {
                title: 'Remarks',
                data: 'remarks',
                render: function (data) {
                  if (data === undefined || data === '') {
                    return "---";
                  } else {
                    return data;
                  }
                }
              },
              {
                title: 'Remarks By Manager',
                visible: false,
                data: 'remarksMgr',
                render: function (data) {
                  if (data === undefined || data === '') {
                    return "---";
                  } else {
                    return data;
                  }
                }
              },
              {
                title: 'Remarks By Admin',
                visible: false,
                data: 'remarksAdmin',
                render: function (data) {
                  if (data === undefined || data === '') {
                    return "---";
                  } else {
                    return data;
                  }
                }
              },
              /*{
                title:'Doctor Calls',
                data:'doctorCalls'
              },
              {
                title:'Chemist Calls',
                data:'chemistCalls'
              },*/
              {
                title: 'Joint Work',
                data: 'jointWork',
                render: function (data) {
                  if (data === undefined || data === '') {
                    return "---";
                  } else {
                    return data;
                  }
                }
              }
            ],

          }




          //checking if expense approved by admin then all column should render
          if (this.dataSource[0].hasOwnProperty("appByMgr") === true && this.dataSource[0].appByMgr.length > 0 && this.dataSource[0].hasOwnProperty("appByAdm") === true && this.dataSource[0].appByAdm.length > 0) {
            this.approveByAdmin = "Yes";
            this.approveByMgr = "Yes"

            //showing daMgr & daAdmin
            this.dtOptions.columns[6].visible = true;
            this.dtOptions.columns[7].visible = true;

            // showing fareMgr & fareAdmin
            this.dtOptions.columns[9].visible = true;
            this.dtOptions.columns[10].visible = true;

            // showing misExpenseMgr & misExpenseAdmin
            this.dtOptions.columns[12].visible = true;
            this.dtOptions.columns[13].visible = true;

            // showing totalExpenseMgr & totalExpenseAdmin
            this.dtOptions.columns[15].visible = true;
            this.dtOptions.columns[16].visible = true;

            // showing remarksMgr & remarksAdmin
            this.dtOptions.columns[18].visible = true;
            this.dtOptions.columns[19].visible = true;
          } else if (this.dataSource[0].hasOwnProperty("appByAdm") === true && this.dataSource[0].appByAdm.length > 0) {
            this.approveByAdmin = "Yes";
            this.approveByMgr = "No"
            //showing daMgr & daAdmin
            this.dtOptions.columns[6].visible = true;
            this.dtOptions.columns[7].visible = true;

            // showing fareMgr & fareAdmin
            this.dtOptions.columns[9].visible = true;
            this.dtOptions.columns[10].visible = true;

            // showing misExpenseMgr & misExpenseAdmin
            this.dtOptions.columns[12].visible = true;
            this.dtOptions.columns[13].visible = true;

            // showing totalExpenseMgr & totalExpenseAdmin
            this.dtOptions.columns[15].visible = true;
            this.dtOptions.columns[16].visible = true;

            // showing remarksMgr & remarksAdmin
            this.dtOptions.columns[18].visible = true;
            this.dtOptions.columns[19].visible = true;

          } else if (this.dataSource[0].hasOwnProperty("appByMgr") === true && this.dataSource[0].appByMgr.length > 0) {
            this.approveByAdmin = "No";
            this.approveByMgr = "Yes"
            //showing daMgr & daAdmin
            this.dtOptions.columns[6].visible = true;
            //this.dtOptions.columns[7].visible = true;

            // showing fareMgr & fareAdmin
            this.dtOptions.columns[9].visible = true;
            //this.dtOptions.columns[10].visible = true;

            // showing misExpenseMgr & misExpenseAdmin
            this.dtOptions.columns[12].visible = true;
            //this.dtOptions.columns[13].visible = true;

            // showing totalExpenseMgr & totalExpenseAdmin
            this.dtOptions.columns[15].visible = true;
            //this.dtOptions.columns[16].visible = true;

            // showing remarksMgr & remarksAdmin
            this.dtOptions.columns[18].visible = true;
            //this.dtOptions.columns[19].visible = true;
          } else {
            this.approveByAdmin = "No";
            this.approveByMgr = "No"
          }
          if(obj["userId"] === this.expenseFilterForm.value.userId){
            this.getFixedExpenses(this.expenseFilterForm.value.userId);
          } else {
            this.getFixedExpenses();
          }

          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          this.changeDetectorRef.detectChanges();

          //new MatTableDataSource(array);
          //this.dataSource.sort = this.sort;
          //this.dataSource.paginator = this.paginator;
          this.isShowReport = true;
        } else {
          this._toastr.info("Expense is not submitted for the selected  month & year", "Expense not Submitted");
          this.isShowReport = false;
        }
        this.changeDetectorRef.detectChanges();

      })
    }
  }

  getFixedExpenses(userId?) {

    let param = {};
    if (this.IS_DIVISION_EXIST === true) {
      param["divisionId"] = this.DIVISION_ID
    }
    if(userId){
      param = {
        companyId: this.COMPANY_ID,
        type: this.DA_TYPE,
        isDivisionExist: this.IS_DIVISION_EXIST
      };
      this.userDetailsService.getEmployee(this.currentUser.companyId, userId).subscribe(user => {
          param['designation'] = user[0].designation;
          param['category'] = user[0].daCategory;
        
        this.dailyallowanceService.getFixedExpense(param).subscribe(res => {
          // console.log("expense response",res)
            let array = res.map(item => {
              return {
                ...item
              };
            })
      
            if (array.length > 0) {
              this.dataSourceFixed = JSON.parse(JSON.stringify(array));
              if(this.dataSourceFixed[0].hasOwnProperty("mobileAllowance")===true){
                this.total+=this.dataSourceFixed[0].mobileAllowance
              }
              if(this.dataSourceFixed[0].hasOwnProperty("netAllowance")===true){
                this.total+=this.dataSourceFixed[0].netAllowance
              }
              this.changeDetectorRef.detectChanges();
            }
          })
      });
      
    } else{
      param = {
        companyId: this.COMPANY_ID,
        type: this.DA_TYPE,
        category: this.DA_CATEGORY,
        isDivisionExist: this.IS_DIVISION_EXIST
      };
      this.dailyallowanceService.getFixedExpense(param).subscribe(res => {
        // console.log("expense response",res)
          let array = res.map(item => {
            return {
              ...item
            };
          })
    
          if (array.length > 0) {
            this.dataSourceFixed = JSON.parse(JSON.stringify(array));
            if(this.dataSourceFixed[0].hasOwnProperty("mobileAllowance")===true){
              this.total+=this.dataSourceFixed[0].mobileAllowance
            }
            if(this.dataSourceFixed[0].hasOwnProperty("netAllowance")===true){
              this.total+=this.dataSourceFixed[0].netAllowance
            }
            this.changeDetectorRef.detectChanges();
          }
        })
    }
  }

  // getFixedExpenses() {
  //   //getting fixed expense
  //   let param = {
  //     companyId: this.COMPANY_ID,
  //     type: this.DA_TYPE,
  //     category: this.DA_CATEGORY,
  //     isDivisionExist: this.IS_DIVISION_EXIST
  //   };

  //   if (this.IS_DIVISION_EXIST === true) {

  //     param["divisionId"] = this.DIVISION_ID
  //   }

  //   this.dailyallowanceService.getFixedExpense(param).subscribe(res => {
	// 	// console.log("expense response",res)
  //     let array = res.map(item => {
  //       return {
  //         ...item
  //       };
  //     })

  //     if (array.length > 0) {
  //       this.dataSourceFixed = JSON.parse(JSON.stringify(array));
  //       if(this.dataSourceFixed[0].hasOwnProperty("mobileAllowance")===true){
  //         this.total+=this.dataSourceFixed[0].mobileAllowance
  //       }
  //       if(this.dataSourceFixed[0].hasOwnProperty("netAllowance")===true){
  //         this.total+=this.dataSourceFixed[0].netAllowance
  //       }
  //       this.changeDetectorRef.detectChanges();
  //     }
  //   })
  // }
}
