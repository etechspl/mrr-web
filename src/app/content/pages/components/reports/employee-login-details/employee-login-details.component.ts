import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StateComponent } from '../../filters/state/state.component';
import { NativeDateAdapter, DateAdapter, MatDateFormats, MAT_DATE_FORMATS, MatStepper, MatSlideToggleChange, MatPaginator, MatTableDataSource, MatSort } from "@angular/material";
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import * as _moment from 'moment';
import { UtilsService, isString } from '../../../../../core/services/utils.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import * as XLSX from 'xlsx';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'm-employee-login-details',
  templateUrl: './employee-login-details.component.html',
  styleUrls: ['./employee-login-details.component.scss']
})
export class EmployeeLoginDetailsComponent implements OnInit {
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('tableData') tableRef: ElementRef;
  @ViewChild('sort') sort: MatSort;

  currentMonth = new Date().getMonth();
	currentYear = new Date().getFullYear();
  lastDay = new Date(this.currentYear, this.currentMonth, 0).getDate();
  @ViewChild('paginator') paginator: MatPaginator;

  actualMonth = this.currentMonth + 1;

  currentMonthStartDate = this.currentYear + "-" + this.actualMonth + "-01";
	currentMonthLastDate =
		this.currentYear + "-" + this.actualMonth + "-" + this.lastDay;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  showProcessing: boolean = false;
  showReportType: boolean = true;
  showType: boolean = true;
  showState: boolean = false;
  showDistrict: boolean = false;
  showReport: boolean = false;
  showEmployee: boolean = false;
  showDesignation: boolean = false;
  showDivisionFilter: boolean = false;
  dataSource: MatTableDataSource<any>; 
   displayedColumns: any;

  isShowTeamInfo: boolean = false;
  dynamicTableHeader: any = [];
  currentDate = new Date();
  public showDistrictWise: boolean = false;
  public showUserWise: boolean = false;
  constructor(private utilService: UtilsService,   
    private toastr: ToastrService
,    private _DCRReportService: DcrReportsService, private _ChangeDetectorRef: ChangeDetectorRef) { }
  ngOnInit() {
  
    let date=_moment(new Date(this.currentDate)).format("YYYY-MM-DD");
		let whereObj={
			
			companyId:this.currentUser.companyId,
			dcrDate:date,
			workingStatus:{neq:["Leave","Holiday"]}
			
		}
     this._DCRReportService.getAllEmployeeCallLoginDetails(whereObj).subscribe(res=>{
       if(res.length>0){
        this.showUserWise = true;   
        this.displayedColumns = ['sn','username','districtName', 'totalWorkingDay'];
        this._ChangeDetectorRef.detectChanges();
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 200);
        this._ChangeDetectorRef.detectChanges();
       }else{
        this.toastr.info("No Record Found.");
        this.showProcessing = false;
        this.showUserWise = false;     
      
      }

     })
  }
  applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase();
		this.dataSource.filter = filterValue
	  }

}
