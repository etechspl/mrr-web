import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleDistributedIssuedBalanceDetailsComponent } from './sample-distributed-issued-balance-details.component';

describe('SampleDistributedIssuedBalanceDetailsComponent', () => {
  let component: SampleDistributedIssuedBalanceDetailsComponent;
  let fixture: ComponentFixture<SampleDistributedIssuedBalanceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleDistributedIssuedBalanceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleDistributedIssuedBalanceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
