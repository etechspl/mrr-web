import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSlideToggleChange, MatTableDataSource, matTabsAnimations } from '@angular/material';
import { values } from 'lodash';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { DcrProviderVisitDetailsService } from '../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';

@Component({
  selector: 'm-sample-distributed-issued-balance-details',
  templateUrl: './sample-distributed-issued-balance-details.component.html',
  styleUrls: ['./sample-distributed-issued-balance-details.component.scss']
})
export class SampleDistributedIssuedBalanceDetailsComponent implements OnInit {

  isToggled = true;
  isShowIssueSample: boolean = false;
  designationData: any[];

  currentUser = JSON.parse(sessionStorage.currentUser);

  //


  displayedColumns = ['username', 'district', 'distributed', 'receipt', 'balance'];


  resultData: MatTableDataSource<any>;

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('paginator') paginator: MatPaginator;



  constructor(
    private fb: FormBuilder,
    private userDetailsService: UserDetailsService,
    private exportAsService: ExportAsService,
    private _dcrProviderVisitDetailsService: DcrProviderVisitDetailsService,
  ) { }

  ngAfterViewInit() {
    this.resultData.paginator = this.paginator;
  }

  ngOnInit() {
    this.resultData.paginator = this.paginator;
    console.log(this.currentUser);
  }

  exportAsConfig: ExportAsConfig = {
    type: "xlsx", // the type you want to download
    elementId: "tableId1", // the id of html/table element
  };


  editSampleAndGift = new FormGroup({
    stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null),
    employeeInfo: new FormControl(null),
    designation: new FormControl(null),
    moduleTypeName: new FormControl(null, Validators.required),
    toDate: new FormControl(null, Validators.required),
    fromDate: new FormControl(null, Validators.required),
  })



  toggleFilter(val: MatSlideToggleChange) {
    this.isToggled = val.checked;
  }

  exportToExcel() {
    this.exportAsService
      .save(this.exportAsConfig, "Sample Details")
      .subscribe(() => {
        // save started
      });
  }


  viewSampleAndGiftDetail() {
    this.isToggled = false;
    let obj = {
      companyId: this.currentUser.companyId,
      type: this.editSampleAndGift.value.moduleTypeName,
      fromDate: this.editSampleAndGift.value.fromDate,
      toDate: this.editSampleAndGift.value.toDate,
      userId: this.editSampleAndGift.value.employeeInfo,
    }

    this._dcrProviderVisitDetailsService.getSampleIssueDistributedBalanceDetails(obj).subscribe(res => {

      console.log(res);
      this.resultData = new MatTableDataSource(res);
      console.log('Table DATA->', this.resultData);
      setTimeout(() => {
        this.resultData.paginator = this.paginator;
      }, 200);
    });
    this.isShowIssueSample = true;

  }

  getStateValue(val) {
    let state = [];
    state.push(val);
    this.editSampleAndGift.patchValue({ stateInfo: state });
    this.designationData = [];
    this.userDetailsService.getDesignationValue(this.currentUser.companyId, val, this.editSampleAndGift.value.districtInfo, 'Employeewise').subscribe(res => {
      this.designationData = res;
    });
  }

  getEmployeeValue(val) {
    let district = [];
    this.callEmployeeComponent.getMgrList(this.currentUser.companyId, this.editSampleAndGift.value.stateInfo, val, [true]);
    this.editSampleAndGift.patchValue({ designation: val });
    for (let i = 0; i < val.length; i++) {
      district.push(val[i]);
    }
    this.editSampleAndGift.patchValue({ districtInfo: district });
  }

  setEmployeeDetail(val) {
    this.editSampleAndGift.patchValue({ employeeInfo: val });
  }

  moduleSubmitType(val) {
    this.editSampleAndGift.patchValue({ moduleTypeName: val })
  }

  getFromDate(event) {
    this.editSampleAndGift.patchValue({ fromDate: event });
  }

  getToDate(event) {
    this.editSampleAndGift.patchValue({ toDate: event });
  }

}
