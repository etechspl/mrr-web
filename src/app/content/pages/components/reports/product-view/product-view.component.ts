import { Component, OnInit, NgModule, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatTableDataSource, MatSlideToggleChange } from '@angular/material';
import { URLService } from '../../../../../core/services/URL/url.service';
 import { EditProductComponent } from '../edit-product/edit-product.component';
import { viewEngine_ChangeDetectorRef_interface } from '@angular/core/src/render3/view_ref';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import * as XLSX from 'xlsx';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';

declare var $;
@Component({
  selector: 'm-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {

  showProductDetails: boolean = false;
  displayedColumns = ['edit', 'delete', 'prdsn', 'state', 'productName', 'code', 'type', 'shortName', 'packageSize', 'samplePackageSize', 'ptr', 'ptw', 'mrp', 'sampleRate', 'includeRate'];
  displayedColumns2 = ['edit', 'delete', 'prdsn', 'state', 'division', 'productName', 'code', 'type', 'shortName', 'packageSize', 'samplePackageSize', 'ptr', 'ptw', 'mrp', 'sampleRate', 'includeRate'];
  public showProcessing: boolean = false;



  dataSource: MatTableDataSource<any>;

  constructor(urlService: URLService, private _productService: ProductService,
    public _toasterService: ToastrService, public dialog: MatDialog, private _changeDetectorRef: ChangeDetectorRef,
    private exportAsService: ExportAsService
  ) {
  }

  currentUser = JSON.parse(sessionStorage.currentUser);
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  DA_TYPE = this.currentUser.company.expense.daType;
  IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  isShowDivision = false;
  @ViewChild('TABLE') TABLE: ElementRef;

  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;




  exportAsConfig2: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element

  }

  public showDivisionFilter: boolean = false;
  productForm = new FormGroup({
    stateInfo: new FormControl(null, Validators.required),
    divisionId: new FormControl(null)

  })
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.isShowDivision = true;
      this.productForm.setControl('divisionId', new FormControl(null, Validators.required))
    }
    let filter = {
      where: {
        companyId: this.currentUser.company.id,
        status:true
      }

    }
    this._productService.getAllProducts(filter).subscribe(productResponse => {
      this.dataSource = productResponse;
      this.dataSource = new MatTableDataSource(productResponse);
      if (productResponse.length > 0) {
        this.showProcessing = false;
        this.showProductDetails = true;
      } else {
        this.showProcessing = false;
        this.showProductDetails = false;
        this._toasterService.error('Data not available');
      }
      this._changeDetectorRef.detectChanges();

    })

  }

  getStateValue(val) {
    this.productForm.patchValue({ stateInfo: val });
  }
  getDivisionValue($event) {
    this.productForm.patchValue({ divisionId: $event });
    if (this.IS_DIVISION_EXIST === true) {
      this.callStateComponent.getStateBasedOnDivision({
        companyId: this.currentUser.companyId,
        isDivisionExist: this.IS_DIVISION_EXIST,
        division: this.productForm.value.divisionId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      })


      //getting employee if we change the division filter after select the designation
      if (this.productForm.value.divisionId != null && this.productForm.value.designation != null) {
        if (this.productForm.value.designation.length > 0) {
          this.callEmployeeComponent.getEmployeeListBasedOnDivision({
            companyId: this.companyId,
            division: this.productForm.value.divisionId,
            status: [true],
            designationObject: this.productForm.value.designation
          })
        }
      }

    }
  }
  exportToExcel() {
    if (this.currentUser.company.isDivisionExist == true) {

      let lastCol = this.displayedColumns.splice(13, 2);
      setTimeout(() => {
        this.exportAsService.save(this.exportAsConfig2, 'Product Details').subscribe(() => {
          // save started

          this.displayedColumns = this.displayedColumns.concat(lastCol)

        });

      }, 300);
    }
    else {

      let lastCol = this.displayedColumns.splice(13, 2);
      setTimeout(() => {
        this.exportAsService.save(this.exportAsConfig2, 'Product Details').subscribe(() => {
          this.displayedColumns = this.displayedColumns.concat(lastCol)
        });

      }, 300);
    }


  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue
  }

  getProducts() {
    this.isToggled = false;
    this.showProcessing = true;
    const users = [];
    if (this.productForm.valid) {
      this._productService.getProducts(this.productForm.value, this.currentUser.company.id).subscribe(productResponse => {
        this.dataSource = productResponse;
        this.dataSource = new MatTableDataSource(productResponse);
        if (productResponse.length > 0) {
          this.showProcessing = false;
          this.showProductDetails = true;
        } else {
          this.showProcessing = false;
          this.showProductDetails = false;
          this._toasterService.error('Data not available');
        }
        this._changeDetectorRef.detectChanges();
      }, err => {
        console.log(err)
        this.showProductDetails = false;
      })
    }
  }


  //--------------------------------------------
  deleteProducts(delValue) {
    
    let productId = delValue.id;
    this._productService.deleteProduct(delValue, this.currentUser.company.id).subscribe(productDelResponse => {
      this.getProducts();
      this._toasterService.success('Product Deleted Successfully');
      this.showProductDetails = true;
    }, err => {
      console.log(err)
      this.showProductDetails = false;
    })
  }
  //------------------------------------------
  //-------------edit product by preeti arora-------------------
  editProducts(EditDataValues) {
    const dialogRef = this.dialog.open(EditProductComponent, {
      data: { EditDataValues }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getProducts();


    }) 
  }
  //------------------------------------------
  ngOnChanges(){
    console.log("chal rha h");
    

  }

}
