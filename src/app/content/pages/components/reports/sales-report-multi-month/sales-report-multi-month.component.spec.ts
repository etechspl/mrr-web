import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesReportMultiMonthComponent } from './sales-report-multi-month.component';

describe('SalesReportMultiMonthComponent', () => {
  let component: SalesReportMultiMonthComponent;
  let fixture: ComponentFixture<SalesReportMultiMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesReportMultiMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesReportMultiMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
