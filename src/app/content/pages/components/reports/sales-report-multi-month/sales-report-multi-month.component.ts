import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption, MatPaginator, MatSelect, MatSort } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { URLService } from '../../../../../core/services/URL/url.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import * as XLSX from 'xlsx';
import { ExportAsConfig } from 'ngx-export-as';
import { StockiestService } from '../../../../../core/services/Stockiest/stockiest.service';
import { ERPServiceService } from '../../../../../core/services/erpservice.service';
import * as _moment from "moment";
import { StateService } from '../../../../../core/services/state.service';
import { DistrictService } from '../../../../../core/services/district.service';
import { findIndex } from 'lodash';
//import * as fs from 'file-saver';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';

@Component({
  selector: 'm-sales-report-multi-month',
  templateUrl: './sales-report-multi-month.component.html',
  styleUrls: ['./sales-report-multi-month.component.scss']
})
export class SalesReportMultiMonthComponent implements OnInit {

  isShowDivision = false;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  showTable = false;
  div = true;
  month = true;
  isDisabledBtn = false;
  showProcessing = false;
  allSelected: Boolean = false;
  allSelectedHq: Boolean = false;
  dataSource = [];
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  rl = this.currentUser.userInfo[0].rL;
  showDownloadProcessing: boolean = false;
  monthYearArray: any;
  isButtonDisabled: boolean = false;
  isShowDiv: boolean = false;
  statesIds: string[] = [];
  data: any[] = [];
  workSheet;
  workBook;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild('paginator1') paginator1: MatPaginator;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('selectHQ') selectHQ: MatSelect;
  @ViewChild('select') select: MatSelect;
  progressValue: number = 0;
  DumpPrimaryandSecondary: FormGroup;
  constructor(
    private _toastrService: ToastrService,
    private _urlService: URLService,
    private fb: FormBuilder,
    private stockiestService: StockiestService,
    private _changeDetectorRef: ChangeDetectorRef,
    private ERPServiceService: ERPServiceService,
    private _stateService: StateService,
    private _districtService: DistrictService,
    private _productMaster: ProductService
  ) {

  }

  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel,
      };
      this.callDivisionComponent.getDivisions(obj);
    } else {
      this.isShowDivision = false;
    }
    this.createForm();
    this.getStates();
    this.reportType('Headquarterwise');
    //this.createForm();
    //this.getProductOnSelectedUserStateWise();
  }
  createForm() {
    this.DumpPrimaryandSecondary = new FormGroup({
      type: new FormControl(null),
      divisionId: new FormControl("", Validators.required),
      product: new FormControl(null),
      fromDate: new FormControl("", Validators.required),
      toDate: new FormControl("", Validators.required),
    });
  }

  reportType(val) {
    this.callDivisionComponent.setBlank();
    this.data.length = 0;
    this.DumpPrimaryandSecondary.removeControl("stateInfo");
    this.DumpPrimaryandSecondary.removeControl("districtInfo");
    this.DumpPrimaryandSecondary.removeControl("designation");
    this.DumpPrimaryandSecondary.removeControl("userId");
    if (val == 'Employeewise') {
      this.DumpPrimaryandSecondary.addControl("designation", new FormControl("", Validators.required));
      this.DumpPrimaryandSecondary.addControl("userId", new FormControl("", Validators.required));
    } else {
      this.DumpPrimaryandSecondary.addControl("stateInfo", new FormControl("", Validators.required));
      this.DumpPrimaryandSecondary.addControl("districtInfo", new FormControl("", Validators.required));
    }
    this.DumpPrimaryandSecondary.patchValue({ type: val })
    this.isShowDiv = !this.isShowDiv;
  }

  getDivisionValue(val) {
    this.div = false;
    this.showTable = false;
    this.DumpPrimaryandSecondary.patchValue({ divisionId: val })
    this.getProductOnSelectedUserStateWise();
    this.designationComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }
    this.designationComponent.getDesignationBasedOnDivision(passingObj);
  }

  getDesignation(val) {
    this.showTable = false;
    this.DumpPrimaryandSecondary.patchValue({ designation: val.designationLevel });
    this.callEmployeeComponent.setBlank();
    let passingObj = {
      companyId: this.currentUser.companyId,
      designationObject: [val],
      status: [true],
      division: this.DumpPrimaryandSecondary.value.divisionId
    }
    this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
  }

  getEmployee(val) {
    this.showTable = false;
    this.DumpPrimaryandSecondary.patchValue({ userId: val });
  }

  stateData: any;
  stateIdArr = [];
  getStates() {
    this.showTable = false;
    this._stateService.getStates(this.currentUser.companyId).subscribe(res => {
      this.stateData = res[0].stateInfo;
    })
  }
  HqData: any;
  getStateValue(val) {
    this.showTable = false;
    this.stateIdArr = [];
    this.DumpPrimaryandSecondary.patchValue({ stateInfo: val })
    val.forEach(element => {
      this.stateIdArr.push(element.id);
    });
    this.HqData = [];
    this._districtService.getDistrictsForTarget(this.currentUser.companyId, this.stateIdArr).subscribe(res => {
      this.HqData = res;
    })
  }

  getDistrictValue(val) {
    this.showTable = false;
    this.isButtonDisabled = false;
    this.DumpPrimaryandSecondary.patchValue({ districtInfo: val })
  }

  getProduct(product) {
    this.showTable = false;
    this.isButtonDisabled = false;
    this.DumpPrimaryandSecondary.patchValue({ product: product })
  }

  getProductOnSelectedUserStateWise() {
    this._productMaster.getProductsBasedOnDivision(this.currentUser.companyId, this.DumpPrimaryandSecondary.value.divisionId).subscribe(res => {
      this.data.length = 0;
      this.data = res;
    })
    this._changeDetectorRef.detectChanges();
  }

  getFromDate(val) {
    this.month = false;
    this.showTable = false;
    this.isButtonDisabled = false;
    this.DumpPrimaryandSecondary.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.showTable = false;
    this.isButtonDisabled = false;
    this.DumpPrimaryandSecondary.patchValue({ toDate: val });
  }

  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  toggleAllSelectionForHQ() {
    this.allSelectedHq = !this.allSelectedHq;  // to control select-unselect
    if (this.allSelectedHq) {
      this.selectHQ.options.forEach((item: MatOption) => item.select());
    } else {
      this.selectHQ.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  //getting month and year on the basis of from and to date

  monthNameAndYearBTWTwoDate(fromDate, toDate) {
    let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let arr = [];
    let datFrom = new Date(fromDate);
    let datTo = new Date(toDate);
    let fromYear = datFrom.getFullYear();
    let toYear = datTo.getFullYear();
    let diffYear = (12 * (toYear - fromYear)) + datTo.getMonth();
    for (let i = datFrom.getMonth(); i <= diffYear; i++) {
      arr.push({
        monthName: monthNames[i % 12],
        month: (i % 12) + 1,
        year: Math.floor(fromYear + (i / 12)),
        endDate: parseInt(_moment(new Date(Math.floor(fromYear + (i / 12)), (i % 12), 1)).endOf("month").format('DD'))
      });
    }
    return arr;
  }

  //END

  async getsssDetails() {
    this.progressValue = 0;
    this.dataSource.length = 0;
    this.showTable = false;
    this.showProcessing = true;
    this.showDownloadProcessing = false;
    this.isButtonDisabled = false;
    let params = {};
    let fromDate = _moment(this.DumpPrimaryandSecondary.value.fromDate).format("YYYY-MM-DD");
    let toDate = _moment(this.DumpPrimaryandSecondary.value.toDate).format("YYYY-MM-DD");
    this.monthYearArray = this.monthNameAndYearBTWTwoDate(fromDate, toDate);
    params = {
      type: this.DumpPrimaryandSecondary.value.type,
      divisionId: this.DumpPrimaryandSecondary.value.divisionId,
      companyId: this.currentUser.companyId,
      APIEndPoint: this.currentUser.company.APIEndPoint,
      product: this.DumpPrimaryandSecondary.value.product
    }

    if (this.DumpPrimaryandSecondary.value.type == "Headquarterwise") {
      const states = this.DumpPrimaryandSecondary.value.stateInfo;
      const districts = this.DumpPrimaryandSecondary.value.districtInfo.filter(item => item);
      for (let i = 0; i < states.length; i++) {
        const stateSpecificDistricts = districts.filter((district) => district.stateId.toString() === states[i].id.toString()).map(data => {
          const obj = {
            "districtName": data.districtName,
            "erpHqCode": data.erpHqCode,
            "districtId": data._id,
            "isPoolDistrict": data.isPoolDistrict
          }
          return obj;
        });

        params['selectedHqDetails'] = stateSpecificDistricts;
        const data = await this.getSingleStateData({ ...params });
        let finalObj = { districts: [], months: [], data: [] };
        data.forEach(districtwise => {
          districtwise['districtWiseData'].forEach((productwiseData, productwiseIndex) => {
            if (!Boolean(finalObj.districts.find(x => x.district === productwiseData['district']))) {
              finalObj['districts'].push({ district: productwiseData['district'] });
              finalObj['data'].push(productwiseData["data"]);
            } else {
              productwiseData['data'].forEach((monthwise, monthwiseIndex) => {
                finalObj.data[productwiseIndex][monthwiseIndex] = [...finalObj.data[productwiseIndex][monthwiseIndex], ...(monthwise.slice(3))]
              });
            }
          });
        });

        let total = [];
        let primaryQty = 0;
        let targetValQty = 0;
        let totalAchQty = 0;
        let primaryAmt = 0;
        let targetValAmt = 0;
        let totalAchAmt = 0;
        finalObj['data'].forEach((hqWise, indexHq) => {
          hqWise.forEach((element, productIndex) => {
            //total.length = 0;
            let horizontalTotalPrimaryQty = 0, horizontalTotalPrimaryAmt = 0.0, horizontalTotalReturnQty = 0, horizontalTotalReturnAmt = 0.0, horizontalTotalExpiryQty = 0, horizontalTotalExpiryAmt = 0.0, horizontalTotalBreakageQty = 0, horizontalTotalBreakageAmt = 0.0,
              horizontalTotalOpeningQty = 0, horizontalTotalOpeningAmt = 0.0, horizontalTotalSecondaryQty = 0, horizontalTotalSecondaryAmt = 0.0, horizontalTotalClosingQty = 0, horizontalTotalClosingAmt = 0.0, horizontalTotalPrimaryFreeQty = 0, horizontalTotalPrimaryFreeAmt = 0.0,
              horizontalTotalReturnFreeQty = 0, horizontalTotalReturnFreeAmt = 0.0, horizontalTotalTargetQty = 0, horizontalTotalTargetAmt = 0.0, horizontalTotalPerAchQty = 0, horizontalTotalPerAchAmt = 0.0;
            // Horizontal Productwise Grand Total Work
            let j = 3;
            let nextIndex = 22;
            for (let i = 0; i < this.monthYearArray.length; i++) {
              horizontalTotalTargetQty += parseInt(element[j]);
              horizontalTotalOpeningQty += parseInt(element[j + 1]);
              horizontalTotalPrimaryQty += parseInt(element[j + 2]);
              horizontalTotalPrimaryFreeQty += parseInt(element[j + 3]);
              horizontalTotalSecondaryQty += parseInt(element[j + 4]);
              horizontalTotalPerAchQty += parseInt(element[j + 5]);
              horizontalTotalReturnQty += parseInt(element[j + 6]);
              horizontalTotalReturnFreeQty += parseInt(element[j + 7]);
              horizontalTotalExpiryQty += parseInt(element[j + 8]);
              horizontalTotalBreakageQty += parseInt(element[j + 9]);
              horizontalTotalClosingQty += parseInt(element[j + 10]);
              horizontalTotalTargetAmt += parseFloat(element[j + 11]);
              horizontalTotalOpeningAmt += parseFloat(element[j + 12]);
              horizontalTotalPrimaryAmt += parseFloat(element[j + 13]);
              horizontalTotalPrimaryFreeAmt += parseFloat(element[j + 14]);
              horizontalTotalSecondaryAmt += parseFloat(element[j + 15]);
              horizontalTotalPerAchAmt += parseFloat(element[j + 16]);
              horizontalTotalReturnAmt += parseFloat(element[j + 17]);
              horizontalTotalReturnFreeAmt += parseFloat(element[j + 18]);
              horizontalTotalExpiryAmt += parseFloat(element[j + 19]);
              horizontalTotalBreakageAmt += parseFloat(element[j + 20]);
              horizontalTotalClosingAmt += parseFloat(element[j + 21]);
              j = j + nextIndex;
            }
            //total.push();
            primaryQty = horizontalTotalPrimaryQty
            targetValQty = horizontalTotalTargetQty;
            totalAchQty = primaryQty / targetValQty * 100;
            primaryAmt = horizontalTotalPrimaryAmt
            targetValAmt = horizontalTotalTargetAmt;
            totalAchAmt = primaryAmt / targetValAmt * 100;
            finalObj['data'][indexHq][productIndex].push(horizontalTotalTargetQty, horizontalTotalOpeningQty, horizontalTotalPrimaryQty, horizontalTotalPrimaryFreeQty,
              horizontalTotalSecondaryQty, totalAchQty ? totalAchQty.toFixed(2) : 0, horizontalTotalReturnQty, horizontalTotalReturnFreeQty, horizontalTotalExpiryQty, horizontalTotalBreakageQty,
              horizontalTotalClosingQty, horizontalTotalTargetAmt, horizontalTotalOpeningAmt, horizontalTotalPrimaryAmt, horizontalTotalPrimaryFreeAmt,
              horizontalTotalSecondaryAmt, totalAchAmt ? totalAchAmt.toFixed(2) : 0.0, horizontalTotalReturnAmt, horizontalTotalReturnFreeAmt, horizontalTotalExpiryAmt, horizontalTotalBreakageAmt, horizontalTotalClosingAmt);
          });
        });
        let districtwise = [];
        finalObj['districts'].forEach((element, index) => {
          districtwise.push({ district: element.district, data: finalObj['data'][index] });
        });
        const finalData = { districtWiseData: districtwise, months: this.monthYearArray }
        if (data) this.dataSource.push({ ...finalData })
        this.progressValue = Math.floor((i + 1) / states.length * 100)
        this._changeDetectorRef.detectChanges();
      }

      if (this.dataSource) {
        this.showProcessing = false;
        this.showTable = true;
        this.isDisabledBtn = false;
        this.dataSource['headerName'] = "Headquarter";
        this.dataSource['tabName'] = "All Headquarters";
        this.dataSource['fileName'] = 'Target & SSS Report(Headquarterwise).xlsx';
      }
      this._changeDetectorRef.detectChanges();
    } else if (this.DumpPrimaryandSecondary.value.type == "Employeewise") {
      let filter = {};
      if (this.DumpPrimaryandSecondary.value.designation > 1) {
        filter = { companyId: this.currentUser.companyId, supervisorId: { '$in': [this.DumpPrimaryandSecondary.value.userId] }, fromMultiMonth: true, mgrSelf: true };
      } else {
        filter = { companyId: this.currentUser.companyId, userId: { '$in': [this.DumpPrimaryandSecondary.value.userId] }, fromMultiMonth: true };
      }
      this.stockiestService.getManagerHierarchyWithUniqueHQs(filter).subscribe(async heirarchy => {
        if (heirarchy.length > 0) {
          const heirarchyData = heirarchy[0];
          for (let i = 0; i < heirarchyData.stateId.length; i++) {
            const stateSpecificUsers = heirarchyData.uniqueUsers.filter((user) => user.stateId.toString() === heirarchyData.stateId[i].toString()).map(data => {
              let erpHqCodeVar;
              if (data.erpCodeForMainHQ) {
                erpHqCodeVar = data.erpCodeForMainHQ;
              } else {
                erpHqCodeVar = data.erpHqCode;
              }
              const obj = {
                "userName": data.userName,
                "userId": data.userId,
                "employeeCode": data.employeeCode,
                "userDesignation": data.userDesignation,
                "erpHqCode": erpHqCodeVar,
                //"districtName": data.districtName,
                //"districtId": data._id,
                //"isPoolDistrict": data.isPoolDistrict
              }
              return obj;
            })
            params['selectedUserDetails'] = stateSpecificUsers;
            const data = await this.getSingleStateData({ ...params });
            let finalObj = { districts: [], months: [], data: [] };
            data.forEach(districtwise => {
              districtwise['districtWiseData'].forEach((productwiseData, productwiseIndex) => {
                if (!Boolean(finalObj.districts.find(x => x.district === productwiseData['district']))) {
                  finalObj['districts'].push({ district: productwiseData['district'] });
                  finalObj['data'].push(productwiseData["data"]);
                } else {
                  productwiseData['data'].forEach((monthwise, monthwiseIndex) => {
                    finalObj.data[productwiseIndex][monthwiseIndex] = [...finalObj.data[productwiseIndex][monthwiseIndex], ...(monthwise.slice(3))]
                  });
                }
              });
            });

            let total = [];
            let primaryQty = 0;
            let targetValQty = 0;
            let totalAchQty = 0;
            let primaryAmt = 0;
            let targetValAmt = 0;
            let totalAchAmt = 0;
            finalObj['data'].forEach((hqWise, indexHq) => {
              hqWise.forEach((element, productIndex) => {
                //total.length = 0;
                let horizontalTotalPrimaryQty = 0, horizontalTotalPrimaryAmt = 0.0, horizontalTotalReturnQty = 0, horizontalTotalReturnAmt = 0.0, horizontalTotalExpiryQty = 0, horizontalTotalExpiryAmt = 0.0, horizontalTotalBreakageQty = 0, horizontalTotalBreakageAmt = 0.0,
                  horizontalTotalOpeningQty = 0, horizontalTotalOpeningAmt = 0.0, horizontalTotalSecondaryQty = 0, horizontalTotalSecondaryAmt = 0.0, horizontalTotalClosingQty = 0, horizontalTotalClosingAmt = 0.0, horizontalTotalPrimaryFreeQty = 0, horizontalTotalPrimaryFreeAmt = 0.0,
                  horizontalTotalReturnFreeQty = 0, horizontalTotalReturnFreeAmt = 0.0, horizontalTotalTargetQty = 0, horizontalTotalTargetAmt = 0.0, horizontalTotalPerAchQty = 0, horizontalTotalPerAchAmt = 0.0;
                // Horizontal Productwise Grand Total Work
                let j = 3;
                let nextIndex = 22;
                for (let i = 0; i < this.monthYearArray.length; i++) {
                  horizontalTotalTargetQty += parseInt(element[j]);
                  horizontalTotalOpeningQty += parseInt(element[j + 1]);
                  horizontalTotalPrimaryQty += parseInt(element[j + 2]);
                  horizontalTotalPrimaryFreeQty += parseInt(element[j + 3]);
                  horizontalTotalSecondaryQty += parseInt(element[j + 4]);
                  horizontalTotalPerAchQty += parseInt(element[j + 5]);
                  horizontalTotalReturnQty += parseInt(element[j + 6]);
                  horizontalTotalReturnFreeQty += parseInt(element[j + 7]);
                  horizontalTotalExpiryQty += parseInt(element[j + 8]);
                  horizontalTotalBreakageQty += parseInt(element[j + 9]);
                  horizontalTotalClosingQty += parseInt(element[j + 10]);
                  horizontalTotalTargetAmt += parseFloat(element[j + 11]);
                  horizontalTotalOpeningAmt += parseFloat(element[j + 12]);
                  horizontalTotalPrimaryAmt += parseFloat(element[j + 13]);
                  horizontalTotalPrimaryFreeAmt += parseFloat(element[j + 14]);
                  horizontalTotalSecondaryAmt += parseFloat(element[j + 15]);
                  horizontalTotalPerAchAmt += parseFloat(element[j + 16]);
                  horizontalTotalReturnAmt += parseFloat(element[j + 17]);
                  horizontalTotalReturnFreeAmt += parseFloat(element[j + 18]);
                  horizontalTotalExpiryAmt += parseFloat(element[j + 19]);
                  horizontalTotalBreakageAmt += parseFloat(element[j + 20]);
                  horizontalTotalClosingAmt += parseFloat(element[j + 21]);
                  j = j + nextIndex;
                }
                //total.push();
                primaryQty = horizontalTotalPrimaryQty
                targetValQty = horizontalTotalTargetQty;
                totalAchQty = primaryQty / targetValQty * 100;
                primaryAmt = horizontalTotalPrimaryAmt
                targetValAmt = horizontalTotalTargetAmt;
                totalAchAmt = primaryAmt / targetValAmt * 100;
                finalObj['data'][indexHq][productIndex].push(horizontalTotalTargetQty, horizontalTotalOpeningQty, horizontalTotalPrimaryQty, horizontalTotalPrimaryFreeQty,
                  horizontalTotalSecondaryQty, totalAchQty ? totalAchQty.toFixed(2) : 0, horizontalTotalReturnQty, horizontalTotalReturnFreeQty, horizontalTotalExpiryQty, horizontalTotalBreakageQty,
                  horizontalTotalClosingQty, horizontalTotalTargetAmt, horizontalTotalOpeningAmt, horizontalTotalPrimaryAmt, horizontalTotalPrimaryFreeAmt,
                  horizontalTotalSecondaryAmt, totalAchAmt ? totalAchAmt.toFixed(2) : 0.0, horizontalTotalReturnAmt, horizontalTotalReturnFreeAmt, horizontalTotalExpiryAmt, horizontalTotalBreakageAmt, horizontalTotalClosingAmt);
              });
            });
            let districtwise = [];
            finalObj['districts'].forEach((element, index) => {
              districtwise.push({ district: element.district, data: finalObj['data'][index] });
            });
            const finalData = { districtWiseData: districtwise, months: this.monthYearArray }
            if (data) this.dataSource.push({ ...finalData })
            this.progressValue = Math.floor((i + 1) / heirarchyData.stateId.length * 100)
            this._changeDetectorRef.detectChanges();
          }
          if (this.dataSource) {
            this.showProcessing = false;
            this.showTable = true;
            this.isDisabledBtn = false;
          }
          this.dataSource['headerName'] = "Employee Name";
          this.dataSource['tabName'] = "All Employees";
          this.dataSource['fileName'] = 'Target & SSS Report(Employeewise).xlsx';
          this._changeDetectorRef.detectChanges();
        } else {
          alert('Manager heirarchy not found !!');
          this._changeDetectorRef.detectChanges();
        }
      });
    }
  }

  async getSingleStateData(params) {
    let singleStateMultiMonthData = [];
    for (let i = 0; i < this.monthYearArray.length; i++) {
      params['fromDate'] = _moment(this.monthYearArray[i].year + "-" + this.monthYearArray[i].month + "-01").format("YYYY-MM-DD");
      params['toDate'] = _moment(this.monthYearArray[i].year + "-" + this.monthYearArray[i].month + "-" + this.monthYearArray[i].endDate).format("YYYY-MM-DD");
      params['fromPrimDate'] = _moment(this.monthYearArray[i].year + "-" + this.monthYearArray[i].month + "-01").format("DD-MM-YYYY");
      params['toPrimDate'] = _moment(this.monthYearArray[i].year + "-" + this.monthYearArray[i].month + "-" + this.monthYearArray[i].endDate).format("DD-MM-YYYY");
      const singlemonthdata = await this.getSingleMonthData({ ...params });
      singleStateMultiMonthData.push({ ...singlemonthdata });
    }
    return singleStateMultiMonthData;
  }


  getSingleMonthData(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this.stockiestService.getSSSDumpMultiMonth(params).subscribe(res => {
        if (res) {
          resolve(res);
        }
      }, err => reject(err))
    })
  }

  exportTable() {
    this.isButtonDisabled = true;
    this.showDownloadProcessing = true;
    //Define header names
    const dataSource = this.dataSource[0];
    const finalHeader = [this.dataSource['headerName'], "Product Name", "Product Rate"];
    const multiLoopHeader = ["Target (Qty)", "Opening (Qty)", "Primary Actual (Qty)", "Primary Free (Qty)", "Secondary (Qty)",
      "Ach % (Qty)", "Return (Qty)", "Return Free (Qty)", "Expiry (Qty)", "Breakage (Qty)", "Closing (Qty)", "Target (Value)", "Opening (Value)", "Primary Actual (Value)",
      "Primary Free (Value)", "Secondary (Value)", "Ach % (Value)", "Return (Value)", "Return Free (Value)", "Expiry (Value)", "Breakage (Value)", "Closing Value"];

    const totalHorizontalHeader = ["Total Target (Qty)", "Total Opening (Qty)", "Total Primary Actual (Qty)", "Total Primary Free (Qty)", "Total Secondary (Qty)",
      "Total Ach % (Qty)", "Total Return (Qty)", "Total Return Free (Qty)", "Total Expiry (Qty)", "Total Breakage (Qty)", "Total Closing (Qty)", "Total Target (Value)", "Total Opening (Value)", "Total Primary Actual (Value)",
      "Total Primary Free (Value)", "Total Secondary (Value)", "Total Ach % (Value)", "Total Return (Value)", "Total Return Free (Value)", "Total Expiry (Value)", "Total Breakage (Value)", "Total Closing Value"];

    dataSource['months'].forEach(month => {
      multiLoopHeader.forEach(loopHeader => {
        finalHeader.push(loopHeader + " " + month.monthName + "-" + month.year);
      });
    });
    totalHorizontalHeader.forEach(totalHeader => {
      finalHeader.push(totalHeader);
    });

    let grandTotalIntialValueForAll: any[] = ['GRAND TOTAL', '----', '----'];
    let grandTotalIntialValueForSingleTab: any[] = ['GRAND TOTAL', '----', '----'];
    let excelAllTabData = [];
    let grandTotal = {};
    let grandTotalForSingleTab = {};
    let blankRow = {};
    grandTotal[finalHeader[0]] = grandTotalIntialValueForAll[0];
    grandTotal[finalHeader[1]] = grandTotalIntialValueForAll[1];
    grandTotal[finalHeader[2]] = grandTotalIntialValueForAll[2];
    grandTotalForSingleTab[finalHeader[0]] = grandTotalIntialValueForSingleTab[0];
    grandTotalForSingleTab[finalHeader[1]] = grandTotalIntialValueForSingleTab[1];
    grandTotalForSingleTab[finalHeader[2]] = grandTotalIntialValueForSingleTab[2];
    finalHeader.forEach((element, index) => {
      //Adding Blank row before Grand TOTAL
      blankRow[element] = '';
      //TOTAL Array values
      if (index > 2) {
        grandTotalIntialValueForAll.push(0);
        grandTotalIntialValueForSingleTab.push(0);
      }
    });

    // Make new work book
    this.workBook = XLSX.utils.book_new();
    //All headquarters Tab data
    this.dataSource.forEach((stateWise) => {
      stateWise['districtWiseData'].forEach((elem) => {
        if (elem.district != "NA") {
          elem.data.forEach(elemData => {
            let dataToExport = {};
            for (let i = 0; i < elemData.length; i++) {
              dataToExport[finalHeader[i]] = elemData[i];
              if (i > 2) {
                grandTotalIntialValueForAll[i] = grandTotalIntialValueForAll[i] + elemData[i];
              }
            }
            excelAllTabData.push({ ...dataToExport });
          });
        }
      });
    });
    finalHeader.forEach((element, index) => {
      //grandTotal[element] = parseInt(grandTotalIntialValueForAll[index]).toFixed(2);
      grandTotal[element] = grandTotalIntialValueForAll[index];
    });
    let i = 3, j = 5, k = 8;
    let acheivementIndexes = [];
    const totalIndexLength = ((this.monthYearArray.length * 4));
    for (let ii = 0; ii < totalIndexLength; ii++) {
      acheivementIndexes.push({
        targetIndex: i,
        primaryIndex: j,
        achPushIndex: k
      });
      i = i + 11;
      j = j + 11;
      k = k + 11;
    };
    let primary = 0.0;
    let targetVal = 0.0;
    let totalAch = 0.0;
    acheivementIndexes.forEach(element => {
      primary = grandTotal[finalHeader[element['primaryIndex']]];
      targetVal = grandTotal[finalHeader[element['targetIndex']]];
      totalAch = primary / targetVal * 100;
      grandTotal[finalHeader[element['achPushIndex']]] = totalAch.toFixed(2);
    });
    excelAllTabData.push({ ...blankRow }, { ...grandTotal });
    this.workSheet = XLSX.utils.json_to_sheet([...excelAllTabData]);
    XLSX.utils.book_append_sheet(this.workBook, this.workSheet, this.dataSource['tabName']);

    //Start tabwise excel
    let excelTabData = [];
    this.dataSource.forEach((stateWise) => {
      stateWise['districtWiseData'].forEach((elem) => {
        if (elem.district != "NA") {
          excelTabData.length = 0;
          elem.data.forEach(elemData => {
            let dataToExport = {};
            for (let i = 0; i < elemData.length; i++) {
              dataToExport[finalHeader[i]] = elemData[i];
              if (i > 2) {
                grandTotalIntialValueForSingleTab[i] = grandTotalIntialValueForSingleTab[i] + elemData[i];
              }
            }
            excelTabData.push({ ...dataToExport });
          });
          finalHeader.forEach((element, index) => {
            grandTotalForSingleTab[element] = grandTotalIntialValueForSingleTab[index];
          });
          excelTabData.push({ ...blankRow }, { ...grandTotalForSingleTab });
          this.workSheet = XLSX.utils.json_to_sheet([...excelTabData]);
          XLSX.utils.book_append_sheet(this.workBook, this.workSheet, elem.district);
          grandTotalForSingleTab = {};
          grandTotalIntialValueForSingleTab.length = 0;
          grandTotalIntialValueForSingleTab = ['GRAND TOTAL', '----', '----'];
          grandTotalForSingleTab[finalHeader[0]] = grandTotalIntialValueForSingleTab[0];
          grandTotalForSingleTab[finalHeader[1]] = grandTotalIntialValueForSingleTab[1];
          grandTotalForSingleTab[finalHeader[2]] = grandTotalIntialValueForSingleTab[2];
          finalHeader.forEach((element, index) => {
            //TOTAL Array values
            if (index > 2) {
              grandTotalIntialValueForSingleTab.push(0);
            }
          });
        }
      });
    });
    this.showDownloadProcessing = false;
    XLSX.writeFile(this.workBook, this.dataSource['fileName'])//"Target & SSS Report.xlsx");
    this.isButtonDisabled = false;
    this._changeDetectorRef.detectChanges();
  }
}
