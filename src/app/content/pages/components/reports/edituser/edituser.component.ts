import { ToastrService } from "ngx-toastr";
import {
	Component,
	OnInit,
	Inject,
	ViewChild,
	ɵConsole,
	ChangeDetectorRef,
} from "@angular/core";
import {
	FormGroup,
	FormBuilder,
	Validators,
	FormControl,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { UserService } from "../../../../../core/services/user.service";
import { UserDetailsService } from "../../../../../core/services/user-details.service";
import { UserLogin } from "../../../_core/models/userlogin.model";
import { DesignationService } from "../../../../../core/services/Designation/designation.service";
import { DistrictService } from "../../../../../core/services/district.service";
import { DesignationComponent } from "../../filters/designation/designation.component";
import {
	NativeDateAdapter,
	DateAdapter,
	MatDateFormats,
	MAT_DATE_FORMATS,
} from "@angular/material";
import { DivisionComponent } from "../../filters/division/division.component";

export class AppDateAdapter extends NativeDateAdapter {
	format(date: Date, displayFormat: Object): string {
		if (displayFormat === "input") {
			let day: string = date.getDate().toString();
			day = +day < 10 ? "0" + day : day;
			let month: string = (date.getMonth() + 1).toString();
			month = +month < 10 ? "0" + month : month;
			let year = date.getFullYear();
			return `${day}-${month}-${year}`;
		}
		return date.toDateString();
	}
}
export const APP_DATE_FORMATS: MatDateFormats = {
	parse: {
		dateInput: { month: "short", year: "numeric", day: "numeric" },
	},
	display: {
		dateInput: "input",
		monthYearLabel: { year: "numeric", month: "numeric" },
		dateA11yLabel: {
			year: "numeric",
			month: "long",
			day: "numeric",
		},
		monthYearA11yLabel: { year: "numeric", month: "long" },
	},
};

@Component({
	selector: "m-edituser",
	templateUrl: "./edituser.component.html",
	styleUrls: ["./edituser.component.scss"],
	providers: [
		{ provide: DateAdapter, useClass: AppDateAdapter },
		{ provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
	],
})
export class EdituserComponent implements OnInit {
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;

	userEditForm: FormGroup;
	dataSource;
	districtData;
	designationData: any;
	divisionData: any;
	managerData: any;
	showDivisionFilter: boolean = false;
	isShowDivision = false;
	queryObject;
	usernameAlreadyExist: boolean = true;

	reportingManagerData: UserLogin;
	obj = JSON.parse(sessionStorage.currentUser);
	currentUser = JSON.parse(sessionStorage.currentUser);

	constructor(
		public dialogRef: MatDialogRef<EdituserComponent>,
		private toastrService: ToastrService,
		private _userService: UserService,
		private changeDetectRef: ChangeDetectorRef,
		private districtService: DistrictService,
		private designationServie: DesignationService,
		private userDetailsService: UserDetailsService,
		// private _divisionService: DivisionService,

		@Inject(MAT_DIALOG_DATA) public data: any,
		private fb: FormBuilder
	) {}
	// desigSelected = this.data.obj.designation;

	ngOnInit() {
		console.log(this.data);
		this.userEditForm = this.fb.group({
			id: this.data.obj.id,
			employeeCode: this.data.obj.employeeCode,
			name: this.data.obj.name,
			designation: this.data.obj.designation,
			designationLevel: this.data.obj.designationLevel,
			lockingPeriod: this.data.obj.lockingPeriod,
			editLockingPeriod: this.data.obj.editLockingPeriod,
			email: this.data.obj.email,
			mobile: this.data.obj.mobile,
			address: this.data.obj.address,
			username: this.data.obj.username,
			password: this.data.obj.password,
			pan: this.data.obj.PAN,
			aadhaar: this.data.obj.aadhaar,
			DLNumber: this.data.obj.DLNumber,
			// immediateSupervisorName: this.data.obj.immediateSupervisorName,
			//  immediateSupervisorId: this.data.obj.immediateSupervisorId,             //since for employee who dont has any existing hierarchy, These Four
			//supervisorDesignation: this.data.obj.supervisorDesignation,             //fields are undefined in current emmited object.
			// supervisorDesignationLevel:this.data.obj.immediateManager.supervisorDesignationLevel,
			immediateSupervisorName: new FormControl(null),
		  immediateSupervisorId: new FormControl(null),
			supervisorDesignation: new FormControl(null),
			supervisorDesignationLevel: new FormControl(null),
			dateOfBirth: this.data.obj.dateOfBirth,
			dateOfAnniversary: this.data.obj.dateOfAnniversary,
			dateOfJoining: this.data.obj.dateOfJoining,
			reportingDate: this.data.obj.dateOfJoining,
			status: this.data.obj.status,
			divisionId: this.data.obj.division, // edited by umesh dec-2019
			bankName: this.data.obj.bankName,
			bankAccountNumber: this.data.obj.bankAccountNumber,
			ifsc: this.data.obj.ifsc,
			district: this.data.obj.districtName,
		});

		//----value will be selected at selection filter--
		//  this.callDesignationComponent.desigSelected = [this.data.obj.designation];
		this.designationServie
			.getDesignations(this.obj.companyId)
			.subscribe((res) => {
				this.designationData = res;
			});

		this.userDetailsService
			.getAllManager(this.obj.companyId)
			.subscribe((res) => {
				this.managerData = res;
				console.log("Manager",this.managerData );
			});

		if (this.currentUser.company.isDivisionExist == true) {
			this.userEditForm.setControl("division", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.showDivisionFilter = true;
		} else {
			this.isShowDivision = false;
		}
	}

	getDivisionValue($event) {
		this.userEditForm.patchValue({ divisionId: $event });
	}

	onChange($event) {
		var stateId = $event;
		this.districtService
			.getDistricts(this.obj.companyId, stateId)
			.subscribe((districts) => {
				this.districtData = districts;
			});
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	userNameCheckUnique() {
		this.queryObject = {
			where: {
				username: this.userEditForm.value.username,
			},
		};
		this.userDetailsService
			.getUserName(this.queryObject)
			.subscribe((res) => {
				if (res.length > 0) {
					if (res[0].username === this.data.obj.username) {
						this.usernameAlreadyExist = true;
					} else {
						this.usernameAlreadyExist = false;
						this.toastrService.error(
							"Username is already exist..."
						);
					}
				} else {
					this.usernameAlreadyExist = true;
				}
			});
	}

	getManagerDetails(val) {
		this.userEditForm.patchValue({ immediateSupervisorName: val });
		if (
			this.data.obj.immediateManager.hasOwnProperty(
				"immediateSupervisorId"
			) &&
			this.data.obj.immediateManager.hasOwnProperty(
				"supervisorDesignation"
			) &&
			this.data.obj.immediateManager.hasOwnProperty("immediateManager")
		)
	     {
			this.userEditForm.patchValue({
				immediateSupervisorId: this.data.obj.immediateSupervisorId,
			});
			this.userEditForm.patchValue({
				immediateSupervisorId: this.data.obj.supervisorDesignation,
			});
			this.userEditForm.patchValue({
				immediateSupervisorId:
					this.data.obj.immediateManager.supervisorDesignationLevel,
			});
		}
	}

	userEdit() {
		let formData = this.userEditForm.value;
		if ((this.usernameAlreadyExist = false)) {
			this.toastrService.error("Please enter valid username");
		} else {
			this.userDetailsService
				.editUser(formData, this.obj.company.id, this.obj.id)
				.subscribe(
					(res) => {
            
						this.toastrService.success(
							"User Modify Successfully..."
						);
						//
						this.changeDetectRef.detectChanges();
						if (!this.changeDetectRef["destroyed"]) {
							this.changeDetectRef.detectChanges();
						}
						//
					},
					(err) => {
						console.log(err);
						this.changeDetectRef.detectChanges();
						this.toastrService.error(
							"Error in User edit component"
						);
					}
				);
			this.ngOnInit();
		}
	}

	getDesignationValue(desig: any): void {
		const selectedDesignationLevel = this.designationData.find(
			(res) => res.designation === desig
		).designationLevel;
		this.userEditForm.patchValue({
			designationLevel: selectedDesignationLevel,
		});
	}
}
