import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDCRResignedUserComponent } from './view-dcrresigned-user.component';

describe('ViewDCRResignedUserComponent', () => {
  let component: ViewDCRResignedUserComponent;
  let fixture: ComponentFixture<ViewDCRResignedUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDCRResignedUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDCRResignedUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
