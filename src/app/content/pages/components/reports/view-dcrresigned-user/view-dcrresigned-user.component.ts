import { Component, OnInit, ChangeDetectorRef, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DCRProviderVisitDetailsBackupService } from '../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import * as moment from 'moment';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';

declare var $;
;

@Component({
  selector: 'm-view-dcrresigned-user',
  templateUrl: './view-dcrresigned-user.component.html',
  styleUrls: ['./view-dcrresigned-user.component.scss']
})

export class ViewDCRResignedUserComponent implements OnInit {
  count = 0;

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  dataSources;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;
  public showReport1: boolean = false;
  public showDCRSummary:boolean=false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  @ViewChild("userDataTable") dataTable1;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;

  userDataTable: any;
  userDTOptions: any;
  constructor(public dialogRef: MatDialogRef<ViewDCRResignedUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private changeDetectedRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private DCRProviderVisitDetailsBackupService: DCRProviderVisitDetailsBackupService,
    private exportAsService: ExportAsService,
    private _dcrReportsService: DcrReportsService,

  ) { }

  ngOnInit() {
  }
  dcrForm = new FormGroup({
    toDate: new FormControl(null),
    fromDate: new FormControl(null)
  })
  getFromDate(val) {
    this.dcrForm.patchValue({ fromDate: val });

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  getToDate(val) {
    this.dcrForm.patchValue({ toDate: val });
  }
  getDCRInfo(val) {
    if (val != undefined) {
      this.showDCRSummary = false;
      this.callUserCompleteDCR.getUserCompleteDCRDetails(val);

    } else {
      this.showDCRSummary = true;
    }
    //this.router.navigate(['/reports/usercompletedcr'],{ queryParams: { dcrId: val } });
  }
  showReport() {
    this.showProcessing = true;
    if(this.data.type=="DCRView"){
    let userId = this.data.obj._id.submitBy;
    let fromDate = this.dcrForm.value.fromDate;
    let toDate = this.dcrForm.value.toDate;

    this._dcrReportsService.getUserDateWiseDCRDetail(userId, fromDate, toDate).subscribe(result => {
      
      if ( result == undefined) {
        this.showReport1 = false;
        this.showProcessing = false;
        this.toastr.info("No Record Found.")

      } else{
        this.showReport1 = true;
        this.showProcessing = false;

      this.userDTOptions = {
        "pagingType": 'full_numbers',
        "paging": true,
        "ordering": true,
        "info": true,
        "scrollY": 300,
        "scrollX": true,
        "fixedColumns": true,
        destroy: true,
        data: result,
        responsive: true,
        columns: [
          {
            title: 'Division',
            data: 'divisionName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Date',
            data: 'dcrDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                console.log("dcr date=",data);
                
               var date1= moment(data).format('DD-MM-YYYY')
                return ` <a id="dcrView" style="cursor: pointer;color: blue;white-space:normal;width:100px;"> ${date1} </a>`
              }
            },
            defaultContent: '---'
          }, {
            title: 'Name',
            data: 'userName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'Working Status',
            data: 'workingStatus',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }
          , {
            title: 'Visited Block',
            data: 'visitedBlock',
            render: function (data: any) {
              if (data === '') {
                return '---'
              } else {
                let visitedBlock = '';
                for (let i = 0; i < data.length; i++) {
                  if (i == 0) visitedBlock =  `<b>From</b> : ${data[i].from } - <b>To</b> : ${data[i].to}`  // 'From : '+ data[i].from + '--  To : ' + data[i].to;
                  else visitedBlock = `${visitedBlock},<br> <b>From</b> : ${data[i].from } - <b>To</b> : ${data[i].to}`
                }

                return `<div style="white-space:normal;width:250px;"> ${visitedBlock} </div>`
              }
            },
            defaultContent: '---'
          }, {
            title: 'Time In',
            data: 'timeIn',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'Time Out',
            data: 'timeOut',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'Joint Work',
            data: 'jointWork',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                let jointWork = '';
                for (let i = 0; i < data.length; i++) {
                  if (i == 0) jointWork = data[i].jointWork
                  else jointWork = jointWork+data[i].jointWork 
                return `<div style="white-space:normal;width:250px;"> ${jointWork} </div>`
              }
              return jointWork
            }},
            defaultContent: '---'
          }, {
            title: 'Total'+ this.currentUser.company.lables.doctorLabel +'Met',
            data: 'RMPCount',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'Total'+ this.currentUser.company.lables.doctorLabel +'POB',
            data: 'doctorPOB',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'Listed'+ this.currentUser.company.lables.doctorLabel +'Met',
            data: 'listedCount',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'Listed'+ this.currentUser.company.lables.doctorLabel +'POB',
            data: 'listedDoctorPOB',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title:  'UnListed'+ this.currentUser.company.lables.doctorLabel +'Met',
            data: 'unlistedRMPcount',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'UnListed'+ this.currentUser.company.lables.doctorLabel +'POB',
            data: 'unlistedDoctorPOB',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: this.currentUser.company.lables.vendorLabel+ "/"+this.currentUser.company.lables.stockistLabel+" Met" ,
            data: 'DrugStoreCount',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: this.currentUser.company.lables.vendorLabel+ "/"+this.currentUser.company.lables.stockistLabel+" POB" ,
            data: 'vendorPOB',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: 'Remarks',
            data: 'remarks',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }

        ],
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          
          const self = this;
           console.log("raw call");
           
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td', row).unbind('click');

          $('td a', row).bind('click', (event) => {
            console.log("row",row);
            
            //console.log("row : ", data);
            //console.log(event.target.id);            
            let rowObject = JSON.parse(JSON.stringify(data));
            if (event.target.id == "dcrView") {
               self.getDCRInfo(rowObject.dcrId);
            } 
           
          });
          return row;
        },
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search Records",
        },
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }
        ]
      };
      if (this.currentUser.company.isDivisionExist == false) {
        this.userDTOptions.columns[0].visible = false;
      }
      this.changeDetectedRef.detectChanges();
      this.userDataTable = $(this.dataTable1.nativeElement);
      this.userDataTable.DataTable(this.userDTOptions);
      this.changeDetectedRef.detectChanges();
    }
    }, err => {
      console.log(err)
      this.toastr.info("No Record Found.")
      // alert("Oooops! \nNo Record Found");
      this.changeDetectedRef.detectChanges();
    });
  }
  else if(this.data.type=="LeaveView"){
    let userId = this.data.obj._id.submitBy;
    let fromDate = this.dcrForm.value.fromDate;
    let toDate = this.dcrForm.value.toDate;

    this._dcrReportsService.getUserDateWiseDCRDetail(userId, fromDate, toDate).subscribe(result => {
      this.dataSources = result;
      if ( result == undefined) {
        this.showReport1 = false;
        this.showProcessing = false;
        this.toastr.info("No Record Found.")

      } else{
        let res=[];
         for(var i=0;i<result.length;i++){
        if(result[i].workingStatus=="leave"){
          res.push(result[i])
        }
      }
      if(res.length==0){
        this.toastr.info("No Record Found.")
        this.showProcessing = false;
        this.showReport1 = false;


      }else{

        this.showReport1 = true;
        this.showProcessing = false;
        this.userDTOptions = {
        "pagingType": 'full_numbers',
        "paging": true,
        "ordering": true,
        "info": true,
        "scrollY": 300,
        "scrollX": true,
        "fixedColumns": true,
        destroy: true,
        data: res,
        responsive: true,
        columns: [
          {
            title: 'Division',
            data: 'divisionName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Date',
            data: 'dcrDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                var date= moment(data).format('DD-MM-YYYY')
                 return `<div style="white-space:normal;width:100px;"> ${date} </div>`
               }
            },
            defaultContent: '---'
          }, {
            title: 'Name',
            data: 'userName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }
          , {
            title: "Working Status" ,
            data: 'workingStatus',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                if(data!="leave")
                {
                  return '--'
                }
                else{
                  return data
                }
              }
            },
            defaultContent: '---'
          }, {
            title: 'Remarks',
            data: 'remarks',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }

        ],
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search Records",
        },
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }
        ]
      };
      if (this.currentUser.company.isDivisionExist == false) {
        this.userDTOptions.columns[0].visible = false;
      }
      this.changeDetectedRef.detectChanges();
      this.userDataTable = $(this.dataTable1.nativeElement);
      this.userDataTable.DataTable(this.userDTOptions);
      this.changeDetectedRef.detectChanges();
    }
    }
    }, err => {
      console.log(err)
      this.toastr.info("No Record Found.")
      // alert("Oooops! \nNo Record Found");
      this.changeDetectedRef.detectChanges();
    });

  }
}
}
