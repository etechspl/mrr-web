import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption, MatSelect, MatSlideToggle, MatSlideToggleChange } from '@angular/material';
import { DivisionService } from '../../../../../core/services/Division/division.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { StateComponent } from '../../filters/state/state.component';
import { TypeComponent } from '../../filters/type/type.component';

@Component({
  selector: 'm-generate-excel-on-selection',
  templateUrl: './generate-excel-on-selection.component.html',
  styleUrls: ['./generate-excel-on-selection.component.scss']
})
export class GenerateExcelOnSelectionComponent implements OnInit {

  isToggled: boolean = true;
  showState: boolean = true;
  showDistrict: boolean = false;
  showReports: boolean = false;
  showEmployee: boolean = false;
  showProcessing: boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isShowDivision: boolean = false;
  divisionData: any
  isDivisionExist = this.currentUser.company.isDivisionExist;
  
  constructor(
    private _divisionService: DivisionService
  ) { }

  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent")
  callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent")
  callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent")
  callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent : DesignationComponent
  

  excelForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    division: new FormControl(null),
    state: new FormControl(null),
    headquarter: new FormControl(null),
    designation : new FormControl(null),
    employeeDetails: new FormControl(null),
    fromDate: new FormControl(null, Validators.required),
    toDate: new FormControl(null, Validators.required)
  })

  ngOnInit() {
    console.log(this.currentUser);

    if (this.isDivisionExist == true) {
      this.isShowDivision = true;
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel,
      };
      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this._divisionService.getDivisions(obj).subscribe(res => {
        if (res.length > 0) {
          res[0].divisionObj.sort(function (a, b) {
            var textA = a.divisionName.toUpperCase();
            var textB = b.divisionName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
          this.divisionData = res[0].divisionObj;
        }
      });
    } else {
      this.isShowDivision = false;
    }

    if (
      this.currentUser.userInfo[0].designationLevel == 0 ||
      this.currentUser.userInfo[0].designationLevel > 1
    ) {
      //For Admin And MGR
      //this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Geographical",
        "WithHeadquarter"
      );
    }
  }

  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }



  getReportType(val) {
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Geographical",
        "WithHeadquarter"
      );
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Hierarachy",
        "empWise"
      );
    }
  }

  getTypeValue(val) {
    this.excelForm.patchValue({type:val})
    if (val == 'State') {
      this.showDistrict = false;
      this.showState =true;
      this.showEmployee =false;
    } else if (val == 'Headquarter') {
      this.callDistrictComponent.setBlank();
      this.callStateComponent.setBlank();
      this.showDistrict = true;
      this.showState = true;
      this.showEmployee =false;
    } else if (val == 'Employee Wise') {
      // this.callDesignationComponent.setBlank();
      // this.callEmployeeComponent.setBlank();
      this.showDistrict = false;
      this.showState = false;
      this.showEmployee = true;
    }
  }

  getDivisionValue(val) {
    this.excelForm.patchValue({ division: val })
    this.callEmployeeComponent.setBlank();
    if (this.isDivisionExist === true) {
      if (
        this.excelForm.value.type === "State" ||
        this.excelForm.value.type === "Headquarter"
      ) {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.excelForm.value.division,
          designationLevel: this.currentUser.userInfo[0]
            .designationLevel,
        });
      }
    }
    if (this.excelForm.value.type === "Employee Wise") {     
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    }
  }

  getStateValue(val) {
    this.excelForm.patchValue({state:val})
    if (this.isDivisionExist === true) {
			if (this.excelForm.value.type == "Headquarter") { 
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.currentUser.userInfo[0].designationLevel,
					companyId: this.currentUser.companyId,
					stateId: this.excelForm.value.state,
					isDivisionExist: this.isDivisionExist,
					division: this.excelForm.value.division,
				});
			}
		} else {
			if (this.excelForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
  }

  getDistrictValue(val) {
    this.excelForm.patchValue({headquarter:val})
  }

  getFromDate(val) {
    this.excelForm.patchValue({fromDate:val})
  }

  getToDate(val) {
    this.excelForm.patchValue({toDate:val})
  }

  getDesignation(val){
    this.excelForm.patchValue({designation:val})
    if (
        this.excelForm.value.division != null &&
        this.excelForm.value.designation != null
      ) {
        if (this.excelForm.value.designation.length > 0) {
          this.callEmployeeComponent.getEmployeeListBasedOnDivision(
            {
              companyId: this.currentUser.companyId,
              division: this.excelForm.value.division,
              status: [true],
              designationObject: this.excelForm.value
                .designation,
            }
          );
        }
      }
  }

  getEmployeeValue(val){
    this.excelForm.patchValue({employeeDetails : val})
  }

  viewRecords() {
    console.log(JSON.stringify(this.excelForm.value));
    
  }
  allSelected: Boolean = false;
  @ViewChild('select') select: MatSelect;
  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }
  }
}
