import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateExcelOnSelectionComponent } from './generate-excel-on-selection.component';

describe('GenerateExcelOnSelectionComponent', () => {
  let component: GenerateExcelOnSelectionComponent;
  let fixture: ComponentFixture<GenerateExcelOnSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateExcelOnSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateExcelOnSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
