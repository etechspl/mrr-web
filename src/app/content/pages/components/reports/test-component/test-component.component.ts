import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FileHandlingService } from '../../../../../core/services/FileHandling/file-handling.service';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'm-test-component',
  templateUrl: './test-component.component.html',
  styleUrls: ['./test-component.component.scss']
})
export class TestComponentComponent implements OnInit {

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _fileHandlingService: FileHandlingService
  ) {
    console.clear()
  }

  ngOnInit() {
  }


  selectedFile;
  imageToShow = new Array<string>();
  currentUser = JSON.parse(sessionStorage.currentUser);

  selectFile(event) {
    console.log(event);

    //this.selectedFile = <File>event.target.files[0];
     this.selectedFile = event.target.files;


    //Show Image Preview
    if (this.selectedFile) {
      for (const file of this.selectedFile) {
        let reader = new FileReader();
        reader.onload = (event: any) => {
          this.imageToShow.push(event.target.result)
          this._changeDetectorRef.detectChanges();
        }
        reader.readAsDataURL(file)
      }
    }
  }

  methodToSameFileSelected(event) {
    //Method to delete same file from source which is previsioly seleccted.
    //event.target.value = '';
  }


  updateUserImage() {
    this._fileHandlingService.uploadUserImage(this.currentUser.companyId, this.currentUser.id, "MailAttachments", this.selectedFile).subscribe(events => {
      if (events.type === HttpEventType.UploadProgress) {
        console.log("Uploaded : " + Math.round(events.loaded / events.total * 100) + "%");
        //this.imageProgress = Math.round(events.loaded / events.total * 100);
        this._changeDetectorRef.detectChanges();
      } else if (events.type === HttpEventType.Response) {
        //this.imageUrl = './assets/app/media/img/users/default-user.png';
        this._changeDetectorRef.detectChanges();
      }
    },
      res => {
        console.log(res)
      }
    )
  }

}
