import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductStatewiseSaleComponent } from './product-statewise-sale.component';

describe('ProductStatewiseSaleComponent', () => {
  let component: ProductStatewiseSaleComponent;
  let fixture: ComponentFixture<ProductStatewiseSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductStatewiseSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductStatewiseSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
