import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StpCreationComponent } from './stp-creation.component';

describe('StpCreationComponent', () => {
  let component: StpCreationComponent;
  let fixture: ComponentFixture<StpCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StpCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StpCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
