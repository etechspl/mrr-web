import { ChangeDetectorRef, Input, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatInput, MatSelect, MatSlideToggleChange, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ToastrService } from 'ngx-toastr';
import { STPService } from '../../../../../core/services/stp.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EditStpComponent } from '../../master/edit-stp/edit-stp.component';
import { StpApprovalService } from '../../../../../core/services/stp-approval.service';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { FareRateCardService } from '../../../../../core/services/FareRateCard/fare-rate-card.service';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';



@Component({
  selector: 'm-stp-creation',
  templateUrl: './stp-creation.component.html',
  styleUrls: ['./stp-creation.component.scss']
})
export class StpCreationComponent implements OnInit {

  dataArraySection: FormGroup;
  stpForm: FormGroup;

  /*
   stpData: this.fb.array([this.fb.group({
            FromArea: new FormControl(),
            toArea: new FormControl(),
            station: new FormControl(),
            distance : new FormControl()
          })]),
   */

  constructor(private fb: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private stpApprovalService: StpApprovalService,
    private _userDetailService: UserDetailsService,
    private userAreaMapping: UserAreaMappingService,
    private _toastr: ToastrService,
    private _stpService: STPService,
    private dialog: MatDialog,
    public hierarchyService: HierarchyService,
    private _fareRateCardService: FareRateCardService,
  ) {
    this.stpForm = this.fb.group({
      division: new FormControl(null),
      employeeName: new FormControl(null, Validators.required),
      designation: new FormControl(null, Validators.required),
      fromArea: new FormControl(null, Validators.required),
      toArea: new FormControl(null, Validators.required),
      station: new FormControl(null, Validators.required),
      distance: new FormControl(null, Validators.required),
      frequencyVisit: new FormControl(null),
      frc: new FormControl(null, Validators.required)
    })



    this.dataArraySection = this.fb.group({
      data1: this.fb.array([])
    });
  }


  public serchText: FormControl = new FormControl();
  protected _onDestroy = new Subject<void>();

  @ViewChild("callDistrictComponent")
  callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent")
  callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent")
  callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild('areaFrom') areaFrom: MatSelect;
  @ViewChild('areaTo') areaTo: MatSelect;
  @ViewChild('Stype') Stype: MatSelect;
  @ViewChild('singleSelect') singleSelect: MatSelect;

  @Input() data: any;

  currentUser = JSON.parse(sessionStorage.currentUser);
  showDivisionFilter: boolean = false;
  isToggled: boolean = true;
  areas = [];
  type = ['EX', 'HQ', 'OUT']
  fromArea: any;
  displayedColumns = ['select', 'sn', 'fromArea', 'toArea', 'type', 'distance', 'fVisit', 'edit'];
  displayedColumns1 = ['sn', 'eName', 'fromArea', 'toArea', 'type', 'distance', 'fVisit', 'edit', 'addStp']
  toArea: any;
  dataSource: MatTableDataSource<any>;
  selection1 = new SelectionModel<any>(true, []);
  selectedItems = [];
  selectedItems1 = [];
  selection = new SelectionModel<any>(true, []);

  itemsToBeSelected = [];
  itemsToBeSelected1 = [];
  inputValue = '';
  dialogConfig = new MatDialogConfig();
  dialogRef: any = null;
  employee: any;
  filteredData = new ReplaySubject(1);
  userIds: any;
  searchText;
  EmployeeFilter: boolean = false;
  dataSource2: MatTableDataSource<any>;
  managerTable: boolean = false;
  frcData: any;
  stpTable: boolean = false;

  searchedList: any;



  ngOnInit() {
    //console.log('currentUser', this.currentUser);

    if (this.currentUser.companyId == '606be85e215053124c448cb9') {
      this.EmployeeFilter = true;
    }

    if (this.currentUser.company.isDivisionExist == true) {
      this.showDivisionFilter = true;
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel,
      };
      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);
    } else {
      this.showDivisionFilter = false;
    }


  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ////Search Operation comparisoin
  setInitialValue() {
    this.filteredData.pipe(take(1), takeUntil(this._onDestroy)).subscribe(() => {
      this.singleSelect.compareWith = (a, b) => a && b && a.name === b.name;
    })
  }

  //Search Operation
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }


  applyFilter(eventState: Event) {
    console.log(eventState);
  }


  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  getDivisionValue(val) {
    this.stpForm.patchValue({ division: val })
    let param={
      companyId:this.currentUser.companyId,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      userId : this.currentUser.id,
      division: [val],
    };
    this.callDesignationComponent.setBlank();
    if (this.currentUser.company.isDivisionExist==true) {
      
      console.log(JSON.stringify(param));
      
      this.callDesignationComponent.getDesignationBasedOnDivision(param);
    } else {
      this.callDesignationComponent.getDesignation(this.currentUser.companyId);
    }
  }

  getDesignation(val) {
    this.isToggled = true;
    if (val.designationLevel == 1) {
      this.managerTable = false;
    }
    this.stpForm.patchValue({ designation: val });
    //this.callEmployeeComponent.setBlank();
    let obj = {}
    if (this.currentUser.company.isDivisionExist == true) {
      obj = {
        companyId: this.currentUser.companyId,
        division: this.stpForm.value.division,
        designation: val.designationLevel
      }
      this._userDetailService.getEmployeeOnDivision(obj).subscribe(res => {
        if (res == null || res == undefined) {
          this._toastr.error('no Data Found');
        } else {
          this.employee = res;
          this.filteredData.next(this.employee.slice());    //Search Operation
          this.serchText.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {    //Search Operation
            this.filterEmployee();    //Search Operation
          })
          this._changeDetectorRef.detectChanges();
        }

      })
    } else {
      this._userDetailService
        .getUserInfoBasedOnDesignation(this.currentUser.companyId, val.designationLevel, true)
        .subscribe(
          (res) => {
            this.employee = res;
            //this.filteredData.next(this.employee.slice());
            

            this._changeDetectorRef.detectChanges();
          },
          (err) => {
            console.log(err);
          }
        );
    }

  }


  //Search Operation
  protected filterEmployee() {
    if (!this.employee) {
      return;
    }
    let search = this.serchText.value;
    if (!search) {
      this.filteredData.next(this.employee.slice());
      return;

    } else {
      search = search.toLowerCase();
    }
    this.filteredData.next(
      this.employee.filter(v => v.name.toLowerCase().indexOf(search) > -1)
    );

  }

  getEmployeeValue(val) {
    this.stpForm.patchValue({ employeeName: val.userId })
    this.getUserMappedAreas(val.userId);
    if (this.stpForm.value.designation.designationLevel > 1) {
      this.generateReport();
      this.managerData();
      //this.managerTable = true;
    } else {
      this.generateReport();
      this.managerTable = false;
    }


    let obj = {
      companyId: this.currentUser.companyId,
      userId: val.userId,
    }
    this._stpService.getUserInfo(obj).subscribe(result => {
      let where = {
      }
      //console.log(result);

      if (this.currentUser.company.expense.daType === "category wise") {
        where["daCategory"] = result[0]["daCategory"];
        where["companyId"] = result[0]["companyId,"],
          where["type"] = result[0]["type"]
      }
      if (this.currentUser.company.expense.daType === "Designation-State Wise") {
        where["designation"] = result[0]["designation"],
          where["companyId"] = result[0]["companyId"],
          where["type"] = result[0]["type"]
      }
      if (this.currentUser.company.isDivisionExist === true) {
        where["isDivisionExist"] = true;
        where["divisionId"] = result[0]["divisionId"];
      }
      if (this.currentUser.company.expense.daType === 'employee wise') {
        where["userId"] = result[0].userId;
        where["companyId"] = result[0]["companyId"];
        where["frcType"] = this.currentUser.company.expense.daType;
      }
      //console.log('fgfhhgfgfgfgffhgfhgf', where);

      this._fareRateCardService.getFareRateCardDetails(where).subscribe(res => {
        if (res.length === 0 || res === null || res === undefined) {
          this._toastr.error('No FRC Data')
        } else {
          //console.log('DZ res',res);
          this.frcData = res;
        }
      })
    })
  }

  addMoreData() {
    // (<FormArray>this.dataArraySection.get('data1')).push(this.addSkillFromGroup());
    //console.log("Index Data =>", val);
    const add = this.dataArraySection.get('data1') as FormArray;
    add.push(this.fb.group({
      fromArea: new FormControl(null, [Validators.required]),
      toArea: new FormControl(null, [Validators.required]),
      station: new FormControl(null, [Validators.required]),
      distance: new FormControl(null, [Validators.required])
    }))
    console.log("Index Data =>", this.dataArraySection.value);
    //console.log(this.stpForm.value);

    (!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
  }

  deleteData(index: number) {
    const add = this.dataArraySection.get('data1') as FormArray;
    add.removeAt(index);
    (!this._changeDetectorRef['destroyed']) ? this._changeDetectorRef.detectChanges() : null;
  }

  getUserMappedAreas(userId) {
    this.userAreaMapping.getMappedAreas(this.currentUser.companyId, userId).subscribe(res => {
      this.areas = res;
    });
  }

  getFromArea(val) {
    //console.log(val);

    this.stpForm.patchValue({ fromArea: val.areaId })
    this.fromArea = val
  }
  stpFilter: boolean = false;
  getToArea(val) {
    this.stpForm.patchValue({ toArea: val.areaId })
    this.toArea = val;

    if (this.dataSource != null) {
      this.dataSource.data.forEach(element => {
        if ((element.fromArea.id == this.stpForm.value.fromArea) && (element.toArea.id == val.areaId)) {
          this._toastr.info('STP already Exist');
          this.stpFilter = true;
        }
      });
    }   
  }

  viewRecords() {
    if (this.stpFilter == true) {
      this._toastr.error('STP Data Already Present for Given User');
      this.stpForm.controls['distance'].reset();
      this.stpForm.controls['frequencyVisit'].reset();
      this._changeDetectorRef.detectChanges();
    } else {
      this.isToggled = false;
      let obj = {};
      let date = new Date();
      obj['companyId'] = this.currentUser.companyId
      obj['userId'] = this.stpForm.value.employeeName;
      obj['fromArea'] = this.stpForm.value.fromArea;
      obj['toArea'] = this.stpForm.value.toArea;
      obj['distance'] = this.stpForm.value.distance;
      obj['type'] = this.stpForm.value.station;
      obj['createdAt'] = date;
      obj['updatedAt'] = date;
      obj['appByMgr'] = '';
      obj['mgrAppDate'] = date;
      obj['mgrDelDate'] = date;
      obj['finalDelDate'] = date;
      obj['appByAdmin'] = '';
      obj['finalAppDate'] = date;
      obj['status'] = true;
      obj['appStatus'] = 'approved';
      obj['freqVisit'] = parseInt(this.stpForm.value.frequencyVisit);
      obj['stateId'] = this.fromArea.stateId;
      obj['districtId'] = this.toArea.districtId;
      obj['frcId'] = this.stpForm.value.frc;


      this._stpService.createSTPs(obj).subscribe(res => {
        if (res == null || res.length == 0 || res == undefined) {
          this.isToggled = true;
          this._toastr.error('Something went wrong...')
        }
        //console.log(res);
        this._toastr.success('STP created');

        this.generateReport(); // this will control the stp table
      });
    }
  }

  generateReport() {

    //THIS METHOD is only responsible for displaying STP of employee selected in the filter

    let param = {
    };
    param["companyId"] = this.currentUser.companyId;
    param["userId"] = this.stpForm.value.employeeName;
    console.log(param);

    this._stpService.getSTPDetails(param).subscribe(res => {
      if (res.length == 0 || res == null || res == undefined) {
        this.isToggled = true;
        this.stpTable = false;
        this._toastr.info("STP Data is not available for selected Employee");
        this._changeDetectorRef.detectChanges();
      } else {
        this.isToggled = false;
        this.stpTable = true;
        let array = res.map(item => {
          return {
            ...item
          };
        });

        array.sort(function (a, b) {
          if (a.fromArea.areaName < b.fromArea.areaName) { return -1; }
          if (a.fromArea.areaName > b.fromArea.areaName) { return 1; }

          return 0;
        });

        if (array.length > 0) {
          //console.log(array);

          this.dataSource = new MatTableDataSource(array);
          //console.log(this.dataSource);

          this.itemsToBeSelected = array;
          this.stpForm.controls['distance'].reset();
          this.stpForm.controls['frequencyVisit'].reset();
          this._changeDetectorRef.detectChanges();
        }
      }
    })

  }

  disableButton: boolean = false;
  addManagerSTP(event, i, row) {
    // console.log(row);

    let obj = {};
    let date = new Date();
    obj['companyId'] = this.currentUser.companyId
    obj['userId'] = this.stpForm.value.employeeName;
    obj['fromArea'] = row.fromArea.id;
    obj['toArea'] = row.toArea.id;
    obj['distance'] = row.distance;
    obj['type'] = row.type;
    obj['createdAt'] = date;
    obj['updatedAt'] = date;
    obj['appByMgr'] = '';
    obj['mgrAppDate'] = date;
    obj['mgrDelDate'] = date;
    obj['finalDelDate'] = date;
    obj['appByAdmin'] = '';
    obj['finalAppDate'] = date;
    obj['status'] = true;
    obj['appStatus'] = 'approved';
    obj['freqVisit'] = row.freqVisit;
    obj['stateId'] = row.fromArea.stateId;
    obj['districtId'] = row.toArea.districtId;
    obj['frcId'] = row.frcId;

    this._stpService.createSTPs(obj).subscribe(res => {
      if (res == null || res.length == 0 || res == undefined) {
        this._toastr.error('Something went wrong...')
      } else {
        //console.log(res);
        row.disableButton = !row.disableButton;
        this._toastr.success('STP created');
        this.generateReport();
        this._changeDetectorRef.detectChanges()
      }
    })
  }

  managerData() {

    //METHOD TO GET STP OF LOWER HIERARCHY OF MANAGER


    //this.managerTable = true;
    var object = {
      companyId: this.currentUser.companyId,
      supervisorId: this.stpForm.value.employeeName,
      type: 'lower'
    };
    let resultArr = [];
    let empData = []
    let param = {
      companyId: this.currentUser.companyId,
    }
    this.hierarchyService.getManagerHierarchy(object).subscribe(async res => {
      let i = 0;


      res.forEach(element => {
        if (element.designationLevel == (this.stpForm.value.designation.designationLevel - 1) && element.designationLevel > 0) {
          resultArr[i] = element;
          i += 1;
        }
      });
      i = 0;
      let j = 0;

      for (let stp = 0; stp < resultArr.length; stp++) {
        param["userId"] = resultArr[i].userId;
        const stpData = await this.getStps(param);
        i += 1;
        stpData.forEach(element => {
          empData[j] = element;
          j += 1;
        })

      }

      if (empData.length == 0) {
        this.managerTable = false;
        this._toastr.info('No Team STP Found');
        //this.isToggled = true;

      } else {
        this.isToggled = false;
        this.managerTable = true;
        this.dataSource2 = new MatTableDataSource(empData);
      }
      //console.log(this.dataSource2);

      this._changeDetectorRef.detectChanges()
    })
  }
  getStps(param): Promise<any> {
    return new Promise((resolve, reject) => {
      this._stpService.getSTPDetails(param).subscribe(res => {
        if (!Boolean(res)) reject();
        else resolve(res);
      })
    })
  }

  getSelectionValue($event, i: number, row: any) {
    let id = row.id;
    if (this.selectedItems.indexOf(id) < 0) {
      this.selectedItems.push(id);
    } else if (this.selectedItems.indexOf(id) >= 0) {
      this.selectedItems.splice(this.selectedItems.indexOf(id), 1);
    }
  }

  isAllSelected() {
    const numSelected = this.selectedItems.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedItems = [];
    } else {
      this.selectedItems = [];
      this.itemsToBeSelected.forEach((row, index) => {
        this.selection.select(row);
        this.selectedItems.push(row.id);
      })

    }
  }

  isAllSelected1() {
    const numSelected1 = this.selection1.selected.length;
    const numRows1 = this.dataSource2.data.length;
    return numSelected1 === numRows1;
  }

  masterToggle1() {
    this.isAllSelected1()
      ? this.selection1.clear()
      : this.dataSource2.data.forEach(row => this.selection1.select(row));

      console.log(this.selection1.selected.length);
      
  }

  editStp(event, i, row) {
    //console.log('Gourav', row)
    this.dialogConfig.data = row;

    (this.dialogRef === null) ? (this.dialogRef = this.dialog.open(EditStpComponent, this.dialogConfig)) : null;
    this.dialogRef.afterClosed().subscribe((res) => {

      if (res) {
        this.generateReport();
      }
      this.dialogRef = null;
    });

  }

  editStpManager(event, i, row) {
    console.log(row);
    this.dialogConfig.data = row;
    this.dialogConfig.data['managerStatus'] = true;
    this.dialogConfig.data['managerData'] = this.stpForm.value;

    (this.dialogRef === null) ? (this.dialogRef = this.dialog.open(EditStpComponent, this.dialogConfig)) : null;
    this.dialogRef.afterClosed().subscribe((res) => {

      if (res) {
        this.managerData();
        this.generateReport();
      }
      this.dialogRef = null;
    });


  }

  deleteSTP() {

    this.stpApprovalService.deleteSTPApprovalReq(this.selectedItems).subscribe(res => {
      if (JSON.parse(res.result).count > 0) {
        this._toastr.success(' Selected STP deleted successfully.');
        this.generateReport();
      } else {
        this._toastr.error(' Something went wrong.');
      }
    })
  }

}
