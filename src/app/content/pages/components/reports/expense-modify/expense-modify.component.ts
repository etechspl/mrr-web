import {
	Component,
	OnInit,
	Inject,
	ViewChild,
	ChangeDetectorRef,
} from "@angular/core";
import {
	FormGroup,
	FormBuilder,
	FormControl,
	Validators,
	FormArray,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ExpenseClaimedService } from "../../../../../core/services/expense-claimed.service";
import { ToastrService } from "ngx-toastr";
import { FinalizeExpenseComponent } from "../finalize-expense/finalize-expense.component";
import { ExpenseHeadService } from "../../../../../core/services/ExpensesHead/expense-head.service";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";

@Component({
	selector: "m-expense-modify",
	templateUrl: "./expense-modify.component.html",
	styleUrls: ["./expense-modify.component.scss"],
})
export class ExpenseModifyComponent implements OnInit {
	constructor(
		private dialogRef: MatDialogRef<ExpenseModifyComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private expenseClaimedService: ExpenseClaimedService,
		private fb: FormBuilder,
		private toastr: ToastrService,
		private formBuilder: FormBuilder,
		private expenseHeadService: ExpenseHeadService,
		private changeDetectorRef: ChangeDetectorRef,
		private dcrReportsService: DcrReportsService
	) {}
	expenseHead: any;
	visitedBlock: any;
	currentUser = JSON.parse(sessionStorage.currentUser);
	divData : boolean = false;
	ngOnInit() {
		console.log(this.data);
		
		if(this.currentUser.companyId=='5cd162dd50ce3f0f80f64e14'){
			this.divData=true;
		}
		this.createExpenseModifyForm();
		this.getExpensesHead();
		this.changeDetectorRef.detectChanges();
	}
	details = [];
	isFormLoading: boolean = true;
	getExpensesHead() {
		let where = {
			id: this.data.element.dcrId,
		};
		if(this.currentUser.companyId === '606be85e215053124c448cb9'){
			this.visitedBlock = this.data.element.visitedAreaAsPerProviderVisit;
		}else{
			this.visitedBlock = this.data.element.visitedBlock;
		}
		

		// const visitedBlockFormArray = this.visitedBlock.map(i=>this.createItem())

		const arrayForm = this.expenseModifyForm.get(
			"visitedBlock"
		) as FormArray;
			console.log(this.visitedBlock)
		this.visitedBlock.forEach((element, i) => {
			this.dcrReportsService
				.getSTPDetails({
					where: {
						companyId: this.data.element.companyId,
						userId: this.data.element.userId,
						appStatus: "approved",
						status: true,
						or: [
							{
								fromArea: element.fromId,
								toArea: element.toId,
							},
							{
								fromArea: element.toId,
								toArea: element.fromId,
							},
							
						]
					},
					include: [
						{
							relation: "frc",
							scope: {
								rate: true,
							},
						}
						// {
						// 	relation:"fromArea",
						// 	scope:{
						// 		fields:{
						// 			"areaName":true
						// 		}
						// 	}
						// },
						// {
						// 	relation:"toArea",
						// 	scope:{
						// 		fields:{
						// 			"areaName":true
						// 		}
						// 	}
						// }
						
					],
					
				})
				.subscribe((res) => {
					console.log('responseData',res);
					
					if (res.length > 0) {
						element["distance"] = res[0].distance;
						element["rate"] = res[0].frc.rate;
						element["fare"] = res[0].distance * res[0].frc.rate;
					} else {
						element["distance"] = 0;
						element["rate"] = 0;
						element["fare"] = 0;
					}
					arrayForm.push(this.createItem(element));
					this.changeDetectorRef.detectChanges();

				});
		});

		this.dcrReportsService
			.getDCRCopy(this.data.element.dcrId)
			.subscribe((res) => {
				this.expenseHead = res[0];
				if (this.expenseHead.dcrExpense) {
					this.details = this.expenseHead.dcrExpense.map((item) => {
						this.expenseModifyForm.setControl(
							item.expenseType.replace(" ", ""),
							new FormControl(item.amount)
						);
						return {
							key: item.expenseType.replace(" ", ""),
							label: item.expenseType,
						};
					});
				}
			});
	}
	createItem(value: any): FormGroup {
		if(this.currentUser.companyId === '606be85e215053124c448cb9'){
			return this.formBuilder.group({
				distanceMgr: new FormControl(value.distance),
				fareMgr: new FormControl(value.fare),
				rateMgr: new FormControl(value.rate),
				fromArea: new FormControl(value.fromArea),
				toArea: new FormControl(value.toArea),
				
			});
		}else{
			return this.formBuilder.group({
				distanceMgr: new FormControl(value.distance),
				fareMgr: new FormControl(value.fare),
				rateMgr: new FormControl(value.rate),
				fromArea: new FormControl(value.from),
				toArea: new FormControl(value.to),
				
			});
		}
	}

	@ViewChild("finalizeExpenseComponent")
	finalizeExpenseComponent: FinalizeExpenseComponent;
	

	expenseModifyForm: FormGroup;
	getBlockData: FormGroup;
	checkValue: boolean = false;
	createExpenseModifyForm() {
		if (this.currentUser.userInfo[0].rL > 1) {
			this.expenseModifyForm = this.fb.group({
				id: new FormControl(this.data.element.id),
				daMgr: new FormControl(
					this.data.element.daMgr,
					Validators.required
				),
				miscExpenseMgr: new FormControl(
					this.data.element.miscExpenseMgr,
					Validators.required
				),
				// distanceMgr: new FormControl(this.data.element.distanceMgr),
				fareMgr: new FormControl(this.data.element.fareAdmin),
				remarksMgr: new FormControl(
					this.data.element.remarksMgr,
					Validators.required
				),
				sumExpMgr: new FormControl(
					this.data.element.sumExpMgr,
					Validators.required
				),
				subtExpeMgr: new FormControl(
					this.data.element.subtExpeMgr,
					Validators.required
				),
				visitedBlock: new FormArray([]),
				adminRate: new FormControl(
					this.data.element.adminRate
				),
			});
		} else if (this.currentUser.userInfo[0].rL == 0) {
			this.expenseModifyForm = this.fb.group({
				id: new FormControl(this.data.element.id),
				daMgr: new FormControl(
					this.data.element.daAdmin,
					Validators.required
				),
				fareMgr: new FormControl(this.data.element.fareAdmin),
				distanceMgr: new FormControl(this.data.element.distanceAdmin),
				miscExpenseMgr: new FormControl(
					this.data.element.miscExpenseAdmin,
					Validators.required
				),
				remarksMgr: new FormControl(
					this.data.element.remarksAdmin,
					Validators.required
				),
				sumExpAdm: new FormControl(this.data.element.sumExpAdm),
				subtExpeAdm: new FormControl(this.data.element.subtExpeAdm),
				visitedBlock: new FormArray([]),
				adminRate: new FormControl(
					this.data.element.adminRate
				),
			});
		}
	}
	get VisitedBlockArray() {
		return this.expenseModifyForm.get("visitedBlock") as FormArray;
	}
	getVisitBlockData() {
		console.log("this.expenseModifyForm: ",this.expenseModifyForm.get("visitedBlock"));
		this.expenseModifyForm.get("visitedBlock") as FormArray;
		const modifiedBlock = this.expenseModifyForm.value.visitedBlock;
		let totalFare = 0;
		let totalDistance = 0;
		//if(this.expenseModifyForm.value.distanceMgr == 0){
		// 	totalFare += this.expenseModifyForm.value.distanceMgr * this.expenseModifyForm.value.adminRate,
		// 	totalDistance += this.expenseModifyForm.value.distanceMgr;

		// 	this.expenseModifyForm.patchValue({ distanceMgr: totalDistance });
		// this.expenseModifyForm.patchValue({ fareMgr: totalFare });
	// 	}else{
		modifiedBlock.forEach((element) => {
			//totalFare += element.distanceMgr * element.rateMgr;
			totalFare += element.distanceMgr * this.expenseModifyForm.value.adminRate,
			totalDistance += element.distanceMgr;
		});
		this.expenseModifyForm.patchValue({ distanceMgr: totalDistance });
		this.expenseModifyForm.patchValue({ fareMgr: totalFare });
	//}
		
	}



	// getDistance(event, i) {
	// 	this.totalDistance = this.totalDistance + parseInt(event.target.value);
	// 	console.log(parseInt(event.target.value), i);
	// 	this.expenseModifyForm.patchValue({ distanceMgr: this.totalDistance });
	// }

	submitForm() {
		if (this.expenseModifyForm.valid) {
			let obj = this.expenseModifyForm.value;
			obj["appBy"] = this.currentUser.id;
			obj["rL"] = this.currentUser.userInfo[0].rL;
			obj["userId"] = this.data.element.userId;
			obj["finalize"] = false;
			this.expenseClaimedService.modifyExpense(obj).subscribe(
				(res) => {
					if (res["count"] >= 1) {
						this.dialogRef.close({ data: "updated" });
						this.toastr.success("", "Modified Successfully");
					} else {
						this.dialogRef.close({ data: "updated" });
					}
				},
				(err) => {
					console.log(err);
				}
			);
		}
	}
}
