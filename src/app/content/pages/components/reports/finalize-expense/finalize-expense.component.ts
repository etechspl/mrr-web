import { DivisionComponent } from './../../filters/division/division.component';
import { Component, OnInit, ChangeDetectorRef, ViewChild, EventEmitter, Output, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ExpenseClaimedService } from '../../../../../core/services/expense-claimed.service';
import { ToastrService } from 'ngx-toastr';
import { TypeComponent } from '../../filters/type/type.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import * as _moment from 'moment';
import { MatSort, MatTable, MatPaginator, MatTableDataSource, MatDialog, MatSlideToggleChange } from '@angular/material';
import { ExpenseModifyComponent } from '../expense-modify/expense-modify.component';
import { DailyallowanceService } from '../../../_core/services/dailyallowance.service';
import { FixExpenseService } from '../../../../../core/services/FixExpense/fix-expense.service';
import { StateComponent } from '../../filters/state/state.component';
import { URLService } from '../../../../../core/services/URL/url.service';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { FileHandlingService } from '../../../../../core/services/FileHandling/file-handling.service';
import { ItemsList } from '@ng-select/ng-select/ng-select/items-list';
import { log } from 'console';
import * as XLSX from "xlsx";
@Component({
  selector: 'm-finalize-expense',
  templateUrl: './finalize-expense.component.html',
  styleUrls: ['./finalize-expense.component.scss']
})
export class FinalizeExpenseComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
    private expenseClaimedService: ExpenseClaimedService,
    private changeDetectorRef: ChangeDetectorRef,
    private dailyallowanceService: DailyallowanceService,
    private fixExpenseService: FixExpenseService,
    private toastr: ToastrService,
    private exportAsService: ExportAsService,
    private _urlService: URLService,
    private _fileHandlingService: FileHandlingService,
  ) { }

  public isDisableButton: boolean = true;
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showDesignation: boolean = false;
  public showEmployee: boolean = false;
  public color: string;
  public isShowReport: boolean = false;
  public hideColumn: boolean = false;
  public hideachievement;
  public deductionAmount = 0;
  public IncrementAmount = 0;
  public IncrementRemarks = "";
  public NetRemarks = "";
  public MobileRemarks = "";
  public deductionRemarks = "";
  public achievementsAmount = 0;
  public achievementsRemarks = "";
  public TotalRemarks = "";
  showProcessing: boolean = false;
  @ViewChild("MatTable") table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('paginator1') paginator1: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  dataSource: any;
  dataSource2: any;
  finalizeExpnesForm: FormGroup;

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1' // the id of html/table element
  }

  exportAsConfig2: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element

  }

  exportAsConfig3: ExportAsConfig = {
    type: 'pdf', // the type you want to download
    elementId: 'tableId2', // the id of html/table element

  }

  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @Output() reRender: EventEmitter<null> = new EventEmitter();
  @HostListener('click')
  click() {
    this.reRender.emit();
  }
  isShowColumn: boolean = false;

  obj = {};
  isShowReportExpense = false;
  isShowDivision = false;
  displayedColumns = [];
  displayedColumns2 = ['sn', 'name', 'desig', 'appByMgr', 'appByAdm', 'da', 'daMgr', 'daAdmin', 'fare', 'fareMgr', 'fareAdmin', 'miscExpense', 'miscExpenseMgr', 'miscExpenseAdmin', 'totalExpense', 'totalExpenseMgr', 'totalExpenseAdmin', 'approveButton'];

  currentUser = JSON.parse(sessionStorage.currentUser);
  totalAdminExpense: any;
  COMPANY_ID;
  DA_TYPE = this.currentUser.company.daType;
  tablerowleave: any = [];
  tablerowWorking: any = [];
  hq: any = [];
  ex: any = [];
  out: any = [];
  workingDaysCount = 0;
  hqcount = 0;
  excount = 0;
  outcount = 0;
  totalExpenseCalculated = 0;
  totalExpenseCalculatedMgr = 0;
  totalExpenseCalculatedAdmin = 0;
  leaveDaysCount = 0;
  IS_DIVISION_EXIST;

  totalDistance;
  totalDistanceMgr;
  totalDistanceAdmin;


  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }




  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.currentUser);
    this.COMPANY_ID = this.currentUser.companyId;
    this.DA_TYPE = this.currentUser.company.expense.daType;
    this.IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
    this.createForm();
    if (this.currentUser.userInfo[0].rL > 1) {
      this.isShowColumn = false;
      this.displayedColumns = ['action', 'userName', 'dcrDate', 'workingStatus','plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'type', 'da', 'daMgr', 'fare', 'fareMgr', 'miscExpense', 'miscExpenseMgr', 'totalExpense', 'totalExpenseMgr', 'remarks', 'remarksMgr', 'jointWork', 'uploadPicture'];
    } else if (this.currentUser.userInfo[0].rL === 0) {
      this.isShowColumn = true;
      this.displayedColumns = ['action', 'userName', 'dcrDate', 'plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'distanceAdmin', 'type', 'da', 'daMgr', 'daAdmin', 'fare', 'fareMgr', 'fareAdmin', 'miscExpense', 'miscExpenseMgr', 'miscExpenseAdmin', 'totalExpense', 'totalExpenseMgr', 'totalExpenseAdmin', 'remarks', 'remarksMgr', 'remarksAdmin', 'jointWork', 'uploadPicture'];
    }

    if (this.currentUser.companyId == "5ec00cbf989fbc1a406eae93") {
      this.hideachievement = false;
    }
    else {
      this.hideachievement = true;
    }

    if (this.IS_DIVISION_EXIST === true) {
      this.isShowDivision = true;
      this.finalizeExpnesForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);


    } else {
      this.isShowDivision = false;
    }


  }

  createForm() {
    this.finalizeExpnesForm = this.fb.group({
      "reportType": new FormControl('Geographical'),
      "type": new FormControl('', Validators.required),
      "stateInfo": new FormControl(''),
      "districtInfo": new FormControl(''),
      "designation": new FormControl(''),
      "status": new FormControl(true),
      "employeeIds": new FormControl(''),
      "month": new FormControl('', Validators.required),
      "year": new FormControl('', Validators.required)
    })
    if (this.currentUser.userInfo[0].designationLevel === 0) { //For Admin
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    }
  }



  getReportType(reportType: string) {
    if (reportType === "Geographical") {
      if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

      } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      }
    } else {
      if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
        this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

      } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
        this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "onlyEmployeeWise");

      }
    }
    this.finalizeExpnesForm.patchValue({ reportType: reportType })

  }

  getTypeValue(val: string) {
    if (val.toLowerCase() == "state") {
      this.showState = true;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showEmployee = true;
      if (this.IS_DIVISION_EXIST === true) {
        this.callDivisionComponent.setBlank();
      }
      //add control
      this.finalizeExpnesForm.setControl('stateInfo', new FormControl('', Validators.required));
      //remove control

      this.finalizeExpnesForm.removeControl('districtInfo');
      this.finalizeExpnesForm.removeControl('designation');
    } else if (val.toLowerCase() == "headquarter") {
      this.showState = true;
      this.showDistrict = true;
      this.showDesignation = false;
      this.showEmployee = true;

      //add control
      this.finalizeExpnesForm.setControl('stateInfo', new FormControl('', Validators.required));
      this.finalizeExpnesForm.setControl('districtInfo', new FormControl('', Validators.required));
      //remove control
      this.finalizeExpnesForm.removeControl('designation');
    } else if (val.toLowerCase() == "employee wise") {
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = true;
      this.showEmployee = true;
      //add control
      this.finalizeExpnesForm.setControl('designation', new FormControl('', Validators.required))
      //remove control
      if (this.IS_DIVISION_EXIST === true) {
        this.callDivisionComponent.setBlank();
      }
      this.finalizeExpnesForm.removeControl('stateInfo');
      this.finalizeExpnesForm.removeControl('districtInfo');
    }
    this.finalizeExpnesForm.patchValue({ type: val });
  }

  getStateValue(stateInfo: any) {
    this.finalizeExpnesForm.patchValue({ stateInfo: stateInfo });
    if (this.finalizeExpnesForm.value.type.toLowerCase() === 'headquarter') {
      this.callDistrictComponent.getDistricts(this.currentUser.companyId, stateInfo, true);
    }
    //this.callEmployeeComponent.getEmployeeStateWiseOrDistrictWise({ companyId: this.currentUser.companyId, stateId: this.finalizeExpnesForm.value.stateInfo });
  }
  getDistrictValue(districtInfo: any) {
    this.finalizeExpnesForm.patchValue({ districtInfo: districtInfo });
    // this.callEmployeeComponent.getEmployeeStateWiseOrDistrictWise({ companyId: this.currentUser.companyId, stateId: this.finalizeExpnesForm.value.stateInfo, districtId: this.finalizeExpnesForm.value.districtInfo });

    //this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.finalizeExpnesForm.value.stateInfo, this.finalizeExpnesForm.value.districtInfo);
  }

  getDesignation(designation: any) {
    this.finalizeExpnesForm.patchValue({ designation: designation });
    //this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.finalizeExpnesForm.value.designation.designationLevel, this.finalizeExpnesForm.value.status);
  }

  getStatusValue(status: any) {



    this.finalizeExpnesForm.patchValue({ status: status });
    //this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.finalizeExpnesForm.value.designation.designationLevel, status);

  }
  getEmployeeValue(employeeId: any) {


    this.finalizeExpnesForm.patchValue({ employeeIds: employeeId });

  }
  getMonth(month: any) {
    this.finalizeExpnesForm.patchValue({ month: month });
  }
  getYear(year: any) {
    this.finalizeExpnesForm.patchValue({ year: year });
  }


  //setting division into dcrform
  getDivisionValue($event) {
    this.finalizeExpnesForm.patchValue({ divisionId: $event });
    if (this.IS_DIVISION_EXIST === true) {
      if (this.finalizeExpnesForm.value.type === "State" || this.finalizeExpnesForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.finalizeExpnesForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.finalizeExpnesForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.finalizeExpnesForm.value.divisionId != null && this.finalizeExpnesForm.value.designation != null) {
          if (this.finalizeExpnesForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.COMPANY_ID,
              division: this.finalizeExpnesForm.value.divisionId,
              status: [true],
              designationObject: this.finalizeExpnesForm.value.designation
            })
          }
        }
      }
    }
  }

  approvedByMgr: string;
  approvedByAdmin: string;
  checkApprovalStatus(item: any) {
    //for manager
    if (this.approvalCountMgr === 0) {
      if (item.hasOwnProperty("appByMgr") === true) {
        this.approvedByMgr = "Yes";
        this.approvalCountMgr = 1;
      } else {
        this.approvedByMgr = "No";
      }
    }


    // for admin
    if (this.approvalCountAdm === 1) {
      if (item.hasOwnProperty('appByAdm') === true) {
        this.approvalCountAdm = 1;
        this.approvedByAdmin = "Yes";
      } else {
        this.approvedByAdmin = "No";
      }
    }

  }

  //generate Report
  generatereport() {
    console.log("Gourav",this.finalizeExpnesForm.value)
    this.showProcessing = true;
    this.isToggled = false;
    this.isShowReport = false;
    if (this.finalizeExpnesForm.valid) {
      let month = this.finalizeExpnesForm.value.month;
      if (month < 10) {
        month = "0" + month
      }
      let date = this.finalizeExpnesForm.value.year + '-' + month ;
      let startDate = _moment.utc(new Date(date)).startOf('month').format('YYYY-MM-DD');
      let endDate = _moment.utc(new Date(date)).endOf('month').format('YYYY-MM-DD');
      if (this.IS_DIVISION_EXIST === true) {
        this.obj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.finalizeExpnesForm.value.divisionId,
          startDate: startDate,
          endDate: endDate,
          status: [this.finalizeExpnesForm.value.status],
          type: this.finalizeExpnesForm.value.type,
          rL: this.currentUser.userInfo[0].rL
        }
      } else {
        this.obj = {
          companyId: this.currentUser.companyId,
          startDate: startDate,
          endDate: endDate,
          status: [this.finalizeExpnesForm.value.status],
          type: this.finalizeExpnesForm.value.type,
          rL: this.currentUser.userInfo[0].rL
        }
      }

      if (this.currentUser.userInfo[0].rL > 1) {
        this.obj["supervisorId"] = [this.currentUser.id];
      }

      if (this.finalizeExpnesForm.value.type === 'State') {
        this.obj["stateId"] = this.finalizeExpnesForm.value.stateInfo;
      } else if (this.finalizeExpnesForm.value.type === 'Headquarter') {
        this.obj["stateId"] = this.finalizeExpnesForm.value.stateInfo;
        this.obj["districtIds"] = this.finalizeExpnesForm.value.districtInfo;
      } else if (this.finalizeExpnesForm.value.type === 'Employee Wise') {
        this.obj["designation"] = this.finalizeExpnesForm.value.designation.designation;
      }

      this.expenseClaimedService.getUserWiseExpense(this.obj).subscribe(res => {
        
        let array2 = [];
        array2 = res.map(item => {



          // let param = {
          //   companyId: this.COMPANY_ID,
          //   type: this.DA_TYPE,
          //   category: item.daCategory,
          //   isDivisionExist: this.IS_DIVISION_EXIST
          // };

          // if (this.IS_DIVISION_EXIST === true) {
          //   param["divisionId"] = item.divisionId[0]
          // }
          // let totalFixedExpense=0;

          // this.fixExpenseService.getFixExpense(param).subscribe(res=>{
          //   if(res.length>0){
          //     for (const fix of res) {
          //       totalFixedExpense+=fix.amount
          //     }
          //   }else{
          //     totalFixedExpense=0;
          //   }
          //   console.log(totalFixedExpense);

          // })

          // by nipun => for now (temporarily) we are calculating total expense at frontend 03-JUN--2020



          (item.da) ? (this.totalExpenseCalculated += item.da) : null;
          (item.fare) ? (this.totalExpenseCalculated += item.fare) : null;
          (item.miscExpense) ? (this.totalExpenseCalculated += item.miscExpense) : null;

          (item.daMgr) ? (this.totalExpenseCalculatedMgr += item.daMgr) : null;
          (item.fareMgr) ? (this.totalExpenseCalculatedMgr += item.fareMgr) : null;
          (item.miscExpenseMgr) ? (this.totalExpenseCalculatedMgr += item.miscExpenseMgr) : null;

        
          (item.totalExpenseAdmin) ? (this.totalExpenseCalculatedAdmin += item.totalExpenseAdmin) : null;
          //(item.miscExpenseAdmin) ? (this.totalExpenseCalculatedAdmin += item.miscExpenseAdmin) : null;


          return {
            color: "#4286f4",
            totalExpenseCalculated: this.totalExpenseCalculated,
            totalExpenseCalculatedMgr: this.totalExpenseCalculatedMgr,
            totalExpenseCalculatedAdmin: this.totalExpenseCalculatedAdmin,
            ...item
          };
          // by nipun  03-JUN--2020 END

        });
        if (array2.length > 0) {
          this.showProcessing = false;
          this.dataSource2 = new MatTableDataSource(res);
          this.dataSource2.sort = this.sort;
          setTimeout(() => {
            this.dataSource2.paginator = this.paginator2;
          }, 1000);
          this.isShowReportExpense = true;
        } else {
          this.showProcessing = false;
          this.toastr.info("Expense is not submitted for the selected  month & year", "Expense not Submitted");
          this.isShowReport = false;
          this.isShowReportExpense = false;
        }
        this.changeDetectorRef.detectChanges();
      })
    }
  }

  modifyExpense(element: any) {
    const dialogRef = this.dialog.open(ExpenseModifyComponent, {
      data: { element },
    });


    // Create subscription
    dialogRef.afterClosed().subscribe((res) => {
      if (res && res.data && res.data === 'updated') {

        this.generatereport();
        setTimeout(() => {
          this.expense(this.approveExpenseUserId);
        }, 1500);
      }
    });
  }

  finalizeExpense(userId: any) {
    let date = this.finalizeExpnesForm.value.year + '-' + this.finalizeExpnesForm.value.month;

    let startDate = _moment.utc(new Date(date)).startOf('month').format('YYYY-MM-DDT00:00:00.000Z');
    let endDate = _moment.utc(new Date(date)).endOf('month').format('YYYY-MM-DDT00:00:00.000Z');


    let obj = {
      finalize: true,
      appBy: this.currentUser.id,
      rL: this.currentUser.userInfo[0].rL,
      userId: userId,
      fromDate: startDate,
      endDate: endDate,
      appAdmDate: new Date()
    }


    this.expenseClaimedService.modifyExpense(obj).subscribe(res => {

      this.toastr.success("Expense Approved Successfully");
      this.generatereport();
      this.isDisableButton = false;
      this.isShowReport = false;
      this.IncrementAmount = null;
      this.deductionAmount = null;
      this.changeDetectorRef.detectChanges();
    }, err => {
      console.log(err);
    });
  }

  approvalCountMgr;
  approvalCountAdm;
  approveExpenseUserId: any;
  total: number = 0;
  totalDa;
  totalDaMgr;
  totalDaAdmin;
  totalFare;
  totalFareMgr;
  totalFareAdmin;
  totalMiscExpense;
  totalMiscExpenseMgr;
  totalMiscExpenseAdmin;
  totalExpense;
  totalExpenseMgr;
  totalExpenseAdmin;
  DAValue;


  expense(element: any) {

    this.total = 0;
    this.totalDistance = 0;
    this.totalDistanceMgr = 0;
    this.totalDistanceAdmin = 0;
    this.totalDa = 0;
    this.totalDaMgr = 0;
    this.totalDaAdmin = 0;
    this.totalFare = 0;
    this.totalFareMgr = 0;
    this.totalFareAdmin = 0;
    this.totalMiscExpense = 0;
    this.totalMiscExpenseMgr = 0;
    this.totalMiscExpenseAdmin = 0;
    this.totalExpense = 0;
    this.totalExpenseMgr = 0;
    this.totalExpenseAdmin = 0;


    let params = this.obj;
    this.approveExpenseUserId = element;
    
    params["userId"] = element.__data.userId;
    let where = {
      companyId: params["companyId"]
    }
    if (this.DA_TYPE == "employee wise") {
      where["userId"] = params["userId"]

    } else if (this.DA_TYPE == "category wise") {
      where["category"] = element.__data["daCategory"]


    } else if (this.DA_TYPE == "Designation-State Wise") {
      where["designation"] = element.__data["designation"]
    }
    this.expenseClaimedService.getDADetails(where).subscribe(res => {
      if (res.length > 0) {
        this.DAValue = res
      }
      else {
        console.log("err");
      }
      
    })
    this.expenseClaimedService.getExpenseDetails(params).subscribe(res => {

      let array = [];
      this.approvalCountMgr = 0;
      this.approvalCountAdm = 0;
      
      array = res.map(item => {
        
        let where = {
          dcrId: item.dcrId,
          companyId: item.companyId,
          submitBy: item.userId
        }
        this.expenseClaimedService.getVisitedAreaAsPerProviderVisit(where).subscribe(visitedArea => {

          // add visited block as per provider's visiting by preeti arora----.
          item.visitedAreaAsPerProviderVisit = visitedArea;
          //--------------
        })

        // code for the enable & disable approved button according
        
        if (this.currentUser.userInfo[0].rL === 0) {
          this.totalDistance += item.distance;
          this.totalDistanceMgr += item.distanceMgr;
          this.totalDistanceAdmin += item.distanceAdmin;
          
          if (element.hasOwnProperty("appByAdm") === true && element.appByAdm.length >= 1) {
            // 'doctorCalls', 'chemistCalls',
            if (element.hasOwnProperty("appByAdm") === true && element.appByAdm.length >= 1) {
              this.approvedByAdmin = "Yes";
            } else {
              this.approvedByAdmin = "No";
            }
            this.displayedColumns = ['action', 'userName', 'dcrDate', 'workingStatus','plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'type', 'da', 'daMgr', 'daAdmin', 'fare', 'fareMgr', 'fareAdmin', 'miscExpense', 'miscExpenseMgr', 'miscExpenseAdmin', 'totalExpense', 'totalExpenseMgr', 'totalExpenseAdmin', 'remarks', 'remarksMgr', 'remarksAdmin', 'jointWork', 'uploadPicture'];
            if(this.currentUser.userInfo[0].designationLevel===0){
            this.isDisableButton = false;
          }else{this.isDisableButton=true}
        } else {
          this.displayedColumns = ['action', 'userName', 'dcrDate', 'plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'distanceAdmin', 'type', 'da', 'daMgr', 'daAdmin', 'fare', 'fareMgr', 'fareAdmin', 'miscExpense', 'miscExpenseMgr', 'miscExpenseAdmin', 'totalExpense', 'totalExpenseMgr', 'totalExpenseAdmin', 'remarks', 'remarksMgr', 'remarksAdmin', 'jointWork', 'uploadPicture'];
            this.isDisableButton = false;
          }
          //check for expense approved by manager
          if (element.hasOwnProperty("appByMgr") === true && element.appByMgr.length >= 1) {
            //'doctorCalls', 'chemistCalls',
            if (element.hasOwnProperty("appByMgr") === true && element.appByMgr.length >= 1) {
              this.approvedByMgr = "Yes";
            } else {
              this.approvedByMgr = "No";
            }
          }
          
          this.total += (item.daAdmin + item.fareAdmin + item.miscExpenseAdmin );
          
          
          
        } else if (this.currentUser.userInfo[0].rL > 1) {
          this.totalDistance += item.distance;
          this.totalDistanceMgr += item.distanceMgr;
          if (element.hasOwnProperty("appByMgr") === true && element.appByMgr.length >= 1 || element.hasOwnProperty("appByAdm") === true && element.appByAdm.length >= 1) {
            if (element.hasOwnProperty("appByAdm") === true) {
              if (element.appByAdm.length >= 1) {
                this.approvedByAdmin = "Yes";
              } else {
                this.approvedByAdmin = "No";
              }
              this.total += item.daAdmin + item.fareAdmin + item.miscExpenseAdmin;
            } else if (element.hasOwnProperty("appByMgr") === true) {
              if (element.appByMgr.length >= 1) {
                this.approvedByMgr = "Yes";
              } else {
                this.approvedByMgr = "No";
              }
              this.total += item.daMgr + item.fareMgr + item.miscExpenseMgr
            } else {
              this.approvedByAdmin = "No";
              this.approvedByMgr = "No";
            }
            this.displayedColumns = ['action', 'userName', 'dcrDate', 'plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'type', 'da', 'daMgr', 'daAdmin', 'fare', 'fareMgr', 'fareAdmin', 'miscExpense', 'miscExpenseMgr', 'miscExpenseAdmin', 'totalExpense', 'totalExpenseMgr', 'totalExpenseAdmin', 'remarks', 'remarksMgr', 'remarksAdmin', 'jointWork', 'uploadPicture'];
            this.isDisableButton = true;
          } else {
            //'doctorCalls', 'chemistCalls',
            this.totalDistance += item.distance;
            
            this.approvedByAdmin = "No";
            this.approvedByMgr = "No";
            
            this.total += item.daMgr + item.fareMgr + item.miscExpenseMgr
            this.displayedColumns = ['action', 'userName', 'dcrDate', 'plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'distanceAdmin', 'type', 'da', 'daMgr', 'fare', 'fareMgr', 'miscExpense', 'miscExpenseMgr', 'totalExpense', 'totalExpenseMgr', 'remarks', 'remarksMgr', 'jointWork', 'uploadPicture'];
            this.isDisableButton = false;
          }
          //this.checkApprovalStatus(item);
          
        }
        
        //if item modified by both mgr & admin then different color
        if (item.hasOwnProperty("modifiedByMgr") === true && item.modifiedByMgr === true && item.hasOwnProperty("modifiedByAdm") === true && item.modifiedByAdm === true) {
          this.color = "ffe0b2";
        } else if (item.hasOwnProperty("modifiedByMgr") === true && item.modifiedByMgr === true) {
          this.color = "f8bbd0";
        } else if (item.hasOwnProperty("modifiedByAdm") === true && item.modifiedByAdm === true) {
          this.color = "c8e6c9";
        } else {
          this.color = "FFFFFF";
        }


        this.totalDa += item.da;
        this.totalDaMgr += item.daMgr;
        this.totalDaAdmin += item.daAdmin;
        
        
        this.totalFare += item.fare;
        
        this.totalFareMgr += item.fareMgr;
        this.totalFareAdmin += item.fareAdmin;
        
        this.totalMiscExpense += item.miscExpense;
        this.totalMiscExpenseMgr += item.miscExpenseMgr;
        this.totalMiscExpenseAdmin += item.miscExpenseAdmin;
        
        
        // by nipun => for now (temporarily) we are calculating total expense at frontend 03-JUN--2020
        
        
        this.totalExpense += (item.da + item.fare + item.miscExpense);
        this.totalExpenseMgr += (item.daMgr + item.fareMgr + item.miscExpenseMgr);
        // this.totalExpenseAdmin += (item.daAdmin + item.fareAdmin + item.miscExpenseAdmin);
        
        
        
        // this.totalExpense += (item.da + item.fare + item.miscExpense);
        // this.totalExpenseMgr += item.totalExpenseMgr;
        this.totalExpenseAdmin += item.totalExpenseAdmin;
        
        let totalExpenseCalculated = 0;
        let totalExpenseCalculatedMgr = 0;
        let totalExpenseCalculatedAdmin = 0;
        
        (item.da) ? (totalExpenseCalculated += item.da) : null;
        (item.fare) ? (totalExpenseCalculated += item.fare) : null;
        (item.miscExpense) ? (totalExpenseCalculated += item.miscExpense) : null;
        
        (item.daMgr) ? (totalExpenseCalculatedMgr += item.daMgr) : null;
        (item.fareMgr) ? (totalExpenseCalculatedMgr += item.fareMgr) : null;
        (item.miscExpensecrMgr) ? (totalExpenseCalculatedMgr += item.miscExpenseMgr) : null;
        
        (item.totalExpenseAdmin) ? (totalExpenseCalculatedAdmin += item.totalExpenseAdmin) : null;
        //(item.miscExpenseAdmin) ? (totalExpenseCalculatedAdmin += item.miscExpenseAdmin) : null;
        
        
        
        
        return {
          color: this.color,
          totalExpenseCalculated: totalExpenseCalculated,
          totalExpenseCalculatedMgr: totalExpenseCalculatedMgr,
          totalExpenseCalculatedAdmin: totalExpenseCalculatedAdmin,
          ...item
        };
        
        
        // by nipun  03-JUN--2020 END
      });
      
      
      
      
      if (array.length > 0) {
        
        this.getFixedExpenses(element);
        //--------adding condition to get misc expense picture by Preeti Arorafor
        array.forEach(async item => {
          if (item.dcrId) {
            // add visited block as per provider's visiting by preeti arora----.
            let where = {
              dcrId: item.dcrId,
              companyId: item.companyId,
              submitBy: item.userId
            }
            this.expenseClaimedService.getVisitedAreaAsPerProviderVisit(where).subscribe(visitedArea => {
              item["visitedAreaAsPerProviderVisit"] = visitedArea;
            })

            //--------------by Preeti Arora----for adding this on the requirment of bestochem 16-12-2020---------------------
            this.expenseClaimedService.getProviderVisit(where).subscribe(res => {
              //item["visitedAreaAsPerProviderVisit"] = visitedArea;
              let RMPCount = 0
              let dtugStockistCount = 0
              if (res.length > 0) {
                res.forEach(item => {
                  if (item.providerType == "RMP") {
                    RMPCount++
                  } else {
                    dtugStockistCount++
                  }
                  
                });
              }
              item["doctorCall"] = RMPCount;
              item["chemStkCall"] = dtugStockistCount;
              
              
            })
            let jointWorkName = []
            if (item.hasOwnProperty("dCRMaster")) {
              if (item.dCRMaster.jointWork.length > 0) {
                item.dCRMaster.jointWork.forEach(item => {
                  jointWorkName.push(item.Name);
                });
              }
            } else {
              jointWorkName.push("---");
            }
            item["jointWorkName"] = jointWorkName;
            
            //----------------------------

            params["dcrId"] = item.dcrId;
            this.expenseClaimedService.getMiscExpeFileId(params).subscribe(dcrResult => {
              dcrResult.forEach(dcr => {
                if (dcr.dcrExpense) {
                  let modifiedFileName = [];
                  let miscExpenseType = [];
                  
                  dcr.dcrExpense.forEach(expense => {
                    miscExpenseType.push(expense.expenseType)
                    if (expense.fileId) {
                      
                      this._fileHandlingService.getImageDetailBasedOnId(expense.fileId).subscribe(fileResult => {
                        if (fileResult.length) {
                          modifiedFileName.push(fileResult[0].modifiedFileName);
                        }
                      }, err => {
                        console.log(err);
                      });
                    }
                    item["miscExpenseType"] = miscExpenseType
                    item["modifiedFileName"] = modifiedFileName
                  });
                  
                }
              });
            })
          }
        });
        //---------------
        
        // console.log("Tabel",array)
        this.dataSource = new MatTableDataSource(array);
        
        
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.dataSource.paginator = this.paginator1;
        }, 1000);
        this.isShowReport = true;
        
        this.changeDetectorRef.detectChanges();
        
      } else {
        this.toastr.info("Expense is not submitted for the selected  month & year", "Expense not Submitted");
        this.isShowReport = false;
      }
      this.changeDetectorRef.detectChanges();

    });
  }

  sumByKey(val: string) {
    const res = (this.dataSource.data || []).reduce((accumulator, currentValue) => {
      return accumulator + currentValue[val];
    }, 0)
    if (val == 'totalExpenseAdmin') {
      this.totalAdminExpense = res
    }
    return res;
  }

  dataSourceFixed: any;
  totalFixedExpene;
  isLoading;
  getFixedExpenses(element) {

    let param = {
      companyId: this.COMPANY_ID,
      type: this.DA_TYPE,
      isDivisionExist: this.IS_DIVISION_EXIST,

    };

    if (this.DA_TYPE === 'category wise') {
    param["category"] = element.__data.daCategory;
    } else {
    param["designation"] = element.__data.designation;
    param["userId"] = element.__data.userId;

    }

    if (this.IS_DIVISION_EXIST === true) {

      param["divisionId"] = element.__data.divisionId[0]
    }

    

    this.dailyallowanceService.getFixedExpense(param).subscribe(res => {
      this.totalFixedExpene = 0;

      let array = res.map(item => {

        // this.total += item.amount;
        // this.totalFixedExpene += item.amount;
        return {
          ...item
        };
      })
      

      if (array.length > 0) {
        this.dataSourceFixed = JSON.parse(JSON.stringify(array));

        // ++++++++
        if (this.dataSourceFixed[0].hasOwnProperty("mobileAllowance") === true) {
          this.total += this.dataSourceFixed[0].mobileAllowance
        }
        if (this.dataSourceFixed[0].hasOwnProperty("netAllowance") === true) {
          this.total += this.dataSourceFixed[0].netAllowance
        }
        
         if (this.dataSourceFixed[0].hasOwnProperty("specialAllowance") === true) {
          this.total += this.dataSourceFixed[0].specialAllowance
        }
        // ++++++++
        this.changeDetectorRef.detectChanges();
      }
    })
  }
  showImage(fileDetall) {
    let fileName = "";
    if (typeof (fileDetall) == "object") {
      fileName = JSON.parse(JSON.stringify(fileDetall)).modifiedFileName;
    } else {
      fileName = fileDetall;
    }

    let url = 'http://103.11.84.144:3015/api/containers/DCRImage/download/' + fileName;
    window.open(url, "Title but its not working", 'toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500');

  }
  // download sample file
  // downloadAttachedFile(fileDetails) {
  //   let fileName = "";
  //   if (typeof (fileDetails) == "object") {
  //     fileName = JSON.parse(JSON.stringify(fileDetails)).modifiedFileName;
  //   } else {
  //     fileName = fileDetails;
  //   }
  // 	let link = document.createElement("a");
  // 	link.download = "fileName";
  // 	link.href = 'http://103.11.84.144:3015/api/containers/DCRImage/download/' + fileName;
  //   link.click();
  // }
  exporttoExcel() {

    const lastCol = this.displayedColumns2.pop();

    // download the file using old school javascript method
    // setTimeout(() => {
    //   this.exportAsService.save(this.exportAsConfig, 'Finalize Expense Report').subscribe(() => {
    //     // save started
    //     this.displayedColumns2.push(lastCol);
    //   });
    // }, 300);

    let dataToExport = [];
    dataToExport = this.dataSource2.data.map((x) => ({
      "Name": x.__data.name,
      Designation: x.__data.designation,
      "App By Manager": x.hasOwnProperty("appByMgr") === true ? (x.appByMgr.length > 0 ? "Yes" : "No") : "---",
      "App By Admin": x.hasOwnProperty("appByAdm") === true ? (x.appByAdm.length > 0 ? "Yes" : "No") : "---",
      Distance: x.hasOwnProperty("distance") === true ? x.distance : "---",
      "DA": x.hasOwnProperty("da") === true ? (x.da).toFixed(2) : "---",
      "DA Manager": x.hasOwnProperty("daMgr") === true ? (x.daMgr).toFixed(2) : "---",
      "DA Admin": x.hasOwnProperty("daAdmin") === true ? (x.daAdmin).toFixed(2) : "---",
      Fare: x.hasOwnProperty("fare") === true ? (x.fare).toFixed(2) : "---",
      "Fare Manager": x.hasOwnProperty("fareMgr") === true ? (x.fareMgr).toFixed(2) : "---",
      "Fare Admin": x.hasOwnProperty("fareAdmin") === true ? (x.fareAdmin).toFixed(2) : "---",
      "Misc Expense": x.hasOwnProperty("miscExpense") === true ? (x.miscExpense).toFixed(2) : "---",
      "Misc Expense Mgr": x.hasOwnProperty("miscExpenseMgr") === true ? (x.miscExpenseMgr).toFixed(2) : "---",
      "Misc Expense Admin": x.hasOwnProperty("miscExpenseAdmin") === true ? (x.miscExpenseAdmin).toFixed(2) : "---",
      "Total Expense": x.totalExpenseCalculated ? x.totalExpenseCalculated.toFixed(2) : "---",
      "Total Expense Admin": x.totalExpenseCalculatedAdmin ? (x.totalExpenseCalculatedAdmin).toFixed(2) : "---"
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Finalize-Expense.xlsx");
  }


  exporttoExcel2() {
    //const lastCol = this.displayedColumns.shift();

    if (this.currentUser.companyId == "5e19bc6d51dc150bdc75f671") {
      let removeValFromIndex = [0, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21];
      var backupArray = [];

      for (var i = removeValFromIndex.length - 1; i >= 0; i--) {
        backupArray[i] = this.displayedColumns.splice(removeValFromIndex[i], 1);
      }


      setTimeout(() => {
        // download the file using old school javascript method
        this.exportAsService.save(this.exportAsConfig2, 'Finalize Expense Report').subscribe(() => {

          this.displayedColumns = ['action', 'userName', 'dcrDate', 'plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'type', 'da', 'daMgr', 'daAdmin', 'fare', 'fareMgr', 'fareAdmin', 'miscExpense', 'miscExpenseMgr', 'miscExpenseAdmin', 'totalExpense', 'totalExpenseMgr', 'totalExpenseAdmin', 'remarks', 'remarksMgr', 'remarksAdmin', 'jointWork'];



        });
      }, 300);

    }




    else {


      if (this.currentUser.companyId == "5ee4ee67989fbc160405c8ce") {
        let removeValFromIndex = [0, 5, 7, 8, 9, 11, 12, 14, 15, 16, 17, 18, 20, 21, 23, 24, 25, 26];
        var backupArray = [];

        for (var i = removeValFromIndex.length - 1; i >= 0; i--) {
          backupArray[i] = this.displayedColumns.splice(removeValFromIndex[i], 1);
        }

        setTimeout(() => {
          // download the file using old school javascript method
          this.exportAsService.save(this.exportAsConfig2, 'Finalize Expense Report').subscribe(() => {
            this.displayedColumns = ['action', 'userName', 'dcrDate', 'plannedBlock', 'visitedBlock', 'expenseType', 'distance', 'distanceMgr', 'type', 'da', 'daMgr', 'daAdmin', 'fare', 'fareMgr', 'fareAdmin', 'miscExpense', 'miscExpenseMgr', 'miscExpenseAdmin', 'totalExpense', 'totalExpenseMgr', 'totalExpenseAdmin', 'remarks', 'remarksMgr', 'remarksAdmin', 'jointWork'];
          });
        }, 300);

      }
      else {
        const lastCol = this.displayedColumns.shift();

        // download the file using old school javascript method
        setTimeout(() => {
          this.exportAsService.save(this.exportAsConfig2, 'Finalize Expense Report').subscribe(() => {
            // save started
            this.displayedColumns.unshift(lastCol);
          });
        }, 300);

      }


    }
  }


  PrintTableFunction() {
    //const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
    const currentDateAndTime = _moment(new Date()).format('DD/MM/YYYY, dddd , HH:mm A');
    const department = this.currentUser.section;
    const companyName = this.currentUser.companyName;
    const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;


    this.tablerowleave = [];
    this.tablerowWorking = [];
    this.hq = [];
    this.ex = [];
    this.out = [];
    this.workingDaysCount = 0;
    this.hqcount = 0;
    this.excount = 0;
    this.outcount = 0;
    this.leaveDaysCount = 0;
    let tableRows: any = [];
    let tablerowfirst: any = [];
    let tablerowEmplCode: any = [];
    let tablerowDOJ: any = [];
    let bank: any = [];

    let tablerowdesignation: any = [];
    let tablerowdivisionname: any = [];
    let tablerowMonth: any = [];
    let tablerowmobile: any = [];
    let tablerowNet: any = [];
    let tabelSpecial : any = [];
    let tablerowtotal: any = [];
    let tablerowtotalMR: any = [];
    let tablerowtotalMGR: any = [];


    let totalDAValue = 0;
    let totalDAMRVAlue = 0;
    let totalDAMGRVAlue = 0;
    let totalExpAdmin = 0;
    let ExpAdmin = 0

    let totalDistanceValue = 0;
    let totalFareValue = 0;
    let totalMiscellanouseValue = 0;
    let TotalSumValue = 0;
    let TotalValue = 0;

    let totalDistanceValueMR = 0;
    let totalFareValueMR = 0;
    let totalMiscellanouseValueMR = 0;
    let TotalSumValueMR = 0;
    let TotalValueMR = 0;
    let totalExpenceAdminSum = 0;

    let totalDistanceValueMGR = 0;
    let totalFareValueMGR = 0;
    let totalMiscellanouseValueMGR = 0;
    let TotalSumValueMGR = 0;
    let TotalValueMGR = 0;
    let hq = [];


    for (let i = 0; i < 1; i++) {

      bank.push(`
      <td class="tg-oiyu">${this.dataSource.filteredData[i].userLogin.bankName == undefined ? "---" : this.dataSource.filteredData[i].userLogin.bankName}</td>
      `)

      tablerowEmplCode.push(`
  <td class="tg-oiyu">${this.dataSource.filteredData[i].userLogin.employeeCode}</td>
  `)
      hq.push(`
  <td class="tg-oiyu">${this.dataSource.filteredData[i].district.districtName}</td>
  `)

    }
    for (let i = 0; i < 1; i++) {

      tablerowfirst.push(`
  <td class="tg-oiyu" style="font-weight:600">${this.dataSource.filteredData[i].userLogin.name}</td>
  `)

    }
    for (let i = 0; i < 1; i++) {

      tablerowdesignation.push(`
  
  <td class="tg-oiyu">${this.dataSource.filteredData[i].userLogin.designation}</td>
  `)

    }
    for (let i = 0; i < 1; i++) {

      tablerowdivisionname.push(`
  
  <td class="tg-oiyu">${this.dataSource.filteredData[i].userLogin.divisionName}</td>
  `)

    }
    for (let i = 0; i < 1; i++) {
      var mydate = new Date(this.dataSource.filteredData[i].userLogin.dateOfJoining);
      tablerowDOJ.push(`
  
  <td class="tg-oiyu">${mydate.getDate()}-${mydate.getMonth() + 1}-${mydate.getFullYear()}</td>
  `)

    }

    for (let i = 0; i < 1; i++) {
      var mydate = new Date(this.dataSource.filteredData[i].dcrDate);


      let data = mydate.getMonth() + 1;

      let monthNAme = "";

      if (data == 1) {

        monthNAme = "January";
      }
      else if (data == 2) {
        monthNAme = "February";

      }
      else if (data == 3) {

        monthNAme = "March";
      }
      else if (data == 4) {
        monthNAme = "April";

      }
      else if (data == 5) {
        monthNAme = "May";

      }
      else if (data == 6) {

        monthNAme = "June";
      }
      else if (data == 7) {

        monthNAme = "July";
      }
      else if (data == 8) {
        monthNAme = "August";

      }
      else if (data == 9) {
        monthNAme = "September";

      }
      else if (data == 10) {

        monthNAme = "October";
      }
      else if (data == 11) {
        monthNAme = "November";

      }
      else if (data == 12) {
        monthNAme = "December";

      }


      tablerowMonth.push(`
  <td class="tg-oiyu">${monthNAme}</td>
  `)
    }

    for (let i = 0; i < 1; i++) {

      let visitedBlock;

      if (this.dataSourceFixed[i].mobileAllowance) {

        visitedBlock = `<td class="tg-oikpyu" width="200">${this.dataSourceFixed[i].mobileAllowance}</td>`

      } else {
        visitedBlock = `<td class="tg-oikpyu" width="200">${'---'}---${'---'}</td>`


      }

      tablerowmobile.push(`
  
    ${visitedBlock}

  `)

    }

    let visitedNetAllowance;
    let specialAllounces;

    for (let i = 0; i < 1; i++) {

      if (this.dataSourceFixed[i].netAllowance) {

        visitedNetAllowance = `<td class="tg-oikpyu" width="200" >${this.dataSourceFixed[i].netAllowance}</td>`

      } else {
        visitedNetAllowance = `<td class="tg-oikpyu" width="200">${'---'}---${'---'}</td>`


      }
      if (this.dataSourceFixed[i].specialAllowance) {

        specialAllounces = `<td class="tg-oikpyu" width="200" >${this.dataSourceFixed[i].specialAllowance}</td>`

      } else {
        specialAllounces = `<td class="tg-oikpyu" width="200">${'---'}---${'---'}</td>`


      }

      tablerowNet.push(`
  
    ${visitedNetAllowance}

    `)

    tabelSpecial.push(`${specialAllounces}`)


    }

    let count = 0 
    this.dataSource.filteredData.forEach(element => {
      count+=1;
      if (element.workingStatus == "Working") {

        this.workingDaysCount++;
      } else if (element.workingStatus == "Leave") {
        this.leaveDaysCount++;
      }
      if (element.type == "HQ") {
        this.hqcount++;
      } else if (element.type == "EX") {
        this.excount++;
      }
      else if (element.type == "OUT") {
        this.outcount++;
      }
      let visitedBlock = [];

       let visitedBlockbasedOnDCR = [];

      let fromvisitedblock = [];

      let tovisitedblock = [];

      let totalvisitedblock = [];

  let fromvisitedblockBasedOnDCR = [];

      let tovisitedblockBasedOnDCR = [];

      let totalvisitedblockBasedOnDCR = [];

      let TPBlock = [];

      let fromTpvisitedblock = [];

      let toTPvisitedblock = [];

      let totalTPvisitedblock = [];

      if (element.hasOwnProperty("plannedBlock")) {
        for (let i = 0; i < element.plannedBlock.length; i++) {
          fromTpvisitedblock[i] = element.plannedBlock[i].from;
          toTPvisitedblock[i] = element.plannedBlock[i].to;
          totalTPvisitedblock[i] = fromTpvisitedblock[i] + "---" + toTPvisitedblock[i];
        }
      }

      if (element.plannedBlock && element.plannedBlock[0] && element.plannedBlock[0].from) {

        TPBlock[0] = `<td class="tg-oiyu">${totalTPvisitedblock}</td>`

      } else {
        TPBlock[0] = `<td class="tg-oiyu">${'---'}---${'---'}</td>`


      }


if(element.hasOwnProperty("visitedBlock")){
  for (let i = 0; i < element.visitedBlock.length; i++) {
    fromvisitedblock[i] = element.visitedBlock[i].from;
    tovisitedblock[i] = element.visitedBlock[i].to;
    totalvisitedblock[i] = fromvisitedblock[i] + "---" + tovisitedblock[i];
  }
}
      

   for (let i = 0; i < element.visitedAreaAsPerProviderVisit.length; i++) {
        fromvisitedblockBasedOnDCR[i] = element.visitedAreaAsPerProviderVisit[i].fromArea;
        tovisitedblockBasedOnDCR[i] = element.visitedAreaAsPerProviderVisit[i].toArea;
        totalvisitedblockBasedOnDCR[i] = fromvisitedblockBasedOnDCR[i] + "---" + tovisitedblockBasedOnDCR[i];
      }



      if (element.visitedBlock && element.visitedBlock[0] && element.visitedBlock[0].from) {

        visitedBlock[0] = `<td class="tg-oiyu">${totalvisitedblock}</td>`

      } else {
        visitedBlock[0] = `<td class="tg-oiyu">${'---'}---${'---'}</td>`


      }

        if (element.visitedAreaAsPerProviderVisit && element.visitedAreaAsPerProviderVisit[0] && element.visitedAreaAsPerProviderVisit[0].fromArea) {

        visitedBlockbasedOnDCR[0] = `<td class="tg-oiyu">${totalvisitedblockBasedOnDCR}</td>`

      } else {
        visitedBlockbasedOnDCR[0] = `<td class="tg-oiyu">${'---'}---${'---'}</td>`


      }


      let type = '';

      if (element.type) {
        type = `<td class="tg-oiyu">${element.type}</td>`
      } else {
        type = `<td class="tg-oiyu">---</td>`
      }

      totalDAValue = totalDAValue + element.daAdmin;
      totalDAMRVAlue = totalDAMRVAlue + element.da;
      totalDAMGRVAlue = totalDAMGRVAlue + element.daMgr;
      totalExpAdmin = totalExpAdmin + element.totalExpenseAdmin;
      ExpAdmin = ExpAdmin + element.fareAdmin;


      totalDistanceValue = totalDistanceValue + element.distanceAdmin;
      totalFareValue = totalFareValue + element.fareAdmin;
      totalMiscellanouseValue = totalMiscellanouseValue + element.miscExpenseAdmin;
      TotalSumValue = totalDAValue + totalFareValue + totalMiscellanouseValue;
      TotalValue = TotalValue + TotalSumValue;

      totalDistanceValueMR = totalDistanceValueMR + element.distance;
      totalFareValueMR = totalFareValueMR + element.fare;
      totalMiscellanouseValueMR = totalMiscellanouseValueMR + element.miscExpense;
      TotalSumValueMR = totalDAMRVAlue + totalFareValueMR + totalMiscellanouseValueMR;
      TotalValueMR = TotalValueMR + TotalSumValueMR;
      totalExpenceAdminSum = totalExpenceAdminSum + element.totalExpenseCalculatedAdmin;



      totalDistanceValueMGR = totalDistanceValueMGR + element.distanceMgr;
      totalFareValueMGR = totalFareValueMGR + element.fareMgr;
      totalMiscellanouseValueMGR = totalMiscellanouseValueMGR + element.miscExpenseMgr;
      TotalSumValueMGR = totalDAMGRVAlue + totalFareValueMGR + totalMiscellanouseValueMGR;
      TotalValueMGR = TotalValueMGR + TotalSumValueMGR;


      var mydate = new Date(element.dcrDate);

      let totalGRandSum;
      let workingStatus;


      if (element.dCRMaster != undefined && element.dCRMaster.workingStatus == "Leave") {
        workingStatus = `<td class="tg-oiyu" style="color:red">${element.dCRMaster.workingStatus}</td>`
      } else if (element.dCRMaster != undefined) {
        workingStatus = `<td class="tg-oiyu">${element.dCRMaster.workingStatus}</td>`
      } else if (element.hasOwnProperty('workingStatus')) {
        workingStatus = `<td class="tg-oiyu">${element.workingStatus}</td>`
      } else {
        workingStatus = "---"
      }

      tableRows.push(`<tr>
  <td class="tg-oiyu"  >${mydate.getDate()}-${mydate.getMonth() + 1}-${mydate.getFullYear()} </td>
  ${this.currentUser.company.isDivisionExist ? "" : TPBlock} 

  
   ${visitedBlock}
    ${visitedBlockbasedOnDCR}
  <td class="tg-oiyu">${element.distanceAdmin} </td>
  ${workingStatus}
  <td class="tg-oiyu">${element.fareAdmin}</td>
  ${type}
  <td class="tg-oiyu">${element.daAdmin}</td>
  ${this.currentUser.company.isDivisionExist ? "" : `<td class="tg-oiyu">${element.doctorCall}</td>`} 
  ${this.currentUser.company.isDivisionExist ? "" : `<td class="tg-oiyu">${element.chemStkCall}</td>`} 

  <td class="tg-oiyu">${element.jointWorkName}</td>
  <td class="tg-oiyu">${element.miscExpenseAdmin}</td> 
  <td class="tg-oiyu">${element.miscExpenseType == undefined ? "---" : element.miscExpenseType}</td>
  <td class="tg-oiyu">${element.totalExpenseCalculatedAdmin}</td>
  <td class="tg-oiyu">${element.remarksAdmin == undefined ? "---" : element.remarksAdmin}</td>
  </tr>`)
    });
    // ---------------------------COLUM added totalExpenseCalculatedAdmin By GOURAV  line 1370 and 1769-----------------------

    let mobileDAta;
    let netDAta;
    let specialDAta;
    let deductGrandAmount;



    for (let i = 0; i < 1; i++) {



      if (this.dataSourceFixed[i].mobileAllowance) {
        mobileDAta = this.dataSourceFixed[i].mobileAllowance;

      }

      else {
        mobileDAta = 0;

      }

      if (this.dataSourceFixed[i].netAllowance) {

        netDAta = this.dataSourceFixed[i].netAllowance;

      }
      else {

        netDAta = 0;

      }

      if (this.dataSourceFixed[i].specialAllowance) {

        specialDAta = this.dataSourceFixed[i].specialAllowance;

      }
      else {

        specialDAta = 0;

      }


      
      if (this.deductionAmount) {
        deductGrandAmount = this.deductionAmount;
      }
      else {
        deductGrandAmount = 0
      }

      tablerowtotal.push(`
  
  <td class="tg-oikpyu">${totalExpenceAdminSum + mobileDAta + specialDAta+netDAta - deductGrandAmount + this.IncrementAmount}</td>
  `)
      tablerowtotalMR.push(`
  <td class="tg-oikpyu">${totalExpenceAdminSum + mobileDAta +specialDAta+ netDAta - deductGrandAmount + this.IncrementAmount}</td>
  `)

      //(`

      //<td class="tg-oikpyu">${TotalSumValueMR + mobileDAta + netDAta - deductGrandAmount + this.IncrementAmount}</td>
      //`)
      tablerowtotalMGR.push(`
  <td class="tg-oikpyu">${totalExpenceAdminSum + mobileDAta + ExpAdmin +specialDAta+ netDAta - deductGrandAmount + this.IncrementAmount}</td>
  `)

      /* (`
      
      <td class="tg-oikpyu">${TotalSumValueMGR + mobileDAta + netDAta - deductGrandAmount + this.IncrementAmount}</td>
      `) */

    }

    for (let i = 0; i < 1; i++) {
      this.tablerowleave.push(`
  <td class="tg-oiyu">${this.leaveDaysCount}</td>
  `)
    }
    for (let i = 0; i < 1; i++) {
      this.tablerowWorking.push(`
  <td class="tg-oiyu">${this.workingDaysCount}</td>
  `)
    }
    for (let i = 0; i < 1; i++) {
      this.hq.push(`
  <td class="tg-oiyu">${this.hqcount}</td>
  `)
    }
    for (let i = 0; i < 1; i++) {
      this.ex.push(`
  <td class="tg-oiyu">${this.excount}</td>
  `)
    }
    for (let i = 0; i < 1; i++) {
      this.out.push(`
  <td class="tg-oiyu">${this.outcount}</td>
  `)
    }

    let showHeaderAndTable: boolean = false;
    //this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;

    let popupWin;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
    <head>
    <style>
    img {
    float: left;
    }
  
    .flex-container {
    display: flex;
    justify-content: space-between;
    }
  
    .tg {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }

   
  
    .tg td {
    font-family: Arial, sans-serif;
    font-size: 14px;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tg th {
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tg .tg-lboi {
    border-color: inherit;
    text-align: left;
    vertical-align: middle;
  
    }
  
    .tg .tg-u6fn {
    font-weight: bold;
    font-size: 18px;
    background-color: #efefef;
    color:black;
    border-color: inherit;
    text-align: left;
    vertical-align: top;
    padding-top: 0px;
    padding-bottom: 0px;
    }
  
    .tg .tg-yz93 {
    border-color: inherit;
    text-align: right;
    vertical-align: middle
    }
  
    .tb2 {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }
  
    .tb2 td {
    font-family: Arial, sans-serif;
    font-size: 12px;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 th {
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-weight: normal;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 .tg-oiyu {
    font-size: 10px;
    text-align: left;
    vertical-align: middle
    }

    .tb2 .tg-oikpyu {
      font-size: 10px;
      text-align: center;
      vertical-align: middle
      }


    .tg-bckGnd {
    font-weight: bold;
    background-color: #efefef;
    }
  
  
    .tb2 .tg-3ppa {
    font-size: 10px;
    background-color: #efefef;
    color:black;
    text-align: left;
    vertical-align: middle;
    padding-top: 2px;
    padding-bottom: 2px;
    }
  

    .tb2 .tg-3ppka {
      font-size: 10px;
      background-color: #efefef;
      color:black;
      text-align: center;
      vertical-align: middle;
      padding-top: 2px;
      padding-bottom: 2px;
      }

      .tb2 .tg-3pljka {
        font-size: 10px;
        background-color: #efefef;
        color:black;
        text-align: left;
        vertical-align: middle;
        padding-top: 2px;
        padding-bottom: 2px;
        }

        .tb2 .tg-3pjka {
          font-size: 10px;
          text-align: left;
          padding-top: 2px;
          padding-bottom: 2px;
          }

    .footer {
    position: sticky;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #efefef;
    color: rgb(2, 2, 2);
    text-align: right
    }
  
  
  
    button {
    background-color: #008CBA;
    border: none;
    color: white;
    margin: 5px 0px;
    padding: 2px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    border-radius: 12px;
    cursor: pointer;
    outline: none;
    }
  
    @media print {
    @page { size: A4 portrait;
      max-height:100%;
      max-width:100% }

      body { 
        -webkit-print-color-adjust: exact; 
      }
  
    .footer {
    position: relative;
    }
    tr {
    page-break-inside: avoid;
    }
  
    button{
    display : none;
    }
    }
    @media print{
      body{
          zoom: 70%;
      }
    }
    </style>
    </head>
    <body onload="window.print();window.close()" style="padding-bottom:30px">
    <div class="flex-container">
    <div style="width: 10%; ">
    <img src=${logoURL} style="width:200px;height:50px;margin-right:5px;">
  
    </div>

  <h2 style="margin-top: 0px; margin-bottom: 4px " ; align="left"><u>Monthly Expenses Statement</u></h2>

    <div>
    
    <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
    <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
    </div>
    </div>
    </div>
  
    <div>
    ${/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/''}

    <table class="tb2">
    <tr>
    <th class="tg-3ppa"><span style="font-weight:600">Name</span>
    </th>
   ${tablerowfirst.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">Code</span>
    </th>
    ${tablerowEmplCode.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">HQ</span>
    </th>
    ${hq.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">Designation</span>
    </th>
    ${tablerowdesignation.join("")}

    ${this.currentUser.company.isDivisionExist
        ? `  <th class="tg-3ppa"><span style="font-weight:600">Division</span>
        </th>`
        : ""
      } 
    ${this.currentUser.company.isDivisionExist ? `<td class="tg-oiyu">  ${tablerowdivisionname.join("")}</td>` : ""} 
  
    <th class="tg-3ppa"><span style="font-weight:600">Date Of Joining</span>
    </th>
    ${tablerowDOJ.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">Month</span>
    </th>
    ${tablerowMonth.join("")}
    </tr>



    <tr>
    <th class="tg-3ppa"><span style="font-weight:600">Leave</span>
    </th>
    ${this.tablerowleave.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">HQ Visit</span>
    </th>
    ${this.hq.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">EX Visit</span>
    </th>
    ${this.ex.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">OUT Visit</span>
    </th>
    ${this.out.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">Working Days</span>
    </th>
    ${this.tablerowWorking.join("")}
    <th class="tg-3ppa"><span style="font-weight:600">Bank</span>
    </th>
    ${bank.join("")}
        </tr>

    </table>
    <table class="tb2"> 
    <tr>
    <th class="tg-3ppa"><span style="font-weight:600">Date</span></th>
    ${this.currentUser.company.isDivisionExist
        ? ""
        : ` <th class="tg-3ppa" ><span style="font-weight:600">Tour Program</span></th>`
      } 
   
    <th class="tg-3ppa"><span style="font-weight:600">Visited (From--To)</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Visited Area(From--To) Based On DCR Call</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Distance</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Working Type</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">TA</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Area Type</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">DA</span></th>

    ${this.currentUser.company.isDivisionExist
        ? ""
        : `<th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.doctorLabel} Calls</span></th>`
      } 
    ${this.currentUser.company.isDivisionExist
        ? ""
        : `<th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.vendorLabel}/${this.currentUser.company.lables.stockistLabel} Calls</span></th>`
      } 
       
    <th class="tg-3ppa"><span style="font-weight:600">Worked With </span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Misc Expense Amount</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Misc Expense Type </span></th>
    <th class="tg-3ppa"><span style="font-weight:600"> Total Expense Admin</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Remarks</span></th>
    </tr>
    ${tableRows.join("")}
    <tr>
    ${this.currentUser.company.isDivisionExist
        ? `<td colspan="2" style="font-weight:600">Total:</td>`
        : ` <td colspan="3" style="font-weight:600">Total:</td>`
      }
      <td class="tg-oiyu" style="font-weight:600"></td>
    <td class="tg-oiyu" style="font-weight:600">${totalDistanceValue}</td>
    <td class="tg-oiyu" style="font-weight:600"></td>
    <td class="tg-oiyu" style="font-weight:600" >${totalFareValue}</td>
    <td class="tg-oiyu" style="font-weight:600"></td>
    <td class="tg-oiyu" style="font-weight:600" >${totalDAValue}</td>
    ${this.currentUser.company.isDivisionExist
        ? ""
        : `<td class="tg-oiyu" style="font-weight:600"></td>`
      }
    ${this.currentUser.company.isDivisionExist
        ? ""
        : `<td class="tg-oiyu" style="font-weight:600"> </td>`
      }    <td class="tg-oiyu"></td>
  
    <td class="tg-oiyu" style="font-weight:600">${totalMiscellanouseValue}</td>
    <td class="tg-oiyu"></td>
    <td class="tg-oiyu" style="font-weight:600"> ${totalExpenceAdminSum} </td>
    <td class="tg-oiyu"></td>
</tr>
    </table>
   
    </div>
    <div>
    <table class="tb2" style="width:800px;  " align="center">
    
                                            <tr>
                                                <th class="tg-3pljka" style="font-weight: bold;" width="110">Expenses</th>
                                               
                                                <th class="tg-3ppka" style="font-weight: bold;">Admin Amount</th>

                                                <th class="tg-3ppka" style="font-weight: bold;">Remarks</th>
                                            </tr>
                                            <tr>
                                            <th class="tg-3pjka" width="110">Total</th>
                                          
                                            <td class="tg-oikpyu">${totalExpenceAdminSum}</td>
                                         
                                            <td class="tg-oikpyu">${this.TotalRemarks}</td>
                                        </tr>


                                        
                                      
                                            <tr>
                                                <th class="tg-3pjka" width="110">Mobile</th>
                                             
                                                ${tablerowmobile.join("")}
                                                <td class="tg-3pjka">${this.TotalRemarks}</td>
                                                </tr>
                                            <tr>
                                                <th class="tg-3pjka" width="110">Net Allowance</th>
                                              
                                                ${tablerowNet.join("")}

                                                <td class="tg-oikpyu">${this.NetRemarks}</td>
                                                </tr>
                                            <tr>
                                                <th class="tg-3pjka" width="110">Special Allounces</th>
                                              
                                                ${tabelSpecial.join("")}

                                                <td class="tg-oikpyu">---</td>
                                              </tr>
                                              
                                            <tr>
                                            <th class="tg-3pjka" width="110">Addition</th>
                                        
                                            <td class="tg-oikpyu">${this.IncrementAmount}</td>
                                            <td class="tg-oikpyu"> ${this.IncrementRemarks}
                                            </td>
                                        </tr>
                                        <tr>
                                        <th class="tg-3pjka" width="110">Deduction</th>
                                      
                                        <td class="tg-oikpyu">${deductGrandAmount}</td>

                                        <td class="tg-oikpyu">${this.deductionRemarks}
                                        </td>
                                    </tr>
                                   
                                            <tr style="font-weight: bold;">
                                                <th class="tg-3pjka" style="font-weight: bold;" width="110">Grand Total</th>
                                              
                                                ${tablerowtotal.join("")}
                                                <td class="tg-3pjka">${this.TotalRemarks}
                                            </tr>
                                        </tbody>
                                        </table>
    
    </div
   
    </body>
    </html>
    `);

    //popupWin.document.close();

  }


}
