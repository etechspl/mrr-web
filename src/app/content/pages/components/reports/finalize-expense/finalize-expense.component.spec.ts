import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalizeExpenseComponent } from './finalize-expense.component';

describe('FinalizeExpenseComponent', () => {
  let component: FinalizeExpenseComponent;
  let fixture: ComponentFixture<FinalizeExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalizeExpenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalizeExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
