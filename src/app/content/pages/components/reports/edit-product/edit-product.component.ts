import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { StateComponent } from '../../filters/state/state.component';
import { ProductService } from '../../../../../core/services/Product/product.service';
import { DivisionComponent } from '../../filters/division/division.component';

@Component({
  selector: 'm-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);

  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;

  productEditForm: FormGroup;
  constructor(public dialogRef: MatDialogRef<EditProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _toasterService: ToastrService,
    private fb: FormBuilder, private _productService: ProductService) {
    this.productEditForm = this.fb.group({
      productId: this.data.EditDataValues.id,
      divisionId: this.data.EditDataValues.divisionId,
      assignedStates: this.data.EditDataValues.stateId,
      productName: this.data.EditDataValues.productName,
      productCode: this.data.EditDataValues.productCode,
      productType: this.data.EditDataValues.productType,
      productShortName: this.data.EditDataValues.productShortName,
      productPackageSize: this.data.EditDataValues.productPackageSize,
      samplePackageSize: this.data.EditDataValues.samplePackageSize,
      ptw: this.data.EditDataValues.ptw,
      ptr: this.data.EditDataValues.ptr,
      mrp: this.data.EditDataValues.mrp,
      sampleRate: this.data.EditDataValues.sampleRate,
      includeVat: this.data.EditDataValues.includeVat,
    })
  }

  ngOnInit() {

    this.callStateComponent.stateSelected = typeof (this.data.EditDataValues.assignedStates) == "object" ? this.data.EditDataValues.assignedStates : [this.data.EditDataValues.assignedStates];
    this.callDivisionComponent.selected = typeof (this.data.EditDataValues.assignedDivision) == "object" ? this.data.EditDataValues.assignedDivision : [this.data.EditDataValues.assignedDivision];
    this.getDivisionValue(typeof (this.data.EditDataValues.assignedDivision) == "object" ? this.data.EditDataValues.assignedDivision : [this.data.EditDataValues.assignedDivision])
    this.getStateValue(typeof (this.data.EditDataValues.assignedStates) == "object" ? this.data.EditDataValues.assignedStates : [this.data.EditDataValues.assignedStates])

    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
    }
  }

  getStateValue(val) {
    this.productEditForm.patchValue({ assignedStates: val });
  }
  getDivisionValue(val) {
    this.productEditForm.patchValue({ divisionId: val });

  }
  updateProduct() {

    this._productService.getEditedData(this.productEditForm.value, this.currentUser.company.id).subscribe(productEditResponse => {
      this._toasterService.success('Product Updated Successfully');

    }, err => {
      console.log(err);
      this._toasterService.error('Product Cannot Be Modified');

    });

  }
  onNoClick() {
    this.dialogRef.close();
  }

}
