import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidervisitdetailComponent } from './providervisitdetail.component';

describe('ProvidervisitdetailComponent', () => {
  let component: ProvidervisitdetailComponent;
  let fixture: ComponentFixture<ProvidervisitdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidervisitdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidervisitdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
