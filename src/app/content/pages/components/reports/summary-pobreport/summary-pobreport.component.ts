import { ToastrService } from "ngx-toastr";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { StateService } from "../../../../../core/services/state.service";
import {
	FormGroup,
	FormControl,
	Validators,
	FormBuilder,
	NgModel,
} from "@angular/forms";
import { MonthYearService } from "../../../../../core/services/month-year.service";
import { DistrictService } from "../../../../../core/services/district.service";
import { TourProgramService } from "../../../../../core/services/TourProgram/tour-program.service";
import { DistrictComponent } from "../../filters/district/district.component";
import { DivisionComponent } from "../../filters/division/division.component";
import { StateComponent } from "../../filters/state/state.component";
import { MatPaginator, MatSlideToggleChange, MatTableDataSource } from "@angular/material";
import { URLService } from "../../../../../core/services/URL/url.service";
declare var $;
import * as _moment from "moment";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";
import { ExportAsConfig, ExportAsService } from "ngx-export-as";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
@Component({
	selector: 'm-summary-pobreport',
	templateUrl: './summary-pobreport.component.html',
	styleUrls: ['./summary-pobreport.component.scss']
})
export class SummaryPOBReportComponent implements OnInit {
	@ViewChild('paginator') paginator: MatPaginator;

	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;

	currentUser = JSON.parse(sessionStorage.currentUser);
	showTPDetails = false;
	check: boolean;
	public showCompleteTPDetails: boolean = false;
	public showProcessing: boolean = false;
	@ViewChild("dataTable") table;
	@ViewChild("dataTableForTPInDetail") tableForTPInDetail;
	dataTable: any;
	dtOptions: any;
	dataTableForTPInDetail: any;
	dtOptionsForTPInDetail: any;
	SummaryPOB: FormGroup;
	showData: boolean = false;
	dataSource: MatTableDataSource<any>;
	exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementId: 'tableId1', // the id of html/table element
	}
	public showDivisionFilter: boolean = false;

	isDivisionExist = this.currentUser.company.isDivisionExist;
	submittedFilter = [];
	constructor(private fb: FormBuilder,
		private stateService: StateService,
		private districtService: DistrictService,
		private monthYearService: MonthYearService,
		private _tpService: TourProgramService,
		private _toastr: ToastrService,
		private _providerService:ProviderService,
		private exportAsService: ExportAsService,

		private _dcrReportsService: DcrReportsService,
		private _changeDetectorRef: ChangeDetectorRef,

		private _urlService: URLService) {
		this.SummaryPOB = this.fb.group({
			stateInfo: new FormControl(null, Validators.required),
			districtInfo: new FormControl(null, Validators.required),
			fromDate: new FormControl(true),
			toDate: new FormControl(true),
		});
	}
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
	displayedColumns = [
		"sn",
		"state",
		"hq",
		"userName",
		"team",
		"statusDated",
		"TC",
		"PC",
		"POB",
		"SOB",
		"stockistForwarded",
		"workWith",
		"totalTC",
		"totalPC",
		"totalPOB",
		"totalSOB",

	];

	ngOnInit() {
		if (this.currentUser.company.isDivisionExist == true) {
			this.SummaryPOB.setControl("division", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.showDivisionFilter = true;
		}
	}
	getDivisionValue(val) {
		this.showCompleteTPDetails = false;
		this.showTPDetails = false;

		this.SummaryPOB.patchValue({ division: val });
		//-------Clearing Filter--------------
		this.callStateComponent.setBlank();
		this.callDistrictComponent.setBlank();
		//--------------------END-----------------------------------------
		if (this.currentUser.userInfo[0].designationLevel == 0) {
			let divisionIds = {
				division: val,
			};
			this.callStateComponent.getStateBasedOnDivision(divisionIds);
		} else if (this.currentUser.userInfo[0].designationLevel >= 2) {
			let passingObj = {
				companyId: this.currentUser.companyId,
				isDivisionExist: true,
				division: val,
				supervisorId: this.currentUser.id,
			};
			this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(
				passingObj
			);
		}
	}

	getStateValue(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;
		this.SummaryPOB.patchValue({ stateInfo: val });

		this.callDistrictComponent.setBlank();

		if (this.currentUser.company.isDivisionExist == true) {
			let passingObj = {
				division: this.SummaryPOB.value.division,
				companyId: this.currentUser.companyId,
				stateId: val,
			};
			this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
		} else if (this.currentUser.company.isDivisionExist == false) {
			this.callDistrictComponent.getDistricts(
				this.currentUser.companyId,
				val,
				true
			);
		}
	}
	getDistrictValue(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;
		this.SummaryPOB.patchValue({ districtInfo: val });
	}
	getFromDate(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;

		this.SummaryPOB.patchValue({ fromDate: val });
	}
	getToDate(val) {
		this.showCompleteTPDetails = false;

		this.showTPDetails = false;
		this.SummaryPOB.patchValue({ toDate: val });
	}
	submittedStatus(event) {
		this.submittedFilter = event;
	}
	exporttoExcel() {
		this.exportAsService.save(this.exportAsConfig, 'POB Summary Report State Wise').subscribe(() => {
		});
	}
	getStockistName(data):Promise<any>{
		return new Promise((resolve, reject) => {
			this._providerService.getStockistDetails(data).subscribe(res=>{
			  if(res.length>0){
				resolve(res);
			  }else{
				reject([]);
			  }
			});
		});  
	  }
	showReport() {
		this.isToggled = false;
		this.showProcessing = true;
		let obj = {
			companyId: this.currentUser.companyId,
			stateId: this.SummaryPOB.value.stateInfo,
			districtId: this.SummaryPOB.value.districtInfo,
			fromDate: this.SummaryPOB.value.fromDate,
			toDate: this.SummaryPOB.value.toDate,
		}

		this._dcrReportsService.getPobDetailsStockistWiseSummarised(obj).subscribe((res) => {
			if (res.length > 0) {
				try {
					res.sort(function (a, b) {
						if (a.hasOwnProperty("state") === true && b.hasOwnProperty("state") === true) {
							var textA = a.state.toUpperCase();
							var textB = b.state.toUpperCase();
							return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
						}
					});
					res.forEach(async (element,i,arr) => {
						let jointWorkWith = [];
						if (element.jointWorkWith.length > 0) {
							element.jointWorkWith.forEach(jointName => {
								if (jointName != null && jointName.length > 0) {
									jointWorkWith.push(jointName[0].Name)
								}
							});
						}
						let stockistNameArr=[];
						if(element.POBForwardedLastDateToStockist){
							let stockistIds=[];
							let stockistIdsWithPOB=[];
						   element.POBForwardedLastDateToStockist.forEach( (stockistId) => {                              
								 if(stockistId!=null && stockistId.hasOwnProperty("stockist")){
								   stockistIds.push(stockistId.stockist);
								   stockistIdsWithPOB.push({stockistId:stockistId.stockist,pob:stockistId.pob})
								 }
							});
							if(stockistIds.length>0){
							 var obj={
							   where:{
								 companyId:this.currentUser.companyId,
								 _id:{inq:stockistIds},
							   }
							}
							const response = await this.getStockistName(obj).catch(err=>{console.log(err); });
							if(response.length>0){
							  response.forEach(element => {
								stockistIdsWithPOB.forEach(stkId => {                                   
								  if((element.id).toString()==(stkId.stockistId).toString()){
								   let data=element.providerName +"-"+ stkId.pob
								   stockistNameArr.push(data)
								  }
								  
								});
								
								
							  });
							} 
							}
							   
						   
											
						}
						element.stockistName=stockistNameArr;
						element.workWith = jointWorkWith
						element.statusDate = this.SummaryPOB.value.toDate;

					});

					this.showData = true;
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
					this.dataSource = new MatTableDataSource(res);
					setTimeout(() => {
						this.dataSource.paginator = this.paginator;
					}, 100);
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;


				} catch (ex) {
					console.log(ex)
				}
			} else {
				this._toastr.error("For Selected Filter data not found",
					"Data Not Found");
			}
			this._changeDetectorRef.detectChanges();
		});

	}

}
