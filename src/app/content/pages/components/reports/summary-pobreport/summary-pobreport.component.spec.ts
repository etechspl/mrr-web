import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryPOBReportComponent } from './summary-pobreport.component';

describe('SummaryPOBReportComponent', () => {
  let component: SummaryPOBReportComponent;
  let fixture: ComponentFixture<SummaryPOBReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryPOBReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryPOBReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
