import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { UtilsService } from '../../../../../core/services/utils.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StatusComponent } from '../../filters/status/status.component';
import { Designation } from '../../../_core/models/designation.model';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import * as XLSX from 'xlsx';

import { NativeDateAdapter, DateAdapter, MatDateFormats, MAT_DATE_FORMATS, MatStepper, MatSlideToggleChange } from "@angular/material";

export interface matRangeDatepickerRangeValue<D> {
  begin: D | null;
  end: D | null;
}
export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      let day: string = date.getDate().toString();
      day = +day < 10 ? '0' + day : day;
      let month: string = (date.getMonth() + 1).toString();
      month = +month < 10 ? '0' + month : month;
      let year = date.getFullYear();
      return `${day}-${month}-${year}`;
    }
    return date.toDateString();
  }
}
export const APP_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
  },
  display: {
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'numeric' },
    dateA11yLabel: {
      year: 'numeric', month: 'long', day: 'numeric'
    },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};
@Component({
  selector: 'm-manager-analysis',
  templateUrl: './manager-analysis.component.html',
  styleUrls: ['./manager-analysis.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class ManagerAnalysisComponent implements OnInit {
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  currentUser = JSON.parse(sessionStorage.currentUser);
  mgrAnalysisForm: FormGroup;
  showDivisionFilter: boolean = false;
  showProcessing: boolean = false;
  dynamicTableHeader: any = [];
  data: any = [];
  showReport: boolean = false;
  isMultipleEmp: boolean = false;
  currentDate = new Date();
  isToggled = true;
  showEmployee: boolean = false;
  toggleFilter(event: MatSlideToggleChange) {
    console.log('toggle', event.checked);
    this.isToggled = event.checked;
  }
  @ViewChild('tableData') tableRef: ElementRef;

  constructor(private _toastrService: ToastrService, private utilService: UtilsService, private _DCRReportService: DcrReportsService, private _ChangeDetectorRef: ChangeDetectorRef) {
    this.mgrAnalysisForm = new FormGroup({
      designation: new FormControl('', Validators.required),
      status: new FormControl(true),
      userId: new FormControl('', Validators.required),
      dateRange: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {
      this.mgrAnalysisForm.setControl('division', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  getDivisionValue(val) {
    this.showReport = false;

    this.showProcessing = false;
    this.mgrAnalysisForm.patchValue({ division: val });
    //=======================Clearing Filter===================
    this.designationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================
    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.designationComponent.getDesignationBasedOnDivision(passingObj);
  }
//   getDesignationLevel(designation: Designation) {
//     this.showReport = false;

//     this.mgrAnalysisForm.patchValue({ designation: designation.designationLevel })
//   }
  getDesignation(val) {
   this.mgrAnalysisForm.patchValue({ designation: val.designationLevel });
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.mgrAnalysisForm.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel] //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.mgrAnalysisForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

    }
  }



  hideTable():void{
    this.showReport = false;
    this.showProcessing = false;
  }

  getStatusValue(val) {
	this.mgrAnalysisForm.patchValue({ status: val });
  }
  getEmployee(emp: string) {
	this.showProcessing = false;
    this.showReport = false;
    this.mgrAnalysisForm.patchValue({ userId: emp })
  }


  generateReport() {
    this.isToggled = false;

    // if (!this.mgrAnalysisForm.valid) {
    //   this._toastrService.error("Select the required field to generate the report", "All Field required");
    //   return;
    // }
    this.showReport = false;

    this.showProcessing = true;

    let params = {
      companyId: this.currentUser.companyId,
      userId: this.mgrAnalysisForm.value.userId,
      unlistedValidations: {
        unlistedDocCall: this.currentUser.company.validation.unlistedDocCall,
        unlistedVenCall: this.currentUser.company.validation.unlistedVenCall,
        unlistedDocAvg: this.currentUser.company.validation.unlistedDocAvg,
        unlistedVenAvg: this.currentUser.company.validation.unlistedVenAvg,
	  },
	  status:this.mgrAnalysisForm.value.status,
      fromDate: _moment(new Date(this.mgrAnalysisForm.value.dateRange.begin)).format("YYYY-MM-DD"),
      toDate: _moment(new Date(this.mgrAnalysisForm.value.dateRange.end)).format("YYYY-MM-DD")
    }
console.log("params......",params)
    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
      this._DCRReportService.getManagerAnalysis(params).subscribe(res => {
      this.showProcessing = false;
      this.data = res;
      this.showReport = true;
      this._ChangeDetectorRef.detectChanges();
      console.log(res);
    },err=>{
      this.showProcessing = false;
      this.showReport = false;
      this._ChangeDetectorRef.detectChanges();
    });

  }

  exportToExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tableRef.nativeElement );
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, ' Manager Analysis Report.xlsx');
  }
}

