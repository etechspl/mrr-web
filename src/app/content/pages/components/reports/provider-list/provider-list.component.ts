import { HierarchyService } from './../../../../../core/services/hierarchy-service/hierarchy.service';
import { UserAreaMappingService } from './../../../../../core/services/UserAreaMpping/user-area-mapping.service';
import { AreaComponent } from './../../filters/area/area.component';
import { ProviderService } from './../../../../../core/services/Provider/provider.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TypeComponent } from './../../filters/type/type.component';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatSlideToggleChange } from '@angular/material';
import { StatusComponent } from '../../filters/status/status.component';
import { StateComponent } from '../../filters/state/state.component';
import { ToastrService } from 'ngx-toastr';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { CategoryComponent } from '../../filters/category/category.component';
import * as _moment from "moment";
import { URLService } from '../../../../../core/services/URL/url.service';
declare var $;


@Component({
  selector: 'm-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.scss']
})
export class ProviderListComponent implements OnInit {

  public showReportType: boolean = true;
  public showType: boolean = true;
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showArea: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showDesignation: boolean = false;
  public isShowTeamInfo: boolean = false;
  public showDivisionFilter: boolean = false;
  public showCategory: boolean = false;
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  //dcrForm: FormGroup;
  @ViewChild("callAreaComponent") callAreaComponent: AreaComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callCategoryComponent") callCategoryComponent: CategoryComponent;

  constructor(private dialog: MatDialog, private _hierarchyservice: HierarchyService,
	 private _userareamappingservice: UserAreaMappingService, private _providerservice: ProviderService, private changedetectorref: ChangeDetectorRef, private _toastrService: ToastrService ,private _urlService: URLService) {
  }
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;

  displayedColumns = ['index', 'state', 'district', 'block', 'employeeName', 'providerName','contactPerson', 'providerCode', 'providerType', 'degree', 'specialization', 'category', 'address', 'phone', 'frequencyVisit', 'dob', 'doa', 'status', 'focusProduct', 'productAlreadyUsing'];
  //dataSource = new MatTableDataSource();
  dataSource: any = [];

  showProcessing: boolean = false;
  providerForm = new FormGroup({
    reportType: new FormControl('Geographical'),
	// type: new FormControl(null, Validators.required),
	type: new FormControl(null),
    division: new FormControl(null),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    areaInfo: new FormControl(null),
    employeeId: new FormControl(null),
    providerType: new FormControl(null, Validators.required),
    status: new FormControl(true),
    designation: new FormControl(null),
    category: new FormControl(null),
  })

  check: boolean;
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  providerTypes = [this.currentUser.company.lables.doctorLabel, this.currentUser.company.lables.vendorLabel, this.currentUser.company.lables.stockistLabel];
  chkArray = [];
  ngOnInit() {
    if (this.currentUser.company.lables.hasOwnProperty('other')) {
      this.currentUser.company.lables.otherType.forEach(i => {
        this.providerTypes.push(i.label)
        // Object.keys(i).forEach(j => {
        //   this.providerTypes.push(i[j]);
        // });
      });
    }
     

    if (this.currentUser.company.isDivisionExist == true) {
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
    this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithArea");
    if (this.currentUser.userInfo[0].rL == 1) {
      this.showReportType = false;
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showType = false;
      this.showArea = true;
      this.showDivisionFilter = false;

      this.callAreaComponent.getAreaList(this.currentUser.companyId, [], this.currentUser.userInfo[0].rL, [this.currentUser.userInfo[0].userId]);
    }
  }
  updateChkbxArray(chk, isChecked) {
    let type = "";
    if (isChecked) {
      if (chk == this.currentUser.company.lables.doctorLabel) {
        this.chkArray.push('RMP');
        this.showCategory = true;
        this.providerForm.get('category').setValidators([Validators.required])
        this.providerForm.get('category').updateValueAndValidity();
      } else if (chk == this.currentUser.company.lables.vendorLabel) {
        this.chkArray.push('Drug');
        this.showCategory = false;
        this.providerForm.get('category').clearValidators()
        this.providerForm.get('category').updateValueAndValidity();
        this.providerForm.patchValue({ category: [] });
      } else if (chk == this.currentUser.company.lables.stockistLabel) {
        this.chkArray.push('Stockist');
        this.showCategory = false;
        this.providerForm.get('category').clearValidators()
        this.providerForm.get('category').updateValueAndValidity();
        this.providerForm.patchValue({ category: [] });
      }
      else if (this.currentUser.company.lables.hasOwnProperty('otherType')) {
        this.currentUser.company.lables.otherType.forEach(i => {
          // Object.keys(i).forEach(j => {
          //   if (chk === i[j]) {
          //     this.chkArray.push(j);
          //     this.showCategory = false;

          //   }
          // });

             if (chk === i.label) {
              this.chkArray.push(i.key);
              this.showCategory = false;

            }
          this.providerForm.get('category').clearValidators()
          this.providerForm.get('category').updateValueAndValidity();
          this.providerForm.patchValue({ category: [] });
        });

      }
      this.changedetectorref.detectChanges();
    }
    else {
      if (chk == this.currentUser.company.lables.doctorLabel) {
        type = 'RMP';
        this.showCategory = false;
      } else if (chk == this.currentUser.company.lables.vendorLabel) {
        type = 'Drug';
        this.showCategory = false;
      } else if (chk == this.currentUser.company.lables.stockistLabel) {
        type = 'Stockist';
        this.showCategory = false;
      } else if (this.currentUser.company.lables.hasOwnProperty('otherType')) {
        this.currentUser.company.lables.otherType.forEach(i => {
          if (chk === i.label) {
                type = i.key;
                this.showCategory = false;
              }
          // Object.keys(i).forEach(j => {
          //   if (chk === i[j]) {
          //     type = j;
          //     this.showCategory = false;
          //   }
          // });
        });
      }
      this.providerForm.patchValue({ category: [] });
      this.changedetectorref.detectChanges();
      let idx = this.chkArray.indexOf(type);
      if (idx !== -1) {
        this.chkArray.splice(idx, 1);
      }
    }

  }
  getReportType(val) {

    this.providerForm.patchValue({ type: '' });
    this.providerForm.patchValue({ division: '' });
    this.providerForm.patchValue({ stngateInfo: '' });
    this.showProcessing = false;
    this.showReport = false;
    this.callDivisionComponent.setBlank();
    this.callStateComponent.setBlank();
    this.callDistrictComponent.setBlank();

    if (val === "Geographical") {
      this.showState = false;
      this.showDesignation = false;
      this.showEmployee = false;
      this.showCategory = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.callDivisionComponent.setBlank();
        this.callStateComponent.setBlank();
        this.showDivisionFilter = true;
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithArea");

    } else {
      this.showDesignation = false;
      this.showEmployee = false;
      this.showDistrict = false;
      this.showState = false;
      this.showArea = false;
      this.showDivisionFilter = false;
      this.showCategory = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }

  }

  getTypeValue(val) {

    this.showProcessing = false;
    this.showReport = false;
    this.showCategory = false;
    this.providerForm.patchValue({ type: val });

    if ((val == "Headquarter" || val == "Area") && (this.currentUser.userInfo[0].rL > 1 || this.currentUser.userInfo[0].rL == 0)) {
      this.showState = true;
      this.showDistrict = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.providerForm.removeControl('employeeId');
      this.providerForm.removeControl('designation');
      this.providerForm.setControl("stateInfo", new FormControl('', Validators.required));
      this.providerForm.setControl("districtInfo", new FormControl('', Validators.required));
    } else {
      this.showDistrict = false;
    }
    if (val == "Area") {
      this.showArea = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.providerForm.setControl("stateInfo", new FormControl('', Validators.required));
      this.providerForm.setControl("districtInfo", new FormControl('', Validators.required));
    } else {
      this.showArea = false;
    }

    if (this.currentUser.userInfo[0].rL === 0 && val === 'State') {


      //=======================Clearing Filter===================
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }

      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      //===============================END=======================
      this.providerForm.removeControl('employeeId');
      this.providerForm.removeControl('designation');
      this.providerForm.setControl("stateInfo", new FormControl('', Validators.required));
      this.showState = true;
    } else {
      if (this.providerForm.value.type === 'Self') {
        this.showState = false;
        this.isShowTeamInfo = false;
        this.showDesignation = false;
        this.showEmployee = false;
        this.showDivisionFilter = false;
      } else if (this.providerForm.value.type === 'Employee Wise') {
        this.showState = false;
        this.isShowTeamInfo = true;
        this.showDesignation = true;
        this.showEmployee = true;
        this.providerForm.removeControl('stateInfo');
        this.providerForm.removeControl('districtInfo');

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================
        this.providerForm.setControl('employeeId', new FormControl('', Validators.required))
        this.providerForm.setControl('designation', new FormControl('', Validators.required))

        if (this.currentUser.company.isDivisionExist == false) {
          this.showDivisionFilter = false;
        } else if (this.currentUser.company.isDivisionExist == true) {
          this.showDivisionFilter = true;
        }
      } else if (this.providerForm.value.type === 'State' && this.currentUser.company.isDivisionExist == false) {
        this.showState = true;
        this.callStateComponent.getUniqueStateOnManager(this.currentUser.companyId, this.currentUser.id)
      } else if (this.providerForm.value.type === 'State' && this.currentUser.company.isDivisionExist == true) {
        this.showState = true;
      }

    }
  }



  getDivisionValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.showCategory = false;
    this.providerForm.patchValue({ division: val });

    // if (this.currentUser.userInfo[0].designationLevel == 0) {
    //   let divisionIds = {
    //     division: val
    //   };
    //   this.callStateComponent.getStateBasedOnDivision(divisionIds);

    // } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
    //   let passingObj = {
    //     companyId: this.currentUser.companyId,
    //     isDivisionExist: true,
    //     division: this.providerForm.value.division,
    //     supervisorId: this.currentUser.id,
    //   }
    //   this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)
    // }


    if (this.currentUser.userInfo[0].designationLevel == 0) {
      if (this.providerForm.value.type == "Employee Wise") {
        //=======================Clearing Filter===================

        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.providerForm.value.type == "State" || this.providerForm.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================


        let divisionIds = {
          division: val
        };
        this.callStateComponent.getStateBasedOnDivision(divisionIds);
      }
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      if (this.providerForm.value.type == "Employee Wise") {
        //=======================Clearing Filter===================
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.providerForm.value.type == "State" || this.providerForm.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================

        let passingObj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: true,
          division: this.providerForm.value.division,
          supervisorId: this.currentUser.id,
        }
        this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)

      }
    }

  }

  getStateValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.showCategory = false;
    this.providerForm.patchValue({ stateInfo: val });

    this.callDistrictComponent.setBlank();

    if (this.providerForm.value.type == "Headquarter" || this.providerForm.value.type == "Area") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.providerForm.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.showCategory = false;
    this.providerForm.patchValue({ districtInfo: val });
    this.callAreaComponent.getAreaList(this.currentUser.companyId, val, this.currentUser.userInfo[0].rL, [this.currentUser.id]);

  }
  getAreaValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.showCategory = false;
    this.providerForm.patchValue({ areaInfo: val });
  }
  getEmployeeValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.showCategory = false;
    this.providerForm.patchValue({ employeeId: val });
  }

  getDesignation(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.showCategory = false;
    this.providerForm.patchValue({ designation: val.designationLevel });


    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.providerForm.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
      }
    } else {

      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel] //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.providerForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

      // this.callEmployeeComponent.getManagerHierarchy(this.currentUser.companyId, val[0].designationLevel, [true]);
    }
  }
  getCategory(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.providerForm.patchValue({ category: val });
  }
  getStatusValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.providerForm.patchValue({ status: val });
  }

  viewRecords() {
    this.isToggled = false;
    let userIds;
    if (this.providerForm.value.type == "Self" || this.providerForm.value.type == null) { //Null is for MR level
      userIds = [this.currentUser.id];
    } else {
      userIds = this.providerForm.value.employeeId
    }
    this.providerForm.value.providerType = this.chkArray;
    this.showReport = false;
    this.showProcessing = true;

    if (this.providerForm.value.type == 'Employee Wise' || this.currentUser.userInfo[0].rL >= 2 || this.currentUser.userInfo[0].rL >= 0) {

      if (this.currentUser.userInfo[0].rL == 0) {
        if (this.providerForm.value.type == 'State' || this.providerForm.value.type == 'Headquarter' || this.providerForm.value.type == 'Area') {
          this._userareamappingservice.getProvidersList(this.currentUser.companyId, this.providerForm.value.type, this.providerForm.value.stateInfo, this.providerForm.value.districtInfo, this.providerForm.value.areaInfo, userIds, this.providerForm.value.providerType, [this.providerForm.value.status], this.providerForm.value.designation, this.providerForm.value.category,this.providerForm.value.division,this.currentUser.company.isDivisionExist).subscribe(res => {
console.log("mahender",res)

            if (res == null || res.length == 0 || res == undefined) {
              this.showReport = false;
              this.showProcessing = false;
              this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
            } else {

              res.map((item, index) => {
                item['index'] = index;
            });

              this.dataSource = res;

              this.showReport = true;
              this.showProcessing = false;

              let docLabel = this.currentUser.company.lables.doctorLabel;
              let venLabel = this.currentUser.company.lables.vendorLabel;
              let stockLabel = this.currentUser.company.lables.stockistLabel;
    
              const PrintTableFunction = this.PrintTableFunction.bind(this);
              if(this.currentUser.companyId == '6138dfcb609535159491a211'){
                this.dtOptions = {
                  "pagingType": 'full_numbers',
                  "paging": true,
                  "ordering": true,
                  "info": true,
                  "scrollY": 300,
                  "scrollX": true,
                  "fixedColumns": true,
                  destroy: true,
                  data: this.dataSource,
                  columns: [
                    {
                      title: 'S. NO comt.',
                      data: 'index',
                      render: function (data) {
                        if (data === '') {
                          return '---'
                        } else {
                          return data + 1;
                        }
                      },
                      defaultContent: '---'
                    },
                    {
                    title: 'Division',
                    data: 'divisionName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'STATE NAME',
                    data: 'stateName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'HEADQUARTER',
                    data: 'districtName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'AREA',
                    data: 'areaName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'EMPLOYEE NAME',
                    data: 'name',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'DR NAME ',
                    data: 'providerName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  
                  {
                    title: 'DATE OF BIRTH',
                    data: 'dob',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return _moment(data).format(
                          "DD-MM-YYYY"
                          );
                      }
                    },
                    defaultContent: '---'
                  },
        
                  {
                    title: 'DATE OF ANNEIVERSARY',
                    data: 'doa',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return _moment(data).format(
                          "DD-MM-YYYY"
                          );
                      }
                    },
                    defaultContent: '---'
                  },
                
                  {
                    title: 'DEGREE',
                    data: 'degree',
                    render: function (data) {
                      if (data === '') {
            
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },{
                    title: 'WHATSAPP',
                    data: 'Whatsapp',
                    render: function (data) {
                      if (data === '') {
            
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'SPECIALIZATION',
                    data: 'specialization',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'CATEGORY',
                    data: 'category',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'CITY',
                    data: 'city',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'ADDRESS',
                    data: 'address',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'CONTACT ',
                    data: 'phone',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'EMAIL',
                    data: 'email',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'FREQUENCY VISIT',
                    data: 'frequencyVisit',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'FOCUS PRODUCTS',
                    data: 'focus',
                    render: (data) => {
                      if (data === '' || data == undefined) {
                        return '---';
                      } else {
                        return data.toString();
                      }
                    }
                  },
                  {
                    title: 'PRODUCTS ALREADY USING',
                    data: 'productAlready',
                    render: (data) => {
                      if (data === '' || data == undefined) {
                        return '---';
                      } else {
                        return data.toString();
                      }
                    }
                  }
                  ],
                  language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                  },
                  dom: 'Bfrtip',
            
                  buttons: [
                    {
                      extend: 'excel',
                      exportOptions: {
                        columns: ':visible'
                      }
                    },
                    {
                      extend: 'csv',
                      exportOptions: {
                        columns: ':visible'
                      }
                    },
                    {
                      extend: 'copy',
                      exportOptions: {
                        columns: ':visible'
                      }
                    },
                    
                  {
                    extend: 'print',
                    title: function () {
                    return 'State List';
                    },
                    exportOptions: {
                    columns: ':visible'
                    },
                    action: function (e, dt, node, config) {
                    PrintTableFunction(res);
                        }
                       }
                  ]
                };
              }
              else{
                this.dtOptions = {
                  "pagingType": 'full_numbers',
                  "paging": true,
                  "ordering": true,
                  "info": true,
                  "scrollY": 300,
                  "scrollX": true,
                  "fixedColumns": true,
                  destroy: true,
                  data: this.dataSource,
                  columns: [
                    {
                      title: 'S. No.',
                      data: 'index',
                      render: function (data) {
                        if (data === '') {
                          return '---'
                        } else {
                          return data + 1;
                        }
                      },
                      defaultContent: '---'
                    },
                    {
                    title: 'Division',
                    data: 'divisionName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'State Name',
                    data: 'stateName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: this.currentUser.company.lables.hqLabel,
                    data: 'districtName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: this.currentUser.company.lables.areaLabel,
                    data: 'areaName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Employee Name',
                    data: 'name',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Provider Name ',
                    data: 'providerName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Provider Number',
                    data: 'contactPerson',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Provider Code',
                    data: 'providerCode',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'date of Birth',
                    data: 'dob',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return _moment(data).format(
                          "DD-MM-YYYY"
                          );
                      }
                    },
                    defaultContent: '---'
                  },
        
                  {
                    title: 'date of Anniversary',
                    data: 'doa',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return _moment(data).format(
                          "DD-MM-YYYY"
                          );
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Provider Type',
                    data: 'providerType',
                    render: function (data) {
                      if (data === 'RMP') {
                        return docLabel
                      }
                      else if (data === 'Drug') {
                        return venLabel
                      }
                      else if (data === 'Stockist') {
                        return stockLabel
                      }
                      else if (data === 'hospitalManagement') {
                        return "Hospital Management"
                      }
                      else if (data === 'purchaseManager') {
                        return "Purchase Manager"
                      }
                      else {
                        return "---"
                      }
            
                    },
                    defaultContent: '---'
                  },
                 
                  {
                    title: 'Facebook',
                    data: 'facebook',
                    render: function (data) {
                      if (data === '') {
            
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Instagram',
                    data: 'instagram',
                    render: function (data) {
                      if (data === '') {
            
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },{
                    title: 'Twitter',
                    data: 'twitter',
                    render: function (data) {
                      if (data === '') {
            
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'LinkedIn',
                    data: 'linkedin',
                    render: function (data) {
                      if (data === '') {
            
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Degree',
                    data: 'degree',
                    render: function (data) {
                      if (data === '') {
            
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Specialization',
                    data: 'specialization',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Category',
                    data: 'category',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'City',
                    data: 'city',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Address',
                    data: 'address',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Phone',
                    data: 'phone',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Email',
                    data: 'email',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Frequency Visit',
                    data: 'frequencyVisit',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
                  },
                  {
                    title: 'Focus Products',
                    data: 'focus',
                    render: (data) => {
                      if (data === '' || data == undefined) {
                        return '---';
                      } else {
                        return data.toString();
                      }
                    }
                  },
                  {
                    title: 'Products Already Using',
                    data: 'productAlready',
                    render: (data) => {
                      if (data === '' || data == undefined) {
                        return '---';
                      } else {
                        return data.toString();
                      }
                    }
                  }
                  ],
                  language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                  },
                  dom: 'Bfrtip',
            
                  buttons: [
                    {
                      extend: 'excel',
                      exportOptions: {
                        columns: ':visible'
                      }
                    },
                    {
                      extend: 'csv',
                      exportOptions: {
                        columns: ':visible'
                      }
                    },
                    {
                      extend: 'copy',
                      exportOptions: {
                        columns: ':visible'
                      }
                    },
                    
                  {
                    extend: 'print',
                    title: function () {
                    return 'State List';
                    },
                    exportOptions: {
                    columns: ':visible'
                    },
                    action: function (e, dt, node, config) {
                    PrintTableFunction(res);
                        }
                       }
                  ]
                };
              }
        
             // this.getTableDetails();

              if (this.currentUser.company.isDivisionExist == false) {
                this.dtOptions.columns[1].visible = false;
              }

              this.dataTable = $(this.table.nativeElement);
              this.dataTable.DataTable(this.dtOptions);

            }
            this.changedetectorref.detectChanges();
          }, err => {
            console.log(err)
            this.showReport = false;
            //this.showProcessing = false;
            this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
            this.changedetectorref.detectChanges();
          })
        } else {
          this._userareamappingservice.getMappedAreasOfEmployees(this.currentUser.companyId, this.providerForm.value.employeeId).subscribe(areasres => {
            let areas = [];
            if (areasres == null || areasres.length == 0 || areasres == undefined) {
              this.showReport = false;
              this.showProcessing = false;
              if (this.providerForm.value.designation == 1) {
                this._toastrService.info('Record is not found for selected filter.', 'No Record Found');

              }
            } else {
              for (let n = 0; n < areasres.length; n++) {
                areas.push(areasres[n].areaId);
              }
            }
            this.changedetectorref.detectChanges();
            this._userareamappingservice.getProvidersList(this.currentUser.companyId, this.providerForm.value.type, this.providerForm.value.stateInfo, this.providerForm.value.districtInfo, areas, this.providerForm.value.employeeId, this.providerForm.value.providerType, [this.providerForm.value.status], this.providerForm.value.designation, this.providerForm.value.category,this.providerForm.value.division,this.currentUser.company.isDivisionExist).subscribe(res => {
              if (res == null || res.length == 0 || res == undefined) {
                this.showReport = false;
                this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
              } else {

                res.map((item, index) => {
                  item['index'] = index;
              });
                this.dataSource = res;
                this.showReport = true;
                this.showProcessing = true;

                let docLabel = this.currentUser.company.lables.doctorLabel;
                let venLabel = this.currentUser.company.lables.vendorLabel;
                let stockLabel = this.currentUser.company.lables.stockistLabel;
      
                const PrintTableFunction = this.PrintTableFunction.bind(this);
          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            destroy: true,
            data: this.dataSource,
            columns: [
              {
                title: 'S. No.',
                data: 'index',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data + 1;
                  }
                },
                defaultContent: '---'
              },
              {
              title: 'Division',
              data: 'divisionName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'State Name',
              data: 'stateName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.hqLabel,
              data: 'districtName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.areaLabel,
              data: 'areaName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Employee Name',
              data: 'name',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Provider Name ',
              data: 'providerName',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Provider Number',
              data: 'contactPerson',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Provider Code',
              data: 'providerCode',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },

            {
              title: 'date of Birth',
              data: 'dob',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return _moment(data).format(
                    "DD-MM-YYYY"
                    );
                }
              },
              defaultContent: '---'
            },

            {
              title: 'date of Anniversary',
              data: 'doa',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return _moment(data).format(
                    "DD-MM-YYYY"
                    );
                }
              },
              defaultContent: '---'
            },

            {
              title: 'Provider Type',
              data: 'providerType',
              render: function (data) {
                if (data === 'RMP') {
                  return docLabel
                }
                else if (data === 'Drug') {
                  return venLabel
                }
                else if (data === 'Stockist') {
                  return stockLabel
                }
                else if (data === 'hospitalManagement') {
                  return "Hospital Management"
                }
                else if (data === 'purchaseManager') {
                  return "Purchase Manager"
                }
                else {
                  return "---"
                }
      
              },
              defaultContent: '---'
            },

            {
              title: 'Facebook',
              data: 'facebook',
              render: function (data) {
                if (data === '') {
      
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Instagram',
              data: 'instagram',
              render: function (data) {
                if (data === '') {
      
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },{
              title: 'Twitter',
              data: 'twitter',
              render: function (data) {
                if (data === '') {
      
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'LinkedIn',
              data: 'linkedin',
              render: function (data) {
                if (data === '') {
      
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Degree',
              data: 'degree',
              render: function (data) {
                if (data === '') {
      
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Specialization',
              data: 'specialization',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Category',
              data: 'category',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'City',
              data: 'city',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Address',
              data: 'address',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Phone',
              data: 'phone',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Email',
              data: 'email',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Frequency Visit',
              data: 'frequencyVisit',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Focus Products',
              data: 'focus',
              render: (data) => {
                if (data === '' || data == undefined) {
                  return '---';
                } else {
                  return data.toString();
                }
              }
            },
            {
              title: 'Products Already Using',
              data: 'productAlready',
              render: (data) => {
                if (data === '' || data == undefined) {
                  return '---';
                } else {
                  return data.toString();
                }
              }
            }
            ],
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search records",
            },
            dom: 'Bfrtip',
      
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'copy',
                exportOptions: {
                  columns: ':visible'
                }
              },
              
            {
              extend: 'print',
              title: function () {
              return 'State List';
              },
              exportOptions: {
              columns: ':visible'
              },
              action: function (e, dt, node, config) {
              PrintTableFunction(res);
                  }
                 }
            ]
          };

               // this.getTableDetails();
                if (this.currentUser.company.isDivisionExist == false) {
                  this.dtOptions.columns[1].visible = false;
                }
                this.dataTable = $(this.table.nativeElement);
                this.dataTable.DataTable(this.dtOptions);

              }
              this.showProcessing = false;
              this.changedetectorref.detectChanges();
            }, err => {
              console.log(err)
              this.showReport = false;
              this.showProcessing = false;
              //this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
              this.changedetectorref.detectChanges();

            })
          })
        }


      } else {

        let obj = {}
        if (this.providerForm.value.type === 'Self') {
          obj = {
            "companyId": this.currentUser.companyId,
            "userId": [this.currentUser.userInfo[0].userId]
          }
          this._userareamappingservice.getProvidersList(this.currentUser.companyId, 'Employee Wise', [this.currentUser.userInfo[0].stateId], [this.currentUser.userInfo[0].districtId], null, userIds, this.providerForm.value.providerType, [this.providerForm.value.status], this.providerForm.value.designation, this.providerForm.value.category,this.providerForm.value.division,this.currentUser.company.isDivisionExist).subscribe(res => {
            if (res == null || res.length == 0 || res == undefined) {
              this.showReport = false;

              this._toastrService.info('Record is not found for selected filter.', 'No Record Found');

            } else {


              res.map((item, index) => {
                item['index'] = index;
            });


              this.dataSource = res;
              this.showReport = true;
              this.showProcessing = false;

              let docLabel = this.currentUser.company.lables.doctorLabel;
          let venLabel = this.currentUser.company.lables.vendorLabel;
          let stockLabel = this.currentUser.company.lables.stockistLabel;

          const PrintTableFunction = this.PrintTableFunction.bind(this);
    this.dtOptions = {
      "pagingType": 'full_numbers',
      "paging": true,
      "ordering": true,
      "info": true,
      "scrollY": 300,
      "scrollX": true,
      "fixedColumns": true,
      destroy: true,
      data: this.dataSource,
      columns: [
        {
          title: 'S. No.',
          data: 'index',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data + 1;
            }
          },
          defaultContent: '---'
        },
        {
        title: 'Division',
        data: 'divisionName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'State Name',
        data: 'stateName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: this.currentUser.company.lables.hqLabel,
        data: 'districtName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: this.currentUser.company.lables.areaLabel,
        data: 'areaName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Employee Name',
        data: 'name',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Name ',
        data: 'providerName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Number',
        data: 'contactPerson',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Code',
        data: 'providerCode',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'date of Birth',
        data: 'dob',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return _moment(data).format(
              "DD-MM-YYYY"
              );
          }
        },
        defaultContent: '---'
      },

      {
        title: 'date of Anniversary',
        data: 'doa',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return _moment(data).format(
              "DD-MM-YYYY"
              );
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Type',
        data: 'providerType',
        render: function (data) {
          if (data === 'RMP') {
            return docLabel
          }
          else if (data === 'Drug') {
            return venLabel
          }
          else if (data === 'Stockist') {
            return stockLabel
          }
          else if (data === 'hospitalManagement') {
            return "Hospital Management"
          }
          else if (data === 'purchaseManager') {
            return "Purchase Manager"
          }
          else {
            return "---"
          }

        },
        defaultContent: '---'
      },
     
      {
        title: 'Facebook',
        data: 'facebook',
        render: function (data) {
          if (data === '') {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Instagram',
        data: 'instagram',
        render: function (data) {
          if (data === '') {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },{
        title: 'Twitter',
        data: 'twitter',
        render: function (data) {
          if (data === '') {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'LinkedIn',
        data: 'linkedin',
        render: function (data) {
          if (data === '') {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Degree',
        data: 'degree',
        render: function (data) {
          if (data === '') {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Specialization',
        data: 'specialization',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Category',
        data: 'category',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'City',
        data: 'city',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Address',
        data: 'address',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Phone',
        data: 'phone',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Email',
        data: 'email',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Frequency Visit',
        data: 'frequencyVisit',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Focus Products',
        data: 'focus',
        render: (data) => {
          if (data === '' || data == undefined) {
            return '---';
          } else {
            return data.toString();
          }
        }
      },
      {
        title: 'Products Already Using',
        data: 'productAlready',
        render: (data) => {
          if (data === '' || data == undefined) {
            return '---';
          } else {
            return data.toString();
          }
        }
      }
      ],
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      },
      dom: 'Bfrtip',

      buttons: [
        {
          extend: 'excel',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'csv',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'copy',
          exportOptions: {
            columns: ':visible'
          }
        },
        
			{
				extend: 'print',
				title: function () {
				return 'State List';
				},
				exportOptions: {
				columns: ':visible'
				},
				action: function (e, dt, node, config) {
				PrintTableFunction(res);
			    	}
		     	}
      ]
    };

             // this.getTableDetails();
              if (this.currentUser.company.isDivisionExist == false) {
                this.dtOptions.columns[1].visible = false;
              }
              this.dataTable = $(this.table.nativeElement);
              this.dataTable.DataTable(this.dtOptions);

            }
            this.showProcessing = false;

            this.changedetectorref.detectChanges();

          }, err => {
            console.log(err)
            this.showReport = false;
            this.showProcessing = false;
            this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
            this.changedetectorref.detectChanges();

          })
        } else {
          obj = {
            "companyId": this.currentUser.companyId,
            "supervisorId": [this.currentUser.userInfo[0].userId]
          }
          this._hierarchyservice.getManagerHierarchyInArray(obj).subscribe(hirarchyres => {
            if (hirarchyres == null || hirarchyres == undefined) {
              this.showReport = false;
              this.showProcessing = false;
              this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
              this.changedetectorref.detectChanges();
            } else {
              let stateIds = []
              let districtIds = [];
              if (this.providerForm.value.type == "State") {
                stateIds = this.providerForm.value.stateInfo;
              } else if (this.providerForm.value.type == "Headquarter" || this.providerForm.value.type == "Area") {
                stateIds = this.providerForm.value.stateInfo;
                districtIds = this.providerForm.value.districtInfo;
              } else if (this.providerForm.value.type == null) { // For MR Level
                stateIds = [this.currentUser.userInfo[0].stateId];
                districtIds = [this.currentUser.userInfo[0].districtId];
              }
              this._userareamappingservice.getProvidersList(this.currentUser.companyId, this.providerForm.value.type, stateIds, districtIds, this.providerForm.value.areaInfo, userIds, this.providerForm.value.providerType, [this.providerForm.value.status], this.providerForm.value.designation, this.providerForm.value.category,this.providerForm.value.division,this.currentUser.company.isDivisionExist).subscribe(res => {
                if (res == null || res.length == 0 || res == undefined) {
                  this.showReport = false;
                  this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
                } else {


                  res.map((item, index) => {
                    item['index'] = index;
                });


                  this.dataSource = res;
                  this.showReport = true;
                  this.showProcessing = false;

                  let docLabel = this.currentUser.company.lables.doctorLabel;
                  let venLabel = this.currentUser.company.lables.vendorLabel;
                  let stockLabel = this.currentUser.company.lables.stockistLabel;
        
                  const PrintTableFunction = this.PrintTableFunction.bind(this);
            this.dtOptions = {
              "pagingType": 'full_numbers',
              "paging": true,
              "ordering": true,
              "info": true,
              "scrollY": 300,
              "scrollX": true,
              "fixedColumns": true,
              destroy: true,
              data: this.dataSource,
              columns: [
                {
                  title: 'S. No.',
                  data: 'index',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data + 1;
                    }
                  },
                  defaultContent: '---'
                },
                {
                title: 'Division',
                data: 'divisionName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'State Name',
                data: 'stateName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.hqLabel,
                data: 'districtName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.areaLabel,
                data: 'areaName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Employee Name',
                data: 'name',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Provider Name ',
                data: 'providerName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Provider Number',
                data: 'contactPerson',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Provider Code',
                data: 'providerCode',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Provider Type',
                data: 'providerType',
                render: function (data) {
                  if (data === 'RMP') {
                    return docLabel
                  }
                  else if (data === 'Drug') {
                    return venLabel
                  }
                  else if (data === 'Stockist') {
                    return stockLabel
                  }
                  else if (data === 'hospitalManagement') {
                    return "Hospital Management"
                  }
                  else if (data === 'purchaseManager') {
                    return "Purchase Manager"
                  }
                  else {
                    return "---"
                  }
        
                },
                defaultContent: '---'
              },

              {
                title: 'date of Birth',
                data: 'dob',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return _moment(data).format(
                      "DD-MM-YYYY"
                      );
                  }
                },
                defaultContent: '---'
              },
        
              {
                title: 'date of Anniversary',
                data: 'doa',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return _moment(data).format(
                      "DD-MM-YYYY"
                      );
                  }
                },
                defaultContent: '---'
              },
             
              {
                title: 'Facebook',
                data: 'facebook',
                render: function (data) {
                  if (data === '') {
        
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Instagram',
                data: 'instagram',
                render: function (data) {
                  if (data === '') {
        
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Twitter',
                data: 'twitter',
                render: function (data) {
                  if (data === '') {
        
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'LinkedIn',
                data: 'linkedin',
                render: function (data) {
                  if (data === '') {
        
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Degree',
                data: 'degree',
                render: function (data) {
                  if (data === '') {
        
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Specialization',
                data: 'specialization',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Category',
                data: 'category',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'City',
                data: 'city',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Address',
                data: 'address',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Phone',
                data: 'phone',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Email',
                data: 'email',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Frequency Visit',
                data: 'frequencyVisit',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Focus Products',
                data: 'focus',
                render: (data) => {
                  if (data === '' || data == undefined) {
                    return '---';
                  } else {
                    return data.toString();
                  }
                }
              },
              {
                title: 'Products Already Using',
                data: 'productAlready',
                render: (data) => {
                  if (data === '' || data == undefined) {
                    return '---';
                  } else {
                    return data.toString();
                  }
                }
              }
              ],
              language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
              },
              dom: 'Bfrtip',
        
              buttons: [
                {
                  extend: 'excel',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'csv',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                {
                  extend: 'copy',
                  exportOptions: {
                    columns: ':visible'
                  }
                },
                
              {
                extend: 'print',
                title: function () {
                return 'State List';
                },
                exportOptions: {
                columns: ':visible'
                },
                action: function (e, dt, node, config) {
                PrintTableFunction(res);
                    }
                   }
              ]
            };
                  

                  //this.getTableDetails();
                  if (this.currentUser.company.isDivisionExist == false) {
                    this.dtOptions.columns[1].visible = false;
                  }
                  this.dataTable = $(this.table.nativeElement);
                  this.dataTable.DataTable(this.dtOptions);

                }
                this.showProcessing = false;

                this.changedetectorref.detectChanges();
              }, err => {
                console.log(err)
                this.showReport = false;
                this.showProcessing = false;
                this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
                this.changedetectorref.detectChanges();

              })
            }
          });
        }
      }
    } else {
      this._userareamappingservice.getProvidersList(this.currentUser.companyId, this.providerForm.value.type, [this.currentUser.userInfo[0].stateId], [this.currentUser.userInfo[0].districtId], this.providerForm.value.areaInfo, userIds, this.providerForm.value.providerType, [this.providerForm.value.status], this.providerForm.value.designation, this.providerForm.value.category,this.providerForm.value.division,this.currentUser.company.isDivisionExist).subscribe(res => {
        if (res == null || res.length == 0 || res == undefined) {
          this.showReport = false;
          this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
        } else {

          res.map((item, index) => {
            item['index'] = index;
        });


          this.dataSource = res;
          this.showReport = true;
          this.showProcessing = false;

          let docLabel = this.currentUser.company.lables.doctorLabel;
          let venLabel = this.currentUser.company.lables.vendorLabel;
          let stockLabel = this.currentUser.company.lables.stockistLabel;

          const PrintTableFunction = this.PrintTableFunction.bind(this);
    this.dtOptions = {
      "pagingType": 'full_numbers',
      "paging": true,
      "ordering": true,
      "info": true,
      "scrollY": 300,
      "scrollX": true,
      "fixedColumns": true,
      destroy: true,
      data: this.dataSource,
      columns: [
        {
          title: 'S. No.',
          data: 'index',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data + 1;
            }
          },
          defaultContent: '---'
        },
        {
        title: 'Division',
        data: 'divisionName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'State Name',
        data: 'stateName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: this.currentUser.company.lables.hqLabel,
        data: 'districtName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: this.currentUser.company.lables.areaLabel,
        data: 'areaName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Employee Name',
        data: 'name',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Name ',
        data: 'providerName',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Number',
        data: 'contactPerson',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Code',
        data: 'providerCode',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Provider Type',
        data: 'providerType',
        render: function (data) {
          if (data === 'RMP') {
            return docLabel
          }
          else if (data === 'Drug') {
            return venLabel
          }
          else if (data === 'Stockist') {
            return stockLabel
          }
          else if (data === 'hospitalManagement') {
            return "Hospital Management"
          }
          else if (data === 'purchaseManager') {
            return "Purchase Manager"
          }
          else {
            return "---"
          }

        },
        defaultContent: '---'
      },
     
      {
        title: 'date of Birth',
        data: 'dob',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return _moment(data).format(
              "DD-MM-YYYY"
              );
          }
        },
        defaultContent: '---'
      },

      {
        title: 'date of Anniversary',
        data: 'doa',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return _moment(data).format(
              "DD-MM-YYYY"
              );
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Facebook',
        data: 'facebook',
        render: function (data) {
          if (data === ''||data === undefined) {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Instagram',
        data: 'instagram',
        render: function (data) {
          if (data === ''||data === undefined) {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },{
        title: 'Twitter',
        data: 'twitter',
        render: function (data) {
          if (data === ''||data === undefined) {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'LinkedIn',
        data: 'linkedin',
        render: function (data) {
          if (data === ''||data === undefined) {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Degree',
        data: 'degree',
        render: function (data) {
          if (data === '') {

            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Specialization',
        data: 'specialization',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Category',
        data: 'category',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'City',
        data: 'city',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Address',
        data: 'address',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Phone',
        data: 'phone',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Email',
        data: 'email',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Frequency Visit',
        data: 'frequencyVisit',
        render: function (data) {
          if (data === '') {
            return '---'
          } else {
            return data
          }
        },
        defaultContent: '---'
      },
      {
        title: 'Focus Products',
        data: 'focus',
        render: (data) => {
          if (data === '' || data == undefined) {
            return '---';
          } else {
            return data.toString();
          }
        }
      },
      {
        title: 'Products Already Using',
        data: 'productAlready',
        render: (data) => {
          if (data === '' || data == undefined) {
            return '---';
          } else {
            return data.toString();
          }
        }
      }
      ],
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      },
      dom: 'Bfrtip',

      buttons: [
        {
          extend: 'excel',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'csv',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'copy',
          exportOptions: {
            columns: ':visible'
          }
        },
        
			{
				extend: 'print',
				title: function () {
				return 'State List';
				},
				exportOptions: {
				columns: ':visible'
				},
				action: function (e, dt, node, config) {
				PrintTableFunction(res);
			    	}
		     	}
      ]
    };
          
          //this.getTableDetails();

          if (this.currentUser.company.isDivisionExist == false) {
            this.dtOptions.columns[1].visible = false;
          }
          this.dataTable = $(this.table.nativeElement);
          this.dataTable.DataTable(this.dtOptions);

        }
        this.showProcessing = false;

        this.changedetectorref.detectChanges();

      }, err => {
        console.log(err)
        this.showReport = false;
        this.showProcessing = false;
        this._toastrService.info('Record is not found for selected filter.', 'No Record Found');
        this.changedetectorref.detectChanges();

      })
    }
  }

  
 
PrintTableFunction(data?: any): void {
const companyName = this.currentUser.companyName;
  
	const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

  const tableRows: any = [];
  let docLabel = this.currentUser.company.lables.doctorLabel;
  let venLabel = this.currentUser.company.lables.vendorLabel;
  let stockLabel = this.currentUser.company.lables.stockistLabel;
 

	data.forEach(element => {

    if(element.providerType === "RMP"){
      element.providerType = docLabel
    }else if(element.providerType === "Drug"){
      element.providerType= venLabel
    }else if(element.providerType === "Stockist"){
      element.providerType = stockLabel
    }

    
    if(element.providerCode === undefined ){
      element.providerCode = "----"
          }
          if(element.contactPerson === undefined ){
            element.contactPerson = "----"
                }
    if(element.facebook === undefined ){
element.facebook = "----"
    }
    if(element.instagram === undefined ){
      element.instagram = "----"
          }

          if(element.twitter === undefined ){
            element.twitter = "----"
                }

                if(element.linkedin === undefined ){
                  element.linkedin = "----"
                      }
                      if(element.degree === undefined ||element.degree ==="" ||element.degree === null){
                        element.degree = "----"
                            }
                            if(element.specialization === undefined || element.specialization ==="" ||element.specialization === null){
                              element.specialization = "----"
                                  }
                                  
                                  if(element.category === undefined ||element.category ===""||element.category === null){
                                    element.category = "----"
                                        }
                  
                            
                      if(element.city === undefined ||element.city === "" ||element.city === null){
                        element.city = "----"
                            }

                            if(element.address === undefined ||element.address === "" || element.address === null){
                              element.address = "----"
                                  }
                                  if(element.phone === undefined ||element.phone === "" || element.phone === null){
                                    element.phone = "----"
                                        }
      

                                  if(element.email === undefined ||element.email === ""||element.email === null){
                              element.email = "----"
                                  }
                                  if(element.frequencyVisit === undefined ||element.frequencyVisit === "" ||element.frequencyVisit ===null){
                                    element.frequencyVisit = "----"
                                        }

    tableRows.push(`<tr>
    <td class="tg-oiyu">${element.index}</td>
    <td class="tg-oiyu">${element.stateName}</td>
    <td class="tg-oiyu">${element.districtName}</td>
    <td class="tg-oiyu">${element.areaName}</td>
    <td class="tg-oiyu">${element.name}</td>
    <td class="tg-oiyu">${element.providerName}</td>
    <td class="tg-oiyu">${element.providerCode}</td>
    <td class="tg-oiyu">${element.contactPerson}</td>

    <td class="tg-oiyu">${element.providerType}</td>
    <td class="tg-oiyu">${element.facebook}</td>
    <td class="tg-oiyu">${element.instagram}</td>
    <td class="tg-oiyu">${element.twitter}</td>
    <td class="tg-oiyu">${element.linkedin}</td>
    <td class="tg-oiyu">${element.degree}</td>
    <td class="tg-oiyu">${element.specialization}</td>
    <td class="tg-oiyu">${element.category}</td>
    <td class="tg-oiyu">${element.city}</td>
    <td class="tg-oiyu">${element.address}</td>
    <td class="tg-oiyu">${element.phone}</td>
    <td class="tg-oiyu">${element.email}</td>
    <td class="tg-oiyu">${element.frequencyVisit}</td>
    <td class="tg-oiyu">${element.focus}</td>
    <td class="tg-oiyu">${element.productAlready}</td>
    </tr>`)
    });

    let showHeaderAndTable: boolean = false;
    // this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
    let printContents, popupWin;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
    <head>
    <style>
    img {
    float: left;
    }
  
    .flex-container {
    display: flex;
    justify-content: space-between;
    }
  
    .tg {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }
  
    .tg td {
    font-family: Arial, sans-serif;
    font-size: 14px;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
    
    .tb2 .tg-oikpyu {
      font-size: 10px;
      text-align: center;
      vertical-align: middle
      }
  
    
    .tg th {
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tg .tg-lboi {
    border-color: inherit;
    text-align: left;
    vertical-align: middle;
  
    }
  
    .tg .tg-u6fn {
    font-weight: bold;
    font-size: 18px;
    background-color: #efefef;
    color:black;
    border-color: inherit;
    text-align: left;
    vertical-align: top;
    padding-top: 0px;
    padding-bottom: 0px;
    }
  
    .tg .tg-yz93 {
    border-color: inherit;
    text-align: right;
    vertical-align: middle
    }
    .tb2 {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }
    
    
  
    .tb2 td {
    font-family: Arial, sans-serif;
    font-size: 12px;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 th {
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-weight: normal;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 .tg-oiyu {
    font-size: 10px;
    text-align: left;
    vertical-align: middle
    }
    .tg-bckGnd {
    font-weight: bold;
    background-color: #efefef;
    }
  
    .tb2 .tg-3ppa {
    font-size: 10px;
    background-color: #efefef;
    color:black;
    text-align: left;
    vertical-align: middle;
    padding-top: 2px;
    padding-bottom: 2px;
    }
  
    .footer {
    position: sticky;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #efefef;
    color: rgb(2, 2, 2);
    text-align: right
    }
  
    button {
    background-color: #008CBA;
    border: none;
    color: white;
    margin: 5px 0px;
    padding: 2px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    border-radius: 12px;
    cursor: pointer;
    outline: none;
    }
    @media print {
    @page { size: A4; margin: 5mm; }
    .footer {
    position: relative;
    }
    tr {
    page-break-inside: avoid;
    }
    button{
    display : none;
    }
    }
    </style>
    </head>
    <body onload="window.print();window.close()" style="padding-bottom:30px">
    <div class="flex-container">
    <div style="width: 40%; ">
    <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
    <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
  
    </div>
  
    <h2 style="margin-top: 0px; margin-bottom: 5px " ; align="left"><u>${this.currentUser.company.lables.doctorLabel} Details </u></h2>
    <div>
    <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
    <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
    </div>
    </div>
    </div>
  
    <div>
    
    <table class="tb2">
    <tr>
    <th class="tg-3ppa"><span style="font-weight:20">S.No.</span></th>
    <th class="tg-3ppa"><span style="font-weight:20">State</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">District</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Area</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Employee Name </span></th>
	<th class="tg-3ppa"><span style="font-weight:300">${this.currentUser.company.lables.doctorLabel} Name</span></th>
  <th class="tg-3ppa"><span style="font-weight:300">Provider Code</span></th>	
  <th class="tg-3ppa"><span style="font-weight:300">Provider Type</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Facebook</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Instagram</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Twitter</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">LinkedIn</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Degree</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Specialization</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Category</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">City</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Address</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Phone</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Email</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Frequency Visit</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Focus Products</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Products Already Using</span></th>
                
    </tr>
    ${tableRows.join('')}
    </table>
    </div>
    
    </body>
    </html>
    `);

}

}
