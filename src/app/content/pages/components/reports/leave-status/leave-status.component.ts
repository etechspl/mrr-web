import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange, MatTableDataSource } from '@angular/material';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'm-leave-status',
  templateUrl: './leave-status.component.html',
  styleUrls: ['./leave-status.component.scss']
})
export class LeaveStatusComponent implements OnInit {
  isDevisionExist: boolean = false;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  showDivisionFilter: boolean = false;
  showHeadquarter: boolean = false;
  employeeFilter: boolean = false;
  stateFilter: boolean = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns = ['S.No', 'dcrDate', 'name', 'punch', 'reason', 'leaveType', 'appByAdmin', 'appByMgr'];

  typeList = [];
  typeList1 = [];
  typeFilter: boolean;

  constructor(
    private _dcrReportsService: DcrReportsService,
    private _changeDetect: ChangeDetectorRef
  ) { }

  LeaveStatusForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    state: new FormControl(null),
    division:new FormControl(null),
    district: new FormControl(null),
    designation: new FormControl(null),
    employee: new FormControl(null),
    fromDate: new FormControl(null,Validators.required),
    toDate: new FormControl(null,Validators.required)
  })

  isToggled: boolean = true;
  currentUser = JSON.parse(sessionStorage.currentUser)

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) {
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
    if (this.currentUser.company.isDivisionExist === true) {
      this.isDevisionExist = true;
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }    
  }

  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  getReportType(val) {
    if (this.currentUser.company.isDivisionExist === true) {
      this.isDevisionExist = true;
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
    if (val === "Geographical") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        //this.callDivisionComponent.setBlank();
      }


      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      // if (this.isDevisionExist === true) {
      //   this.callDivisionComponent.setBlank();
      // }
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }

  }

  getTypeValue(val) {
    this.LeaveStatusForm.patchValue({ type: val })
    if (val === 'State') {
      this.callStateComponent.setBlank();
      this.showHeadquarter = false;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        //this.callDivisionComponent.setBlank();
      }
    } else if (val === 'Headquarter') {
      this.stateFilter = true;
      this.showHeadquarter = true;
      this.employeeFilter = false;
      this.callStateComponent.setBlank();
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
        this.callDistrictComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        //this.callDivisionComponent.setBlank();
      }
    } else if (val === 'Employee Wise') {
      this.stateFilter = false;
      this.showHeadquarter = false;
      this.employeeFilter = true;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;

        this.callEmployeeComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        //this.callDivisionComponent.setBlank();
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
      }
    }
  }

  getDivisionValue(val) {
    this.LeaveStatusForm.patchValue({division:val})
    if (this.LeaveStatusForm.value.type == "Employee Wise") {
      //=======================Clearing Filter===================
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    } else if (this.LeaveStatusForm.value.type == "State" || this.LeaveStatusForm.value.type == "Headquarter") {
      //=======================Clearing Filter===================
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      //===============================END=======================
      let divisionIds = {
        division: val
      };
      this.callStateComponent.getStateBasedOnDivision(divisionIds);
    }
  }

  getStateValue(val) {
    this.LeaveStatusForm.patchValue({ state: val })
    if (this.LeaveStatusForm.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.LeaveStatusForm.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrict(val) {
    this.LeaveStatusForm.patchValue({ district: val });
  }

  getDesignation(val) {
    this.LeaveStatusForm.patchValue({ designation: val });
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.LeaveStatusForm.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    }
  }

  getEmployee(val) {
    this.LeaveStatusForm.patchValue({ employee: val })
  }

  getFromDate(val) {
    this.LeaveStatusForm.patchValue({ fromDate: val })
  }

  getToDate(val) {
    this.LeaveStatusForm.patchValue({ toDate: val })
  }

  getRecords() {
    this.isToggled = true;
    let obj = {
      companyId: this.currentUser.companyId
    }
    console.log("this.LeaveStatusForm.value.type",this.LeaveStatusForm.value.type)
    if (this.LeaveStatusForm.value.type === 'State') {
      obj['state'] = this.LeaveStatusForm.value.state;
      //obj[''] =this.LeaveStatusForm.value.type;
    } else if (this.LeaveStatusForm.value.type === 'Headquarter') {
      obj['type'] =this.LeaveStatusForm.value.type;
      obj['state'] = this.LeaveStatusForm.value.state;
      obj['district'] = this.LeaveStatusForm.value.district;
    } else if (this.LeaveStatusForm.value.type === 'Employee Wise') {
      obj['type'] =this.LeaveStatusForm.value.type;
      obj['employee'] = this.LeaveStatusForm.value.employee;
    }

    obj['fromDate'] = new Date(this.LeaveStatusForm.value.fromDate);
    obj['toDate'] = new Date(this.LeaveStatusForm.value.toDate);
    this._dcrReportsService.getLeaveStatus(obj).subscribe((res: any) => {
      if (!Boolean(res) || !res.length) {
        this.alert("error", "No data found for the selection");
        this.isToggled = true;
      } else {
        this.isToggled = false;
        
        this.dataSource = new MatTableDataSource(res);
        this._changeDetect.detectChanges();
      }
    })

  }

  alert(icon, title) {
    const Toast = (Swal as any).mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      type: icon,
      title: title
    })
  }
}
