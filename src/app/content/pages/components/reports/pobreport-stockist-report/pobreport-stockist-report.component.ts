import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { UtilsService } from './../../../../../core/services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { TypeComponent } from "./../../filters/type/type.component";

import { Component, OnInit, ViewChild, ChangeDetectorRef, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from "../../filters/state/state.component";
import { DistrictComponent } from '../../filters/district/district.component';

import { DesignationComponent } from '../../filters/designation/designation.component';
import { CategoryComponent } from '../../filters/category/category.component';
import { MatOption, MatPaginator, MatSelect, MatSlideToggleChange, MatTableDataSource } from '@angular/material';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { EventEmitter, promise } from 'protractor';
import { AreaService } from '../../../../../core/services/Area/area.service';
// import { TypeComponent } from '../../filters/type/type.component';
// import { StateComponent } from '../../filters/state/state.component';
// import { DistrictComponent } from '../../filters/district/district.component';
@Component({
  selector: 'm-pobreport-stockist-report',
  templateUrl: './pobreport-stockist-report.component.html',
  styleUrls: ['./pobreport-stockist-report.component.scss']
})
export class POBReportStockistReportComponent implements OnInit {
  
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild("callDistrictComponent")
  callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;

  @ViewChild('dataTable') table;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callCategoryComponent") callCategoryComponent: CategoryComponent;
  /* @Output() valueChange = new EventEmitter();
  @Input() check: boolean;
  @Input() required: boolean = true; */


  @ViewChild('selectVal') selectVal: MatSelect;
  dataTable: any;
  dtOptions: any;
  hierarchyResult: any;

  currentUser = JSON.parse(sessionStorage.currentUser);

  showForMGROnly = false;
  allSelected = false;
  allSelectedHQ = false;

  dataSourceYearly: any;
  dataSourceFinancly: any;
  dataSourceHierarchyWise: any;
  showForTeamSelectionOnly = false;
  data: any;
  otherTypeLablesExist = (this.currentUser.company.lables.otherType) ? true : false;

  viewForArray = [];
  selectedCategory = [];
	public showDistrict: boolean = false;
	public showEmployee: boolean = false;
	public showState: boolean = false;

  // @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  // @ViewChild("callStateComponent") callStateComponent: StateComponent;
  // @ViewChild("callDistrictComponent")
  // callDistrictComponent: DistrictComponent;

  public dataViewFor: string = this.currentUser.company.lables.doctorLabel;
  public showProviderVisitAnalysisDetailsYearly: boolean = false;
  public showProviderVisitAnalysisDetailsFinanceYearly: boolean = false;
  public showYearFilter: boolean = true;
  public showReport: boolean = false;
  public showFinancialFilter: boolean = false;
  public showDoctorColumn: boolean = true;
  public showProcessing: boolean = false;
  public showDivisionFilter: boolean = false;
  public showCategory: boolean = false;
  public showFromDate: boolean = false;
  public showToDate: boolean = false;
  public dynamicTableHeader: any = [];
  dataSource: MatTableDataSource<any>;
  showData: boolean = false;
  public showReportHirearchyWise: boolean = false;

  // showState : boolean = true;
  // showDistrict : boolean = false
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  displayedColumns = [
    "sn",
    "employeeCode",
    "userName",
    "hq",
    "statusDated",

    "designation",
    "dateOfJoining",

    "TotalWorkingCount",


    "TC",
    "PC",
    "POB",
    "AreaName",
    "stockistForwarded",
    "SOB",
    "totalTC",
    "totalPC",
    "totalPOB",
    "stockistDetails",
    "totalSOB"
  ];

  POBForm: FormGroup;
  selectedValue: any = [];
  // showEmployee: boolean = false;
  constructor(private fb: FormBuilder,
    private _providerService: ProviderService,
    private _areaService: AreaService,
    private _toastr: ToastrService,
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _monthYearService: MonthYearService,
    private utilService: UtilsService,
    private exportAsService: ExportAsService,
    private _hierarchy: HierarchyService) {
    this.POBForm = this.fb.group({
      type:new FormControl(''),
      state:new FormControl(''),
      district:new FormControl(''),
      designation: new FormControl(''),
      status: new FormControl(true),
      team: new FormControl(""),
      employeeId: new FormControl(null),
      toDate: new FormControl(null),
      fromDate: new FormControl(null)
    });
  }
  isDivisionExist = this.currentUser.company.isDivisionExist;
  excelForm = new FormGroup({
		reportType: new FormControl("Geographical"),
		type: new FormControl(null, Validators.required),
		stateInfo: new FormControl(null),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		month: new FormControl(null),
		year: new FormControl(null),
		designation: new FormControl(null),
		toDate: new FormControl(null, Validators.required),
		fromDate: new FormControl(null, Validators.required),
		category: new FormControl(null),
	});
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
  this.getReportType('Geographical')
     this.hierarchyResult;
    if (this.currentUser.company.isDivisionExist == true) {
      this.POBForm.setControl('division', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
      this.getReportType('Geographical')
    }

  }
  getReportType(val) {
		if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Geographical",
        "WithHeadquarter"
      );
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType(
        "Hierarachy",
        "empWise"
      );
    }
  }



  

  getTypeValue(val) {
    this.excelForm.patchValue({type:val})
    this.POBForm.patchValue({type:val})
   
    if (val == 'State') {
      this.showDistrict = false;
      this.showState =true;
      this.showEmployee =false;
      this.showData=false;
    } else if (val == 'Headquarter') {
      this.callDistrictComponent.setBlank();
      this.callStateComponent.setBlank();
      this.showDistrict = true;
      this.showState = true;
      this.showEmployee =false;
      this.showData=false;
    } else if (val == 'Employee Wise') {
      // this.callDesignationComponent.setBlank();
      // this.callEmployeeComponent.setBlank();
      this.showDistrict = false;
      this.showState = false;
      this.showEmployee = true;
      this.showData=false;
    }
  }

  getDivisionValue(val) {
    this.showProcessing = false;
    this.showReportHirearchyWise = false;
    this.showReport = false;
    this.showCategory = true;
    this.POBForm.patchValue({ division: val });

    this.callDesignationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.callStateComponent.getStateBasedOnDivision(
      {
        companyId: this.currentUser.companyId,
        isDivisionExist: this.currentUser.company.isDivisionExist,
        division: val,
        designationLevel: this.currentUser.userInfo[0]
          .designationLevel,
      }
    )

    this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
  }

  getStateValue(val) {
    this.POBForm.patchValue({state:val});
    if (this.currentUser.company.isDivisionExist === true) {
			if (this.POBForm.value.type == "Headquarter") { 
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.currentUser.userInfo[0].designationLevel,
					companyId: this.currentUser.companyId,
					stateId: this.POBForm.value.state,
					isDivisionExist: this.currentUser.company.isDivisionExist,
					division: this.POBForm.value.division,
				});
			}
		} else {
			if (this.POBForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
  }

  // getReportType(val) {
  //   if (val === "Geographical") {
  //     this.showState = true;
  //     this.showEmployee = false;
  //     this.callTypeComponent.getTypesBasedOnReportingType(
  //       "Geographical",
  //       "WithHeadquarter"
  //     );
  //   } else {
  //     this.showState = false;
  //     this.showDistrict = false;
  //     this.callTypeComponent.getTypesBasedOnReportingType(
  //       "Hierarachy",
  //       "empWise"
  //     );
  //   }
  // }

  getDistrictValue(val){
    this.POBForm.patchValue({district:val});
  }

  

  exporttoExcel() {
    this.exportAsService.save(this.exportAsConfig, 'POB Report').subscribe(() => {
    });
  }

  getDesignation(val) {
    this.showProviderVisitAnalysisDetailsYearly = false;
    this.showProviderVisitAnalysisDetailsFinanceYearly = false;
    this.showCategory = true;
    this.POBForm.patchValue({ designation: val });
    if (val.designationLevel > 1) {
      this.showForMGROnly = true;
    } else {
      this.showForMGROnly = false;
    }
    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.POBForm.value.division,
        };

        this.callEmployeeComponent.getEmployeeListBasedOnDivision(
          passingObj
        );
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(
          this.currentUser.companyId,
          [val],
          [true]
        );
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.POBForm.value.division,
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
      }
    }
  }
  getTeamType(val) {
    if (val == "withTeam") {
      this.showForTeamSelectionOnly = true
    } else {
      this.showForTeamSelectionOnly = false;
    }

  }
  getStatusValue(val) {
    this.POBForm.patchValue({ status: val });

    if (this.currentUser.company.isDivisionExist == true) {
      if (this.currentUser.company.isDivisionExist == true && this.POBForm.value.division != null && this.POBForm.value.status != null) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: this.POBForm.value.designation.designationLevel,
          status: val,
          division: this.POBForm.value.division
        }

        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      }
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.POBForm.value.designation.designationLevel, val);
    }
  }
  getHierachyData(val) {
    let userIds = val.map((user) => {
      return user.userId
    })
    this.POBForm.patchValue({ team: userIds })
  }

  getEmployeeValue(emp: string) {
    this.showReport = false;
    this.showReportHirearchyWise = false;
    this.POBForm.patchValue({ employeeId: emp });
    const filter = { companyId: this.currentUser.companyId, supervisorId: emp, type: "lowerWithUpper" };
    this._hierarchy.getManagerHierarchy(filter).subscribe(hierachyRes => {
      this.hierarchyResult = hierachyRes
    })

  }
  getStockistName(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this._providerService.getStockistDetails(data).subscribe(res => {
        if (res.length > 0) {
          resolve(res);
        } else {
          reject([]);
        }
      });
    });
  }

  getAreaDetails(val): Promise<any> {
    return new Promise((resolve, reject) => {

      console.log("val+++",val);
      let blockIds = []

      val.forEach(element => {
        blockIds.push(element);
      });

      let data = {
        "where": {
          id: { inq: blockIds }
        }
      }
      //console.log("Object.keys(data.where.id).length",Object.keys(data.where.id).length);

      if (Object.keys(data.where.id.inq).length > 0) {
        this._areaService.getAreaNameBasedOnId(data).subscribe(res => {

          if (res.length > 0) {
            resolve(res);
            //  console.log("res+++++",res);

          } else {
            console.log("Data Not Found");
            reject(console.error());
          }

        })

      }
      else {
        let res = ["---"];
        resolve(res);

      }

    })

  }

  viewRecords() {
    console.log(this.POBForm.value)
    this.isToggled = false;
    this.showProcessing = true;
    this.showData = false;
    this.showReport = false;
    let obj = {
      companyId: this.currentUser.companyId,
      //managerId: this.POBForm.value.employeeId,
      //teamId: this.POBForm.value.team,
      type:this.POBForm.value.type,
      status: this.POBForm.value.status,
      fromDate: this.POBForm.value.fromDate,
      toDate: this.POBForm.value.toDate,
    }

    if(this.POBForm.value.type == 'State'){
      obj['stateId'] = [this.POBForm.value.state]
    }else if (this.POBForm.value.type == 'Headquarter'){
      obj['stateId'] = [this.POBForm.value.state],
      obj['districtId'] = [this.POBForm.value.Headquarter]
    }else if(this.POBForm.value.type == 'Employee Wise'){
      obj["managerId"] = this.POBForm.value.employeeId;
      obj["teamId"] =  this.POBForm.value.team;
    }

console.log("obj : -------",obj);
    this._dcrReportsService.getPobDetailsStockistWise(obj).subscribe(async (res) => {
      //console.log("res",res);
      this.showProcessing = false;
      if (res.length > 0) {


        try {
          let areaData;
          for (let i = 0; i < res.length; i++) {

            areaData = await this.getAreaDetails(res[i].blockIds);

            let areaDataName = areaData.map(x => {
              return x.areaName
            })

            res[i]['Area'] = areaDataName ? areaDataName : ['---'];



            let value = res.map(async (element, i, arr) => {

              let stockistNameArr = [];
              let stockistNameArr1 = [];
              if (element.POBForwardedLastDateToStockist) {
                let stockistIds = [];
                let stockistIdsWithPOB = [];
                element.POBForwardedLastDateToStockist.forEach((stockistId) => {
                  if (stockistId != null && stockistId.hasOwnProperty("stockist")) {
                    stockistIds.push(stockistId.stockist);
                    stockistIdsWithPOB.push({ stockistId: stockistId.stockist, pob: stockistId.pob })
                  }
                });
                if (stockistIds.length > 0) {
                  var obj = {
                    where: {
                      companyId: this.currentUser.companyId,
                      _id: { inq: stockistIds },
                    }
                  }
                  const response = await this.getStockistName(obj).catch(err => { console.log(err); });
                  if (response.length > 0) {
                    response.forEach(element => {
                      stockistIdsWithPOB.forEach((stkId, i) => {
                        if ((element.id).toString() == (stkId.stockistId).toString()) {
                          let index = stockistNameArr.findIndex(res => {
                            return element.providerName == res.providerName
                          })

                          if (index > -1) {
                            stockistNameArr[index].pob = stockistNameArr[index]["pob"] + stkId.pob
                          } else {
                            stockistNameArr.push({
                              providerName: element.providerName,
                              pob: stkId.pob
                            });
                          }
                        }
                      });

                    });
                  }
                }



              }
              element.stockistName = stockistNameArr;
              element.statusDate = this.POBForm.value.toDate;

              if (element.POBToStockist) {
                let stockistIds = [];
                let stockistIdsWithPOB = [];
                element.POBToStockist.forEach((stockistId) => {
                  if (stockistId != null && stockistId.hasOwnProperty("stockist")) {
                    stockistIds.push(stockistId.stockist);
                    stockistIdsWithPOB.push({ stockistId: stockistId.stockist, pob: stockistId.pob })
                  }
                });
                if (stockistIds.length > 0) {
                  var obj = {
                    where: {
                      companyId: this.currentUser.companyId,
                      _id: { inq: stockistIds },
                    }
                  }
                  const response = await this.getStockistName(obj).catch(err => { console.log(err); });
                  if (response.length > 0) {
                    response.forEach(element => {
                      stockistIdsWithPOB.forEach((stkId, i) => {
                        if ((element.id).toString() == (stkId.stockistId).toString()) {
                          let index = stockistNameArr1.findIndex(res => {
                            return element.providerName == res.providerName
                          })
                          if (index > -1) {
                            stockistNameArr1[index].pob = stockistNameArr1[index]["pob"] + stkId.pob
                          } else {
                            stockistNameArr1.push({
                              providerName: element.providerName,
                              pob: stkId.pob
                            });
                          }
                        }
                      });


                    });
                  }
                }


              }
              element.stockistNameWithPOB = stockistNameArr1;


            });

          }

          console.log(res)

         // one field is missing from data


          !this._changeDetectorRef["destroyed"]
            ? this._changeDetectorRef.detectChanges()
            : null;

          //  console.log(res);
          this.dataSource = new MatTableDataSource(res);

          setTimeout(() => {
            this.dataSource.paginator = this.paginator;
          }, 100);
          !this._changeDetectorRef["destroyed"]
            ? this._changeDetectorRef.detectChanges()
            : null;
          this.showData = true;


        } catch (ex) {
          console.log(ex)
        }
      } else {
        this.showProcessing = false;
        this._toastr.error("For Selected Filter data not found",
          "Data Not Found");
      }
      this._changeDetectorRef.detectChanges();
    });

  }
  getFromDate(val) {
    this.showReportHirearchyWise = false;
    this.POBForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.showReportHirearchyWise = false;
    this.POBForm.patchValue({ toDate: val });
  }

  toggleAllSelectionForHQ() {
    this.allSelectedHQ = !this.allSelectedHQ;  // to control select-unselect

    if (this.allSelectedHQ) {


      this.selectVal.options.forEach((item: MatOption) => item.select());
    } else {
      this.selectVal.options.forEach((item: MatOption) => { item.deselect() });
    }

  }
}
