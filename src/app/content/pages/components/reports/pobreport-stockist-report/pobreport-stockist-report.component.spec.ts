import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { POBReportStockistReportComponent } from './pobreport-stockist-report.component';

describe('POBReportStockistReportComponent', () => {
  let component: POBReportStockistReportComponent;
  let fixture: ComponentFixture<POBReportStockistReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ POBReportStockistReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(POBReportStockistReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
