import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetvsachievementComponent } from './targetvsachievement.component';

describe('TargetvsachievementComponent', () => {
  let component: TargetvsachievementComponent;
  let fixture: ComponentFixture<TargetvsachievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetvsachievementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetvsachievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
