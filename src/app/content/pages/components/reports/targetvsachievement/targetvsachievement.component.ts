import { TodateComponent } from '../../filters/todate/todate.component';
import {
	Input,
	EventEmitter,
	Output,
	SimpleChange,
} from "@angular/core";
import { retryWhen, delay, take } from "rxjs/operators";
import { LayoutConfigService } from "../../../../../core/services/layout-config.service";
import { SubheaderService } from "../../../../../core/services/layout/subheader.service";
import * as objectPath from "object-path";
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { TourProgramService } from "../../../../../core/services/TourProgram/tour-program.service";
import { PrimaryAndSaleReturnService } from "../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service";
import { StateService } from "../../../../../core/services/state.service";
// import { BroadcastMessageService } from "../../../../core/services/BroadcastMessage/broadcast-message.service";
import { ERPServiceService } from "../../../../../core/services/erpservice.service";
import * as moment from "moment";
import { LeaveApprovalService } from "../../../../../core/services/leave-approval.service";
import { CrmService } from "../../../../../core/services/CRM/crm.service";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import {
	Component,
	OnInit,
	ViewChild,
	ChangeDetectorRef,
	ChangeDetectionStrategy,
	ElementRef,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import * as _moment from "moment";
declare var $;
import Swal from "sweetalert2";
import { ActivatedRoute } from "@angular/router";
import { MatPaginator, MatSlideToggleChange, MatTableDataSource } from "@angular/material";
import { DivisionComponent } from '../../filters/division/division.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { StateComponent } from '../../filters/state/state.component';
import { TypeComponent } from '../../filters/type/type.component';
import { values } from 'lodash';
import { DcrProviderVisitDetailsService } from '../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
import { MonthYearService } from "../../../../../core/services/month-year.service";
export interface trafficLight {
	employeeName: String;
	state: String;
	district: String;
	target: String;
	primary: String;
	secondary: String;
	drCallAvg: String;
	score: String;
	rowColor: String;
}

@Component({
  selector: 'm-targetvsachievement',
  templateUrl: './targetvsachievement.component.html',
  styleUrls: ['./targetvsachievement.component.scss']
})
export class TargetvsachievementComponent implements OnInit {
  currencySymbol;
	currentUser = JSON.parse(sessionStorage.currentUser);
  currentMonth = new Date().getMonth();
	currentYear = new Date().getFullYear();
	Date = new Date().getDate();
  lastDay = new Date(this.currentYear, this.currentMonth, 0).getDate();
	actualMonth = this.currentMonth + 1;
	currentDate = this.currentYear + "-" + this.actualMonth + "-" + this.Date;
	currentMonthStartDate = this.currentYear + "-" + this.actualMonth + "-01";
	currentMonthLastDate =
		this.currentYear + "-" + this.actualMonth + "-" + this.lastDay;
	currentYearStartDate = this.currentYear + "-01-01";
	currentYearLastDate = this.currentYear + "-12-31";
  expenseUpdateForm: FormGroup;
  monthsName = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];
  private chart: any;
	currentMonthName = this.monthsName[this.currentMonth];

  constructor(
    private _primaryAndSaleReturnService: PrimaryAndSaleReturnService,
    private AmCharts: AmChartsService,
		private _changeDetectorRef: ChangeDetectorRef,
    private _fb: FormBuilder,

  ) { 
    this.createForm(this._fb);

  }

  ngOnInit() {
    let that = this;
    let passingObjectForTargetGraphForState = {
			companyId: this.currentUser.companyId,
			type: "state",
			lookingFor: "all",
			month: this.actualMonth,
			year: this.currentYear,
			designationLevel: this.currentUser.userInfo[0].designationLevel,
			userId: this.currentUser.id,
			isDivisonExist: this.currentUser.company.isDivisionExist,
		};
		this._primaryAndSaleReturnService
			.getTargetVsAchievementData(passingObjectForTargetGraphForState)
			.subscribe(
				(res) => {
					console.log("mahender0 ", res)

					let dynamicDataProvider = [];
					for (let i = 0; i < res.length; i++) {
						dynamicDataProvider.push({
							category: res[i].stateName,
							"column-1": res[i].target,
							"column-2": res[i].primary,
							"column-3": res[i].secondary,
							stateId: res[i].stateId,
							stateName: res[i].stateName,
						});
					}
					this.chart = this.AmCharts.makeChart(
						"targetAchievementGraph",
						{
							type: "serial",
							categoryField: "category",
							"fontFamily": "Vardana",
							startDuration: 1,
							categoryAxis: {
								gridPosition: "start",
								labelRotation: 45,
							},
							trendLines: [],
							mouseWheelScrollEnabled: false,
							// "mouseWheelZoomEnabled": false,
							chartScrollbar: {
								autoGridCount: true,
								hideResizeGrips: true,
								scrollbarHeight: 20,
							},
							graphs: [
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]L",
									balloonFunction: (
										graphDataItem,
										graphs
									) => {
										//var value = graphDataItem.values.value * 100000;
										let value = graphDataItem.values.value;
										return (
											graphs.title +
											` of ` +
											graphDataItem.category +
											`: ${this.currencySymbol
												? this.currencySymbol
												: "₹"
											}` +
											value
										);
									},
									fillAlphas: 1,
									id: "AmGraph-1",
									title: "Target",
									type: "column",
									valueField: "column-1",
									//labelText: "[[column-1]]", //hide top value of graph
									fixedColumnWidth: 22,
									colorField: "red",
								},
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]L",
									balloonFunction: (
										graphDataItem,
										graphs
									) => {
										//var value = graphDataItem.values.value * 100000;
										let value = graphDataItem.values.value;

										return (
											graphs.title +
											` of ` +
											graphDataItem.category +
											`: ${this.currencySymbol
												? this.currencySymbol
												: "₹"
											}` +
											value
										);
									},
									fillAlphas: 1,
									id: "AmGraph-2",
									title: "Primary Sales",
									type: "column",
									valueField: "column-2",
									//labelText: "[[column-2]]",
									fixedColumnWidth: 22,
								},
								{
									balloonText:
										"[[title]] of [[category]]:[[value]]L",
									balloonFunction: (
										graphDataItem,
										graphs
									) => {
										//var value = graphDataItem.values.value * 100000;
										let value = graphDataItem.values.value;

										return (
											graphs.title +
											` of ` +
											graphDataItem.category +
											`: ${this.currencySymbol
												? this.currencySymbol
												: "₹"
											}` +
											value
										);
									},

									fillAlphas: 1,
									id: "AmGraph-3",
									title: "Secondary Sales",
									type: "column",
									valueField: "column-3",
									//	labelText: "[[column-3]]",
									fixedColumnWidth: 22,
								},
							],
							guides: [],
							valueAxes: [
								{
									id: "ValueAxis-1",
									title: `Sales Amount in ${this.currencySymbol
											? this.currencySymbol
											: "₹"
										} `,
									// "unit": "L",
									// "labelFunction": function (value, valueText, valueAxis) {
									// 	if (value < 9999999) {
									// 		return "" + value + "L"
									// 	} else {
									// 		return "" + 1 + "Cr."
									// 	}
									// }
								},
							],
							allLabels: [],
							balloon: {},
							legend: {
								enabled: true,
								useGraphSettings: true,
							},
							titles: [
								{
									id: "Title-1",
									size: 20,
									text:
										"Sale Status of " +
										this.currentMonthName +
										"," +
										this.currentYear +
										" ",
								},
							],
							dataProvider: dynamicDataProvider,
						
							export: {
								enabled: true,
							},
						}
					);
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				},
				(err) => {
					console.log(err);
				}
			);
  }
 
 generateReport(){
    let that = this;
    let passingObjectForTargetGraphForState = {
      companyId: this.currentUser.companyId,
      type: "state",
      lookingFor: "all",
      month: this.expenseUpdateForm.value.month,
      year: this.expenseUpdateForm.value.year,
      designationLevel: this.currentUser.userInfo[0].designationLevel,
      userId: this.currentUser.id,
      isDivisonExist: this.currentUser.company.isDivisionExist,
    };
    console.log("passingObjectForTargetGraphForState",passingObjectForTargetGraphForState)
    this._primaryAndSaleReturnService
      .getTargetVsAchievementData(passingObjectForTargetGraphForState)
      .subscribe(
        (res) => {
          console.log("mahendernew----------------------->",res)
          let dynamicDataProvider = [];
          for (let i = 0; i < res.length; i++) {
            dynamicDataProvider.push({
              category: res[i].stateName,
              "column-1": res[i].target,
              "column-2": res[i].primary,
              "column-3": res[i].secondary,
              stateId: res[i].stateId,
              stateName: res[i].stateName,
            });
          }
          this.chart = this.AmCharts.makeChart(
            "targetAchievementGraph",
            {
              type: "serial",
              categoryField: "category",
              "fontFamily": "Vardana",
              startDuration: 1,
              categoryAxis: {
                gridPosition: "start",
                labelRotation: 45,
              },
              trendLines: [],
              mouseWheelScrollEnabled: false,
              // "mouseWheelZoomEnabled": false,
              chartScrollbar: {
                autoGridCount: true,
                hideResizeGrips: true,
                scrollbarHeight: 20,
              },
              graphs: [
                {
                  balloonText:
                    "[[title]] of [[category]]:[[value]]L",
                  balloonFunction: (
                    graphDataItem,
                    graphs
                  ) => {
                    //var value = graphDataItem.values.value * 100000;
                    let value = graphDataItem.values.value;
                    return (
                      graphs.title +
                      ` of ` +
                      graphDataItem.category +
                      `: ${this.currencySymbol
                        ? this.currencySymbol
                        : "₹"
                      }` +
                      value
                    );
                  },
                  fillAlphas: 1,
                  id: "AmGraph-1",
                  title: "Target",
                  type: "column",
                  valueField: "column-1",
                  //labelText: "[[column-1]]", //hide top value of graph
                  fixedColumnWidth: 22,
                  colorField: "red",
                },
                {
                  balloonText:
                    "[[title]] of [[category]]:[[value]]L",
                  balloonFunction: (
                    graphDataItem,
                    graphs
                  ) => {
                    //var value = graphDataItem.values.value * 100000;
                    let value = graphDataItem.values.value;
  
                    return (
                      graphs.title +
                      ` of ` +
                      graphDataItem.category +
                      `: ${this.currencySymbol
                        ? this.currencySymbol
                        : "₹"
                      }` +
                      value
                    );
                  },
                  fillAlphas: 1,
                  id: "AmGraph-2",
                  title: "Primary Sales",
                  type: "column",
                  valueField: "column-2",
                  //labelText: "[[column-2]]",
                  fixedColumnWidth: 22,
                },
                {
                  balloonText:
                    "[[title]] of [[category]]:[[value]]L",
                  balloonFunction: (
                    graphDataItem,
                    graphs
                  ) => {
                    //var value = graphDataItem.values.value * 100000;
                    let value = graphDataItem.values.value;
  
                    return (
                      graphs.title +
                      ` of ` +
                      graphDataItem.category +
                      `: ${this.currencySymbol
                        ? this.currencySymbol
                        : "₹"
                      }` +
                      value
                    );
                  },
  
                  fillAlphas: 1,
                  id: "AmGraph-3",
                  title: "Secondary Sales",
                  type: "column",
                  valueField: "column-3",
                  //	labelText: "[[column-3]]",
                  fixedColumnWidth: 22,
                },
              ],
              guides: [],
              valueAxes: [
                {
                  id: "ValueAxis-1",
                  title: `Sales Amount in ${this.currencySymbol
                      ? this.currencySymbol
                      : "₹"
                    } `,
                  // "unit": "L",
                  // "labelFunction": function (value, valueText, valueAxis) {
                  // 	if (value < 9999999) {
                  // 		return "" + value + "L"
                  // 	} else {
                  // 		return "" + 1 + "Cr."
                  // 	}
                  // }
                },
              ],
              allLabels: [],
              balloon: {},
              legend: {
                enabled: true,
                useGraphSettings: true,
              },
              titles: [
                {
                  id: "Title-1",
                  size: 20,
                  text:
                    "Sale Status of " +
                    this.currentMonthName +
                    "," +
                    this.currentYear +
                    " ",
                },
              ],
              dataProvider: dynamicDataProvider,
              export: {
                enabled: true,
              },
            }
          );
          !this._changeDetectorRef["destroyed"]
            ? this._changeDetectorRef.detectChanges()
            : null;
        },
        (err) => {
          console.log(err);
        }
      );
  }
  
  createForm(_fb: FormBuilder) {

    this.expenseUpdateForm = _fb.group({

      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })

  }
  getFromDate(month: object) {
    this.expenseUpdateForm.patchValue({ month: month })
  }

  getToDate(year: number) {
    this.expenseUpdateForm.patchValue({ year: year })
  }

}
