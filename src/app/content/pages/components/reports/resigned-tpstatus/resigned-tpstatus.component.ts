import { Component, OnInit, Inject, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TourProgram } from '../../../_core/models/tour-program.model';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
declare var $;

@Component({
  selector: 'm-resigned-tpstatus',
  templateUrl: './resigned-tpstatus.component.html',
  styleUrls: ['./resigned-tpstatus.component.scss']
})

export class ResignedTPStatusComponent implements OnInit {
  teamTPFilter: FormGroup;
  showTPDetails = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  @ViewChild("userDataTable") dataTable1;
  userDataTable: any;
  userDTOptions: any;
  constructor(public dialogRef: MatDialogRef<ResignedTPStatusComponent>,private _tourProgram:TourProgramService,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,private changeDetectedRef: ChangeDetectorRef,) {

      this.teamTPFilter = this.fb.group({
        monthModel: new FormControl(null, Validators.required),
        yearModel: new FormControl(null, Validators.required)
      })
     }
  ngOnInit() {
  }
  getMonth(val) {
    // this.showSpinner = false;
    this.showTPDetails = false;
    this.teamTPFilter.patchValue({ monthModel: val });
  }
  getYear(val) {
    //this.showSpinner = false;
    this.showTPDetails = false;
    this.teamTPFilter.patchValue({ yearModel: val });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  showReport(){
    if (this.teamTPFilter.valid) {
         let userId = "";
       userId = this.data.obj._id.submitBy;
        this._tourProgram.viewTourProgram(this.currentUser.companyId, userId, this.teamTPFilter.value.monthModel, this.teamTPFilter.value.yearModel).subscribe(res => {
          this.userDTOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            destroy: true,
            data: res,
            responsive: true,
            columns: [
              // {
              //   title: 'Division',
              //   data1: 'divisionName',
              //   render: function (data1) {
              //     if (data1 === '') {
              //       return '---'
              //     } else {
              //       return data1
              //     }
              //   },
              //   defaultContent: '---'
              // },
              {
                title: 'Date',
                data: 'date',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Submitted Activity',
                data: 'submittedActivity',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Approved Activity',
                data: 'apprvActivity',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Submitted Area',
                data: 'submittedArea',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Submitted Activity',
                data: 'submittedActivity',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Approved Area',
                data: 'approvedArea',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'submitted Doctor',
                data: 'submittedDoctor',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Approved Doctor',
                data: 'apprvDoctor',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Submitted Chemist',
                data: 'submittedChemist',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Approved Chemist',
                data: 'approvedChemists',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Submitted Remark',
                data: 'submittedRemark',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },{
                title: 'Approved Remark',
                data: 'apprvRemark',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }

            ],
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search Records",
            },
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'copy',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'print',
                exportOptions: {
                  columns: ':visible'
                }
              }
            ]
          };
          if (this.currentUser.company.isDivisionExist == false) {
            this.userDTOptions.columns[0].visible = false;
          }
          this.changeDetectedRef.detectChanges();
          this.userDataTable = $(this.dataTable1.nativeElement);
          this.userDataTable.DataTable(this.userDTOptions);
          this.changeDetectedRef.detectChanges();
        }, err => {
          //this.showProcessing = false;
          this.changeDetectedRef.detectChanges();
        })
    }
  }

}
