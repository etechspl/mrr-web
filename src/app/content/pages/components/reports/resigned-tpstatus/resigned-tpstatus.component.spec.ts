import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResignedTPStatusComponent } from './resigned-tpstatus.component';

describe('ResignedTPStatusComponent', () => {
  let component: ResignedTPStatusComponent;
  let fixture: ComponentFixture<ResignedTPStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResignedTPStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResignedTPStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
