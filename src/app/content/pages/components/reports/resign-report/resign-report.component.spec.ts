import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResignReportComponent } from './resign-report.component';

describe('ResignReportComponent', () => {
  let component: ResignReportComponent;
  let fixture: ComponentFixture<ResignReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResignReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResignReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
