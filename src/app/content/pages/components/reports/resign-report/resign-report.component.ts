import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { StateComponent } from '../../filters/state/state.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DCRBackupService } from '../../../../../core/services/DCRBackup/dcrbackup.service';
import { DCRProviderVisitDetailsBackupService } from '../../../../../core/services/DcrProviderVisitDetailsBackup/dcrprovider-visit-details-backup.service';
import { ExportAsService } from 'ngx-export-as';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { MatDialog, MatSlideToggleChange } from '@angular/material';
import { EdituserComponent } from '../edituser/edituser.component';
import { ResignedTPStatusComponent } from '../resigned-tpstatus/resigned-tpstatus.component';
import { ViewDCRResignedUserComponent } from '../view-dcrresigned-user/view-dcrresigned-user.component';
declare var $;

import { URLService } from '../../../../../core/services/URL/url.service';
import * as _moment from 'moment';

@Component({
  selector: 'm-resign-report',
  templateUrl: './resign-report.component.html',
  styleUrls: ['./resign-report.component.scss']
})

export class ResignReportComponent implements OnInit {
  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;
  isShowReport = false;
  showAdminAndMGRLevelFilter = false;
  @ViewChild("userDataTable") dataTable1;
  userDataTable: any;
  userDTOptions: any;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;


  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  dtOptions: any;
  dataSource;
  constructor( private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private DCRProviderVisitDetailsBackupService: DCRProviderVisitDetailsBackupService,
    private exportAsService: ExportAsService,
    private _userDetailService:UserDetailsService,
    private changeDetectedRef: ChangeDetectorRef,
    private dialog: MatDialog,
    private _urlService: URLService
    ) { }

    isShowDivision = false;
    currentUser = JSON.parse(sessionStorage.currentUser);
    isDivisionExist = this.currentUser.company.isDivisionExist;
    designationLevel = this.currentUser.userInfo[0].designationLevel;
    companyId = this.currentUser.companyId;

    dcrForm = new FormGroup({
      reportType: new FormControl('Geographical'),
      type: new FormControl(null, Validators.required),
      stateInfo: new FormControl(null),
      districtInfo: new FormControl(null),
      employeeId: new FormControl(null),
      //month: new FormControl(null),
      //year: new FormControl(null),
      designation: new FormControl(null),
      toDate: new FormControl(null, Validators.required),
      fromDate: new FormControl(null, Validators.required)
    })

    check: boolean = false;
    isToggled = true;
    toggleFilter(event: MatSlideToggleChange) {
      this.isToggled = event.checked;
    }
  getReportType(val) {

    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this. callDivisionComponent.setBlank();
      this.dcrForm.patchValue({type: ''});
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      this.showState = false;
      this.showDistrict = false;
      this. callDivisionComponent.setBlank();
      this.dcrForm.patchValue({divisionId: ''});
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    if (val == "State") {
      this.showDistrict = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");
      this.dcrForm.setControl("stateInfo", new FormControl('', Validators.required))
    } else if (val == "Headquarter") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");
      this.dcrForm.setControl("stateInfo", new FormControl('', Validators.required))
      this.dcrForm.setControl("districtInfo", new FormControl('', Validators.required))
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.dcrForm.removeControl("stateInfo");
      this.dcrForm.removeControl("districtInfo");
      this.dcrForm.setControl("designation", new FormControl('', Validators.required))
      this.dcrForm.setControl("employeeId", new FormControl('', Validators.required))
    } else if (val == "Self") {
      this.showEmployee = false;
    }

    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.dcrForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }
  getStateValue(val) {
    this.dcrForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.dcrForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.dcrForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.dcrForm.value.divisionId
        })

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.dcrForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.dcrForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.dcrForm.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.dcrForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.dcrForm.patchValue({ toDate: val });
  }

  getDesignation(val) {
    this.dcrForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.dcrForm.value.divisionId,
        status: [false],
        designationObject: this.dcrForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [false]);
    }

  }
  //setting division into dcrform
  getDivisionValue($event) {
    this.dcrForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.dcrForm.value.type === "State" || this.dcrForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.dcrForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.dcrForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.dcrForm.value.divisionId != null && this.dcrForm.value.designation != null) {
          if (this.dcrForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.dcrForm.value.divisionId,
              status: [true],
              designationObject: this.dcrForm.value.designation
            })
          }
        }
      }
    }
  }
  ngOnInit() {

    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.dcrForm.setControl('divisionId', new FormControl('', Validators.required));
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }

    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }


  }
  editUser(obj) {
    const dialogRef = this.dialog.open(ResignedTPStatusComponent, {
      maxWidth: '90%',
      minWidth:"80%",

      data: { obj }
    });

    this.viewRecords();
  }
  viewDCR(obj,type){
    const dialogRef = this.dialog.open(ViewDCRResignedUserComponent, {
      maxWidth: '90%',
      minWidth:"80%",

      data: { obj,type }

  });
  this.viewRecords();
}

  viewRecords(){
    this.isToggled = false;

    this.isShowReport = false;
    this.showProcessing = true;
    if (this.dcrForm.value.employeeId == null) {
      this.dcrForm.value.employeeId = [this.currentUser.id];
    }

    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.dcrForm.value.divisionId;
    } else {
      params["division"] = [];
    }

    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.dcrForm.value.stateInfo;
    params["districtIds"] = this.dcrForm.value.districtInfo;
    params["type"] = this.dcrForm.value.type;
    params["employeeId"] = this.dcrForm.value.employeeId;
    params["fromDate"] = this.dcrForm.value.fromDate;
    params["toDate"] = this.dcrForm.value.toDate;
    this._userDetailService.getResignedEmployeeDetails(params).subscribe(res => {
      this.showProcessing = false;
      this.showReport = true;
      this.isShowReport = true;
      this.dataSource = res;
      res.map((item, index) => {
        item['index'] = index;
    });
      const PrintTableFunction = this.PrintTableFunction.bind(this);
      this.userDTOptions = {
        "pagingType": 'full_numbers',
        "paging": true,
        "ordering": true,
        "info": true,
        "scrollY": 300,
        "scrollX": true,
        "fixedColumns": true,
        destroy: true,
        data: res,
        responsive: true,
        columns: [
          {
            title: 'S. No.',
            data: 'index',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data + 1;
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Division',
            data: 'divisionName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'State Name',
            data: 'state',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: this.currentUser.company.lables.hqLabel + " Name",
            data: 'district',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Name",
            data: 'Name',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: "Designation",
            data: 'designation',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "Employee Id",
            data: 'employeeId',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "First DCR Date",
            data: 'firstDCRDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }, {
            title: "Last DCR Date",
            data: 'lastDCRDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'TP View',
            defaultContent: "<button id='TPView' class='btn btn-warning's><i class='la la-view'></i>&nbsp;&nbsp;View</button>"
          },
          {
            title: 'DCR View',
            defaultContent: "<button id='DCRView' class='btn btn-warning's><i class='la la-view'></i>&nbsp;&nbsp;View</button>"
          },
          {
            title: 'Leave Status',
            defaultContent: "<button id='LeaveView' class='btn btn-warning's><i class='la la-view'></i>&nbsp;&nbsp;View</button>"
          },
        ],
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;

          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td', row).unbind('click');

          $('td button', row).bind('click', (event) => {

            //console.log("row : ", data);
            //console.log(event.target.id);
            let rowObject = JSON.parse(JSON.stringify(data));
            if (event.target.id == "TPView") {
              self.editUser(rowObject);
            }
            else if (event.target.id == "DCRView") {
              //Making Inactive
              self.viewDCR(rowObject,'DCRView');
            }
            else if (event.target.id == "LeaveView") {
               self.viewDCR(rowObject,"LeaveView");
             }
          });
          return row;
        },
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search Records",
        },
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible'
            }
          },
           //********************* UMESH 17-03-2020 ************************ */
              {
                extend: "print",
                title: function () {
                return "State List";
                },
                exportOptions: {
                columns: ":visible"
                },
                action: function (e, dt, node, config) {
                PrintTableFunction(res);
                }
                }
        ]
      };
    
      if (this.currentUser.company.isDivisionExist == false) {
        this.userDTOptions.columns[1].visible = false;
      }

      this.changeDetectedRef.detectChanges();
      this.userDataTable = $(this.dataTable1.nativeElement);
      this.userDataTable.DataTable(this.userDTOptions);
      this.changeDetectedRef.detectChanges();
    }, err => {
      this.showDateWiseDCRReport = false;
      this.showProcessing = false;
      this.toastr.info("No Record Found.")
      this._changeDetectorRef.detectChanges();
    })

  }



   ///*****************************************UMESH 19-03-2020 *************/


PrintTableFunction(data?: any): void {
  //const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
  const currentDateAndTime = _moment(new Date()).format('DD/MM/YYYY, dddd , HH:mm A');
  const department = this.currentUser.section;
  const designation = this.currentUser.designation;
  const companyName = this.currentUser.companyName;

  const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

  let tableRows: any = [];
  data.forEach(element => {
  tableRows.push(`<tr>
  <td class="tg-oiyu">${element.divisionName}</td>
  <td class="tg-oiyu">${element.state}</td>
  <td class="tg-oiyu">${element.district}</td>
  <td class="tg-oiyu">${element.Name}</td>
  <td class="tg-oiyu">${element.designation}</td>
  <td class="tg-oiyu">${element.employeeId}</td>
  <td class="tg-oiyu">${element.firstDCRDate}</td>
  <td class="tg-oiyu">${element.lastDCRDate}</td>

  </tr>`)
  });

  let showHeaderAndTable: boolean = false;
  //this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;

  let printContents, popupWin;
  popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  popupWin.document.open();
  popupWin.document.write(`
  <html>
  <head>
  <style>
  img {
  float: left;
  }

  .flex-container {
  display: flex;
  justify-content: space-between;
  }

  .tg {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tg td {
  font-family: Arial, sans-serif;
  font-size: 14px;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg th {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tg .tg-lboi {
  border-color: inherit;
  text-align: left;
  vertical-align: middle;

  }

  .tg .tg-u6fn {
  font-weight: bold;
  font-size: 18px;
  background-color: #efefef;
  color:black;
  border-color: inherit;
  text-align: left;
  vertical-align: top;
  padding-top: 0px;
  padding-bottom: 0px;
  }

  .tg .tg-yz93 {
  border-color: inherit;
  text-align: right;
  vertical-align: middle
  }

  .tb2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-top: 10px;
  }

  .tb2 td {
  font-family: Arial, sans-serif;
  font-size: 12px;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tb2 th {
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-weight: normal;
  padding: 2px;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  word-break: normal;
  border-color: black;
  }

  .tb2 .tg-oiyu {
  font-size: 10px;
  text-align: left;
  vertical-align: middle
  }
  .tg-bckGnd {
  font-weight: bold;
  background-color: #efefef;
  }


  .tb2 .tg-3ppa {
  font-size: 10px;
  background-color: #efefef;
  color:black;
  text-align: left;
  vertical-align: middle;
  padding-top: 2px;
  padding-bottom: 2px;
  }

  .footer {
  position: sticky;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #efefef;
  color: rgb(2, 2, 2);
  text-align: right
  }



  button {
  background-color: #008CBA;
  border: none;
  color: white;
  margin: 5px 0px;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  }

  @media print {
  @page { size: A4; margin: 5mm; }

  .footer {
  position: relative;
  }
  tr {
  page-break-inside: avoid;
  }

  button{
  display : none;
  }
  }
  </style>
  </head>
  <body onload="window.print();window.close()" style="padding-bottom:30px">
  <div class="flex-container">
  <div style="width: 40%; ">
  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

  </div>

  <div style="text-align: right;">
  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Consolidate Report</h2>
  <div>
  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
  </div>
  </div>
  </div>

  <div>
  ${/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/''}
  <table class="tb2">
  <tr>
  <th class="tg-3ppa"><span style="font-weight:600">Division</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">State Name</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Headquarter Name</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Name</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Designation</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Employee Id</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">First DCR Date</span></th>
  <th class="tg-3ppa"><span style="font-weight:600">Last DCR Date</span></th>
  </tr>
  ${tableRows.join("")}
  </table>
  </div>
  <div class="footer flex-container">
  <p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>

  <p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
  </div>
  </body>
  </html>
  `);

  //popupWin.document.close();

  }

}
