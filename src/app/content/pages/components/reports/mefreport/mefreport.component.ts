import { ToastrService } from 'ngx-toastr';
import { DcrProviderVisitDetailsService } from './../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TypeComponent } from './../../filters/type/type.component';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { AreaService } from '../../../../../core/services/Area/area.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { CategoryComponent } from '../../filters/category/category.component';
import { URLService } from '../../../../../core/services/URL/url.service';
import { MatSlideToggleChange } from '@angular/material';
import * as XLSX from 'xlsx';
import * as _moment from 'moment';
declare var $;
@Component({
  selector: 'm-mefreport',
  templateUrl: './mefreport.component.html',
  styleUrls: ['./mefreport.component.scss']
})
export class MEFReportComponent implements OnInit {

  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showAreaOnMR: boolean = false;
  public showDivisionFilter: boolean = false;
  dataSource;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild('dataTable') table;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callCategoryComponent") callCategoryComponent: CategoryComponent;

  dataTable: any;
  dtOptions: any;
  dtTrigger: any;

  showProcessing: boolean = false;
  showAdminAndMGRLevelFilter = false;
  logoURL: any;
  constructor(private changeDetectorRef: ChangeDetectorRef,
    private _dcrProviderVisitService: DcrProviderVisitDetailsService,
    private _toastr: ToastrService,
    private _areaService: AreaService,
    private _urlService: URLService ) { }
    currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  @ViewChild('dataTable') tableRef: ElementRef;
  dcrForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    stateInfo: new FormControl(null),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    designation: new FormControl(null),
    toDate: new FormControl(null, Validators.required),
    fromDate: new FormControl(null, Validators.required),
   // category: new FormControl(null, Validators.required)
  })
  check: boolean;
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    this.logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;

    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }

    if (this.currentUser.company.isDivisionExist == true) {

      this.dcrForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  getReportType(val) {
    this.showReport = false;
    this.callDivisionComponent.setBlank();
    this.dcrForm.patchValue({type: ''});
    if (val === "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showEmployee = false;

      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = false;
      this.showDistrict = false;
      this.showDivisionFilter = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    this.showReport = false;
    if (val == "State") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
      }
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = true;
      this.showDistrict = false;
      this.callStateComponent.setBlank();
      this.dcrForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");
      this.dcrForm.removeControl("districtInfo");
    } else if (val == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
      }
      this.callDivisionComponent.setBlank();
      this.callStateComponent.setBlank();
      this.dcrForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.dcrForm.setControl("districtInfo", new FormControl(null, Validators.required))
      this.dcrForm.removeControl("employeeId");
      this.dcrForm.removeControl("designation");

      this.showState = true;
      this.showDistrict = true;
    } else if (val == "Employee Wise") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.showEmployee = true;
      this.dcrForm.removeControl("stateInfo");
      this.dcrForm.removeControl("districtInfo");
      this.dcrForm.setControl("designation", new FormControl(null, Validators.required))
      this.dcrForm.setControl("employeeId", new FormControl(null, Validators.required))
    } else if (val == "Self") {
      this.showDivisionFilter = false;
      this.showEmployee = false;
    }

    this.dcrForm.patchValue({ type: val });

  }

  getDivisionValue(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.dcrForm.patchValue({ division: val });

    if (this.currentUser.userInfo[0].designationLevel == 0) {
      if (this.dcrForm.value.type == "Employee Wise") {
        //=======================Clearing Filter===================

        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.dcrForm.value.type == "State" || this.dcrForm.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================


        let divisionIds = {
          division: val
        };
        this.callStateComponent.getStateBasedOnDivision(divisionIds);
      }
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      if (this.dcrForm.value.type == "Employee Wise") {
        //=======================Clearing Filter===================
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.dcrForm.value.type == "State" || this.dcrForm.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================

        let passingObj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: true,
          division: this.dcrForm.value.division,
          supervisorId: this.currentUser.id,
        }
        this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)

      }
    }

  }

  getStateValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ stateInfo: val });

    this.callDistrictComponent.setBlank();

    if (this.dcrForm.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.dcrForm.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ toDate: val });
  }
  getCategory(val) {
    this.showProcessing = false;
    this.showReport = false;
    this.dcrForm.patchValue({ category: val });
  }

  getDesignation(val) {
    this.showReport = false;
    this.dcrForm.patchValue({ designation: val });

    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.dcrForm.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    } else {
      let desig = [];
      for (let i = 0; i < val.length; i++) {
        desig.push(val[i].designationLevel);
      }
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: desig,
          isDivisionExist: true,
          division: this.dcrForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

      // this.callEmployeeComponent.getManagerHierarchy(this.currentUser.companyId, val[0].designationLevel, [true]);
    }
  }
  viewRecords() {
    this.isToggled = false;
    let showEmployeeColumn = true;
    if (this.currentUser.userInfo[0].designationLevel == 1) {
      showEmployeeColumn = false;
    }
    let userIds;
    if (this.dcrForm.value.type == "Self" || this.dcrForm.value.type == null) { //Null is for MR level
      userIds = [this.currentUser.id];
    } else {
      userIds = this.dcrForm.value.employeeId
    }

    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.dcrForm.value.division;
    }

    this.showReport = false;
    this.showProcessing = true;
    this._dcrProviderVisitService.MEFTrackerReport(this.currentUser.companyId, this.dcrForm.value.type, this.dcrForm.value.stateInfo, this.dcrForm.value.districtInfo, userIds, this.dcrForm.value.fromDate, this.dcrForm.value.toDate, ["RMP"], this.currentUser.company.validation.unlistedDocCall, this.currentUser.company.isDivisionExist, divisionIds).subscribe(mefRecords => {
    
    if (mefRecords == null || mefRecords.length == 0 || mefRecords == undefined) {

      this.showProcessing = false;
      this.showReport = false;
      this._toastr.info('For Selected Filter data not found', 'Data Not Found');
    } else {

      mefRecords.map((item, index) => {
        item['index'] = index;
    });
    
    this.dataSource = mefRecords;
    this.showReport = true;
    this.showProcessing = false;
    this.dtOptions = {
      "pagingType": 'full_numbers',
      "paging": true,
      "ordering": true,
      "info": true,
      "scrollY": 300,
      "scrollX": true,
      "fixedColumns": true,
      destroy: true,
      data: this.dataSource,
      columns: [
        {
          title: 'S. No.',
          data: 'index',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data + 1;
            }
          },
          defaultContent: '---'
        },
        {
          title: 'State Name',
          data: 'stateName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: this.currentUser.company.lables.hqLabel + ' Name',
          data: 'districtName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        }
        ,
        {
          title: this.currentUser.company.lables.areaLabel + ' Name',
          data: 'blockName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Division',
          data: 'divisionName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: 'User Name',
          data: 'userName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        
        {
          title: this.currentUser.company.lables.doctorLabel + ' Name',
          data: 'providerName',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Category',
          data: 'category',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Address',
          data: 'address',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Phone',
          data: 'phone',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        },
        {
          title: 'Visit Dates',
          data: 'visitDates',
          render: function (data) {
            if (data === '') {
              return '---'
            } else {
              return data
            }
          },
          defaultContent: '---'
        }, 
            {
          title: 'Focus Product',
          data: 'focusProduct',
          render: function (data, row) {
            if (data === '' || data == 'null' || data == undefined || data === [] ) {
              return '---'
            } else {
            
              return data
            }

          }
        },    
        {
          title: 'Product Discussed',
          data: 'productName',
          render: function (data, row) {
            if (data === '' || data == 'null' || data == undefined || data === [] ) {
              return '---'
            } else {
            
              return data
            }

          }
        },
        {
          title: 'Compititor Product He is using',
          data: 'competitorProduct',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        }, {
          title: 'What is the problem Dr. is facing with Comp. Product',
          data: 'drProductProblem',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        }, {
          title: 'What solution I have given by telling our product',
          data: 'problemSolnToDr',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        }, {
          title: 'What is Drs feedback/qery about our product',
          data: 'doctorFeedback',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        }, {
          title: 'Have I given him solution to his query?',
          data: 'isQuerySolnGiven',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              if(data== true){
               data= "Yes"
              }else{
               data= "No"
              }
              return data
            }
          }
        }, {
          title: 'Solution Given to Query',
          data: 'querySoln',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        }, {
          title: 'What help I need from Senior',
          data: 'seniorHelp',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        }, {
          title: 'Conversion Done',
          data: 'conversionDone',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              if(data== true){
                data= "Yes"
               }else{
                data= "No"
               }
               return data
                          }
          }
        },
         {
          title: 'Conversion No Reason',
          data: 'conversionNoReason',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        },
        {
          title: 'Quote Given',
          data: 'quoteGiven',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {

        if(data== true){
               data= "Yes"
              }else{
               data= "No"
              }
              return data            }
          }
        },
        {
          title: 'Quotation Given. date and Status',
          data: 'quoteStatus',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        },{
          title: 'Quote Others',
          data: 'quoteOthers',
          render: function (data) {
            if (data === '' || data == 'null' || data == undefined) {
              return '---'
            } else {
              return data
            }
          }
        }
      ],
      dom: 'Bfrtip',
      buttons: [
       
        {
          extend: 'excel',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'copy',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'csv',
          exportOptions: {
            columns: ':visible'
          }
        },
  {
    extend: 'print',
    title: function () {
    return 'State List';
    },
    exportOptions: {
    columns: ':visible'
    },
    action: function (e, dt, node, config) {
        }
       }
      ]
       };
     this.changeDetectorRef.detectChanges()
    if (this.currentUser.company.isDivisionExist == false) {
      this.dtOptions.columns[4].visible = false;
    }
    this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
  }
  this.changeDetectorRef.detectChanges()

    })
  }

}
