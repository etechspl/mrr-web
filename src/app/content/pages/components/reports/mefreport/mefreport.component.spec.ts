import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MEFReportComponent } from './mefreport.component';

describe('MEFReportComponent', () => {
  let component: MEFReportComponent;
  let fixture: ComponentFixture<MEFReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MEFReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MEFReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
