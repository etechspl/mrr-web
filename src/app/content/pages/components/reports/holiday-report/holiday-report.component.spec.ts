import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidayReportComponent } from './holiday-report.component';

describe('HolidayReportComponent', () => {
  let component: HolidayReportComponent;
  let fixture: ComponentFixture<HolidayReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolidayReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidayReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
