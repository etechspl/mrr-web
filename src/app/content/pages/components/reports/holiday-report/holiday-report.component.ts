import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StatusComponent } from '../../filters/status/status.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { StateComponent } from '../../filters/state/state.component';
import { MatSlideToggleChange } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import * as _moment from 'moment';

import Swal from "sweetalert2";
declare var $;


@Component({
  selector: 'm-holiday-report',
  templateUrl: './holiday-report.component.html',
  styleUrls: ['./holiday-report.component.scss']
})
export class HolidayReportComponent implements OnInit {

  public showState: boolean = true;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  public showDateWiseDCRReport: boolean = false;
  public showProcessing: boolean = false;
  public showDetails:boolean=false;
  logoURL: any;
  count = 0;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("setStatus") setStatus: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @ViewChild('dataTable') table;
  @ViewChild('dataTableForDCRDateWise') dataTableForDCRDateWise;
  dataTable: any;
  showAdminAndMGRLevelFilter = false;

  dtOptions: any;
  constructor(  
    private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private exportAsService: ExportAsService,
    private _hierarchyService: HierarchyService,
    private dcrService:DcrReportsService ,                
    private _urlService: URLService) { }
  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  colspanValue;
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;

  ngOnInit() {
    this.logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    this.colspanValue = 9;
    // this.colspanValue = (this.isPOBExist === false )? 8 : 7;
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.holidayForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);



    } else {
      this.isShowDivision = false;
    }

    // if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
    //   this.showAdminAndMGRLevelFilter = true;
    //   this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    // }
  }
  check: boolean = false;

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  holidayForm = new FormGroup({
    reportType: new FormControl('Geographical'),
  //  type: new FormControl(null, Validators.required),
    status: new FormControl([true]),
    stateInfo: new FormControl(null),
    employeeId: new FormControl(null),
    month: new FormControl(null),
    year: new FormControl(null),
    designation: new FormControl(null),
    toDate: new FormControl(null, Validators.required),
    fromDate: new FormControl(null, Validators.required)
  })
  getReportType(val) {
    if (val === "Geographical") {
      this.showState = true;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {

      this.showState = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

    }
  }
  // getTypeValue(val) {
    
  //   this.showReport = false;
  //   if (val == "State") {
  //     this.showDistrict = false;
  //     if (this.isDivisionExist === true) {
  //       this.callDivisionComponent.setBlank();
  //     }
  //     this.callStateComponent.setBlank();
  //     this.holidayForm.setControl("stateInfo", new FormControl(null, Validators.required))
  //     this.holidayForm.removeControl("employeeId");
  //     this.holidayForm.removeControl("designation");
  //     this.holidayForm.removeControl("districtInfo");

  //   }
  //   else if (val == "Headquarter") {
  //     this.callStateComponent.setBlank();
  //     this.holidayForm.removeControl("employeeId");
  //     this.holidayForm.setControl("stateInfo", new FormControl(null,Validators.required))
  //     this.holidayForm.setControl("districtInfo", new FormControl(null,Validators.required))
  //     if (this.isDivisionExist === true) {
  //       this.callDivisionComponent.setBlank();
  //     }
  //     this.holidayForm.removeControl("designation");
  //     this.showDistrict = true;
  //   }
  //   else if (val == "Employee Wise") {
  //     this.showEmployee = true;
  //     if (this.isDivisionExist === true) {
  //       this.callDivisionComponent.setBlank();
  //     }
  //     this.holidayForm.removeControl("stateInfo");
  //     this.holidayForm.removeControl("districtInfo");
  //     this.holidayForm.setControl("designation", new FormControl(null,Validators.required))
  //     this.holidayForm.setControl("employeeId", new FormControl(null,Validators.required))
  //   } else if (val == "Self") {
  //     this.showEmployee = false;
  //   }

  //   if (this.isDivisionExist === true) {
  //     this.isShowDivision = true;
  //   } else {
  //     this.isShowDivision = false;
  //   }

  //   this.holidayForm.patchValue({ type: val });
  //   this._changeDetectorRef.detectChanges();


  // }
  getStateValue(val) {
    this.holidayForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.holidayForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.holidayForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.holidayForm.value.divisionId
        })

        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.holidayForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }
  getDistrictValue(val) {
    this.holidayForm.patchValue({ districtInfo: val });
  }
  getEmployeeValue(val) {
    this.holidayForm.patchValue({ employeeId: val });
  }
  getFromDate(val) {
    this.holidayForm.patchValue({ fromDate: val });

  }
  getToDate(val) {
    this.holidayForm.patchValue({ toDate: val });
  }

  getDesignation(val) {
    this.holidayForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.holidayForm.value.divisionId,
        status: [true],
        designationObject: this.holidayForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  //setting division into holidayForm
  getDivisionValue($event) {
    this.holidayForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
     // if (this.holidayForm.value.type === "State" || this.holidayForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.holidayForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
     // }

      // if (this.holidayForm.value.type === "Employee Wise") {
      //   //getting employee if we change the division filter after select the designation
      //   if (this.holidayForm.value.divisionId != null && this.holidayForm.value.designation != null) {
      //     if (this.holidayForm.value.designation.length > 0) {
      //       this.callEmployeeComponent.getEmployeeListBasedOnDivision({
      //         companyId: this.companyId,
      //         division: this.holidayForm.value.divisionId,
      //         status: [true],
      //         designationObject: this.holidayForm.value.designation
      //       })
      //     }
      //   }
      // }
    }
  }
  getStatusValue(val){}
  viewRecords(){
    this.isToggled = false;
    this.showReport = false;
    this.showProcessing = true;
    this.showDetails=true;
    if (this.holidayForm.value.employeeId == null) {
      this.holidayForm.value.employeeId = [this.currentUser.id];
    }
    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.holidayForm.value.divisionId;
    } else {
      params["division"] = [];
    }

    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.holidayForm.value.stateInfo;
    params["districtIds"] = this.holidayForm.value.districtInfo;
    params["type"] = this.holidayForm.value.type;
    params["employeeId"] = this.holidayForm.value.employeeId;
    params["fromDate"] = this.holidayForm.value.fromDate;
    params["toDate"] = this.holidayForm.value.toDate;
    params["status"] = this.holidayForm.value.status;

    this.dcrService.getHolidayDetails(params).subscribe(result => {
      this.showProcessing=false;
      if (result.length == 0 || result == undefined) {
        (Swal as any).fire({
          title: "No Records Found",
          type: "info",
        });
      }else{
        result.forEach((item, i) => {
          item["index"] = i + 1;
        });
        this.dtOptions = {
          pagingType: "full_numbers",
          ordering: true,
          info: true,
          scrollY: 300,
          scrollX: true,
          scrollCollapse: true,
          paging: false,
          destroy: true,
          data: result,
          responsive: true,
          columns: [
            {
              title: "S No.",
              data: "index",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "State Name",
              data: "state",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },{
              title: "Holiday Name",
              data: "holidayName",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },{
              title: "Holiday Date",
              data: "holidayDate",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return  _moment(data).format("MMM DD, YYYY")
                }
              },
              defaultContent: "---",
            },
          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Records",
          },
          dom: "Bfrtip",
          buttons: [
            {
              extend: "excel",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: "csv",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: "print",
              exportOptions: {
                columns: ":visible",
              },
            },
          ],
        };
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
      }

    })
  }
}
