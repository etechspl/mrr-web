import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption, MatPaginator, MatSelect, MatSort } from '@angular/material';
import { ExportAsConfig } from 'ngx-export-as';
import * as XLSX from 'xlsx';
import * as _moment from "moment";
import { ToastrService } from 'ngx-toastr';
import { DistrictService } from '../../../../../core/services/district.service';
import { StateService } from '../../../../../core/services/state.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { ExpenseClaimedService } from '../../../../../core/services/expense-claimed.service';

@Component({
  selector: 'm-expense-report-dump',
  templateUrl: './expense-report-dump.component.html',
  styleUrls: ['./expense-report-dump.component.scss']
})
export class ExpenseReportDumpComponent implements OnInit {

  isShowDivision = false;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  showTable = false;
  month = true;
  isDisabledBtn: Boolean = false;
  showProcessing = false;
  allSelected: Boolean = false;
  allSelectedHq: Boolean = false;
  dataSource = [];
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild('paginator1') paginator1: MatPaginator;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild('selectHQ') selectHQ: MatSelect;
  @ViewChild('select') select: MatSelect;
  progressValue: number = 0;
  constructor(
    private _toastrService: ToastrService,
    private _urlService: URLService,
    private fb: FormBuilder,
    private _expense: ExpenseClaimedService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _stateService: StateService,
    private _districtService: DistrictService
  ) {

  }

  ExpenseDumpForm = new FormGroup({
    //divisionId: new FormControl("", Validators.required),
    stateInfo: new FormControl("", Validators.required),
    districtInfo: new FormControl("", Validators.required),
    month: new FormControl("", Validators.required),
    year: new FormControl("", Validators.required),
  });

  getDivisionValue(val) {
    this.showTable = false;
    this.ExpenseDumpForm.patchValue({ divisionId: val })
    this.getStates()
  }

  stateData: any;
  stateIdArr = [];
  getStates() {
    if(this.currentUser.company.isDivisionExist === true){
      let obj = {
        companyId:this.currentUser.companyId,
        divisionId:this.ExpenseDumpForm.value.divisionId
      }
      this._stateService.getStatesBasedOnDivision(obj).subscribe(res=>{
        this.stateData = res[0].stateInfo; 
      })
    }else{
      this._stateService.getStates(this.currentUser.companyId).subscribe(res => {
        this.stateData = res[0].stateInfo;
      })
    }
  }


  HqData: any;
  getStateValue(val) {
    this.stateIdArr = [];
    this.ExpenseDumpForm.patchValue({ stateInfo: val })
    val.forEach(element => {
      this.stateIdArr.push(element.id);
    });
    this.HqData = [];
    this._districtService.getDistrict(this.currentUser.companyId, this.stateIdArr).subscribe(res => {
      
      this.HqData = res[0].districtObj;
      res[0].districtObj.sort(function (a, b) {
      var textA = a.districtName.toUpperCase();
      var textB = b.districtName.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    
      })
    })
  }

    

  getDistrictValue(val) {
    this.ExpenseDumpForm.patchValue({ districtInfo: val })
    this.isDisabledBtn=false; 
      this.showTable=false;
  }


  getMonth(val) {
    this.month = false;
    this.isDisabledBtn=false; 
    this.showTable = false;
    this.ExpenseDumpForm.patchValue({ month: val })

  }

  getYear(val) {
    this.showTable = false;
    this.ExpenseDumpForm.patchValue({ year: val })
  }
 
  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  toggleAllSelectionForHQ() {
    this.allSelectedHq = !this.allSelectedHq;  // to control select-unselect
    if (this.allSelectedHq) {
      this.selectHQ.options.forEach((item: MatOption) => item.select());
    } else {
      this.selectHQ.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.ExpenseDumpForm.setControl(
        "divisionId",
        new FormControl()
      );
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel,
      };
      this.callDivisionComponent.getDivisions(obj);
    } else {
      this.isShowDivision = false;
      this.getStates();
    }
    // this.getStates();
  }

  async getExpenseDetails() {
    this.showProcessing=true;
    let fromDate = _moment(this.ExpenseDumpForm.value.year + "-" + this.ExpenseDumpForm.value.month).startOf('month').format("YYYY-MM-DD");
    let toDate = _moment(this.ExpenseDumpForm.value.year + "-" + this.ExpenseDumpForm.value.month).endOf('month').format("YYYY-MM-DD");
    
let params={
  companyId: this.currentUser.companyId,
  stateId: this.ExpenseDumpForm.value.stateInfo,
    districtId: this.ExpenseDumpForm.value.districtInfo,
    
};
    params['fromDate'] = fromDate;
    params['toDate'] = toDate;

    const data = await this.getExpenseDump({ ...params });
      if (data) 
      this.dataSource = data;
      this.showProcessing = false;
      this.isDisabledBtn=true; 
      this.showTable=true;
      this._changeDetectorRef.detectChanges();
  }



  getExpenseDump(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._expense.getExpenseDump(params).subscribe(res => {
        if (res) {
          resolve(res);
        }
      }, err => reject(err))
    })
  }

  workSheet;
  workBook;
  exportTable() {
    let dataToExport = [];
    this.workBook = XLSX.utils.book_new();
    this.dataSource['userData'].forEach((elem) => {
      dataToExport.length=0;
      let userwiseData=elem.userId;
      if(this.dataSource['data'][userwiseData])
      this.dataSource['data'][userwiseData].forEach((data) => {
       
        const dataToExportAll = {
         // const dataToExportAll = data.map((x) => ({
            "Emp Code": data.employeeCode? data.employeeCode:"---",
            "Emp Name": data.empname,
            "HQ": data.headquarter,
            "Date": _moment.utc(data.date).format('D MMM YYYY'),
            "Tour Program":data.tpAreas? data.tpAreas.toString():"---",
            "Visited (From--To)": data.VisitedAreas? data.VisitedAreas.toString():"---",
            "Area Type": data.areaType,
            "Distance As per STP": data.stpDistance,
            "Distance As per Google": 0,
            "Fare": data.fare,
            "Working Type": data.workType,
            "DA": data.da,
            "Doctor Calls": data.doctorCalls,
            "Chemist Calls": data.chemistCalls,
            "1st Call Time": _moment.utc(data.timeIn).format('D MMM YYYY HH:MM')? data.timeIn:"---",
            "Last Call Time":_moment.utc(data.timeOut).format('D MMM YYYY HH:MM')? data.timeOut:"---",
            "Worked With": data.jointWorks? data.jointWorks.toString():"---",
            "Misc Expense Amount":data.miscExpAmount? data.miscExpAmount.toString():"0",
            "Misc Expense Type": data.miscExpType? data.miscExpType.toString():"---",
            "Total Expense Admin": data.totalExpenseAdmin,
            "Remarks": data.remarks,
          };
          dataToExport.push(dataToExportAll)
      });
      
      this.workSheet = XLSX.utils.json_to_sheet(dataToExport);
      XLSX.utils.book_append_sheet(this.workBook, this.workSheet, elem.name.substr(0,29));
     
    });
    XLSX.writeFile(this.workBook, "Expense Report Dump.xlsx");
    this._changeDetectorRef.detectChanges();


    // this.dataSource.forEach((elem) => {
    //   if (elem.empname != "NA") {
    //     const dataToExport = elem.data.map((x) => ({
    //         "Emp Code": x.employeeCode,
    //         "Emp Name": x.empname,
    //         "HQ": x.headquarter,
    //         "Date": x.date,
    //         "Visited (From--To)": x.productRate,
    //         "Distance As per STP": x.stpDistance,
    //         "Distance As per Google": 0,
    //         "Fare": x.fare,
    //         "Working Type": x.workType,
    //         "Area Type": x.areaType,
    //         "DA": x.da,
    //         "Doctor Calls": x.RMPCount,
    //         "Chemist Calls": x.DrugStoreCount,
    //         "Stockist Calls": x.stockistCount,
    //         "1st Call Time": x.timeIn,
    //         "Last Call Time": x.timeOut,
    //         "Worked With": x.jointWork,
    //         "Total Expense Admin": x.totalExpenseAdmin,
    //         "Remarks": x.remarks,
    //       }));
        
    //     this.workSheet = XLSX.utils.json_to_sheet(dataToExport);
    //     XLSX.utils.book_append_sheet(this.workBook, this.workSheet, elem.userId);
    //   }
    // });
    // XLSX.writeFile(this.workBook, "Expense Report Dump.xlsx");
    // this._changeDetectorRef.detectChanges();
  }
}
