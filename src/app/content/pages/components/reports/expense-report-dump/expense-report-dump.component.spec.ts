import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseReportDumpComponent } from './expense-report-dump.component';

describe('ExpenseReportDumpComponent', () => {
  let component: ExpenseReportDumpComponent;
  let fixture: ComponentFixture<ExpenseReportDumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenseReportDumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseReportDumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
