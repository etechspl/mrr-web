import { ToastrService } from "ngx-toastr";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DcrProviderVisitDetailsService } from "./../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service";
import { TypeComponent } from "./../../filters/type/type.component";
import { EmployeeComponent } from "./../../filters/employee/employee.component";
import { DistrictComponent } from "./../../filters/district/district.component";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { DivisionComponent } from "../../filters/division/division.component";
import { StateComponent } from "../../filters/state/state.component";
import { DesignationComponent } from "../../filters/designation/designation.component";
import { MatSlideToggleChange } from "@angular/material";
import * as _moment from "moment";
import { URLService } from "../../../../../core/services/URL/url.service";
declare var $;

@Component({
	selector: "m-vendorvisitdetail",
	templateUrl: "./vendorvisitdetail.component.html",
	styleUrls: ["./vendorvisitdetail.component.scss"],
})
export class VendorvisitdetailComponent implements OnInit {
	public showState: boolean = false;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	public showDateWiseDCRReport: boolean = false;
	public showDivisionFilter: boolean = false;

	//dcrForm: FormGroup;
	displayedColumns = [
		"providerName",
		"blockName",
		"districtName",
		"stateName",
		"providerCode" /*, 'category'*/,
		"address",
		"phone",
		"status" /*, 'frequencyVisit'*/,
		"totalVisit",
		"visitDates",
	];
	dataSource;
	@ViewChild("dataTable") table;
	dataTable: any;
	dtOptions: any;
	showProcessing: boolean = false;
	showAdminAndMGRLevelFilter = false;

	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;

	constructor(
		private _dcrProviderVisitService: DcrProviderVisitDetailsService,
		private _toastr: ToastrService,
		private _changeDetectorRef: ChangeDetectorRef,
		private _urlService: URLService
	) {}
	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;

	dcrForm = new FormGroup({
		reportType: new FormControl("Geographical"),
		type: new FormControl(null, Validators.required),
		stateInfo: new FormControl(null),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		month: new FormControl(null),
		year: new FormControl(null),
		designation: new FormControl(null),
		toDate: new FormControl(null, Validators.required),
		fromDate: new FormControl(null, Validators.required),
		category: new FormControl(null),
	});
	check: boolean;
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		console.log("toggle", event.checked);
		this.isToggled = event.checked;
	}

	ngOnInit() {
		//this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
		if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			this.showAdminAndMGRLevelFilter = true;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		}
		if (this.currentUser.company.isDivisionExist == true) {
			this.dcrForm.setControl("division", new FormControl());

			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.showDivisionFilter = true;
		}
	}
	getReportType(val) {
		this.callDivisionComponent.setBlank();
		this.dcrForm.patchValue({ type: "" });

		if (val === "Geographical") {
			this.showState = false;
			this.showDistrict = false;
			this.showEmployee = false;
			if (this.currentUser.company.isDivisionExist == false) {
				this.showDivisionFilter = false;
			} else if (this.currentUser.company.isDivisionExist == true) {
				this.showDivisionFilter = true;
				this.callDivisionComponent.setBlank();
			}
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.showEmployee = true;
			this.showState = false;
			this.showDistrict = false;
			this.showDivisionFilter = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
	}
	getTypeValue(val) {
		if (val == "State") {
			if (this.currentUser.company.isDivisionExist == false) {
				this.showDivisionFilter = false;
			} else if (this.currentUser.company.isDivisionExist == true) {
				this.showDivisionFilter = true;
			}
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}

			this.showState = true;
			this.showDistrict = false;
			this.dcrForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.dcrForm.removeControl("employeeId");
			this.dcrForm.removeControl("designation");
			this.dcrForm.removeControl("districtInfo");
		} else if (val == "Headquarter") {
			if (this.currentUser.company.isDivisionExist == false) {
				this.showDivisionFilter = false;
			} else if (this.currentUser.company.isDivisionExist == true) {
				this.showDivisionFilter = true;
			}
			this.showState = true;
			this.showDistrict = true;
			this.callDivisionComponent.setBlank();
			this.dcrForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.dcrForm.setControl(
				"districtInfo",
				new FormControl("", Validators.required)
			);
			this.dcrForm.removeControl("employeeId");
			this.dcrForm.removeControl("designation");
		} else if (val == "Employee Wise") {
			if (this.currentUser.company.isDivisionExist == false) {
				this.showDivisionFilter = false;
			} else if (this.currentUser.company.isDivisionExist == true) {
				this.showDivisionFilter = true;
				this.callDivisionComponent.setBlank();
			}
			this.showEmployee = true;
			this.dcrForm.removeControl("stateInfo");
			this.dcrForm.removeControl("districtInfo");
			this.dcrForm.setControl(
				"designation",
				new FormControl("", Validators.required)
			);
			this.dcrForm.setControl(
				"employeeId",
				new FormControl("", Validators.required)
			);
		} else if (val == "Self") {
			this.showDivisionFilter = false;
			this.showEmployee = false;
		}

		this.dcrForm.patchValue({ type: val });
	}
	getDivisionValue(val) {
		this.showProcessing = false;
		this.showReport = false;
		this.dcrForm.patchValue({ division: val });

		if (this.currentUser.userInfo[0].designationLevel == 0) {
			if (this.dcrForm.value.type == "Employee Wise") {
				//=======================Clearing Filter===================

				this.callDesignationComponent.setBlank();
				this.callEmployeeComponent.setBlank();
				//===============================END================================

				let passingObj = {
					companyId: this.currentUser.companyId,
					userId: this.currentUser.id,
					userLevel: this.currentUser.userInfo[0].designationLevel,
					division: val,
				};

				this.callDesignationComponent.getDesignationBasedOnDivision(
					passingObj
				);
			} else if (
				this.dcrForm.value.type == "State" ||
				this.dcrForm.value.type == "Headquarter"
			) {
				//=======================Clearing Filter===================
				this.callStateComponent.setBlank();
				this.callDistrictComponent.setBlank();
				//===============================END=======================

				let divisionIds = {
					division: val,
				};
				this.callStateComponent.getStateBasedOnDivision(divisionIds);
			}
		} else if (this.currentUser.userInfo[0].designationLevel >= 2) {
			if (this.dcrForm.value.type == "Employee Wise") {
				//=======================Clearing Filter===================
				this.callDesignationComponent.setBlank();
				this.callEmployeeComponent.setBlank();
				//===============================END================================

				let passingObj = {
					companyId: this.currentUser.companyId,
					userId: this.currentUser.id,
					userLevel: this.currentUser.userInfo[0].designationLevel,
					division: val,
				};

				this.callDesignationComponent.getDesignationBasedOnDivision(
					passingObj
				);
			} else if (
				this.dcrForm.value.type == "State" ||
				this.dcrForm.value.type == "Headquarter"
			) {
				//=======================Clearing Filter===================
				this.callStateComponent.setBlank();
				this.callDistrictComponent.setBlank();
				//===============================END=======================

				let passingObj = {
					companyId: this.currentUser.companyId,
					isDivisionExist: true,
					division: this.dcrForm.value.division,
					supervisorId: this.currentUser.id,
				};
				this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(
					passingObj
				);
			}
		}
	}

	getStateValue(val) {
		this.showReport = false;

		this.dcrForm.patchValue({ stateInfo: val });
		this.callDistrictComponent.setBlank();

		if (this.dcrForm.value.type == "Headquarter") {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					division: this.dcrForm.value.division,
					companyId: this.currentUser.companyId,
					stateId: val,
				};
				this.callDistrictComponent.getDistrictsBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
	}
	getDistrictValue(val) {
		this.dcrForm.patchValue({ districtInfo: val });
	}
	getEmployeeValue(val) {
		this.dcrForm.patchValue({ employeeId: val });
	}
	getFromDate(val) {
		this.dcrForm.patchValue({ fromDate: val });
	}
	getToDate(val) {
		this.dcrForm.patchValue({ toDate: val });
	}

	getDesignation(val) {
		this.dcrForm.patchValue({ designation: val });

		this.callEmployeeComponent.setBlank();

		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					companyId: this.currentUser.companyId,
					designationObject: val,
					status: [true],
					division: this.dcrForm.value.division,
				};
				this.callEmployeeComponent.getEmployeeListBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.callEmployeeComponent.getEmployeeList(
					this.currentUser.companyId,
					val,
					[true]
				);
			}
		} else {
			let desig = [];
			for (let i = 0; i < val.length; i++) {
				desig.push(val[i].designationLevel);
			}
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: desig,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			} else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: desig,
					isDivisionExist: true,
					division: this.dcrForm.value.division,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}

			// this.callEmployeeComponent.getManagerHierarchy(this.currentUser.companyId, val[0].designationLevel, [true]);
		}
	}

	viewRecords() {
		this.isToggled = false;
		let showEmployeeColumn = true;
		if (this.currentUser.userInfo[0].designationLevel == 1) {
			showEmployeeColumn = false;
		}
		let userIds;
		if (
			this.dcrForm.value.type == "Self" ||
			this.dcrForm.value.type == null
		) {
			//Null is for MR level
			userIds = [this.currentUser.id];
		} else {
			userIds = this.dcrForm.value.employeeId;
		}

		let divisionIds = [];
		if (this.currentUser.company.isDivisionExist) {
			divisionIds = this.dcrForm.value.division;
		}

		this.showReport = false;
		this.showProcessing = true;
    let type;
    let otherType;
		if (this.currentUser.company.lables.hasOwnProperty("otherType")) {
			otherType = this.currentUser.company.lables.otherType.map(
				(element) => {
					return element.key;
				}
			);
			otherType.push("Drug", "Stockist");
			// type = returnData;
		} else {
			otherType = ["Drug", "Stockist"];
    }
    console.log('otherType',otherType);
    
		this._dcrProviderVisitService
			.getProviderVisitDetails(
				this.currentUser.companyId,
				this.dcrForm.value.type,
				this.dcrForm.value.stateInfo,
				this.dcrForm.value.districtInfo,
				userIds,
				this.dcrForm.value.fromDate,
				this.dcrForm.value.toDate,
				otherType,
				this.currentUser.company.validation.unlistedVenCall,
				this.currentUser.company.isDivisionExist,
				divisionIds,
				""
			)
			.subscribe(
				(res) => {
					if (res == null || res.length == 0 || res == undefined) {
						this.showReport = false;
						this.showProcessing = false;
						this._toastr.error("Data Not Found", "");
					} else {
						this.dataSource = res;

						console.log("this.dataSource+++++++++++",this.dataSource);

						this.showReport = true;
						this.showProcessing = false;
						res.map((item, index) => {
							item["index"] = index;
						});
						const PrintTableFunction = this.PrintTableFunction.bind(
							this
						);
						this.dtOptions = {
							pagingType: "full_numbers",
							paging: true,
							ordering: true,
							info: true,
							scrollY: 300,
							scrollX: true,
							fixedColumns: true,
							destroy: true,
							data: this.dataSource,
							columns: [
								{
									title: "S. No.",
									data: "index",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data + 1;
										}
									},
									defaultContent: "---",
								},
								{
									title: "State Name",
									data: "stateName",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										this.currentUser.company.lables
											.hqLabel + " Name",
									data: "districtName",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										this.currentUser.company.lables
											.areaLabel + " Name",
									data: "blockName",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									visible: showEmployeeColumn,
									title: "Employee Name",
									data: "userName",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Division",
									data: "divisionName",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										this.currentUser.company.lables
											.vendorLabel + " Name ",
									data: "providerName",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Code",
									data: "providerCode",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Address",
									data: "address",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Phone",
									data: "phone",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Status",
									data: "status",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Total Visit",
									data: "totalVisit",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Visit Dates",
									data: "visitDates",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},

								{ //*******************  Aakash Bist  ****  17-02-2020   *******************
									title: `<table style="width: 100%;">
									<thead><tr><th style=" border-bottom: transparent;" colspan="5" class="text-center">Product Given</th></tr>
									<tr>
										<th style="width: 55%;  border-bottom: transparent;">Product Name</th>
										<th style="width: 15%;  border-bottom: transparent;">POB</th>
										<th style="width: 15%;  border-bottom: transparent;">Order Qty</th>
										<th style="width: 15%;  border-bottom: transparent;">Sample Qty</th>
									</tr></thead></table>`,
									data: 'givenProducts',
									render: function (data, row) {
									  if (data === '' || data == 'null' || data == undefined || data === [] ) {
										return '---'
									  } else {
										let product = ``;
										data.forEach(item => {
										  product += `
										  <tr style="border-style: hidden;background-color: transparent;" >
										  <td style="width: 30%;">${item.productName || '---'}</td>
										  <td style="width: 20%;">${item.pob || '---'}</td>
										  <td style="width: 15%;">${item.availableStock || '---'}</td>
										  <td style="width: 15%;">${item.productQuantity || '---'}</td>
										  </tr>
										 `
										})
										let table = `<table style="width: 100%;" > ${product}</table>`
										return (table)
									  }
					  
									}
								  },
								  
								  
								  {
								   title: 'Discussion',
								   data: 'providerDiscussion',
								   render: function (data) {
									 if (data === '' || data == 'null' || data == undefined) {
									   return '---'
									 } else {
									   return data
									 }
								   }
								 }






							],
							dom: "Bfrtip",
							buttons: [
								{
									extend: "excel",
									exportOptions: {
										columns: ":visible",
									},
								},
								{
									extend: "csv",
									exportOptions: {
										columns: ":visible",
									},
								},
								{
									extend: "copy",
									exportOptions: {
										columns: ":visible",
									},
								},
								{
									extend: "print",
									title: function () {
										return "State List";
									},
									exportOptions: {
										columns: ":visible",
									},
									action: function (e, dt, node, config) {
										PrintTableFunction(res);
									},
								},
							],
						};

						if (this.currentUser.company.isDivisionExist == false) {
							this.dtOptions.columns[5].visible = false;
						}

						this.dataTable = $(this.table.nativeElement);
						this.dataTable.DataTable(this.dtOptions);
					}
					this._changeDetectorRef.detectChanges();
				},
				(err) => {
					this.showReport = false;
					this.showProcessing = false;
					this._toastr.error("Data Not Found", "");
				}
			);
	}

	PrintTableFunction(data?: any): void {
		// const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
		const currentDateAndTime = _moment(new Date()).format(
			"DD/MM/YYYY, dddd , HH:mm A"
		);
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;

		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;

		const tableRows: any = [];
		data.forEach((element) => {
			tableRows.push(`<tr>
	<td class="tg-oiyu">${element.stateName}</td>
	<td class="tg-oiyu">${element.districtName}</td>
	<td class="tg-oiyu">${element.blockName}</td>
	<td class="tg-oiyu">${element.userName}</td>
	<td class="tg-oiyu">${element.divisionName}</td>
	<td class="tg-oiyu">${element.providerName}</td>
	<td class="tg-oiyu">${element.providerCode}</td>
	<td class="tg-oiyu">${element.address}</td>
	<td class="tg-oiyu">${element.phone}</td>
	<td class="tg-oiyu">${element.status}</td>
	<td class="tg-oiyu">${element.totalVisit}</td>
	<td class="tg-oiyu">${element.visitDates}</td>
	</tr>`);
		});

		let showHeaderAndTable: boolean = false;
		// this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
		let printContents, popupWin;
		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
	<html>
	<head>
	<style>
	img {
	float: left;
	}

	.flex-container {
	display: flex;
	justify-content: space-between;
	}

	.tg {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg .tg-lboi {
	border-color: inherit;
	text-align: left;
	vertical-align: middle;

	}

	.tg .tg-u6fn {
	font-weight: bold;
	font-size: 18px;
	background-color: #efefef;
	color:black;
	border-color: inherit;
	text-align: left;
	vertical-align: top;
	padding-top: 0px;
	padding-bottom: 0px;
	}

	.tg .tg-yz93 {
	border-color: inherit;
	text-align: right;
	vertical-align: middle
	}
	.tb2 {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tb2 td {
	font-family: Arial, sans-serif;
	font-size: 12px;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 th {
	font-family: Arial, sans-serif;
	font-size: 12px;
	font-weight: normal;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 .tg-oiyu {
	font-size: 10px;
	text-align: left;
	vertical-align: middle
	}
	.tg-bckGnd {
	font-weight: bold;
	background-color: #efefef;
	}

	.tb2 .tg-3ppa {
	font-size: 10px;
	background-color: #efefef;
	color:black;
	text-align: left;
	vertical-align: middle;
	padding-top: 2px;
	padding-bottom: 2px;
	}

	.footer {
	position: sticky;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: #efefef;
	color: rgb(2, 2, 2);
	text-align: right
	}

	button {
	background-color: #008CBA;
	border: none;
	color: white;
	margin: 5px 0px;
	padding: 2px 10px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
	border-radius: 12px;
	cursor: pointer;
	outline: none;
	}
	@media print {
	@page { size: A4; margin: 5mm; }
	.footer {
	position: relative;
	}
	tr {
	page-break-inside: avoid;
	}
	button{
	display : none;
	}
	}
	</style>
	</head>
	<body onload="window.print();window.close()" style="padding-bottom:30px">
	<div class="flex-container">
	<div style="width: 40%; ">
	<img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	<h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

	</div>

	<div style="text-align: right;">
	<h2 style="margin-top: 0px; margin-bottom: 5px">Chemist / Stockist Visit Report</h2>
	<div>
	<button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	<button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	</div>
	</div>
	</div>

	<div>
	${
		/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/ ""
	}
	<table class="tb2">
	<tr>

	<th class="tg-3ppa"><span style="font-weight:600">State Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Headquarter Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">AreaName</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Employee Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Division</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">${this.currentUser.company.lables.vendorLabel} Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Code</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Address</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Phone</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Status</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Total Visit</span></th>
	<th class="tg-3ppa"><span style="font-weight:600">Visit Dates</span></th>
	</tr>
	${tableRows.join("")}
	</table>
	</div>
	<div class="footer flex-container">
	<p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>

	<p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
	</div>
	</body>
	</html>
	`);

		// popupWin.document.close();
	}
}
