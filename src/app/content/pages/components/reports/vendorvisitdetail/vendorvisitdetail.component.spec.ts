import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorvisitdetailComponent } from './vendorvisitdetail.component';

describe('VendorvisitdetailComponent', () => {
  let component: VendorvisitdetailComponent;
  let fixture: ComponentFixture<VendorvisitdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorvisitdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorvisitdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
