import { ToastrService } from "ngx-toastr";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators, FormControlName } from "@angular/forms";
import { DesignationComponent } from "../../filters/designation/designation.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { StatusComponent } from "../../filters/status/status.component";
import { Designation } from "../../../_core/models/designation.model";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";
import { MatTableDataSource, MatTable, MatSort, MatPaginator, MatSlideToggleChange } from "@angular/material";
import { DcrProviderVisitDetailsService } from "../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service";
import * as moment from "moment";
import { DivisionComponent } from "../../filters/division/division.component";
import { UtilsService } from "../../../../../core/services/utils.service";
import { URLService } from "../../../../../core/services/URL/url.service";
import * as _moment from "moment";
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import { HierarchyService } from "../../../../../core/services/hierarchy-service/hierarchy.service";
import { array } from "@amcharts/amcharts4/core";
import { UserDetailsComponent } from "../user-details/user-details.component";
declare var $;

@Component({
	selector: "m-mgr-joint-work",
	templateUrl: "./mgr-joint-work.component.html",
	styleUrls: ["./mgr-joint-work.component.scss"],
})
export class MgrJointWorkComponent implements OnInit {

	exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementId: 'tableId1', // the id of html/table element
	}

	mgrJointForm: FormGroup;
	currentUser = JSON.parse(sessionStorage.currentUser);
	@ViewChild("mgrDataTableOBJ1") table1;
	@ViewChild("mgrDataTableOBJ2") table3;
	mgrDataTableOBJ1: any;
	mgrDataTableOBJ2: any;

	mgrDataTableOptions1: any;
	mgrDataTableOptions2: any;

	@ViewChild("territoryWiseData") table2;
	territoryWiseData: any;
	territoryWiseDataOption: any;
	monthlyOrYearlyData = [];
	check: boolean = false;
	isStatus: true;
	isMultipleEmp: boolean = true;
	data: any;
	hierarchyResult: any;
	showProcessing1: boolean = false;
	isShowVisitedReport: boolean = false;
	isShowReport: boolean = false;
	showProcessing: boolean = false;
	showProcessingVisit: boolean = false;
	isShowReportVisit: boolean = false;
	showReportTrerritoryWise: boolean = false;
	isMonthlyWise: boolean = true;
	isTerritoryWise: boolean = false;
	constructor(
		private _toastrService: ToastrService,
		private _fb: FormBuilder,
		private utilService: UtilsService,
		private _dcrReportsService: DcrReportsService,
		private _changeDetectorRef: ChangeDetectorRef,
		private _dcrProviderVisitService: DcrProviderVisitDetailsService,
		private _urlService: URLService,
		private exportAsService: ExportAsService,
		private _hierarchy: HierarchyService
	) {
		this.mgrJointForm = new FormGroup({
			type: new FormControl("monthly", Validators.required),
			designation: new FormControl(""),
			status: new FormControl(true),
			team: new FormControl(""),
			userId: new FormControl("", Validators.required),
			month: new FormControl("", Validators.required),
			year: new FormControl("", Validators.required),
			fromDate: new FormControl(""),
			toDate: new FormControl(""),
		});
	}
	//, 'workedWith', 'district', 'area', 'visits'
	displayedColumns = [
		"sn",
		"dcrDate",
		"userName",
		"workedWith",
		"district",
		"visitedArea",
		"visits",
	];

	displayVisitsedColumns = [
		"sn",
		"dcrDate",
		"userName",
		"workedWith",
		"district",
		"visitedArea",
		"providerName",
		"providerType"
	];
	displayedColumnsProviders = [
		"sn",
		"providerName",
		"providerCode",
		"providerType",
		"status",
	];
	dataSource = new MatTableDataSource<any>();
	prodataSource = new MatTableDataSource<any>();
	showDivisionFilter: boolean = false;
	public dynamicTableHeader: any = [];
	teamData: any = [];

	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;

	ngOnInit() {
		if (this.currentUser.company.isDivisionExist == true) {
			this.mgrJointForm.setControl("division", new FormControl());

			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.showDivisionFilter = true;
		}
	}
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}



	getDivisionValue(val) {

		this.showProcessing = false;
		this.mgrJointForm.patchValue({ division: val });

		//=======================Clearing Filter===================

		this.callDesignationComponent.setBlank();
		this.callStatusComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		//===============================END================================

		let passingObj = {
			companyId: this.currentUser.companyId,
			userId: this.currentUser.id,
			userLevel: this.currentUser.userInfo[0].designationLevel,
			division: val,
		};

		this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
	}

	getDesignationList(val) {
		this.mgrJointForm.patchValue({ type: val.value });
		if (this.mgrJointForm.value.type == "monthly") {
			this.isMonthlyWise = true;
			this.isTerritoryWise = false;
			this.mgrJointForm.removeControl("fromDate");
			this.mgrJointForm.removeControl("toDate");
			this.mgrJointForm.setControl(
				"month",
				new FormControl("", Validators.required)
			);
			this.mgrJointForm.setControl(
				"year",
				new FormControl("", Validators.required)
			);
		} else {
			this.isMonthlyWise = false;
			this.isTerritoryWise = true;
			this.mgrJointForm.removeControl("month");
			this.mgrJointForm.removeControl("year");
			this.mgrJointForm.setControl(
				"fromDate",
				new FormControl("", Validators.required)
			);
			this.mgrJointForm.setControl(
				"toDate",
				new FormControl("", Validators.required)
			);
		}
		//=======================Clearing Filter===================
		this.callStatusComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		this.callDesignationComponent.setBlank();
		this.isShowReport = false;
		this.showReportTrerritoryWise = false;
		this.isShowReportVisit = false;
		this.callDesignationComponent.getManagersDesignation();
	}

	//   getDesignationLevel(designation: Designation) {
	//     this.isShowReport = false;
	//     this.showReportTrerritoryWise = false;

	//     //=======================Clearing Filter===================
	//     this.callStatusComponent.setBlank();
	//     this.callEmployeeComponent.setBlank();
	//     //===============================END================================
	//     this.mgrJointForm.patchValue({ designation: designation.designationLevel })
	//   }

	getDesignation(val) {
		this.mgrJointForm.patchValue({ designation: val.designationLevel });
		this.callEmployeeComponent.setBlank();

		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					companyId: this.currentUser.companyId,
					designationObject: [val],
					status: [true],
					division: this.mgrJointForm.value.division,
				};

				this.callEmployeeComponent.getEmployeeListBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.callEmployeeComponent.getEmployeeList(
					this.currentUser.companyId,
					[val],
					[true]
				);
			}
		} else {
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [val.designationLevel], //desig
				};

				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}
			else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [val.designationLevel], //desig,
					isDivisionExist: true,
					division: this.mgrJointForm.value.division,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}
		}
	}



	getStatusValue(val) {
		this.mgrJointForm.patchValue({ status: val });
	}

	getHierachyData(val) {
		this.teamData = val;
		let userIds = val.map((user) => {
			return user.userId
		})
		this.mgrJointForm.patchValue({ team: userIds })
	}
	getEmployee(emp: string) {

		this.isShowReport = false;
		this.showReportTrerritoryWise = false;
		this.mgrJointForm.patchValue({ userId: emp });
		var filter = {};
		filter = { companyId: this.currentUser.companyId, supervisorId: emp, type: "lowerWithUpper" };

		this._hierarchy.getManagerHierarchy(filter).subscribe(hierachyRes => {
			this.hierarchyResult = hierachyRes
		})

	}

	getMonth(month: object) {
		this.mgrJointForm.patchValue({ month: month });
		this.isShowReport = false;
		this.showReportTrerritoryWise = false;
	}

	getYear(year: number) {
		this.isShowReport = false;
		this.showReportTrerritoryWise = false;
		this.mgrJointForm.patchValue({ year: year });
	}

	getFromDate(obj: object) {
		this.isShowReport = false;
		this.mgrJointForm.patchValue({ fromDate: obj });
	}

	getToDate(obj: number) {
		this.isShowReport = false;

		this.mgrJointForm.patchValue({ toDate: obj });
	}
	getDateWiseReport() {
		alert("hello preeti")
	}


	generateReport() {
		this.isToggled = false;
		if (!this.mgrJointForm.valid) {
			this._toastrService.error(
				"Select the required field to generate the report",
				"All Field required"
			);
			return;
		}

		this.showProcessing = true;
		this.isShowReport = false;

		if (this.mgrJointForm.value.type == "trerritoryWise") {

			let params = {
				companyId: this.currentUser.companyId,
				submitBy: this.mgrJointForm.value.team,
				status: this.mgrJointForm.value.status,
				fromDate: this.mgrJointForm.value.fromDate,
				toDate: this.mgrJointForm.value.toDate
			}
			this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.mgrJointForm.value.fromDate, this.mgrJointForm.value.toDate);

			this._dcrReportsService.getManagerWorkedTrerritoryWise(params).subscribe(res => {

				if (res.length == 0 || res == undefined) {
					this.showProcessing = false;
					this._toastrService.info('For Selected Filter data not found', 'Data Not Found');
				} else {
					this.showProcessing = false;
					this.showReportTrerritoryWise = true;
					this.data = res;
				}
				this._changeDetectorRef.detectChanges();
			}, err => {
				console.log(err)
			});
		} else {
			let params = {
				companyId: this.currentUser.companyId,
				submitBy: this.mgrJointForm.value.team,
				month: this.mgrJointForm.value.month,
				year: this.mgrJointForm.value.year,
				status: this.mgrJointForm.value.status,
			};

			this._dcrReportsService.getManagerWorkedWithMonthlyOrYearly(params)
				.subscribe(
					(res) => {
						this.showProcessing = false;
						this.isShowReport = true;
						this.monthlyOrYearlyData = res;
						let array = res.map((item) => {
							return {
								...item,
							};
						});

						if (array.length > 0) {
							this.dataSource = new MatTableDataSource(array);

							this.showProcessing = false;
							this.isShowReport = true;
							this.showReportTrerritoryWise = false;
						} else {
							this.isShowReport = false;
							this.showProcessing = false;
							this._toastrService.info(
								"Selected Manager data is not availabe for selected month & year.",
								"No Data available."
							);
						}
						const PrintTableFunction = this.PrintTableFunction.bind(this);
						this.mgrDataTableOptions1 = {
							pagingType: "full_numbers",
							paging: true,
							ordering: true,
							info: true,
							scrollY: 300,
							scrollX: true,
							fixedColumns: true,
							destroy: true,
							data: res,
							responsive: true,
							columns: [
								{
									title: "DCR Date",
									data: "dcrDate",

									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return moment(
												new Date(data)
											).format("DD-MMM-YYYY");
										}
									},
									defaultContent: "---",
								},
								{
									title: "Employee Name",
									data: "userName",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Work With",
									data: "workWith",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: this.currentUser.company.lables
										.hqLabel,
									data: "district",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title:
										this.currentUser.company.lables
											.areaLabel + " Visited",
									data: "visitedArea",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								},
								{
									title: "Total POB",
									data: "finalPob",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return data;
										}
									},
									defaultContent: "---",
								}, {
									title: "Visits",
									data: "workWithVisit",
									render: function (data) {
										if (data === "") {
											return "---";
										} else {
											return `<a class='getDateWiseReport' style="color: #3f51b5; cursor:pointer;font-weight: 600;">${data}</a>`;
										}
									},
									defaultContent: "---",
								}
								// {
								//   title: 'View Detail',
								//   defaultContent: "<button class='btn btn-warning's><i class='la la-eye'></i></button>"
								// }
							],
							rowCallback: (row: Node, data: any[] | Object, index: number) => {
								const self = this;
								// Unbind first in order to avoid any duplicate handler
								// (see https://github.com/l-lin/angular-datatables/issues/87)
								$("td", row).unbind("click");
								$("td .getDateWiseReport", row).bind(
									"click",
									() => {

										let rowObject = JSON.parse(JSON.stringify(data));

										self.workWithVisitedDetails(rowObject, this.mgrJointForm.value);
									});
								return row;
							},

							language: {
								search: "_INPUT_",
								searchPlaceholder: "Search records",
							},
							dom: "Bfrtip",
							// buttons: [
							//   'copy', 'csv', 'excel', 'print'
							// ]

							buttons: [
								{
									extend: "copy",
									exportOptions: {
										columns: ":visible",
									},
								},
								{
									extend: "csv",
									exportOptions: {
										columns: ":visible",
									},
								},
								{
									extend: "excel",
									exportOptions: {
										columns: ":visible",
									},
								},

								//********************* UMESH 19-03-2020 ************************ */
								{
									extend: "print",
									title: function () {
										return "State List";
									},
									exportOptions: {
										columns: ":visible",
									},
									action: function (e, dt, node, config) {
										PrintTableFunction(res);
									},
								},
							],
						};
						this._changeDetectorRef.detectChanges();
						this.mgrDataTableOBJ1 = $(this.table1.nativeElement);
						this.mgrDataTableOBJ1.DataTable(
							this.mgrDataTableOptions1
						);
						this._changeDetectorRef.detectChanges();
					},
					(err) => {
						console.log(err);
					}
				);
		}
	}




	workWithVisitedDetails(val, userDetals) {

		this.isToggled = false;
		this.showProcessingVisit = true;
		this.isShowReportVisit = false;
		let params = {
			companyId: this.currentUser.companyId,
			dcrDate: val.dcrDate
		};
		let i = 0;
		console.log(val);		
		userDetals.team.forEach(element => {
			
			if (val.userName == this.teamData[i].name) {
				console.log(this.teamData[i].name);
				
				if (this.teamData[i].userId == element) {
					console.log(this.teamData);
					params['submitBy'] = this.teamData[i].userId;
				}
			}
			i+=1;
		});

		this._dcrReportsService.getMgrVisitDetails(params).subscribe(
			(res) => {
				if (res.length > 0) {
					this.showProcessingVisit = false;
					this.isShowReportVisit = true;
					// const PrintTableFunction = this.PrintTableFunction.bind(
					// 	this
					// );
					this.mgrDataTableOptions2 = {
						pagingType: "full_numbers",
						paging: true,
						ordering: true,
						info: true,
						scrollY: 300,
						scrollX: true,
						fixedColumns: true,
						destroy: true,
						data: res,
						responsive: true,
						columns: [
							{
								title: "State",
								data: "stateName",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data
									}
								},
								defaultContent: "---",
							},
							{
								title: "District",
								data: "districtName",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data
									}
								},
								defaultContent: "---",
							},
							{
								title: "Employee Name",
								data: "userName",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data
									}
								},
								defaultContent: "---",
							},
							{
								title: "Provider Name",
								data: "providerName",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data
									}
								},
								defaultContent: "---",
							},
							{
								title: "DCR Date",
								data: "dcrDate",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return moment(
											new Date(data)
										).format("DD-MMM-YYYY");
									}
								},
								defaultContent: "---",
							},
							{
								title: "Remarks",
								data: "remarks",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data
									}
								},
								defaultContent: "---",
							},
							{
								title: "Punch Date",
								data: "punchDate",

								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data
									}
								},
								defaultContent: "---",
							}
							,
							// { //*******************  Preeti Arora 2-11-2020  *******************
							// 	title: `<table style="width: 100%;">
							// 	<tr>
							// 		<th style="width: 55%;  border-bottom: transparent;">Tagged Address</th>
							// 		<th style="width: 15%;  border-bottom: transparent;">Tagged Time</th>
							// 	</tr></thead></table>`,
							// 	data: 'productDetails',
							// 	render: function (data, row) {
							// 	  if (data === '' || data == 'null' || data == undefined || data === [] ) {                
							// 		return '---'
							// 	  } else {
							// 		let product = ``;
							// 		data.forEach(item => {                  
							// 			product = product + `
							// 		  <tr style="border-style: hidden;background-color: transparent;" >
							// 		  <td style="width: 30%;">${item.productName}</td>
							// 		  <td style="width: 20%;">${item.productPrice}</td>
							// 		  <td style="width: 20%;">${item.pob}</td>
							// 		  </tr>
							// 		 `
							// 		})
							// 		let table = `<table style="width: 100%;" > ${product}</table>`
							// 		return (table)
							// 	  }

							// 	}
							//   }
						],
						rowCallback: (row: Node, data: any[] | Object, index: number) => {
							const self = this;
							// Unbind first in order to avoid any duplicate handler
							// (see https://github.com/l-lin/angular-datatables/issues/87)
							$("td", row).unbind("click");
							$("td .getDateWiseReport", row).bind(
								"click",
								() => {

									let rowObject = JSON.parse(JSON.stringify(data));

									self.workWithVisitedDetails(rowObject, this.mgrJointForm.value);
								});
							return row;
						},

						language: {
							search: "_INPUT_",
							searchPlaceholder: "Search records",
						},
						dom: "Bfrtip",
						// buttons: [
						//   'copy', 'csv', 'excel', 'print'
						// ]

						buttons: [
							{
								extend: "copy",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "csv",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "excel",
								exportOptions: {
									columns: ":visible",
								},
							},

							//********************* UMESH 19-03-2020 ************************ */
							{
								extend: "print",
								title: function () {
									return "State List";
								},
								exportOptions: {
									columns: ":visible",
								},
								// 	action: function (e, dt, node, config) {
								// 		PrintTableFunction(res);
								// 	},
							},
						],
					};
					this._changeDetectorRef.detectChanges();
					this.mgrDataTableOBJ2 = $(this.table3.nativeElement);
					this.mgrDataTableOBJ2.DataTable(
						this.mgrDataTableOptions2
					);
					this._changeDetectorRef.detectChanges();

				} else {
					this.isShowReportVisit = false;
					this.showProcessing = false;
					this._toastrService.info(
						"Selected Manager data is not availabe for selected month & year.",
						"No Data available."
					);
				}

			},
			(err) => {
				console.log(err);
			}
		);
	}


	/////////**************************Umesh Kumar 12-05-2020***************************

	exporttoExcel() {
		// download the file using old school javascript method
		this.exportAsService.save(this.exportAsConfig, 'Manager Joint Working Report').subscribe(() => {
			// save started
		});
	}

	///*****************************************UMESH 19-03-2020 *************/

	PrintTableFunction(data?: any): void {
		//const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
		const currentDateAndTime = _moment(new Date()).format(
			"DD/MM/YYYY, dddd , HH:mm A"
		);
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;

		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER + "/" + this.currentUser.company.logo.container + "/download/" + this.currentUser.company.logo.modifiedFileName;

		let tableRows: any = [];
		data.forEach((element) => {
			tableRows.push(`<tr>
    <td class="tg-oiyu">${element.dcrDate}</td>
    <td class="tg-oiyu">${element.workWith}</td>
    <td class="tg-oiyu">${element.district}</td>
    <td class="tg-oiyu">${element.visitedArea}</td>
    <td class="tg-oiyu">${element.workWithVisit}</td>
    </tr>`);
		});

		let showHeaderAndTable: boolean = false;
		//this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;

		let printContents, popupWin;
		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
    <html>
    <head>
    <style>
    img {
    float: left;
    }

    .flex-container {
    display: flex;
    justify-content: space-between;
    }

    .tg {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }

    .tg td {
    font-family: Arial, sans-serif;
    font-size: 14px;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tg th {
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tg .tg-lboi {
    border-color: inherit;
    text-align: left;
    vertical-align: middle;

    }

    .tg .tg-u6fn {
    font-weight: bold;
    font-size: 18px;
    background-color: #efefef;
    color:black;
    border-color: inherit;
    text-align: left;
    vertical-align: top;
    padding-top: 0px;
    padding-bottom: 0px;
    }

    .tg .tg-yz93 {
    border-color: inherit;
    text-align: right;
    vertical-align: middle
    }

    .tb2 {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }

    .tb2 td {
    font-family: Arial, sans-serif;
    font-size: 12px;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tb2 th {
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-weight: normal;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tb2 .tg-oiyu {
    font-size: 10px;
    text-align: left;
    vertical-align: middle
    }
    .tg-bckGnd {
    font-weight: bold;
    background-color: #efefef;
    }


    .tb2 .tg-3ppa {
    font-size: 10px;
    background-color: #efefef;
    color:black;
    text-align: left;
    vertical-align: middle;
    padding-top: 2px;
    padding-bottom: 2px;
    }

    .footer {
    position: sticky;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #efefef;
    color: rgb(2, 2, 2);
    text-align: right
    }



    button {
    background-color: #008CBA;
    border: none;
    color: white;
    margin: 5px 0px;
    padding: 2px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    border-radius: 12px;
    cursor: pointer;
    outline: none;
    }

    @media print {
    @page { size: A4; margin: 5mm; }

    .footer {
    position: relative;
    }
    tr {
    page-break-inside: avoid;
    }

    button{
    display : none;
    }
    }
    </style>
    </head>
    <body onload="window.print();window.close()" style="padding-bottom:30px">
    <div class="flex-container">
    <div style="width: 40%; ">
    <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
    <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
    </div>

    <div style="text-align: right;">
    <h2 style="margin-top: 0px; margin-bottom: 5px">Manager Joint Working Report</h2>
    <div>
    <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
    <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
    </div>
    </div>
    </div>

    <div>
    ${
		/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/ ""
			}
    <table class="tb2">
    <tr>
    <th class="tg-3ppa"><span style="font-weight:600">DCR Date</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Work With</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Headquarter</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Area Visited</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Visit</span></th>
    </tr>
    ${tableRows.join("")}
    </table>
    </div>
    <div class="footer flex-container">
    <p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>

    <p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
    </div>
    </body>
    </html>
    `);

		//popupWin.document.close();
	}
}
