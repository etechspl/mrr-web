import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MgrJointWorkComponent } from './mgr-joint-work.component';

describe('MgrJointWorkComponent', () => {
  let component: MgrJointWorkComponent;
  let fixture: ComponentFixture<MgrJointWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MgrJointWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MgrJointWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
