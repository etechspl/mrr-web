import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTable, MatSlideToggleChange } from '@angular/material';
import { Designation } from '../../../_core/models/designation.model';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DivisionComponent } from '../../filters/division/division.component';
declare var $;

@Component({
  selector: 'm-mgr-team-performance',
  templateUrl: './mgr-team-performance.component.html',
  styleUrls: ['./mgr-team-performance.component.scss']
})
export class MgrTeamPerformanceComponent implements OnInit {

  mgrTeamForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(
    private _toastrService: ToastrService,
    private _fb: FormBuilder,
    private _dcrReportsService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef
  ) {
    this.createForm(this._fb);
  }

  check: boolean = false;
  isStatus: boolean = true;
  showProcessing: boolean = false;
  isMultipleEmp: boolean = false;
  data: any;
  isShowReport: boolean = false;
  //, 'workedWith', 'district', 'area', 'visits'
  displayedColumns = ['sn', 'district', 'name', 'designation', 'workingDCR', 'otherDCR', 'pendingDCR', 'incompleteDCR', 'tpSubmitted', 'tpApproved', 'totalDoctor', 'totalDoctorTimePeriod', 'totalChemist', 'drCall', 'drCallAvg', 'listedDoctorCall', 'listedDoctorCallAvg', 'listedDoctorCoverage', 'listedActiveDoctorCoverage', 'listedVendorCall', 'listedVendorCallAvg', 'listedVendorCoverage', 'untouchedProvider', 'untouchedProvider1'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild("designationComponent") designationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;

  @ViewChild('dataTable') table1;
  dataTable: any;
  dtOptions: any;
  dtTrigger: any;
  showDivisionFilter: boolean = false;

  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    console.log('toggle', event.checked);
    this.isToggled = event.checked;
  }
  ngOnInit() {

    if (this.currentUser.company.isDivisionExist == true) {

      this.mgrTeamForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  createForm(_fb: FormBuilder) {

    this.mgrTeamForm = _fb.group({
      designation: new FormControl('', Validators.required),
      status: new FormControl(true),
      userId: new FormControl('', Validators.required),
      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })

  }
  getDivisionValue(val) {
    this.showProcessing = false;
    this.mgrTeamForm.patchValue({ division: val });
    //=======================Clearing Filter===================

    this.designationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.designationComponent.getDesignationBasedOnDivision(passingObj);
  }

  getDesignationList() {
    //this.designationComponent.getManagersDesignation();
  }

//   getDesignationLevel(designation: Designation) {
//     this.mgrTeamForm.patchValue({ designation: designation.designationLevel })
//   }

  getDesignation(val) {
     this.mgrTeamForm.patchValue({ designation: val.designationLevel });
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.mgrTeamForm.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
      }
    } else {
      let dynamicObj = {};
      if (this.currentUser.company.isDivisionExist == false) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel] //desig
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      } else if (this.currentUser.company.isDivisionExist == true) {
        dynamicObj = {
          supervisorId: this.currentUser.id,
          companyId: this.currentUser.companyId,
          status: true,
          type: "lower",
          designation: [val.designationLevel], //desig,
          isDivisionExist: true,
          division: this.mgrTeamForm.value.division
        };
        this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
      }

    }
  }


  getStatusValue(val) {
	this.mgrTeamForm.patchValue({ status: val });
  }

  getEmployee(emp: string) {
    this.mgrTeamForm.patchValue({ userId: emp })
  }

  getFromDate(month: object) {
    this.mgrTeamForm.patchValue({ month: month })
  }

  getToDate(year: number) {
    this.mgrTeamForm.patchValue({ year: year })
  }

  generateReport() {
	  this.isToggled = false;
    this.isShowReport = false;
    if (!this.mgrTeamForm.valid) {
      this._toastrService.error("Select the required field to generate the report", "All Field required");
      return;
    }
    this.showProcessing = true;
    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.mgrTeamForm.value.division;
    }
    let params = {
      companyId: this.currentUser.companyId,
      status: this.mgrTeamForm.value.status,
      supervisorId: this.mgrTeamForm.value.userId,
      designation: this.mgrTeamForm.value.designation,
      month: this.mgrTeamForm.value.month,
      year: this.mgrTeamForm.value.year,
      type: "lowerWithMgr",
      unlistedValidations: {
        unlistedDocCall: this.currentUser.company.validation.unlistedDocCall,
        unlistedVenCall: this.currentUser.company.validation.unlistedVenCall,
        unlistedDocAvg: this.currentUser.company.validation.unlistedDocAvg,
        unlistedVenAvg: this.currentUser.company.validation.unlistedVenAvg,
      },
      isDivisionExist: this.currentUser.company.isDivisionExist,
      division: divisionIds
	}
    this._dcrReportsService.getManagerTeamPerformance(params).subscribe(
      res => {

        let array = res.map(item => {
          return {
            ...item
          };
        });

        if (array.length > 0) {

          this.dataSource = new MatTableDataSource(array);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.isShowReport = true;
          this.showProcessing = false;
          this.dtOptions = {
            "pagingType": 'full_numbers',
            "paging": true,
            "ordering": true,
            "info": true,
            "scrollY": 300,
            "scrollX": true,
            "fixedColumns": true,
            data: res,
            destroy: true,

            columns: [

              {
                title: "Division Name",
                data: 'divisionName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.hqLabel + " Name",
                data: 'districtName',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },

              {
                title: 'Name',
                data: 'name',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Designation',
                data: 'designation',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Working DCR',
                data: 'workingDCR',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }
              ,
              {
                title: 'Other DCR',
                data: 'otherDCR',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }
              ,
              {
                title: 'Pending DCR',
                data: 'totalPendingDCR',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Incomplete DCR', //DCR is working but no doctor is selected.
                data: 'incompleteDCR',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'TP Submitted',
                data: 'submitted',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: "Total Current Active " + this.currentUser.company.lables.doctorLabel,
                data: 'totalDoctors',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },

              {

                  title: "Total Current MEF Active " + this.currentUser.company.lables.doctorLabel,
                  data: 'totalDoctorsMEF',
                  render: function (data) {
                    if (data === '') {
                      return '---'
                    } else {
                      return data
                    }
                  },
                  defaultContent: '---'



              },
              {
                title: "Total Current NON MEF Active " + this.currentUser.company.lables.doctorLabel,
                data: 'totalDoctorsNONMEF',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Active ' + this.currentUser.company.lables.doctorLabel + ' On Selected Time Period',
                data: 'totalActiveDoctorOnSelectedTimePeriod',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.doctorLabel + ' Calls',
                data: 'doctorCall',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.doctorLabel + ' Call Avg. On Selected Time Period',
                data: 'drCallAvg',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Listed ' + this.currentUser.company.lables.doctorLabel + " Calls",
                data: 'listedDoctorCall',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Listed ' + this.currentUser.company.lables.doctorLabel + " Call Avg.",
                data: 'listedDoctorCallAvg',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Mef'+this.currentUser.company.lables.doctorLabel + ' Calls',
                data: 'MefDoctorCall',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }, {
                title: 'Mef'+this.currentUser.company.lables.doctorLabel + ' Call Avg.',
                data: 'mefDrCallAvg',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Non-Mef'+this.currentUser.company.lables.doctorLabel + ' Calls',
                data: 'nonMefDoctorCall',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }, {
                title: 'Non-Mef'+this.currentUser.company.lables.doctorLabel + ' Call avg.',
                data: 'nonMefDrCallAvg',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Listed ' + this.currentUser.company.lables.doctorLabel + " Coverage On Selected Time Period",
                data: 'listedActiveDoctorCoverage',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }, {
                title: 'Untouched ' + this.currentUser.company.lables.doctorLabel + ' On Selected Time Period',
                data: 'dataa',
                render: function (data, type, row) {
                  if (data === '') {
                    return '---'
                  } else {
                    var dataa = 0;
                    if (row.totalActiveDoctorOnSelectedTimePeriod > 0) {
                      dataa = row.totalActiveDoctorOnSelectedTimePeriod - row.listedDoctorCall;
                    }
                    return dataa
                  }
                },
                defaultContent: '---'
              },
              // {
              //   title: 'Active Listed' + this.currentUser.company.lables.doctorLabel + "Coverage On Time Period",
              //   data: 'listedActiveDoctorCoverage',
              //   render: function (data) {
              //     if (data === '') {
              //       return '---'
              //     } else {
              //       return data
              //     }
              //   },
              //   defaultContent: '---'
              // },
              // {
              //   title: "Listed" + this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel + 'Call',
              //   data: 'listedVendorCall',
              //   render: function (data) {
              //     if (data === '') {
              //       return '---'
              //     } else {
              //       return data
              //     }
              //   },
              //   defaultContent: '---'
              // },

              {
                title: "Total Current Active " + this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel,
                data: 'totalChemists',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }, {
                title: 'Active ' + this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel + ' On Seletected Time Period',
                data: 'totalActiveVendorOnSelectedTimePeriod',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              }, {
                title: this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel + ' Call',
                data: 'vendorCall',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel + ' Call Avg. On Selected Time Period',
                data: 'venCallAvg',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: 'Listed ' + this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel + " Calls",
                data: 'listedVendorCall',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: "Listed " + this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel + ' Call Average',
                data: 'listedVendorCallAvg',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              {
                title: "Listed " + this.currentUser.company.lables.vendorLabel + "/" + this.currentUser.company.lables.stockistLabel + ' Coverage',
                data: 'listedVendorCoverage',
                render: function (data) {
                  if (data === '') {
                    return '---'
                  } else {
                    return data
                  }
                },
                defaultContent: '---'
              },
              // {
              //   title: 'Untouched' + this.currentUser.company.lables.doctorLabel,
              //   data: 'dataa',
              //   render: function (data, type, row) {
              //     if (data === '') {
              //       return '---'
              //     } else {
              //       var dataa = 0;
              //       if (row.totalDoctors > 0) {
              //         dataa = row.totalDoctors - row.listedDoctorCall;
              //       }
              //       return dataa
              //     }
              //   },
              //   defaultContent: '---'
              // }
              // ,

            ],
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'csv',
                exportOptions: {
                  columns: ':visible'
                }
              },
              {
                extend: 'copy',
                exportOptions: {
                  columns: ':visible'
                }
              },

            ]
          };
		  this._changeDetectorRef.detectChanges();

          if (this.currentUser.company.isDivisionExist == false) {
            this.dtOptions.columns[0].visible = false;
          }
          if (this.currentUser.company.validation.isMefEnabled == false) {
            this.dtOptions.columns[10].visible = false;
            this.dtOptions.columns[11].visible = false;
            this.dtOptions.columns[18].visible = false;
            this.dtOptions.columns[17].visible = false;
            this.dtOptions.columns[19].visible = false;
            this.dtOptions.columns[20].visible = false;
          }

          this.dataTable = $(this.table1.nativeElement);
          this.dataTable.DataTable(this.dtOptions);


        } else {
          this.isShowReport = false;
          this.showProcessing = false;
          this._toastrService.info('Selected Manager data is not availabe for selected month & year.', 'No Data available.')
        }
         this._changeDetectorRef.detectChanges();
      },
      err => {
        this.isShowReport = false;
        this.showProcessing = false;
        this._toastrService.info('Selected Manager data is not availabe for selected month & year.', 'No Data Found.')

        console.log(err)
      }
    )
  }
  intializeForm() {
    this.mgrTeamForm.setValue({
      type: '',
      designation: '',
      status: '',
      userId: '',
      fromDate: '',
      toDate: ''
    })
  }

}
