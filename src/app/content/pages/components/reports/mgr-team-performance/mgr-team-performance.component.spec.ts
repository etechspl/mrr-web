import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MgrTeamPerformanceComponent } from './mgr-team-performance.component';

describe('MgrTeamPerformanceComponent', () => {
  let component: MgrTeamPerformanceComponent;
  let fixture: ComponentFixture<MgrTeamPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MgrTeamPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MgrTeamPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
