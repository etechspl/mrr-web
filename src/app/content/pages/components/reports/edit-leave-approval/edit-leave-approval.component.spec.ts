import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLeaveApprovalComponent } from './edit-leave-approval.component';

describe('EditLeaveApprovalComponent', () => {
  let component: EditLeaveApprovalComponent;
  let fixture: ComponentFixture<EditLeaveApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLeaveApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLeaveApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
