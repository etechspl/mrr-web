import { Component, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LeaveApprovalService } from '../../../../../core/services/leave-approval.service';

@Component({
  selector: 'm-edit-leave-approval',
  templateUrl: './edit-leave-approval.component.html',
  styleUrls: ['./edit-leave-approval.component.scss']
})
export class EditLeaveApprovalComponent implements OnInit {
  previousData;
  currentUser = JSON.parse(sessionStorage.currentUser);
  constructor(
    private _toastr: ToastrService,
    private fb: FormBuilder,
    private _LeaveApprovalService: LeaveApprovalService,
    public dialogRef: MatDialogRef<EditLeaveApprovalComponent>,
    @Inject(MAT_DIALOG_DATA) data,
  ) { 
    this.previousData = data;
  }

  ngOnInit() {
    console.log("this.previousData>>>",this.previousData);
    this.userleaveList(this.previousData.obj.userDetails.userId);
  }
  userleaveList(obj){
  let params = {
    type: 'userLeaveList',
    companyId: this.currentUser.companyId,
    userId: obj
  }

  this._LeaveApprovalService.getLeavesForApproval(params).subscribe( res =>{
    console.log("<><><><><> dialogList <><><><><>", res);     

  })

  }
}
