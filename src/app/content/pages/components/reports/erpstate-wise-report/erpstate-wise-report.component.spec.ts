import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ERPStateWiseReportComponent } from './erpstate-wise-report.component';

describe('ERPStateWiseReportComponent', () => {
  let component: ERPStateWiseReportComponent;
  let fixture: ComponentFixture<ERPStateWiseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ERPStateWiseReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ERPStateWiseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
