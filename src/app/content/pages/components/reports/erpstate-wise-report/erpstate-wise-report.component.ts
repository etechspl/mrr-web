import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ERPServiceService } from '../../../../../core/services/erpservice.service';
import { retryWhen, delay, take } from 'rxjs/operators'
import { ToastrService } from 'ngx-toastr';
import { ExportAsService } from 'ngx-export-as';
import { DataTableItemModel } from '../../../../../core/models/datatable-item.model';

@Component({
  selector: 'm-erpstate-wise-report',
  templateUrl: './erpstate-wise-report.component.html',
  styleUrls: ['./erpstate-wise-report.component.scss']
})
export class ERPStateWiseReportComponent implements OnInit {
   data=[];
   HqData=[];
   header;
   showHQTable=false;
   ERPParams;
  constructor(
    private activateRoute: ActivatedRoute,
    private ERPServiceService:ERPServiceService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _toastrService: ToastrService,
    private exportAsService: ExportAsService,

  ) { }

  ngOnInit() {
    this.activateRoute.queryParams.subscribe(params => {
      this.ERPParams=params;
      if(params.collection=="primary"){
        this.header="Primary"
      }else if(params.collection=="collection"){
        this.header="Collection"

      }else if(params.collection=="outstanding"){
        this.header="Outstanding"
      }
        this.ERPServiceService.getStateWiseDetails(params)
        .pipe(
          retryWhen(errors => errors.pipe(delay(1000), take(10)))
        )
        .subscribe(res => {
          this.data.push(res);
          this._changeDetectorRef.detectChanges();

        }, err => {
          console.log(err)
        });
    })
  }
  getHQWiseSale(data){
    let obj={}
    this.HqData=[]
    this.showHQTable=true;
      this.ERPServiceService.getHQWiseDetails(this.ERPParams)
        .pipe(
          retryWhen(errors => errors.pipe(delay(1000), take(10)))
        )
        .subscribe(res => {
          if(this.ERPParams.collection=="primary"){
           obj["headquarter"]=res.Headquater
           obj["amount"]=res.primSales
          }else if(this.ERPParams.collection=="collection"){
            obj["headquarter"]=res.Headquater
            obj["amount"]=res.amount
          }else if(this.ERPParams.collection=="outstanding"){
            obj["headquarter"]=res.headquater
            obj["amount"]=res.amount
          }
          this.HqData.push(obj);
          this._changeDetectorRef.detectChanges();

        }, err => {
          console.log(err)
        });
  }

}
