import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesReportHqWiseComponent } from './sales-report-hq-wise.component';

describe('SalesReportHqWiseComponent', () => {
  let component: SalesReportHqWiseComponent;
  let fixture: ComponentFixture<SalesReportHqWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesReportHqWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesReportHqWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
