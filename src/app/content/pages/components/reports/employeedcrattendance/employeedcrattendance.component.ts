  import { Component, OnInit, ChangeDetectorRef, ViewChild, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TypeComponent } from '../../filters/type/type.component';
import { ActivityService } from '../../../../../core/services/Activity/activity.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { URLService } from '../../../../../core/services/URL/url.service';
import { MatSlideToggleChange, MatSelect, MatOption } from '@angular/material';
//import { saveAs } from 'file-saver';
declare var $;

@Component({
  selector: 'm-employeedcrattendance',
  templateUrl: './employeedcrattendance.component.html',
  styleUrls: ['./employeedcrattendance.component.scss']
})

export class EmployeedcrattendanceComponent implements OnInit, OnDestroy {
  @ViewChild('select') select: MatSelect;

  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  dataSource: any;
  countdataSource:any;
  columns = [];
  allSelected=false;

  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showReport: boolean = false;
  public showEmployee: boolean = false;
  showProcessing: boolean = false;
  showAdminAndMGRLevelFilter = false;
  workingType = [];
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  constructor(private changeDetectorRef: ChangeDetectorRef,
    private _dcrReportService: DcrReportsService,
    private _toastr: ToastrService,
    private _activityService: ActivityService,
    private changedetectorref: ChangeDetectorRef,
    private exportAsService: ExportAsService,
    private _urlService: URLService
  ) { }


  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;


  attendanceForm = new FormGroup({
    companyId: new FormControl(this.currentUser.companyId),
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    isDivisionExist: new FormControl(this.isDivisionExist),
    stateInfo: new FormControl(''),
    districtInfo: new FormControl(null),
    employeeId: new FormControl(null),
    month: new FormControl(null, Validators.required),
    year: new FormControl(null, Validators.required),
    designation: new FormControl(null),
    selectedWorkingTypes: new FormControl([], Validators.required),
    totalWorkingTypes: new FormControl([])

  });
  isToggled = true; 
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  ngOnInit() {
    //this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.attendanceForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }


    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showAdminAndMGRLevelFilter = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
    this._activityService.getActivities(this.currentUser.companyId).subscribe(res => {
      this.workingType = res[0].activitiesForWeb;
      
      this.attendanceForm.patchValue({ totalWorkingTypes: res[0].activitiesForWeb });
      this.changedetectorref.detectChanges();
    }, err => {
      console.log(err)
    });

  }

  getReportType(val) {
    this.showReport = false;
    this.showProcessing = false;
    if (val === "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else {
      this.showState = false;
      this.showDistrict = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    this.showReport = false;
    this.showProcessing = false;
    if (val == "State") {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = true;
      this.showDistrict = false;
      this.attendanceForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.attendanceForm.removeControl("designation");
      this.attendanceForm.removeControl("employeeId");

    } else if (val == "Headquarter") {
      this.showState = true;
      this.showDistrict = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.attendanceForm.setControl("stateInfo", new FormControl(null, Validators.required))
      this.attendanceForm.setControl("districtInfo", new FormControl(null, Validators.required))
      this.attendanceForm.removeControl("employeeId");
      this.attendanceForm.removeControl("designation");

    } else if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.attendanceForm.removeControl("stateInfo");
      this.attendanceForm.removeControl("districtInfo");
      this.attendanceForm.setControl("designation", new FormControl(null, Validators.required));
      this.attendanceForm.setControl("employeeId", new FormControl(null, Validators.required));

    } else if (val == "Self") {
      this.showEmployee = false;

      this.attendanceForm.removeControl("stateInfo");
      this.attendanceForm.removeControl("districtInfo");
      this.attendanceForm.removeControl("designation");
      this.attendanceForm.setControl("employeeId", new FormControl(null, Validators.required));

    }
    // show division on change of the type
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }

    this.attendanceForm.patchValue({ type: val });

  }


  getDivisionValue($event) {
    this.attendanceForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.attendanceForm.value.type === "State" || this.attendanceForm.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.isDivisionExist,
          division: this.attendanceForm.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.attendanceForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.attendanceForm.value.divisionId != null && this.attendanceForm.value.designation != null) {
          if (this.attendanceForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.attendanceForm.value.divisionId,
              status: [true],
              designationObject: this.attendanceForm.value.designation
            })
          }
        }
      }
    }
  }

  getStateValue(val) {
    this.showReport = false;
    this.showProcessing = false;

    this.attendanceForm.patchValue({ stateInfo: val });
    if (this.isDivisionExist === true) {
      if (this.attendanceForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.companyId,
          "stateId": this.attendanceForm.value.stateInfo,
          isDivisionExist: this.isDivisionExist,
          division: this.attendanceForm.value.divisionId
        })
        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.attendanceForm.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }


  getDistrictValue(val) {
    this.showReport = false;
    this.showProcessing = false;
    this.attendanceForm.patchValue({ districtInfo: val });
  }


  getDesignation(val) {
    this.showReport = false;
    this.showProcessing = false;
    this.attendanceForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.attendanceForm.value.divisionId,
        status: [true],
        designationObject: this.attendanceForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }

  getEmployeeValue(val) {
    this.showReport = false;
    this.showProcessing = false;
    this.attendanceForm.patchValue({ employeeId: val });
  }
  getMonth(val) {
    this.showReport = false;
    this.showProcessing = false;
    this.attendanceForm.patchValue({ month: val });
  }
  getYear(val) {
    this.showReport = false;
    this.showProcessing = false;
    this.attendanceForm.patchValue({ year: val });
  }
toastrInfo(){
  this._toastr.info(
    `Selected type will be considered as working`,
    "",
    {
      timeOut: 0,
      extendedTimeOut: 0,
      closeButton: true,
      positionClass: "toast-bottom-right",
    }
  );
}
  chexkBoxArray = [];
  updateChkbxArray(value, isChecked) {
    this.showReport = false;
    this.showProcessing = false;
    if (isChecked) {
      this.chexkBoxArray.push(value)
      
    } else {
      if (this.chexkBoxArray.includes(value)) {
        let idx = this.chexkBoxArray.indexOf(value);
        this.chexkBoxArray.splice(idx, 1);
      }
    }
    this.attendanceForm.patchValue({ selectedWorkingTypes: this.chexkBoxArray })
  }

  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    
    if (this.allSelected) {
      this.select.options.forEach( (item : MatOption) => item.select());
    } else {
      this.select.options.forEach( (item : MatOption) => {item.deselect()});
    }

  }
  viewRecords() {
    this._toastr.clear();
    this.isToggled = false;
    this.showReport = false;
    this.showProcessing = true;
    // console.log(this.attendanceForm.value);
    //Null is for MR Level, actually we do not select any type filter on MR level.
    //So Type is null for MR level.
    //Self is for Manager itself.

    //default i m setting the isDivisionExist value 


    if (this.attendanceForm.value.type == null || this.attendanceForm.value.type == 'Self') {
      this.attendanceForm.patchValue({ employeeId: [this.currentUser.id] });
    }
    this._dcrReportService.getEmployeesdcrAttendance(this.attendanceForm.value).subscribe(res => {

      this.columns = [];

     

//       let myarray=[{
//         totalDoc:any,

// totalChem:any,
// totalStock:any
//       }



      
     // console.log("Attandance result",res);
      if (res.length > 0) {
        this.dataSource = JSON.parse(JSON.stringify(res));
      
        let date = 1;

        
        
        console.log("data array=>",this.dataSource);


        
         for (let i = 0; i < this.dataSource[0].dateArray.length; i++) {
    

          this.columns.push(date);
        date++;



        } 
  

        


        this.showProcessing = false;
        this.showReport = true;

        this.changeDetectorRef.detectChanges();
      } else {
        this._toastr.info("Data not available")
        this.showProcessing = false;
        this.changeDetectorRef.detectChanges();
        this.showReport = false;
      }

    }, err => {
      this.showReport = false;
      this.showProcessing = false;
      this.changeDetectorRef.detectChanges();

      console.log(err);
    });
    this.changeDetectorRef.detectChanges();

  }

  exporttoExcel() {                         
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Employee Attendance Report').subscribe(() => {
      // save started
    });
  }


  onPreviewClick(){
   
		let printContents, popupWin;
	  
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
    const companyName = this.currentUser.companyName;
    
    let monthname;

    if(this.attendanceForm.value.month==10){
      monthname="October"
    }

    if(this.attendanceForm.value.month==11){
      monthname="Novermber"
    }

    if(this.attendanceForm.value.month==12){
      monthname="December"
    }

		printContents = document.getElementById('tabledivId1').innerHTML;
		const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
	
		popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		popupWin.document.open();
		popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;
	}
	
	
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	 
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4 landscape;
		max-height:100%;
		max-width:100%}
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
    <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
   
    <h2 style="margin-top: 0px; margin-bottom: 5px">&nbsp;${companyName}</h2>
    
    
    </div>
    
    <h2 > &nbsp; Month : ${monthname}</h2>
    
    
	  
	
	  <div style="text-align: right;">
	  <h2 style="margin-top: 0px; margin-bottom: 5px">Attendance Report</h2>
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  ${printContents}</body>
	  </html>
	  `
		);
	   // popupWin.document.close();
	 }

  ngOnDestroy(){
    this._toastr.clear();
  }

}
