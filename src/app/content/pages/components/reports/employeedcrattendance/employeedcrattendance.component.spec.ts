import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeedcrattendanceComponent } from './employeedcrattendance.component';

describe('EmployeedcrattendanceComponent', () => {
  let component: EmployeedcrattendanceComponent;
  let fixture: ComponentFixture<EmployeedcrattendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeedcrattendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeedcrattendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
