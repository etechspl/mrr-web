import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetVsAchievementHqWiseComponent } from './target-vs-achievement-hq-wise.component';

describe('TargetVsAchievementHqWiseComponent', () => {
  let component: TargetVsAchievementHqWiseComponent;
  let fixture: ComponentFixture<TargetVsAchievementHqWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetVsAchievementHqWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetVsAchievementHqWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
