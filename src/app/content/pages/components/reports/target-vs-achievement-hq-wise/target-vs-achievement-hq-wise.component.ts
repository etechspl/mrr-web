import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption, MatPaginator, MatSelect, MatSlideToggle, MatTableDataSource } from '@angular/material';
import { TypeComponent } from '../../filters/type/type.component';
import { ErpservicehqwiseService } from "../../../../../core/services/erpservicehqwise.service";
import * as _moment from "moment";
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import * as XLSX from "xlsx";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'm-target-vs-achievement-hq-wise',
  templateUrl: './target-vs-achievement-hq-wise.component.html',
  styleUrls: ['./target-vs-achievement-hq-wise.component.scss']
})
export class TargetVsAchievementHqWiseComponent implements OnInit {
  // ----------------------------------GOURAV------------------------------------------
  // ----------------------------------------------------------------------------------

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId2', // the id of html/table element
  }
  public showProcessing: boolean = false;
  isToggle: Boolean = true;
  allSelected: Boolean = false;
  allSelectedHq: Boolean = false;
  showReport: Boolean = false;
  showState: Boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  stateData: any;
  select1: boolean = true;
  showHq: Boolean = false;
  ddValue = [];
  ddValue2 = [];
  desigData: any;
  managerData: any;
  dataSource: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  HqData: any;
  hqShow: boolean = false;
  totalTarget = 0;
  totalPrimary = 0;
  totalClosing = 0;
  totalSecondary = 0;
  viewRadioType="unit";
  totalAchivement = 0;
  isDiv : boolean = false;
  showHeadquarterName : boolean = false;
  showStateName : boolean = false;
  showEmpName : boolean = false;
  showFinalReport : boolean = true;
  hqResult: boolean = false;


  displayedColumns = ['headquarterName','productName', 'targetAmount', 'primaryAmount','achievementAmount', 'closingAmount', 'secondaryAmount'];

  displayedColumns1 = ['position','hq', 'tQuantity', 'tValue','pQuantity','pValue','sQuantity','sValue','acQuantity','acValue','cQuantity','cValue'];

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator1') paginator1: MatPaginator;

  @ViewChild('selectHQ') selectHQ: MatSelect;
  @ViewChild('select') select: MatSelect;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  totalPrimaryQty: any;
  totalTargetQty: any;
  closingQty: any;
  secondaryQty: any;
  AchivQty: any;

  constructor(
    private ERPServiceService: ErpservicehqwiseService,
    private changeDetectorRefs: ChangeDetectorRef,
    private exportAsService: ExportAsService,
    private _toastrService: ToastrService

  ) { }

  erpForm = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null),
    stateInfo: new FormControl(null),
    toDate: new FormControl(null),
    fromDate: new FormControl(null),
    designation: new FormControl(null),
    employeeId: new FormControl(null),
    HqInfo: new FormControl(null)
  });

  ngOnInit() {
    this.isToggle = true;
    if (
      this.currentUser.userInfo[0].designationLevel == 0 ||
      this.currentUser.userInfo[0].designationLevel > 1
    ) {
      this.getReportType('Geographical')
    }

    // this.dataSource.paginator = this.paginator;
     this.dataSource2.paginator = this.paginator1;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource2.paginator = this.paginator1;
  }

  toggleFilter(event: MatSlideToggle) {
    this.isToggle = event.checked;
  }

  viewRecords() {
    this.totalTarget = 0;
    this.totalPrimary = 0;
    this.totalClosing = 0;
    this.totalSecondary = 0;
    this.totalAchivement = 0;
    this.showReport = false;
    this.isToggle = false;
    this.showProcessing = true;

    if (this.erpForm.value.employeeId == null) {
      this.erpForm.value.employeeId = [this.currentUser.id];
    }
    let params = {};
    let result;
    let stateCode = [];
    let j = 0
    let statecode = this.erpForm.value.stateInfo;
    params = {
      companyId: this.currentUser.companyId,
      APIEndPoint: this.currentUser.company.APIEndPoint,
    }
    if (this.erpForm.value.type == 'Headquarter') {
      this.showFinalReport = false;
      this.showHeadquarterName = true;
      this.showStateName=false;
      this.showEmpName = false;
      this.erpForm.value.stateInfo.forEach(element => {
        if (element != 0) {
          stateCode[j] = element;
          j++;
        }
      });
      params['stateCode'] = stateCode;
      params['userId'] = this.currentUser.employeeCode,
      params['type'] = this.erpForm.value.type;
      params['fromDate'] = _moment(this.erpForm.value.fromDate).format('YYYY-MM-DD');
      params['toDate'] = _moment(this.erpForm.value.toDate).format('YYYY-MM-DD');
      params['fromPrimDate'] = _moment(this.erpForm.value.fromDate).format('DD-MM-YYYY');
      params['toPrimDate'] = _moment(this.erpForm.value.toDate).format('DD-MM-YYYY');
      params['viewType']=this.viewRadioType;
      let statename = [];
      let hqCode = [];
      this.stateData.forEach(element => {
        if (element.code == statecode) {
          statename.push(element.name)
        }
      });
      params['stateName'] = statename;
      let headquarter = [];
    
      this.erpForm.value.HqInfo.forEach(element => {
        this.HqData.forEach(code => {
          if (element == code.code) {
            headquarter.push(code.name)
          }
        });
      });
      params['headquarterName'] = headquarter;
      params['hqCode'] = this.erpForm.value.HqInfo;
      let result;
      console.log('params',params);
      this.ERPServiceService.getTargetVsAchievementVsPrimaryVsClosingHqWise(params).subscribe((res) => {
        console.log('res',res);
        if (res == null || res == undefined || res[0].details.length==0) {
          this.showFinalReport = false;
          this.showProcessing = false;
          this.showReport = false;
          this.isToggle = true;
          this.hqResult = false;
          this._toastrService.error("Selected Data Not Found");
        } else {
          this.hqResult = true;
          this.showProcessing = false;
          this.showReport=true;
          result = res[0].details;
          this.totalPrimary = res[0].totalAmtPrimary;
          this.totalPrimaryQty = res[0].totalPrimaryQty;

          this.totalTargetQty = res[0].totalAmtTargetQty;
          this.totalTarget = res[0].totalAmtTarget;

          this.totalClosing = res[0].totalClosingAmt;
          this.closingQty = res[0].totalClosingQty;

          this.totalSecondary = res[0].totalSecondaryAmt;
          this.secondaryQty = res[0].totalSecondaryQty

          this.totalAchivement = res[0].totalPerAchAmt;
          this.AchivQty= res[0].totalPerAchAmtQty;
        }

        this.dataSource2 = new MatTableDataSource(result);
        this.changeDetectorRefs.detectChanges();
        this.dataSource2.paginator = this.paginator1;
        
      })
     // this.changeDetectorRefs.detectChanges();
    }
    if (this.erpForm.value.type == 'Manager') {
      this.showFinalReport = true;
      this.hqResult = false;
      this.showHeadquarterName = false;
      this.showStateName=false;
      this.showEmpName = true;
      params['targetType'] = 'productwise';
      params['type'] = 'Employee Wise';
      params['fromDate'] = _moment(this.erpForm.value.fromDate).format('YYYY-MM-DD');
      params['toDate'] = _moment(this.erpForm.value.toDate).format('YYYY-MM-DD');
      params['fromPrimDate'] = _moment(this.erpForm.value.fromDate).format('DD-MM-YYYY');
      params['toPrimDate'] = _moment(this.erpForm.value.toDate).format('DD-MM-YYYY');
      params['isDivisionExist'] = this.isDiv;
      params['userId'] = [this.erpForm.value.employeeId];
      params['viewType']=this.viewRadioType;
      let empname = [];
      let i = 0;
      this.managerData.forEach(element => {
        if (element.code == this.erpForm.value.employeeId) {
          empname = element.name;
        }
      });
      params['Name'] = empname;
      this.ERPServiceService.getTargetVsAchievementVsPrimaryVsClosingHqWise(params).subscribe((res) => {
        if (res == null || res == undefined || res[0].details.length ==0) {
          this.showFinalReport = false;
          this.showProcessing = false;
          this.isToggle = true;
          this._toastrService.error("Selected Data Not Found");
        } else {
          this.showProcessing=false;
          this.showReport=true;
          result = res[0].details;
          this.totalPrimary = res[0].totalAmtPrimary;
          this.totalTarget = res[0].totalAmtTarget;
          this.totalClosing = res[0].totalClosingAmt;
          this.totalSecondary = res[0].totalSecondaryAmt;
          this.totalAchivement = res[0].totalPerAchAmt;
        }
        this.dataSource = new MatTableDataSource(result);
        this.changeDetectorRefs.detectChanges();
        this.dataSource.paginator = this.paginator;
       
      })
    }
  }

  getReportType(val) {
    if (val == 'Geographical') {
      this.select1 = true;
      this.showState = true;
      this.ddValue = ['Headquarter']
    } else {
      this.select1 = false;
      this.ddValue2 = ['Manager']
      this.showState = false;
    }
  }

  getTypeValue(val) {
    // this.showReport = false;
    // if (val == 'State') {
    //   this.getERPStateData();
    //   this.select1 = true;
    //   this.hqShow = false;
    //   this.showState = true;
    // } else 
    if (val == 'Headquarter') {
      this.hqShow = true;
      this.getERPStateData();
      this.showState = true;
    } else if (val == 'Manager') {
      this.hqShow = false;
      this.getERPStateData();
      this.getDesignation(val);
    }
    this.erpForm.patchValue({ type: val });
  }

  getERPStateData() {
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, }
    this.ERPServiceService.getERPStateData(params).subscribe((stateList) => {
      this.stateData = stateList;
    });
  }

  getERPHeadquartersData(val) {
    let stateCode = (val.filter(x => x)).toString();
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint, userId: this.currentUser.employeeCode, stateId: stateCode }
    this.ERPServiceService.getERPHeadquarterData(params).subscribe((districtDataResult) => {
      this.HqData = districtDataResult;
    })
  }

  getHqData(val) {
    this.erpForm.patchValue({ HqInfo: val });
  }

  getStateValue(val) {
    this.erpForm.patchValue({ stateInfo: val })
    this.getERPHeadquartersData(this.erpForm.value.stateInfo);
  }

  /* getStateHqValue(val){
    this.erpForm.patchValue({stateHqInfo : val})
  } */

  getDesignation(val) {
    this.erpForm.patchValue({ designation: val });
    this.getDesignationData(val);
  }

  getDesignationData(val) {
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint }
    this.ERPServiceService.getDesignationsData(params).subscribe((designationResult) => {
      this.desigData = designationResult;
    });
    this.erpForm.patchValue({ designation: val });
    this.getManagerData(val);
  }

  getManagerData(val) {
    let params = { APIEndPoint: this.currentUser.company.APIEndPoint, Designationid: val }
    this.ERPServiceService.getManagerData(params).subscribe((managerData) => {
      this.managerData = managerData;
      if(managerData != null)
      {
        this.isDiv = true;
      }
    })
  }

  getDistrictValue(val) {
    this.erpForm.patchValue({ HqInfo: val });
  }

  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  toggleAllSelectionForHQ() {
    this.allSelectedHq = !this.allSelectedHq;  // to control select-unselect
    if (this.allSelectedHq) {
      this.selectHQ.options.forEach((item: MatOption) => item.select());
    } else {
      this.selectHQ.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  getFromDate(val) {
    this.erpForm.patchValue({ fromDate: val });
  }

  getToDate(val) {
    this.erpForm.patchValue({ toDate: val });
  }
  toggleFunction(event){
   this.viewRadioType=event;    
  }

  exporttoExcel() {
    // this.exportAsService.save(this.exportAsConfig, 'Target report').subscribe(() => {
    //   //Downloading start
    // });
    let dataToExport=[];
    if(this.showHeadquarterName == true){
    dataToExport = this.dataSource.data.map((x) => ({
      "Headquarter Name": x.headquarterName,
      "Product Name": x.productName,
      "Target":parseInt(x.targetAmount),
      Primary : parseInt(x.primaryAmount),
      "Achievement Percent" : parseInt(x.achievementAmount),
      Closing : parseInt(x.closingAmount),
      "Secondary" : parseInt(x.secondaryAmount),  
    }));
    dataToExport.push(
      {
        "Headquarter Name": '----',
        "Product Name": '----',
        "Target":'----',
         Primary : '----',
        "Achievement Percent" : '----',
         Closing : '----',
        "Secondary" : '----',
      },
      {
      "Headquarter Name": 'Total',
      "Product Name": '',
      "Target":this.totalTarget,
       Primary : this.totalPrimary,
      "Achievement Percent" : this.totalAchivement,
       Closing : this.totalClosing,
      "Secondary" : this.totalSecondary,
    })
  }else if(this.showStateName == true){
       dataToExport = this.dataSource.data.map((x) => ({
        "State Name": x.stateName,
        "Product Name": x.productName,
        "Target":parseInt(x.targetAmount),
        Primary : parseInt(x.primaryAmount),
        "Achievement Percent" : parseInt(x.achievementAmount),
        Closing : parseInt(x.closingAmount),
        "Secondary" : parseInt(x.secondaryAmount)
      }))
      dataToExport.push({
        "State Name": '----',
        "Product Name": '----',
        "Target":'----',
         Primary : '----',
        "Achievement Percent" : '----',
         Closing : '----',
        "Secondary" : '----',
      },{
        "State Name": 'Total',
        "Product Name": '',
        "Target":this.totalTarget,
         Primary : this.totalPrimary,
        "Achievement Percent" : this.totalAchivement,
         Closing : this.totalClosing,
        "Secondary" : this.totalSecondary,
      })
    }
    else if (this.showEmpName == true){
        dataToExport = this.dataSource.data.map((x) => ({
          "Employee Name": x.userName,
          "Product Name": x.productName,
          "Target":parseInt(x.targetAmount),
           Primary : parseInt(x.primaryAmount),
          "Achievement Percent" : parseInt(x.achievementAmount),
           Closing : parseInt(x.closingAmount),
          "Secondary" : parseInt(x.secondaryAmount),
        }));
        dataToExport.push({
          "Employee Name": '----',
          "Product Name": '----',
          "Target":'----',
           Primary : '----',
          "Achievement Percent" : '----',
           Closing : '----',
          "Secondary" : '----',
        },{
          "Employee Name": 'Total',
          "Product Name": '',
          "Target":this.totalTarget,
           Primary : this.totalPrimary,
          "Achievement Percent" : this.totalAchivement,
           Closing : this.totalClosing,
          "Secondary" : this.totalSecondary,
        })
      }
      
		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "Target vs Achievement ERP Report.xlsx");
	}

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue
  }

  applyFilter1(filterValue: string){
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSource2.filter = filterValue;
  }

  exporttoExcel1(){
  
    let dataToExport=[];
    dataToExport=this.dataSource2.data.map((x)=>({
      "Headquarter Name" : x.districtName,
      "Product Name" : x.productName,
      "Target Qty" : x.targetQty,
      "Targer Val"  : x.targetAmount,
      "Primery Qty" : x.primaryQty,
      "Primary Val" : x.primaryAmount,
      "Secondry Qty":x.secondaryQty,
      "Secondary Val":x.secondaryAmount,
      "Primary Achivement Qty" : x.achievementQty,
      "Primary Achivement Val" : x.achievementAmount,
      "Closing Qty" : x.totalClosingQty,
      "Closing Val" : x.closingAmount
    }));
    dataToExport.push(
      {
      "Headquarter Name" :'------',
      "Product Name" : '------',
      "Target Qty" : '------',
      "Targer Val"  : '------',
      "Primery Qty" : '------',
      "Primary Val" : '------',
      "Secondry Qty":'------',
      "Secondary Val":'------',
      "Primary Achivement Qty" :'------',
      "Primary Achivement Val" : '------',
      "Closing Qty" : '------',
      "Closing Val" : '------'
      },
      {
        "Headquarter Name" :'Total',
        "Product Name" : '------',
        "Target Qty" : this.totalTargetQty,
        "Targer Val"  : this.totalTarget,
        "Primery Qty" : this.totalPrimaryQty,
        "Primary Val" : this.totalPrimary,
        "Secondry Qty":this.secondaryQty,
        "Secondary Val":this.totalSecondary,
        "Primary Achivement Qty" :this.AchivQty,
        "Primary Achivement Val" : this.totalAchivement,
        "Closing Qty" : this.closingQty,
        "Closing Val" : this.totalClosing
    })

    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "Target vs Achievement ERP Report.xlsx");
  }
  // ----------------------------------GOURAV------------------------------------------
  // ----------------------------------------------------------------------------------
}
