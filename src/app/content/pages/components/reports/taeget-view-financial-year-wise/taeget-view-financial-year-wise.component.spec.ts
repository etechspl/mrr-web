import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaegetViewFinancialYearWiseComponent } from './taeget-view-financial-year-wise.component';

describe('TaegetViewFinancialYearWiseComponent', () => {
  let component: TaegetViewFinancialYearWiseComponent;
  let fixture: ComponentFixture<TaegetViewFinancialYearWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaegetViewFinancialYearWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaegetViewFinancialYearWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
