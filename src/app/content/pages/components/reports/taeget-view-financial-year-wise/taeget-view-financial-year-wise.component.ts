import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { locateHostElement } from '@angular/core/src/render3/instructions';
import { FormControl, FormGroup } from '@angular/forms';
import { MatOption, MatPaginator, MatSelect, MatSlideToggle, MatTableDataSource } from '@angular/material';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { ToastrService } from 'ngx-toastr';
import { DesignationService } from '../../../../../core/services/Designation/designation.service';
import { DistrictService } from '../../../../../core/services/district.service';
import { DivisionService } from '../../../../../core/services/Division/division.service';
import { ERPServiceService } from '../../../../../core/services/erpservice.service';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { StateService } from '../../../../../core/services/state.service';
import { TargetService } from '../../../../../core/services/Target/target.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StatusComponent } from '../../filters/status/status.component';

@Component({
  selector: 'm-taeget-view-financial-year-wise',
  templateUrl: './taeget-view-financial-year-wise.component.html',
  styleUrls: ['./taeget-view-financial-year-wise.component.scss']
})
export class TaegetViewFinancialYearWiseComponent implements OnInit {

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  currentUser = JSON.parse(sessionStorage.currentUser);
  isToggled: boolean = true;
  viewRadioType = 'unit';
  statusList = ['Active', 'Inactive']
  designationData: any = [];
  EmployeeData: any = [];
  districtData = [];
  districtIds: any;
  districtIdsLength: any;
  employeeData = [];
  showStateName = false;
  showEmpName = false;
  showHQName = false;
  yearList;
  type = ['State', 'Headquarter', 'Employee Wise'];
  catagory = ['amountwise', 'productwise'];
  headquarter: boolean = false;
  state: boolean = false;
  employee: boolean = false;
  allSelected: Boolean = false;
  allSelectedHq: Boolean = false;
  stateData: any;
  stateIds: any;
  stateIdsLength: any;
  divisionData = [];
  userIds: any = [];

  visible: boolean = false;
  showData: boolean = false;
  dataSource
  // dataSource : MatTableDataSource<any>;
  displayedColumns = ['Year', 'month', 'targetQty', 'creditValue']


  @ViewChild('callDivisionComponent') callDivisionComponent: DivisionComponent;
  @ViewChild('callStatusComponent') callStatusComponent: StatusComponent;
  @ViewChild('callEmployeeComponent') callEmployeeComponent: EmployeeComponent;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('select') select: MatSelect;
  @ViewChild('selectHQ') selectHQ: MatSelect;
  @Output() valueChange = new EventEmitter();
  changeDetectorRef: any;


  constructor(
    private _stateService: StateService,
    private _monthYearservice: MonthYearService,
    private _divisionService: DivisionService,
    private _districtService: DistrictService,
    private _designationService: DesignationService,
    private _userDetailservice: UserDetailsService,
    private _targetService: TargetService,
    private _toastrService: ToastrService,
    private exportAsService: ExportAsService,
    private _changeDetectorRef: ChangeDetectorRef,

  ) { }

  targetForm = new FormGroup({
    reportType: new FormControl(null),
    catagory: new FormControl(null),
    stateInfo: new FormControl(null),
    Hqinfo: new FormControl(null),
    designation: new FormControl(null),
    empName: new FormControl(null),
    year: new FormControl(null),
  })

  toggleFilter(event: MatSlideToggle) {
    this.isToggled = event.checked;
  }

  toggleFunction(val) {
    this.viewRadioType = val;
  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  // }

  ngOnInit() {
    let financialYear = this._monthYearservice.getFinancialYear();
    this.yearList = financialYear;

    let obj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      designationLevel: this.currentUser.userInfo[0].designationLevel,
    };

    this._divisionService.getDivisions(obj).subscribe(res => {
      if (res.length > 0) {
        res[0].divisionObj.sort(function (a, b) {
          var textA = a.divisionName.toUpperCase();
          var textB = b.divisionName.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        this.divisionData = res[0].divisionIds;
      }
    })

    this.isToggled = true;
  }

  getData(val) {
    this.targetForm.patchValue({ repoType: val })
  }

  getCatagory(val) {
    this.showData = false;
    this.targetForm.patchValue({ catagory: val });
    this.controller(val);
  }

  controller(val) {
    if (val == 'State') {
      this.employee = false;
      this.headquarter = false;
      this.state = true;
      this.callStateComponent();
    } else if (val == 'Headquarter') {
      this.employee = false;
      this.state = true;
      this.headquarter = true;

      this.callStateComponent();
    } else if (val == 'Employee Wise') {
      this.headquarter = false;
      this.employee = true;
      this.state = false;

      this.callStateComponent();
      this.callDesignationComponent();
    }
  }

  callStateComponent() {
    if (this.currentUser.userInfo[0].rL == 0) {
      this._stateService.getStates(this.currentUser.companyId).subscribe(res => {
        this.stateData = res[0].stateInfo;
        this.stateIds = res[0].stateIds;
        this.stateIdsLength = res[0].stateIds.length;
      });
    }
  }

  getStateValue(val) {
    this.targetForm.patchValue({ stateInfo: val })
    this.callDistrictComponent(val);
  }

  callDistrictComponent(val) {
    if (this.currentUser.company.isDivisionExist == true) {
      let Obj = {
        division: this.divisionData,
        companyId: this.currentUser.companyId,
        stateId: val,
      };


      if (this.currentUser.userInfo[0].designationLevel == 0) {
        this._districtService.getDistrictsBasedOnDivision(Obj).subscribe(res => {
          if (res.length == 0 || res[0].districtObj == undefined) {
            this.districtData = [];
            this.districtIds = [];
            this.districtIdsLength = 0;
          } else {
            res[0].districtObj.sort(function (a, b) {
              var textA = a.districtName.toUpperCase();
              var textB = b.districtName.toUpperCase();
              return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
            this.districtData = res[0].districtObj;
            this.districtIds = res[0].districtIds;
            this.districtIdsLength = res[0].districtIds.length;

          }
        })
      }
    }
  }

  getHeadquarterValue(val) {
    this.targetForm.patchValue({ Hqinfo: val })
  }


  callDesignationComponent() {
    let params = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: this.divisionData,
    }
    this._designationService.getDesignationBasedOnDivision(params).subscribe(
      res => {
        this.designationData = res;
      });
  }

  getDesignation(val) {
    this.targetForm.patchValue({ designation: val })

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let object = {
          companyId: this.currentUser.companyId,
          designationObject: [val],
          status: [true],
          division: this.divisionData,
        };

        this._userDetailservice
          .getUserInfoBasedOnDesignationAndBasedOnDivision(object)
          .subscribe(
            (res) => {
              this.employeeData = res;
              this.userIds = res.map((item) => {
                return item.userId;
              });
            });

      } else if (this.currentUser.company.isDivisionExist == false) {
        this._userDetailservice
          .getUserInfoBasedOnDesignation(this.currentUser.companyId, [val], [true])
          .subscribe(
            (res) => {
              this.employeeData = res;
              this.userIds = res.map((item) => {
                return item.userId;
              });
              this.changeDetectorRef.detectChanges();
            })
      }
    }
  }


  setEmployeeDetail(val) {
    this.targetForm.patchValue({ empName: val });
  }

  setFinantialYear(val) {
    this.targetForm.patchValue({ year: val });
    this.valueChange.emit(val);
  }

  toggleAllSelection() {
    this.allSelected = !this.allSelected;  // to control select-unselect
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => { item.deselect() });
    }
  }

  toggleAllSelectionForHQ() {
    this.allSelectedHq = !this.allSelectedHq;  // to control select-unselect
    if (this.allSelectedHq) {
      this.selectHQ.options.forEach((item: MatOption) => item.select());
    } else {
      this.selectHQ.options.forEach((item: MatOption) => { item.deselect() });
    }
  }



  viewRecords() {
    this.isToggled = false;
    let object = {
      "companyId": this.currentUser.companyId,
      "type": this.targetForm.value.catagory,
      "userId": [this.targetForm.value.empName],
      "year": this.targetForm.value.year,
      stateId: this.targetForm.value.stateInfo,
      districtId: this.targetForm.value.Hqinfo,
      viewType: this.viewRadioType,
      targetType: 'productwise'
    }
    if (this.targetForm.value.catagory == 'State' || this.targetForm.value.catagory == 'Headquarter') {
      object['designation'] = this.targetForm.value.designation;
    } else if (this.targetForm.value.catagory == 'Employee Wise') {
      this.showStateName = false;
      this.showEmpName = true;
      this.showHQName = false;
      object['designation'] = this.targetForm.value.designation.designationLevel;
    }
    if (this.targetForm.value.catagory == 'State') {
      this.showStateName = true;
      this.showEmpName = false;
      this.showHQName = false;
    }
    if (this.targetForm.value.catagory == 'Headquarter') {
      this.showStateName = false;
      this.showEmpName = false;
      this.showHQName = true;
    }
    this._targetService.getTargetFinancialYearWise(object).subscribe((res) => {
      if (res == null || res == undefined || res.length == 0) {
        this.showData = false;
        this.isToggled = true;
        this._toastrService.error("Selected Data Not Found");
      } else {
        this.isToggled = false;
        this.showData = true;
        // this.showProcessing = false;
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
        this.dataSource = res
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
      }
    })
  }

  exporttoExcel() {
    this.exportAsService.save(this.exportAsConfig, 'Target report').subscribe(() => {

    });

  }

  applyFilter() {

  }

}
