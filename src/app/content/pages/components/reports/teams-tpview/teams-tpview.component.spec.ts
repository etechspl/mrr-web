import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsTPViewComponent } from './teams-tpview.component';

describe('TeamsTPViewComponent', () => {
  let component: TeamsTPViewComponent;
  let fixture: ComponentFixture<TeamsTPViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsTPViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsTPViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
