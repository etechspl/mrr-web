import { URLService } from './../../../../../core/services/URL/url.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MonthYearService } from '../../../../../core/services/month-year.service';
import { DesignationService } from '../../../../../core/services/Designation/designation.service';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
import { DivisionComponent } from '../../filters/division/division.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { MatPaginator, MatTableDataSource, MatTable, MatSlideToggleChange } from '@angular/material';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
@Component({
  selector: 'm-teams-tpview',
  templateUrl: './teams-tpview.component.html',
  styleUrls: ['./teams-tpview.component.scss']
})
export class TeamsTPViewComponent implements OnInit {
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatPaginator)
  set paginator(value: MatPaginator) {
    this.dataSource.paginator = value;
  }
  check: boolean;
  public showProcessing: boolean = false;

  displayedColumns = ['date', 'submittedActivity', 'apprvActivity', 'name','submittedArea', 'approvedArea', 'submittedDoctor', 'apprvDoctor', 'submittedChemist', 'approvedChemists', 'submittedRemark', 'apprvRemark'];
  dataSource = new MatTableDataSource<any>();
  private tpSubmittionDate = "---------";
  private tpApprovalDate = "---------";
  private tpApprovedBy = "---------";
  //displayedColumns = ['position', 'name', 'weight', 'symbol'];
  //dataSource = ELEMENT_DATA;

  currentUser = JSON.parse(sessionStorage.currentUser);
  viewFor = new FormControl(null);
  designation = new FormControl(null);
  employees = new FormControl(null);
  month = new FormControl(true);
  year = new FormControl(true);
  teamTPFilter: FormGroup;

  isDesignationFilterView = false;
  designationList;

  showTPDetails = false;
  //showSpinner = false;
  public showDivisionFilter: boolean = false;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  constructor(private fb: FormBuilder,
    private monthYearService: MonthYearService,
    private _designation: DesignationService,
    private _tourProgram: TourProgramService,
    private _changeDetectorRef: ChangeDetectorRef,
	private _toastr: ToastrService,
  private exportAsService: ExportAsService,
  private _urlService: URLService
  
  ) {

    this.teamTPFilter = this.fb.group({
	  viewFor: new FormControl(null, Validators.required),
	  designation: new FormControl(null, Validators.required),
	  employees: new FormControl(null, Validators.required),
	//   designation: new FormControl(null),
	//   employees: new FormControl(null),
      month: new FormControl(null, Validators.required),
      year: new FormControl(null, Validators.required)
    })
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    console.log('toggle', event.checked);
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {

      this.teamTPFilter.setControl('division', new FormControl(null, Validators.required));

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
	}
	  }
  getDivisionValue(val) {
    this.showTPDetails = false;
    this.teamTPFilter.patchValue({ division: val });

    //=======================Clearing Employee Filter===================
    this.callDesignationComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: [val]
    }

    this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);

  }
  getMonth(val) {
    // this.showSpinner = false;
    this.showTPDetails = false;

    this.teamTPFilter.patchValue({ month: val });
  }
  getYear(val) {
    //this.showSpinner = false;
    this.showTPDetails = false;
    this.teamTPFilter.patchValue({ year: val });
  }

  getDesignation(val) {
    //console.log(val)
    //  this.showSpinner = false;
    this.showTPDetails = false;
    this.teamTPFilter.patchValue({ designation: val });

    this.callEmployeeComponent.setBlank();

    if (this.currentUser.company.isDivisionExist == true) {
      if (this.currentUser.company.isDivisionExist == true && this.teamTPFilter.value.division != null) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val.designationLevel,
          status: [true],
          division: [this.teamTPFilter.value.division]
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      }
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, [true]);
    }

  }
  getEmployees(val) {
    // this.showSpinner = false;
    this.showTPDetails = false;
    this.teamTPFilter.patchValue({ employees: val });
  }


  //****************************************** Edit by Umesh 13-05-2020 */
  showEmployeeFilter(event) {
    // this.showSpinner = false;
    this.showTPDetails = false;
    if (event == "teamTP") {
      this.isDesignationFilterView = true;
      if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.teamTPFilter.addControl('designation', new FormControl('', Validators.required));
        this.teamTPFilter.addControl('employees', new FormControl('', Validators.required));
      }
    } else if (event == "self") {
      this.showDivisionFilter = false;
	  this.isDesignationFilterView = false;
	  this.teamTPFilter.removeControl('designation');
	  this.teamTPFilter.removeControl('employees');

    }
  }
//*************************************************************************************** */


  showReport() {
    this.isToggled = false;
    this.showProcessing = true;
    // this.showSpinner = true;
    if (this.teamTPFilter.valid) {

      let userId = "";
      if (this.teamTPFilter.value.viewFor == "teamTP") {
        userId = this.teamTPFilter.value.employees;
      } else if (this.teamTPFilter.value.viewFor == "self") {
        userId = this.currentUser.id;
      }

      this._tourProgram.viewTourProgram(this.currentUser.companyId, userId, this.teamTPFilter.value.month, this.teamTPFilter.value.year).subscribe(res => {
        let array = [];
        array.push(res)
        //this.dataSourceForTPInDetail.push(res);
        if (array[0].length > 0) {
          this.showProcessing = false;

          this.dataSource = new MatTableDataSource(array[0]);
          this.dataSource.paginator = this.paginator;

          this.tpSubmittionDate = array[0][0].sendForApprovalDate;
          this.tpApprovalDate = array[0][0].approvedAt;
          this.tpApprovedBy = array[0][0].apporvedByName;
          this.showTPDetails = true;

        } else {
          this._toastr.info('For Selected Filter data not found', 'Data Not Found');
          this.showProcessing = false;
          this.showTPDetails = false;

        }
        this._changeDetectorRef.detectChanges();


        //  this.showSpinner = false;
        console.log(res);

      }, err => {
        console.log(err);
      })

    }
  }
//   showReport() method end here.



  exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Team TP Report').subscribe(() => {
      // save started
    });
  }

  
	onPreviewClick(){
   
		let printContents, popupWin;
	  
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;
		printContents = document.getElementById('tableId1').innerHTML;
		const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
	
		popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		popupWin.document.open();
		popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;
	}
	
	  tg.th {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  font-weight: normal;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4; margin: 5mm; }
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
	  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
	
	  </div>
	
	  <div style="text-align: right;">
	  <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Basic Info </h2>
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  ${printContents}</body>
	  </html>
	  `
		);
	   // popupWin.document.close();
	 }

}
