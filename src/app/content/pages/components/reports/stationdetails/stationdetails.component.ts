import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import * as _moment from 'moment';

import Swal from "sweetalert2";
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { TypeComponent } from '../../filters/type/type.component';
import { StatusComponent } from '../../filters/status/status.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { UserCompleteDCRInfoComponent } from '../user-complete-dcrinfo/user-complete-dcrinfo.component';
import { StateComponent } from '../../filters/state/state.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserAreaMappingService } from '../../../../../core/services/UserAreaMpping/user-area-mapping.service';

import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { URLService } from '../../../../../core/services/URL/url.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
declare var $;
@Component({
  selector: 'm-stationdetails',
  templateUrl: './stationdetails.component.html',
  styleUrls: ['./stationdetails.component.scss']
})
export class StationdetailsComponent implements OnInit {
  public showReport: boolean = true;
  public showEmployee: boolean = true;
  public showDateWiseDCRReport: boolean = true;
  public showProcessing: boolean = false;
  public showDetails: boolean = true;
  logoURL: any;
  count = 0;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("setStatus") setStatus: StatusComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callUserCompleteDCR") callUserCompleteDCR: UserCompleteDCRInfoComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;

  @ViewChild('dataTable') table;
  showTotalMasterData: boolean = false;
  myAreas;

  constructor(private _changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private providerService: ProviderService,
    private userAreaMappingService: UserAreaMappingService,
    private _userDetailService: UserDetailsService,
    private _urlService: URLService
  ) { }
  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  colspanValue;
  isDivisionExist = this.currentUser.company.isDivisionExist;
  designationLevel = this.currentUser.userInfo[0].designationLevel;
  companyId = this.currentUser.companyId;
  currentActiveUsers;
  totalDOctorCOunt: number = 0;
  totalChemistCount: number = 0;
  totalStockistCount: number = 0;
  // dataSource: any;
  dataTable: any;
  dtOptions: any;
  queryObject: any;
  queryChemistObject: any;
  queryStockistObject: any;
  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;
      this.stationDataForm.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);



    } else {
      this.isShowDivision = false;
    }
    this.getReportType('Hierarachy');
  }
  check: boolean = false;



  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  stationDataForm = new FormGroup({
    type: new FormControl(null, Validators.required),
    status: new FormControl(true),
    stateInfo: new FormControl(null),
    employeeId: new FormControl(null),
    designation: new FormControl(null),
  })
  getReportType(val) {
    if (val === "Hierarachy") {
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }
  getTypeValue(val) {
    if (val == "Employee Wise") {
      this.showEmployee = true;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.stationDataForm.removeControl("stateInfo");
      this.stationDataForm.removeControl("districtInfo");
      this.stationDataForm.setControl("designation", new FormControl('', Validators.required))
      this.stationDataForm.setControl("employeeId", new FormControl('', Validators.required))
    } else if (val == "Self") {
      this.showEmployee = false;
    }



    this.stationDataForm.patchValue({ type: val });
    this._changeDetectorRef.detectChanges();


  }

  getEmployeeValue(val) {
    this.stationDataForm.patchValue({ employeeId: val });
  }


  getDesignation(val) {
    this.stationDataForm.patchValue({ designation: val });
    if (this.isDivisionExist === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.companyId,
        division: this.stationDataForm.value.divisionId,
        status: [true],
        designationObject: this.stationDataForm.value.designation
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
    }

  }
  getDivisionValue($event) {
    this.stationDataForm.patchValue({ divisionId: $event });
    if (this.isDivisionExist === true) {
      if (this.stationDataForm.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.stationDataForm.value.divisionId != null && this.stationDataForm.value.designation != null) {
          if (this.stationDataForm.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.companyId,
              division: this.stationDataForm.value.divisionId,
              status: [true],
              designationObject: this.stationDataForm.value.designation
            })
          }
        }
      }
    }
  }

  getAreas(companyId, userId) {
    this.userAreaMappingService.getMappedAreas(companyId, userId).subscribe(res => {
      let response = [];
      response.push(res);
      if (response[0].length > 0) {
        this.myAreas = res;
      }
    }, err => {
      alert("Error while getting the areas");
    })
  }
  viewRecords() {


    this.totalDOctorCOunt = 0;

    this.totalChemistCount = 0;

    this.totalStockistCount = 0;

    this.isToggled = false;
    this.showReport = false;
    this.showProcessing = true;
    this.showTotalMasterData = false;
    this.showDetails = true;
    if (this.stationDataForm.value.employeeId == null) {
      this.stationDataForm.value.employeeId = [this.currentUser.id];
    }
    let params = {};
    if (this.isDivisionExist === true) {
      params["division"] = this.stationDataForm.value.divisionId;
    } else {
      params["division"] = [];
    }

    params["isDivisionExist"] = this.isDivisionExist;
    params["companyId"] = this.companyId;
    params["stateIds"] = this.stationDataForm.value.stateInfo;
    params["districtIds"] = this.stationDataForm.value.districtInfo;
    params["type"] = this.stationDataForm.value.type;
    params["employeeId"] = this.stationDataForm.value.employeeId;
    params["areaId"] = this.stationDataForm.value.areaId;
    params["status"] = this.stationDataForm.value.status;


    this.providerService.getStationDetails(params).subscribe(res => {
      console.log(":res ",res)
      if (res.length == 0 || res == undefined) {
        this.showProcessing = false;
        this.showTotalMasterData = false;
        (Swal as any).fire({
          title: "No Records Found",
          type: "info",
        });
      } else {
        for (let i = 0; i < res.length; i++) {
          this.totalDOctorCOunt += res[i].totalRMP;
          this.totalChemistCount += res[i].totalDrug;
          this.totalStockistCount += res[i].totalStockist;
        }
        this.showProcessing = false;
        this.showTotalMasterData = true;
        res.forEach((item, i) => {
          item["index"] = i + 1;
        });
        const PrintTableFunction = this.PrintTableFunction.bind(this);
        this.dtOptions = {
          pagingType: "full_numbers",
          ordering: true,
          info: true,
          scrollY: 300,
          scrollX: true,
          scrollCollapse: true,
          paging: false,
          destroy: true,
          data: res,
          responsive: true,
          columns: [
            {
              title: "S No.",
              data: "index",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "User",
              data: "user",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "State",
              data: "state",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "District",
              data: "district",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "Area",
              data: "Area",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: `Total ${this.currentUser.company.lables.vendorLabel}`,
              data: "totalDrug",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: `Total ${this.currentUser.company.lables.doctorLabel}`,
              data: "totalRMP",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: `Total ${this.currentUser.company.lables.stockistLabel}`,
              data: "totalStockist",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            }, {
              title: 'Delete',
              defaultContent: "<button id='modifyId' class='btn btn-warning btn-sm'><i class='la la-delete'></i>&nbsp;&nbsp;Delete</button>"
            }
          ],
          rowCallback: (row: Node, data, index: number) => {
            console.log("data : ",data);
            const self = this;
            $('td', row).unbind('click');
            $('td button', row).bind('click', (event) => {
              let rowObject = JSON.parse(JSON.stringify(data))
              if (event.target.id == "modifyId") {
                Swal.fire({
                  title: 'Are you sure want to delete?',
                  text: 'Enter Deletion Reason',
                  input: 'text',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes, Delete it!',
                  cancelButtonText: 'No, cancel!',
                  reverseButtons: true,
                  allowOutsideClick: false
                }).then(async (result) => {
                  if (result.value) {

                    let filter: any = {
                      areaId: data.areaId,
                      userId: data.userId
                    }
                    let obj = {
                      status: false,
                      delStatus: 'approved'
                    }
                    console.log("filter : 0-----------------------",filter);
                    this.userAreaMappingService.changeAreaStatus({ filter, obj }).subscribe(areaRes => {
                      if (areaRes === 'Status Updated') {
                        Swal.fire({
                          title: 'Area and Mapping deleted successfully!!!',
                          text: 'Your area has been deleted successfully!',
                          type: 'success',
                          allowOutsideClick: false
                        }).then((result2) => {
                          this.showTotalMasterData = false
                          this.isToggled = true;
                          //this.getAreas(this.currentUser.companyId, this.stationDataForm.value.userId);
                          this._changeDetectorRef.detectChanges();
                        });
                      } else {
                        const Toast = (Swal as any).mixin({
                          toast: true,
                          position: 'top-end',
                          showConfirmButton: false,
                          timer: 3000,
                          timerProgressBar: true,
                          didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                          }
                        })

                        Toast.fire({
                          icon: 'error',
                          title: 'Updatation Fail'
                        })
                      }
                    });
                  } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire(
                      'Cancelled',
                      'Area was not deleted.',
                      'error'
                    )
                    this._changeDetectorRef.detectChanges();
                  }
                })
              }
            })

          },
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Records",
          },
          dom: "Bfrtip",
          buttons: [
            {
              extend: "excel",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: "csv",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: 'copy',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'print',
              title: function () {
                return 'State List';
              },
              exportOptions: {
                columns: ':visible'
              },
              action: function (e, dt, node, config) {
                PrintTableFunction(res);
              }
            }
          ],
        };
        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);


        !this._changeDetectorRef["destroyed"]
          ? this._changeDetectorRef.detectChanges()
          : null;
      }
    })

  }


  PrintTableFunction(data?: any): void {

    const companyName = this.currentUser.companyName;

    const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
    const tableRows: any = [];

    data.forEach(element => {

      tableRows.push(`<tr>
  <td class="tg-oiyu">${element.index}</td>
  <td class="tg-oiyu">${element.user}</td>
	<td class="tg-oiyu">${element.state}</td>
	<td class="tg-oiyu">${element.district}</td>
	<td class="tg-oiyu">${element.Area}</td>
	<td class="tg-oiyu">${element.totalDrug}</td>
	<td class="tg-oiyu">${element.totalRMP}</td>
	<td class="tg-oiyu">${element.totalStockist}</td>
	</tr>`)
    });

    let showHeaderAndTable: boolean = false;
    // this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
    let printContents, popupWin;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
	<html>
	<head>
	<style>
	img {
	float: left;
	}

	.flex-container {
	display: flex;
	justify-content: space-between;
	}

	.tg {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
	}

	.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
  }
  
  .tb2 .tg-oikpyu {
    font-size: 10px;
    text-align: center;
    vertical-align: middle
    }

  
	.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 6px 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tg .tg-lboi {
	border-color: inherit;
	text-align: left;
	vertical-align: middle;

	}

	.tg .tg-u6fn {
	font-weight: bold;
	font-size: 18px;
	background-color: #efefef;
	color:black;
	border-color: inherit;
	text-align: left;
	vertical-align: top;
	padding-top: 0px;
	padding-bottom: 0px;
	}

	.tg .tg-yz93 {
	border-color: inherit;
	text-align: right;
	vertical-align: middle
	}
	.tb2 {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	margin-top: 10px;
  }
  
  

	.tb2 td {
	font-family: Arial, sans-serif;
	font-size: 12px;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 th {
	font-family: Arial, sans-serif;
	font-size: 12px;
	font-weight: normal;
	padding: 2px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: black;
	}

	.tb2 .tg-oiyu {
	font-size: 10px;
	text-align: left;
	vertical-align: middle
	}
	.tg-bckGnd {
	font-weight: bold;
	background-color: #efefef;
	}

	.tb2 .tg-3ppa {
	font-size: 10px;
	background-color: #efefef;
	color:black;
	text-align: left;
	vertical-align: middle;
	padding-top: 2px;
	padding-bottom: 2px;
	}

	.footer {
	position: sticky;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: #efefef;
	color: rgb(2, 2, 2);
	text-align: right
	}

	button {
	background-color: #008CBA;
	border: none;
	color: white;
	margin: 5px 0px;
	padding: 2px 10px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
	border-radius: 12px;
	cursor: pointer;
	outline: none;
	}
	@media print {
	@page { size: A4; margin: 5mm; }
	.footer {
	position: relative;
	}
	tr {
	page-break-inside: avoid;
	}
	button{
	display : none;
	}
	}
	</style>
	</head>
	<body onload="window.print();window.close()" style="padding-bottom:30px">
	<div class="flex-container">
	<div style="width: 40%; ">
	<img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	<h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

	</div>

  <h2 style="margin-top: 0px; margin-bottom: 5px " ; align="left"><u>Station Details </u></h2>
	<div>
	<button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	<button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	</div>
	</div>
	</div>

	<div>
  
	<table class="tb2">
	<tr>
  <th class="tg-3ppa"><span style="font-weight:20">S.No.</span></th>
  <th class="tg-3ppa"><span style="font-weight:20">Employee Name</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">State</span></th>
  <th class="tg-3ppa"><span style="font-weight:300">District</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">Area </span></th>
	<th class="tg-3ppa"><span style="font-weight:300">Total Chemist</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">Total Doctors</span></th>
	<th class="tg-3ppa"><span style="font-weight:300">Total Stockist</span></th>
              
	</tr>
	${tableRows.join('')}
	</table>
	</div>
	
	</body>
	</html>
	`);

    // popupWin.document.close();

  }

}