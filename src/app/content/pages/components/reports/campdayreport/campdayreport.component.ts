import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { TypeComponent } from "../../filters/type/type.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { StateComponent } from "../../filters/state/state.component";
import { DistrictComponent } from "../../filters/district/district.component";
import { DivisionComponent } from "../../filters/division/division.component";
import { Division } from "../../../_core/models/division";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";
declare var $;
import * as _moment from "moment";
import { StateService } from "../../../../../core/services/state.service";
import { URLService } from "../../../../../core/services/URL/url.service";
import { MatSlideToggleChange } from "@angular/material";

@Component({
  selector: 'm-campdayreport',
  templateUrl: './campdayreport.component.html',
  styleUrls: ['./campdayreport.component.scss']
})
export class CampdayreportComponent implements OnInit {
	public showState: boolean = true;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	public showDateWiseDCRReport: boolean = false;
	public showProcessing: boolean = false;
	isShowDivision = false;
	showAdminAndMGRLevelFilter = false;
	displayedColumns: string[] = ['position', 'name', 'weight','Camp', 'symbol'];
	isShowUsersInfo : boolean = false;
	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;
	companyId = this.currentUser.companyId;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	stateList;
	constructor(
		private _changeDetectorRef: ChangeDetectorRef,
		private _dcrReportsService: DcrReportsService,
		private stateService: StateService,
		private _urlService: URLService
	) {}

	dcrstatusForm = new FormGroup({
		reportType: new FormControl("Geographical"),
		type: new FormControl(null),
		stateInfo: new FormControl(null),
		// stateInfo: new FormControl(null, Validators.required),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		designation: new FormControl(null),
		toDate: new FormControl(null, Validators.required),
		fromDate: new FormControl(null, Validators.required),
	});
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;

	@ViewChild("dataTable") table;
	dataTable: any;
	dtOptions: any;
	dataSource;

	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
	ngOnInit() {
		this.stateService.getStates("5b2e22083225df01ba7d6059").subscribe(
			(res) => {
				this.stateList = res;
				console.log("state result", res);
			},
			(err) => {
				console.log(err);
				alert("No State Found");
			}
		);
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.dcrstatusForm.setControl("divisionId", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			if (this.currentUser.userInfo[0].rL > 1) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}
		if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			this.showAdminAndMGRLevelFilter = true;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		}
	}

	getReportType(val) {
		this.dcrstatusForm.patchValue({ reporttype: val });
		if (val === "Geographical") {
			this.showState = true;
			this.showEmployee = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			this.showState = false;
			this.showDistrict = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
	}

	getTypeValue(val) {
		this.showReport = false;
		if (val == "State") {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.callStateComponent.setBlank();
			this.showState = true;
			this.showDistrict = false;
			this.dcrstatusForm.removeControl("employeeId");
			this.dcrstatusForm.removeControl("designation");
			this.dcrstatusForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
		} else if (val == "Headquarter") {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.showDistrict = true;
			this.callStateComponent.setBlank();
			this.dcrstatusForm.removeControl("employeeId");
			this.dcrstatusForm.removeControl("designation");
			this.dcrstatusForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.dcrstatusForm.setControl(
				"districtInfo",
				new FormControl("", Validators.required)
			);
		} else if (val == "Employee Wise") {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.showEmployee = true;
			this.dcrstatusForm.removeControl("stateInfo");
			this.dcrstatusForm.removeControl("districtInfo");
			this.dcrstatusForm.setControl(
				"designation",
				new FormControl("", Validators.required)
			);
			this.dcrstatusForm.setControl(
				"employeeId",
				new FormControl("", Validators.required)
			);
		} else if (val == "Self") {
			this.showEmployee = false;
		}
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
		} else {
			this.isShowDivision = false;
		}
		this.dcrstatusForm.patchValue({ type: val });
		this._changeDetectorRef.detectChanges();
	}

	getDivisionValue($event) {
		console.log($event);
		
		this.dcrstatusForm.patchValue({ divisionId: $event });
		if (this.isDivisionExist === true) {
			if (
				this.dcrstatusForm.value.type === "State" ||
				this.dcrstatusForm.value.type === "Headquarter"
			) {
				this.callStateComponent.getStateBasedOnDivision({
					companyId: this.currentUser.companyId,
					isDivisionExist: this.isDivisionExist,
					division: this.dcrstatusForm.value.divisionId,
					designationLevel: this.currentUser.userInfo[0]
						.designationLevel,
				});
			}
			if (this.dcrstatusForm.value.type === "Employee Wise") {
				//getting employee if we change the division filter after select the designation
				if (
					this.dcrstatusForm.value.divisionId != null &&
					this.dcrstatusForm.value.designation != null
				) {
					if (this.dcrstatusForm.value.designation.length > 0) {
						this.callEmployeeComponent.getEmployeeListBasedOnDivision(
							{
								companyId: this.companyId,
								division: this.dcrstatusForm.value.divisionId,
								status: [true],
								designationObject: this.dcrstatusForm.value
									.designation,
							}
						);
					}
				}
			}
		}
	}

	getStateValue(val) {
		this.dcrstatusForm.patchValue({ stateInfo: val });
		if (this.isDivisionExist === true) {
			if (this.dcrstatusForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.designationLevel,
					companyId: this.companyId,
					stateId: this.dcrstatusForm.value.stateInfo,
					isDivisionExist: this.isDivisionExist,
					division: this.dcrstatusForm.value.divisionId,
				});

				//this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
			}
		} else {
			if (this.dcrstatusForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
	}

	getDistrictValue(val) {
		this.dcrstatusForm.patchValue({ districtInfo: val });
	}

	getEmployeeValue(val) {
		this.dcrstatusForm.patchValue({ employeeId: val });
	}
	getFromDate(val) {
		this.dcrstatusForm.patchValue({ fromDate: val });
	}
	getToDate(val) {
		this.dcrstatusForm.patchValue({ toDate: val });
	}
	getDesignation(val) {
		this.dcrstatusForm.patchValue({ designation: val });
		if (this.isDivisionExist === true) {
			this.callEmployeeComponent.getEmployeeListBasedOnDivision({
				companyId: this.companyId,
				division: this.dcrstatusForm.value.divisionId,
				status: [true],
				designationObject: this.dcrstatusForm.value.designation,
			});
		} else {
			this.callEmployeeComponent.getEmployeeList(
				this.currentUser.companyId,
				val,
				[true]
			);
		}
	}

	dcrStatusReport() {
		this.isToggled = false;
		this.showReport = false;
		this.showProcessing = true;
		let params = {};

		if (this.isDivisionExist === true) {
			params["division"] = this.dcrstatusForm.value.divisionId;
		} else {
			params["division"] = [];
		}

		params["isDivisionExist"] = this.isDivisionExist;
		params["companyId"] = this.companyId;
		params["stateIds"] = this.dcrstatusForm.value.stateInfo;
		params["districtIds"] = this.dcrstatusForm.value.districtInfo;
		params["type"] = this.dcrstatusForm.value.type;
		params["employeeId"] = this.dcrstatusForm.value.employeeId;
		params["fromDate"] = this.dcrstatusForm.value.fromDate;
		params["toDate"] = this.dcrstatusForm.value.toDate;

		this._dcrReportsService.campStutas(params).subscribe(
			(res) => {
				this.isShowUsersInfo = true;
				this.dataSource = res;
				this._changeDetectorRef.detectChanges();
				if (this.dataSource.length > 0) {
					this._changeDetectorRef.detectChanges();
				}
			
			},
			(err) => {
				console.log(err);
				this._changeDetectorRef.detectChanges();
			}
		);
	}


}
