import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampdayreportComponent } from './campdayreport.component';

describe('CampdayreportComponent', () => {
  let component: CampdayreportComponent;
  let fixture: ComponentFixture<CampdayreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampdayreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampdayreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
