import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import * as _moment from "moment";
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';

@Component({
  selector: 'm-employee-leave-details',
  templateUrl: './employee-leave-details.component.html',
  styleUrls: ['./employee-leave-details.component.scss']
})
export class EmployeeLeaveDetailsComponent implements OnInit {

  currentUser = JSON.parse(sessionStorage.currentUser);
  currentMonth = new Date().getMonth();
	currentYear = new Date().getFullYear();
	Date = new Date().getDate();
  actualMonth = this.currentMonth + 1;
  currentDate = this.currentYear + "-" + this.actualMonth + "-" + this.Date;
  public onLeave: number = 0;
  districtId=[];
  employeeId = [];
  showProcessing: boolean = false;

  dataSource : MatTableDataSource<any>;
  displayedColumns = ['sn','username','districtName','status'];

  constructor(
    private _DCRReportService: DcrReportsService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    let date = _moment(new Date(this.currentDate)).format("YYYY-MM-DD");
		let onLeaveObj = {
			where: {
				companyId: this.currentUser.companyId,
				dcrDate: date,
				workingStatus: "Leave"
			}
      
		}
		this._DCRReportService.getActiveUsers(onLeaveObj).subscribe(res => {
      if(res==null||res==undefined||res.length==0){
console.log("res+++++",res);
this.showProcessing = false;

      }else{
        console.log("res+++++",res);
        this.showProcessing = false;

        this.dataSource= new MatTableDataSource(res);
        this._changeDetectorRef.detectChanges();
      }
			console.log(this.dataSource);
		})


  }

  callOnLeave() {
		
	}

  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase();
		this.dataSource.filter = filterValue
  }
}
