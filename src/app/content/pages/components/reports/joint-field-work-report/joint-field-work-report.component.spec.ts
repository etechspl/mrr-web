import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JointFieldWorkReportComponent } from './joint-field-work-report.component';

describe('JointFieldWorkReportComponent', () => {
  let component: JointFieldWorkReportComponent;
  let fixture: ComponentFixture<JointFieldWorkReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JointFieldWorkReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JointFieldWorkReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
