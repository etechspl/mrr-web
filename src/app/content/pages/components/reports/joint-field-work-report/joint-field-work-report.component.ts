import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StatusComponent } from '../../filters/status/status.component';
import { Designation } from '../../../_core/models/designation.model';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { MatTableDataSource, MatTable, MatSort, MatPaginator, _MatChipListMixinBase } from '@angular/material';
import { DivisionComponent } from '../../filters/division/division.component';
import { PrimaryAndSaleReturnService } from '../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service';
import { UtilsService } from '../../../../../core/services/utils.service';
import { of } from 'rxjs';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import * as _moment from 'moment';

declare var $;

@Component({
  selector: 'm-joint-field-work-report',
  templateUrl: './joint-field-work-report.component.html',
  styleUrls: ['./joint-field-work-report.component.scss']
})
export class JointFieldWorkReportComponent implements OnInit {

  mgrJointForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  constructor(
    private _toastrService: ToastrService,
    private _fb: FormBuilder,
    private utilService: UtilsService,
    private _primarySaleReturn: PrimaryAndSaleReturnService,
    private _changeDetectorRef: ChangeDetectorRef,
    private exportAsService: ExportAsService,
    private _dcrReportsService: DcrReportsService
  ) {
    this.createForm(this._fb);
  }

  check: boolean = false;
  isStatus: boolean = true;
  isMultipleEmp: boolean = true;
  data: any;
  showProcessing1: boolean = false;
  isShowVisitedReport: boolean = false;
  isShowReport: boolean = false;
  showProcessing: boolean = false;

  //, 'workedWith', 'district', 'area', 'visits'
  displayedColumns = ['sn', 'dcrDate', 'workedWith', 'district', 'visitedArea', 'visits'];
  displayedColumnsProviders = ['sn', 'providerName', 'providerCode', 'providerType', 'status'];
  dataSource = new MatTableDataSource<any>();
  prodataSource = new MatTableDataSource<any>();
  showDivisionFilter: boolean = false;

  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {

      this.mgrJointForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  createForm(_fb: FormBuilder) {
    this.mgrJointForm = _fb.group({
      type: new FormControl('monthly', Validators.required),
      designation: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      userId: new FormControl('', Validators.required),
      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    })
  }

  getDivisionValue(val) {
    this.showProcessing = false;
    this.mgrJointForm.patchValue({ division: val });
    //=======================Clearing Filter===================

    this.callDesignationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
  }

  getDesignationList() {
    this.isShowReport = false;

    this.callDesignationComponent.getManagersDesignation();
  }

  getDesignationLevel(designation: Designation) {
    this.isShowReport = false;
    //=======================Clearing Filter===================
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================
    this.mgrJointForm.patchValue({ designation: designation.designationLevel })
  }

  getStatus(status: boolean) {
    this.isShowReport = false;
    this.mgrJointForm.patchValue({ status: status });
    //=======================Clearing Filter===================
    this.callEmployeeComponent.setBlank();
    //===============================END================================
    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        designationObject: this.mgrJointForm.value.designation,
        status: status,
        division: this.mgrJointForm.value.division
      }
      this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.mgrJointForm.value.designation, status);
    }
  }

  getEmployee(emp: string) {
    this.isShowReport = false;

    this.mgrJointForm.patchValue({ userId: emp })
  }

  getFromDate(month) {
    this.isShowReport = false;

    this.mgrJointForm.patchValue({ month: month })
  }

  getToDate(year) {
    this.isShowReport = false;

    this.mgrJointForm.patchValue({ year: year })
  }


  generateReport() {
    this.showProcessing=true;
    let params = {
      companyId: this.currentUser.companyId,
      submitBy: this.mgrJointForm.value.userId,
      status:this.mgrJointForm.value.status,
      month: this.mgrJointForm.value.month,
      year: this.mgrJointForm.value.year,
      designation:this.mgrJointForm.value.designation
    }
    this._dcrReportsService.getJointFieldWork(params).subscribe(res => {
      this.showProcessing=false;

      if (res.length == 0 || res == undefined) {
        this.showProcessing = false;
        this._toastrService.info('For Selected Filter data not found', 'Data Not Found');
      } else {
        this.showProcessing = false;
        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: res,
          columns: [
          {
            title: 'State Name',
            data: 'stateName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: this.currentUser.company.lables.hqLabel + ' Name',
            data: 'districtName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },{
            title: 'DCR Date',
            data: 'dcrDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return _moment(data).format("DD-MM-YYYY")
              }
            },
            defaultContent: '---'
          },
          {
            title: 'User Name',
            data: 'userName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'User Designation',
            data: 'userDesignation',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Joint Work With',
            data: 'jointWorkWith',                                 
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Joint Work With Designation',
            data: 'jointWorkWithName',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Pre Call Planning',
            data: 'preCallPlanning',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }

            },
            defaultContent: '---'
          },
          {
            title: 'Product Knowledge',
            data: 'productKnowledge',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Utilization Of Visual aid &amp; Inputs as per PMT plan',
            data: 'isUtilizationVisual',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                if(data === true){
                  return 'Yes'
                }
                return 'No'
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Clinic Communication',
            data: 'clinicCommunication',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'BE Carrying Dr List or Not',
            data: 'isCarryingDrList',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                if(data === true){
                  return 'Yes'
                }
                return 'No'              }
            },
            defaultContent: '---'
          },
          {
            title: 'Opening The Call',
            data: 'openingCall',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Closing The Call',
            data: 'closingCall',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'RCPA Status',
            data: 'rcpaStatus',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Cov As on Date',
            data: 'covAsOnDate',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Primary Billing Status',
            data: 'primaryBillStatus',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Secondary Sales Status',
            data: 'secBillStatus',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'BE overall performance and Attitude During Joint Working',
            data: 'performance',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          },
          {
            title: 'Comments',
            data: 'remarks',
            render: function (data) {
              if (data === '') {
                return '---'
              } else {
                return data
              }
            },
            defaultContent: '---'
          }
          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
          },
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'excel',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'csv',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'copy',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'print',
              exportOptions: {
                columns: ':visible'
              }
            }
          ]
        };
      
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);

      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      console.log(err)
    });
 
  }


  exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Joint Field Work Report').subscribe(() => {
      // save started
    });
  }

 



}
