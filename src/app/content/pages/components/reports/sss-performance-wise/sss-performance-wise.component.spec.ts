import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SssPerformanceWiseComponent } from './sss-performance-wise.component';

describe('SssPerformanceWiseComponent', () => {
  let component: SssPerformanceWiseComponent;
  let fixture: ComponentFixture<SssPerformanceWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SssPerformanceWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SssPerformanceWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
