import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ProviderService } from '../../../../../core/services/Provider/provider.service';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { DistrictComponent } from './../../filters/district/district.component';
import { StateComponent } from '../../filters/state/state.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { TypeComponent } from '../../filters/type/type.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { forEach } from 'lodash';
import { element } from 'protractor';
import { MatSlideToggleChange } from "@angular/material";

import Swal from 'sweetalert2';
@Component({
  selector: 'm-providerlocationviweandedit',
  templateUrl: './providerlocationviweandedit.component.html',
  styleUrls: ['./providerlocationviweandedit.component.scss']
})
export class ProviderlocationviweandeditComponent implements OnInit {
  displayedColumns: string[] = ['position','name', 'user', 'weight', 'category', 'symbol', 'address', 'districtName'];
  constructor(
    private _dcrProviderVisitService: ProviderService,
  ) { }

  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  show = false;
  showProcessing: boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist: boolean = false;
  showDivisionFilter: boolean = false;
  isToggled: boolean = false;
  selection = new SelectionModel(true, []);
  selection1 = new SelectionModel(true, []);
  primaryContact: any;
  showHeadquarter: boolean = false;
  employeeFilter: boolean = false;
  stateFilter: boolean = true;
  dataSource: MatTableDataSource<any>;
  // columnHeader = {'studentID': 'ID', 'fname': 'First Name', 'lname': 'Last Name', 'weight': 'Weight', 'symbol': 'Code'};
  tableData: any = [];
  // tableData=[];
  // columnHeader = { 'designation': 'designation', 'employeeName': 'employeeName', 'districtName': 'districtName', 'stateName': 'stateName', 'dcrDate': 'dcrDate', 'dcrNotSubmitted': 'dcrNotSubmitted', 'dcrSubmitted': 'dcrSubmitted' };

  dcrStatus = new FormGroup({
    reportType: new FormControl('Geographical'),
    type: new FormControl(null, Validators.required),
    state: new FormControl(null),
    district: new FormControl(null),
    designation: new FormControl(null),
    employee: new FormControl(null),

  })
  // ---------------------------mahender sharma Start code --------------------------------------------------

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase(); 
  //   this.dataSource.filter = filterValue;
  // }

  // ExportTOExcel()
  // {
  //   let data =  this.dataSource.data;
  //   console.log(data);
  //   const  ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
  //   const wb: XLSX.WorkBook = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  //   XLSX.writeFile(wb,'SheetJS.xlsx');
  //   console.log("exported");

  // }


  // --------------------------------------End Mahender sharma -------------------------------------------------------
  ngOnInit() {
    console.log('currentUser=> ', this.currentUser);


    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) {
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }

    if (this.currentUser.company.isDivisionExist === true) {
      this.isDivisionExist = true;
      this.dcrStatus.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }

  toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}

  getReportType(val) {
    if (val === "Geographical") {
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    } else {
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
    this.dcrStatus.patchValue({ reportType: val })
  }

  getTypeValue(val) {
    console.log(val);
    this.dcrStatus.patchValue({ type: val })
    let { } = this.currentUser;
    if (val === 'State') {

      this.showHeadquarter = false;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callStateComponent.setBlank();
        this.callDivisionComponent.setBlank();
      }
    } else if (val === 'Headquarter') {
      this.showHeadquarter = true;
      this.employeeFilter = false;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;
        this.callDistrictComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDistrictComponent.setBlank();
      }
    } else if (val === 'Employee Wise') {
      this.stateFilter = false;
      this.showHeadquarter = false;
      this.employeeFilter = true;
      if (this.currentUser.company.isDivisionExist == false) {
        this.showDivisionFilter = false;

        this.callEmployeeComponent.setBlank();
      } else if (this.currentUser.company.isDivisionExist == true) {
        this.showDivisionFilter = true;
        this.callDivisionComponent.setBlank();
        this.callDesignationComponent.setBlank();
        this.callEmployeeComponent.setBlank();
      }
    }
  }

  getDivisionValue(val) {
    this.dcrStatus.patchValue({ division: val })
    if (this.dcrStatus.value.type == "Employee Wise") {
      //=======================Clearing Filter===================
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: val
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    } else if (this.dcrStatus.value.type == "State" || this.dcrStatus.value.type == "Headquarter") {
      //=======================Clearing Filter===================
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      //===============================END=======================
      let divisionIds = {
        division: val
      };
      this.callStateComponent.getStateBasedOnDivision(divisionIds);
    }
  }

  getStateValue(val) {
    this.dcrStatus.patchValue({ state: val })
    if (this.dcrStatus.value.type == "Headquarter") {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          division: this.dcrStatus.value.division,
          companyId: this.currentUser.companyId,
          stateId: val
        };
        this.callDistrictComponent.getDistrictsBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrict(val) {
    this.dcrStatus.patchValue({ district: val })
  }

  getDesignation(val) {
    this.dcrStatus.patchValue({ designation: val })
    this.callEmployeeComponent.setBlank();

    if (this.currentUser.userInfo[0].rL === 0) {
      if (this.currentUser.company.isDivisionExist == true) {
        let passingObj = {
          companyId: this.currentUser.companyId,
          designationObject: val,
          status: [true],
          division: this.dcrStatus.value.division
        }
        this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
      } else if (this.currentUser.company.isDivisionExist == false) {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val, [true]);
      }
    }
  }

  getEmployee(val) {
    this.dcrStatus.patchValue({ employee: val })
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    console.log(this.selection.selected);
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(element => {
        this.selection.select(element)
      });
  }



  primaryClick(element) {
    this.primaryContact = element;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }


  getRecords() {
    this.selection.clear();
    console.log('data', this.dcrStatus.value)
    this.showProcessing = true;
    this.isToggled = false;
    let param = {
      companyId: this.currentUser.companyId,
      //type:this.dcrStatus.value.type
    }

    if (this.dcrStatus.value.type === 'State') {
      param['stateId'] = { inq: this.dcrStatus.value.state };
      //param['type'] = 'state'
    } else if (this.dcrStatus.value.type === 'Headquarter') {
      //param['type'] = 'Headquarter'
      param['districtId'] = { inq: this.dcrStatus.value.district };
      param['stateId'] = { inq: this.dcrStatus.value.state };
    }
    else if (this.dcrStatus.value.type === 'Employee Wise') {
      // param['type'] = 'Employee'
      param['userId'] = { inq: this.dcrStatus.value.employee };
    }

    // if (this.currentUser.company.isDivisionExist === true) {
    //   param['divisionId'] = {inq:this.dcrStatus.value.division};
    // }
    // console.log(JSON.stringify(param));

    //----------------------------------mahender sharma--------------------------------------------- 
    console.log("params :--------------------------- ", param);
    this._dcrProviderVisitService.getProvider(param).subscribe(res => {
      console.log(res);

      this.showProcessing = false;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.show = true;
    })
  }

  update(value) {
    this.show = false;
    console.log('test', value);
    let newArr = value.map(x => ({ ...x, showAddGeoLoc: 'yes' }));

    console.log('final', newArr);
    let providerId = []
    value.forEach(element => {
      providerId.push(element.id)
    });

    let obj = {
      showAddGeoLoc: 'yes'
    }

    let where = {
      id: { inq: providerId }
    }

    this._dcrProviderVisitService.updategeoLocationApproval(obj, where).subscribe((res) => {
      Swal.fire(
        'updated sucessfully!',
        'Doctor location status updated sucessfully!',
        'success'
      )
    })

  }
  Remove(value) {
    this.show = false;
    let providerId = []
    let obj={

    }
    value.forEach(element => {
      providerId.push(element.id)
      if(element.hasOwnProperty('showAddGeoLoc')&& element.showAddGeoLoc=='yes'){
        obj['showAddGeoLoc']='no';
      }
    });

    // let valuefilter = value.map(x => x.showAddGeoLoc == 'yes' ? { ...x, showAddGeoLoc: "No" } : null)
    console.log("this value offfff",obj);
   

    let where = {
      id: { inq: providerId }
    }
    this._dcrProviderVisitService.updategeoLocationApproval(obj, where).subscribe((res) => {
      Swal.fire(
        'updated sucessfully!',
        'Doctor location status updated sucessfully!',
        'success'
      )
    })

  }
}



