import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderlocationviweandeditComponent } from './providerlocationviweandedit.component';

describe('ProviderlocationviweandeditComponent', () => {
  let component: ProviderlocationviweandeditComponent;
  let fixture: ComponentFixture<ProviderlocationviweandeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderlocationviweandeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderlocationviweandeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
