import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RCPAReportComponent } from './rcpareport.component';

describe('RCPAReportComponent', () => {
  let component: RCPAReportComponent;
  let fixture: ComponentFixture<RCPAReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RCPAReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RCPAReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
