import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import { UtilsService } from "./../../../../../core/services/utils.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { DcrReportsService } from "./../../../../../core/services/DCR/dcr-reports.service";
import { StateComponent } from "./../../filters/state/state.component";
import { UserCompleteDCRInfoComponent } from "./../user-complete-dcrinfo/user-complete-dcrinfo.component";
import { DivisionComponent } from "./../../filters/division/division.component";
import { TypeComponent } from "./../../filters/type/type.component";
import { MonthComponent } from "./../../filters/month/month.component";
import { EmployeeComponent } from "./../../filters/employee/employee.component";
import { DistrictComponent } from "./../../filters/district/district.component";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { MatSlideToggleChange } from "@angular/material";
import { DcrProviderVisitDetailsService } from "../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service";
import * as moment from "moment";
import { URLService } from '../../../../../core/services/URL/url.service';
declare var $;
@Component({
	selector: "m-pobreport",
	templateUrl: "./pobreport.component.html",
	styleUrls: ["./pobreport.component.scss"],
})
export class POBReportComponent implements OnInit {
	public showState: boolean = true;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	public showDateWiseDCRReport: boolean = false;
	public showProcessing: boolean = false;
	data;
	dataTable: any;
	dtOptions: any;
	public dynamicTableHeader: any = [];
	showAdminAndMGRLevelFilter = false;
	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId1", // the id of html/table element
	};
	exportAsConfigProductDetails: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableProductDetails", // the id of html/table element
	};
	@ViewChild("dataTable") table;
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callUserCompleteDCR")
	callUserCompleteDCR: UserCompleteDCRInfoComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	dataSource: any;
	dataSource2 = [];
	showProviderDetails: boolean = false;
	showProductDetailsReport: boolean = false;
	totalDocpob: number = 0;
	totalvenpob: number = 0;
	totalstockistPob: number = 0;
	totalphoneOrderPob: number = 0;
	totalPob: number = 0;
	constructor(
		private _dcrReportsService: DcrReportsService,
		private _changeDetectorRef: ChangeDetectorRef,
		private toastr: ToastrService,
		private utilService: UtilsService,
		private exportAsService: ExportAsService,
		private _dcrProviderVisitDetailsService: DcrProviderVisitDetailsService,
		private _urlService: URLService
	) {}
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		console.log("toggle", event.checked);
		this.isToggled = event.checked;
	}

	isShowDivision = false;
	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	companyId = this.currentUser.companyId;
	pobForm = new FormGroup({
		reportType: new FormControl("Geographical"),
		type: new FormControl(null, Validators.required),
		toDate: new FormControl(null),
		fromDate: new FormControl(null),
	});

	getReportType(val) {
		if (val === "Geographical") {
			this.showState = true;
			this.showEmployee = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			this.showState = false;
			this.showDistrict = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
	}

	getTypeValue(val) {
		this.showReport = false;
		if (val == "State") {
			this.showDistrict = false;
			this.pobForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.pobForm.removeControl("employeeId");
			this.pobForm.removeControl("designation");
		} else if (val == "Headquarter") {
			this.pobForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.pobForm.setControl(
				"districtInfo",
				new FormControl("", Validators.required)
			);
			this.pobForm.removeControl("employeeId");
			this.pobForm.removeControl("designation");
			this.showDistrict = true;
		} else if (val == "Employee Wise") {
			this.showEmployee = true;
			this.pobForm.removeControl("stateInfo");
			this.pobForm.removeControl("districtInfo");
			this.pobForm.setControl(
				"designation",
				new FormControl("", Validators.required)
			);
			this.pobForm.setControl(
				"employeeId",
				new FormControl("", Validators.required)
			);
		} else if (val == "Self") {
			this.showEmployee = false;
		}

		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
		} else {
			this.isShowDivision = false;
		}

		this.pobForm.patchValue({ type: val });
		this._changeDetectorRef.detectChanges();
	}

	getStateValue(val) {
		this.pobForm.patchValue({ stateInfo: val });
		if (this.isDivisionExist === true) {
			if (this.pobForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.designationLevel,
					companyId: this.companyId,
					stateId: this.pobForm.value.stateInfo,
					isDivisionExist: this.isDivisionExist,
					division: this.pobForm.value.divisionId,
				});

				//this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
			}
		} else {
			if (this.pobForm.value.type == "Headquarter") {
				console.log('this',val);
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
	}

	getDistrictValue(val) {
		this.pobForm.patchValue({ districtInfo: val });
	}
	getEmployeeValue(val) {
		this.pobForm.patchValue({ employeeId: val });
	}
	getFromDate(val) {
		this.pobForm.patchValue({ fromDate: val });
	}
	getToDate(val) {
		this.pobForm.patchValue({ toDate: val });
	}

	getDesignation(val) {
		this.pobForm.patchValue({ designation: val });
		if (this.isDivisionExist === true) {
			this.callEmployeeComponent.getEmployeeListBasedOnDivision({
				companyId: this.companyId,
				division: this.pobForm.value.divisionId,
				status: [true],
				designationObject: this.pobForm.value.designation,
			});
		} else {
			this.callEmployeeComponent.getEmployeeList(
				this.currentUser.companyId,
				val,
				[true]
			);
		}
	}
	//setting division into pobForm
	getDivisionValue($event) {
		this.pobForm.patchValue({ divisionId: $event });
		if (this.isDivisionExist === true) {
			if (
				this.pobForm.value.type === "State" ||
				this.pobForm.value.type === "Headquarter"
			) {
				this.callStateComponent.getStateBasedOnDivision({
					companyId: this.currentUser.companyId,
					isDivisionExist: this.isDivisionExist,
					division: this.pobForm.value.divisionId,
					designationLevel: this.currentUser.userInfo[0]
						.designationLevel,
				});
			}

			if (this.pobForm.value.type === "Employee Wise") {
				//getting employee if we change the division filter after select the designation
				if (
					this.pobForm.value.divisionId != null &&
					this.pobForm.value.designation != null
				) {
					if (this.pobForm.value.designation.length > 0) {
						this.callEmployeeComponent.getEmployeeListBasedOnDivision(
							{
								companyId: this.companyId,
								division: this.pobForm.value.divisionId,
								status: [true],
								designationObject: this.pobForm.value
									.designation,
							}
						);
					}
				}
			}
		}
	}

	ngOnInit() {
		//this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.pobForm.setControl("divisionId", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};

			if (this.currentUser.userInfo[0].rL > 1) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}

		if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			this.showAdminAndMGRLevelFilter = true;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		}
	}

	showDCRSummary = true;

	viewRecords() {
		this.isToggled = false;
		this.showReport = false;
		this.showDateWiseDCRReport = false;
		this.showDCRSummary = true;
		this.showProcessing = true;
		if (this.pobForm.value.employeeId == null) {
			this.pobForm.value.employeeId = [this.currentUser.id];
		}

		let params = {};
		if (this.isDivisionExist === true) {
			params["division"] = this.pobForm.value.divisionId;
		} else {
			params["division"] = [];
		}

		params["isDivisionExist"] = this.isDivisionExist;
		params["companyId"] = this.companyId;
		params["stateIds"] = this.pobForm.value.stateInfo;
		params["districtIds"] = this.pobForm.value.districtInfo;
		params["type"] = this.pobForm.value.type;
		params["employeeId"] = this.pobForm.value.employeeId;
		params["fromDate"] = this.pobForm.value.fromDate;
		params["toDate"] = this.pobForm.value.toDate;

		console.log("params : ",params);
		this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(
			this.pobForm.value.fromDate,
			this.pobForm.value.toDate
		);
		this._dcrReportsService.getPobDetails(params).subscribe((res) => {
			console.log("res :",res);
			if (res.length == 0 || res == undefined) {
				this.showProcessing = false;
				this.toastr.info(
					"For Selected Filter data not found",
					"Data Not Found"
				);
			} else {
				this.showProcessing = false;
				this.showReport = true;
				this.showProviderDetails = false;
				this.showProductDetailsReport = false;
				res.sort(function (a, b) {
					let textA = a.userName.toUpperCase();
					var textB = b.userName.toUpperCase();
					return textA < textB ? -1 : textA > textB ? 1 : 0;
				});
				this.totalDocpob = 0;
				this.totalvenpob = 0;
				this.totalstockistPob = 0;
				this.totalPob=0;
				this.totalphoneOrderPob = 0;
				res.forEach((element) => {
					element.pob.forEach((pob) => {
						this.totalDocpob = pob.docpob + this.totalDocpob;
						this.totalvenpob = pob.venpob + this.totalvenpob;
						this.totalstockistPob =
							pob.stockistPob + this.totalstockistPob;
						this.totalphoneOrderPob =
							pob.phoneOrderPob + this.totalphoneOrderPob;
							
					});
				});
				this.data = res;
				this.totalPob=this.totalphoneOrderPob+this.totalvenpob+this.totalDocpob;
				this._changeDetectorRef.detectChanges();
			}
		});
	}

	getProviderDetail(type, userId, month, year, phoneOrder) {
		this.showProviderDetails = false
		// let fromDateMonth = new Date(this.pobForm.value.fromDate).getMonth() + 1;
		let toDateMonth = new Date(this.pobForm.value.toDate).getMonth() + 1;
		let startOfMonth;
		let endOfMonth;
		if (month == toDateMonth) {
			startOfMonth = moment()
				.month(`${month - 1}`)
				.startOf("month")
				.format("YYYY-MM-DD");
			endOfMonth = this.pobForm.value.toDate;
		} else {
			startOfMonth = moment()
				.month(`${month - 1}`)
				.startOf("month")
				.format("YYYY-MM-DD");
			endOfMonth = moment()
				.month(`${month - 1}`)
				.endOf("month")
				.format("YYYY-MM-DD");
		}
		this._dcrProviderVisitDetailsService
			.getProviderVisitDetails(
				this.currentUser.companyId,
				"user wise",
				this.pobForm.value.stateInfo,
				this.pobForm.value.districtInfo,
				[userId],
				startOfMonth,
				endOfMonth,
				type,
				this.currentUser.company.validation.unlistedDocCall,
				this.currentUser.company.isDivisionExist,
				[],
				""
			)
			.subscribe((res) => {
				console.log("..data found..", res);
				if (res == null || res.length == 0 || res == undefined) {
					this.showProcessing = false;
					this.showReport = false;
					this.toastr.info("Data Not Found", "");
				} else {
					res.map((item, index) => {
						item["index"] = index;
					});
					this.dataSource = res;
					this.showReport = true;
					this.showProcessing = false;
					this.showProviderDetails = true;

					this.dtOptions = {
						pagingType: "full_numbers",
						paging: true,
						ordering: true,
						info: true,
						scrollY: 300,
						scrollX: true,
						fixedColumns: true,
						destroy: true,
						data: this.dataSource,

						columns: [
							{
								title: "S. No.",
								data: "index",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data + 1;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Division",
								data: "divisionName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "State Name",
								data: "stateName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title:
									this.currentUser.company.lables.hqLabel +
									" Name",
								data: "districtName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title:
									this.currentUser.company.lables.areaLabel +
									" Name",
								data: "blockName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "User Name",
								data: "userName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},

							{
								title:
									this.currentUser.company.lables
										.doctorLabel + " Name",
								data: "providerName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Category",
								data: "category",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Product Pob",
								data: "productPob",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										if (data > 0) {
											return `<button class='getPOBDetails btn btn-outline-success' style="color: #3f51b5; cursor:pointer;font-weight: 600;    border: none !important;
											padding: 0.5rem 1.15rem !important;
											font-size: 1.19rem !important;
											line-height: 1.25 !important; 
											border-radius: .25rem !important;
											transition: color 0.5s cubic-bezier(0.31, 0.76, 0.17, 0.01), background-color 0.5s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out !important;">${data}</button>`;
										} else {
											return data;
										}
									}
								},
								defaultContent: "---",
							},
							{
								title: "Total Visit",
								data: "totalVisit",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Visit Dates",
								data: "visitDates",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},

							{
								title: "Discussion",
								data: "providerDiscussion",
								render: function (data) {
									if (
										data === "" ||
										data == "null" ||
										data == undefined
									) {
										return "---";
									} else {
										return data;
									}
								},
							},
						],
						rowCallback: (
							row: Node,
							data: any[] | Object,
							index: number
						) => {
							const self = this;
							$("td", row).unbind("click");
							$("td .getPOBDetails", row).bind("click", () => {
								let rowObject = JSON.parse(
									JSON.stringify(data)
								);
								self.getProductDetails(rowObject.dcrId);
							});
							return row;
						},
						dom: "Bfrtip",
						buttons: [
							{
								extend: "excel",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "copy",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "csv",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "print",
								title: function () {
									return "State List";
								},
								exportOptions: {
									columns: ":visible",
								},
							},
						],
					};
					this._changeDetectorRef.detectChanges();
					if (this.currentUser.company.isDivisionExist == false) {
						this.dtOptions.columns[1].visible = false;
					}

					this.dataTable = $(this.table.nativeElement);
					this.dataTable.DataTable(this.dtOptions);
				}
			});
	}
	exporttoExcel() {
		// download the file using old school javascript method
		this.exportAsService
			.save(this.exportAsConfig, "POB Report")
			.subscribe(() => {
				// save started
			});
	}


	onPreviewClick(){
   
		let printContents, popupWin;
	  
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;
		printContents = document.getElementById('tabledivId1').innerHTML;
		const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
	
		popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		popupWin.document.open();
		popupWin.document.write(`
		<html>
	  <head>
	  <style>
	  img {
	  float: left;
	  }
	
	  .flex-container {
	  display: flex;
	  justify-content: space-between;
	  }
	
	  .tg {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	  .tg td {
	  font-family: Arial, sans-serif;
	  font-size: 14px;
	  padding: 6px 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  }
	
	  table {
		border-collapse: collapse;


			align:right;
	  
		  
	}
	
	
	
	  .tg .tg-lboi {
	  border-color: inherit;
	  text-align: left;
	  vertical-align: middle;
	
	  }
	
	  .tg .tg-u6fn {
	  font-weight: bold;
	  font-size: 18px;
	  background-color: #efefef;
	  color:black;
	  border-color: inherit;
	  text-align: left;
	  vertical-align: top;
	  padding-top: 0px;
	  padding-bottom: 0px;
	  }
	
	  .tg .tg-yz93 {
	  border-color: inherit;
	  text-align: right;
	  vertical-align: middle
	  }
	
	  .tb2 {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  margin-top: 10px;
	  }
	
	   td {
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  border-color: black;
	  width:300px;
	  }
	
	  th {
		background-color:#AAB7B8;
	  font-family: Arial, sans-serif;
	  font-size: 12px;
	  font-weight: normal;
	  padding: 2px;
	  border-style: solid;
	  border-width: 1px;
	  overflow: hidden;
	  word-break: normal;
	  width:300px;
	  
	  }
	
	  .tb2 .tg-oiyu {
	  font-size: 10px;
	  text-align: left;
	  vertical-align: middle
	  }
	  .tg-bckGnd {
	  font-weight: bold;
	  background-color: #efefef;
	  }
	
	
	  .tb2 .tg-3ppa {
	  font-size: 10px;
	  background-color: #efefef;
	  color:black;
	  text-align: left;
	  vertical-align: middle;
	  padding-top: 2px;
	  padding-bottom: 2px;
	  }
	
	  .footer {
	  position: sticky;
	  left: 0;
	  bottom: 0;
	  width: 100%;
	  background-color: #efefef;
	  color: rgb(2, 2, 2);
	  text-align: right
	  }
	
	
	
	  button {
	  background-color: #008CBA;
	  border: none;
	  color: white;
	  margin: 5px 0px;
	  padding: 2px 10px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 15px;
	  border-radius: 12px;
	  cursor: pointer;
	  outline: none;
	  }
	
	  @media print {
	  @page { size: A4 landscape;
		max-height:100%;
		max-width:100%}
	
	  .footer {
	  position: relative;
	  }
	  tr {
	  page-break-inside: avoid;
	  }
	
	  button{
	  display : none;
	  }
	  }
	  </style>
	  </head>
	  <body onload="window.print();window.close()" style="padding-bottom:30px">
	  <div class="flex-container">
	  <div style="width: 40%; ">
	  <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
	  <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
	
	  </div>
	
	  <div style="text-align: right;">
	  <h2 style="margin-top: 0px; margin-bottom: 5px">POB Report</h2>
	  <div>
	  <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
	  <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
	  </div>
	  </div>
	  </div>
	  ${printContents}</body>
	  </html>
	  `
		);
	   // popupWin.document.close();
	 }



	exportProductDetails() {
		// download the file using old school javascript method
		this.exportAsService
			.save(this.exportAsConfigProductDetails, "POB Product Details")
			.subscribe(() => {
				// save started
			});
	}
	getProductDetails(dcrId) {
		this.showProcessing = true;
		this._dcrProviderVisitDetailsService
			.getProviderVisitDetailsUsingDcrId(dcrId)
			.subscribe((res: any) => {
				if (res.length > 0) {
					this.showProductDetailsReport = true;
					this.dataSource2 = res;
					this.showProcessing = false;
					this._changeDetectorRef.detectChanges();
				} else {
					this.showProcessing = false;
					this.toastr.info("Data Not Found", "");
				}
			});
	}
}
