import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { POBReportComponent } from './pobreport.component';

describe('POBReportComponent', () => {
  let component: POBReportComponent;
  let fixture: ComponentFixture<POBReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ POBReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(POBReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
