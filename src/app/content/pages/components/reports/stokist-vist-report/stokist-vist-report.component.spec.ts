import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StokistVistReportComponent } from './stokist-vist-report.component';

describe('StokistVistReportComponent', () => {
  let component: StokistVistReportComponent;
  let fixture: ComponentFixture<StokistVistReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StokistVistReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StokistVistReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
