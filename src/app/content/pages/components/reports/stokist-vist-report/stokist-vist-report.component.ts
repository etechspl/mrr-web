import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggle } from '@angular/material';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StateComponent } from '../../filters/state/state.component';
import { TypeComponent } from '../../filters/type/type.component';

@Component({
  selector: 'm-stokist-vist-report',
  templateUrl: './stokist-vist-report.component.html',
  styleUrls: ['./stokist-vist-report.component.scss']
})
export class StokistVistReportComponent implements OnInit {

  isToggled : boolean = true;
  currentUser=JSON.parse(sessionStorage.currentUser);
  isDivisionExist:boolean=false;
  showState : boolean =false;
  showHq : boolean = false;
  showEmployee : boolean = false;
  constructor() { }


  @ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;

  stokistForm = new FormGroup({
    reportType : new FormControl('Geographical'),
    type: new FormControl(null,Validators.required),
    division : new FormControl(null),
    state: new FormControl(null),
    district : new FormControl(null),
    designation: new FormControl(null),
    employeeName : new FormControl(null),
    toDate : new FormControl(null, Validators.required),
    fromDate : new FormControl(null,Validators.required)
  });
  
  toggleFilter(event: MatSlideToggle){
    this.isToggled = event.checked;
  }

  ngOnInit() {
    console.log('current user',this.currentUser);
    
    if(this.currentUser.company.isDivisionExist==true){
      this.isDivisionExist=true;
    }

    if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			// this.showAdminAndMGRLevelFilter = true;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		}
		if (this.currentUser.company.isDivisionExist == true) {
			this.stokistForm.setControl("division", new FormControl());

			let obj = {
				companyId: this.currentUser.companyId,
				userId: this.currentUser.id,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			this.callDivisionComponent.getDivisions(obj);
			this.isDivisionExist = true;
		}
  }

  getReportType(val){
    this.stokistForm.patchValue({reportType : val});
    this.callDivisionComponent.setBlank();
		this.stokistForm.patchValue({ type: "" });

		if (val === "Geographical") {
			this.showState = false;
			this.showHq = false;
			this.showEmployee = false;
			if (this.currentUser.company.isDivisionExist == false) {
				this.isDivisionExist = false;
			} else if (this.currentUser.company.isDivisionExist == true) {
				this.isDivisionExist = true;
				this.callDivisionComponent.setBlank();
			}
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.showEmployee = true;
			this.showState = false;
			this.showHq = false;
			this.isDivisionExist = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
  }

  getTypeValue(val){
    this.stokistForm.patchValue({type :val})
    if(val=='State'){
      this.showState=true;
      this.showHq=false;
      this.showEmployee=false;
    }
    else if(val=='Headquarter'){
      this.showState=true;
      this.showHq=true;
      this.showEmployee=false;
    }else if(val == 'Employee Wise'){
      this.showState=false;
      this.showHq=false;
      this.showEmployee=true;
    }
  }

  getDivisionValue(val){
    this.stokistForm.patchValue({division : val})
  }

  getStateValue(val){
    this.stokistForm.patchValue({state:val})

    this.callDistrictComponent.setBlank();

		if (this.stokistForm.value.type == "Headquarter") {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					division: this.stokistForm.value.division,
					companyId: this.currentUser.companyId,
					stateId: val,
				};
				this.callDistrictComponent.getDistrictsBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
  }

  getDistrictValue(val){
    this.stokistForm.patchValue({district : val})
  }

  getDesignation(val){
    this.stokistForm.patchValue({designation : val})

    this.callEmployeeComponent.setBlank();

		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.currentUser.company.isDivisionExist == true) {
				let passingObj = {
					companyId: this.currentUser.companyId,
					designationObject: val,
					status: [true],
					division: this.stokistForm.value.division,
				};
				this.callEmployeeComponent.getEmployeeListBasedOnDivision(
					passingObj
				);
			} else if (this.currentUser.company.isDivisionExist == false) {
				this.callEmployeeComponent.getEmployeeList(
					this.currentUser.companyId,
					val,
					[true]
				);
			}
		} else {
			let desig = [];
			for (let i = 0; i < val.length; i++) {
				desig.push(val[i].designationLevel);
			}
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: desig,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			} else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: desig,
					isDivisionExist: true,
					division: this.stokistForm.value.division,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}

			// this.callEmployeeComponent.getManagerHierarchy(this.currentUser.companyId, val[0].designationLevel, [true]);
		}
  }

  getEmployeeValue(val){
    this.stokistForm.patchValue({EmployeeName : val})
  }

  getFromDate(val){
    this.stokistForm.patchValue({fromDate : val})
  }

  getToDate(val){
    this.stokistForm.patchValue({toDate : val})
  }

  viewRecords(){
    console.log(this.stokistForm.value);
    
  }
}
