import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerwiseSSSReportComponent } from './managerwise-sssreport.component';

describe('ManagerwiseSSSReportComponent', () => {
  let component: ManagerwiseSSSReportComponent;
  let fixture: ComponentFixture<ManagerwiseSSSReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerwiseSSSReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerwiseSSSReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
