import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { StatusComponent } from '../../filters/status/status.component';
import { Designation } from '../../../_core/models/designation.model';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { MatTableDataSource, MatTable, MatSort, MatPaginator, _MatChipListMixinBase, MatSlideToggleChange } from '@angular/material';
import { DcrProviderVisitDetailsService } from '../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
import * as moment from 'moment';
import { DivisionComponent } from '../../filters/division/division.component';
import { PrimaryAndSaleReturnService } from '../../../../../core/services/PrimaryAndSaleReturn/primary-and-sale-return.service';
import { UtilsService } from '../../../../../core/services/utils.service';
import { of } from 'rxjs';
import { mergeMap, groupBy, reduce } from 'rxjs/operators';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
//import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
declare var $;

@Component({
  selector: 'm-managerwise-sssreport',
  templateUrl: './managerwise-sssreport.component.html',
  styleUrls: ['./managerwise-sssreport.component.scss']
})
export class ManagerwiseSSSReportComponent implements OnInit {

  mgrJointForm: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  @ViewChild('mgrDataTableOBJ1') table1;
  mgrDataTableOBJ1: any;
  mgrDataTableOptions1: any;

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementId: 'tableId1', // the id of html/table element
  }

  constructor(
    private _toastrService: ToastrService,
    private _fb: FormBuilder,
    private utilService: UtilsService,
    private _primarySaleReturn: PrimaryAndSaleReturnService,
    private _changeDetectorRef: ChangeDetectorRef,
    private exportAsService: ExportAsService
  ) {
    this.createForm(this._fb);
  }

  check: boolean = false;
  isStatus: boolean = true;
  isMultipleEmp: boolean = true;
  data: any;
  showProcessing1: boolean = false;
  isShowVisitedReport: boolean = false;
  isShowReport: boolean = false;
  showProcessing: boolean = false;

  //, 'workedWith', 'district', 'area', 'visits'
  displayedColumns = ['sn', 'dcrDate', 'workedWith', 'district', 'visitedArea', 'visits'];
  displayedColumnsProviders = ['sn', 'providerName', 'providerCode', 'providerType', 'status'];
  dataSource = new MatTableDataSource<any>();
  prodataSource = new MatTableDataSource<any>();
  showDivisionFilter: boolean = false;

  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist == true) {

      this.mgrJointForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  createForm(_fb: FormBuilder) {
    this.mgrJointForm = _fb.group({
      type: new FormControl('monthly', Validators.required),
      designation: new FormControl('', Validators.required),
      status: new FormControl(true),
      userId: new FormControl('', Validators.required),
      fromDate: new FormControl('', Validators.required),
      toDate: new FormControl('', Validators.required)
    })
  }

  getDivisionValue(val) {
    this.showProcessing = false;
    this.mgrJointForm.patchValue({ division: val });
    //=======================Clearing Filter===================

    this.callDesignationComponent.setBlank();
    this.callStatusComponent.setBlank();
    this.callEmployeeComponent.setBlank();
    //===============================END================================

    let passingObj = {
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      division: val
    }

    this.callDesignationComponent.getDesignationBasedOnDivisionOnlyManager(passingObj);
  }

  getDesignationList() {
    this.isShowReport = false;

    this.callDesignationComponent.getManagersDesignation();
  }

//   getDesignationLevel(designation: Designation) {
//     this.isShowReport = false;
//     //=======================Clearing Filter===================
//     this.callStatusComponent.setBlank();
//     this.callEmployeeComponent.setBlank();
//     //===============================END================================
//     this.mgrJointForm.patchValue({ designation: designation.designationLevel })
//   }

  getDesignationLevel(designation: Designation) {
    if(designation.designation === "MR"){
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: this.mgrJointForm.value.division
      }
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      this._toastrService.error("This report is for Managers only", "Can't Select MR");
      this.callDesignationComponent.setBlank();
    } else {
      this.isShowReport = false;
      //=======================Clearing Filter===================
      this.callStatusComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      this.mgrJointForm.patchValue({ designation: designation.designationLevel })
  if (this.currentUser.userInfo[0].rL === 0) {
    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        designationObject: [designation],
        status: [true],
        division: this.mgrJointForm.value.division,
      };
  
      this.callEmployeeComponent.getEmployeeListBasedOnDivision(
        passingObj
      );
    } else if (this.currentUser.company.isDivisionExist == false) {
      this.callEmployeeComponent.getEmployeeList(
        this.currentUser.companyId,
        [designation],
        [true]
      );
    }
  } else {
    let dynamicObj = {};
    if (this.currentUser.company.isDivisionExist == false) {
      dynamicObj = {
        supervisorId: this.currentUser.id,
        companyId: this.currentUser.companyId,
        status: true,
        type: "lower",
        designation: [designation.designationLevel], //desig
      };
      this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
    } else if (this.currentUser.company.isDivisionExist == true) {
      dynamicObj = {
        supervisorId: this.currentUser.id,
        companyId: this.currentUser.companyId,
        status: true,
        type: "lower",
        designation: [designation.designationLevel], //desig,
        isDivisionExist: true,
        division: this.mgrJointForm.value.division,
      };
      this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
    }
  }
    }

}

  getStatus(status: boolean) {
    this.isShowReport = false;

    this.mgrJointForm.patchValue({ status: status });
    //=======================Clearing Filter===================
    this.callEmployeeComponent.setBlank();
    //===============================END================================
    if (this.currentUser.company.isDivisionExist == true) {
      let passingObj = {
        companyId: this.currentUser.companyId,
        designationObject: this.mgrJointForm.value.designation,
        status: status,
        division: this.mgrJointForm.value.division
      }
      this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.mgrJointForm.value.designation, status);
    }





  }

  getEmployee(emp: string) {
    this.isShowReport = false;
    this.mgrJointForm.patchValue({ userId: emp })
  }

  getFromDate(fromDate) {
    this.isShowReport = false;

    this.mgrJointForm.patchValue({ fromDate: fromDate })
  }

  getToDate(toDate) {
    this.isShowReport = false;

    this.mgrJointForm.patchValue({ toDate: toDate })
  }
  dynamicTableHeader;
  openingtot = 0;
  returntot = 0;
  reciepttot = 0;
  secSaletot = 0;
  closetot = 0;
  grandTot = [];
  monthARRAY;
  generateReport() {
	  console.log("form details...",this.mgrJointForm.value)
    this.isToggled = false;
    this.dynamicTableHeader = this.utilService.monthNameAndYearBTWTwoDate(this.mgrJointForm.value.fromDate, this.mgrJointForm.value.toDate);
    if (!this.mgrJointForm.valid) {
      this._toastrService.error("Select the required field to generate the report", "All Field required");
      return;
    }
    let params = {
      companyId: this.currentUser.companyId,
      type: "lower",
      supervisorId: this.mgrJointForm.value.userId,
      fromDate: this.mgrJointForm.value.fromDate,
      toDate: this.mgrJointForm.value.toDate
    }
    this.showProcessing = true;
    this.isShowReport = false;
    this._primarySaleReturn.managerwiseSSS(params).subscribe(res => {
      this.monthARRAY = this.monthNameAndYearBTWTwoDate(this.mgrJointForm.value.fromDate, this.mgrJointForm.value.toDate);
      this.data = res;
      if (this.data.length > 0) {
        this.showProcessing = false;
        this.isShowReport = true;
        this._changeDetectorRef.detectChanges();
      } else {
        alert('No records found for selected details !!')
        this._changeDetectorRef.detectChanges();
      }
    });
  }

  getSaleValue(val, month) {
    return ''
  }

  monthNameAndYearBTWTwoDate(fromDate, toDate) {

    let monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    let arr = [];
    let datFrom = new Date(fromDate);
    let datTo = new Date(toDate);
    let fromYear = datFrom.getFullYear();
    let toYear = datTo.getFullYear();
    let diffYear = (12 * (toYear - fromYear)) + datTo.getMonth();

    for (let i = datFrom.getMonth(); i <= diffYear; i++) {
      arr.push({
        monthName: monthNames[i % 12],
        month: (i % 12) + 1,
        year: Math.floor(fromYear + (i / 12)),
        endDate: parseInt(moment(new Date(Math.floor(fromYear + (i / 12)), (i % 12), 1)).endOf("month").format('DD'))

      });


    }

    return arr;
  }

  exporttoExcel() {
    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'Managerwise SSS Details').subscribe(() => {
      // save started
    });
  }

  getGrandTotalForopening(month) {
    const array1 = [].concat(
      ...this.data.map(d => d.data)
    );
    return array1.reduce((accumulator, currentValue) => {
      return (
        accumulator + (currentValue.month == month.month ? currentValue.opening : 0)
      );
    }, 0);
  }
  getGrandTotalForsaleReturn(month) {
    const array1 = [].concat(
      ...this.data.map(d => d.data)
    );
    return array1.reduce((accumulator, currentValue) => {
      return (
        accumulator + (currentValue.month == month.month ? currentValue.totalSaleReturnAmt : 0)
      );
    }, 0);
  }
  getGrandTotalForPrimary(month) {
    const array1 = [].concat(
      ...this.data.map(d => d.data)
    );
    return array1.reduce((accumulator, currentValue) => {
      return (
        accumulator + (currentValue.month == month.month ? currentValue.totalPrimaryAmt : 0)
      );
    }, 0);
  }
  getGrandTotalForSales(month) {
    const array1 = [].concat(
      ...this.data.map(d => d.data)
    );
    return array1.reduce((accumulator, currentValue) => {
      return (
        accumulator + (currentValue.month == month.month ? currentValue.totalSecondrySaleAmt : 0)
      );
    }, 0);
  }
  getGrandTotalForClosing(month) {
    const array1 = [].concat(
      ...this.data.map(d => d.data)
    );
    return array1.reduce((accumulator, currentValue) => {
      return (
        accumulator + (currentValue.month == month.month ? currentValue.closingAmt : 0)
      );
    }, 0);
  }


}
