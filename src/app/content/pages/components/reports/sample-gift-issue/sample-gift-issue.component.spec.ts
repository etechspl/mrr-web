import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleGiftIssueComponent } from './sample-gift-issue.component';

describe('SampleGiftIssueComponent', () => {
  let component: SampleGiftIssueComponent;
  let fixture: ComponentFixture<SampleGiftIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleGiftIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleGiftIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
