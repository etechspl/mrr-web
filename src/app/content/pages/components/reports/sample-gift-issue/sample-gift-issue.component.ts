import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { GiftService } from '../../../../../core/services/Gift/gift.service';
import { SampleService } from '../../../../../core/services/sample/sample.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatSlideToggleChange } from '@angular/material';
import { StateComponent } from '../../filters/state/state.component';
import * as _moment from "moment";

declare var $;

@Component({
  selector: 'm-sample-gift-issue',
  templateUrl: './sample-gift-issue.component.html',
  styleUrls: ['./sample-gift-issue.component.scss']
})
export class SampleGiftIssueComponent implements OnInit {

  showStateReportType = true;
  showEmpReportType = true;
  showHqReportType = true;
  moduleType = true;
  stateFilter = false;
  designationFilterForUserwiseOnly = false;
  districtFilter = false;
  employeeFilter = false;
  designationFilter = false;
  districtFilterForMulti = false;
  designationFilterForMgr = false;
  showMgrSelfReportType=false;
  designationData = [];
  isShowIssueSample = false;
  @ViewChild('dataTable') dataTable;
  dtOptions: any;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDistrictComponentAgainForMulti") callDistrictComponentAgainForMulti: DistrictComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild('callStateComponent') callStateComponent : StateComponent;

  constructor(
    private fb: FormBuilder,
    private _toastr: ToastrService,
    private userDetailsService: UserDetailsService,
    private _samplesService: SampleService,
    private _giftService: GiftService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { }
  isToggled = true; 
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    this.reportType('Employeewise');
    if (this.currentUser.userInfo[0].rL === 1) {
      this.setEmployeeDetail([this.currentUser.userInfo[0].userId]);
    }
  }

  displayedColumns = ['sno', 'stateName', 'districtName', 'userName', 'productName', 'opening', 'receipt', 'balance', 'issueDate'];
  currentUser = JSON.parse(sessionStorage.currentUser);
  viewSampleAndGift = new FormGroup({
    reportType: new FormControl(null, Validators.required),
    stateInfo: new FormControl(null, Validators.required),
    districtInfo: new FormControl(null),
    userInfo: new FormControl(null),
    designation: new FormControl(null, Validators.required),
    moduleTypeName: new FormControl(null, Validators.required),
    month: new FormControl(null, Validators.required),
    year: new FormControl(null, Validators.required),
  })

  reportType(val) {
    this.isShowIssueSample = false;
    this.viewSampleAndGift.patchValue({ reportType: val });
    if (this.currentUser.userInfo[0].rL === 0) {
      this.designationFilterForMgr = false;
      this.showMgrSelfReportType=false;
      if (val === 'Employeewise') {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = true;
        this.districtFilterForMulti = false;
        this.designationFilter = false;
        this.designationFilterForUserwiseOnly = true;
        this.viewSampleAndGift.patchValue({ designation: '' });
        this.viewSampleAndGift.patchValue({ userInfo: '' });
        this.callStateComponent.setBlank();
        this.viewSampleAndGift.patchValue({stateInfo: ''});
        this.viewSampleAndGift.removeControl('districtInfo');
        this.viewSampleAndGift.patchValue({ districtInfo: '' });
        this.viewSampleAndGift.setControl('userInfo', new FormControl('', Validators.required))
      } else if (val === 'Statewise') {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.districtFilterForMulti = false;
        this.designationFilter = true;
        this.designationFilterForUserwiseOnly = false;
        this.callStateComponent.setBlank();
        this.viewSampleAndGift.patchValue({stateInfo: ''});
        this.viewSampleAndGift.patchValue({ userInfo: '' });
        this.viewSampleAndGift.removeControl('userInfo');
        this.viewSampleAndGift.removeControl('districtInfo');
      } else if (val === 'Headquarterwise') {
        this.stateFilter = true;
        this.districtFilter = false;
        this.employeeFilter = false;
        this.districtFilterForMulti = true;
        this.designationFilter = true;
        this.designationFilterForUserwiseOnly = false;
        this.callStateComponent.setBlank();
        this.viewSampleAndGift.removeControl('userInfo');
        this.viewSampleAndGift.patchValue({ userInfo: '' });
        this.viewSampleAndGift.setControl('districtInfo', new FormControl('', Validators.required))
      }
    } else if (this.currentUser.userInfo[0].rL == 1) {
      this.showStateReportType = false;
      this.showEmpReportType = false;
      this.showHqReportType = false;
      this.stateFilter = false;
      this.districtFilter = false;
      this.employeeFilter = false;
      this.districtFilterForMulti = false;
      this.designationFilter = false;
      this.designationFilterForUserwiseOnly = false;
      this.designationFilterForMgr = false;
      this.showMgrSelfReportType=false;
    } else if (this.currentUser.userInfo[0].rL > 1) {
      this.designationFilterForMgr = true;
      this.showMgrSelfReportType=true;
      this.showStateReportType = false;
      this.showEmpReportType = true;
      this.showHqReportType = false;
      this.stateFilter = false;
      this.districtFilter = false;
      this.employeeFilter = true;
      this.districtFilterForMulti = false;
      this.designationFilter = false;
      this.designationFilterForUserwiseOnly = false;
    }
    this.viewSampleAndGift.patchValue({ reportType: val })
  }

  setEmployeeDetail(val) {
    this.isShowIssueSample = false;
    this.viewSampleAndGift.patchValue({ userInfo: val })
  }

  moduleSubmitType(val) {
    this.isShowIssueSample = false;
    this.viewSampleAndGift.patchValue({ moduleTypeName: val })
  }

  getMonth(val) {
    this.isShowIssueSample = false;
    this.viewSampleAndGift.patchValue({ month: val });
  }
  getYear(val) {
    this.isShowIssueSample = false;
    this.viewSampleAndGift.patchValue({ year: val });
  }

  getStateValue(val) {
    this.isShowIssueSample = false;
    let state = [];
    state.push(val);
    // this.callEmployeeComponent.setBlank();
    this.viewSampleAndGift.patchValue({ stateInfo: state });
    this.callDistrictComponent.getDistricts(this.currentUser.companyId, val,true);
    if (this.viewSampleAndGift.value.reportType == 'Headquarterwise') {
      this.callDistrictComponentAgainForMulti.getDistricts(this.currentUser.companyId, val,true);
    } else if (this.viewSampleAndGift.value.reportType == 'Statewise' || this.viewSampleAndGift.value.reportType == 'Employeewise') {
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, val, this.viewSampleAndGift.value.districtInfo, this.viewSampleAndGift.value.reportType).subscribe(res => {
        this.designationData = res;
      });
    }
  }

  getEmployeeValue(val) {
    this.isShowIssueSample = false;
    let district = [];
    if (this.viewSampleAndGift.value.reportType == 'Employeewise') {
      this.viewSampleAndGift.patchValue({ designation: val });
      this.callEmployeeComponent.getMgrList(this.currentUser.companyId, this.viewSampleAndGift.value.stateInfo, val, [true]);
    } else if (this.viewSampleAndGift.value.reportType == 'Headquarterwise') {
      for (let i = 0; i < val.length; i++) {
        district.push(val[i]);
      }
      this.viewSampleAndGift.patchValue({ districtInfo: district });
      this.userDetailsService.getDesignationValue(this.currentUser.companyId, this.viewSampleAndGift.value.stateInfo, this.viewSampleAndGift.value.districtInfo, this.viewSampleAndGift.value.reportType).subscribe(res => {
        this.designationData = res;
      });
    }
  }

  getMgrEmployee(val) {
    this.isShowIssueSample = false;
    this.viewSampleAndGift.patchValue({ designation: val.designationLevel });
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, [true]);
  }

  mgrSelf(val){
    this.isShowIssueSample = false;
    this.designationFilterForMgr=false;
    this.employeeFilter=false;
    this.setEmployeeDetail([this.currentUser.userInfo[0].userId]);
  }

  dataSource = [];
  viewSampleAndGiftDetail() {
    this.isToggled = false;
    this.isShowIssueSample = false;
    if (this.viewSampleAndGift.value.moduleTypeName == 'sample') {
      this._samplesService.getSampleGiftIssueDetail(this.currentUser.companyId, this.viewSampleAndGift.value, this.currentUser.userInfo[0].rL).subscribe(getIssueDetail => {
        if (getIssueDetail.length > 0) {
          this.dataSource = getIssueDetail;
          this.isShowIssueSample = true;
          this._changeDetectorRef.detectChanges();
          //--------------------add data table----------------
          this.dtOptions = {
            destroy: true,
            data: this.dataSource,
            columns: [
              {
                title: 'State Name',
                    data: 'stateName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Headquarter Name',
                    data: 'districtName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'User Name',
                    data: 'userName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: this.viewSampleAndGift.value.moduleTypeName,
                    data: 'productName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Product Code',
                    data: 'productCode',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Receipt',
                    data: 'receipt',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Balance',
                    data: 'balance',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Issued Date',
                    data: 'issueDate',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              }
            ],
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'print'
            ]
          };
          this.dataTable = $(this.dataTable.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          //----------------------------------------------
        } else {
          this._changeDetectorRef.detectChanges();
          this._toastr.success("Please select another details", "No Samples Found for selected Details !!");
        }
      }, err => {
      });
    } else if (this.viewSampleAndGift.value.moduleTypeName == 'gift') {
      this.isShowIssueSample = false;
      this._samplesService.getSampleGiftIssueDetail(this.currentUser.companyId, this.viewSampleAndGift.value, this.currentUser.userInfo[0].rL).subscribe(getGift => {
        if (getGift.length > 0) {
          this.dataSource = getGift;
          this.isShowIssueSample = true;
          this._changeDetectorRef.detectChanges();

          //----------add data table------------------//
          this.dtOptions = {
            destroy: true,
            data: this.dataSource,
            columns: [
              {
                title: 'State Name',
                    data: 'stateName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Headquarter Name',
                    data: 'districtName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'User Name',
                    data: 'userName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: this.viewSampleAndGift.value.moduleTypeName,
                    data: 'productName',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Product Code',
                    data: 'productCode',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Receipt',
                    data: 'receipt',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Balance',
                    data: 'balance',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              },
              {
                title: 'Issued Date',
                    data: 'issueDate',
                    render: function (data) {
                      if (data === '') {
                        return '---'
                      } else {
                        return data
                      }
                    },
                    defaultContent: '---'
              }
            ],
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'print'
            ]
          };
          this.dataTable = $(this.dataTable.nativeElement);
          this.dataTable.DataTable(this.dtOptions);
          //-------------------------------------------//
        } else {
          this._changeDetectorRef.detectChanges();
          this._toastr.success("Please select another details", "No Gifts Found for selected Details !!");
        }
      }, err => {
      });
    }
  }


}
