import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { TypeComponent } from '../../filters/type/type.component';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { DesignationService } from '../../../../../core/services/designation.service';
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { ExpenseClaimedService } from '../../../../../core/services/expense-claimed.service';
import * as _moment from 'moment';
import { MatSlideToggleChange } from '@angular/material';

declare var $;
export interface matRangeDatepickerRangeValue<D> {
  begin: D | null;
  end: D | null;
}
@Component({
  selector: 'm-consolidated-expense',
  templateUrl: './consolidated-expense.component.html',
  styleUrls: ['./consolidated-expense.component.scss']
})
export class ConsolidatedExpenseComponent implements OnInit {
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;

  @ViewChild('expenseTable') table1;
  expenseTable: any;
  expenseTableOptions: any;

  ConsolidatedExpenseFilter: FormGroup;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  showType: boolean = false;
  showState: boolean = false;
  showDistrict: boolean = false;
  showDesignation: boolean = false;
  showStatus: boolean = false;
  showEmployee: boolean = false;
  showProcessing: boolean = false;
  isShowDivision: boolean = false;
  showTableData: boolean = false;
  showLookingForFilter: boolean = true;
  currentDate = new Date();

  constructor(
    private _designation: DesignationService,
    private _expenseClaimedService: ExpenseClaimedService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _toastr: ToastrService
  ) {
    this.ConsolidatedExpenseFilter = new FormGroup({
      reportType: new FormControl('Geographical'),
      type: new FormControl(null, Validators.required),
      companyId: new FormControl(this.currentUser.companyId),
      lookingFor: new FormControl(null),
      stateIds: new FormControl(null),
      districtIds: new FormControl(null),
      userIds: new FormControl(null),
      status: new FormControl(true),
      designation: new FormControl(null),
      dateRange: new FormControl(null, Validators.required),
      lookingForClaim_Ded: new FormControl(null, Validators.required)
    })
  }
  isToggled = true;
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  ngOnInit() {
    if (this.isDivisionExist === true) {
      this.isShowDivision = true;

      this.ConsolidatedExpenseFilter.setControl('divisionIds', new FormControl('', Validators.required));
      this.ConsolidatedExpenseFilter.setControl('isDivisionExist', new FormControl(true));
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    }

    if (this.currentUser.userInfo[0].designationLevel == 0 || this.currentUser.userInfo[0].designationLevel > 1) { //For Admin And MGR
      this.showType = true;
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
    }
  }

  getReportType(val) {
    this.ConsolidatedExpenseFilter.patchValue({ reportType: val });
    this.showTableData = false;
    this.showProcessing = false;
    if (val === "Geographical") {
      this.showLookingForFilter = true;
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");
      }
    } else {
      this.showLookingForFilter = false;
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");
    }
  }

  getTypeValue(val) {
    this.showTableData = false;
    this.showProcessing = false;
    if (val == "State") {
      this.showState = true;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.ConsolidatedExpenseFilter.removeControl("userIds");
      this.ConsolidatedExpenseFilter.removeControl("status");
      this.ConsolidatedExpenseFilter.removeControl("designation");
      this.ConsolidatedExpenseFilter.setControl("stateIds", new FormControl('', Validators.required))
      // this.ConsolidatedExpenseFilter.setControl("lookingFor", new FormControl('', Validators.required))
    } else if (val == "Headquarter") {
      this.showState = true;
      this.showDistrict = true;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      if (this.isDivisionExist === true) {
        this.callDivisionComponent.setBlank();
      }
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.ConsolidatedExpenseFilter.setControl("stateIds", new FormControl('', Validators.required))
      this.ConsolidatedExpenseFilter.setControl("districtIds", new FormControl('', Validators.required))
      // this.ConsolidatedExpenseFilter.setControl("lookingFor", new FormControl('', Validators.required));
      this.ConsolidatedExpenseFilter.removeControl("userIds");
      this.ConsolidatedExpenseFilter.removeControl("status");
      this.ConsolidatedExpenseFilter.removeControl("designation");

    } else if (val == "Employee Wise") {
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = true;
      this.showStatus = true;
      this.showEmployee = true;
      this.ConsolidatedExpenseFilter.removeControl("stateIds");
      this.ConsolidatedExpenseFilter.removeControl("districtIds");
      // this.ConsolidatedExpenseFilter.removeControl("lookingFor");
      this.ConsolidatedExpenseFilter.setControl("userIds", new FormControl('',Validators.required))
      this.ConsolidatedExpenseFilter.setControl("status", new FormControl(true))
      this.ConsolidatedExpenseFilter.setControl("designation", new FormControl('', Validators.required))
    }
    this.ConsolidatedExpenseFilter.patchValue({ type: val });

  }

  getDivisionValue(val) {
    this.showTableData = false;
    this.showProcessing = false;
    this.ConsolidatedExpenseFilter.patchValue({ divisionIds: val });
    if (this.currentUser.userInfo[0].designationLevel == 0) {
      if (this.ConsolidatedExpenseFilter.value.type == "Employee Wise") {
        //=======================Clearing Filter===================
        this.callDesignationComponent.setBlank();
        this.callStatusComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================
        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.ConsolidatedExpenseFilter.value.type == "State" || this.ConsolidatedExpenseFilter.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================
        let divisionIds = {
          division: val
        };
        this.callStateComponent.getStateBasedOnDivision(divisionIds);
      }
    } else if (this.currentUser.userInfo[0].designationLevel >= 2) {
      if (this.ConsolidatedExpenseFilter.value.type == "Employee Wise") {
        //=======================Clearing Filter===================
        this.callDesignationComponent.setBlank();
        this.callStatusComponent.setBlank();
        this.callEmployeeComponent.setBlank();
        //===============================END================================

        let passingObj = {
          companyId: this.currentUser.companyId,
          userId: this.currentUser.id,
          userLevel: this.currentUser.userInfo[0].designationLevel,
          division: val
        }

        this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
      } else if (this.ConsolidatedExpenseFilter.value.type == "State" || this.ConsolidatedExpenseFilter.value.type == "Headquarter") {

        //=======================Clearing Filter===================
        this.callStateComponent.setBlank();
        this.callDistrictComponent.setBlank();
        //===============================END=======================

        let passingObj = {
          companyId: this.currentUser.companyId,
          isDivisionExist: true,
          division: this.ConsolidatedExpenseFilter.value.divisionIds,
          supervisorId: this.currentUser.id,
        }
        this.callStateComponent.getUniqueStateOnManagerBasedOnDivision(passingObj)

      }
    }

  }

  getStateValue(val) {
    this.showTableData = false;
    this.showProcessing = false;

    this.ConsolidatedExpenseFilter.patchValue({ stateIds: val });

    if (this.isDivisionExist === true) {
      if (this.ConsolidatedExpenseFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.currentUser.userInfo[0].designationLevel,
          companyId: this.currentUser.companyId,
          "stateId": this.ConsolidatedExpenseFilter.value.stateIds,
          isDivisionExist: this.isDivisionExist,
          division: this.ConsolidatedExpenseFilter.value.divisionIds
        })
      }
    } else {
      if (this.ConsolidatedExpenseFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrictValue(val) {
    this.showTableData = false;
    this.showProcessing = false;
    this.ConsolidatedExpenseFilter.patchValue({ districtIds: val });
  }

//   getDesignation(val) {
//     this.showProcessing = false;
//     this.ConsolidatedExpenseFilter.patchValue({ designation: val })
//   }


getDesignation(val) {
	this.ConsolidatedExpenseFilter.patchValue({ designation: val.designationLevel });
	 this.callEmployeeComponent.setBlank();

	 if (this.currentUser.userInfo[0].rL === 0) {
	   if (this.currentUser.company.isDivisionExist == true) {
		 let passingObj = {
		   companyId: this.currentUser.companyId,
		   designationObject: [val],
		   status: [true],
		   division: this.ConsolidatedExpenseFilter.value.divisionIds
		 }
		 this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
	   } else if (this.currentUser.company.isDivisionExist == false) {
		 this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, [val], [true]);
	   }
	 } else {
	   let dynamicObj = {};
	   if (this.currentUser.company.isDivisionExist == false) {
		 dynamicObj = {
		   supervisorId: this.currentUser.id,
		   companyId: this.currentUser.companyId,
		   status: true,
		   type: "lower",
		   designation: [val.designationLevel] //desig
		 };
		 this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
	   } else if (this.currentUser.company.isDivisionExist == true) {
		 dynamicObj = {
		   supervisorId: this.currentUser.id,
		   companyId: this.currentUser.companyId,
		   status: true,
		   type: "lower",
		   designation: [val.designationLevel], //desig,
		   isDivisionExist: true,
		   division: this.ConsolidatedExpenseFilter.value.division
		 };
		 this.callEmployeeComponent.getManagerHierarchy(dynamicObj)
	   }

	 }
   }

  getStatusValue(val) {
    this.showProcessing = false;
    this.showTableData = false;
    this.ConsolidatedExpenseFilter.patchValue({ status: val });
  }

  getEmployee(emp: string) {
	this.showTableData = false;
    this.showProcessing = false;
    this.ConsolidatedExpenseFilter.patchValue({ userIds: emp })
  }


  viewRecords(): void {
    this.isToggled = false;
    this.showProcessing = true;

    this.ConsolidatedExpenseFilter.value.dateRange.begin = _moment(new Date(this.ConsolidatedExpenseFilter.value.dateRange.begin)).format("YYYY-MM-DD");
    this.ConsolidatedExpenseFilter.value.dateRange.end = _moment(new Date(this.ConsolidatedExpenseFilter.value.dateRange.end)).format("YYYY-MM-DD");

    this._expenseClaimedService.getConsolidatedExpenseNew(this.ConsolidatedExpenseFilter.value).subscribe(res => {


      if (res.length == 0 || res == undefined) {
        //this.showUntouchedProviderDetails = false;
        this.showProcessing = false;
        this._toastr.info('For Selected Filter data not found', 'Data Not Found');
      } else {
        this.showProcessing = false;
        this.showTableData = true;
        this.expenseTableOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "fixedColumns": true,
          destroy: true,
          data: res,
          responsive: true,
          columns: [
            {
              title: 'Division',
              data: 'division',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'State',
              data: 'state',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Total No. Of Claims',
              data: 'numberOfClaims',
              visible: false,
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: this.currentUser.company.lables.hqLabel,
              data: 'district',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Date Of Joining',
              data: 'joiningDate',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return _moment(new Date(data)).format("DD-MMM-YYYY")
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Employee Name',
              data: 'name',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Designation',
              data: 'designation',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },

            {
              title: 'Rcvd Claim Amt.',
              data: 'claimedExpense',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Pass Amount By Mgr',
              data: 'approvedExpenseByMgr',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }, {
              title: 'Pass Amount By Admin',
              data: 'approvedExpenseByAdmin',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Deduction',
              data: 'AmountDeduction',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Previous Month Addition',
              data: 'sumExpAdm',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Previous Month Deduction',
              data: 'subtExpAdm',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Claimed DA',
              data: 'claimedDA',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            },
            {
              title: 'Claimed TA',
              data: 'claimedTA',
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            }

          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
          },
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'excel',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'csv',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'print',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'print',
              exportOptions: {
                columns: ':visible'
              }
            }
          ]
        };

        if (this.ConsolidatedExpenseFilter.value.lookingForClaim_Ded == 'claimed') {
          for (const expHD of res[0].expenseHead) {
            this.expenseTableOptions.columns.push({
              title: `Claim ${expHD}`,
              data: `${expHD.replace(/\s/g, '')}`,
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            });
          }
        } else if (this.ConsolidatedExpenseFilter.value.lookingForClaim_Ded == 'deduction') {
          for (const expHD of res[0].expenseHead) {
            this.expenseTableOptions.columns.push({
              title: `Ded. ${expHD}`,
              data: `Ded${expHD.replace(/\s/g, '')}`,
              render: function (data) {
                if (data === '') {
                  return '---'
                } else {
                  return data
                }
              },
              defaultContent: '---'
            });
          }
        }

        if (this.ConsolidatedExpenseFilter.value.reportType === "Geographical" && this.ConsolidatedExpenseFilter.value.lookingFor == "consolidated") {
          this.expenseTableOptions.columns[2].visible = true;
          this.expenseTableOptions.columns[3].visible = false;
          this.expenseTableOptions.columns[4].visible = false;
          this.expenseTableOptions.columns[5].visible = false;
          this.expenseTableOptions.columns[6].visible = false;
          this.expenseTableOptions.columns[13].visible = false;
          this.expenseTableOptions.columns[14].visible = false;
        }
        if(this.isDivisionExist==true){

            this.expenseTableOptions.columns[0].visible = true;

        }
        if(this.ConsolidatedExpenseFilter.value.lookingFor =="consolidated"){
          this.expenseTableOptions.columns[0].visible = false;

        }

        this._changeDetectorRef.detectChanges();
        this.expenseTable = $(this.table1.nativeElement);
        this.expenseTable.DataTable(this.expenseTableOptions);
        this._changeDetectorRef.detectChanges();

      }
      this._changeDetectorRef.detectChanges();
    });
  }

  hideTable(): void {
    this.showTableData = false;
    this.showProcessing = false;
  };

  showDeduction_ClaimedFilter: boolean = false;
  Deduction_ClaimedFilter(val: string): void {
    if (val === 'employeeWise') {
      this.showDeduction_ClaimedFilter = true;
    } else {
      this.showDeduction_ClaimedFilter = false;
    }
  }

}
