import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidatedExpenseComponent } from './consolidated-expense.component';

describe('ConsolidatedExpenseComponent', () => {
  let component: ConsolidatedExpenseComponent;
  let fixture: ComponentFixture<ConsolidatedExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolidatedExpenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolidatedExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
