import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { POBProductWiseComponent } from './pobproduct-wise.component';

describe('POBProductWiseComponent', () => {
  let component: POBProductWiseComponent;
  let fixture: ComponentFixture<POBProductWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ POBProductWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(POBProductWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
