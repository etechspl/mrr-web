import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { EmployeeComponent } from './../../filters/employee/employee.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { DivisionComponent } from './../../filters/division/division.component';
import { TypeComponent } from "./../../filters/type/type.component";
import { DesignationComponent } from '../../filters/designation/designation.component';
import { URLService } from '../../../../../core/services/URL/url.service';
import { MatSlideToggleChange } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { DistrictComponent } from "./../../filters/district/district.component";
import { DcrReportsService } from '../../../../../core/services/DCR/dcr-reports.service';
import { StateComponent } from "./../../filters/state/state.component";
import * as _moment from 'moment';
declare var $;

@Component({
  selector: 'm-pobproduct-wise',
  templateUrl: './pobproduct-wise.component.html',
  styleUrls: ['./pobproduct-wise.component.scss']
})

export class PobProductWiseComponent implements OnInit {
	public showState: boolean = true;
  public showDistrict: boolean = false;
	public showEmployee: boolean = false;
  @ViewChild('dataTable') dataTable1;
  dataTable: any;
  dtOptions: any;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callDistrictComponent")callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;

  count = 0;
  
  isToggled = true; 
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }
  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist = this.currentUser.company.isDivisionExist;
  companyId = this.currentUser.companyId;
  public showDivisionFilter: boolean = false;
  dataSource = [];
  showGeoReportType=false;
  showProcessing=false;
  isShowReport=false;
  POBProductWiseForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private _urlService: URLService,
    private _toastr: ToastrService,
    private route: ActivatedRoute,
    private _dcrReportsService:DcrReportsService,

    ) { 

      this.POBProductWiseForm = this.fb.group({
        type: new FormControl(null, Validators.required),
        designation: new FormControl(null),
        employeeId: new FormControl(null),
        toDate: new FormControl('', Validators.required),
        fromDate: new FormControl('', Validators.required),
      })
    }
    isShowDivision = false;
    designationLevel = this.currentUser.userInfo[0].designationLevel;
getStateValue(val) {
		this.POBProductWiseForm.patchValue({ stateInfo: val });
		if (this.isDivisionExist === true) {
			if (this.POBProductWiseForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.designationLevel,
					companyId: this.companyId,
					stateId: this.POBProductWiseForm.value.stateInfo,
					isDivisionExist: this.isDivisionExist,
					division: this.POBProductWiseForm.value.divisionId,
				});

				//this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
			}
		} else {
			if (this.POBProductWiseForm.value.type == "Headquarter") {
				console.log('this---------------->',val);
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
	}
    
    getFromDate(val) {
      this.POBProductWiseForm.patchValue({ fromDate: val });
  
    }
    getToDate(val) {
      this.POBProductWiseForm.patchValue({ toDate: val });
    }
    getDivisionValue(val) {
      this.POBProductWiseForm.patchValue({ division: val });
      //=======================Clearing Employee Filter===================
      this.callDesignationComponent.setBlank();
      this.callEmployeeComponent.setBlank();
      //===============================END================================
      let passingObj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        division: [val]
      }
      console.log('passingObj---------------------->',passingObj)
      this.callDesignationComponent.getDesignationBasedOnDivision(passingObj);
    }
    getDesignation(val) {
    //  this.showTPDeviationDetails = false;
      this.POBProductWiseForm.patchValue({ designation: val });
  
      this.callEmployeeComponent.setBlank();
  
      if (this.currentUser.company.isDivisionExist == true) {
        if (this.currentUser.company.isDivisionExist == true && this.POBProductWiseForm.value.division != null) {
          let passingObj = {
            companyId: this.currentUser.companyId,
            designationObject: val.designationLevel,
            status: [true],
            division: [this.POBProductWiseForm.value.division]
          }
          this.callEmployeeComponent.getEmployeeListBasedOnDivision(passingObj);
        }
      } else {
        this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, [true]);
      }
    }
    getEmployees(val) {
      this.POBProductWiseForm.patchValue({ employeeId: val });
    }
  ngOnInit() {

    this.getReportType('Geographical');
    if (this.currentUser.company.isDivisionExist == true) {

      this.POBProductWiseForm.setControl('division', new FormControl());

      let obj = {
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj)
      this.showDivisionFilter = true;
    }
    this.route.queryParams.subscribe(res => {
     // console.log('queryParams =>  ',res);
      if(res.toAndFromDate){
        let date  = _moment(new Date(res.toAndFromDate)).format('YYYY-MM-DD')
        this.POBProductWiseForm.patchValue({fromDate: date})
        this.POBProductWiseForm.patchValue({toDate: date})
        this.POBProductWiseForm.patchValue({employeeId: res.userId})
      
      }
    });
    this.route.queryParams = null;
  }
	getReportType(val) {
		if (val === "Geographical") {
			this.showState = true;
			this.showEmployee = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			this.showState = false;
			this.showDistrict = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
	}
  getDistrictValue(val) {
		this.POBProductWiseForm.patchValue({ districtInfo: val });
	}
  getTypeValue(val) {
		if (val == "State") {
			this.showDistrict = false;
			this.POBProductWiseForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.POBProductWiseForm.removeControl("employeeId");
			this.POBProductWiseForm.removeControl("designation");
		} else if (val == "Headquarter") {
      console.log('Headquarter',val)
			this.POBProductWiseForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.POBProductWiseForm.setControl(
				"districtInfo",
				new FormControl("", Validators.required)
			);
			this.POBProductWiseForm.removeControl("employeeId");
			this.POBProductWiseForm.removeControl("designation");
			this.showDistrict = true;
		} else if (val == "Employee Wise") {
			this.showEmployee = true;
			this.POBProductWiseForm.removeControl("stateInfo");
			this.POBProductWiseForm.removeControl("districtInfo");
			this.POBProductWiseForm.setControl(
				"designation",
				new FormControl("", Validators.required)
			);
			this.POBProductWiseForm.setControl(
				"employeeId",
				new FormControl("", Validators.required)
			);
		} else if (val == "Self") {
			this.showEmployee = false;
		}

		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
		} else {
			this.isShowDivision = false;
		}

		this.POBProductWiseForm.patchValue({ type: val });
		this._changeDetectorRef.detectChanges();
	}


  viewRecords(){
    this.isShowReport=false;
    this.isToggled = false;
    this.showProcessing = true;
    if (this.POBProductWiseForm.value.employeeId == null) {
      this.POBProductWiseForm.value.employeeId = [this.currentUser.id];
    }
    let params = {
    fromDate : this.POBProductWiseForm.value.fromDate,
    toDate : this.POBProductWiseForm.value.toDate,
    companyId : this.currentUser.companyId
    };
    if (this.POBProductWiseForm.value.type === 'State') {
      params['stateId'] = this.POBProductWiseForm.value.stateInfo ;
    } else if (this.POBProductWiseForm.value.type === 'Headquarter') {
      params['districtId'] =  this.POBProductWiseForm.value.districtInfo ;
      params['stateId'] = this.POBProductWiseForm.value.stateInfo ;
    }
    else if (this.POBProductWiseForm.value.type === 'Employee Wise') {
      params['userId'] = this.POBProductWiseForm.value.employeeId ;
    }
    // params["fromDate"] = this.POBProductWiseForm.value.fromDate;
    // params["toDate"] = this.POBProductWiseForm.value.toDate;
    // params["companyId"] = this.currentUser.companyId;

    this._dcrReportsService.getPOBProductWiseDetails(params).subscribe(res=>{
     console.log('params---------------------->',params)
      if (res.length == 0 || res == undefined) {
        this.showProcessing = false;
        this.isShowReport = false;
        this._toastr.info('For Selected Filter data not found', 'Data Not Found');
      } else {

        this.showProcessing = false;
        this.isShowReport=true;
        res.forEach((item, i) => {
          item["index"] = i + 1;
        });
        
        const PrintTableFunction = this.PrintTableFunction.bind(this);
        
        this.dtOptions = {
          "pagingType": 'full_numbers',
          "paging": true,
          "ordering": true,
          "info": true,
          "scrollY": 300,
          "scrollX": true,
          "scrollCollapse": true,
          destroy: true,
          data: res,
          responsive: true,
          columns: [
            {
              title: "S No.",
              data: "index",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "ProductName",
              data: "productName",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "Quantity",
              data: "productQty",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "Price",
              data: "productPrice",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
            {
              title: "POB",
              data: "pob",
              render: function (data) {
                if (data === "") {
                  return "---";
                } else {
                  return data;
                }
              },
              defaultContent: "---",
            },
           
            
     
          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Records",
          },
          dom: "Bfrtip",
          buttons: [
            {
              extend: "excel",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: "csv",
              exportOptions: {
                columns: ":visible",
              },
            },
            {
              extend: 'copy',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'print',
              title: function () {
              return 'State List';
              },
              exportOptions: {
              columns: ':visible'
              },
              action: function (e, dt, node, config) {
              PrintTableFunction(res);
                  }
             }
          ],
        };
        this._changeDetectorRef.detectChanges();
        this.dataTable = $(this.dataTable1.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
        this._changeDetectorRef.detectChanges();
      }
      this._changeDetectorRef.detectChanges();
    })
    
  }


  PrintTableFunction(data?: any): void {
    const companyName = this.currentUser.companyName;
    
    const logoURL = this._urlService.API_ENDPOINT_CONTAINER + '/' + this.currentUser.company.logo.container + '/download/' + this.currentUser.company.logo.modifiedFileName;
    const tableRows: any = [];
  
    data.forEach(element => {
  
    tableRows.push(`<tr>
    <td class="tg-oiyu">${element.index}</td>
    <td class="tg-oiyu">${element.productName}</td>
    <td class="tg-oiyu">${element.productQty}</td>
    <td class="tg-oiyu">${element.productPrice}</td>
    <td class="tg-oiyu">${element.pob}</td>
    </tr>`)
    });
  
    let showHeaderAndTable: boolean = false;
    // this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;
    let printContents, popupWin;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
    <head>
    <style>
    img {
    float: left;
    }
  
    .flex-container {
    display: flex;
    justify-content: space-between;
    }
  
    .tg {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }
  
    .tg td {
    font-family: Arial, sans-serif;
    font-size: 14px;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
    
    .tb2 .tg-oikpyu {
      font-size: 10px;
      text-align: center;
      vertical-align: middle
      }
  
    
    .tg th {
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tg .tg-lboi {
    border-color: inherit;
    text-align: left;
    vertical-align: middle;
  
    }
  
    .tg .tg-u6fn {
    font-weight: bold;
    font-size: 18px;
    background-color: #efefef;
    color:black;
    border-color: inherit;
    text-align: left;
    vertical-align: top;
    padding-top: 0px;
    padding-bottom: 0px;
    }
  
    .tg .tg-yz93 {
    border-color: inherit;
    text-align: right;
    vertical-align: middle
    }
    .tb2 {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }
    
    
  
    .tb2 td {
    font-family: Arial, sans-serif;
    font-size: 12px;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 th {
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-weight: normal;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }
  
    .tb2 .tg-oiyu {
    font-size: 10px;
    text-align: left;
    vertical-align: middle
    }
    .tg-bckGnd {
    font-weight: bold;
    background-color: #efefef;
    }
  
    .tb2 .tg-3ppa {
    font-size: 10px;
    background-color: #efefef;
    color:black;
    text-align: left;
    vertical-align: middle;
    padding-top: 2px;
    padding-bottom: 2px;
    }
  
    .footer {
    position: sticky;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #efefef;
    color: rgb(2, 2, 2);
    text-align: right
    }
  
    button {
    background-color: #008CBA;
    border: none;
    color: white;
    margin: 5px 0px;
    padding: 2px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    border-radius: 12px;
    cursor: pointer;
    outline: none;
    }
    @media print {
    @page { size: A4; margin: 5mm; }
    .footer {
    position: relative;
    }
    tr {
    page-break-inside: avoid;
    }
    button{
    display : none;
    }
    }
    </style>
    </head>
    <body onload="window.print();window.close()" style="padding-bottom:30px">
    <div class="flex-container">
    <div style="width: 40%; ">
    <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
    <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>
  
    </div>
  
    <h2 style="margin-top: 0px; margin-bottom: 5px " ; align="left"><u>Station Details </u></h2>
    <div>
    <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
    <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
    </div>
    </div>
    </div>
  
    <div>
    
    <table class="tb2">
    <tr>
    <th class="tg-3ppa"><span style="font-weight:20">S.No.</span></th>
    <th class="tg-3ppa"><span style="font-weight:20">Product Name</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Product Qty</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">Product Price</span></th>
    <th class="tg-3ppa"><span style="font-weight:300">POB</span></th>
                
    </tr>
    ${tableRows.join('')}
    </table>
    </div>
    
    </body>
    </html>
    `);
  
    // popupWin.document.close();
  
    }

}

