import { DivisionComponent } from './../../filters/division/division.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { TypeComponent } from '../../filters/type/type.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { StatusComponent } from '../../filters/status/status.component';
import { StateComponent } from '../../filters/state/state.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { MonthComponent } from '../../filters/month/month.component';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TourProgramService } from '../../../../../core/services/TourProgram/tour-program.service';
import { MatDialog,MatTableDataSource, MatSlideToggleChange,MatSort,Sort } from '@angular/material';
import Swal from 'sweetalert2';
import { ExportAsService, ExportAsConfig} from 'ngx-export-as';

@Component({
  selector: 'm-tpapproval',
  templateUrl: './tpapproval.component.html',
  styleUrls: ['./tpapproval.component.scss']
})
export class TPApprovalComponent implements OnInit {
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callStatusComponent") callStatusComponent: StatusComponent;
  @ViewChild("callEmployeeComponent") callEmployeeComponent: EmployeeComponent;
  @ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['stateName', 'districtName', 'name', 'designation', 'submitted', 'submittedDate', 'approved', 'approvedDate','rejectionMsg'];
  dataSource = [];

  displayedColumnsForTPDetail = ['date', 'submittedActivity', 'submittedArea', ,'submittedDoctor', 'submittedChemist', 'submittedRemark', 'apprvActivity', 'approvedArea','workWith', 'apprvDoctor', 'approvedChemists', 'apprvRemark'];
  dataSourceForTPInDetail
  IS_DIVISION_EXIST;
  COMPANY_ID;
  designationLevel;
  isShowDivision = false;
  currentUser = JSON.parse(sessionStorage.currentUser);

  public isToggled: boolean = true;
  public showState: boolean = false;
  public showDistrict: boolean = false;
  public showDesignation: boolean = false;
  public showStatus: boolean = false;
  public showEmployee: boolean = false;
  public showTPDetails: boolean = false;
  public showCompleteTPDetails: boolean = false;
  public showApproveButton: boolean = false;
  public showRejectButton :boolean=false;

  public showSubmiitedColumn: boolean = false;
  public showApprovedColumn: boolean = false;
  public employeeNameWithStatus: string = "";
  tpApprovalFilter: FormGroup;
  submittedFilter = [];
  

  constructor(
    private fb: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private _tpService: TourProgramService,
    public dialog: MatDialog,
    private exportAsService: ExportAsService,
  ) {
    // this.tpApprovalFilter = this.fb.group({
    //   reportType: new FormControl('Geographical'),
    //   type: new FormControl(null, Validators.required),
    //   stateInfo: new FormControl(null, Validators.required),
    //   // stateInfo: new FormControl(null),
    //   districtInfo: new FormControl(null),
    //   designation: new FormControl(''),
    //   status: new FormControl(true),
    //   employeeIds: new FormControl(null),
    //   month: new FormControl(null, Validators.required),
    //   year: new FormControl(null, Validators.required)
    // })
    this.tpApprovalFilter = this.fb.group({
      reportType: ['Geographical'],
      type: [null, Validators.required],
      stateInfo: [null, Validators.required],
      districtInfo: [null],
      designation: [null],
      status: [[true]],
      employeeIds:[null],
      month: [null, Validators.required],
      year: [null, Validators.required],
      submitted: [null, Validators.required]
    })
  }

  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
   elementId: 'tableId1', // the id of html/table element

 }

  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.currentUser);
    this.COMPANY_ID = this.currentUser.companyId;
    this.IS_DIVISION_EXIST = this.currentUser.company.isDivisionExist;
  this.designationLevel = this.currentUser.userInfo[0].designationLevel;
    if (this.IS_DIVISION_EXIST === true) {
      this.isShowDivision = true;
      this.tpApprovalFilter.setControl('divisionId', new FormControl());
      let obj = {
        companyId: this.currentUser.companyId,
        designationLevel: this.currentUser.userInfo[0].designationLevel
      };

      if (this.currentUser.userInfo[0].rL > 1) {
        obj["supervisorId"] = this.currentUser.id;
      }
      this.callDivisionComponent.getDivisions(obj);

    } else {
      this.isShowDivision = false;
    }

    if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
      this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

    }
  }
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  submittedStatus(event) {
		this.submittedFilter = event;
	}

  getReportType(val) {
    this.tpApprovalFilter.patchValue({type: null});
    this.tpApprovalFilter.patchValue({reportType: val});
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    if (val == "Geographical") {
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.tpApprovalFilter.get('stateInfo').setValidators([Validators.required]);
      this.tpApprovalFilter.patchValue({stateInfo: null});

      this.tpApprovalFilter.get('districtInfo').clearValidators();
      this.tpApprovalFilter.patchValue({districtInfo: null});

      this.tpApprovalFilter.get('designation').clearValidators();
      this.tpApprovalFilter.patchValue({designation: null});

      this.tpApprovalFilter.get('status').clearValidators();

      this.tpApprovalFilter.get('employeeIds').clearValidators();
      this.tpApprovalFilter.patchValue({employeeIds: null});

      (this.callStateComponent)?this.callStateComponent.setBlank(): null;

      if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

      } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
        this.callTypeComponent.getTypesBasedOnReportingType("Geographical", "WithHeadquarter");

      }
    } else {
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;

      this.tpApprovalFilter.get('designation').setValidators([Validators.required]);
      this.tpApprovalFilter.patchValue({designation: null});

      this.tpApprovalFilter.get('status').setValidators([Validators.required]);

      this.tpApprovalFilter.get('employeeIds').setValidators([Validators.required]);
      this.tpApprovalFilter.patchValue({employeeIds: null});

      this.tpApprovalFilter.get('stateInfo').clearValidators();
      this.tpApprovalFilter.patchValue({stateInfo: null});

      this.tpApprovalFilter.get('districtInfo').clearValidators();
      this.tpApprovalFilter.patchValue({districtInfo: null});

      if (this.currentUser.userInfo[0].designationLevel == 0) { //For Admin
        this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "empWise");

      } else if (this.currentUser.userInfo[0].designationLevel > 1) { //For Manager
        this.callTypeComponent.getTypesBasedOnReportingType("Hierarachy", "onlyEmployeeWise");

      }
    }
  }
  getTypeValue(val) {
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    if (val == "State") {
      if (this.IS_DIVISION_EXIST === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = true;
      this.showDistrict = false;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.tpApprovalFilter.get('stateInfo').setValidators([Validators.required]);
      this.tpApprovalFilter.get('districtInfo').clearValidators();
      this.tpApprovalFilter.patchValue({districtInfo: null});
    } else if (val == "Headquarter") {
      if (this.IS_DIVISION_EXIST === true) {
        this.callDivisionComponent.setBlank();
      }
      this.showState = true;
      this.showDistrict = true;
      this.showDesignation = false;
      this.showStatus = false;
      this.showEmployee = false;
      this.tpApprovalFilter.get('districtInfo').setValidators([Validators.required]);
      this.tpApprovalFilter.get('stateInfo').setValidators([Validators.required]);
      this.tpApprovalFilter.patchValue({ stateInfo: null });
      this.tpApprovalFilter.patchValue({ districtInfo: null });
      (this.callStateComponent)?this.callStateComponent.setBlank(): null;
    } else if (val == "Employee Wise") {
      this.showState = false;
      this.showDistrict = false;
      this.showDesignation = true;
      this.showStatus = true;
      this.showEmployee = true;

      if (this.IS_DIVISION_EXIST === true) {
        this.callDivisionComponent.setBlank();
      }

    }

    if (this.IS_DIVISION_EXIST === true) {
      this.isShowDivision = true;
    } else {
      this.isShowDivision = false;
    }
    this.tpApprovalFilter.patchValue({ type: val });


  }


  

  //setting division into dcrform
  getDivisionValue($event) {
    this.tpApprovalFilter.patchValue({ divisionId: $event });
    if (this.IS_DIVISION_EXIST === true) {
      if (this.tpApprovalFilter.value.type === "State" || this.tpApprovalFilter.value.type === "Headquarter") {
        this.callStateComponent.getStateBasedOnDivision({
          companyId: this.currentUser.companyId,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.tpApprovalFilter.value.divisionId,
          designationLevel: this.currentUser.userInfo[0].designationLevel
        })
      }

      if (this.tpApprovalFilter.value.type === "Employee Wise") {
        //getting employee if we change the division filter after select the designation
        if (this.tpApprovalFilter.value.divisionId != null && this.tpApprovalFilter.value.designation != null) {
          if (this.tpApprovalFilter.value.designation.length > 0) {
            this.callEmployeeComponent.getEmployeeListBasedOnDivision({
              companyId: this.COMPANY_ID,
              division: this.tpApprovalFilter.value.divisionId,
              status: [true],
              designationObject: this.tpApprovalFilter.value.designation
            })
          }
        }
      }
    }
  }

 
  getStateValue(val) {
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;

    this.tpApprovalFilter.patchValue({ stateInfo: val });
    if (this.IS_DIVISION_EXIST === true) {
      if (this.tpApprovalFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistrictsBasedOnDivision({
          designationLevel: this.designationLevel,
          companyId: this.COMPANY_ID,
          stateId: this.tpApprovalFilter.value.stateInfo,
          isDivisionExist: this.IS_DIVISION_EXIST,
          division: this.tpApprovalFilter.value.divisionId
        })
        //this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    } else {
      if (this.tpApprovalFilter.value.type == "Headquarter") {
        this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
      }
    }
  }

  getDistrictValue(val) {
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    this.tpApprovalFilter.patchValue({ districtInfo: val });
  }
 
  getDesignation(val) {
    
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    this.tpApprovalFilter.patchValue({ designation: val });
    console.log(this.tpApprovalFilter.value);
    if (this.IS_DIVISION_EXIST === true) {
      this.callEmployeeComponent.getEmployeeListBasedOnDivision({
        companyId: this.COMPANY_ID,
        division: this.tpApprovalFilter.value.divisionId,
        status: [true],
        designationObject: this.tpApprovalFilter.value.designation.designationLevel
      })
    } else {
      this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, val.designationLevel, [true]);
    }

  }


  getStatusValue(val) {
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    this.tpApprovalFilter.patchValue({ status: [val] });
    // this.tpApprovalFilter.patchValue({ employeeIds: null });
    this.callEmployeeComponent.setBlank();
    this.callEmployeeComponent.getEmployeeList(this.currentUser.companyId, this.tpApprovalFilter.value.designation.designationLevel, val);
  }
  getEmployeeValue(val) {
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    this.tpApprovalFilter.patchValue({ employeeIds: val });

  }
  getMonth(val) {
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    this.tpApprovalFilter.patchValue({ month: val });
  }
  getYear(val) {
    this.showTPDetails = false;
    this.showCompleteTPDetails = false;
    this.tpApprovalFilter.patchValue({ year: val });
  }

 
  showReport() {
    let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.tpApprovalFilter.value.divisionId;
    }
          console.log("this.tpApprovalFilter.value",this.tpApprovalFilter.value)
    this._tpService.getTPStatus(this.currentUser.companyId, this.tpApprovalFilter.value, this.IS_DIVISION_EXIST,divisionIds).subscribe(
      
        (res) => {
          console.log(" res.length",res.length)

          if(res){
      this.showTPDetails = true;
      this.showCompleteTPDetails = false;
      this.dataSource=res.filter((tpRes) =>
							this.submittedFilter.includes(tpRes.submitted)
						).sort((a,b)=>a.name.localeCompare(b.name));
    
         
      (res.length)?this.isToggled = false: null;
      this._changeDetectorRef.detectChanges();

    }
    
    }, err => {
      console.log(err)
      this.showTPDetails = false;
      this.showCompleteTPDetails = false;
    })
  }//showReport() method end here.

  getTPInDetail(userId, status, userName) {


    if (status == "submitted") {
      this.employeeNameWithStatus = userName + " - Submitted";
      this.showApproveButton = true;
      this.showRejectButton=false;
      this.showSubmiitedColumn = true;
      this.showApprovedColumn = false;
      this.displayedColumnsForTPDetail = ['date', 'submittedActivity', 'submittedArea','workWith', 'submittedDoctor', 'submittedChemist', 'submittedRemark'];

    } else if (status == "approved") {      
      this.employeeNameWithStatus = userName + " - Approved";
      this.showApproveButton = false;
      this.showRejectButton=false;

      this.showSubmiitedColumn = false;
      this.showApprovedColumn = true;
      this.displayedColumnsForTPDetail = ['date', 'apprvActivity', 'approvedArea','workWith', 'apprvDoctor', 'approvedChemists', 'apprvRemark'];

    }
    this._tpService.viewTourProgram(this.currentUser.companyId, userId, this.tpApprovalFilter.value.month, this.tpApprovalFilter.value.year).subscribe(res => {
      this.dataSourceForTPInDetail = res;

      this.showCompleteTPDetails = true;
      this._changeDetectorRef.detectChanges();
      sessionStorage.setItem("userId", userId);
    }, err => {
      console.log(err)
    })
  }

  updateStatus(status) { 
    this._tpService.updateTPStatus(sessionStorage.getItem("userId"), status, this.tpApprovalFilter.value.month, this.tpApprovalFilter.value.year, "").subscribe(res => {
      sessionStorage.removeItem("userId");
      let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.tpApprovalFilter.value.divisionId;
    }
      this.showCompleteTPDetails = false;
      this._tpService.getTPStatus(this.currentUser.companyId, this.tpApprovalFilter.value, this.IS_DIVISION_EXIST,divisionIds).subscribe(res => {
        // console.log(res);
        this.showTPDetails = true;
        this.showCompleteTPDetails = false;
        this.dataSource = res;
        this._changeDetectorRef.detectChanges();
      }, err => {
        console.log(err)
        this.showTPDetails = false;
        this.showCompleteTPDetails = false;
      })
    }, err => {
      console.log(err);
    })

  }
  rejetctStatus(status) {

    Swal.fire({
      title: 'Are you sure?',
      text: 'Enter TP Rejection Message',
      input: 'text',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Reject it!'
    }).then((result) => {
      if (result.value) {
        this._tpService.updateTPStatus(sessionStorage.getItem("userId"), "rejected", this.tpApprovalFilter.value.month, this.tpApprovalFilter.value.year, result.value).subscribe(res => {
          sessionStorage.removeItem("userId");
          this.showCompleteTPDetails = false;
          let divisionIds = [];
    if (this.currentUser.company.isDivisionExist) {
      divisionIds = this.tpApprovalFilter.value.divisionId;
    }
          this._tpService.getTPStatus(this.currentUser.companyId, this.tpApprovalFilter.value, this.IS_DIVISION_EXIST,divisionIds).subscribe(res => {
             console.log(res);
            this.showTPDetails = true;
            this.showCompleteTPDetails = false;
            this.dataSource = res;
            this._changeDetectorRef.detectChanges();
          }, err => {
            console.log(err)
            this.showTPDetails = false;
            this.showCompleteTPDetails = false;
          })
        }, err => {
          console.log(err);
        })
        Swal.fire(
          'Rejected!',
          'Your TP has been Rejected.',
          'success'
        )
      }
    })

    // const dialogRef = this.dialog.open(TpRejectionComponent, {
    //   data: { status }
    // });

  }

  exporttoExcel() {
  
    

    // download the file using old school javascript method
    
    this.exportAsService.save(this.exportAsConfig, 'Tour Program Report').subscribe(() => {
      // save started
   
    });
 
  }
}

