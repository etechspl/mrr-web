import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TPApprovalComponent } from './tpapproval.component';

describe('TPApprovalComponent', () => {
  let component: TPApprovalComponent;
  let fixture: ComponentFixture<TPApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TPApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TPApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
