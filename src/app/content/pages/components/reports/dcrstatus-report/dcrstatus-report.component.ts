import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { TypeComponent } from "../../filters/type/type.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { StateComponent } from "../../filters/state/state.component";
import { DistrictComponent } from "../../filters/district/district.component";
import { DivisionComponent } from "../../filters/division/division.component";
import { Division } from "../../../_core/models/division";
import { DcrReportsService } from "../../../../../core/services/DCR/dcr-reports.service";
declare var $;
import * as _moment from "moment";
import { StateService } from "../../../../../core/services/state.service";
import { URLService } from "../../../../../core/services/URL/url.service";
import { MatSlideToggleChange } from "@angular/material";

@Component({
	selector: "m-dcrstatus-report",
	templateUrl: "./dcrstatus-report.component.html",
	styleUrls: ["./dcrstatus-report.component.scss"],
})
export class DCRStatusReportComponent implements OnInit {
	public showState: boolean = true;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	public showDateWiseDCRReport: boolean = false;
	public showProcessing: boolean = false;
	isShowDivision = false;
	showAdminAndMGRLevelFilter = false;

	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;
	companyId = this.currentUser.companyId;
	designationLevel = this.currentUser.userInfo[0].designationLevel;
	stateList;
	constructor(
		private _changeDetectorRef: ChangeDetectorRef,
		private _dcrReportsService: DcrReportsService,
		private stateService: StateService,
		private _urlService: URLService
	) {}

	dcrstatusForm = new FormGroup({
		reportType: new FormControl("Geographical"),
		type: new FormControl(null),
		stateInfo: new FormControl(null),
		// stateInfo: new FormControl(null, Validators.required),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		designation: new FormControl(null),
		toDate: new FormControl(null, Validators.required),
		fromDate: new FormControl(null, Validators.required),
	});
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;

	@ViewChild("dataTable") table;
	dataTable: any;
	dtOptions: any;
	dataSource;

	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}
	ngOnInit() {
		this.stateService.getStates("5b2e22083225df01ba7d6059").subscribe(
			(res) => {
				this.stateList = res;
				console.log("state result", res);
			},
			(err) => {
				console.log(err);
				alert("No State Found");
			}
		);
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.dcrstatusForm.setControl("divisionId", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			if (this.currentUser.userInfo[0].rL > 1) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}
		if (
			this.currentUser.userInfo[0].designationLevel == 0 ||
			this.currentUser.userInfo[0].designationLevel > 1
		) {
			//For Admin And MGR
			this.showAdminAndMGRLevelFilter = true;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		}
	}

	getReportType(val) {
		this.dcrstatusForm.patchValue({ reporttype: val });
		if (val === "Geographical") {
			this.showState = true;
			this.showEmployee = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Geographical",
				"WithHeadquarter"
			);
		} else {
			this.showState = false;
			this.showDistrict = false;
			this.callTypeComponent.getTypesBasedOnReportingType(
				"Hierarachy",
				"empWise"
			);
		}
	}

	getTypeValue(val) {
		this.showReport = false;
		if (val == "State") {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.callStateComponent.setBlank();
			this.showState = true;
			this.showDistrict = false;
			this.dcrstatusForm.removeControl("employeeId");
			this.dcrstatusForm.removeControl("designation");
			this.dcrstatusForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
		} else if (val == "Headquarter") {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.showDistrict = true;
			this.callStateComponent.setBlank();
			this.dcrstatusForm.removeControl("employeeId");
			this.dcrstatusForm.removeControl("designation");
			this.dcrstatusForm.setControl(
				"stateInfo",
				new FormControl("", Validators.required)
			);
			this.dcrstatusForm.setControl(
				"districtInfo",
				new FormControl("", Validators.required)
			);
		} else if (val == "Employee Wise") {
			if (this.isDivisionExist === true) {
				this.callDivisionComponent.setBlank();
			}
			this.showEmployee = true;
			this.dcrstatusForm.removeControl("stateInfo");
			this.dcrstatusForm.removeControl("districtInfo");
			this.dcrstatusForm.setControl(
				"designation",
				new FormControl("", Validators.required)
			);
			this.dcrstatusForm.setControl(
				"employeeId",
				new FormControl("", Validators.required)
			);
		} else if (val == "Self") {
			this.showEmployee = false;
		}
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
		} else {
			this.isShowDivision = false;
		}
		this.dcrstatusForm.patchValue({ type: val });
		this._changeDetectorRef.detectChanges();
	}

	getDivisionValue($event) {
		console.log($event);
		
		this.dcrstatusForm.patchValue({ divisionId: $event });
		if (this.isDivisionExist === true) {
			if (
				this.dcrstatusForm.value.type === "State" ||
				this.dcrstatusForm.value.type === "Headquarter"
			) {
				this.callStateComponent.getStateBasedOnDivision({
					companyId: this.currentUser.companyId,
					isDivisionExist: this.isDivisionExist,
					division: this.dcrstatusForm.value.divisionId,
					designationLevel: this.currentUser.userInfo[0]
						.designationLevel,
				});
			}
			if (this.dcrstatusForm.value.type === "Employee Wise") {
				//getting employee if we change the division filter after select the designation
				if (
					this.dcrstatusForm.value.divisionId != null &&
					this.dcrstatusForm.value.designation != null
				) {
					if (this.dcrstatusForm.value.designation.length > 0) {
						this.callEmployeeComponent.getEmployeeListBasedOnDivision(
							{
								companyId: this.companyId,
								division: this.dcrstatusForm.value.divisionId,
								status: [true],
								designationObject: this.dcrstatusForm.value
									.designation,
							}
						);
					}
				}
			}
		}
	}

	getStateValue(val) {
		this.dcrstatusForm.patchValue({ stateInfo: val });
		if (this.isDivisionExist === true) {
			if (this.dcrstatusForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistrictsBasedOnDivision({
					designationLevel: this.designationLevel,
					companyId: this.companyId,
					stateId: this.dcrstatusForm.value.stateInfo,
					isDivisionExist: this.isDivisionExist,
					division: this.dcrstatusForm.value.divisionId,
				});

				//this.callDistrictComponent.getDistricts(this.currentUser.companyId, val, true);
			}
		} else {
			if (this.dcrstatusForm.value.type == "Headquarter") {
				this.callDistrictComponent.getDistricts(
					this.currentUser.companyId,
					val,
					true
				);
			}
		}
	}

	getDistrictValue(val) {
		this.dcrstatusForm.patchValue({ districtInfo: val });
	}

	getEmployeeValue(val) {
		this.dcrstatusForm.patchValue({ employeeId: val });
	}
	getFromDate(val) {
		this.dcrstatusForm.patchValue({ fromDate: val });
	}
	getToDate(val) {
		this.dcrstatusForm.patchValue({ toDate: val });
	}
	getDesignation(val) {
		this.dcrstatusForm.patchValue({ designation: val });
		if (this.isDivisionExist === true) {
			this.callEmployeeComponent.getEmployeeListBasedOnDivision({
				companyId: this.companyId,
				division: this.dcrstatusForm.value.divisionId,
				status: [true],
				designationObject: this.dcrstatusForm.value.designation,
			});
		} else {
			this.callEmployeeComponent.getEmployeeList(
				this.currentUser.companyId,
				val,
				[true]
			);
		}
	}

	dcrStatusReport() {
		this.isToggled = false;
		this.showReport = false;
		this.showProcessing = true;
		let params = {};

		if (this.isDivisionExist === true) {
			params["division"] = this.dcrstatusForm.value.divisionId;
		} else {
			params["division"] = [];
		}

		params["isDivisionExist"] = this.isDivisionExist;
		params["companyId"] = this.companyId;
		params["stateIds"] = this.dcrstatusForm.value.stateInfo;
		params["districtIds"] = this.dcrstatusForm.value.districtInfo;
		params["type"] = this.dcrstatusForm.value.type;
		params["employeeId"] = this.dcrstatusForm.value.employeeId;
		params["fromDate"] = this.dcrstatusForm.value.fromDate;
		params["toDate"] = this.dcrstatusForm.value.toDate;

		this._dcrReportsService.getDCRStatusDetails(params).subscribe(
			(res) => {
				this.dataSource = res;
				this._changeDetectorRef.detectChanges();

				if (this.dataSource.length > 0) {
					this.showProcessing = false;
					this.showReport = true;
					let row = 0;

					res.map((item, index) => {
						item["index"] = index;
					});
					const PrintTableFunction = this.PrintTableFunction.bind(
						this
					);
					this.dtOptions = {
						pagingType: "full_numbers",
						paging: true,
						ordering: true,
						info: true,
						scrollY: 300,
						scrollX: true,
						fixedColumns: true,
						destroy: true,
						data: res,
						responsive: true,
						columns: [
							{
								title: "S. No.",
								data: "index",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data + 1;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Division Name",
								data: "divisionName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Employee Name",
								data: "UserName",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "HeadQuater",
								data: "Headquarter",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Designation",
								data: "UserDesignation",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Employee Code",
								data: "employeeCode",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "Last DCR Date",
								data: "lastDCRDate",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return _moment(data).format(
											"DD-MM-YYYY"
										);
									}
								},
								defaultContent: "---",
							},
							{
								title: "Mobile",
								data: "Mobile",
								render: function (data) {
									if (data === "") {
										return "---";
									} else {
										return data;
									}
								},
								defaultContent: "---",
							},
							{
								title: "DCRStatus",
								data: "DCRStatus",
								render: function (data) {
									if (data === "") {
										return "---";
									} else if (data == "YES") {
										return `<a style="color: blue;">${data}</a>`;
									} else if (data == "NO") {
										return `<a style="color: red;">${data}</a>`;
									}
								},
								defaultContent: "---",
							},
						],
						language: {
							search: "_INPUT_",
							searchPlaceholder: "Search Records",
						},
						dom: "Bfrtip",
						// buttons: [
						//   'copy', 'csv', 'excel', 'print'
						// ]

						buttons: [
							{
								extend: "copy",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "csv",
								exportOptions: {
									columns: ":visible",
								},
							},
							{
								extend: "excel",
								exportOptions: {
									columns: ":visible",
								},
							},

							//********************* UMESH 19-03-2020 ************************ */
							{
								extend: "print",
								title: function () {
									return "State List";
								},
								exportOptions: {
									columns: ":visible",
								},
								action: function (e, dt, node, config) {
									PrintTableFunction(res);
								},
							},
						],
					};
					this._changeDetectorRef.detectChanges();
				}
				if (this.currentUser.company.isDivisionExist == false) {
					this.dtOptions.columns[1].visible = false;
				}
				this.dataTable = $(this.table.nativeElement);
				this.dataTable.DataTable(this.dtOptions);
				this._changeDetectorRef.detectChanges();
			},
			(err) => {
				console.log(err);
				this._changeDetectorRef.detectChanges();
			}
		);
	}

	///*****************************************UMESH 19-03-2020 *************/

	PrintTableFunction(data?: any): void {
		//const name = this.currentUser.empInfo.empFName + ' ' + this.currentUser.empInfo.empMName + ' ' + this.currentUser.empInfo.empLName;
		const currentDateAndTime = _moment(new Date()).format(
			"DD/MM/YYYY, dddd , HH:mm A"
		);
		const department = this.currentUser.section;
		const designation = this.currentUser.designation;
		const companyName = this.currentUser.companyName;

		const logoURL =
			this._urlService.API_ENDPOINT_CONTAINER +
			"/" +
			this.currentUser.company.logo.container +
			"/download/" +
			this.currentUser.company.logo.modifiedFileName;

		let tableRows: any = [];
		data.forEach((element) => {
			tableRows.push(`<tr>
      ${ this.isDivisionExist ? `<td class="tg-oiyu">${element.divisionName}</td>`: "" } 
    <td class="tg-oiyu">${element.UserName}</td>
    <td class="tg-oiyu">${element.Headquarter}</td>
    <td class="tg-oiyu">${element.UserDesignation}</td>
    <td class="tg-oiyu">${element.employeeCode}</td>
    <td class="tg-oiyu">${element.lastDCRDate}</td>
    <td class="tg-oiyu">${element.Mobile}</td>
    <td class="tg-oiyu">${element.DCRStatus}</td>
    </tr>`);
		});

		let showHeaderAndTable: boolean = false;
		//this.globalFilterObject == undefined ? showHeaderAndTable = false : showHeaderAndTable = true;

		let printContents, popupWin;
		popupWin = window.open(
			"",
			"_blank",
			"top=0,left=0,height=100%,width=auto"
		);
		popupWin.document.open();
		popupWin.document.write(`
    <html>
    <head>
    <style>
    img {
    float: left;
    }

    .flex-container {
    display: flex;
    justify-content: space-between;
    }

    .tg {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }

    .tg td {
    font-family: Arial, sans-serif;
    font-size: 14px;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tg th {
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
    padding: 6px 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tg .tg-lboi {
    border-color: inherit;
    text-align: left;
    vertical-align: middle;

    }

    .tg .tg-u6fn {
    font-weight: bold;
    font-size: 18px;
    background-color: #efefef;
    color:black;
    border-color: inherit;
    text-align: left;
    vertical-align: top;
    padding-top: 0px;
    padding-bottom: 0px;
    }

    .tg .tg-yz93 {
    border-color: inherit;
    text-align: right;
    vertical-align: middle
    }

    .tb2 {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    margin-top: 10px;
    }

    .tb2 td {
    font-family: Arial, sans-serif;
    font-size: 12px;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tb2 th {
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-weight: normal;
    padding: 2px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
    }

    .tb2 .tg-oiyu {
    font-size: 10px;
    text-align: left;
    vertical-align: middle
    }
    .tg-bckGnd {
    font-weight: bold;
    background-color: #efefef;
    }


    .tb2 .tg-3ppa {
    font-size: 10px;
    background-color: #efefef;
    color:black;
    text-align: left;
    vertical-align: middle;
    padding-top: 2px;
    padding-bottom: 2px;
    }

    .footer {
    position: sticky;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #efefef;
    color: rgb(2, 2, 2);
    text-align: right
    }



    button {
    background-color: #008CBA;
    border: none;
    color: white;
    margin: 5px 0px;
    padding: 2px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    border-radius: 12px;
    cursor: pointer;
    outline: none;
    }

    @media print {
    @page { size: A4; margin: 5mm; }

    .footer {
    position: relative;
    }
    tr {
    page-break-inside: avoid;
    }

    button{
    display : none;
    }
    }
    </style>
    </head>
    <body onload="window.print();window.close()" style="padding-bottom:30px">
    <div class="flex-container">
    <div style="width: 40%; ">
    <img src=${logoURL} style="width:100px;height:60px;margin-right:5px;">
    <h2 style="margin: 5px; margin-right: 7px"> &nbsp;${companyName}</h2>

    </div>

    <div style="text-align: right;">
    <h2 style="margin-top: 0px; margin-bottom: 5px">DCR Status Report</h2>
    <div>
    <button data-toggle="tooltip" title="Print the Data." onClick="window.print()">Print</button>
    <button style="background-color: red;" data-toggle="tooltip" title="Close the Window." onClick="window.close()">Close</button>
    </div>
    </div>
    </div>

    <div>
    ${
		/*<h2 style="margin-top: 15px; margin-bottom:5px;">Rules Created for the Schemes</h2>*/ ""
	}
    <table class="tb2">
    <tr>
    ${
		this.isDivisionExist
			? `<th class="tg-3ppa"><span style="font-weight:600">Division Name</span></th>`
			: ""
	} 
    <th class="tg-3ppa"><span style="font-weight:600">User Name</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Headquarter</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Designation</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Employee Code</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Last DCR Date</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">Mobile</span></th>
    <th class="tg-3ppa"><span style="font-weight:600">DCR Status</span></th>
    </tr>
    ${tableRows.join("")}
    </table>
    </div>
    <div class="footer flex-container">
    <p style="margin: 5px; margin-right: 7px"> &nbsp;${designation}</p>

    <p style="margin: 5px; margin-right: 7px">${currentDateAndTime}</p>
    </div>
    </body>
    </html>
    `);

		//popupWin.document.close();
	}
}
