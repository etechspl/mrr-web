import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DCRStatusReportComponent } from './dcrstatus-report.component';

describe('DCRStatusReportComponent', () => {
  let component: DCRStatusReportComponent;
  let fixture: ComponentFixture<DCRStatusReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DCRStatusReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DCRStatusReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
