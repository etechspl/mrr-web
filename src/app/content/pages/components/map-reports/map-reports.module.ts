import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule} from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapReportsComponent } from './map-reports.component';
import { EmployeeLocationTrackerComponent } from './employee-location-tracker/employee-location-tracker.component';
import { PartialsModule } from '../../../partials/partials.module';
import {
	MatInputModule,
	MatListModule,
	MatDialogModule,
	MatTableModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatSelectModule,
	MatMenuModule,
	MatButtonModule,
	MatTabsModule,
	MatNativeDateModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MatTooltipModule,
	MatGridListModule,
	MatRadioModule,
	MatProgressBarModule,
	MatIconModule,
	MatCardModule,
	MatPaginatorModule,
	MatCheckboxModule,
	MatSnackBarModule,
	MatSlideToggleModule
} from '@angular/material'
import { FiltersModule } from '../filters/filters.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TaggedLocationComponent } from './tagged-location/tagged-location.component';
const routes: Routes = [
	{
		path: '',
		component: MapReportsComponent,
		children: [
			{	
				path: 'employeelocationtracker',
				component: EmployeeLocationTrackerComponent
			},
			{	
				path: 'tagLocation',
				component: TaggedLocationComponent
			}
		]
	}
];
@NgModule({
		imports: [
			MatDialogModule,
			CommonModule,
			HttpClientModule,
			PartialsModule,
			RouterModule.forChild(routes),
			FormsModule,
			ReactiveFormsModule,
			TranslateModule.forChild(),
			MatButtonModule,
			MatMenuModule,
			MatSelectModule,
			MatInputModule,
			MatTableModule,
			MatAutocompleteModule,
			MatRadioModule,
			MatIconModule,
			MatNativeDateModule,
			MatProgressBarModule,
			MatDatepickerModule,
			MatCardModule,
			MatPaginatorModule,
			MatSortModule,
			MatCheckboxModule,
			MatProgressSpinnerModule,
			MatSnackBarModule,
			MatTabsModule,
			MatTooltipModule,
			FiltersModule,
			MatGridListModule,
			MatListModule,
			MatSnackBarModule,
			MatIconModule,
			MatCardModule,
			MatPaginatorModule,
			MatSnackBarModule,
			MatSlideToggleModule
		],
 
  declarations:[
    MapReportsComponent, EmployeeLocationTrackerComponent, TaggedLocationComponent
  ]
})
export class MapReportsModule { 

}
