import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeLocationTrackerComponent } from './employee-location-tracker.component';

describe('EmployeeLocationTrackerComponent', () => {
  let component: EmployeeLocationTrackerComponent;
  let fixture: ComponentFixture<EmployeeLocationTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeLocationTrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeLocationTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
