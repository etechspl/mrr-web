import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { DistrictComponent } from '../../filters/district/district.component';
import { EmployeeComponent } from '../../filters/employee/employee.component';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DcrProviderVisitDetailsService } from '../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service';
import { LatLongService } from '../../../../../core/services/LatLong/lat-long.service';
import { MatSlideToggleChange } from '@angular/material';

@Component({
  selector: 'm-employee-location-tracker',
  templateUrl: './employee-location-tracker.component.html',
  styleUrls: ['./employee-location-tracker.component.scss']
})
export class EmployeeLocationTrackerComponent implements OnInit {
  @ViewChild('map') mapElement: any;
  map: google.maps.Map;
  mapFilterForm: FormGroup;
  check: boolean = false;
  showProcessing: boolean = false;
  constructor(private fb: FormBuilder,
    private _latLongService:LatLongService,
    private _changeDetectorRef: ChangeDetectorRef,

    private _dcrProviderVisitService:DcrProviderVisitDetailsService) { }
  @ViewChild("employeeComponent") employeeComponent: EmployeeComponent;
  @ViewChild("districtComponent") districtComponent: DistrictComponent;
  isShowReport: boolean = false;
  isLoading: boolean = false;
  currentUser = JSON.parse(sessionStorage.currentUser);
  isToggled = true; 
  toggleFilter(event: MatSlideToggleChange) {
    this.isToggled = event.checked;
  }

  createMapFilterForm() {
    this.mapFilterForm = this.fb.group({
     
    })
      this.mapFilterForm.addControl('stateId', new FormControl('', Validators.required));
      this.mapFilterForm.addControl('districtId', new FormControl('', Validators.required));
      this.mapFilterForm.addControl('userId', new FormControl('', Validators.required));
      this.mapFilterForm.addControl('date', new FormControl('', Validators.required));
  }


  ngOnInit(): void {
    this.createMapFilterForm();
    console.log(this.currentUser.company.validation.length);
    
  }
  getStateValue(stateId: string) {
    this.mapFilterForm.patchValue({ stateId: stateId, districtId: '', userId: '',date:'' });
    this.districtComponent.getDistricts(this.currentUser.companyId, stateId,true);
  }
  //get District Value
  getDistrictValue(districtId: string) {
    this.mapFilterForm.patchValue({ districtId: districtId, userId: '',date:'' });
    let obj = {
      companyId: this.currentUser.companyId,
      stateId: this.mapFilterForm.value.stateId,
      districtId: this.mapFilterForm.value.districtId
    }
    this.employeeComponent.getEmployeeListWithMgr(obj);
  }
  getUserId(userId: string) {
    this.mapFilterForm.patchValue({ userId: userId,date:'' })
  }
  getFromDate(date:string){
    this.mapFilterForm.patchValue({ date:date })
  }

  generateReport() {
    this.isToggled = false;
    this.showProcessing= true;
    if (this.mapFilterForm.valid) {
      var param={};
      param={
        companyId: this.currentUser.companyId,
        submitBy: this.mapFilterForm.value.userId,
        dcrDate: this.mapFilterForm.value.date
      }
      //--------------convert address to lat long----------------------
      // let address="Rohtak";
      // this._dcrProviderVisitService.getLatLongOnAddress(address).subscribe(res => {

      // })
      //---------------------------------------------------
    

    this._dcrProviderVisitService.getEmployeeLocationOnMap(param).subscribe(res => {
      if(res.length>0){
        this.showProcessing= false;

        this._changeDetectorRef.detectChanges();
        var centerLatLng;
        centerLatLng = new google.maps.LatLng(parseFloat(res[0].geoLocation.lat) ,parseFloat(res[0].geoLocation.long));
        this.isShowReport = true;
        const mapProperties = {
             center: centerLatLng,
             zoom: 15,
             mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement,mapProperties);
          var lat_lng = new Array();
        
          var k=1;
     
          for (var i = 0; i < res.length; i++) {	
            if(res[i].geoLocation != undefined){
              var myLatlng = new google.maps.LatLng(parseFloat(res[i].geoLocation.lat) ,parseFloat(res[i].geoLocation.long));
              lat_lng.push(myLatlng);
              var marker = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat(res[i].geoLocation.lat) ,parseFloat(res[i].geoLocation.long)),
                icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+k+'|1b7ee1|000000'  ,
                map: this.map					        
              });
        
            k=k+1;
            let latlong= this._latLongService;
            let currentUsers=this.currentUser;
            google.maps.event.addListener(marker, 'click', (function(marker, i,latlong,currentUsers) {
              return function() {	
                var infowindow = new google.maps.InfoWindow({
                  content: "Hey We are here",
                  maxWidth: 350
                 });

                 let providerType;
                 if(res[i].providerType=="RMP"){
                  providerType=currentUsers.company.lables.doctorLabel;
                 }else if(res[i].providerType=="Drug"){
                  providerType=currentUsers.company.lables.vendorLabel;
                 }else{
                  providerType=currentUsers.company.lables.stockistLabel; 
                 }
                var latlng = {lat: res[i].geoLocation.lat, long: res[i].geoLocation.long};
                var latlng1 = {lat: parseFloat(res[i].geoLocation.lat), lng: parseFloat(res[i].geoLocation.long)};
                
                latlong.getAddressOnLongLatBasis(latlng).subscribe(result=>{
                 if(result.length>0){
                   let str = ``;

                   res.forEach((j, index)=>{
                     console.log("result",j);
                     
                     
                     if(j.geoLocation !=undefined){
                       let status;
                       
                       if( res[i].geoLocation.lat === j.geoLocation.lat && res[i].geoLocation.long === j.geoLocation.long){
                        // if(this.currentUser.company.validation.hasOwnProperty['isShowOnlyLatLong']){
                        //   status = res[i].geoLocation
                        //  }else
                        //  {
                        //    status = ;
                        //  }
                         str = str+  `<tr style='border-bottom: 0.5px solid #d0d0d0;' >
                                       <td style='padding: 4px; color: #696969; font-weight: bold; background: #ffe4c8;'><span style='width: 200px; display: inline-block;white-space: initial;'> ${j.providerName}</span></td>
                                       <td style='padding: 4px; color: #696969; font-weight: bold; background: #a7ffe1;'><span style='width: 200px; display: inline-block;white-space: initial;'>${j.punchDate}</span> </td>
                                       <td style='padding: 4px; color: #696969; font-weight: bold; background: #ffe4c8;'><span style='width: 200px; display: inline-block;white-space: initial;'>${result[0].address}</span></td>
                                       <td style='padding: 4px; color: #696969; font-weight: bold; background: #a7ffe1;'><span style='width: 200px; display: inline-block;white-space: initial;'> ${j.providerType}</span></td>
                                       </tr>`;
                         j.printed  = true;
   
                       }

                     }
                    if(index == res.length-1){
                      infowindow.setContent(`<table style='font-size: 1.2rem;'>
                                              <tr>
                                              <th style='font-weight:bold; padding: 4px; background: #ffb161;'>Provider Name: </th> 
                                              <th style='font-weight:bold; padding: 4px; background: #48e2ae;'>Visit Time : </th>
                                              <th style='font-weight:bold; padding: 4px; background: #ffb161;'>Address : </th>
                                              <th style='font-weight:bold; padding: 4px; background: #48e2ae;'>Provider Type: </th>
                                              </tr>
                                              ${str}
                                              </table>`);
                                              
                      infowindow.open(this.map, marker);
                    }
                  });
                 }else{
                   var geocoder = geocoder = new google.maps.Geocoder();
                   geocoder.geocode({'location': latlng1},(results, status)=> {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {

                          var longLatObj={};
                            longLatObj= {
                                country:"a",
                                city:"a",
                                address:results[0].formatted_address,
                                loc:{


                                    "type" : "Point", 
                                    "coordinates" : [
                                       parseFloat(res[i].geoLocation.lat), 
                                       parseFloat(res[i].geoLocation.long)
                                    ]
                                }
                            }
                          latlong.submitLocationInfo(longLatObj).subscribe(res=>{});
                          let str = ``;
                          res.forEach((j, index)=>{
                            if( res[i].geoLocation.lat === j.geoLocation.lat && res[i].geoLocation.long === j.geoLocation.long){
                              str = str+  `<tr style='border-bottom: 0.5px solid #d0d0d0;'>
                                            <td style='padding: 4px; color: #696969; font-weight: bold; background: #ffe4c8;'><span style='width: 200px; display: inline-block;white-space: initial;'> ${j.providerName} </span></td>
                                            <td style='padding: 4px; color: #696969; font-weight: bold; background: #a7ffe1;'><span style='width: 200px; display: inline-block;white-space: initial;'> ${j.punchDate} </span></td>
                                            <td style='padding: 4px; color: #696969; font-weight: bold; background: #ffe4c8;'><span style='width: 200px; display: inline-block;white-space: initial;'> ${results[0].formatted_address}</span></td>
                                            <td style='padding: 4px; color: #696969; font-weight: bold; background: #a7ffe1;'><span style='width: 200px; display: inline-block;white-space: initial;'> ${j.providerType}</span></td>
                                            </tr>`;
                              j.printed  = true;
        
                            }
                            if(index == res.length-1){
                              infowindow.setContent(`<table style='font-size: 1.2rem;'>
                                                      <tr>
                                                      <th style='font-weight:bold; padding: 4px; background: #ffb161;'>Provider Name: </th> 
                                                      <th style='font-weight:bold; padding: 4px; background: #48e2ae;'>Visit Time : </th>
                                                      <th style='font-weight:bold; padding: 4px; background: #ffb161;'>Address : </th>
                                                      <th style='font-weight:bold; padding: 4px; background: #48e2ae;'>Provider Type: </th>
                                                      </tr>
                                                      ${str}
                                                      </table>`);
                                                      
                              infowindow.open(this.map, marker);
                            }
                          });
              
                          // infowindow.setContent(
                          //     "<div>" + 
                          //     "<p><span style='font-weight:bold;'>Provider Name: </span>" + res[i].providerName+" ("+providerType+")" + "</p>" +
                          //       "<p><span style='font-weight:bold;'>Visit Time : </span>" + res[i].punchDate + "</p>" +
                          //       "<p><span style='font-weight:bold;'>Address : </span>" + results[0].formatted_address+ "</p>" +
                          //     "</div>"
                          //  );
                                            
                          //  infowindow.open(this.map, marker);		            		
                                            
                        }else{
                            infowindow.setContent(
                                    "<div>" + 
                                        "<p><span style='font-weight:bold;'>No Info Found from Google: </span></p>" +
                                    "</div>"
                            );
                            infowindow.open(this.map, marker);
                        }
                      }
                    });			      
                 }
                
                })
                        	
               }
                })(marker, i,latlong,currentUsers));
            }
            
            
              }
    
          //Intialize the Path Array
          //Initialize the Direction Service
          var service = new google.maps.DirectionsService();
          
          //Set the Path Stroke Color
          var poly = new google.maps.Polyline({ map: this.map, strokeColor: '#000000' });
          var path = poly.getPath();;
          for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) <= lat_lng.length) {
                  var src = lat_lng[i];
                  var des = lat_lng[i + 1];
                  path.push(src);
                  poly.setPath(path)
                  service.route({
                      origin: src,
                      destination: des,
                      travelMode:  google.maps.TravelMode.DRIVING
                  }, function (result, status) {
                    
                      if (status == google.maps.DirectionsStatus.OK) {
                          for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            
                              path.push(result.routes[0].overview_path[i]);
                          }
                      }
                  });
            }
          }
          
      }else{
        this._changeDetectorRef.detectChanges();
        this.showProcessing= false;
        this.isShowReport = false;
        alert("No Record Found For Date,"+this.mapFilterForm.value.date+".");
      }
     
    })
  
    }
  }

  // generateReport() {
  //   if (this.mapFilterForm.valid) {
  //     var param={};
  //     param={
  //       companyId: this.currentUser.companyId,
  //       submitBy: this.mapFilterForm.value.userId,
  //       dcrDate: this.mapFilterForm.value.date
  //     }
  //     //--------------convert address to lat long----------------------
  //     let address="Rohtak";
  //     this._dcrProviderVisitService.getLatLongOnAddress(address).subscribe(res => {
  //       console.log("result-",res)

  //     })
  //     //---------------------------------------------------
    

  //   this._dcrProviderVisitService.getEmployeeLocationOnMap(param).subscribe(res => {
  //     if(res.length>0){
  //       this._changeDetectorRef.detectChanges();
  //       var centerLatLng;
  //       centerLatLng = new google.maps.LatLng(parseFloat(res[0].geoLocation.lat) ,parseFloat(res[0].geoLocation.long));
  //       this.isShowReport = true;
  //       const mapProperties = {
  //            center: centerLatLng,
  //            zoom: 15,
  //            mapTypeId: google.maps.MapTypeId.ROADMAP
  //       };
  //       this.map = new google.maps.Map(this.mapElement.nativeElement,mapProperties);
  //         var lat_lng = new Array();
        
  //         var k=1;
     
  //         for (var i = 0; i < res.length; i++) {	
  //           var myLatlng = new google.maps.LatLng(parseFloat(res[i].geoLocation.lat) ,parseFloat(res[i].geoLocation.long));
  //             lat_lng.push(myLatlng);
  //             var marker = new google.maps.Marker({
  //               position: new google.maps.LatLng(parseFloat(res[i].geoLocation.lat) ,parseFloat(res[i].geoLocation.long)),
  //               icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+k+'|1b7ee1|000000'  ,
  //               map: this.map					        
  //             });
        
  //           k=k+1;
  //           let latlong= this._latLongService;
  //           let currentUsers=this.currentUser;
  //           google.maps.event.addListener(marker, 'click', (function(marker, i,latlong,currentUsers) {
  //             return function() {	
  //               var infowindow = new google.maps.InfoWindow({
  //                 content: "Hey We are here",
  //                 maxWidth: 350
  //                });

  //                let providerType;
  //                if(res[i].providerType=="RMP"){
  //                 providerType=currentUsers.company.lables.doctorLabel;
  //                }else if(res[i].providerType=="Drug"){
  //                 providerType=currentUsers.company.lables.vendorLabel;
  //                }else{
  //                 providerType=currentUsers.company.lables.stockistLabel; 
  //                }
  //               var latlng = {lat: res[i].geoLocation.lat, long: res[i].geoLocation.long};
  //               var latlng1 = {lat: parseFloat(res[i].geoLocation.lat), lng: parseFloat(res[i].geoLocation.long)};
                
                
  //               latlong.getAddressOnLongLatBasis(latlng).subscribe(result=>{
  //                if(result.length>0){
  //                 infowindow.setContent(
  //                                         "<div>" + 
  //                                         "<p><span style='font-weight:bold;'>Provider Name: </span>" + res[i].providerName+" ("+providerType+")" + "</p>" +
  //                                           "<p><span style='font-weight:bold;'>Visit Time : </span>" + res[i].punchDate + "</p>" +
  //                                         "<p><span style='font-weight:bold;'>Address : </span>" + result[0].address+ "</p>" +
  //                                             "</div>"
  //                                           );
                                          
  //                                         infowindow.open(this.map, marker);
  //                }else{
  //                  var geocoder = geocoder = new google.maps.Geocoder();
  //                  geocoder.geocode({'location': latlng1},(results, status)=> {
  //                   if (status == google.maps.GeocoderStatus.OK) {
  //                       if (results[0]) {

  //                         var longLatObj={};
  //                           longLatObj= {
  //                               country:"a",
  //                               city:"a",
  //                               address:results[0].formatted_address,
  //                               loc:{

  //                                   "type" : "Point", 
  //                                   "coordinates" : [
  //                                      parseFloat(res[i].geoLocation.lat), 
  //                                      parseFloat(res[i].geoLocation.long)
  //                                   ]
  //                               }
  //                           }
  //                         latlong.submitLocationInfo(longLatObj).subscribe(res=>{});
              
  //                         infowindow.setContent(
  //                             "<div>" + 
  //                             "<p><span style='font-weight:bold;'>Provider Name: </span>" + res[i].providerName+" ("+providerType+")" + "</p>" +
  //                               "<p><span style='font-weight:bold;'>Visit Time : </span>" + res[i].punchDate + "</p>" +
  //                               "<p><span style='font-weight:bold;'>Address : </span>" + results[0].formatted_address+ "</p>" +
  //                             "</div>"
  //                          );
                                            
  //                          infowindow.open(this.map, marker);		            		
                                            
  //                       }else{
  //                           infowindow.setContent(
  //                                   "<div>" + 
  //                                       "<p><span style='font-weight:bold;'>No Info Found from Google: </span></p>" +
  //                                   "</div>"
  //                           );
  //                           infowindow.open(this.map, marker);
  //                       }
  //                     }
  //                   });			      
  //                }
                
  //               })
                        	
  //              }
  //               })(marker, i,latlong,currentUsers));
  //             }
    
  //         //Intialize the Path Array
  //         //Initialize the Direction Service
  //         var service = new google.maps.DirectionsService();
          
  //         //Set the Path Stroke Color
  //         var poly = new google.maps.Polyline({ map: this.map, strokeColor: '#000000' });
  //         var path = poly.getPath();;
  //         for (var i = 0; i < lat_lng.length; i++) {
  //           if ((i + 1) < lat_lng.length) {
  //                 var src = lat_lng[i];
  //                 var des = lat_lng[i + 1];
  //                 path.push(src);
  //                 poly.setPath(path)
  //                 service.route({
  //                     origin: src,
  //                     destination: des,
  //                     travelMode:  google.maps.TravelMode.DRIVING
  //                 }, function (result, status) {
                    
  //                     if (status == google.maps.DirectionsStatus.OK) {
  //                         for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            
  //                             path.push(result.routes[0].overview_path[i]);
  //                         }
  //                     }
  //                 });
  //           }
  //         }
          
  //     }else{
  //       this._changeDetectorRef.detectChanges();

  //       this.isShowReport = false;
  //       alert("No Record Found");
  //     }
     
  //   })
  
  //   }
  // }
 

}
 


