import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaggedLocationComponent } from './tagged-location.component';

describe('TaggedLocationComponent', () => {
  let component: TaggedLocationComponent;
  let fixture: ComponentFixture<TaggedLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaggedLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaggedLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
