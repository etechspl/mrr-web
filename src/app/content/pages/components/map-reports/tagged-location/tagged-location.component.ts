import { Component, OnInit, ChangeDetectorRef,ElementRef } from "@angular/core";
import { ViewChild } from "@angular/core";
import {} from "googlemaps";
import { DistrictComponent } from "../../filters/district/district.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import {
	FormGroup,
	FormBuilder,
	FormControl,
	Validators,
} from "@angular/forms";
import { DcrProviderVisitDetailsService } from "../../../../../core/services/DCRProviderVisitDetails/dcr-provider-visit-details.service";
import { LatLongService } from "../../../../../core/services/LatLong/lat-long.service";
import {
	MatSlideToggleChange,
	MatTableDataSource,
	MatPaginator,
	MatSort,
} from "@angular/material";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
import Swal from "sweetalert2";
import { SelectionModel } from "@angular/cdk/collections";
import { DivisionComponent } from "../../filters/division/division.component";
import { ExportAsService, ExportAsConfig} from 'ngx-export-as';
import * as XLSX from "xlsx";
import { URLService } from '../../../../../core/services/URL/url.service';
import { StateComponent } from "../../filters/state/state.component";
@Component({
	selector: "m-tagged-location",
	templateUrl: "./tagged-location.component.html",
	styleUrls: ["./tagged-location.component.scss"],
})
export class TaggedLocationComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	rl = this.currentUser.userInfo[0].rL;
	isDivisionExist = this.currentUser.company.isDivisionExist;

	@ViewChild("map") mapElement: any;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild('TABLE') table: ElementRef;
	isShowDivision: boolean = false;

	map: google.maps.Map;
	mapFilterForm: FormGroup;
	check: boolean = false;
	showProcessing: boolean = false;
	dataSource: MatTableDataSource<any>;
	selection = new SelectionModel<any>(true, []);

	exportAsConfig: ExportAsConfig = {
		type: "xlsx", // the type you want to download
		elementId: "tableId1", // the id of html/table element
	};

	displayedColumns = [
		"select",
		"sn",
		"providerName",
		"providerCode",
		"specialization",
		"degree",
	];
	/**
	 * provider count variables
	 */
	RMPCount: number = 0;
	drugCount: number = 0;
	showRMPCount: boolean = false;
	showDrugCount: boolean = false;
	showProviderDetail: boolean = false;
	showSearchAddress: boolean = false;
	showMap: boolean = false;
	selectedProviders: any[];

	constructor(
		private fb: FormBuilder,
		private _latLongService: LatLongService,
		private _changeDetectorRef: ChangeDetectorRef,
		private _dcrProviderVisitService: DcrProviderVisitDetailsService,
		private exportAsService: ExportAsService,
		private _providerMaster: ProviderService,
		private _urlService: URLService
	) {}
	@ViewChild("employeeComponent") employeeComponent: EmployeeComponent;
	@ViewChild("districtComponent") districtComponent: DistrictComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;

	isShowReport: boolean = false;
	isLoading: boolean = false;
	isToggled = true;
	toggleFilter(event: MatSlideToggleChange) {
		this.isToggled = event.checked;
	}

	createMapFilterForm() {
		this.mapFilterForm = this.fb.group({
			stateId: new FormControl("", Validators.required),
			districtId: new FormControl("", Validators.required),
			userId: new FormControl("", Validators.required),
			// address: new FormControl("", Validators.required),
		});
	}

	ngOnInit(): void {
		this.createMapFilterForm();
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.mapFilterForm.setControl("divisionId", new FormControl());
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			if (this.rl == 2) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}
	}

	getDivisionValue(event) {
		this.mapFilterForm.patchValue({ divisionId: event });
		if (this.isDivisionExist === true) {
			this.callStateComponent.getStateBasedOnDivision({
				companyId: this.currentUser.companyId,
				isDivisionExist: this.isDivisionExist,
				division: this.mapFilterForm.value.divisionId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			});
		}
	}
	getStateValue(stateId: string) {
		this.mapFilterForm.patchValue({
			stateId: stateId,
		});
		this.districtComponent.getDistricts(
			this.currentUser.companyId,
			stateId,
			true
		);
	}
	//get District Value
	getDistrictValue(districtId: string) {
		this.mapFilterForm.patchValue({
			districtId: districtId,
		});
		let obj = {
			companyId: this.currentUser.companyId,
			stateId: this.mapFilterForm.value.stateId,
			districtId: this.mapFilterForm.value.districtId,
		};
		this.employeeComponent.getEmployeeListWithMgr(obj);
	}
	getUserId(userId: string) {
		this.mapFilterForm.patchValue({ userId: userId });
	}

	// to get list of all the tagged and untagged providers
	getAllProviders() {
		this.isToggled = false;
		this.mapFilterForm.setControl("address", new FormControl()); //to add new "address" FormControl
		let queryRMP = {};
		let queryDrug = {};
		this.showProcessing = true;
		if (this.rl == 0 || this.rl == 2) {
			queryRMP = {
				userId: this.mapFilterForm.value.userId,
				providerType: "RMP",
				appStatus: "approved",
				geoLocation: [],
				delStatus:"",
				status: true,
			};
			queryDrug = {
				userId: this.mapFilterForm.value.userId,
				providerType: "Drug",
				appStatus: "approved",
				delStatus:"",
				geoLocation: [],
				status: true,
			};
		}

		this._providerMaster
			.getCrmProvidersCount(queryRMP)
			.subscribe((rmpCount) => {
				this.RMPCount = rmpCount.count;
				this.showRMPCount = true;
				this.showProcessing = false;
				this._providerMaster
					.getCrmProvidersCount(queryDrug)
					.subscribe((drugCount) => {
						this.drugCount = drugCount.count;
						this.showDrugCount = true;
						this.showProcessing = false;
						!this._changeDetectorRef["destroyed"]
							? this._changeDetectorRef.detectChanges()
							: null;
					});
				!this._changeDetectorRef["destroyed"]
					? this._changeDetectorRef.detectChanges()
					: null;
			});
	}

	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	// To Apply filter option
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	getProviderForTagging(providerType) {
		this.selection.clear();
		this.dataSource = new MatTableDataSource<any>([]);

		this.showProcessing = true;
		let where = {};
		if (this.rl == 0 || this.rl == 2) {
			where = {
				userId: this.mapFilterForm.value.userId,
				providerType: providerType,
				geoLocation: [],
				appStatus: "approved",
				status: true,
			};
		}

		this._providerMaster
			.getProvidersList({ where })
			.subscribe((providerList) => {
				this.showProcessing = false;
				if (providerList.length > 0) {
					this.showProviderDetail = true;
					!this._changeDetectorRef["destroyed"]
						? this._changeDetectorRef.detectChanges()
						: null;
				} else {
					this.showProcessing = false;
					(Swal as any).fire({
						title: `No providers are remaining to be tagged !!`,
						type: "info",
					});
				}
				this.dataSource = new MatTableDataSource(providerList);
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
				}, 100);
				setTimeout(() => {
					this.dataSource.sort = this.sort;
				}, 100);
			});
		!this._changeDetectorRef["destroyed"]
			? this._changeDetectorRef.detectChanges()
			: null;
	}

	// to show the Address Input field
	getSearchAddress() {
		this.showSearchAddress = true;
		setTimeout(() => {
			this.map = new google.maps.Map(this.mapElement.nativeElement, {
				zoom: 5,
				center: { lat: 20.5937, lng: 78.9629 },
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
		}, 1000);
	}

	// to patch the address into the formGroup
	getAddress(event) {
		this.mapFilterForm.patchValue({ address: event.target.value });
	}

	// To get location on map from the input
	getLocation() {
		let centerLatLng = new google.maps.LatLng(23.63936, 68.14712);
		this.showMap = true;
		this.map = new google.maps.Map(this.mapElement.nativeElement, {
			zoom: 15,
			center: centerLatLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		});
		var geocoder = new google.maps.Geocoder();
		this.geocodeAddress(geocoder, this.map);
	}

	geocodeAddress(geocoder, resultsMap) {
		let address = this.mapFilterForm.value.address;
		let obj = {
			address: address,
		};
		this._latLongService.getTaggedAddress(obj).subscribe((locRes) => {
			console.log("location res=>", locRes);
			if (locRes.length > 0) {
				geocoder.geocode({ address: address }, (results, status) => {
					if (status === "OK") {
						console.log("location =>", results);

						resultsMap.setCenter(results[0].geometry.location);
						var marker = new google.maps.Marker({
							map: resultsMap,
							animation: google.maps.Animation.DROP,
							position: results[0].geometry.location,
						});
						google.maps.event.addListener(
							marker,
							"click",
							((marker) => {
								return () => {
									var infowindow = new google.maps.InfoWindow(
										{
											content: "Hey!! We are here",
										}
									);
									let str = ``;
									this.selection.selected.forEach(
										(provider: any, index) => {
											str =
												str +
												`<tr style='border-bottom: 0.5px solid #d0d0d0;'>
										<td style='padding: 4px; color: #696969; font-weight: bold; background: #FFF9C4;'><span style='width: 200px; display: inline-block;white-space: initial;'> ${provider.providerName}</span></td>
										<td style='padding: 4px; color: #696969; font-weight: bold; background: #ffcdd2;'><span style='width: 200px; display: inline-block;white-space: initial;'>${results[0].formatted_address}</span></td>
										</tr>`;
											provider.printed = true;
											if (
												index ==
												this.selection.selected.length -
													1
											) {
												infowindow.setContent(`<table style='font-size: 1.2rem;'>
						   <tr style="line-height: 2rem; font-size: 15px; text-transform: uppercase; letter-spacing: 0.5px;">
						   <th style='font-weight:bold; padding: 4px; background: #16a184;border: 1px solid white;'>Provider Name </th> 
						   <th style='font-weight:bold; padding: 4px; background: #16a184;border: 1px solid white;'>Address </th>
						   </tr>
						   ${str}
						   </table>
						   <div style="padding: 1rem; text-align: center;">
						   <button  id="tagLocation" class="btn btn-success">Tag This Place</button>  
						 </div>
						   `);
												infowindow.open(
													this.map,
													marker
												);
												infowindow.addListener(
													"domready",
													() => {
														document
															.getElementById(
																"tagLocation"
															)
															.addEventListener(
																"click",
																() => {
																	this.tagLocation(
																		locRes[0]
																	);
																}
															);
													}
												);
											}
										}
									);
								};
							})(marker)
						);
					} else {
						alert(
							"Geocode was not successful for the following reason: " +
								status
						);
					}
				});
			}
		});
	}

	tagLocation(address) {
		let providerIdArr = [];
		this.selection.selected.forEach((element) => {
			providerIdArr.push(element.id);
		});
		let where = {
			_id: { inq: providerIdArr },
		};
		let data = {
			geoLocation: [
				{
					lat: address.latitude,
					long: address.longitude,
				},
			],
			updatedAt: new Date(),
		};

		this._providerMaster
			.updateTaggedLocation(where, data)
			.subscribe((res) => {
				this.showRMPCount = false;
				this.showDrugCount = false;
				this.showProviderDetail = false;
				this.showSearchAddress = false;
				this.getAllProviders();
				(Swal as any).fire({
					title: `Location Tagged Successfully !!`,
					showConfirmButton: false,
					timer: 3000,
					timerProgressBar: true,
					type: "success",
				});
			});
	}


exportAsExcel()
  {
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);//converts a DOM TABLE element to a worksheet
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Provider.xlsx');

  }

}
