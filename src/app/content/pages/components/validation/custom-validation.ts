import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ValidationErrors, AbstractControl, AsyncValidatorFn, FormArray } from "@angular/forms";
import { STPService } from '../../../../core/services/stp.service';
import { promise } from 'protractor';

export function uniqueEmailValidator(userDetailsService): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
        var obj = {
            "email": c.value
        }
    console.log(obj);
    
        return userDetailsService.checkUniqueness(obj).pipe(
            map(users => {
                console.log(users)
                if (users[0] !== undefined && users[0].hasOwnProperty('email') === true) {
                    return { 'unique': true, 'message': 'Email Already Exists' };
                } else {
                    return null
                }

            })
        )
    }
}

export function uniqueUserNameValidator(userDetailsService): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
        var obj = {
            "username": c.value
        }
        return userDetailsService.checkUniqueness(obj).pipe(
            map(users => {
                if (users[0] !== undefined && users[0].hasOwnProperty('email') === true) {
                    return { 'unique': true, 'message': 'Username Already Exists' };
                } else {
                    return null
                }

            })
        )
    }
}

export function uniqueMobileValidator(userDetailsService): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
        var obj = {
            "mobile": c.value
        }
        return userDetailsService.checkUniqueness(obj).pipe(
            map(users => {

                if (users[0] !== undefined && users[0].hasOwnProperty('mobile') === true) {
                    
                    return { 'unique': true, 'message': 'Mobile Number Already Exists' };
                } else {
                    
                    return null
                }

            })
        )
    }
}

export function passwordValidator(c: AbstractControl): any {
    if (c.value !== null && c.value !== undefined) {
        const confirmPassword = c.value;
        const passwordControl = c.root.get('password');
        if (passwordControl) {
            const passwordValue = passwordControl.value;
            if (passwordValue !== confirmPassword) {
                return { 'isMatch': true, 'message': 'Password and Confirm password should match!!' };
            }
        }
    }
    return null;
}

export function checkSTPExists(stpService: STPService, index: number): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
        let toArea = c.value;
        let data = c.root.get('data') as FormArray;
        let fromArea = data.controls[index].get('fromArea').value;
        if (fromArea !== undefined && fromArea !== null) {
            let obj = {
                fromArea: fromArea,
                toArea: toArea
            };
            return stpService.checkSTPExisit(obj).pipe(
                map(stp => {
                    if (stp[0] !== undefined) {
                        return { 'unique': true, 'message': 'STP Already is created' };
                    } else {
                        return null;
                    }
                })
            )
        }
    }
}
// export function checkSTPExistsInSelectedValues(index: number): AsyncValidatorFn {
//     return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
//         let toArea = c.value;
//         let data = c.root.get('data') as FormArray;
//         let fromArea = data.controls[index].get('fromArea').value;
//         if (fromArea !== undefined && fromArea !== null) {
//             let obj = {
//                 fromArea: fromArea,
//                 toArea: toArea
//             };
//             data.controls

//             for (let i = 0; i < data.controls.length; i++) {
//                 console.log(data.controls[i])
//                 return 
//             }

//             return null;
//         }
//     }
// }



export class CustomValidation {
}