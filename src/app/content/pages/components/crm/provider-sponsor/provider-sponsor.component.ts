import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import {
	FormGroup,
	FormControl,
	Validators,
	FormBuilder,
	FormArray,
} from "@angular/forms";
import { MatPaginator, MatTableDataSource, MatSort } from "@angular/material";
import { MonthComponent } from "../../filters/month/month.component";
import { SelectionModel } from "@angular/cdk/collections";
import { AreaService } from "../../../../../core/services/Area/area.service";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
import { CrmService } from "../../../../../core/services/CRM/crm.service";
import Swal from "sweetalert2";
@Component({
	selector: "m-provider-sponsor",
	templateUrl: "./provider-sponsor.component.html",
	styleUrls: ["./provider-sponsor.component.scss"],
})
export class ProviderSponsorComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild("callMonthComponent") callMonthComponent: MonthComponent;
	displayedColumns = [
		"select",
		"sn",
		"providerName",
		"providerCode",
		"specialization",
		"degree",
	];
	displayedColumns2 = [
		"sn",
		"providerName",
		"providerCode",
		"investmentType",
		"investmentAmount",
		"investmentMonth",
		"expectedBusiness",
		"monthOfNextInvestment",
		"lastYearInvestment",
	];

	dataSource: MatTableDataSource<any>;
	dataSource2: MatTableDataSource<any>;

	investmentTypes = [
		{ value: "business", viewValue: "Business" },
		{ value: "gift", viewValue: "Gift" },
		{ value: "specialOffer", viewValue: "Special Offer" },
		{ value: "services", viewValue: "Services" },
		{ value: "monthly", viewValue: "Monthly" },
		{ value: "quarterly", viewValue: "Quarterly" },
		{ value: "halfYearly", viewValue: "Half Yearly" },
		{ value: "yearly", viewValue: "Yearly" },
	];

	selection = new SelectionModel<any>(true, []);
	selectedValue = [];
	showLinkedProviderDetail: boolean = false;
	showSponsorshipTable: boolean = false;
	showProcessing: boolean = false;
	month: number;
	year: number;

	constructor(
		private _providerMaster: ProviderService,
		private _crmService: CrmService,
		private changeDetectorRef: ChangeDetectorRef,
		private fb: FormBuilder,
		private _areaService: AreaService
	) {}

	areaData: any;
	// blockIds = ["5c233152aef91d42dcbb08f6"];
	blockIds = [];

	ngOnInit() {}

	sponsorshipForm = new FormGroup({
		stateInfo: new FormControl(null, Validators.required),
		sponsorshipArray: this.fb.array([]),
	});
	get sponsorshipArray() {
		return this.sponsorshipForm.get("sponsorshipArray") as FormArray;
	}

	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	getMonth(event) {
		this.month = event;
	}
	getYear(event) {
		this.year = event;
	}

	getLinkedProvider() {
		this.selection.clear();
		this.showProcessing = true;
		this.showLinkedProviderDetail = false;
		let queryFilter = {
			where: {
				userId: this.currentUser.id,
				crmStatus: true,
				appStatus: "approved",
				status: true,
			},
		};
		this._providerMaster
			.getProvidersList(queryFilter)
			.subscribe((providerList) => {
				this.showProcessing = false;
				if (providerList.length > 0) {
					this.showLinkedProviderDetail = true;
				} else {
					(Swal as any).fire({
						title: `No providers are remaining to link with CRM !!`,
						type: "info",
					});
				}
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
				this.dataSource = new MatTableDataSource(providerList);
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
				}, 100);
				setTimeout(() => {
					this.dataSource.sort = this.sort;
				}, 100);
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
			});
	}
	showSponsorship() {
		let selectedProviders: any = this.selection.selected;
		if (selectedProviders.length == 0) {
			(Swal as any).fire({
				title: `Please select the Providers !!`,
				type: "error",
			});
		} else {
			this.sponsorshipForm = this.fb.group({
				sponsorshipArray: this.fb.array([]),
			});
			this.dataSource2 = selectedProviders;
			this.showSponsorshipTable = true;
			let array = selectedProviders.map((item) => {
				this.sponsorshipArray.push(
					this.fb.group({
						crmStatus: true,
						userId: this.currentUser.id,
						companyId: this.currentUser.companyId,
						providerId: new FormControl(item.id),
						month: new FormControl(this.month),
						year: new FormControl(this.year),
						providerName: new FormControl(item.providerName),
						providerCode: new FormControl(item.providerCode),
						specialization: new FormControl(item.specialization),
						degree: new FormControl(item.degree),
						investmentType: new FormControl(
							"",
							Validators.required
						),
						investmentAmount: new FormControl(
							"",
							Validators.required
						),
						investmentMonth: new FormControl(
							"",
							Validators.required
						),
						expectedBusiness: new FormControl(
							"",
							Validators.required
						),
						monthOfNextInvestment: new FormControl(
							"",
							Validators.required
						),
						lastYearInvestment: new FormControl(
							"",
							Validators.required
						),
						dateOfProposal: new Date(),
					})
				);
				return {
					...item,
				};
			});
		}
	}

	getinvestmentMonth(event, i) {
		this.sponsorshipArray.at(i).patchValue({ investmentMonth: event });
	}
	getMonthOfNextInvestment(event, i) {
		this.sponsorshipArray
			.at(i)
			.patchValue({ monthOfNextInvestment: event });
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	submitCrmSponsorship() {
		let data;
		data = this.sponsorshipForm.value.sponsorshipArray.map(
			(data) => {
				delete data.providerName;
				delete data.providerCode;
				delete data.specialization;
				delete data.degree;
				return data;
			}
		);
		this._crmService
			.submitCrmSponsorship(data)
			.subscribe((submitSponsorship) => {
				this.showLinkedProviderDetail = false;
				this.showSponsorshipTable = false;
				this.selection.clear();
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
				(Swal as any).fire({
					title: `Sponsorship Submitted Successfully !!`,
					type: "success",
				});
			});
	}
}
