import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderSponsorComponent } from './provider-sponsor.component';

describe('ProviderSponsorComponent', () => {
  let component: ProviderSponsorComponent;
  let fixture: ComponentFixture<ProviderSponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderSponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
