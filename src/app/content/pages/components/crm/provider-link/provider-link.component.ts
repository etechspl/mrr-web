import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import {
	FormGroup,
	FormControl,
	Validators,
	FormBuilder,
	FormArray,
} from "@angular/forms";
import {
	MatTableDataSource,
	MatPaginator,
	MatSort,
	Sort,
} from "@angular/material";
import { MonthComponent } from "../../filters/month/month.component";
import { ProviderService } from "../../../../../core/services/Provider/provider.service";
import { AreaService } from "../../../../../core/services/Area/area.service";
import { SelectionModel } from "@angular/cdk/collections";
import Swal from "sweetalert2";
import { DistrictComponent } from "../../filters/district/district.component";
import { EmployeeComponent } from "../../filters/employee/employee.component";
import { TypeComponent } from "../../filters/type/type.component";
import { StatusComponent } from "../../filters/status/status.component";
import { DivisionComponent } from "../../filters/division/division.component";
import { StateComponent } from "../../filters/state/state.component";
import { CrmService } from "../../../../../core/services/CRM/crm.service";
import { DesignationComponent } from "../../filters/designation/designation.component";

@Component({
	selector: "m-provider-link",
	templateUrl: "./provider-link.component.html",
	styleUrls: ["./provider-link.component.scss"],
})
export class ProviderLinkComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.currentUser);
	isDivisionExist = this.currentUser.company.isDivisionExist;
	rl = this.currentUser.userInfo[0].rL;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild("callDistrictComponent")
	callDistrictComponent: DistrictComponent;
	@ViewChild("callEmployeeComponent")
	callEmployeeComponent: EmployeeComponent;
	@ViewChild("callTypeComponent") callTypeComponent: TypeComponent;
	@ViewChild("setStatus") setStatus: StatusComponent;
	@ViewChild("callDivisionComponent")
	callDivisionComponent: DivisionComponent;
	@ViewChild("callStateComponent") callStateComponent: StateComponent;
	@ViewChild("callDesignationComponent")
	callDesignationComponent: DesignationComponent;
	public showState: boolean = true;
	public showDistrict: boolean = false;
	public showReport: boolean = false;
	public showEmployee: boolean = false;
	isShowDivision = false;

	showProviderDetail: boolean = false;
	showSponsorshipFilter: boolean = false;
	showLinkedProviderDetail: boolean = false;
	showSponsorshipTable: boolean = false;
	showProcessing: boolean = false;
	displayedColumns = [
		"select",
		"sn",
		"providerName",
		"providerCode",
		"specialization",
		"degree",
	];
	displayedColumns2: any;
	investmentTypes = [
		{ value: "business", viewValue: "Business" },
		{ value: "gift", viewValue: "Gift" },
		{ value: "specialOffer", viewValue: "Special Offer" },
		{ value: "services", viewValue: "Services" },
		{ value: "monthly", viewValue: "Monthly" },
		{ value: "quarterly", viewValue: "Quarterly" },
		{ value: "halfYearly", viewValue: "Half Yearly" },
		{ value: "yearly", viewValue: "Yearly" },
	];
	kindOfCRM = [
		{ value: "clinicalTrial", viewValue: "Clinical Trial" },
		{ value: "cash", viewValue: "Cash" },
		{ value: "conference", viewValue: "Conference" },
		{ value: "travel", viewValue: "Travel" },
		{ value: "stay", viewValue: "Stay" },
		{ value: "reimbursement", viewValue: "Reimbursement" },
		{ value: "conveyance", viewValue: "Conveyance" },
		{ value: "pleasureTrip", viewValue: "Pleasure Trip" },
		{ value: "cme", viewValue: "CME" },
		{ value: "other", viewValue: "Other" },
	];
	modeOfPayment = [
		{ value: "cash", viewValue: "Cash" },
		{ value: "cheque", viewValue: "Cheque" },
		{ value: "NEFT", viewValue: "NEFT" },
		{ value: "IMPS", viewValue: "IMPS" },
		{ value: "RTGS", viewValue: "RTGS" },
	];
	dataSource: MatTableDataSource<any>;
	dataSource2: MatTableDataSource<any>;

	selection = new SelectionModel<any>(true, []);
	selectedValue = [];
	RMPCount: number = 0;
	drugCount: number = 0;
	showRMPCount: boolean = false;
	showDrugCount: boolean = false;

	areaData: any;
	// blockIds = ["5c233152aef91d42dcbb08f6"];
	blockIds = [];

	constructor(
		private _providerMaster: ProviderService,
		private changeDetectorRef: ChangeDetectorRef,
		private fb: FormBuilder,
		private _areaService: AreaService,
		private _crmService: CrmService
	) {}

	ngOnInit() {
		if (this.rl == 1) {
			this.getProvidersCount();
		}
		if (this.isDivisionExist === true) {
			this.isShowDivision = true;
			this.providerSponsorshipForm.setControl(
				"divisionId",
				new FormControl()
			);
			let obj = {
				companyId: this.currentUser.companyId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			};
			if (this.rl == 2) {
				obj["supervisorId"] = this.currentUser.id;
			}
			this.callDivisionComponent.getDivisions(obj);
		} else {
			this.isShowDivision = false;
		}
	}
	providerSponsorshipForm: FormGroup = this.fb.group({
		sponsorshipArray: this.fb.array([]),
		status: new FormControl([true]),
		stateInfo: new FormControl(null),
		districtInfo: new FormControl(null),
		employeeId: new FormControl(null),
		designation: new FormControl(null),
	});

	get sponsorshipArray() {
		return this.providerSponsorshipForm.get(
			"sponsorshipArray"
		) as FormArray;
	}

	clearSponsorshipArray() {
		this.providerSponsorshipForm.removeControl("sponsorshipArray");
		this.providerSponsorshipForm.addControl(
			"sponsorshipArray",
			this.fb.array([])
		);
	}

	getDivisionValue(event) {
		this.callStateComponent.setBlank();
		this.callDistrictComponent.setBlank();
		this.callDesignationComponent.setBlank();
		this.providerSponsorshipForm.patchValue({ divisionId: event });
		if (this.isDivisionExist === true) {
			this.callStateComponent.getStateBasedOnDivision({
				companyId: this.currentUser.companyId,
				isDivisionExist: this.isDivisionExist,
				division: this.providerSponsorshipForm.value.divisionId,
				designationLevel: this.currentUser.userInfo[0].designationLevel,
			});
		}
	}
	getStateValue(val) {
		this.callDistrictComponent.setBlank();
		this.callEmployeeComponent.setBlank();
		this.callDesignationComponent.setBlank();
		this.providerSponsorshipForm.patchValue({ stateInfo: val });

		if (this.isDivisionExist === true) {
			this.callDistrictComponent.getDistrictsBasedOnDivision({
				designationLevel: this.currentUser.userInfo[0].designationLevel,
				companyId: this.currentUser.companyId,
				stateId: this.providerSponsorshipForm.value.stateInfo,
				isDivisionExist: this.isDivisionExist,
				division: this.providerSponsorshipForm.value.divisionId,
			});
		} else {
			this.callDistrictComponent.getDistricts(
				this.currentUser.companyId,
				val,
				true
			);
		}
	}
	getDistrictValue(val) {
		this.callDesignationComponent.setBlank();
		this.callDesignationComponent.ngOnInit();
		this.providerSponsorshipForm.patchValue({ districtInfo: val });
	}
	getDesignation(val) {
		this.callEmployeeComponent.setBlank();

		this.providerSponsorshipForm.patchValue({ designation: val });
		if (this.currentUser.userInfo[0].rL === 0) {
			if (this.isDivisionExist === true) {
				this.callEmployeeComponent.getEmployeeListBasedOnDivision({
					companyId: this.currentUser.companyId,
					division: this.providerSponsorshipForm.value.divisionId,
					districtId: this.providerSponsorshipForm.value.districtInfo,
					status: [true],
					designationObject: this.providerSponsorshipForm.value
						.designation,
				});
			} else {
				let designationArray = val.map((element) => {
					return element.designationLevel;
				});
				this.callEmployeeComponent.getEmployeeDesignatiuonWise(
					this.currentUser.companyId,
					this.providerSponsorshipForm.value.districtInfo,
					designationArray,
					[true]
				);
			}
		} else {
			let dynamicObj = {};
			if (this.currentUser.company.isDivisionExist == false) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [val[0].designationLevel], //desig
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			} else if (this.currentUser.company.isDivisionExist == true) {
				dynamicObj = {
					supervisorId: this.currentUser.id,
					companyId: this.currentUser.companyId,
					status: true,
					type: "lower",
					designation: [val[0].designationLevel], //desig,
					isDivisionExist: true,
					division: this.providerSponsorshipForm.value.divisionId,
				};
				this.callEmployeeComponent.getManagerHierarchy(dynamicObj);
			}
		}
	}
	getStatusValue(val) {}
	getEmployeeValue(val) {
		this.providerSponsorshipForm.patchValue({ employeeId: val });
	}

	// getAreaList(companyId, districtIds, designationLevel, userIds) {
	// 	this._areaService
	// 		.getAreaList(companyId, districtIds, designationLevel, userIds)
	// 		.subscribe(
	// 			(res) => {
	// 				this.areaData = res;
	// 				if (this.areaData.length > 0) {
	// 					this.areaData.forEach((element) => {
	// 						this.blockIds.push(element.id);
	// 					});

	// 					let queryRMP = {
	// 						blockId: { inq: this.blockIds },
	// 						providerType: "RMP",
	// 						crmStatus: false,
	// 						appStatus: "approved",
	// 						status: true,
	// 					};
	// 					let queryDrug = {
	// 						blockId: { inq: this.blockIds },
	// 						providerType: "Drug",
	// 						crmStatus: false,
	// 						appStatus: "approved",
	// 						status: true,
	// 					};
	// 					this._providerMaster
	// 						.getCrmProvidersCount(queryRMP)
	// 						.subscribe((rmpCount) => {
	// 							this.RMPCount = rmpCount.count;
	// 							this.showRMPCount = true;
	// 							this.showProcessing = false;

	// 							!this.changeDetectorRef["destroyed"]
	// 								? this.changeDetectorRef.detectChanges()
	// 								: null;
	// 						});
	// 					this._providerMaster
	// 						.getCrmProvidersCount(queryDrug)
	// 						.subscribe((drugCount) => {
	// 							this.drugCount = drugCount.count;
	// 							this.showDrugCount = true;
	// 							this.showProcessing = false;
	// 							!this.changeDetectorRef["destroyed"]
	// 								? this.changeDetectorRef.detectChanges()
	// 								: null;
	// 						});
	// 				}
	// 			},
	// 			(err) => {
	// 				console.log(err);
	// 			}
	// 		);
	// }

	getProvidersCount() {
		let queryRMP = {};
		let queryDrug = {};
		this.showProcessing = true;

		if (this.rl == 1) {
			queryRMP = {
				userId: this.currentUser.id,
				providerType: "RMP",
				crmStatus: false,
				appStatus: "approved",
				status: true,
			};
			queryDrug = {
				userId: this.currentUser.id,
				providerType: "Drug",
				crmStatus: false,
				appStatus: "approved",
				status: true,
			};
		} else if (this.rl == 0 || this.rl == 2) {
			queryRMP = {
				userId: { inq: this.providerSponsorshipForm.value.employeeId },
				providerType: "RMP",
				crmStatus: false,
				appStatus: "approved",
				status: true,
			};
			queryDrug = {
				userId: this.providerSponsorshipForm.value.employeeId,
				providerType: "Drug",
				crmStatus: false,
				appStatus: "approved",
				status: true,
			};
		}

		this._providerMaster
			.getCrmProvidersCount(queryRMP)
			.subscribe((rmpCount) => {
				this.RMPCount = rmpCount.count;
				this.showRMPCount = true;
				this.showProcessing = false;

				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
			});
		this._providerMaster
			.getCrmProvidersCount(queryDrug)
			.subscribe((drugCount) => {
				this.drugCount = drugCount.count;
				this.showDrugCount = true;
				this.showProcessing = false;
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
			});
	}

	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	getProviderForLinking(providerType) {
		this.dataSource = null;
		this.showProviderDetail = false;
		this.showProcessing = true;

		let where = {};
		if (this.rl == 1) {
			where = {
				userId: this.currentUser.id,
				providerType: providerType,
				crmStatus: false,
				appStatus: "approved",
				status: true,
			};
		} else if (this.rl == 0 || this.rl == 2) {
			where = {
				userId: { inq: this.providerSponsorshipForm.value.employeeId },
				providerType: providerType,
				crmStatus: false,
				appStatus: "approved",
				status: true,
			};
		}
		this._providerMaster
			.getProvidersList({ where })
			.subscribe((providerList) => {
				this.showProcessing = false;
				if (providerList.length > 0) {
					this.showProviderDetail = true;
				} else {
					(Swal as any).fire({
						title: `No providers are remaining to link with CRM !!`,
						type: "info",
					});
				}

				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
				this.dataSource = new MatTableDataSource(providerList);
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
				}, 100);
				setTimeout(() => {
					this.dataSource.sort = this.sort;
				}, 100);
				!this.changeDetectorRef["destroyed"]
					? this.changeDetectorRef.detectChanges()
					: null;
			});
	}
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	// linking providers to CRM for MR
	updateProviderCrmStatus() {
		let selectedProvider = [];
		this.selection.selected.forEach((data) => {
			selectedProvider.push(data.id);
		});
		if (selectedProvider.length == 0) {
			(Swal as any).fire({
				title: "Please select the providers to link with CRM",
				type: "error",
			});
		} else {
			let whereQuery = {
				id: { inq: selectedProvider },
			};
			let setQuery = {
				crmStatus: true,
			};
			this._providerMaster
				.getProviderUpdateDetail(whereQuery, setQuery)
				.subscribe((updateProvider) => {
					(Swal as any).fire({
						title: `Provider Linked with CRM Successfully !!`,
						type: "success",
					});
					this.showProviderDetail = false;
					this.ngOnInit();
					this.selection.clear();
				});
		}
	}

	//For showing admin level Provider count
	// showProviders() {
	// 	this.showProcessing = true;
	// 	console.log(
	// 		"providerSponsorshipForm",
	// 		this.providerSponsorshipForm.value
	// 	);
	// 	// this.getAreaList(
	// 	// 	this.currentUser.companyId,
	// 	// 	this.providerSponsorshipForm.value.districtInfo,
	// 	// 	this.currentUser.userInfo[0].designationLevel,
	// 	// 	this.providerSponsorshipForm.value.stateInfo
	// 	// );

	// 	let queryRMP = {
	// 		userId: { inq: this.providerSponsorshipForm.value.employeeId },
	// 		providerType: "RMP",
	// 		crmStatus: false,
	// 		appStatus: "approved",
	// 		status: true,
	// 	};
	// 	let queryDrug = {
	// 		userId: this.providerSponsorshipForm.value.employeeId,
	// 		providerType: "Drug",
	// 		crmStatus: false,
	// 		appStatus: "approved",
	// 		status: true,
	// 	};
	// 	console.log("queryRMP...", queryRMP);

	// 	this._providerMaster
	// 		.getCrmProvidersCount(queryRMP)
	// 		.subscribe((rmpCount) => {
	// 			this.RMPCount = rmpCount.count;
	// 			this.showRMPCount = true;
	// 			this.showProcessing = false;

	// 			!this.changeDetectorRef["destroyed"]
	// 				? this.changeDetectorRef.detectChanges()
	// 				: null;
	// 		});
	// 	this._providerMaster
	// 		.getCrmProvidersCount(queryDrug)
	// 		.subscribe((drugCount) => {
	// 			this.drugCount = drugCount.count;
	// 			this.showDrugCount = true;
	// 			this.showProcessing = false;
	// 			!this.changeDetectorRef["destroyed"]
	// 				? this.changeDetectorRef.detectChanges()
	// 				: null;
	// 		});
	// }
	// To get a sponsorship form
	getSponsorshipForm() {
		let selectedProviders: any = this.selection.selected;
		if (selectedProviders.length == 0) {
			(Swal as any).fire({
				title: `Please select the Providers !!`,
				type: "error",
			});
		} else {
			selectedProviders.forEach((element, i) => {
				element["index"] = i;
			});

			if (this.rl == 0) {
				this.displayedColumns2 = [
					"sn",
					"providerName",
					"providerCode",
					"investmentType",
					"kindOfCRM",
					"modeOfPayment",
					"month",
					"year",
					"investmentMonth",
					"investmentAmount",
					"expectedBusiness",
					"monthOfNextInvestment",
					"lastYearInvestment",
					"paidTo",
					"billNum",
					"remarks",
				];
			} else {
				this.displayedColumns2 = [
					"sn",
					"providerName",
					"providerCode",
					"investmentType",
					// "kindOfCRM",
					// "modeOfPayment",
					"month",
					"year",
					"investmentMonth",
					"investmentAmount",
					"expectedBusiness",
					"monthOfNextInvestment",
					"lastYearInvestment",
					// "paidTo",
					// "billNum",
					"remarks",
				];
			}

			// this.providerSponsorshipForm = this.fb.group({
			// 	sponsorshipArray: this.fb.array([]),

			// });
			this.clearSponsorshipArray();

			this.dataSource2 = selectedProviders;
			this.showSponsorshipTable = true;
			let array = selectedProviders.map((item) => {
				this.sponsorshipArray.push(
					this.fb.group({
						appByAdm: this.currentUser.id,
						approvalStatusByAdmin: "approved",
						crmStatus: true,
						dateOfProposal: new Date(),
						appAdmDate: new Date(),
						userId: this.currentUser.id,
						companyId: this.currentUser.companyId,
						providerId: new FormControl(item.id),
						// month: new FormControl(""),
						// year: new FormControl(""),
						providerName: new FormControl(item.providerName),
						providerCode: new FormControl(item.providerCode),
						specialization: new FormControl(item.specialization),
						degree: new FormControl(item.degree),
						investmentType: new FormControl(
							"",
							Validators.required
						),
						investmentAmount: new FormControl(
							"",
							Validators.required
						),
						month: new FormControl("", Validators.required),
						year: new FormControl("", Validators.required),
						investmentMonth: new FormControl(
							"",
							Validators.required
						),
						expectedBusiness: new FormControl(
							"",
							Validators.required
						),
						monthOfNextInvestment: new FormControl(
							"",
							Validators.required
						),
						lastYearInvestment: new FormControl(
							"",
							Validators.required
						),
						kindOfCRM: new FormControl("", Validators.required),
						modeOfPayment: new FormControl("", Validators.required),
						index: item.index,
						paidTo: new FormControl("", Validators.required),
						billNum: new FormControl("", Validators.required),
						remarks: new FormControl("", Validators.required),
					})
				);
				return {
					...item,
				};
			});
		}
	}
	getMonth(event, i) {
		this.sponsorshipArray.at(i).patchValue({ month: event });
	}
	getYear(event, i) {
		this.sponsorshipArray.at(i).patchValue({ year: event });
	}
	getinvestmentMonth(event, i) {
		this.sponsorshipArray.at(i).patchValue({ investmentMonth: event });
	}
	getMonthOfNextInvestment(event, i) {
		this.sponsorshipArray
			.at(i)
			.patchValue({ monthOfNextInvestment: event });
	}
	submitCrmSponsorship() {
		// console.log(
		// 	"sponsorshipArray",
		// 	this.providerSponsorshipForm.value.sponsorshipArray
		// );
		let selectedProvider = [];
		this.providerSponsorshipForm.value.sponsorshipArray.forEach((data) => {
			selectedProvider.push(data.providerId);
		});
		let whereQuery = {
			id: { inq: selectedProvider },
		};
		let setQuery = {
			crmStatus: true,
		};
		this._providerMaster
			.getProviderUpdateDetail(whereQuery, setQuery)
			.subscribe((updateProvider) => {
				this.showProviderDetail = false;
				let data;
				if (this.rl == 0) {
					data = this.providerSponsorshipForm.value.sponsorshipArray.map(
						(data) => {
							delete data.providerName;
							delete data.providerCode;
							delete data.specialization;
							delete data.degree;
							data["approvalStatusByMgr"] = "approved";
							return data;
						}
					);
				} else {
					data = this.providerSponsorshipForm.value.sponsorshipArray.map(
						(data) => {
							delete data.kindOfCRM;
							delete data.modeOfPayment;
							delete data.paidTo;
							delete data.billNum;
							delete data.appByAdm;
							delete data.approvalStatusByAdmin;
							delete data.appAdmDate;
							delete data.providerName;
							delete data.providerCode;
							delete data.specialization;
							delete data.degree;
							data[
								"appAndDisAppByMgrLevel"
							] = this.currentUser.designationLevel;
							data["appAndDisAppByMgr"] = this.currentUser.id;
							data["approvalStatusByMgr"] = "approved";
							data["appAndDisAppMgrDate"] = new Date();
							return data;
						}
					);
				}
				this._crmService
					.submitCrmSponsorship(data)
					.subscribe((submitSponsorship) => {
						this.showProviderDetail = false;
						this.showSponsorshipTable = false;
						this.selection.clear();
						this.ngOnInit();
						this.showRMPCount = false;
						this.showDrugCount = false;
						this.callDivisionComponent.setBlank();
						this.callDistrictComponent.setBlank();
						this.callStateComponent.setBlank();
						this.callDesignationComponent.setBlank();
						this.clearSponsorshipArray();
						this.callEmployeeComponent.setBlank();
						!this.changeDetectorRef["destroyed"]
							? this.changeDetectorRef.detectChanges()
							: null;
						(Swal as any).fire({
							title: `Sponsorship Submitted Successfully !!`,
							type: "success",
						});
					});
			});
	}
}
