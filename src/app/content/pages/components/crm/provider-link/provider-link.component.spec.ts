import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderLinkComponent } from './provider-link.component';

describe('ProviderLinkComponent', () => {
  let component: ProviderLinkComponent;
  let fixture: ComponentFixture<ProviderLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
