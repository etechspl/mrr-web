import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CRMComponent } from './crm.component';
import { Routes, RouterModule } from '@angular/router';
import { ProviderLinkComponent } from './provider-link/provider-link.component';
import { ProviderSponsorComponent } from './provider-sponsor/provider-sponsor.component';
import { MatSliderModule, MatCardModule, MatTableModule, MatSelectModule, MatPaginatorModule, MatCheckboxModule, MatButtonModule, MatInputModule, MatProgressSpinnerModule, MatSortModule, MatIconModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FiltersModule } from '../filters/filters.module';
import { LayoutModule } from '@angular/cdk/layout';
import { PartialsModule } from '../../../partials/partials.module';

const routes: Routes = [
	{
		path: '',
		component: CRMComponent,
		children: [
			{
				path: 'crm',
				component: CRMComponent
			},
			{
				path: 'linkProviders',
				component: ProviderLinkComponent
			},
			{
				path: 'sponsorship',
				component: ProviderSponsorComponent
			}
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		MatSliderModule,
		FiltersModule,
		MatCardModule,
		MatSortModule,
		MatInputModule,
		MatProgressSpinnerModule,
		LayoutModule,
		PartialsModule,
		MatIconModule,
		MatTableModule,
		MatPaginatorModule,
		MatSelectModule,
		MatButtonModule,
		MatCheckboxModule,
		FormlyBootstrapModule,
		FormlyMaterialModule,
		RouterModule.forChild(routes),
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	declarations: [CRMComponent, ProviderLinkComponent, ProviderSponsorComponent]
})

export class CRMModule { }
