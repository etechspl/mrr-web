import { style } from '@angular/animations';
import { DesignationService } from './../../../../core/services/Designation/designation.service';
import { UserDetailsService } from './../../../../core/services/user-details.service';
import { Component, Input, Output, EventEmitter, ViewChild, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'mwl-demo-utils-calendar-header',
  template: `
  
    <div class="row text-center">
      <div class="col-md-3">
        <div class="btn-group">
          <div
            class="btn btn-primary"
            mwlCalendarPreviousView
            [view]="view"
            [(viewDate)]="viewDate"
            (viewDateChange)="viewDateChange.next(viewDate)">
            Previous
          </div>
          <div
            class="btn btn-outline-secondary"
            mwlCalendarToday
            [(viewDate)]="viewDate"
            (viewDateChange)="viewDateChange.next(viewDate)">
            Current
          </div>
          <div
            class="btn btn-primary"
            mwlCalendarNextView
            [view]="view"
            [(viewDate)]="viewDate"
            (viewDateChange)="viewDateChange.next(viewDate)">
            Next
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <h3>{{ viewDate | calendarDate:(view + 'ViewTitle'):locale }}</h3>
      </div>
      <div class="col-lg-6" *ngIf="this.currentUser.userInfo[0].designationLevel!=1">        
        <select placeholder="Select Designation" required (change)='getUserBasedOnDesignation($event.target.value)'>
          <option [value]="">Select Designation &nbsp;</option>  
          <option *ngFor="let des of designationInfo" [value]="des.designationLevel"  >
            {{des.designation}}
          </option>
        </select>&nbsp;   
            <select placeholder="Select Employee" required (change)='changedValue($event.target.value)'>
              <option [value]="">Select Employee &nbsp;</option>  
              <option value="All">All</option>  
              <option value="Self" *ngIf="this.currentUser.userInfo[0].rL>1" class="">Self</option>
              <option *ngFor="let employee of userData" [value]="employee.userId"  >
                {{employee.name}}
              </option>
            </select>
      </div>

    </div>
    <br>
    <div class="row text-center">
    <div class="col-md-12"  align="right">
      <table border="0" align="right" *ngIf="(this.currentUser.userInfo[0].designationLevel!=1 && (this.userIdDetail=='' || this.userIdDetail=='All' || this.userIdDetail==undefined))">
       <tr>
        <td style="background-color: goldenrod;color:#FFFFFF;"><span class="cal-day-badge" >&nbsp;Total DCR&nbsp;</span></td>&nbsp;
        <td  style="background-color: rgb(204, 96, 57);color:#FFFFFF;"><span class="cal-day-badge"> &nbsp;Working DCR&nbsp; </span></td>&nbsp;
        <td  style="background-color: rgb(99, 121, 194);color:#FFFFFF;"><span class="cal-day-badge">&nbsp; View By Manager &nbsp;</span></td>&nbsp;
        <td  style="background-color: rgb(54, 161, 90);color:#FFFFFF;"><span class="cal-day-badge">&nbsp; Doctor Visited &nbsp;</span></td>&nbsp;
        <td style="background-color: rgb(185, 89, 185);color:#FFFFFF;"><span class="cal-day-badge" >&nbsp; Vendor Visited &nbsp;</span></td>&nbsp;
        <td  style="background-color: rgb(78, 171, 247);color:#FFFFFF;"><span class="cal-day-badge">&nbsp; POB &nbsp;</span></td>
        </tr>
      </table>
      <table border="0" align="right" *ngIf="this.currentUser.userInfo[0].designationLevel==1 || this.userIdDetail!=='' && this.userIdDetail!='Self' && this.userIdDetail!='All'">
       <tr>
        <td style="background-color: #4AB21D;color:#000000;"><span class="cal-day-badge" >&nbsp;Submitted DCR&nbsp;</span></td>&nbsp;
        <td  style="background-color: red;color:#000000;"><span class="cal-day-badge"> &nbsp;Locked DCR&nbsp; </span></td>&nbsp;
        <td  style="background-color: yellow;color:#000000;"><span class="cal-day-badge">&nbsp;Pending DCR &nbsp;</span></td>&nbsp;
        </tr>
      </table>
    </div>
  </div>
  `,
  styles:[
    ` 
    select {
        border-radius: .25rem;
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /* border: 1px solid transparent; */
        padding: .58rem 1.00rem;
        font-size: 1rem;
        line-height: 1.25;
        border-radius: .25rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    
    }
    `
  ]
})
export class CalendarHeaderComponent {
  @Input()
  view: string;

  @Input()
  viewDate: Date;

  @Input()
  userId: string;
 

  @Input()
  locale: string = 'en';

  @Output()
  viewChange: EventEmitter<string> = new EventEmitter();

  @Output()
  viewDateChange: EventEmitter<Date> = new EventEmitter();
  @Output() changedValues = new EventEmitter();

  constructor(private _userDetailservice: UserDetailsService,private _desigantionservice:DesignationService,    private _changeDetectorRef: ChangeDetectorRef
  ) { }
  userData: any;
  designationInfo:any;
  currentUser = JSON.parse(sessionStorage.currentUser);
  userIdDetail="";  
  ngOnInit(){
    this._desigantionservice.getLowerLevelDesignations(this.currentUser.companyId, this.currentUser.userInfo[0].rL).subscribe(res => {
      this.designationInfo = res;
      this._changeDetectorRef.detectChanges();

    }, err => {
      console.log(err)
    });

  }
  getUserBasedOnDesignation(val){
    this._userDetailservice.getUserInfoBasedOnDesignation(this.currentUser.companyId, parseInt(val), [true]).subscribe(res => {
      this.userData = res;
      console.log(this.userData);
      this._changeDetectorRef.detectChanges();
    }, err => {
      console.log(err)
    });
  }
  changedValue(val) {
    this.userIdDetail=val;
    this.changedValues.emit(val);
  
  }

}
