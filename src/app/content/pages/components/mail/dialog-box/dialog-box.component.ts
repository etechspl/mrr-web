import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatOption } from '@angular/material';
import { DivisionComponent } from '../../filters/division/division.component';
import { StateComponent } from '../../filters/state/state.component';
import { DesignationComponent } from '../../filters/designation/designation.component';
import { DistrictComponent } from '../../filters/district/district.component';
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'm-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent implements OnInit {
  @ViewChild("callDivisionComponent") callDivisionComponent: DivisionComponent;
  @ViewChild("callStateComponent") callStateComponent: StateComponent;
  @ViewChild("callDistrictComponent") callDistrictComponent: DistrictComponent;
  @ViewChild("callDesignationComponent") callDesignationComponent: DesignationComponent;
  @ViewChild('allSelected') private allSelected: MatOption;
  @ViewChild('matOption') private matOption: MatOption;

  currentUser = JSON.parse(sessionStorage.currentUser);
  isDivisionExist: boolean = false;
  filterForm: FormGroup;
  showDivisionFilter: boolean = false;
  showStateFilter: boolean = false;
  showDistrictFilter: boolean = false;
  showDesignationFilter: boolean = false;
  showEmployeeFilter: boolean = false;
  whichFilter: string = "";
  constructor(
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _userDetailService: UserDetailsService,
    private _toaster: ToastrService,
  ) {
    this.whichFilter = data.whichFilter;
    this.filterForm = new FormGroup({
      reportType: new FormControl(null, Validators.required),
      division: new FormControl(null),
      state: new FormControl(null),
      district: new FormControl(null),
      designation: new FormControl(null, Validators.required),
      employee: new FormControl(null, Validators.required)
    });

  }
  ngOnInit() {
    if (this.currentUser.company.isDivisionExist) {

      this.isDivisionExist = true;
    }

  }


  reportType: string = "";
  getReportType(selectedReportType) {
    this.reportType = selectedReportType;
    this.filterForm.reset();
    if (selectedReportType.value == "division") {
      const obj = {
        companyId: this.currentUser.companyId,
        supervisorId: this.currentUser.id, //For Manager Level Login
        designationLevel: this.currentUser.userInfo[0].designationLevel
      }
      this.callDivisionComponent.getDivisions(obj);

      this.empData = [];
      this.callDivisionComponent.setBlank();
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.callDesignationComponent.setBlank();

      this.showDivisionFilter = true;
      this.showStateFilter = false;
      this.showDistrictFilter = false;
      this.showDesignationFilter = true;
      this.showEmployeeFilter = true;

      this.filterForm.controls.division.setValidators(Validators.required);
      this.filterForm.controls.division.updateValueAndValidity();
      this.filterForm.controls.state.clearValidators();
      this.filterForm.controls.state.updateValueAndValidity();
      this.filterForm.controls.district.clearValidators();
      this.filterForm.controls.district.updateValueAndValidity();
    } else if (selectedReportType.value == "state") {
      //----------Call Automatically
      this.empData = [];
      this.showDivisionFilter = false;
      this.showStateFilter = true;
      this.showDistrictFilter = false;
      this.showDesignationFilter = true;
      this.showEmployeeFilter = true;

      this.callDivisionComponent.setBlank();
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.callDesignationComponent.setBlank();

      this.filterForm.controls.division.clearValidators();
      this.filterForm.controls.division.updateValueAndValidity();
      this.filterForm.controls.state.setValidators(Validators.required);
      this.filterForm.controls.state.updateValueAndValidity();
      this.filterForm.controls.district.clearValidators();
      this.filterForm.controls.district.updateValueAndValidity();
    } else if (selectedReportType.value == "district") {
      this.empData = [];
      this.showDivisionFilter = false;
      this.showStateFilter = false;
      this.showDistrictFilter = true;
      this.showDesignationFilter = true;
      this.showEmployeeFilter = true;

      this.callDivisionComponent.setBlank();
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.callDesignationComponent.setBlank();

      this.filterForm.controls.division.clearValidators();
      this.filterForm.controls.division.updateValueAndValidity();
      this.filterForm.controls.state.clearValidators();
      this.filterForm.controls.state.updateValueAndValidity();
      this.filterForm.controls.district.setValidators(Validators.required);
      this.filterForm.controls.district.updateValueAndValidity();

      this.callDistrictComponent.getDistricts(this.currentUser.companyId, undefined, false);

    } else if (selectedReportType.value == "employee") {
      this.empData = [];
      this.showDivisionFilter = false;
      this.showStateFilter = false;
      this.showDistrictFilter = false;
      this.showDesignationFilter = true;
      this.showEmployeeFilter = true;

      this.callDivisionComponent.setBlank();
      this.callStateComponent.setBlank();
      this.callDistrictComponent.setBlank();
      this.callDesignationComponent.getAllDesignationForMail({
        companyId: this.currentUser.companyId,
        userLevel: this.currentUser.userInfo[0].designationLevel,
        userId: this.currentUser.id
      });

      this.filterForm.controls.division.clearValidators();
      this.filterForm.controls.division.updateValueAndValidity();
      this.filterForm.controls.state.clearValidators();
      this.filterForm.controls.state.updateValueAndValidity();
      this.filterForm.controls.district.clearValidators();
      this.filterForm.controls.district.updateValueAndValidity();
    }
    this.filterForm.patchValue({ "reportType": selectedReportType.value });
  }
  getDivisionValue(val) {
    this.empData = [];
    const selectedDivision = new Array(val);
    this.filterForm.patchValue({ division: val });
    this.callDesignationComponent.getDesignationBasedOnDivision({
      companyId: this.currentUser.companyId,
      userLevel: this.currentUser.userInfo[0].designationLevel,
      userId: this.currentUser.id, //For Manager Level
      division: val
    });
  }
  getStateValue(val) {
    this.empData = [];
    this.filterForm.patchValue({ state: val });
    this.callDesignationComponent.getUniqueDesignationOnStateOrDistrict({
      companyId: this.currentUser.companyId,
      stateId: val,
      districtId: "", //Set Blank
      lookingFor: "onState",
      userLevel: this.currentUser.userInfo[0].designationLevel,
      userId: this.currentUser.id
    });
  }
  getDistrictValue(val) {
    this.empData = [];
    this.filterForm.patchValue({ district: val });
    this.callDesignationComponent.getUniqueDesignationOnStateOrDistrict({
      companyId: this.currentUser.companyId,
      stateId: "",//Set Blank
      districtId: val,
      lookingFor: "onDistrict",
      userLevel: this.currentUser.userInfo[0].designationLevel,
      userId: this.currentUser.id
    });
  }

  empData: any = [];
  expandedEmpData: any = [];
  getDesignation(val) {
    this.empData = [];
    this.filterForm.patchValue({ designation: val });

    const passingObject = {
      checkingLevel: this.currentUser.userInfo[0].designationLevel,
      lookingOn: this.filterForm.value.reportType, //#division or #state or #district
      divisionIds: this.filterForm.value.division, //#stateIds[] or districtIds[]
      stateIds: this.filterForm.value.state,
      districtIds: this.filterForm.value.district,
      desigations: val.map(desg => desg.designation),
      status: [true], //#[true,false] or [false]
      companyId: this.currentUser.companyId,
      userId: this.currentUser.id //For Manager Level
    }
    this._userDetailService.getEmployeeBasedOnDivisionOrStateOrDistrictAndDesignationORAllEmp(passingObject).subscribe((res: any) => {

      this.expandedEmpData = res;
      const group = res.reduce((r, a) => {
        r[a.designation] = [...r[a.designation] || [], a];
        return r;
      }, {});
      this.empData = group;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  togglePerEmp(eachMatOption: MatOption) {

    if (this.allSelected.selected) {
      this.allSelected.deselect();
      return false;
    }
    if (this.filterForm.controls.employee.value.length > 20) {
      this.matOption = eachMatOption;
      this.matOption.deselect();
      this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient")
    } else {
      if (this.filterForm.controls.employee.value.length == this.expandedEmpData.length)
        this.allSelected.select();
    }
  }

  toggleAllSelection() {
    if (this.allSelected.selected) {
      if (this.expandedEmpData.length > 20) {
        this.allSelected.deselect();
        this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient")
      } else {
        this.filterForm.controls.employee.patchValue([0, ...this.expandedEmpData]);
      }
    } else {
      this.filterForm.controls.employee.patchValue([]);
    }
  }


}
