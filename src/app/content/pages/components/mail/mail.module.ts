import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MailComponent } from './mail.component';
import { ComposeComponent } from './compose/compose.component';
import { InboxComponent } from './inbox/inbox.component';
import { SentComponent } from './sent/sent.component';
import { QuillModule } from 'ngx-quill';
import { ReactiveFormsModule } from '@angular/forms';

import { PartialsModule } from '../../../partials/partials.module';

import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatProgressBarModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTabsModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatSnackBarModule,
  MatTooltipModule,
  MatSliderModule,
  MatChipsModule,
  MatExpansionModule
} from '@angular/material';
import { FiltersModule } from '../filters/filters.module';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { CoreModule } from '../../../../core/core.module';
import { UnderProcessComponent } from './under-process/under-process.component';
import { DraftComponent } from './draft/draft.component';

const routes = [
  {
    path: '',
    component: MailComponent,
    children: [{
      path: 'inbox',
      component: InboxComponent
    }, {
      path: 'compose',
      component: ComposeComponent
      // component:InboxComponent
    }, {
      path: 'sent',
      component: SentComponent
      //component: InboxComponent
    }, {
      path: 'draft',
      //component: SentComponent
      // component: InboxComponent
      // component: UnderProcessComponent
      component: DraftComponent
    }, {
      path: 'reps',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'doctors',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'vendors',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'single',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'addCampaignDoctors',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'delCampaignDoctors',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'submitCRM',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'transferROI',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'transferSSS',
      // component: InboxComponent
      component: UnderProcessComponent
    }, {
      path: 'delteSSS',
      // component: InboxComponent
      component: UnderProcessComponent
    },
    {
      path: 'rcpa',
      // component: InboxComponent
      component: UnderProcessComponent
    },
    {
      path: 'viewRcpa',
      // component: InboxComponent
      component: UnderProcessComponent
    },
    {
      path: 'viewCampaignActivity',
      // component: InboxComponent
      component: UnderProcessComponent
    }
      ,
    {
      path: 'crmDoctorVisit',
      // component: InboxComponent
      component: UnderProcessComponent
    },
    {
      path: 'crmDoctorInvestment',
      // component: InboxComponent
      component: UnderProcessComponent
    }
      ,
    {
      path: 'yearlyCRMInvestment',
      // component: InboxComponent
      component: UnderProcessComponent
    },
    {
      path: 'doctorDispairity',
      // component: InboxComponent
      component: UnderProcessComponent
    }


    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    QuillModule,
    ReactiveFormsModule,
    PartialsModule,
    MatRadioModule,
    MatButtonModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatSelectModule,
    MatMenuModule,
    MatProgressBarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTabsModule,
    MatNativeDateModule,
    MatCardModule,
    MatRadioModule,
    MatIconModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatSliderModule,
    FiltersModule,
    MatChipsModule,
    MatExpansionModule,
    CoreModule
  ],
  entryComponents: [DialogBoxComponent],
  declarations: [MailComponent,
    ComposeComponent,
    InboxComponent,
    SentComponent,
    DialogBoxComponent,
    UnderProcessComponent,
    DraftComponent]
})

export class MailModule { }
