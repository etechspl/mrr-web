import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { InobxService } from '../../../../../core/services/inbox/inobx.service';
import { MatChipInputEvent, MatAutocomplete, MatAutocompleteSelectedEvent, MatDialog } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { Observable, merge, forkJoin } from 'rxjs';
import { map, startWith } from 'rxjs/operators'
import { UserDetailsService } from '../../../../../core/services/user-details.service';
import { FileHandlingService } from '../../../../../core/services/FileHandling/file-handling.service';
import { HttpEventType } from '@angular/common/http';
import { URLService } from '../../../../../core/services/URL/url.service';
import { ToastrService } from 'ngx-toastr';
import { HierarchyService } from '../../../../../core/services/hierarchy-service/hierarchy.service';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';
import { SentMailService } from '../../../../../core/services/SentMail/sent-mail.service';
import { DraftMailService } from '../../../../../core/services/DraftMail/draft-mail.service';
@Component({
  selector: 'm-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  currentUser = JSON.parse(sessionStorage.currentUser);

  visible = true;
  selectable = true;
  removable = true;

  selectableCC = true;
  removableCC = true;

  selectableBCC = true;
  removableBCC = true;

  separatorKeysCodes: number[] = [ENTER, COMMA];

  ToCtrl = new FormControl();
  ccCtrl = new FormControl();
  bccCtrl = new FormControl();
  mailBody = new FormControl();

  filteredUsers: Observable<any[]>;

  toUsers: any[] = [];
  ccUsers: any[] = [];
  bccUsers: any[] = [];
  hierarchyUserList: any = [];
  showDiv = -1;

  inboxMail: any = [];
  showReplyRichTextBox: boolean = false;
  isMailForwading: boolean = false;
  inputTypeReadOnly: boolean = true;

  mailStatus: string = "Mail Fetching...";

  @ViewChild('autoTo') matAutocompleteTo: MatAutocomplete;
  @ViewChild('autoCC') matAutocompleteCC: MatAutocomplete;
  @ViewChild('autoBCC') matAutocompleteBCC: MatAutocomplete;

  @ViewChild('toUserInput') toUserInput: ElementRef<HTMLInputElement>;
  @ViewChild('ccUserInput') ccUserInput: ElementRef<HTMLInputElement>;
  @ViewChild('bccUserInput') bccUserInput: ElementRef<HTMLInputElement>;


  constructor(
    private _inboxService: InobxService,
    private _sentMailService: SentMailService,
    private _draftMailService:DraftMailService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _userDetailservice: UserDetailsService,
    private _fileHandlingService: FileHandlingService,
    private _urlServive: URLService,
    private _hierarchyService: HierarchyService,
    private _userDetailService: UserDetailsService,
    private _toaster: ToastrService,
    public dialog: MatDialog
  ) {
  }

  openDialog(openedFor: string) {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      height: "40%",
      width: "50%",
      data: { whichFilter: openedFor },
      hasBackdrop: true,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && openedFor == "To") {
        result = result.filter(user => user); //For REmoving 0 if All selected...
        result.forEach(elem => {
          const newUser = {
            id: elem.userId,
            name: elem.name,
            designation: elem.designation,
            district: elem.districtId ? ` - ${elem.districtName}` : null
          };
          const index = this.toUsers.findIndex(i => JSON.stringify(i) === JSON.stringify(newUser));
          if (index < 0) {
            this.toUsers.push(newUser);
            this._changeDetectorRef.detectChanges();
          }
        });
      } else if (result && openedFor == "CC") {
        //console.log("openedFor Before : ", openedFor);
        result = result.filter(user => user); //For REmoving 0 if All selected...
        result.forEach(elem => {
          const newUser = {
            id: elem.userId,
            name: elem.name,
            designation: elem.designation,
            district: elem.districtId ? ` - ${elem.districtName}` : null
          };
          const index = this.ccUsers.findIndex(i => JSON.stringify(i) === JSON.stringify(newUser));
          if (index < 0) {
            this.ccUsers.push(newUser);
            this._changeDetectorRef.detectChanges();
          }
        });
      } else if (result && openedFor == "BCC") {
        //console.log("openedFor Before : ", openedFor);
        result = result.filter(user => user); //For REmoving 0 if All selected...
        result.forEach(elem => {
          const newUser = {
            id: elem.userId,
            name: elem.name,
            designation: elem.designation,
            district: elem.districtId ? ` - ${elem.districtName}` : null
          };
          const index = this.bccUsers.findIndex(i => JSON.stringify(i) === JSON.stringify(newUser));
          if (index < 0) {
            this.bccUsers.push(newUser);
            this._changeDetectorRef.detectChanges();
          }
        });
      }
    });
  }
  empData: any = [];
  ngOnInit() {

    this._inboxService.getMails({
      loggedInId: this.currentUser.id
    }).subscribe(res => {
      if (res.length > 0) {
        this.mailStatus = '';
        this.inboxMail = res;
      } else {
        this.mailStatus = 'No mail found...';
      }
      this._changeDetectorRef.detectChanges();
    }, err => {
      this.mailStatus = 'No mail found...';
      this._changeDetectorRef.detectChanges();
      console.log(err);
    });

    if (this.currentUser.userInfo[0].designationLevel == 1) {
      this.inputTypeReadOnly = false;
      const passingObject = {
        checkingLevel: this.currentUser.userInfo[0].designationLevel,
        lookingOn: '', //#state or #district   
        status: [true],
        companyId: this.currentUser.companyId,
        userId: this.currentUser.id //For Manager Level
      }
      this._userDetailService.getEmployeeBasedOnDivisionOrStateOrDistrictAndDesignationORAllEmp(passingObject).subscribe((res: any) => {
        //console.log("Hierarchy Result : ", res);
        this.hierarchyUserList = res.map(rs => {
          return {
            id: rs.userId,
            name: rs.name,
            designation: rs.designation,
            district: rs.districtName ? ` - ${rs.districtName}` : null
          }
        });
      });

      this.filteredUsers = merge(this.ToCtrl.valueChanges, this.ccCtrl.valueChanges, this.bccCtrl.valueChanges)
        .pipe(
          startWith(null),
          map((fruit: (string | null)[]) => fruit ? this.To_filter(fruit) : this.hierarchyUserList.slice()));

    }


  }


  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our User
    if ((value || '').trim()) {
      this.toUsers.push({
        id: "noId",
        name: value.trim(),
        designation: "No Designation",
        district: null
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    //this.ToCtrl.setValue(null);
  }
  addCC(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our User
    if ((value || '').trim()) {
      this.ccUsers.push({
        id: "noId",
        name: value.trim(),
        designation: "No Designation",
        district: null
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    //this.ToCtrl.setValue(null);
  }
  addBCC(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our User
    if ((value || '').trim()) {
      this.bccUsers.push({
        id: "noId",
        name: value.trim(),
        designation: "No Designation",
        district: null
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    //this.ToCtrl.setValue(null);
  }

  remove(deleteUserObject: any): void {
    const index = this.toUsers.findIndex(user => user.id === deleteUserObject.id);
    if (index >= 0) {
      this.toUsers.splice(index, 1);
    }
    this._changeDetectorRef.detectChanges();
  }
  removeCC(deleteUserObject: any): void {
    const index = this.ccUsers.findIndex(user => user.id === deleteUserObject.id);
    if (index >= 0) {
      this.ccUsers.splice(index, 1);
    }
    this._changeDetectorRef.detectChanges();
  }
  removeBCC(deleteUserObject: any): void {
    const index = this.bccUsers.findIndex(user => user.id === deleteUserObject.id);
    if (index >= 0) {
      this.bccUsers.splice(index, 1);
    }
    this._changeDetectorRef.detectChanges();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    const newUser = {
      id: event.option.value.id,
      name: event.option.value.name,
      designation: event.option.value.designation,
      district: event.option.value.district
    };
    const index = this.toUsers.findIndex(i => JSON.stringify(i) === JSON.stringify(newUser));
    (index < 0) ? this.toUsers.push(newUser) : this._toaster.error("The user already selected.", "Duplicate User")
    this.toUserInput.nativeElement.value = '';
    this.ToCtrl.setValue(null);

  }

  selectedCC(event: MatAutocompleteSelectedEvent): void {
    const newUser = {
      id: event.option.value.id,
      name: event.option.value.name,
      designation: event.option.value.designation,
      district: event.option.value.district
    };

    const index = this.ccUsers.findIndex(i => JSON.stringify(i) === JSON.stringify(newUser));
    (index < 0) ? this.ccUsers.push(newUser) : this._toaster.error("The user already selected.", "Duplicate User")
    this.ccUserInput.nativeElement.value = '';
    this.ccCtrl.setValue(null);
  }

  selectedBCC(event: MatAutocompleteSelectedEvent): void {
    const newUser = {
      id: event.option.value.id,
      name: event.option.value.name,
      designation: event.option.value.designation,
      district: event.option.value.district
    };
    const index = this.bccUsers.findIndex(i => JSON.stringify(i) === JSON.stringify(newUser));
    (index < 0) ? this.bccUsers.push(newUser) : this._toaster.error("The user already selected.", "Duplicate User")
    this.bccUserInput.nativeElement.value = '';
    this.bccCtrl.setValue(null);

  }

  private To_filter(value: any): string[] {
    if (typeof (value) === "object") {
      const filterValue = value.name.toLowerCase();
      return this.hierarchyUserList.filter(user => `${user.name}${user.designation}${user.district}`.toLowerCase().indexOf(filterValue) !== -1);
    } else {
      const filterValue = value.toLowerCase();
      return this.hierarchyUserList.filter(user => `${user.name}${user.designation}${user.district}`.toLowerCase().indexOf(filterValue) !== -1);
    }
  }

  Reply(mailObject, index) {
    console.log("Mail Object : ", mailObject);

    //Resetting.....'
    this.isMailForwading = false;
    this.showDiv = index;
    this.toUsers = [];
    this.ccUsers = [];
    this.bccUsers = [];
    this.urls = [];
    this.uploadedDocuments = [];
    this.mailBody.setValue(null);
    this.showReplyRichTextBox = true;
    //--------------------------Setting up To Variable-------------------

    this.toUsers.push({
      id: mailObject.fromId,
      name: mailObject.fromName,
      designation: mailObject.fromUserDesignation,
      district: mailObject.fromUserDistrict ? ` - ${mailObject.fromUserDistrict}` : null
    });

    this._changeDetectorRef.detectChanges();



  }
  ReplyAll(mailObject, index) {
    console.log("MailObject : ", mailObject);

    //Resetting.....'
    this.isMailForwading = false;
    this.showDiv = index;
    this.toUsers = [];
    this.ccUsers = [];
    this.bccUsers = [];
    this.urls = [];
    this.uploadedDocuments = [];
    this.mailBody.setValue(null);
    this.showReplyRichTextBox = true;
    //--------------------------Setting up To Variable-------------------


    this.toUsers.push({
      id: mailObject.fromId,
      name: mailObject.fromName,
      designation: mailObject.fromUserDesignation,
      district: (mailObject.fromUserDistrict) ? (` - ${mailObject.fromUserDistrict}`) : null
    });

    this.ccUsers = [].concat(
      mailObject.To.filter(res => {
        const _res = { ...res };
        _res.district = _res.district ? ` - ${_res.district}` : null;
        return _res.id.toString() !== this.currentUser.id.toString();
      }),
      mailObject.CC.filter(res => {
        const _res = { ...res };
        _res.district = _res.district ? ` - ${_res.district}` : null;
        return _res.id.toString() !== this.currentUser.id.toString();
      }));
    this._changeDetectorRef.detectChanges();
  }

  Forward(mailObject: any, index: number) {
    //Resetting.....'
    this.isMailForwading = true;
    this.showDiv = index;
    this.toUsers = [];
    this.ccUsers = [];
    this.bccUsers = [];
    this.urls = [];
    this.uploadedDocuments = [];
    this.mailBody.setValue(null);

    //Making Image List for Forwarding mail.
    if (mailObject.attachments) {
      mailObject.attachments.forEach((image, index) => {
        this.urls.push({
          name: image.originalFileName,
          localImageName: `${image.modifiedFileName}_${index}`,
          fileUploaded: 100
        });

        this.uploadedDocuments.push(image)
      });
    }

    const forwardDetails = `
    <p><br></p><p><br></p><p><br></p><p><br></p>
    <p>---------- Forwarded message ---------</p>
    <p>From: <strong>${mailObject.fromName}(${mailObject.fromUserDesignation})${mailObject.fromUserDistrict ? ` - ${mailObject.fromUserDistrict}` : null}</strong></p>
    <p>Date: ${mailObject.MailDateTime}</p>
    <p>Subject: ${mailObject.Subject}</p>
    <p>To: ${mailObject.To.map(ids => {
        return ids.district ? `${ids.name}(${ids.designation}) - ${ids.district}` : `${ids.name}(${ids.designation})`
      }).join(", ")}</p>
    ${mailObject.CC.length > 0 ? `<p>Cc: ${mailObject.CC.map(ids => {
        return ids.district ? `${ids.name}(${ids.designation}) - ${ids.district}` : `${ids.name}(${ids.designation})`
      }).join(", ")} </p>` : ''}
    
    <p><br></p><p><br></p><p><br></p>
    <p>${mailObject.Message}</p>
    `

    this.mailBody.setValue(forwardDetails);
    this.showReplyRichTextBox = true;


    //--------------------------Setting up To Variable-------------------
  }

  Send(mailObject: any, index: number) {

    // console.log("To Cntrl : ", this.toUsers);
    // console.log("CC Cntrl : ", this.ccUsers);
    // console.log("BCC Cntrl : ", this.bccUsers);
    // console.log("Body Cntrl : ", this.mailBody.value);
    // console.log("uploadedDocuments : ", this.uploadedDocuments);
    // console.log("urls... ", this.urls);
    if (this.toUsers.length > 0 || this.ccUsers.length > 0 || this.bccUsers.length > 0) {
      let finalObject: any = [];
      if (this.toUsers.length > 20) {
        this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient in To.")
      } else if (this.ccUsers.length > 20) {
        this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient in CC.")
      } else if (this.bccUsers.length > 20) {
        this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient in BCC.")
      } else if (this.mailBody.value === '' || this.mailBody.value === null) {
        this._toaster.error("Please Enter Mail Body.")
      } else {
        this.toUsers.forEach(toUserList => {
          finalObject.push({
            "companyId": this.currentUser.companyId,
            "from": this.currentUser.id,
            "to": toUserList.id,
            "tos": this.toUsers.map(user => user.id),
            "cc": this.ccUsers.map(user => user.id),
            "bcc": this.bccUsers.map(user => user.id),
            "subject": this.isMailForwading ? `Fwd : ${mailObject.Subject}` : mailObject.Subject,
            "message": this.mailBody.value,
            "mailDate": new Date(),
            "fileId": this.uploadedDocuments.length > 0 ? this.uploadedDocuments.map(doc => { delete doc.localImageName; return doc }) : undefined,
            "createdAt": new Date(),
            "updatedAt": new Date()
          })
        });
        this.ccUsers.forEach(toUserList => {
          finalObject.push({
            "companyId": this.currentUser.companyId,
            "from": this.currentUser.id,
            "to": toUserList.id,
            "tos": this.toUsers.map(user => user.id),
            "cc": this.ccUsers.map(user => user.id),
            "bcc": this.bccUsers.map(user => user.id),
            "subject": this.isMailForwading ? `Fwd : ${mailObject.Subject}` : mailObject.Subject,
            "message": this.mailBody.value,
            "mailDate": new Date(),
            "fileId": this.uploadedDocuments.length > 0 ? this.uploadedDocuments.map(doc => { delete doc.localImageName; return doc }) : undefined,

            "createdAt": new Date(),
            "updatedAt": new Date()
          })
        });
        this.bccUsers.forEach(toUserList => {
          finalObject.push({
            "companyId": this.currentUser.companyId,
            "from": this.currentUser.id,
            "to": toUserList.id,
            "tos": this.toUsers.map(user => user.id),
            "cc": this.ccUsers.map(user => user.id),
            "bcc": this.bccUsers.map(user => user.id),
            "subject": this.isMailForwading ? `Fwd : ${mailObject.Subject}` : mailObject.Subject,
            "message": this.mailBody.value,
            "mailDate": new Date(),
            "fileId": this.uploadedDocuments.length > 0 ? this.uploadedDocuments.map(doc => { delete doc.localImageName; return doc }) : undefined,
            "createdAt": new Date(),
            "updatedAt": new Date()
          })
        });
        console.log("Final Object : ", finalObject);

        const sentItem = {
          "companyId": this.currentUser.companyId,
          "from": this.currentUser.id,
          "tos": this.toUsers.map(user => user.id),
          "cc": this.ccUsers.map(user => user.id),
          "bcc": this.bccUsers.map(user => user.id),
          "subject": this.isMailForwading ? `Fwd : ${mailObject.Subject}` : mailObject.Subject,
          "message": this.mailBody.value,
          "mailDate": new Date(),
          "fileId": this.uploadedDocuments.length > 0 ? this.uploadedDocuments.map(doc => { delete doc.localImageName; return doc }) : undefined,
          "createdAt": new Date(),
          "updatedAt": new Date()
        }
        forkJoin(this._inboxService.createInbox(finalObject), this._sentMailService.createSentItem(sentItem)).subscribe(res => {
          this.showDiv = index == this.showDiv ? -1 : index;
          this._toaster.success("Mail has been sent successfuly !!!");
          this._changeDetectorRef.detectChanges();
        });
        // this._inboxService.createInbox(finalObject).subscribe(res => {
        //   this.showDiv = index == this.showDiv ? -1 : index;
        //   this._toaster.success("Mail has been sent successfuly !!!");
        //   this._changeDetectorRef.detectChanges();

        // }, err => {
        //   console.log("Error : ", err);
        // });
      }

    } else {
      this._toaster.error("Please specify at least one recipient.", "Error");
    }
  }

  SaveAsDraft(mailObject: any, index: number) {

    // console.log("To Cntrl : ", this.toUsers);
    // console.log("CC Cntrl : ", this.ccUsers);
    // console.log("BCC Cntrl : ", this.bccUsers);
    // console.log("Body Cntrl : ", this.mailBody.value);
    // console.log("uploadedDocuments : ", this.uploadedDocuments);
    // console.log("urls... ", this.urls);
    // console.log("Subject : ", this.subject.value);


    if (this.toUsers.length > 20) {
      this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient in To.")
    } else if (this.ccUsers.length > 20) {
      this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient in CC.")
    } else if (this.bccUsers.length > 20) {
      this._toaster.error("Only 20 Recipient can be select at a time.", "Select 20 Recipient in BCC.")
    } else {   
      const sentItem = {
        "companyId": this.currentUser.companyId,
        "from": this.currentUser.id,
        "tos": this.toUsers.map(user => user.id),
        "cc": this.ccUsers.map(user => user.id),
        "bcc": this.bccUsers.map(user => user.id),
        "subject": this.isMailForwading ? `Fwd : ${mailObject.Subject}` : mailObject.Subject,
        "message": this.mailBody.value,
        "mailDate": new Date(),
        "fileId": this.uploadedDocuments.length > 0 ? this.uploadedDocuments.map(doc => { delete doc.localImageName; return doc }) : undefined,
        "createdAt": new Date(),
        "updatedAt": new Date()
      }
      forkJoin(this._draftMailService.createDraftItem(sentItem)).subscribe(res => {
        this.showDiv = index == this.showDiv ? -1 : index;
        this._toaster.success("Mail has been saved in draft successfuly !!!");        
        this._changeDetectorRef.detectChanges();
      });
    }


  }

  extractNameFromArray(arrayOfObject: any): any {
    if (arrayOfObject) {
      return arrayOfObject.map(ids => {
        return ids.district ? `${ids.name}(${ids.designation}) - ${ids.district}` : `${ids.name}(${ids.designation})`
      }).join(", ");
    } else {
      return "-----";
    }
  }

  downloadAttachment(attachment: any) {

    this._fileHandlingService.getDocument(attachment.container, attachment.modifiedFileName).subscribe(res => {
      //console.log(res);
      const a = document.createElement("a");
      a.href = URL.createObjectURL(res);
      a.download = attachment.originalFileName;
      // start download
      a.click();
    }, err => {
      console.log(err);
    });

  }

  urls = [];
  uploadedDocuments = [];
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      //let coutner = this.urls.length - 1;
      const prevUplodedLength = this.urls.length;
      for (let i = 0; i < filesAmount; i++) {
        this.urls.push({
          name: event.target.files[i].name,
          localImageName: `${event.target.files[i].name}_${prevUplodedLength + i}`,
        });
        this.uploadImages(event.target.files[i]).subscribe(events => {
          if (events.type === HttpEventType.UploadProgress) {
            //console.log("Uploaded : " + Math.round(events.loaded / events.total * 100) + "%");
            this.urls[prevUplodedLength + i]["fileUploaded"] = Math.round(events.loaded / events.total * 100);
            this._changeDetectorRef.detectChanges();
          } else if (events.type === HttpEventType.Response) {
            this.uploadedDocuments.push({ ...events.body, localImageName: `${event.target.files[i].name}_${prevUplodedLength + i}` });
          }

        });
      }
    }
  }

  uploadImages(image: File): Observable<any> {
    return this._fileHandlingService.uploadUserImage(this.currentUser.companyId, this.currentUser.id, "MailAttachments", image);
  }

  deleteAttachment(imageObject: any) {
    //console.log("Delete Image : ", imageObject);
    //console.log("Detelion Object : ", this.uploadedDocuments);

    const index = this.urls.findIndex(i => i === imageObject);
    if (index >= 0) {
      this.urls.splice(index, 1);
    }
    //Deleting Uploded Image on Server...
    // const index1 = this.uploadedDocuments.findIndex(i => i.localImageName === imageObject.localImageName);
    // if (index1 >= 0) {
    //   this.uploadedDocuments.splice(index1, 1);

    //   const uploadedFileObject: any = this.uploadedDocuments.find(i => i.localImageName == imageObject.localImageName);
    //   forkJoin(
    //     this._fileHandlingService.removeDocument("MailAttachments", uploadedFileObject.modifiedFileName),
    //     this._fileHandlingService.removeFileLibrary(uploadedFileObject.fileId)
    //   ).subscribe(res => {
    //     //Removing From Array
    //     this.uploadedDocuments.splice(index1, 1);
    //   }, err => {
    //     console.log(err);
    //   });

    this._changeDetectorRef.detectChanges();
  }




  openMatExpansionPanel(index: number) {

    //this.showReplyRichTextBox = false;
    //console.log("Number : ", index);
    if (!this.inboxMail[index].readStatus) {
      const whereObject = {
        id: this.inboxMail[index].mailId
      }
      const updateObject = {
        readStatus: true,
        readDate: new Date(),
        updatedAt: new Date()
      }
      this._inboxService.updateInboxMail(whereObject, updateObject).subscribe(res => {
        this.inboxMail[index].readColour = "#ebedf2"; //Grey Colour
        this._changeDetectorRef.detectChanges();
      }, err => {
        console.log("Not Updated")
      });

    }

    //this.inboxMail[index].readColour = "#ebedf2";
  }






}
