import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ActionComponent } from './header/action/action.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ProfileComponent } from './header/profile/profile.component';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from "./components/inner/inner.component";
import { AuthGuard } from '../../core/services/auth.guard';
import { FaqComponent } from './snippets/faq/faq.component';
import { SupportComponent } from './snippets/support/support.component';
const routes: Routes = [
	{
		path: '',
		component: PagesComponent,
		// Remove comment to enable login
		//canActivate: [NgxPermissionsGuard],
		canActivate: [AuthGuard], //PK ,
		/*data: {
			permissions: {
				only: ['ADMIN', 'USER'],
				except: ['GUEST'],
				redirectTo: '/login'
			}
		},*/
		children: [
			{
				path: '',
				loadChildren: './components/dashboard/dashboard.module#DashboardModule'
			},
			{
				path: 'master',
				loadChildren: './components/master/master.module#MasterModule'
			},
			{
				path: 'reports',
				loadChildren: './components/reports/reports.module#ReportsModule'
			},
			{
				path: 'filters',
				canActivate: [AuthGuard],
				loadChildren: './components/filters/filters.module#FiltersModule'
			},
			{
				path: 'builder',
				loadChildren: './builder/builder.module#BuilderModule'
			},
			{
				path: 'mail',
				loadChildren: './components/mail/mail.module#MailModule'
			},
			{
				path: 'header/actions',
				component: ActionComponent
			},
			{
				path: 'profile',
				component: ProfileComponent
			},
			{
				path: 'inner',
				component: InnerComponent
			},
			{
				path: 'faq',
				component: FaqComponent
			},
			{
				path: 'support',
				component: SupportComponent
			},
			{
				path: 'mapReports',
				loadChildren: './components/map-reports/map-reports.module#MapReportsModule'
			},
			{
				path: 'crm',
				loadChildren: './components/crm/crm.module#CRMModule'
			}
		]
	}, {
		path: 'dashboard',
		component: PagesComponent,
		// Remove comment to enable login
		//canActivate: [NgxPermissionsGuard],
		canActivate: [AuthGuard], //PK ,
		/*data: {
			permissions: {
				only: ['ADMIN', 'USER'],
				except: ['GUEST'],
				redirectTo: '/login'
			}
		},*/
		children: [
			{
				path: '',
				loadChildren: './components/dashboard/dashboard.module#DashboardModule'
			},
			{
				path: 'master',
				canActivate: [AuthGuard],
				loadChildren: './components/master/master.module#MasterModule'
			},
			{
				path: 'reports',
				canActivate: [AuthGuard],
				loadChildren: './components/reports/reports.module#ReportsModule'
			},
			{
				path: 'filters',
				canActivate: [AuthGuard],
				loadChildren: './components/filters/filters.module#FiltersModule'
			},
			{
				path: 'builder',
				loadChildren: './builder/builder.module#BuilderModule'
			},
			{
				path: 'header/actions',
				component: ActionComponent
			},
			{
				path: 'profile',
				component: ProfileComponent
			},
			{
				path: 'inner',
				component: InnerComponent
			},
			{
				path: 'faq',
				component: FaqComponent
			},
			{
				path: 'support',
				component: SupportComponent
			}
		]
	},
	{
		path: 'adminPage',
		component: PagesComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: '',
				loadChildren: 'app/admin/admin.module#AdminModule'
			}
		]
	},
	{
		path: 'login',
		// canActivate: [NgxPermissionsGuard],
		//canActivate: [AuthGuard], //PK ,
		loadChildren: './auth/auth.module#AuthModule',
		data: {
			permissions: {
				except: 'ADMIN'
			}
		},
	},
	{
		path: '404',
		component: ErrorPageComponent
	},
	{
		path: 'error/:type',
		component: ErrorPageComponent
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
