import {
	Component,
	OnInit,
	Input,
	HostBinding,
	OnDestroy,
	Output,
	ViewChild
} from '@angular/core';
import { LayoutConfigService } from '../../../core/services/layout-config.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { LayoutConfig } from '../../../config/layout';
import { Subject } from 'rxjs';
import { AuthNoticeService } from '../../../core/auth/auth-notice.service';
import { TranslationService } from '../../../core/services/translation.service';
import Typewriter from 't-writer.js';

@Component({
	selector: 'm-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
	@HostBinding('id') id = 'm_login';
	@HostBinding('class')
	// tslint:disable-next-line:max-line-length
	classses: any = 'm-grid m-grid--hor m-grid--root m-page';

	@Input() action = 'login';
	today: number = Date.now();
	@ViewChild('tw') typewriterElement;
	@ViewChild('tw2') typewriterElement2;
	@ViewChild('tw3') typewriterElement3;
	constructor(
		private layoutConfigService: LayoutConfigService,
		public authNoticeService: AuthNoticeService,
		private translationService: TranslationService,
	) {}

	ngOnInit(): void {
		const target2 = this.typewriterElement2.nativeElement;
		const target3 = this.typewriterElement3.nativeElement;
		const target = this.typewriterElement.nativeElement

		// const writer = new Typewriter(target, {
		// 	loop: true,
		// 	typeColor: 'white'
		//   })
		//   writer
		//   .type(`MR Reporting from E-tech.`)
		//   .rest(500)
		//   .start()
	

		const writer2 = new Typewriter(target2, {
			typeColor: 'white'
		  })
		  const writer3 = new Typewriter(target3, {
			typeColor: 'red'
		  })
	  
		writer2
		.type('MR Reporting')
		.changeTypeColor('white')
		.removeCursor()
		.then(writer3.start.bind(writer3))
		.start()
	  
	  writer3
	  .changeTypeColor('yellow')
		.type("Making things possible")
		.rest(500)
		.clear()
		.changeTypeColor('red')
		.type("A better way")
		.rest(500)
		.clear()
		.changeTypeColor('gold')
		.type("Digitally yours")
		.rest(500)
		.clear()
		.changeTypeColor('cyan')
		.type("Superior by design")
		.rest(500)
		.clear()
		.changeTypeColor('magenta')
		.type("Makes it happen")
		.rest(500)
		.clear()
		.changeTypeColor('orange')
		.type("Software that can think")
		.rest(500)
		.clear()
		.changeTypeColor('buff')
		.then(writer2.start.bind(writer2))


		// set login layout to blank
		this.layoutConfigService.setModel(new LayoutConfig({ content: { skin: '' } }), true);

		this.translationService.getSelectedLanguage().subscribe(lang => {
			if (lang) {
				setTimeout(() => this.translationService.setLanguage(lang));
			}
		});
	}

	ngOnDestroy(): void {
		// reset back to fluid
		this.layoutConfigService.reloadSavedConfig();
	}

	register() {
		this.action = 'register';
	}
}
