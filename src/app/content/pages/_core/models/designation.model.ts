export class Designation{
    id: string;
    companyId:string;
    designation: string;
    designationLevel: number;
    fullName: string; 
}