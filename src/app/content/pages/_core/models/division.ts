export class Division {
    id: string;
    companyid: string;
    //assignedTo: Array<string>;
    divisionName: string;
    status: boolean;
    createdAt: string;
    updatedAt: string;
}