
export class Product {
    companyId : string;
    divisionId : Array<Object>;
    productName : string; 
    productCode : string; 
    ptr : Number;
    ptw : Number; 
    mrp : Number;
    sampleRate: Number;
    includevat: Number;
    assignedStates : Array<Object>;
    
}