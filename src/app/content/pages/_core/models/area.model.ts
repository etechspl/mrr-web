export class AreaModel {

    private companyId: string;
    private stateId:string;
    private stateName: string;
    private districtId: string;
    private districtName: string;
    private id: string;
    private areaName:string;
    private type: string;
    private appStatus:string;
    private delStatus:string;
    private appByMgr:string;
    private finalAppBy:string;
    private mgrAppDate: Date;
    private finalAppDate: Date;
    private delByMgr:string;
    private finalDelBy:string;
    private mgrDelDate: Date;
    private finalDelDate: Date;
    private status: boolean;
    private createdAt: Date;
    private updatedAt: Date;

}