export class TourProgram {
    id: string;
    name: string;
    designation: string;
    assignedTo: Array<string>;
    submitted: boolean;
    approved: boolean;
}
