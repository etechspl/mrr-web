export class HierarchyModel{
    "_id" : string; 
    "userId" : string; 
    "companyId" : string; 
    "userName" : string; 
    "userDesignation" : string; 
    "supervisorId" : string;
    "supervisorName" : string;
    "supervisorDesignation" : string;
    "immediateSupervisorId" : string;
    "immediateSupervisorName" : string;
    "status" : boolean; 
    "createdAt" : Date; 
    "updatedAt" : Date;
}