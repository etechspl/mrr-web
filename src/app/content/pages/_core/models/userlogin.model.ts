export class UserLogin {

    "id": string;
    "companyId": string;
    "companyName": string;
    "divisionId": string;
    "divisionName": string;

    "name": string;
    "age": number;
    "gender": string;
    "address": string;
    "dateOfBirth": Date;
    "dateOfAnniversary": Date;
    "dateOfJoining": Date;
    "mobile": number;
    "email": string;

    "reportingManager": string;

    "aadhaar": string
    "PAN": string;
    "DLNumber": string;

    "username": string;
    "viewPassword": string;
    "password": string;

    "status": boolean;
    "fcmToken": string;
    "mPIN": number;
    "deviceId": string
    "isMPINStatus": boolean;

    "createdAt": Date;
    "updatedAt": Date;
    "deactivationDate": Date;

    //user-info


    "userId": string;

    "stateId": string;
    "districtId": string;
    "employeeCode":string;
    "assignDistricts": Array<Object>;
    "designation": string;
    "designationLevel": number;
    // "status": boolean;
    "lockingPeriod": number;
    "dateOfReporting": Date;
    "imageId":string;

}