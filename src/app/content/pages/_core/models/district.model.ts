export class District {
    id: string;
    districtName: string;
    stateId: string;
    assignedTo: Array<string>;
    createdAt: string;
    updatedAt: string;
}