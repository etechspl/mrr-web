import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../../../core/auth/authentication.service';
import { URLService } from '../../../../core/services/URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class DailyallowanceService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) {
  }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  createDailyAllowance(data: any) {
    let url = this._urlService.API_ENDPOINT_DAILY_ALLOWANCES;
    return this.http.post(url, JSON.stringify(data), { headers: this.headers });
  }

  getDailyAllowanceDetails(param: any): Observable<any> {
    let obj = {
      where: {
        companyId: param.companyId,
        type:param.type,
        status:true
      },
      include: [{
        relation: 'state',
        scope:{
          fields: {"stateName":true}
        }
      },{
        relation:'userInfo',
        scope:{
          fields:{"name":true,"id":true}
        }
      },{
        relation:"divisionMaster",
        scope:{
          fields:{
            "divisionName":true,
            "id":true
          }
        }
      },{
        relation:'district',
        scope:{
          fields:{
            "districtName":true,
            "id":true
          }
        }
      }]
    }
    let url = this._urlService.API_ENDPOINT_DAILY_ALLOWANCES + '/?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });
  }

  checkDataExists(param:any): Observable<any>{
    let obj = {
      where: {
        companyId: param.companyId,
        type:param.type
      }
    }
    if(param.type==="employee wise"){
      obj.where["userId"]=param.userId;
    }else if(param.type==="category wise"){
      obj.where["category"] = param.category;
    }else if(param.type==="Designation-State Wise"){
      obj.where["stateId"]=param.stateId;
      obj.where["designation"]=param.designation;
    }

    if(param.isDivisionExist===true){
      obj.where["divisionId"]=param.divisionId
    }
    let url = this._urlService.API_ENDPOINT_DAILY_ALLOWANCES + '/?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });

  }

  getFixedExpense(param:any) : Observable<any>{
    let obj = {
      where: {
        companyId: param.companyId,
        type:param.type,
        // category:param.category
      }
    }
    if(param.type === "Designation-State Wise"){
      obj.where['designation'] = param.designation;
    } else if(param.type === "category wise"){
      obj.where['category'] = param.category;
    }
    else if(param.type === "employee wise"){

       obj.where["userId"]=param.userId;
    }




    if(param.isDivisionExist===true){
      obj.where["divisionId"]=param.divisionId
    }
    
    let url = this._urlService.API_ENDPOINT_DAILY_ALLOWANCES+ "/?filter=" + JSON.stringify(obj)
    return this.http.get(url, { headers: this.headers });

  }




//*****************************umesh 31-01-2020******************************

  getDaCategory(param: any): Observable<any>{

    let obj = {
      where: {
        companyId: param.companyId,
        }
    }
    if(param.hasOwnProperty('divisionId')===true){
      obj.where["divisionId"]={inq:param.divisionId}
    }

    let url = this._urlService.API_ENDPOINT_Category+ '/?filter=' + JSON.stringify(obj)

     return this.http.get(url,{headers:this.headers});
  }

// -----------------Rahul Saini (13-04-2020)-----
editDAAPrl(params): Observable<any>{
  return this.http.post(this._urlService.API_ENDPOINT_DAILY_ALLOWANCES+'/editDA?params='+JSON.stringify(params), { headers: this.headers });
}
// -----------------Rahul Saini (13-04-2020)-----



// mahender sharma 
deleteFareRateCard(obj:object,update:object):Observable<any>{

  let url = this._urlService.API_ENDPOINT_DAILY_ALLOWANCES + '/update?where=' + JSON.stringify(obj);
  return this.http.post(url, update, { headers: this.headers });
}

getFareRateCard(object):Observable<any>{
  let url: string = this._urlService.API_ENDPOINT_DAILY_ALLOWANCES + '?filter=' + JSON.stringify(object);
  return this.http.get(url, { headers: this.headers }) 
 }

}



