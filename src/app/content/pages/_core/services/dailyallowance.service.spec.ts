import { TestBed } from '@angular/core/testing';

import { DailyallowanceService } from './dailyallowance.service';

describe('DailyallowanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyallowanceService = TestBed.get(DailyallowanceService);
    expect(service).toBeTruthy();
  });
});
