import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserDetailsService } from '../../../../core/services/user-details.service';
import { uniqueEmailValidator, uniqueMobileValidator, passwordValidator } from '../../components/validation/custom-validation';

//--------For Date Format---------
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import Swal from 'sweetalert2'
import { AuthenticationService } from '../../../../core/auth/authentication.service';
import { Router } from '@angular/router';
import { FileHandlingService } from '../../../../core/services/FileHandling/file-handling.service';
import { HttpEventType } from '@angular/common/http';

const moment = _rollupMoment || _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

//------------------END For Date Format-----------

@Component({
  selector: 'm-profile',
  templateUrl: './profile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [

    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class ProfileComponent implements OnInit {

  currentUser = JSON.parse(sessionStorage.currentUser);

  userForm: FormGroup;
  loginCredentialForm: FormGroup;
  constructor(public fb: FormBuilder,
    private userDetailsService: UserDetailsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _userDetailsService: UserDetailsService,
    private router: Router,
    private authService: AuthenticationService,
    private _fileHandlingService: FileHandlingService

  ) {
    this.userForm = this.fb.group({
      name: this.currentUser.name,
      mobile: [this.currentUser.mobile,
      [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ],
        //uniqueMobileValidator(this.userDetailsService)
      ],
      email: [this.currentUser.email,
      Validators.compose([
        Validators.required,
        Validators.email,
      ]),
        //uniqueEmailValidator(this.userDetailsService)
      ],
      dateOfBirth: [this.currentUser.dateOfBirth, Validators.required],
      dateOfAnniversary: this.currentUser.dateOfAnniversary,
      address: this.currentUser.address,
      gender: this.currentUser.gender,
      aadhaar: this.currentUser.aadhaar,
      PAN: this.currentUser.PAN
    });
    this.loginCredentialForm = this.fb.group({
      loggedInUserId: this.currentUser.id,
      currentPassword: [null, Validators.required],
      password: [null, Validators.required],
      confirmPassword: [null, passwordValidator],
    });

    this.loginCredentialForm.controls.password.valueChanges.subscribe(
      x => this.loginCredentialForm.controls.confirmPassword.updateValueAndValidity()
    );
  }



  get email() {
    return this.userForm.get('email') as FormControl;
  }
  get mobile() {
    return this.userForm.get('mobile') as FormControl;
  }
  get currentPassword() {
    return this.loginCredentialForm.get('currentPassword') as FormControl;
  }
  get password() {
    return this.loginCredentialForm.get('password') as FormControl;
  }
  get confirmPassword() {
    return this.loginCredentialForm.get('confirmPassword') as FormControl;
  }
  showAddImageButton;
  showEditDeleteImageButton;
  isImageSelected = false;
  imageToShow = './assets/app/media/img/users/default-user.png'; //Default Image
  selectedFile: File = null;
  imageProgress = 0;

  ngOnInit() {
    this.showAddImageButton = !this.currentUser.imageUploaded;
    this.showEditDeleteImageButton = this.currentUser.imageUploaded;
    //UserProfile is Container    
    this._fileHandlingService.getDocument("UserProfile", this.currentUser.image.modifiedFileName).subscribe(res => {
      //console.log(res);
      this.createImageFromBlob(res);
    }, err => {
      console.log(err);

    });

  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result.toString();
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }



  selectFile(event) {
    this.imageProgress = 0;
    this.selectedFile = <File>event.target.files[0];
    //Show Image Preview
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageToShow = event.target.result
      this._changeDetectorRef.detectChanges();
    }
    reader.readAsDataURL(this.selectedFile);

    //this.showAddImageButton = false;
    //this.showEditDeleteImageButton = true;
    this.isImageSelected = true;
    this._changeDetectorRef.detectChanges();
  }

  removeImage() {
    this.imageToShow = './assets/app/media/img/users/default-user.png';
    this.showAddImageButton = true;
    this.showEditDeleteImageButton = false;
    this._changeDetectorRef.detectChanges();
  }

  removeImageIfImageIsExist() {
    this._fileHandlingService.removeImage(this.currentUser.companyId, this.currentUser.id).subscribe(res => {
      this.imageToShow = './assets/app/media/img/users/default-user.png';
      this.showAddImageButton = true;
      this._changeDetectorRef.detectChanges();

      console.log(res);
    }, err => {
      console.log(err);

    })
  }

  methodToSameFileSelected(event) {
    //Method to delete same file from source which is previsioly seleccted.
    event.target.value = '';
  }

  updateUserImage() {
    this._fileHandlingService.uploadUserImage(this.currentUser.companyId, this.currentUser.id, "UserProfile", this.selectedFile).subscribe(events => {
      if (events.type === HttpEventType.UploadProgress) {
        console.log("Uploaded : " + Math.round(events.loaded / events.total * 100) + "%");
        this.imageProgress = Math.round(events.loaded / events.total * 100);

        Swal.fire({
          title: 'Image Uploaded Successfully!!!',
          text: '',
          type: 'success',
          allowOutsideClick: false
        }).then((result2) => {
          let timerInterval
          Swal.fire({
            title: 'Auto Logout Alert!',
            html: 'The Panel Will Be Logout in <strong></strong> seconds.',
            timer: 5000,
            allowOutsideClick: false,

            onBeforeOpen: () => {
              Swal.showLoading()
              timerInterval = setInterval(() => {
                //Swal.getContent().querySelector('strong').textContent = Swal.getTimerLeft()
                Swal.getContent().querySelector('strong').textContent = Math.ceil(Swal.getTimerLeft() / 1000) + ""
              }, 100)
            },
            onClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            if (
              // Read more about handling dismissals
              result.dismiss === Swal.DismissReason.timer
            ) {
              //console.log('I was closed by the timer')
              this.authService.logout(true);
              this.router.navigate(['login']);
            }
          })
        });

        this._changeDetectorRef.detectChanges();
      } else if (events.type === HttpEventType.Response) {
        //this.imageUrl = './assets/app/media/img/users/default-user.png';
        this._changeDetectorRef.detectChanges();

      }
    },
      res => {
        console.log(res)
      }
    )
  }

  updatePersonalInformations(info) {

    if (this.currentUser.email == this.userForm.value.email) {
      //Same Email Id
    } else {
      let obj = {
        email: this.userForm.value.email
      };
      this._userDetailsService.checkUniqueness(obj).subscribe(res => {
        let EMAIl_Control = this.userForm.get('email');
        EMAIl_Control.markAsTouched();
        EMAIl_Control.markAsDirty();
        EMAIl_Control.setErrors({ 'unique': true, 'message': 'Email Already Exists' });
        this._changeDetectorRef.detectChanges();

      }, err => {
        console.log(err)
      });
    }

    if (this.currentUser.mobile == this.userForm.value.mobile) {
      //Same Email Id
    } else {
      let obj = {
        mobile: this.userForm.value.mobile
      };
      this._userDetailsService.checkUniqueness(obj).subscribe(res => {
        let Mobile_Control = this.userForm.get('mobile');
        Mobile_Control.markAsTouched();
        Mobile_Control.markAsDirty();
        Mobile_Control.setErrors({ 'unique': true, 'message': 'Mobile Number Already Exists' });
        this._changeDetectorRef.detectChanges();

      }, err => {
        console.log(err)
      });
    }

    if (this.userForm.valid) {
      this._userDetailsService.updateIndividualUserDetail(this.currentUser.companyId,this.currentUser.id, this.userForm.value).subscribe(res => {


        Swal.fire({
          title: 'Information Updated Successfully!!!',
          text: 'Your Peronal Information has been updated successfully!!!',
          type: 'success',
          allowOutsideClick: false
        }).then((result2) => {
          let timerInterval
          Swal.fire({
            title: 'Auto Logout Alert!',
            html: 'The Panel Will Be Logout in <strong></strong> seconds.',
            timer: 5000,
            allowOutsideClick: false,

            onBeforeOpen: () => {
              Swal.showLoading()
              timerInterval = setInterval(() => {
                //Swal.getContent().querySelector('strong').textContent = Swal.getTimerLeft()
                Swal.getContent().querySelector('strong').textContent = Math.ceil(Swal.getTimerLeft() / 1000) + ""
              }, 100)
            },
            onClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            if (
              // Read more about handling dismissals
              result.dismiss === Swal.DismissReason.timer
            ) {
              //console.log('I was closed by the timer')
              this.authService.logout(true);
              this.router.navigate(['login']);
            }
          })
        });
      }, err => {
        Swal.fire('Oops...', err.error.error.message, 'error');
      });

    }


  }

  updateLoginCredentials(loginForm) {
    if (loginForm.valid) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false,
      })

      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Change it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true,
        allowOutsideClick: false
      }).then((result) => {

        if (result.value) {

          this._userDetailsService.updateUserPassword(loginForm.value).subscribe(res => {
            //console.log(res)
            swalWithBootstrapButtons.fire({
              title: 'Password Changed Successfully!!!',
              text: 'Your password has been changed successfully! Thank you',
              type: 'success',
              allowOutsideClick: false
            }).then((result2) => {
              let timerInterval
              Swal.fire({
                title: 'Auto Logout Alert!',
                html: 'The Panel Will Be Logout in <strong></strong> seconds.',
                timer: 5000,
                allowOutsideClick: false,

                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    //Swal.getContent().querySelector('strong').textContent = Swal.getTimerLeft()
                    Swal.getContent().querySelector('strong').textContent = Math.ceil(Swal.getTimerLeft() / 1000) + ""
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval)
                }
              }).then((result) => {
                if (
                  // Read more about handling dismissals
                  result.dismiss === Swal.DismissReason.timer
                ) {
                  //console.log('I was closed by the timer')
                  this.authService.logout(true);
                  this.router.navigate(['login']);
                }
              })
            });



          }, err => {
            Swal.fire('Oops...', err.error.error.message, 'error');

            //console.log(err.error.error.message)

          });

        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your Password is Same as Before.',
            'error'
          )
        }
      });






    }
  }
}
