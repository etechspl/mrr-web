import { Component, OnInit, Input } from '@angular/core';


@Component({
	selector: 'm-doughnut-chart',
	templateUrl: './doughnut-chart.component.html',
	styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent implements OnInit {
	// Doughnut

	public doughnutChartLabels: string[] = ['Download', 'In-Store', 'Mail-Order'];
	public doughnutChartData: number[] = [22, 2615, 100];
	public doughnutChartType: string = 'doughnut';
	public barChartOptions:any = {
		legend: {position: 'bottom'}
	}
	public colorsEmptyObject =[{ backgroundColor: ['#86c92e', 'red'] }];

	constructor () {
	}
	ngOnInit () {

	}

	// events
	chartClicked(e: any): void {
	}

	chartHovered(e: any): void {
	}

}
