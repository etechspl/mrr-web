import { TestBed } from '@angular/core/testing';

import { DcrReportsService } from './dcr-reports.service';

describe('DcrReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DcrReportsService = TestBed.get(DcrReportsService);
    expect(service).toBeTruthy();
  });
});
