import { Observable } from "rxjs";
import { AuthenticationService } from "../../auth/authentication.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { URLService } from "../URL/url.service";
import { Injectable } from "@angular/core";
import { formatDate } from "@angular/common";

@Injectable({
	providedIn: "root",
})
export class DcrReportsService {
	constructor(
		private _urlService: URLService,
		private http: HttpClient,
		private authservice: AuthenticationService
	) { }
	headers = new HttpHeaders({
		"Content-Type": "application/json; charset=utf-8",
		Authorization: this.authservice.getAccessTokenNew(),
	});
	//companyId: string, type: string, stateIds: any, districtIds: any, userId: any, fromDate: string, toDate: string
	getEmployeeWiseDCRDetails(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRMASTER;
		var obj = {};
		// -----------nipun (09-01-2020) added status field in obj---
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
				status: params.status,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
				status: params.status,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
				status: params.status,
			};
		}
		// -----------nipun (09-01-2020) end-------

		url = baseUrl + "/getUserWiseDCRSummary?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}

	//----------------------------

	//----------------------------
	getProductStatewiseSale(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_PrimaryAndSaleReturn;
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}

		url =
			baseUrl + "/getProductStatewiseSale?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}
	//------------------
	//----------------------------
	HqWiseSecondaryReport(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_PrimaryAndSaleReturn;
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}
		url = baseUrl + "/HqWiseSecondaryReport?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}
	//------------------
	//----------------------stockist wise details-------------
	stkWiseMgrReport(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_PrimaryAndSaleReturn;
		url =
			baseUrl + "/stockistMonthWiseSale?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}
	//------------------
	//----------product wise details----------
	//----------------------stockist wise details-------------
	productWisesale(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_PrimaryAndSaleReturn;
		url = baseUrl + "/singleProductSale?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}
	//------------------
	//-------------------
	//---------------getUserInfo-----------------------

	getUserInfo(params): Observable<any> {
		let where = {
			where: {
				userId: params,
			},
		};

		let url =
			this._urlService.API_ENDPOINT_USERINFO +
			"?filter=" +
			JSON.stringify(where);
		return this.http.get(url, { headers: this.headers });
	}
	//--------------------------

	//--------------get productss----------------
	//---------------getUserInfo-----------------------

	getProductsWithSampleQty(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_Products;


		url =
			baseUrl +
			"/getProductsWithSampleQty?params=" +
			JSON.stringify(params);


		return this.http.get(url, { headers: this.headers });
	}
	//--------------------------
	//---------------------

	getUserDateWiseDCRDetail(userId: string,
		fromDate: string,
		toDate: string
	): Observable<any> {
		var obj = {};
		obj = {
		  "userId": userId,
		  "fromDate": fromDate,
		  "toDate": toDate
		}
	
		let url = this._urlService.API_ENDPOINT_DCRMASTER + '/getUserDateWiseDCRDetail?userId=' + userId + '&fromDate=' + fromDate + '&toDate=' + toDate;
		return this.http.get(url, { headers: this.headers })

	}

	
	getUserconsolidateDateWiseDCRDetail(
		userId: [],
		fromDate: string,
		toDate: string
	): Observable<any> {
		var obj = {};
		obj = {
			userId: userId,
			fromDate: fromDate,
			toDate: toDate,
		};

	

		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getUserconsolidateDateWiseDCRDetail?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}


	getUserJointworkDetails(userId: string, fromDate: string, toDate: string) {
		var obj = {};
		obj = {
			userId: userId,
			fromDate: fromDate,
			toDate: toDate,
		};
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/getUserJointworkDetails?userId=" +
			userId +
			"&fromDate=" +
			fromDate +
			"&toDate=" +
			toDate;

		return this.http.get(url, { headers: this.headers });
	}
	// getLockedDCR(userId: string, fromDate: string, toDate: string) {
	//   var obj = {};
	//   obj = {
	//     "userId": userId,
	//     "fromDate": fromDate,
	//     "toDate": toDate
	//   }
	//   let url = this._urlService.API_ENDPOINT_TrackLockOpenDetail + '/getLockingDetail?userId=' + userId + '&fromDate=' + fromDate + '&toDate=' + toDate;

	//   return this.http.get(url, { headers: this.headers })

	// }
	stationWiseWorking(
		companyId: string,
		Formtype: any,
		userIds: string
	): Observable<any> {
		var obj = {};
		if (Formtype.type == "State") {
			obj = {
				companyId: companyId,
				stateIds: Formtype.stateInfo,
				type: Formtype.type,
				month: Formtype.month,
				year: Formtype.year,
			};
		} else if (Formtype.type == "Headquarter") {
			obj = {
				companyId: companyId,
				districtIds: Formtype.districtInfo,
				type: Formtype.type,
				month: Formtype.month,
				year: Formtype.year,
			};
		} else {
			obj = {
				companyId: companyId,
				userIds: userIds,
				type: Formtype.type,
				month: Formtype.month,
				year: Formtype.year,
			};
		}
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/stationWiseWorking?params=" +
			JSON.stringify(obj);

		return this.http.get(url, { headers: this.headers });
	}
	///********************************************Umesh Kumar 26/12/2019******************************* /
	///******************************************DCR STATUS REPORT *******************************/
	getDCRStatusDetails(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRMASTER;
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}
		url = baseUrl + "/getDCRStatusNew?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}
	getUserCompleteDCRDetails(dcrId: string): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			'?filter={"where": {"id":"' +
			dcrId +
			'"} ,"include": [{"relation": "info","scope": { "order":"totalSession.timeIn ASC","include":[{"relation":"providerinfo","scope":{"fields":["providerName","address","category"] ,"order":"totalSession.timeIn ASC"}},{"relation":"block","scope":{"fields":["areaName"]}},{"relation":"stockist","scope":{"fields":["providerName"]}},{"relation":"file"}]}},{"relation":"userInfo","scope":{"fields":["name","districtName"]}}]}';



		return this.http.get(url, { headers: this.headers });
	}

	getUserCompleteDCRDoctorDetails(obj: any): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getDCRDetailDoctorWise?params=" +
			JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}

	getUserCompleteDCROnDateBasis(dcrDate: string, userId: string) {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			'?filter={"where": {"dcrDate":"' +
			dcrDate +
			'","submitBy":"' +
			userId +
			'"},"include": {"relation": "info","scope":{"fields":["providerId","productDetails","giftDetails","jointWorkWithId","detailingDone","callOutcomes","callObjective","remarks","providerType","mefAnswers"]}}}';
		return this.http.get(url, { headers: this.headers });
	}

	insertAllLockedDcr(
		companyId: string,
		divisionId: string,
		stateId: string,
		districtId: string,
		userId: string,
		designation: any,
		locktype: string,
		lockFilterType: string,
		lockDetails: number
	): Observable<any> {
		let filter = {
			companyId: companyId,
			divisionId: divisionId,
			stateId: stateId,
			districtId: districtId,
			userId: userId,
			designation: designation,
			locktype: locktype,
			lockFilterType: lockFilterType,
			lockDetails: lockDetails,
		};
		let url = this._urlService.API_ENDPOINT_DCRMASTER + "/insertAllLockedDCR?params=" + JSON.stringify(filter);
		return this.http.get(url, { headers: this.headers });
	}
	getAllLockedDcr(
		companyId: string,
		divisionId: string,
		stateId: string,
		districtId: string,
		userId: string,
		designation: any,
		locktype: string,
		lockFilterType: string,
		lockDetails: number
	): Observable<any> {
		let filter = {
			companyId: companyId,
			divisionId: divisionId,
			stateId: stateId,
			districtId: districtId,
			userId: userId,
			designation: designation,
			locktype: locktype,
			lockFilterType: lockFilterType,
			lockDetails: lockDetails,
		};
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getAllLockedDCR?params=" +
			JSON.stringify(filter);
		return this.http.get(url, { headers: this.headers });
	}

	getTotalSubmmitedAndPendingDCR(
		companyId: string,
		fromDate: string,
		toDate: string,
		status: any,
		level: number,
		userId: string
	) {
		let obj = {
			companyId: companyId,
			fromDate: fromDate,
			toDate: toDate,
			status: status,
			level: level,
			userId: userId,
		};
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getTotalSubmmitedAndPendingDCR?params=" +
			JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}

	getBasicDetails(
		companyId: string,
		month: number,
		year: number,
		level: number,
		userId: string
	) {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getBasicStatus?argCompanyId=" +
			companyId +
			"&argMonth=" +
			month +
			"&argYear=" +
			year +
			"&argLevel=" +
			level +
			"&argUserId=" +
			userId;
		return this.http.get(url, { headers: this.headers });
	}

	getCallAvgStateorEmp(
		companyId: string,
		fromDate: string,
		toDate: string,
		level: number,
		userId: string,
		unlistedValidations: object
	): any {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getDrVenAvgStateOrEmpWise?companyId=" +
			companyId +
			"&fromdate=" +
			fromDate +
			"&todate=" +
			toDate +
			"&status=" +
			status +
			"&level=" +
			level +
			"&userId=" +
			userId +
			"&unlistedValidations=" +
			JSON.stringify(unlistedValidations);
		return this.http.get<any>(url, { headers: this.headers });
	}
	getDrVenAvgMonthWise(
		companyId: string,
		fromDate: string,
		toDate: string,
		level: string,
		userIds: string,
		unlistedValidations: object
	): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getDrVenAvgMonthWise?companyId=" +
			companyId +
			"&fromdate=" +
			fromDate +
			"&todate=" +
			toDate +
			"&level=" +
			level +
			"&userIds=" +
			userIds +
			"&unlistedValidations=" +
			JSON.stringify(unlistedValidations);
		return this.http.get(url, { headers: this.headers });
	}
	//---------------------------------Praveen Kumar(24-11-2018)------------------------------
	getUntouchedDotorDetail(
		companyId: string,
		type: any,
		stateIds: any,
		districtIds: any,
		areaIds: any,
		designation: number,
		userId: any,
		viewFor: any,
		fromDate: string,
		toDate: string,
		isDivisionExist: boolean,
		status: any,
		divisionId: any,
		category: any
	): Observable<any> {
		let obj = {};
		obj = {
			companyId: companyId,
			stateIds: stateIds,
			districtIds: districtIds,
			areaIds: areaIds,
			designationLevel: designation,
			userIds: userId,
			type: type,
			viewFor: viewFor,
			fromDate: fromDate,
			toDate: toDate,
			isDivisionExist: isDivisionExist,
			status: status,
			division: divisionId,
			category: category,
		};

		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getUntouchedDoctorDetail?params=" +
			JSON.stringify(obj);
		url = url.replace(/[+]/g, "%2B");
		url = url.replace(/[*]/g, "%2A");
		url = url.replace(/&/g, "%26");
		url = url.replace(/[-]/g, "%2D");
		url = url.replace(/[.]/g, "%2E");


		return this.http.get(url, { headers: this.headers });
	}

	//------------------------------------------END-------------------------------------------
	getManagerWorkedWithMonthlyOrYearly(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/mgrWorkedWithMontlyOrYearly?params=" +
			JSON.stringify(params);
		return this.http.get<any>(url, { headers: this.headers });
	}

	//----------------------------------Praveen Singh(27-12-2018)----------------------------//
	getManagerTeamPerformance(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getManagerTeamPerformance?params=" +
			JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}

	//----------------------------------Anjana (22-01-2019)----------------------------//
	getDateWisePerformance(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getDateWisePerformance?params=" +
			JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}

	getMOnthOnMonthProgress(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/getMonthOnMOnthProgression?params=" +
			JSON.stringify(params);

		return this.http.get(url, { headers: this.headers });
	}
	getEmployeesAttendance(param: object): Observable<any> {
		let paramObj = JSON.parse(JSON.stringify(param));
		let selectedMonth = paramObj.month;
		if (selectedMonth < 10) {
			selectedMonth = "0" + selectedMonth;
		}

		let lastDay = new Date(paramObj.year, paramObj.month, 0).getDate();
		let startDate = paramObj.year + "-" + selectedMonth + "-01";
		let endDate = paramObj.year + "-" + selectedMonth + "-" + lastDay;

		let finalObject = {
			companyId: paramObj.companyId,
			fromDate: startDate,
			toDate: endDate,
			month: paramObj.month,
			year: paramObj.year,
			type: paramObj.type,
			stateIds: paramObj.stateInfo,
			districtIds: paramObj.districtInfo,
			workingStatus: paramObj.selectedWorkingTypes,
			totalWorkingTypes: paramObj.totalWorkingTypes,
			userIds: paramObj.employeeId,
			isDivisionExist: paramObj.isDivisionExist,
			status: paramObj.status,
			division: paramObj.divisionId
		};

		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getEmployeesAttendance?param=" +
			JSON.stringify(finalObject).replace(/&/g, "%26");

		return this.http.get(url, { headers: this.headers });
	}
	//---------preeti arora---
	deleteDCR(dcrObj: any): Observable<any> {
		let obj = {
			dcrId: dcrObj,
		};
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/deleteDCR?params=" +
			JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}
	releaseUserOpeningLock(param: any) {
		let url =
			this._urlService.API_ENDPOINT_USERINFO +
			"/releaseUserforOpeningLock?params=" +
			JSON.stringify(param);
		return this.http.get(url, { headers: this.headers });
	}

	getDCRCopy(whereObject: any): Observable<any> {
		let where = {
			where: {
				_id: whereObject,
			},
		};
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"?filter=" +
			JSON.stringify(where);

		return this.http.get(url, { headers: this.headers });
	}
	getSTPDetails(whereObject: any): Observable<any> {

		
		let url =
			this._urlService.API_ENDPOINT_STP +
			"?filter=" +
			JSON.stringify(whereObject);

		return this.http.get(url, { headers: this.headers });
	}
	getActiveUsers(whereObject: any): Observable<any> {
	
			let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRMASTER;

		url = baseUrl + "/getActiveLeaveUsers?params=" + JSON.stringify(whereObject);
		return this.http.get(url, { headers: this.headers });
	}

	getActiveUsersList(whereObject: any): Observable<any> {
		let url =
		this._urlService.API_ENDPOINT_DCRMASTER +
		"?filter=" +
		JSON.stringify(whereObject);
		
		return this.http.get(url, { headers: this.headers });
		}
	//------------

	//////////////Rahul Saini//////////////////////////
	getEmployeesdcrAttendance(param: object): Observable<any> {
		let paramObj = JSON.parse(JSON.stringify(param));
		let selectedMonth = paramObj.month;
		if (selectedMonth < 10) {
			selectedMonth = "0" + selectedMonth;
		}

		let lastDay = new Date(paramObj.year, paramObj.month, 0).getDate();
		let startDate = paramObj.year + "-" + selectedMonth + "-01";
		let endDate = paramObj.year + "-" + selectedMonth + "-" + lastDay;

		let finalObject = {
			companyId: paramObj.companyId,
			fromDate: startDate,
			toDate: endDate,
			month: paramObj.month,
			year: paramObj.year,
			type: paramObj.type,
			stateIds: paramObj.stateInfo,
			districtIds: paramObj.districtInfo,
			workingStatus: paramObj.selectedWorkingTypes,
			totalWorkingTypes: paramObj.totalWorkingTypes,
			userIds: paramObj.employeeId,
			isDivisionExist: paramObj.isDivisionExist,
			division: paramObj.divisionId,
		};

		//console.log("finalObject",finalObject);
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getDCREmployeesAttendance?param=" +
			JSON.stringify(finalObject).replace(/&/g, "%26");

		//console.log("url",url);

		return this.http.get(url, { headers: this.headers });
	}

	submitDCR(dcrObj: any, step: number, dcrId: string): Observable<any> {
		if (step == 0) {
			console.log("1")
			if (dcrId == "0") {
				let url = this._urlService.API_ENDPOINT_DCRMASTER;
				return this.http.post(url, dcrObj, { headers: this.headers });
			} else {
				let whereObj = {
					id: dcrId,
				};
				let url =
					this._urlService.API_ENDPOINT_DCRMASTER +
					"/update?where=" +
					JSON.stringify(whereObj);
				return this.http.post(url, JSON.stringify(dcrObj), {
					headers: this.headers,
				});
			}
		} else if (step == 1 || step == 2) {
			console.log("2")
			let url = this._urlService.API_ENDPOINT_DCRProviderVisitDetails;
			let finalObj = [];
			finalObj.push(dcrObj);

			return this.http.post(url, dcrObj, { headers: this.headers });
		} else if (step == 3) {
			console.log("3")
			let whereObj = {
				id: dcrId,
			};
			let updateObj = {
				dcrMeetings: dcrObj,
			};
			let url =
				this._urlService.API_ENDPOINT_DCRMASTER +
				"/update?where=" +
				JSON.stringify(whereObj);
			return this.http.post(url, JSON.stringify(updateObj), {
				headers: this.headers,
			});
		} else if (step == 4) {
			console.log("4")
			let whereObj = {
				id: dcrId,
			};
			let updateObj = {
				dcrExpense: dcrObj,
			};
			let url =
				this._urlService.API_ENDPOINT_DCRMASTER +
				"/update?where=" +
				JSON.stringify(whereObj);
			return this.http.post(url, JSON.stringify(updateObj), {
				headers: this.headers,
			});
		} else if (step == 11) {
			console.log("5")
			let whereObj = {
				id: dcrId,
			};

			let url =
				this._urlService.API_ENDPOINT_DCRMASTER +
				"/update?where=" +
				JSON.stringify(whereObj);
			return this.http.post(url, JSON.stringify(dcrObj), {
				headers: this.headers,
			});
		}
	}
	deleteDCRProviderVisitDetail(dcrId: string, providerType: any) {
		let whereObj = {
			dcrId: dcrId,
			providerType: providerType,
		};
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/deleteMatchedRecord?params=" +
			JSON.stringify(whereObj);
		return this.http.get(url, { headers: this.headers });
	}

	//-----------------------Divya(04-11-2019)---------------------------
	//Method Created by Divya and implemented by PK

	getDoctorCallAverage(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getDoctorCallAverage?params=" +
			JSON.stringify(params);
console.log("url : ",url);
		return this.http.get(url, { headers: this.headers });
	}
	//---------------------------END-------------------------------------
	getPobDetails(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRMASTER;
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}
		url = baseUrl + "/getPobDetails?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}

	getManagerWorkedTrerritoryWise(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/mgrWorkedWithTrerritoryWise?params=" +
			JSON.stringify(params);
		return this.http.get<any>(url, { headers: this.headers });
	}

	getJointFieldWork(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_PMTFEEDBACK +
			"/getJointFieldWork?params=" +
			JSON.stringify(params);
		return this.http.get<any>(url, { headers: this.headers });
	}

	//-----------------------PK(12-12-2019)---------------------------
	getManagerAnalysis(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/MangerDCRAnalysis?params=" +
			JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}

	//---------------------------END-------------------------------------

	// return this.http.get(url, { headers: this.headers })

	// }
	// getLockedDCR(userId: string, fromDate: string, toDate: string) {
	// var obj = {};
	// obj = {
	// "userId": userId,
	// "fromDate": fromDate,
	// "toDate": toDate
	// }
	// let url = this._urlService.API_ENDPOINT_TrackLockOpenDetail + '/getLockingDetail?userId=' + userId + '&fromDate=' + fromDate + '&toDate=' + toDate;

	// return this.http.get(url, { headers: this.headers })

	// }

	//---------------------------END-------------------------------------
	//------------------get holiday list by preeti------------
	getHolidayDetails(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_Holiday;
		var obj = {};
		// -----------nipun (09-01-2020) added status field in obj---
		obj = {
			companyId: params.companyId,
			stateIds: params.stateIds,
			type: params.type,
			fromDate: params.fromDate,
			toDate: params.toDate,
			division: params.division,
			isDivisionExist: params.isDivisionExist,
			status: params.status,
		};
		// -----------nipun (09-01-2020) end-------

		url = baseUrl + "/getholidayDetails?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}
	//-------------------
	//-------------By preeti arora 18-08-2020---------
	expenseUpdated(whereObject: any): Observable<any> {

		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/expenseUpdate?params=" +
			JSON.stringify(whereObject);
		return this.http.get(url, { headers: this.headers });

	}


	getDayPlanLoginDayEndOut(whereObject: any): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getDayPlanLoginDayEndOut?params=" +
			JSON.stringify(whereObject);

		return this.http.get(url, { headers: this.headers });
	}
	//--------------


	getMgrVisitDetails(params: object): Observable<any> {
		let url = this._urlService.API_ENDPOINT_DCRMASTER + "/getMgrVisitDetails?params=" + JSON.stringify(params);
		return this.http.get<any>(url, { headers: this.headers });
	}

	/////////////////Rahul Saini///////////////////
	getDCRDetails(params: any): Observable<any> {
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				division: params.division,
				status: params.status,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				status: params.status,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				status: params.status,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}




		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getDCRData?params=" +
			JSON.stringify(obj);



		return this.http.get(url, { headers: this.headers });


	}
	///////////////Rahul Saini///////////////////

	getPOBProductWiseDetails(params: any): Observable<any> {
		var obj = {
			companyId: params.companyId,
			submitBy: params.userId,
			fromDate: params.fromDate,
			toDate: params.toDate,
		};
		let url = this._urlService.API_ENDPOINT_DCRProviderVisitDetails + "/getProductWisePOBDetails?params=" + JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}

	//-----------Preeti Arora-----------
	getPobDetailsStockistWise(params): Observable<any> {
		console.log("params : ",params);
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRMASTER;
		url = baseUrl + "/getPobDetailsStockistWise?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}
	getPobDetailsStockistWiseSummarised(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRMASTER;
		url = baseUrl + "/getPobDetailsStockistWiseSummarised?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}
	getAllEmployeeCallAverage(params:object): Observable<any> {

	

		const url = this._urlService.API_ENDPOINT_DCRMASTER + "/getAllEmployeeCallAverage?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
		
	}

	getAllEmployeeCallLoginDetails(params): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRMASTER;

		url = baseUrl + "/getAllEmployeeLoginDetails?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}



	getMissedDoctorsMonthwise(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/getMissedDoctorsMonthwise?params=" +
			JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}


	getLeaveStatus(param:object):Observable<any>{
		const url = this._urlService.API_ENDPOINT_DCRMASTER + "/getLeaveStatusInformation?param=" +JSON.stringify(param);
		return this.http.get(url, {headers: this.headers});
	}

	getDcrStatusSummery(params : object):Observable<any>{
		let url = this._urlService.API_ENDPOINT_DCRMASTER + "/getDcrStatusSummary?params=" +JSON.stringify(params);
		return this.http.get(url,{headers: this.headers});

	}
	dcrcallreport(params:object):Observable<any>{
		let url = this._urlService.API_ENDPOINT_DCRMASTER + "/dcrCallDetails?params=" +JSON.stringify(params);
		return this.http.get(url,{headers: this.headers});
	}
	getAlldayPobEmployeeCallAverage(params:object): Observable<any> {
     const url = this._urlService.API_ENDPOINT_DCRMASTER + "/getEmployeePobAverage?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
		
	}

	// ------------------------------------------mahender------------------------------------------
	campStutas(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRMASTER +
			"/campworkingAverage?params=" +
			JSON.stringify(params);
         console.log("url : ",url);
		return this.http.get(url, { headers: this.headers });
	}
}
