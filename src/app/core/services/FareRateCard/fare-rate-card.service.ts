import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FareRateCardService {

  @Output() frcUpdatedOrNot: EventEmitter<any> = new EventEmitter();
  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  createFareRateCard(data: any) {
    let url = this._urlService.API_ENDPOINT_FARERATECARD;
    return this.http.post(url, JSON.stringify(data), { headers: this.headers });
  }

  //companyId: string, designation: string
  getFareRateCardDetails(param:any): Observable<any> {
    let obj;
    if(param.frcType == 'employee wise'){
      obj = {
        where: {
          companyId: param.companyId,
          frcType:param.frcType,
          status:true
        },
        include:[{
          relation:"userInfo",
          scope:{
            fields:{
              name:true,
              designation:true,
              designationLevel:true,
            }
          }
        }],
        // order: 'designationLevel ASC'
      };
      if(param.userId){
        obj.where["userId"] = param.userId
      }

    } else {
      obj = {
        where: {
          companyId: param.companyId,
          frcType:param.frcType,
          status:true
        },
        include:[{
          relation:"divisionMaster",
          scope:{
            fields:{
              divisionName:true
            }
          }
        }],
        order: 'designationLevel ASC'
      };
    }

    if(param.isDivisionExist===true && param.divisionId!=null){
      obj.where["divisionId"]={
        inq:param.divisionId
      }
    }
    if (param.hasOwnProperty("designation")===true) {
      obj.where["designation"] = param.designation;
    }

    if(param.hasOwnProperty("daCategory")===true){
      obj.where["frcCode"]=param.daCategory;
    }    
    
    console.log("xy",obj);

    let url: string = this._urlService.API_ENDPOINT_FARERATECARD + '?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  updateFareRateCard(obj: Object): Observable<any>{
    return this.http.get(this._urlService.API_ENDPOINT_FARERATECARD+'/editFRC?params='+ JSON.stringify(obj));
  }
  getFareRateCard(obj:object):Observable<any>{
    let url: string = this._urlService.API_ENDPOINT_STP + '?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })  }

  deleteFareRateCard(obj:object,update:object):Observable<any>{
    let url = this._urlService.API_ENDPOINT_FARERATECARD + '/update?where=' + JSON.stringify(obj);
    return this.http.post(url, update, { headers: this.headers });
  }

}
