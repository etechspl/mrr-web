import { TestBed } from '@angular/core/testing';

import { FareRateCardService } from './fare-rate-card.service';

describe('FareRateCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FareRateCardService = TestBed.get(FareRateCardService);
    expect(service).toBeTruthy();
  });
});
