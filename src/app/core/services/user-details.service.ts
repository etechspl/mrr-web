import { UserLogin } from './../../content/pages/_core/models/userlogin.model';
import { URLService } from './URL/url.service';
import { AuthenticationService } from '../auth/authentication.service';
import { Observable, forkJoin } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { EdituserComponent } from '../../content/pages/components/reports/edituser/edituser.component';
import { UserDetailsComponent } from '../../content/pages/components/reports/user-details/user-details.component';


@Injectable({
  providedIn: 'root'
})
export class UserDetailsService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }
  userLogin = {};
  userInfo = {};

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  currentUser = JSON.parse(sessionStorage.currentUser);

  getReportingManager(companyId: string, stateId: string, designationLevel: number): Observable<UserLogin> {
    var obj = {
      "stateId": stateId,
      "companyId": companyId,
      "designationLevel": designationLevel
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + '/getReportingManager?params=' + JSON.stringify(obj);

    return this.http.get<UserLogin>(url, { headers: this.headers })
  }

  createLogin(userModel: any, currentUser: Object) {
    let url = this._urlService.API_ENDPOINT_USERLOGINS;
    this.userLogin = {
      "companyId": currentUser['companyId'],
      "companyName": currentUser['companyName'],
      //"designation": userModel.designation['designation'],
      //"designationLevel": userModel.designation['designationLevel'],
      "name": userModel.name,
      "age": userModel.age,
      "gender": userModel.gender,
      "address": userModel.address,
      "dateOfBirth": userModel.dateOfBirth,
      "dateOfAnniversary": userModel.dateOfAnniversary,
      "dateOfJoining": userModel.dateOfJoining,
      "aadhaar": userModel.aadhaar,
      "PAN": userModel.PAN,
      "DLNumber": userModel.DLNumber,
      "mobile": userModel.mobile,
      "email": userModel.email,
      "username": userModel.username,
      "viewPassword": userModel.password,
      "password": userModel.password,
      "imageId": "5ca6a4a5b0980e55a1bb19b3",
      "imageUploaded": false,
      "employeeCode": userModel.employeeCode,
      "deactivationDate": new Date("1900-01-01"),
      bankName: userModel.bank,
      bankAccountNumber: userModel.account,
      ifsc: userModel.ifsc,



    };
    return this.http.post(url, this.userLogin, { headers: this.headers })
  }

  createUserInfo(userModel: any, userCreatedResponse: object, currentUser: Object): Observable<any> {
    let url1 = this._urlService.API_ENDPOINT_USERINFO;
    let rl;
    if (userModel.designation['designationLevel'] === 1) {
      rl = 1;
    } else if (userModel.designation['designationLevel'] >= 1) {
      rl = 2;
    } else if (userModel.designation['designationLevel'] === 0) {
      rl = 0;
    };
    if (userModel['divisionId'] == null) {
      this.userInfo = {
        "companyId": currentUser['companyId'],
        "companyName": currentUser['companyName'],
        "userId": userCreatedResponse['id'],
        "stateId": userModel.stateId,
        "districtId": userModel.districtId,
        "assignDistricts": [{}],
        "rL": rl,
        "editLockingPeriod": currentUser['company'].validation['editLockingPeriod'],
        "reportingDate": userModel.dateOfReporting,
        "designation": userModel.designation['designation'],
        "designationLevel": userModel.designation['designationLevel'],
        "lockingPeriod": userModel.lockingPeriod,
        "name": userModel.name,
        "religion": userModel['religion'],
        "promotedOn": new Date("1900-01-01T00:00:00.000+0000"),
        "deactivationDate": new Date("1900-01-01T00:00:00.000+0000")

      };
    } else {
      this.userInfo = {
        "companyId": currentUser['companyId'],
        "companyName": currentUser['companyName'],
        "userId": userCreatedResponse['id'],
        "stateId": userModel.stateId,
        "districtId": userModel.districtId,
        "assignDistricts": [{}],
        "rL": rl,
        "editLockingPeriod": currentUser['company'].validation['editLockingPeriod'],
        "reportingDate": userModel.dateOfReporting,
        "designation": userModel.designation['designation'],
        "designationLevel": userModel.designation['designationLevel'],
        "lockingPeriod": userModel.lockingPeriod,
        "name": userModel.name,
        "religion": userModel['religion'],
        "divisionId": userModel['divisionId'],
        "isMainDivision": userModel['isMainDivision'],
        "promotedOn": new Date("1900-01-01T00:00:00.000+0000"),
        "deactivationDate": new Date("1900-01-01T00:00:00.000+0000"),
        "daCategory": userModel.daCategory
      };
    }

    return this.http.post(url1, this.userInfo, { headers: this.headers });
  }


  getEmployees(companyId: string, id: string): Observable<UserLogin> {
    let url = this._urlService.API_ENDPOINT_USERLOGINS + '?filter={"where":{"_id":"' + id + '","companyId":"' + companyId + '"}}';

    return this.http.get<UserLogin>(url, { headers: this.headers })
  }

  getEmployee(companyId: string, userId: string): Observable<any> {
    let filter = {
      where: { id: userId, companyId: companyId }
    }
    return this.http.get(this._urlService.API_ENDPOINT_USERINFO + '?filter={"where":{"userId":"' + userId + '","companyId":"' + companyId + '"}}');
  }

  //REST For USER INFO GET METHOD -------------Gourav
  get(filter): Observable<any> {
    return this.http.get(this._urlService.API_ENDPOINT_USERINFO + '?filter='+JSON.stringify(filter));
  }



  getUserInfoBasedOnDesignation(companyId: string, designationObject: any, status: any): Observable<any> {
    let currentUser = JSON.parse(sessionStorage.currentUser);
    let obj = {};
    if (typeof (designationObject) == "object") {
      let temp = [];
      let designationLevelForPasssing = [];
      temp[0] = designationObject
      for (let i = 0; i < temp[0].length; i++) {
        designationLevelForPasssing.push(temp[0][i].designationLevel)
      }

      obj = {
        companyId: companyId,
        designationLevel: designationLevelForPasssing,
        status: status,
        checkingLevel: currentUser.userInfo[0].designationLevel,
        checkingUserId: currentUser.userInfo[0].userId,
        isDivisionExist: false
      }

    } else {
      obj = {
        companyId: companyId,
        designationLevel: [designationObject],  //Single Designation at a time
        status: status,
        checkingLevel: currentUser.userInfo[0].designationLevel,
        checkingUserId: currentUser.userInfo[0].userId,
        isDivisionExist: false,
        include: [
          {
            relation:'district',
            scope:{
              fields:{
                'districtName':true,
                'id':true
              }
            }
          }
        ]
      }
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + "/getUserInfoBasedOnDesignation?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    return this.http.get<any>(url)

  }

  getUserInfoBasedOnDesignations(companyId: string, designationObject: any, status: any,stateIds:any): Observable<any> {
        let currentUser = JSON.parse(sessionStorage.currentUser);
        let obj = {};
        if (typeof (designationObject) == "object") {
          let temp = [];
          let stateId =[];
          let designationLevelForPasssing = [];
          temp[0] = designationObject
          for (let i = 0; i < temp[0].length; i++) {
            designationLevelForPasssing.push(temp[0][i].designationLevel)
          }
    
          for (let i = 0; i < stateIds.length; i++) {
            stateId.push(stateIds[i])
          }
          obj = {
            companyId: companyId,
            designationLevel: designationLevelForPasssing,
            status: status,
            state: stateId,
            checkingLevel: currentUser.userInfo[0].designationLevel,
            checkingUserId: currentUser.userInfo[0].userId,
            isDivisionExist: false
          }
          console.log("--------------------------",obj)

        } else {
          obj = {
            companyId: companyId,
            designationLevel: [designationObject],  //Single Designation at a time
            status: status,
            checkingLevel: currentUser.userInfo[0].designationLevel,
            checkingUserId: currentUser.userInfo[0].userId,
            isDivisionExist: false,
           
          }
        }
    
        let url = this._urlService.API_ENDPOINT_USERINFO + "/getUserInfoBasedOnDesignation?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
        return this.http.get<any>(url)
    
      }
  getMgrBasedOnStateAndDesignation(companyId: string, stateInfo: any, designationLevel: number, status: any, designation?: string): Observable<any> {

    let filter = {
      where: {
        companyId: companyId,
        designationLevel: designationLevel,
        stateId: { inq: stateInfo },
        status: { inq: status },

      },
      fields: {
        id: true,
        userId: true,
        name: true,
        designation: true,
        designationLevel: true
      }
    }
    if (designation !== undefined) {
      filter.where['designation'] = designation;
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);

    //let url = this._urlService.API_ENDPOINT_USERINFO + "/getUserInfoBasedOnDesignation?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    return this.http.get<any>(url)
  }


  // --------------------------- By Gourav for getting employee on division 10-06-2021  Start------------------------
  getEmployeeOnDivision(obj): Observable<any> {
    let filter = {
      where: {
        companyId: obj.companyId,
        divisionId: obj.division,
        designationLevel: obj.designation
      }, fields: {
        districtId: true,
        userId: true,
        name: true,
        designation: true,
        designationLevel: true
      }, include: [
        {
          relation: 'district',
          scope: {
            fields: {
              "districtName": true,
              "id": true
            }
          }
        },
        // {
        //   relation:'hierarchy',
        //   scope:{
        //     fields:{
        //       "supervisorId":true
        //     }
        //   }
        // }
      ],order: 'name ASC'
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get<any>(url);
  }
  // --------------------------- By Gourav for getting employee on division 10-06-2021  End------------------------

  // -----------------------------method Created by Gourav for update Hierarchy component 08-06-2021  START--------------------------------------------------------------------------------
  getEmployeeOnStateDesignationAndDivision(companyId: string, stateInfo: any, designationLevel: number, status: any, designation: string, division: any): Observable<any> {
    let isDivisionExist = false;
    if (this.currentUser.company.isDivisionExist == true) {
      isDivisionExist = true;
    } else {
      isDivisionExist = false;
    }
    let filter = {
      where: {
        companyId: companyId,
        designationLevel: designationLevel,
        stateId: { inq: stateInfo },
        status: { inq: status },
        divisionId: division
      },
      fields: {
        id: true,
        userId: true,
        name: true,
        designation: true,
        designationLevel: true,
        divisionId:true
      }
    }
    if (designation !== undefined) {
      filter.where['designation'] = designation;
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);

    //let url = this._urlService.API_ENDPOINT_USERINFO + "/getUserInfoBasedOnDesignation?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    return this.http.get<any>(url)
  }
  // -----------------------------method Created by Gourav for update Hierarchy component 08-06-2021  END--------------------------------------------------------------------------------


  // getMgrBasedOnStateAndDesignationAndDesigLVL(companyId: string, stateInfo: any, designationLevel: number, status: any, designation?): Observable<UserLogin> {

  //   let filter = {
  //     where: {
  //       companyId: companyId,
  //       designation: designation,
  //       designationLevel: designationLevel,
  //       stateId: { inq: stateInfo },
  //       status: { inq: status }
  //     },
  //     fields: {
  //       id: true,
  //       userId: true,
  //       name: true,
  //     }
  //   }
  //   let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
  //   //let url = this._urlService.API_ENDPOINT_USERINFO + "/getUserInfoBasedOnDesignation?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
  //   return this.http.get<UserLogin>(url)
  // }

  //getting users list based on stateIds
  // getUsers(companyId: string, stateIdsArr: any, isDivisionExist: boolean, divisionIds: any): Observable<any> {
  //   let obj = {
  //     companyId: companyId,
  //     stateIds: stateIdsArr,
  //     isDivisionExist: isDivisionExist,
  //     division: divisionIds
  //   }
  //   let url = this._urlService.API_ENDPOINT_USERINFO + '/getUsers?param=' + JSON.stringify(obj);
  //   return this.http.get<any>(url, { headers: this.headers });
  // }
  getUsers(object: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getUsers?param=' + JSON.stringify(object);
    return this.http.get<any>(url, { headers: this.headers });
  }

  
  updateStatus(obj: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USERINFO + '/updateStatus';
    return this.http.post<any>(url, { param: obj }, { headers: this.headers });
  }

  getUsersForTrueStatus(object: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(object);
    return this.http.get<any>(url, { headers: this.headers })
  }

  checkUniqueness(params: Object): Observable<UserLogin> {
    var obj = {
      "where": {}
    }
    if (params.hasOwnProperty('mobile') === true) {
      obj.where['mobile'] = params['mobile'];
    } else if (params.hasOwnProperty('email') === true) {
      obj.where['email'] = params['email'];
    } else if (params.hasOwnProperty('username') === true) {
      obj.where['username'] = params['username'];
    }
    let url = this._urlService.API_ENDPOINT_USERLOGINS + '?filter=' + JSON.stringify(obj);
    return this.http.get<UserLogin>(url, { headers: this.headers });
  }

  getManagerStatewise(companyId, stateId) {
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getManagerStatewise?companyId=' + companyId + '&stateId=' + stateId;
    return this.http.get<any>(url, { headers: this.headers })
  }

  getAllManager(companyId) {
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getAllManager?companyId=' + companyId;
    return this.http.get<any>(url, { headers: this.headers })
  }


  // -----------------------------method Created by Gourav for update Hierarchy component 08-06-2021 START--------------------------------------------------------------------------------
  getAllManagerOnDivision(companyId, division) {
    let isDivisionExist = false;
    if (this.currentUser.company.isDivisionExist == true) {
      isDivisionExist = true;
    } else {
      isDivisionExist = false;
    }
    let param = {
      companyId: companyId,
      division: division,
      isDivisionExist: isDivisionExist
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getAllManagerDivision?param=' + JSON.stringify(param);
    return this.http.get<any>(url, { headers: this.headers })
  }
  // -----------------------------method Created by Gourav for update Hierarchy component 08-06-2021  END--------------------------------------------------------------------------------

  getEmployeeLinkValue(companyId: string, districts: any): Observable<any> {
    var obj;
    console.log(this.currentUser.company.target);

    obj = {
      "companyId": companyId,
      "districts": districts,
      "designationLevel": this.currentUser.company.target.enableForMgr.desigLevel,
      "rL": this.currentUser.company.target.enableForMgr.rL,
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + '/getAllUserIdsOnMultiDistrictBasis?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  getEmployeeStateDistDesig(companyId: string, districts: any, designationLevel: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "districtId": districts,
      "designationLevel": designationLevel
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getAllUsersStateDistrictBasis?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }



  getDesignationValue(companyId: string, stateInfo: any, districtInfo: any, reportType: string): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "stateId": stateInfo,
      "districtInfo": districtInfo,
      "reportType": reportType
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getAllDesignationOnStateBasis?param=' + JSON.stringify(obj);

    return this.http.get(url, { headers: this.headers })
  }

  //getting users list based on designatin and stateId or DistrictId
  getEmployeeDesignationStatewiseOrDistrictWise(params: Object): Observable<any> {
    let filter = {};
    let designations = [];
    for (let desig of params['designation']) {
      designations.push(desig['designation'])
    }
    if (params['type'] === 'state') {
      filter = {
        type: "state",
        stateId: params['stateId'],
        companyId: params['companyId'],
        designation: designations
      }
    } else {
      filter = {
        type: "district",
        stateId: params['stateId'],
        companyId: params['companyId'],
        districtId: params['districtId'],
        designation: designations
      }
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getEmployeeDesignationStatewiseOrDistrictWises?params=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers });
  }

  getUserFromUserInfo(users: Object): Observable<any> {
    let filter = {
      where: {
        userId: {
          inq: users['userId']
        }
      },
      fields: {
        stateId: true,
        districtId: true,
        userId: true,
        designation: true,
        designationLevel: true
      }
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
  }


  //----------------------Delete Users-------------------------------------------
  updatedUserStatus(modifyUserId: string, companyId: string, updatedByuserId: string, changeTo: boolean, result: string): Observable<UserDetailsComponent> {
    //delDetails['companyId'] = companyId;
    // delDetails['updatedBy'] = userId;
    // delDetails['changeTo'] = changeTo;
    let passingObj = {
      companyId: companyId,
      changeTo: changeTo,
      updatedBy: updatedByuserId,
      modifyUserId: modifyUserId,
      msg: result
    }
    let url = this._urlService.API_ENDPOINT_USERLOGINS + '/updatedUserStatus?params=' + JSON.stringify(passingObj);
    return this.http.get<UserDetailsComponent>(url);
  }

  //-----------------------------------------------------------------------------
  editUser(editUserDetail: Object, companyId: string, session_id: string): Observable<EdituserComponent> {

    editUserDetail['companyId'] = companyId;
    editUserDetail['employeeCode'] = editUserDetail['employeeCode'];
    editUserDetail['name'] = editUserDetail['name'].toString();
    editUserDetail['dateOfBirth'] = new Date(editUserDetail['dateOfBirth']);
    editUserDetail['dateOfAnniversary'] = new Date(editUserDetail['dateOfAnniversary']);
    editUserDetail['dateOfJoining'] = new Date(editUserDetail['dateOfJoining']);
    editUserDetail['aadhaar'] = editUserDetail['aadhaar'];
    editUserDetail['mobile'] = editUserDetail['mobile'];
    editUserDetail['email'] = editUserDetail['email'];
    editUserDetail['username'] = editUserDetail['username'];
    editUserDetail['designation'] = editUserDetail['designation'];
    editUserDetail['designationLevel'] = editUserDetail['designationLevel'];
    editUserDetail['PAN'] = editUserDetail['PAN'];
    editUserDetail['address'] = editUserDetail['address'];
    editUserDetail['password'] = editUserDetail['password'];
    editUserDetail['status'] = editUserDetail['status'];
    editUserDetail['lockingPeriod'] = editUserDetail['lockingPeriod'];
    editUserDetail['editLockingPeriod'] = editUserDetail['editLockingPeriod'];
    editUserDetail['immediateSupervisorName'] = editUserDetail['immediateSupervisorName'];
    editUserDetail['immediateSupervisorId'] = editUserDetail['immediateSupervisorId'];
    editUserDetail['updatedBy'] = session_id;
    editUserDetail['reportingDate'] = new Date(editUserDetail['reportingDate']);
    editUserDetail['updatedAt'] = new Date();
    editUserDetail['divisionId'] = editUserDetail['divisionId'];

    editUserDetail['bankName'] = editUserDetail['bankName'];
    editUserDetail['bankAccountNumber'] = editUserDetail['bankAccountNumber'];
    editUserDetail['ifsc'] = editUserDetail['ifsc'];

    let url = this._urlService.API_ENDPOINT_USERLOGINS + '/editUser?params=' + JSON.stringify(editUserDetail);
    return this.http.get<EdituserComponent>(url);
  }
  //update by praveen singh for getting the districtwise users(mr and mgr)
  getEmployeeListWithMgr(obj: object): Observable<any> {
    let filter = {};

    if (obj["isDivisionExist"] == true) {
      filter = {
        where: {
          companyId: obj["companyId"],
          divisionId: obj["division"],
          stateId: obj["stateId"],
          districtId: obj["districtId"],
          status: true
        },
        fields: {
          name: true,
          stateId: true,
          districtId: true,
          userId: true,
          designation: true,
          designationLevel: true
        },
        order: "name ASC"
      }
    } else {
      filter = {
        where: {
          companyId: obj["companyId"],
          stateId: obj["stateId"],
          districtId: obj["districtId"],
          status: true
        },
        fields: {
          name: true,
          stateId: true,
          districtId: true,
          userId: true,
          designation: true,
          designationLevel: true
        },
        order: "name ASC"
      }
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })

  }
  //added by praveen sing for getting users list based on state wise or district wise
  getEmployeeStateWiseOrDistrictWise(params: object): Observable<any> {
    let paramObj = JSON.parse(JSON.stringify(params));
    let filter = {};
    if (paramObj.hasOwnProperty("districtId") === true && paramObj.hasOwnProperty("status") === true) {
      // Anjana rana- 08-07-2019- Customized for Data Upload Module
      if (paramObj.hasOwnProperty("division") === true) {
        filter = {
          where: {
            companyId: paramObj["companyId"],
            stateId: { inq: paramObj["stateId"] },
            districtId: { inq: paramObj["districtId"] },
            status: status,
            rL: 1,
            divisionId: { inq: paramObj["divisionId"] },
          },
          fields: {
            name: true,
            userId: true
          }
        }
      } else {
        filter = {
          where: {
            companyId: paramObj["companyId"],
            stateId: { inq: paramObj["stateId"] },
            districtId: { inq: paramObj["districtId"] },
            status: true,
            rL: 1
          },
          fields: {
            name: true,
            userId: true
          }
        }
      }

    } else if (paramObj.hasOwnProperty("districtId") === true) {
      filter = {
        where: {
          companyId: paramObj["companyId"],
          stateId: { inq: paramObj["stateId"] },
          districtId: { inq: paramObj["districtId"] }
        },
        fields: {
          name: true,
          userId: true
        }
      }
    } else {
      filter = {
        where: {
          companyId: paramObj["companyId"],
          stateId: { inq: paramObj["stateId"] }
        },
        fields: {
          name: true,
          userId: true
        }
      }
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers });
  }

  getLockOpenDetails(companyId: string, stateId: string, districtId: string, userId: string, designation: any, locktype: string, lockFilterType: string, lockDetails: number): Observable<any> {
    let filter = {};
    let stateIdsArr = [];
    let updateData = {};
    let hqIdArray = [];
    let desigArray = [];
    let empArray = [];
    if (locktype == "day") {
      if (lockFilterType == "state") {
        for (var i = 0; i < stateId.length; i++) {
          stateIdsArr.push(stateId[i]);
        }
        filter = {
          companyId: companyId,
          stateId: { inq: stateIdsArr }
        }
      } else if (lockFilterType == "Headquarter") {

        for (var i = 0; i < districtId.length; i++) {
          hqIdArray.push(districtId[i]);
        }
        filter = {
          companyId: companyId,
          districtId: { inq: hqIdArray }
        }
      }
      else if (lockFilterType == "designation") {
        for (var i = 0; i < designation.length; i++) {
          desigArray.push(designation[i].designation);
        }
        filter = {
          companyId: companyId,
          designation: { inq: desigArray }
        }
      }
      else if (lockFilterType == "employee") {
        for (var i = 0; i < userId.length; i++) {
          empArray.push(userId[i]);
        }
        filter = {
          companyId: companyId,
          userId: { inq: empArray }
        }

      }

    }

    updateData = {
      lockingPeriod: lockDetails,
      updatedAt: new Date()
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + '/update?where=' + JSON.stringify(filter);
    //return null
    return this.http.post(url, updateData, { headers: this.headers });

  }
  //-----------------------end ---------------------------------
  submitLockOpenDetails(companyId: string, stateId: string, districtId: string, userId: string, designation: any, locktype: string, lockFilterType: string, lockDetails: number) {
    let filter = {
      companyId: companyId,
      stateId: stateId,
      districtId: districtId,
      userId: userId,
      designation: designation,
      locktype: locktype,
      lockFilterType: lockFilterType,
      lockDetails: lockDetails
    };

    let url = this._urlService.API_ENDPOINT_TrackLockOpenDetail + '/submitLockOpenDetails?params=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers });
  }

  updateIndividualUserDetail(companyId: string, userId: string, updateObject: object) {
    let whereObjForUserInfo = { companyId: companyId, userId: userId };

    let whereObjForUserLogin = { id: userId };

    let urlForUserLogin = this._urlService.API_ENDPOINT_USERLOGINS + '/update?where=' + JSON.stringify(whereObjForUserLogin);

    let urlForUserInfo = this._urlService.API_ENDPOINT_USERINFO + '/update?where=' + JSON.stringify(whereObjForUserInfo)

    let UserInfoAPI = this.http.post(urlForUserInfo, { name: updateObject['name'] }, { headers: this.headers })

    let UserLoginAPI = this.http.post(urlForUserLogin, JSON.stringify(updateObject), { headers: this.headers })

    return forkJoin([UserInfoAPI, UserLoginAPI]);
  }

  updateUserPassword(credentials: object) {

    let jsonCredentialsObj = JSON.parse(JSON.stringify(credentials));

    let updateObj = {
      userId: jsonCredentialsObj.loggedInUserId,
      oldPassword: jsonCredentialsObj.currentPassword,
      newPassword: jsonCredentialsObj.confirmPassword
    }
    let url = this._urlService.API_ENDPOINT_USERLOGINS + '/changePasswordAPI/'
    return this.http.post(url, updateObj, { headers: this.headers })

  }


  //--------------------------------PK(2019-06-20)-----------------------------------
  getUserInfoBasedOnDesignationAndBasedOnDivision(object: any): Observable<any> {
    let currentUser = JSON.parse(sessionStorage.currentUser);
    let obj = {};
    if (typeof (object.designationObject) == "object") {

      // let temp = [];
      let designationLevelForPasssing = object.designationObject.map((item) => { return item.designationLevel });
      // temp[0] = object.designationObject
      // for (let i = 0; i < temp[0].length; i++) {
      //   designationLevelForPasssing.push(temp[0][i].designationLevel)
      // }
      obj = {
        companyId: object.companyId,
        designationLevel: designationLevelForPasssing,
        status: object.status,
        stateId: object.stateIds,
        checkingLevel: currentUser.userInfo[0].designationLevel,
        checkingUserId: currentUser.userInfo[0].userId,
        isDivisionExist: this.currentUser.company.isDivisionExist,
        division: object.division
      }

console.log("obj 1",obj)
    } else {

      obj = {
        companyId: object.companyId,
        designationLevel: [object.designationObject],  //Single Designation at a time
        status: object.status,
        stateId: object.stateIds,
        checkingLevel: currentUser.userInfo[0].designationLevel,
        checkingUserId: currentUser.userInfo[0].userId,
        isDivisionExist: this.currentUser.company.isDivisionExist,
        division: object.division
      }
      console.log("obj 2",obj)

    }
    //if stateId exists
    if (object.hasOwnProperty("stateId") === true) {
      obj["stateId"] = object.stateId
    }
    //if district id is exists
    if (object.hasOwnProperty("districtId") === true) {
      obj["districtId"] = object.districtId
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + "/getUserInfoBasedOnDesignation?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    return this.http.get<any>(url)

  }

  //-----------------------------------END--------------------------------------------
  insertUserRoleInUserAccess(param: any) {
    let url = this._urlService.API_ENDPOINT_UserAccess;
    return this.http.post(url, param, { headers: this.headers })

  }
  getUserInfoOnTypeBasis(obj: any) {
    let filter = {};
    if (obj.type == "State") {
      filter = {
        where: {
          companyId: obj.companyId,
          stateId: {
            inq: obj['stateId']
          },
          status: true,
          designationLevel: { neq: 0 }
        },
        fields: {
          stateId: true,
          districtId: true,
          userId: true
        }
      }
    } else if (obj.type == "Headquarter") {
      filter = {
        where: {
          companyId: obj.companyId,
          districtId: {
            inq: obj['districtIds']
          },
          status: true,
          designationLevel: { neq: 0 }
        },
        fields: {
          stateId: true,
          districtId: true,
          userId: true
        }
      }
    } else {
      filter = {
        where: {
          companyId: obj.companyId,
          userId: {
            inq: obj['employeeId']
          }
        },
        fields: {
          stateId: true,
          districtId: true,
          userId: true
        }
      }
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
  }
  //------------------ get details of resigned employees---by preeti arora 29-11-2019-------------//
  getResignedEmployeeDetails(params): Observable<any> {
    let url;
    let baseUrl = this._urlService.API_ENDPOINT_USERINFO;
    var obj = {};
    if (params.type == "State") {
      obj = {
        "companyId": params.companyId,
        "stateIds": params.stateIds,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist
      }
    } else if (params.type == "Headquarter") {
      obj = {
        "companyId": params.companyId,
        "districtIds": params.districtIds,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist
      }
    } else {
      obj = {
        "companyId": params.companyId,
        "userIds": params.employeeId,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist
      }
    }

    url = baseUrl + '/getResignedEmployeeDetails?params=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })

  }

  //---------------------------end ------------------------------------




  //----------------------Praveen Kumar-----------------------------

  updateUserInfo(whereObject: object, updateObject: object): Observable<any> {

    console.log("whereObject",whereObject);
    console.log("updateObject",updateObject);
    const url = this._urlService.API_ENDPOINT_USERINFO + '/update?where=' + JSON.stringify(whereObject);
    return this.http.post(url, updateObject, { headers: this.headers });
  }
  //------------------------END-------------------------------------------

  //------------------ Nipun (2020-01-03)------------------------------------------------
  getUsersList(whereObject: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USERLOGINS + '?filter=' + JSON.stringify(whereObject);
    return this.http.get<UserLogin>(url)
  }
  //----------------------------------END---------------------------------------------
  //------------------ Rahul Saini (2020-04-24)------------------------------------------------
  getInactiveUsersList(whereObject: object): Observable<any> {


    let url = this._urlService.API_ENDPOINT_USERLOGINS + '?filter=' + JSON.stringify(whereObject);
    return this.http.get<UserLogin>(url)
  }
  //----------------------------------END---------------------------------------------
  //------------Preeti Arora------------
  TransferUser(obj: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USERINFO + '/userTransfer?param=' + JSON.stringify(obj);
    return this.http.get<UserLogin>(url)
  }
  //--------------------

  //------------Rahul Choudhary-(2020-01-21)-----------------------------------------------------
  updateDayPlanLockOpen(param: any, companyId: string, updatedBy: string, isDivisionExist: any, plantype: string): Observable<any> {
    let obj = {};
    if (plantype === 'dp') {
      param.lockOpenDate = new Date(new Date().setHours(param.lockOpenDate.substring(0, 2), param.lockOpenDate.substring(3, 5)));
    }
    if (isDivisionExist == true) {
      if (param.type == "State") {
        obj = {
          companyId: companyId,
          stateId: param.stateInfo,
          type: param.type,
          //   : param.lockOpenDate,
          divisionId: param.divisionId,
          isDivisionExist: true,
          updatedBy: updatedBy
        };
        (plantype === 'tp') ? obj["date"] = param.lockOpenDateTp : obj["lockOpenDate"] = param.lockOpenDate;
      } else if (param.type == "Headquarter") {

        obj = {
          companyId: companyId,
          stateId: param.stateInfo,
          districtId: param.districtInfo,
          type: param.type,
          //  lockOpenDate : param.lockOpenDate.toISOString(),
          divisionId: param.divisionId,
          isDivisionExist: true,
          updatedBy: updatedBy
        };
        (plantype === 'tp') ? obj["date"] = param.lockOpenDateTp : obj["lockOpenDate"] = param.lockOpenDate.toISOString();
      } else if (param.type == "Employee Wise") {
        obj = {
          companyId: companyId,
          designation: param.designation,
          employeeId: param.employeeId,
          type: param.type,
          //  lockOpenDate : param.lockOpenDate,
          divisionId: param.divisionId,
          isDivisionExist: true,
          updatedBy: updatedBy
        };
        (plantype === 'tp') ? obj["date"] = param.lockOpenDateTp : obj["lockOpenDate"] = param.lockOpenDate;
      }
    } else {
      if (param.type == "State") {
        obj = {
          companyId: companyId,
          stateId: param.stateInfo,
          type: param.type,
          // lockOpenDate : param.lockOpenDate,
          isDivisionExist: false,
          updatedBy: updatedBy
        };
        (plantype === 'tp') ? obj["date"] = param.lockOpenDateTp : obj["lockOpenDate"] = param.lockOpenDate;
      } else if (param.type == "Headquarter") {

        obj = {
          companyId: companyId,
          stateId: param.stateInfo,
          districtId: param.districtInfo,
          type: param.type,
          // lockOpenDate : param.lockOpenDate,
          isDivisionExist: false,
          updatedBy: updatedBy
        };
        (plantype === 'tp') ? obj["date"] = param.lockOpenDateTp : obj["lockOpenDate"] = param.lockOpenDate;
      } else if (param.type == "Employee Wise") {
        obj = {
          companyId: companyId,
          designation: param.designation,
          employeeId: param.employeeId,
          type: param.type,
          // lockOpenDate : param.lockOpenDate,
          isDivisionExist: false,
          updatedBy: updatedBy
        };
        (plantype === 'tp') ? obj["date"] = param.lockOpenDateTp : obj["lockOpenDate"] = param.lockOpenDate;
      }
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getUserDetailsForDayPlanLockOpen?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })

  }

  // ----------------nipun(01-20-2020)-----------
  getUserDetailsDesignationWise(obj: any): Observable<any> {

    return this.http.get(this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(obj));
  }
  // ----------------nipun(01-20-2020)-----------


  getEmployeeListOfStates(obj: any): Observable<any> {

    let filter = {
      where: obj,
      include: [{
        relation: 'state',
        scope: {
          fields: { "stateName": true }
        }
      },
      {
        relation: 'district',
        scope: {
          fields: {
            "districtName": true,
            "id": true
          }
        }
      }]
    }

    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })

  }


  //----------------Praveen Kumar(06-04-2020)
  getEmployeeBasedOnDivisionOrStateOrDistrictAndDesignationORAllEmp(obj: any) {
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getEmployeeBasedOnDivisionOrStateOrDistrictAndDesignationORAllEmp?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });
  }


  getRolesLabel() {
    let obj = {
      "status": true
    }

    let url = this._urlService.API_ENDPOINT_ROLES_LABEL + '?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url)
  }


  getUserAccessForCheck(form: any, companyId: string) {
    let obj = {
      "companyId": companyId,
      "userId": form.employeeId
    }
    let url = this._urlService.API_ENDPOINT_UserAccess + '/getUserAccess?params=' + JSON.stringify(obj);
    return this.http.get<any>(url)
  }

  adduserAccessDetails(rolesFormValues: any, formDetails: any, companyId: string) {

    let obj = {
      companyId: companyId,
      userId: formDetails.employeeId,
      items: rolesFormValues
    };
    let url = this._urlService.API_ENDPOINT_UserAccess;
    return this.http.post(url, JSON.stringify(obj), { headers: this.headers })
  }

  updateUserAccessDetails(rolesFormValues: any, formDetails: any, companyId: string) {

    let filter = {
      companyId: companyId,
      userId: formDetails.employeeId,
    };
    let updateData = {
      items: rolesFormValues
    };

    let url = this._urlService.API_ENDPOINT_UserAccess + '/update?where=' + JSON.stringify(filter);
    return this.http.post(url, updateData, { headers: this.headers });
  }

  getUserListForTPLockOpenReport(params: any): Observable<any> {
    let obj = {
      where: {},
      include: [{
        relation: 'state',
        scope: {
          fields: { "stateName": true }
        }
      },
      {
        relation: 'district',
        scope: {
          fields: {
            "districtName": true,
            "id": true
          }
        }
      }]
    }
    if (params.type == "State") {
      obj.where = {
        companyId: params.companyId,
        stateId: { inq: params.stateIds },
        status: params.status,
      };
    } else if (params.type == "Headquarter") {
      obj.where = {
        companyId: params.companyId,
        districtId: { inq: params.districtIds },
        status: params.status,
      };
    } else {
      obj.where = {
        companyId: params.companyId,
        userId: { inq: params.employeeId },
        status: params.status,
      };
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })

  }

  getUsersBirthdayAndAnniversaryDetails(obj: any): Observable<any> {
    // let url = this._urlService.API_ENDPOINT_USERINFO + '/getAllUsersBirthdaysAndAnniversaries?param=' + JSON.stringify(obj);
    let url = this._urlService.API_ENDPOINT_PROVIDERS + '/NotificationCount?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });
  }


  getUserName(whereObject: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USERLOGINS + '?filter=' + JSON.stringify(whereObject);
    return this.http.get<UserLogin>(url)
  }

  getUserCode(params: any): Observable<any> {
    let obj = {
      where: {
        companyId: params.companyId,
        userId: params.id
      },
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(obj);
    return this.http.get<UserLogin>(url)
  }

  getEmployeesOfCompaney(companyId:any, designation:any){
    let filter = {
      where:{
        "companyId":companyId,
        "designationLevel":designation.designationLevel
      }
    }
    const url = this._urlService.API_ENDPOINT_USERLOGINS + '?filter=' + JSON.stringify(filter);
    return this.http.get<UserLogin>(url);
  }
// mahender -------------------------- mahender ------
  getuserList(obj){
    let filter = {
      where:{
        "companyId":obj.companyId,
        "designationLevel":obj.designationId,
        "stateId":{inq:obj.stateId}
      }
    }
    console.log('filter',filter)
    const url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get<UserLogin>(url);
  }
// mahender -------------------------- End -mahender ------

}

