import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class TargetService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private urlService: URLService) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();

  getGroup(companyId: string): Observable<any> {
    var obj;
      obj = {
        "where": {
          "companyId": companyId,
          "status":true
        },
        "order": "productName ASC",
      }
    let url = this.urlService.API_ENDPOINT + '/GroupMasters?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getProductBasedOnStateForTargetModule(companyId: string, stateId: any,groupName:string,groupSelectedOrNot:number): Observable<any> {
    var obj;
    if(groupSelectedOrNot==1){
      obj = {
        "where": {
          "companyId": companyId,
          "status":"true",
          "assignedStates": {
            "inq": stateId
          },
          "groupLinked": groupName
        },
        "order": "productName ASC",
        //"limit" : 50
      }
    }else{
      obj = {
        "where": {
          "companyId": companyId,
          "status":"true",
          "assignedStates": {
            "inq": stateId
          }
        },
        "order": "productName ASC",
        //"limit" : 50
      }
    }
    let url = this.urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  createTarget(companyId: string, formInfo: any, targetInfo: any,userLoginId:string): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "formInfo": formInfo,
      "targetInfo": targetInfo,
      "userLoginId":userLoginId
    }
    console.log("obj-------------------------->",obj)
    let url = this.urlService.API_ENDPOINT_TARGET + '/createTarget?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

   getTarget(companyId: string, formInfo: any): Observable<any> {
    var obj;
    //formInfo["year"]=2022;
    obj = {
      "companyId": companyId,
      "formInfo": formInfo
    }
    let url = this.urlService.API_ENDPOINT_TARGET + '/getTarget?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getTargetSubmittedDetail(companyId: string, formInfo: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "formInfo": formInfo
    }
    let url = this.urlService.API_ENDPOINT_TARGET + '/getTargetSubmittedDetail?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  modifyTarget(companyId: string, formInfo: any,resultValue:any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "formInfo": formInfo,
      "resultValue":resultValue
    }
    let url = this.urlService.API_ENDPOINT_TARGET + '/modifyTarget';
    return this.http.post(url,obj, { headers: this.headers })
  }

  getTargetFinancialYearWise(object):Observable<any>{
    let url = this.urlService.API_ENDPOINT_TARGET + '/getTargetFinancialYearWise?param=' + JSON.stringify(object);  
    return this.http.get<any>(url, { headers: this.headers});
  }

}
