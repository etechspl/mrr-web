import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { Division } from '../../../content/pages/_core/models/division';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DivisionService {
  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }
  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  getDivisions(object: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_DivisionMaster + "/getDivisions?params=" + JSON.stringify(object);
       return this.http.get(url)
  }
  getDivisionWithMappedState(object: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_DivisionMaster+"/getDivisionWithMappedState?params="+JSON.stringify(object);
      return this.http.get(url)
  }


  getUserDivision(Object: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_DivisionMaster + "/getUserDivision?params=" + JSON.stringify(Object);
    return this.http.get<any>(url)
  }
  getDivisionIdOnNameBasis(Object: any): Observable<any> {
      let url = this._urlService.API_ENDPOINT_DivisionMaster + "/getDivisions?params=" + JSON.stringify(Object);
     return this.http.get<any>(url)
  }
  createDivision(Object: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_DivisionMaster;
     return this.http.post(url, Object, { headers: this.headers });
  }


  //-----------------PK 26-12-2019----------------------------------
  getDivisionNew(whereObject: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_DivisionMaster + "?filter=" + JSON.stringify(whereObject);
    return this.http.get<any>(url)
  }

  
  updateStatus(where: any, data: any): Observable<any>{
    let url = this._urlService.API_ENDPOINT_DivisionMaster+"/update?where=" + JSON.stringify(where);
    return this.http.post(url,data,  { headers: this.headers });
  }
  //----------------------------END----------------------------
}
