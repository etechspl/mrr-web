import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';
import { URLService } from './URL/url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class STPService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  createSTP(data: any) {
    let url = this._urlService.API_ENDPOINT_STP;
    return this.http.post(url, JSON.stringify(data), { headers: this.headers });
  }

  getSTPDetails(obj: object): Observable<any> {
    let filter = {
      where: {
        //status: true,
        companyId: obj['companyId'],
        userId: obj['userId'],
        status: true
      }, "include": [
        {
          "relation": "fromArea",
        },
        {
          "relation": "toArea"
        },
        {
          relation: 'userInfo',
          scope: {
            fields: {
              "name": true,
              "id": true,
              "designationLevel": true
            }
          }
        },
        // {
        //   relation: 'FareRateCard',
        //   scope: {
        //     fields: {
        //       "frcCode": true
        //     }
        //   }
        // }
      ],order: 'fromArea.areaName ASC'
    }

    let url = this._urlService.API_ENDPOINT_STP + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers });
  }
  getUserInfo(obj: object): Observable<any> {
    let filter = {
      where: {
        companyId: obj['companyId'],
        userId: obj['userId'],
        status: true
      }
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers });
  }

  getSTPApprovalReq(data: object) {
    let filter = {
      where: {
        companyId: data['companyId'],
        userId: data['userId'],
        appBymgr: ''
      }
    }
    let url = this._urlService.API_ENDPOINT_STP + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers });
  }

  updateSTP(params: Object) {
    let obj = {
      id: params["id"]
    }
    delete params["id"];
    let url = this._urlService.API_ENDPOINT_STP + '/update?where=' + JSON.stringify(obj);
    return this.http.post(url, JSON.stringify(params), { headers: this.headers });
  }

  checkSTPExisit(obj: object) {

    let filter = {
      where: {
        // "companyId": obj['companyId'],
        // "userId": obj['userId'],
        "or": [{
          "fromArea": obj["fromArea"],
          "toArea": obj["toArea"]
        }, {
          "toArea": obj["fromArea"],
          "fromArea": obj["toArea"]
        }]
      }
    }

    let url = this._urlService.API_ENDPOINT_STP + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers });
  }
// -----------------nipun (19-02-2020)-----
editSTPApproval(params): Observable<any>{
  let data="Data"
 return this.http.post(this._urlService.API_ENDPOINT_STP+'/editSTPApproval?params='+JSON.stringify(params), { headers: this.headers });

}
// -----------------nipun (19-02-2020)----- 

getSTPMigrateDetails(obj: object): Observable<any> {
  let filter = {
    where: {
      //status: true,
      companyId: obj['companyId'],
      userId: obj['userId'],
      status: true
    }, "include": [
      {
        "relation": "fromArea"
      },
      {
        "relation": "toArea"
      }
    ]
  }
  let url = this._urlService.API_ENDPOINT_STP + '?filter=' + JSON.stringify(filter);
  
  return this.http.get(url, { headers: this.headers });
}


migrateSTP(obj: object): Observable<any> {

    let url = this._urlService.API_ENDPOINT_STP + '/migrateSTPDetails?params=' + JSON.stringify(obj);
   return this.http.get(url, { headers: this.headers });
}

transferStp(obj: object): Observable<any> {
  let filter = {
    companyId: obj['companyId'],
     userId:obj['emloyeeId'],
    _id : { inq: obj['id']},
  };
    
  let updateData={
    userId:obj['emloyeeIdTo'],
    distance:obj['distance'],
    type : obj['type'],
    updatedAt:new Date()
  }
  let url = this._urlService.API_ENDPOINT_STP + '/update?where=' + JSON.stringify(filter);
  return this.http.post(url, updateData, { headers: this.headers });
}

createSTPs(obj: object): Observable<any>{
  let url = this._urlService.API_ENDPOINT_STP;
  return this.http.post(url, JSON.stringify(obj), { headers: this.headers });
}

}
