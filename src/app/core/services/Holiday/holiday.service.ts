import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HolidayService {

  constructor(private _urlService: URLService,
    private http: HttpClient,
    private _authservice: AuthenticationService) { }
    headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': this._authservice.getAccessTokenNew(),
  
    });
  
    acessToken = "&access_token=" + this._authservice.getAccessTokenNew();

    /** Created By Rahul */
  
    // createHoliday(holydayForm: Object, companyId: string) {
    //   holydayForm['companyId'] = companyId;
    //   holydayForm['createdAt'] = new Date();
    //   holydayForm['updatedAt'] = new Date();
  
    //   let url = this._urlService.API_ENDPOINT_Holiday + '/createHolyday?params=' + JSON.stringify(holydayForm);
    //   console.log("url : "+url);
    //   return this.http.get(url, { headers: this.headers })
    // }
    // getHolydayDetail(companyId: string,holydayForm: Object): Observable<any> {
    //   let obj = {
    //     "companyId": companyId,
    //     "status": "true"
    //   }
    //   let url = this._urlService.API_ENDPOINT_Holiday + '/getHolyday?companyId=' + companyId + '&status=' + status;
    //   return this.http.get<any>(url, { headers: this.headers });
   
    // }
    /** -----------------------End---------------- */
    uploadHolidays(holidayObj:any): Observable<any> {
      let url = this._urlService.API_ENDPOINT_Holiday;
      return this.http.post(url, holidayObj, { headers: this.headers })
    }
}
