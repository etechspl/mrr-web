import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class LatLongService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();
  getAddressOnLongLatBasis(latLong: any): Observable<any> {    
    let url = this._urlService.API_ENDPOINT_Latlong + '/getLatLong?params=' + JSON.stringify(latLong);
  
    return this.http.post<any>(url, { headers: this.headers })
  }
  submitLocationInfo(longLatObj:any){
       let url = this._urlService.API_ENDPOINT_Latlong;
       return this.http.post(url,longLatObj, { headers: this.headers })
  }
  getTaggedAddress(obj: any): Observable<any> {    
    console.log("object=>",obj);
    
    let url = this._urlService.API_ENDPOINT_STP + '/taggedLocation?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }
}
