import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ObservableLike, Observable } from 'rxjs';
import { Designation } from '../../../content/pages/_core/models/designation.model';
@Injectable({
  providedIn: 'root'
})
export class DesignationService {

  constructor(
    private _urlService: URLService,
    private http: HttpClient,
    private _authservice: AuthenticationService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this._authservice.getAccessTokenNew()
  });
  getDesignations(companyId: string) {
    let url = this._urlService.API_ENDPOINT_Designation + "?filter={\"where\":{\"companyId\":\"" + companyId + "\"}}" + this._authservice.ACCESSTOKEN;
    return this.http.get(url)
  }
  getLowerLevelDesignations(companyId: string, level: number): Observable<Designation> {
    let obj = {};
    if (level == 0) {
      obj = {
        where: {
          companyId: companyId,
          designationLevel: {
            ne: level
          },
        }, order: "designationLevel ASC"
      };
    }
    else if(level == 1){
      obj = {
        where: {
          companyId: companyId,
          designationLevel: {
            between: [1, level]
          },
        }, order: "designationLevel ASC"
      };

    }
    
    else if (level > 1) {
      obj = {
        where: {
          companyId: companyId,
          designationLevel: {
            between: [1, level]
          },
        }, order: "designationLevel ASC"
      };

    }
    let url = this._urlService.API_ENDPOINT_Designation + "?filter=" + JSON.stringify(obj);
    return this.http.get<Designation>(url, { headers: this.headers })
  }

  getManagersDesignation(param: Object) {
    let filter = {
      where: {
        companyId: param['companyId'],
        designationLevel: {
          nin: [1, 0]
        }
      },
      order: "designationLevel DESC"
    }
    let url = this._urlService.API_ENDPOINT_Designation + '?filter=' + JSON.stringify(filter);
    return this.http.get<any>(url, { headers: this.headers });
  }

  //-------------------Praveen Kumar(11-04-2019)-----------------
  getUniqueDesignationOnStateOrDistrict(param: Object) {
    let paramObject = JSON.parse(JSON.stringify(param));
    let passingObject = {
      companyId: paramObject.companyId,
      lookingFor: paramObject.lookingFor,   // onState or onDistrict
      stateIds: paramObject.stateId,
      districtIds: paramObject.districtId,
      userLevel: paramObject.userLevel,
      userId: paramObject.userId
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getUniqueDesignationOnStateOrDistrict?param=' + JSON.stringify(passingObject)
    return this.http.get(url, { headers: this.headers })

  }
  //-----------------------------END-----------------------------

  //-------------------Praveen Kumar(09-08-2019)-----------------
  getDesignationBasedOnDivision(param: Object) {
    let paramObject = JSON.parse(JSON.stringify(param));
    let passingObject = {
      companyId: paramObject.companyId,
      userLevel: paramObject.userLevel,
      userId: paramObject.userId,
      division: paramObject.division
    }

    if (param.hasOwnProperty("stateId") === true) {
      passingObject["stateId"] = param["stateId"];
    }
    if (param.hasOwnProperty("districtId") === true) {
      passingObject["districtId"] = param["districtId"];
    }
    console.log(passingObject);
    
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getDesignationBasedOnDivision?param=' + JSON.stringify(passingObject)
    return this.http.get(url, { headers: this.headers })

  }

  isDesignationExist(whereObject: any) {

    let url = this._urlService.API_ENDPOINT_Designation + "?filter=" + JSON.stringify(whereObject);
    console.log("url :"); console.log(url);
    return this.http.get(url)
  }

  createDesig(obj: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_Designation;
    return this.http.post<any>(url, obj);
  }

  //-------------------Praveen Kumar(08-04-2020)-----------------
  getAllDesignationForMail(param: any) {
    let passingObject = {
      companyId: param.companyId,
      userLevel: param.userLevel,
      userId: param.userId
    }
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getAllDesignationForMail?param=' + JSON.stringify(passingObject)
    return this.http.get(url, { headers: this.headers })

  }
}
