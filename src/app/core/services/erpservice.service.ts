import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';
import { URLService } from './URL/url.service';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ERPServiceService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService) { }

    headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': this.authservice.getAccessTokenNew()
    });

    getPrimaryCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getPrimaryCountforadmin?param=' + JSON.stringify(data);
      console.log("url :11 ",data);
    return this.http.get(url, { headers: this.headers })
    }
    getCollectionCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getCollectionCount?param=' + JSON.stringify(data);
      console.log("url : 22",data);
      return this.http.get(url, { headers: this.headers })
    }
    //getOutstandingCount
    getOutstandingCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getOutstandingCount?param=' + JSON.stringify(data);
      console.log("url : 33",data);
      return this.http.get(url, { headers: this.headers })
    }
    getSalesReturnCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getSalesReturnCount?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    //done by ravi on 15-10-2020
    
    getStateWiseDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getStateWiseDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }
    getStateWiseProductDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getStateWiseProductDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }

    getHQWiseDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getHQWiseDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }
    getHQWiseProductDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getHQWiseProductDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }

    getPartyWiseDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getPartyWiseDetails?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    getPartyWiseProductDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getPartyWiseProductDetails?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    getERPStateData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getERPStateData?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }

    getERPHeadquarterData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getERPHeadquarterData?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    getDesignationsData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getDesignationData?param=' + JSON.stringify(data);
     
    return this.http.get(url, { headers: this.headers })
    }
    getManagerData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_STATE + '/getManagerData?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    getTargetVsAchievementVsPrimaryVsClosing(data : any) : Observable<any>{      
        let url = this._urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getTargetVsAchievementVsPrimaryVsClosing?params=' + JSON.stringify(data);
        return this.http.get(url, { headers: this.headers})
    }

    // getStateWiseProductDetails(data: any) :Observable<any>{
    //   let url = this._urlService.API_ENDPOINT_STATE + '/getStateWiseProductDetails?param=' + JSON.stringify(data);
    //   console.log("url : getStateWiseProductDetails ",url);
    //   return this.http.get(url, { headers: this.headers })
    // }

    // getHqWiseProductDetails(data: any) :Observable<any>{
    //   let url = this._urlService.API_ENDPOINT_STATE + '/getHqWiseProductDetails?param=' + JSON.stringify(data);
    //   console.log("url : getHqWiseProductDetails ",url);
    //   return this.http.get(url, { headers: this.headers })
    // }
    // getPartyProductWiseDetails(data: any) :Observable<any>{
    //   let url = this._urlService.API_ENDPOINT_STATE + '/getPartyProductWiseDetails?param=' + JSON.stringify(data);
    //   console.log("url getPartyProductWiseDetails : ",url);
    //   return this.http.get(url, { headers: this.headers })
    // }


    //END
}
