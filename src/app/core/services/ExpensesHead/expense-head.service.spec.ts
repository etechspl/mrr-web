import { TestBed } from '@angular/core/testing';

import { ExpenseHeadService } from './expense-head.service';

describe('ExpenseHeadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExpenseHeadService = TestBed.get(ExpenseHeadService);
    expect(service).toBeTruthy();
  });
});
