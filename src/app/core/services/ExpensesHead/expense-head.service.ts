import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
@Injectable({
  providedIn: 'root'
})
export class ExpenseHeadService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  getExpensesHead(data: any) {
    let url = this._urlService.API_ENDPOINT_EXPENSES+'?filter='+JSON.stringify(data);
    return this.http.get(url, { headers: this.headers });
  }
}





  


