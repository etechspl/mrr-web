import { TestBed } from '@angular/core/testing';

import { MefService } from './mef.service';

describe('MefService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MefService = TestBed.get(MefService);
    expect(service).toBeTruthy();
  });
});
