import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { Product } from '../../../content/pages/_core/models/productCreation';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MefService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  //serverUrl = this._urlService.API_ENDPOINT;
  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();
 
  mefCreation(formValue: any, currentUser: any, companyId: string):Observable<any>  {
    let paramArr=[];
    let solutionToHisQry:boolean;
    let haveInformed:boolean;
    let srGivenSolution:boolean;
    let quotation:boolean;
    let conversion:boolean;
    let jointWork:any;
    if(currentUser['rL']==1){
      jointWork=[];
    }else if(currentUser['rL']>1){
      jointWork=[formValue.jointWork];
    }
    if(formValue.solutionToHisQry=='yes'){
      solutionToHisQry=true;
    }else if(formValue.solutionToHisQry=='no'){
      solutionToHisQry=false;
    }
    if(formValue.haveInformed=='yes'){
      haveInformed=true;
    }else if(formValue.haveInformed=='no'){
      haveInformed=false;
    }
    if(formValue.srGivenSolution=='yes'){
      srGivenSolution=true;
    }else if(formValue.srGivenSolution=='no'){
      srGivenSolution=false;
    }
    if(formValue.quotation=='yes'){
      quotation=true;
    }else if(formValue.quotation=='no'){
      quotation=false;
    }
    if(formValue.conversion=='yes'){
      conversion=true;
    }else if(formValue.conversion=='no'){
      conversion=false;
    }

    var mef = {
      "date": new Date(formValue.mefDate),
      "companyId": companyId,
      "areaId": formValue['area'],
      "providerId": formValue['provider'],  
      "stateId": currentUser.stateId,
      "districtId": currentUser.districtId, 
      "competitorProduct": formValue.competitorProduct,
      "productDiscussed": formValue.focusProduct,
      "drProductProblem": formValue.DrFacingProduct,
      "problemSolnToDr": formValue.solutionOurProduct,
      "doctorFeedback": formValue.drFeedback,
      "isQuerySolnGiven": solutionToHisQry,
      "querySoln": formValue.ifYesSpecified,
      "seniorHelp": formValue.helpNeedFromSr,
      "isInformToSenior": haveInformed,
      "isSolnFromSenior": srGivenSolution,
      "solnGivenBySenior": formValue.solutionGiven,
      "quoteGiven": quotation,
      "quoteDate": new Date(formValue.quotationDate),
      "quoteStatus": formValue.quotationStatus,
      "quoteOthers": formValue.quotationOther,
      "submitBy": currentUser['userId'],
      "ipAddress": "",
      "jointWorkWith": jointWork,
      "conversionDone": conversion,
      "conversionNoReason": formValue.notReason,
    };    
    paramArr.push(mef);
    let url = this._urlService.API_ENDPOINT_MefTracker + '/createMef?param=' + JSON.stringify(paramArr);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getMefDetail(companyId: string,formDetail: any ): Observable<any> {    
    var obj = {
      formDetail: formDetail,
      companyId: companyId
    }
    let url = this._urlService.API_ENDPOINT_MefTracker + '/getMefDetail?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getMefDetailForEditForm(companyId: string,mefDate: any,userId:string ): Observable<any> {    
    var obj = {
      companyId: companyId,
      date: mefDate,
      submitBy:userId
    }
    let url = this._urlService.API_ENDPOINT_MefTracker + '/getMefDetailForEditForm?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  setMefDetailForEditInfo(mefId: string ): Observable<any> {    
    var obj = {
      "where": {
        id: mefId,
      }
    }
  let url = this._urlService.API_ENDPOINT + '/MefTrackers?filter=' + JSON.stringify(obj);
  return this.http.get<any>(url, { headers: this.headers })
  }

  mefModify(formValue: any, currentUser: any, companyId: string):Observable<any>  {
    let paramArrForModify=[];
    let solutionToHisQry:boolean;
    let haveInformed:boolean;
    let srGivenSolution:boolean;
    let quotation:boolean;
    let conversion:boolean;
    let jointWork:any;
    if(currentUser['rL']==1){
      jointWork=[];
    }else if(currentUser['rL']>1){
      jointWork=[formValue.jointWork];
    }
    if(formValue.solutionToHisQry=='yes'){
      solutionToHisQry=true;
    }else if(formValue.solutionToHisQry=='no'){
      solutionToHisQry=false;
    }
    if(formValue.haveInformed=='yes'){
      haveInformed=true;
    }else if(formValue.haveInformed=='no'){
      haveInformed=false;
    }
    if(formValue.srGivenSolution=='yes'){
      srGivenSolution=true;
    }else if(formValue.srGivenSolution=='no'){
      srGivenSolution=false;
    }
    if(formValue.quotation=='yes'){
      quotation=true;
    }else if(formValue.quotation=='no'){
      quotation=false;
    }
    if(formValue.conversion=='yes'){
      conversion=true;
    }else if(formValue.conversion=='no'){
      conversion=false;
    }

    var mef = {
      "id":formValue['id'],
      "date": new Date(formValue.mefDate),
      "companyId": companyId,
      "areaId": formValue['area'],
      "providerId": formValue['provider'],  
      "stateId": currentUser.stateId,
      "districtId": currentUser.districtId, 
      "competitorProduct": formValue.competitorProduct,
      "productDiscussed": formValue.focusProduct,
      "drProductProblem": formValue.DrFacingProduct,
      "problemSolnToDr": formValue.solutionOurProduct,
      "doctorFeedback": formValue.drFeedback,
      "isQuerySolnGiven": solutionToHisQry,
      "querySoln": formValue.ifYesSpecified,
      "seniorHelp": formValue.helpNeedFromSr,
      "isInformToSenior": haveInformed,
      "isSolnFromSenior": srGivenSolution,
      "solnGivenBySenior": formValue.solutionGiven,
      "quoteGiven": quotation,
      "quoteDate": new Date(formValue.quotationDate),
      "quoteStatus": formValue.quotationStatus,
      "quoteOthers": formValue.quotationOther,
      "submitBy": currentUser['userId'],
      "ipAddress": "",
      "jointWorkWith": jointWork,
      "conversionDone": conversion,
      "conversionNoReason": formValue.notReason,
    };    
    paramArrForModify.push(mef);
    let url = this._urlService.API_ENDPOINT_MefTracker + '/modifyMef?param=' + JSON.stringify(paramArrForModify);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getmefCategory(companyId: string) {
    let url = this._urlService.API_ENDPOINT_Designation + "?filter={\"where\":{\"companyId\":\"" + companyId + "\"}}" + this.authservice.ACCESSTOKEN;
    console.log("url : ");
    return this.http.get(url)
  }

}
