import { TestBed } from '@angular/core/testing';

import { InobxService } from './inobx.service';

describe('InobxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InobxService = TestBed.get(InobxService);
    expect(service).toBeTruthy();
  });
});
