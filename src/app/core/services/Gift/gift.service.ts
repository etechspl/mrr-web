import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EditGiftComponent } from '../../../content/pages/components/master/edit-gift/edit-gift.component';
@Injectable({
  providedIn: 'root'
})
export class GiftService {
  dialogData: any;
  constructor(
    private _urlService: URLService,
    private http: HttpClient,
    private _authservice: AuthenticationService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this._authservice.getAccessTokenNew(),

  });

  acessToken = "&access_token=" + this._authservice.getAccessTokenNew();

  getGiftDetail(companyId: string): Observable<any> {
    let obj = {
      "companyId": companyId,
      "status": true
    }
    let url = this._urlService.API_ENDPOINT_Gift + '/getGiftDetail?out=' + JSON.stringify(obj);
    console.log(url);
    return this.http.get<any>(url);
  }
  // ---------------createGift -----------

  createGift(GiftCreation: Object, companyId: string, id: string): Observable<any> {
    GiftCreation['companyId'] = companyId;
    GiftCreation['giftName'] = GiftCreation['giftName'].toString();
    GiftCreation['giftCode'] = GiftCreation['giftCode'].toString();
    GiftCreation['giftCost'] = GiftCreation['giftMrp'];
    GiftCreation['assignedStates'] = ["123456789"];
    GiftCreation['status'] = true;
    GiftCreation['createdBy'] = id;
    GiftCreation['createdAt'] = new Date();
    GiftCreation['updatedAt'] = new Date();

    let url = this._urlService.API_ENDPOINT_Gift + '/createGift?params=' + JSON.stringify(GiftCreation);
    return this.http.get<any>(url, { headers: this.headers })
    this.getGiftDetail(companyId);
  }

  updateIssue(issue: any): void {
    this.dialogData = issue;
  }

  editGift(editGiftVal: Object, companyId: string, _id: string): Observable<EditGiftComponent> {
    editGiftVal['companyId'] = companyId;
    editGiftVal['giftName'] = editGiftVal['giftName'].toString();
    editGiftVal['giftCode'] = editGiftVal['giftCode'].toString();
    editGiftVal['giftCost'] = editGiftVal['giftCost'];
    editGiftVal['updatedBy'] = _id;
    editGiftVal['updatedAt'] = new Date();
    let url = this._urlService.API_ENDPOINT_Gift + '/editGift?params=' + JSON.stringify(editGiftVal);
    return this.http.get<EditGiftComponent>(url);
  }

  deleteGift(delDetails: Object, companyId: string, _id: string): Observable<any> {
    delDetails['companyId'] = companyId;
    delDetails['giftName'] = delDetails['giftName'].toString();
    delDetails['giftCode'] = delDetails['giftCode'].toString();
    delDetails['giftCost'] = delDetails['giftCost'];
    delDetails['updatedBy'] = _id;
    delDetails['updatedAt'] = new Date();
    let url = this._urlService.API_ENDPOINT_Gift + '/deleteGift?params=' + JSON.stringify(delDetails);
    return this.http.get<any>(url);
  }

  getGiftBasedOnState(companyId: string, stateId: any,groupName:string,groupSelectedOrNot:number): Observable<any> {
    var obj;
    if(groupSelectedOrNot==1){
      obj = {
        "where": {
          "companyId": companyId,
         // "assignedStates": {
           // "inq": stateId
         // },
          status:true,
          //"groupLinked": groupName
        },
        "order": "giftName ASC",
        //"limit" : 50
      }
    }else{
      obj = {
        "where": {
          "companyId": companyId,
          //"assignedStates": {
          //  "inq": stateId
          //},
          status:true
        },
        "order": "giftName ASC",
        //"limit" : 50
      }
    }
    let url = this._urlService.API_ENDPOINT + '/Gifts?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

}
