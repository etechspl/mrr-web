import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class URLService {
  API_ENDPOINT: string;
  API_GEOCODE: string;
  ERP_API_ENDPOINT: string;
  API_ENDPOINT_STATE: string;
  API_ENDPOINT_TOURPROGRAM: string;
  API_ENDPOINT_Designation: string;
  API_ENDPOINT_DCRMASTER: string;
  API_ENDPOINT_DCRMASTERBACKUP: string;
  API_ENDPOINT_USERINFO: string;
  API_ENDPOINT_DISTRICTS: string;
  API_ENDPOINT_USERLOGINS: string;
  API_ENDPOINT_AREAS: string;
  API_ENDPOINT_USER_AREA_MAPPINGS: string;
  API_ENDPOINT_DCRProviderVisitDetails: string;
  API_ENDPOINT_DCRProviderVisitBackup: string;
  API_ENDPOINT_HIERARCHY: string;
  API_ENDPOINT_PROVIDERS: string;
  API_ENDPOINT_UserStockiestMapping: string;
  API_ENDPOINT_PrimaryAndSaleReturn: string;
  API_ENDPOINT_FARERATECARD: string;
  API_ENDPOINT_DivisionMaster: string;
  API_ENDPOINT_Products: string;
  API_ENDPOINT_Gift: string;
  API_ENDPOINT_DAILY_ALLOWANCES: string;
  API_ENDPOINT_STP: string;
  API_ENDPOINT_Activity: string;
  API_ENDPOINT_EXPENSE_CLAIMED: string;
  API_ENDPOINT_TARGET: string;
  API_ENDPOINT_CONTAINER: string;
  API_ENDPOINT_DataTransferHistory: string;
  API_ENDPOINT_Samples: string;
  API_ENDPOINT_INBOX: string;
  API_ENDPOINT_SampleGiftIssueDetail: string;
  API_ENDPOINT_GroupMaster: string;
  API_ENDPOINT_MefTracker: string;
  API_ENDPOINT_Gis: string;
  API_ENDPOINT_Latlong: string;
  API_ENDPOINT_UserAccess: string;
  API_ENDPOINT_BroadcastMessages: string;
  API_ENDPOINT_Holiday: string;
  API_ENDPOINT_FileLiberary: string;
  API_ENDPOINT_FIX_EXPENSE: string;
  API_ENDPOINT_TrackLockOpenDetail: string;
  API_ENDPOINT_BlockUser: string;
  API_ENDPOINT_EXPENSES: string;
  API_ENDPOINT_ProductGroups: string;
  API_ENDPOINT_Assignproducttogroup: string;
  API_ENDPOINT_ProductSubGroup: string;
  //------------ERP--------------------------
  ERP_API_ENDPOINT_STATE: string;
  ERP_API_ENDPOINT_HEADQUARTER: string;
  ERP_API_ENDPOINT_MANAGERLIST: string;
  ERP_API_ENDPOINT_MANAGER_HEADQUARTER: string;
  ERP_API_ENDPOINT_TOTAL_PRIMARY_COUNT: string;
  ERP_API_ENDPOINT_TOTAL_SALE_RETURN_COUNT: string;
  ERP_API_ENDPOINT_TOTAL_OUTSTANDING_COUNT: string;
  ERP_API_ENDPOINT_TOTAL_COLLECTION_COUNT: string;
  ERP_API_ENDPOINT_STATE_WISE_PRIMARY_DATA: string;
  ERP_API_ENDPOINT_HQ_WISE_PRIMARY_DATA: string;
  ERP_API_ENDPOINT_PARTY_WISE_PRIMARY_DATA: string;
  ERP_API_ENDPOINT_STATE_WISE_OUTSTANDING_DATA: string;
  ERP_API_ENDPOINT_HQ_WISE_OUTSTANDING_DATA: string;
  ERP_API_ENDPOINT_PARTY_WISE_OUTSTANDING_DATA: string;
  ERP_API_ENDPOINT_STATE_WISE_COLLECTION_DATA: string;
  ERP_API_ENDPOINT_HQ_WISE_COLLECTION_DATA: string;
  ERP_API_ENDPOINT_PARTY_WISE_COLLECTION_DATA: string;
  ERP_API_ENDPOINT_STATE_WISE_SALE_RETURN_DATA: string;
  ERP_API_ENDPOINT_HQ_WISE_SALE_RETURN_DATA: string;
  ERP_API_ENDPOINT_PARTY_WISE_SALE_RETURN_DATA: string;
  ERP_API_ENDPOINT_LEDGER_WISE_OUTSTANDING_DATA: string;
  API_ENDPOINT_PMTFEEDBACK: string;
  API_ENDPOINT_COMPANYMASTER: string;
  API_ENDPOINT_COMPANYCREATIONDETAILS: string;
  API_ENDPOINT_DOCTORVENDORLINKING: string;
  API_ENDPOINT_Category: string;
  API_ENDPOINT_leaveApproval: string;
  API_ENDPOINT_LABELS: string;
  API_ENDPOINT_SENTITEM: string;
  API_ENDPOINT_DRAFTMAIL: string;
  API_ENDPOINT_ROLES_LABEL: string;
  API_ENDPOINT_DOCTOR_CHEMIST_LINKING: string;
  API_ENDPOINT_DOCTOR_CHEMIST_SALE: String;
  API_ENDPOINT_CRM_SPONSORSHIP: string;
  API_ENDPOINT_MGRInvestment: string;
  API_ENDPOINT_MGRInvestmentVisitDetails: string;
  API_ENDPOINT_PatchMasters: string;
  API_ENDPOINT_ERP: string;
  API_ENDPOINT_DEVICES :string;
  API_ENDPOINT_CHANNELFORMS:string;
  constructor() {


  //this.ERP_API_ENDPOINT = 'http://10.0.4.61/PharmaMRApi/api/';
 this.API_ENDPOINT = 'https://mrreportingapi.espldemo.com/api';
  // this.API_ENDPOINT = 'http://localhost:3000/api';
  // this.API_ENDPOINT = 'http://localhost:3001/api'; //Gourav
  
    
    
    
    this.API_GEOCODE = 'https://maps.googleapis.com/maps/api/geocode/';
    this.API_ENDPOINT_STATE = this.API_ENDPOINT + "/States";
    this.API_ENDPOINT_TOURPROGRAM = this.API_ENDPOINT + "/TourPrograms";
    this.API_ENDPOINT_DCRMASTER = this.API_ENDPOINT + "/DCRMasters";
    this.API_ENDPOINT_USERINFO = this.API_ENDPOINT + "/UserInfos";
    this.API_ENDPOINT_Designation = this.API_ENDPOINT + "/Designations";
    this.API_ENDPOINT_DISTRICTS = this.API_ENDPOINT + "/Districts";
    this.API_ENDPOINT_USERLOGINS = this.API_ENDPOINT + "/UserLogins";
    this.API_ENDPOINT_AREAS = this.API_ENDPOINT + '/Areas';
    this.API_ENDPOINT_USER_AREA_MAPPINGS = this.API_ENDPOINT + '/UserAreaMappings'
    this.API_ENDPOINT_DCRProviderVisitDetails = this.API_ENDPOINT + "/DCRProviderVisitDetails";
    this.API_ENDPOINT_HIERARCHY = this.API_ENDPOINT + '/Hierarchies';
    this.API_ENDPOINT_PROVIDERS = this.API_ENDPOINT + '/Providers';
    this.API_ENDPOINT_DivisionMaster = this.API_ENDPOINT + '/DivisionMasters';
    this.API_ENDPOINT_UserStockiestMapping = this.API_ENDPOINT + '/UserStockiestMappings';
    this.API_ENDPOINT_PrimaryAndSaleReturn = this.API_ENDPOINT + '/PrimaryAndSalereturns';
    this.API_ENDPOINT_Products = this.API_ENDPOINT + '/Products';
    this.API_ENDPOINT_Gift = this.API_ENDPOINT + '/Gifts';
    this.API_ENDPOINT_FARERATECARD = this.API_ENDPOINT + '/FareRateCards';
    this.API_ENDPOINT_DAILY_ALLOWANCES = this.API_ENDPOINT + '/DailyAllowances';
    this.API_ENDPOINT_STP = this.API_ENDPOINT + '/STPs';
    this.API_ENDPOINT_Activity = this.API_ENDPOINT + '/Activities';
    this.API_ENDPOINT_EXPENSE_CLAIMED = this.API_ENDPOINT + '/ExpenseClaimeds'
    this.API_ENDPOINT_TARGET = this.API_ENDPOINT + '/Targets';
    this.API_ENDPOINT_CONTAINER = this.API_ENDPOINT + '/containers';
    this.API_ENDPOINT_DataTransferHistory = this.API_ENDPOINT + '/DataTransferHistories';
    this.API_ENDPOINT_Samples = this.API_ENDPOINT + '/sampleissues';
    this.API_ENDPOINT_INBOX = this.API_ENDPOINT + '/Inboxes';
    this.API_ENDPOINT_GroupMaster = this.API_ENDPOINT + '/GroupMasters';
    this.API_ENDPOINT_SampleGiftIssueDetail = this.API_ENDPOINT + '/SampleGiftIssueDetails';
    this.API_ENDPOINT_MefTracker = this.API_ENDPOINT + '/MefTrackers';
    this.API_ENDPOINT_Gis = this.API_ENDPOINT + '/gis';
    this.API_ENDPOINT_Latlong = this.API_ENDPOINT + '/longLatInfos';
    this.API_ENDPOINT_UserAccess = this.API_ENDPOINT + '/UserAccesses';
    this.API_ENDPOINT_Holiday = this.API_ENDPOINT + '/Holidays';
    this.API_ENDPOINT_BroadcastMessages = this.API_ENDPOINT + '/BroadcastMessages';
    this.API_ENDPOINT_FileLiberary = this.API_ENDPOINT + '/FileLibraries';
    this.API_ENDPOINT_FIX_EXPENSE = this.API_ENDPOINT + '/FixExpenses';
    this.API_ENDPOINT_DCRMASTERBACKUP = this.API_ENDPOINT + '/DCRMasterBackups';
    this.API_ENDPOINT_DCRProviderVisitBackup = this.API_ENDPOINT + '/DCRProviderVisitDetailsBackups';
    this.API_ENDPOINT_TrackLockOpenDetail = this.API_ENDPOINT + '/TrackLockOpenDetails';
    this.API_ENDPOINT_BlockUser = this.API_ENDPOINT + '/BlockUsers';
    this.API_ENDPOINT_EXPENSES = this.API_ENDPOINT + '/Expenses';
    this.API_ENDPOINT_PMTFEEDBACK = this.API_ENDPOINT + '/PMTFeedbacks';
    this.API_ENDPOINT_Category = this.API_ENDPOINT + '/Categories';
    this.API_ENDPOINT_DOCTORVENDORLINKING = this.API_ENDPOINT + '/DoctorVendorLinkings';
    this.API_ENDPOINT_leaveApproval = this.API_ENDPOINT + '/LeaveApprovals';
    this.API_ENDPOINT_LABELS = this.API_ENDPOINT + '/Labels'
    this.API_ENDPOINT_SENTITEM = this.API_ENDPOINT + '/SentItems';
    this.API_ENDPOINT_DRAFTMAIL = this.API_ENDPOINT + '/Drafts';
    this.API_ENDPOINT_CRM_SPONSORSHIP = this.API_ENDPOINT + '/CrmSponsorships';
    this.API_ENDPOINT_ROLES_LABEL = this.API_ENDPOINT + '/RolesLabels';
    this.API_ENDPOINT_MGRInvestment = this.API_ENDPOINT + '/Mgrinvestmentmasters';
    this.API_ENDPOINT_MGRInvestmentVisitDetails = this.API_ENDPOINT + '/Mgrinvestmentvisitdetails';
    this.API_ENDPOINT_ProductGroups = this.API_ENDPOINT + '/ProductGroups';
    this.API_ENDPOINT_PatchMasters = this.API_ENDPOINT + '/PatchMasters';
    this.API_ENDPOINT_DEVICES = this.API_ENDPOINT + '/DeviceInfos';

    this.API_ENDPOINT_Assignproducttogroup = this.API_ENDPOINT + '/Assignproducttogroups';
    this.API_ENDPOINT_ERP = this.API_ENDPOINT + '/ERPs';
    this.API_ENDPOINT_ProductSubGroup = this.API_ENDPOINT + '/Productsubgroups';
    //------------------------ERP API's Endpoint---------------------

    this.ERP_API_ENDPOINT_STATE = this.ERP_API_ENDPOINT + 'StateDropDown';
    this.ERP_API_ENDPOINT_HEADQUARTER = this.ERP_API_ENDPOINT + 'StateHQDropDown';
    this.ERP_API_ENDPOINT_MANAGERLIST = this.ERP_API_ENDPOINT + 'ManagerDropDown';
    this.ERP_API_ENDPOINT_MANAGER_HEADQUARTER = this.ERP_API_ENDPOINT + 'ManagerHQDropDown';
    this.ERP_API_ENDPOINT_TOTAL_PRIMARY_COUNT = this.ERP_API_ENDPOINT + 'TotalPrimaryCount';
    this.ERP_API_ENDPOINT_TOTAL_SALE_RETURN_COUNT = this.ERP_API_ENDPOINT + 'TotalSalesReturnCount';
    this.ERP_API_ENDPOINT_TOTAL_OUTSTANDING_COUNT = this.ERP_API_ENDPOINT + 'TotalOutStandingCount';
    this.ERP_API_ENDPOINT_TOTAL_COLLECTION_COUNT = this.ERP_API_ENDPOINT + 'TotalCollectionCount';
    this.ERP_API_ENDPOINT_STATE_WISE_PRIMARY_DATA = this.ERP_API_ENDPOINT + 'StateWisePrimaryData';
    this.ERP_API_ENDPOINT_HQ_WISE_PRIMARY_DATA = this.ERP_API_ENDPOINT + 'HeadQuarterWisePrimaryData';
    this.ERP_API_ENDPOINT_PARTY_WISE_PRIMARY_DATA = this.ERP_API_ENDPOINT + 'PartyWisePrimaryData';
    this.ERP_API_ENDPOINT_STATE_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'StateWiseOutStandingData';
    this.ERP_API_ENDPOINT_HQ_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'HeadQuarterWiseOutStandingData';
    this.ERP_API_ENDPOINT_PARTY_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'PartyWiseOutStandingData';
    this.ERP_API_ENDPOINT_STATE_WISE_COLLECTION_DATA = this.ERP_API_ENDPOINT + 'StateWiseCollectionData';
    this.ERP_API_ENDPOINT_HQ_WISE_COLLECTION_DATA = this.ERP_API_ENDPOINT + 'HeadquarterWiseCollectionData';
    this.ERP_API_ENDPOINT_PARTY_WISE_COLLECTION_DATA = this.ERP_API_ENDPOINT + 'PartyWiseCollectionData';
    this.ERP_API_ENDPOINT_STATE_WISE_SALE_RETURN_DATA = this.ERP_API_ENDPOINT + 'StateWiseSalesReturnData';
    this.ERP_API_ENDPOINT_HQ_WISE_SALE_RETURN_DATA = this.ERP_API_ENDPOINT + 'HeadquarterWiseSalesReturnData';
    this.ERP_API_ENDPOINT_PARTY_WISE_SALE_RETURN_DATA = this.ERP_API_ENDPOINT + 'PartyWiseSalesReturnData';
    this.ERP_API_ENDPOINT_LEDGER_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'LedgerWiseOutStandingData';
    this.API_ENDPOINT_Category = this.API_ENDPOINT + '/Categories';
    this.API_ENDPOINT_LABELS = this.API_ENDPOINT + '/LabelsMasters';
    this.API_ENDPOINT_DOCTOR_CHEMIST_LINKING = this.API_ENDPOINT + '/DoctorChemists';
    this.API_ENDPOINT_DOCTOR_CHEMIST_LINKING = this.API_ENDPOINT + '/DoctorChemists';
    this.API_ENDPOINT_DOCTOR_CHEMIST_SALE = this.API_ENDPOINT + '/DoctorChemistSales';
    this.API_ENDPOINT_CHANNELFORMS=this.API_ENDPOINT + '/ChannelForms';

    //------------------------ERP API's Endpoint---------------------

    this.ERP_API_ENDPOINT_STATE = this.ERP_API_ENDPOINT + 'StateDropDown';
    this.ERP_API_ENDPOINT_HEADQUARTER = this.ERP_API_ENDPOINT + 'StateHQDropDown';
    this.ERP_API_ENDPOINT_MANAGERLIST = this.ERP_API_ENDPOINT + 'ManagerDropDown';
    this.ERP_API_ENDPOINT_MANAGER_HEADQUARTER = this.ERP_API_ENDPOINT + 'ManagerHQDropDown';
    this.ERP_API_ENDPOINT_TOTAL_PRIMARY_COUNT = this.ERP_API_ENDPOINT + 'TotalPrimaryCount';
    this.ERP_API_ENDPOINT_TOTAL_SALE_RETURN_COUNT = this.ERP_API_ENDPOINT + 'TotalSalesReturnCount';
    this.ERP_API_ENDPOINT_TOTAL_OUTSTANDING_COUNT = this.ERP_API_ENDPOINT + 'TotalOutStandingCount';
    this.ERP_API_ENDPOINT_TOTAL_COLLECTION_COUNT = this.ERP_API_ENDPOINT + 'TotalCollectionCount';
    this.ERP_API_ENDPOINT_STATE_WISE_PRIMARY_DATA = this.ERP_API_ENDPOINT + 'StateWisePrimaryData';
    this.ERP_API_ENDPOINT_HQ_WISE_PRIMARY_DATA = this.ERP_API_ENDPOINT + 'HeadQuarterWisePrimaryData';
    this.ERP_API_ENDPOINT_PARTY_WISE_PRIMARY_DATA = this.ERP_API_ENDPOINT + 'PartyWisePrimaryData';
    this.ERP_API_ENDPOINT_STATE_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'StateWiseOutStandingData';
    this.ERP_API_ENDPOINT_HQ_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'HeadQuarterWiseOutStandingData';
    this.ERP_API_ENDPOINT_PARTY_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'PartyWiseOutStandingData';
    this.ERP_API_ENDPOINT_STATE_WISE_COLLECTION_DATA = this.ERP_API_ENDPOINT + 'StateWiseCollectionData';
    this.ERP_API_ENDPOINT_HQ_WISE_COLLECTION_DATA = this.ERP_API_ENDPOINT + 'HeadquarterWiseCollectionData';
    this.ERP_API_ENDPOINT_PARTY_WISE_COLLECTION_DATA = this.ERP_API_ENDPOINT + 'PartyWiseCollectionData';
    this.ERP_API_ENDPOINT_STATE_WISE_SALE_RETURN_DATA = this.ERP_API_ENDPOINT + 'StateWiseSalesReturnData';
    this.ERP_API_ENDPOINT_HQ_WISE_SALE_RETURN_DATA = this.ERP_API_ENDPOINT + 'HeadquarterWiseSalesReturnData';
    this.ERP_API_ENDPOINT_PARTY_WISE_SALE_RETURN_DATA = this.ERP_API_ENDPOINT + 'PartyWiseSalesReturnData';
    this.ERP_API_ENDPOINT_LEDGER_WISE_OUTSTANDING_DATA = this.ERP_API_ENDPOINT + 'LedgerWiseOutStandingData';

  }
}
