import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { Observable } from 'rxjs';
import { state } from '@angular/animations';
import { Designation } from '../../../content/pages/_core/models/designation.model';

@Injectable({
  providedIn: 'root'
})
export class SamplesIssueService {

  constructor(private _urlService: URLService,
    private http: HttpClient,
    private _authservice: AuthenticationService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this._authservice.getAccessTokenNew()

  });
  acessToken = "&access_token=" + this._authservice.getAccessTokenNew();

  //Insetion Sample Issued
  createSampleIssue(sampleDetails: Object, sampleDetails1: Object, companyId: string, id: string): Observable<any> {
    var obj = {
      form1: sampleDetails,
      form2: sampleDetails1,
    }
    console.log(obj);
    let url = this._urlService.API_ENDPOINT_Samples + '/sampleInsert?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }
  //Insetion Gift Issued
  createGiftIssue(sampleDetails: Object, sampleDetails1: Object, companyId: string, id: string): Observable<any> {
    var obj = {
      form1: sampleDetails,
      form2: sampleDetails1
    }
    let url = this._urlService.API_ENDPOINT_Samples + '/giftInsert?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }



}
