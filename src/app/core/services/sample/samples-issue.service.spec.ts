import { TestBed } from '@angular/core/testing';

import { SamplesIssueService } from './samples-issue.service';

describe('SamplesIssueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SamplesIssueService = TestBed.get(SamplesIssueService);
    expect(service).toBeTruthy();
  });
});
