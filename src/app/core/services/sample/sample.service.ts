import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { Observable } from 'rxjs';
import { state } from '@angular/animations';
import { Designation } from '../../../content/pages/_core/models/designation.model';

@Injectable({
  providedIn: 'root'
})
export class SampleService {

  constructor(private _urlService: URLService,
    private http: HttpClient,
    private _authservice: AuthenticationService
    ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this._authservice.getAccessTokenNew()

  });
  acessToken = "&access_token=" + this._authservice.getAccessTokenNew();

  //Insetion Sample Issued
  sampleIssueDetail(formDetail: Object, sampleIssueDetail: Object, companyId: string, id: string): Observable<any> {
    var obj = {
      formDetail: formDetail,
      sampleIssueDetail: sampleIssueDetail,
      companyId: companyId,
      submitBy: id
    }
    let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail + '/sampleIssue';
    return this.http.post<any>(url,obj,{ headers: this.headers })
  }

  getSampleGiftIssueDetail(companyId: string,formDetail: any,rL:number ): Observable<any> {    
    var obj = {
      formDetail: formDetail,
      companyId: companyId,
    
      rL:rL
    }
    let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail + '/getSampleGiftIssueDetail?params=' + JSON.stringify(obj);
    console.log("url DATA=>",url);
    return this.http.get<any>(url, { headers: this.headers })
  }
  
  getSampleGiftIssueDetailForEdit(companyId: string,formDetail: any,rL:number ): Observable<any> {    
    var obj = {
      formDetail: formDetail,
      companyId: companyId,
      rL:rL
    }
    let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail + '/getSampleGiftIssueDetailForEdit?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  modifySampleIssueDetail(formDetail: Object, sampleIssueDetail: Object, companyId: string): Observable<any> {
    var obj = {
      formDetail: formDetail,
      modifyIssueDetail: sampleIssueDetail,
      companyId: companyId,
    }
  
    let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail + '/modifySampleIssueDetail?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getGiftDetailsCompanyWise(Object:any){

    let url = this._urlService.API_ENDPOINT_Gift + '?filter=' + JSON.stringify(Object);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getProductDetailsCompanyWise(Object:any){

    let url = this._urlService.API_ENDPOINT_Products + '?filter=' + JSON.stringify(Object);
    return this.http.get<any>(url, { headers: this.headers })
  }

  
  giftIssueReportDoctorWise(Object:any){

console.log("Object+++++++",Object);

    let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail +'/giftIssueReportDoctorWise?params=' + JSON.stringify(Object);
    return this.http.get<any>(url, { headers: this.headers })


  }

  sampleIssueReportDoctorWise(Object:any){

    console.log("Object+++++++",Object);
    
        let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail +'/giftIssueReportDoctorWise?params=' + JSON.stringify(Object);
        return this.http.get<any>(url, { headers: this.headers })
    
    
      }


  sampleIssueReportSampleWise(Object:any){

    console.log("Object+++++++",Object);
    
        let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail +'/sampleIssueReportDoctorWise?params=' + JSON.stringify(Object);
        return this.http.get<any>(url, { headers: this.headers })
      }

  getRIBOfGiftIssue(Object:any){

    let url = this._urlService.API_ENDPOINT_SampleGiftIssueDetail +'/getRIBOfGiftIssue?params=' + JSON.stringify(Object);
    return this.http.get<any>(url, { headers: this.headers })

  }
}

