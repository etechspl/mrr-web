import { TestBed } from '@angular/core/testing';

import { TourProgramService } from './tour-program.service';

describe('TourProgramService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TourProgramService = TestBed.get(TourProgramService);
    expect(service).toBeTruthy();
  });
});
