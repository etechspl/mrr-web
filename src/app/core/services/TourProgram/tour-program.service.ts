import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { Observable } from 'rxjs';
import { TourProgram } from '../../../content/pages/_core/models/tour-program.model';
import { ConsoleLogPipe } from '../../pipes/console-log.pipe';

@Injectable({
  providedIn: 'root'
})
export class TourProgramService {

  constructor(private _urlService: URLService,
    private http: HttpClient,
    private authservice: AuthenticationService
  ) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });


  viewTourProgram(companyId: string, userId: string, month: number, year: number): Observable<TourProgram> {
    let obj = {
      companyId: companyId,
      userId: userId,
      month: month,
      year: year
  }

    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + "/getTPDetail?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    return this.http.get<TourProgram>(url);
  }

  getTotalSbmiitedAndApprovedTPStatus(month: number, year: number, companyId: string, status: any, level: number, userId: string) {
    let obj = {
      month: month,
      year: year,
      companyId: companyId,
      status: status,
      level: level,
      userId: userId
    }
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + '/usersTPStatusAPI?params=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });
  }

  getTPStatus(companyId: string, formObj: object, isDivisionExist: boolean, divisionIds: any): Observable<any> {

    let formValue = JSON.parse(JSON.stringify(formObj));
console.log("-=-=-=-=-=-=-=-=-==-",formValue)
    let obj = {
      "companyId": companyId,
      "stateIds": formValue.stateInfo,
      "districtIds": formValue.districtInfo,
      "status": formValue.status,
      "userIds": formValue.employeeIds,
      "type": formValue.type,
      "month": formValue.month,
      "year": formValue.year,
      "isDivisionExist": isDivisionExist, 
      "division": divisionIds,
      "submittedTP":formValue.submittedTP

    }
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + "/getTPStatus?params=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    return this.http.get(url,{ headers: this.headers });
  }

  updateTPStatus(userId, status, month, year, message) {
    let currentUser = JSON.parse(sessionStorage.currentUser);

    let whereObj = {
      companyId: currentUser.companyId,
      createdBy: userId,
      month: month,
      year: year
    }
    let updateObj = {};
    if (status == true) {
      updateObj = {
        approvalStatus: status,
        approvedById: currentUser.userInfo[0].userId,
        approvedByName: currentUser.name,
        approvedAt: new Date(),
        updatedAt: new Date(),
        rejectionMessage:"",

      }
    } else if (status == false) {
      updateObj = {
        approvalStatus: status,
        approvedById: "",
        approvedByName: "",
      //  rejectionMessage:"Disapproved",
        approvedAt: "1900-01-01T00:00:00.000Z",
        updatedAt: new Date()
      }
    }
    else if (status == "rejected") {
      updateObj = {
        approvalStatus: false,
        approvedById: "",
        approvedByName: "",
        rejectionMessage: message,
        approvedAt: "1900-01-01T00:00:00.000Z",
        updatedAt: new Date()
      }
    }
    else if (status == "rejected") {
      updateObj = {
        approvalStatus: false,
        approvedById: "",
        approvedByName: "",
        rejectionMessage: message,
        approvedAt: "1900-01-01T00:00:00.000Z",
        updatedAt: new Date()
      }
    }

    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + "/update?where=" + JSON.stringify(whereObj);
    return this.http.post<TourProgram>(url, JSON.stringify(updateObj), { headers: this.headers });
  }

  getTourProgramDeviation(companyId, formObject) {
	      let obj = {
      companyId: companyId,
      userId: formObject.employeeId,
      month: formObject.month,
      year: formObject.year
	}
	   let url = this._urlService.API_ENDPOINT_TOURPROGRAM + "/TPDeviationDetails?param=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;

    return this.http.get<TourProgram>(url);

  }
  getPlannedAreaOnDate(date, createdBy) {
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + '?filter={"where": {"date":"' + date + '","createdBy":"' + createdBy + '"},"fields":{"approvedActivity":true, "area":true, "TPStatus":true,"approvalStatus":true}}';

    return this.http.get(url, { headers: this.headers })
  }

  getPlannedPatchOnDate(date, createdBy) {
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + '?filter={"where": {"date":"' + date + '","createdBy":"' + createdBy + '"},"fields":{"approvedActivity":true, "patchId":true, "TPStatus":true,"approvalStatus":true}}';

    return this.http.get(url, { headers: this.headers })
  }



  getConsolidatedTPDetail(formObj: object): Observable<TourProgram> {
    let formValue = JSON.parse(JSON.stringify(formObj));
    let obj = {
      "companyId": formValue.companyId,
      "stateIds": formValue.stateInfo,
      "districtIds": formValue.districtInfo,
      "status": formValue.status,
      "userIds": formValue.employeeIds,
      "type": formValue.type,
      "month": formValue.month,
      "year": formValue.year
	}
	let url = this._urlService.API_ENDPOINT_TOURPROGRAM + "/getConsolidatedTPDetail?params=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    return this.http.get<TourProgram>(url);
  }

  insertTourProgram(data: any) {
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM;
    return this.http.post(url, JSON.stringify(data), { headers: this.headers });
  }

  // get Dates which tp has been submitted.
  getTPSubmittedDates(data: any):Observable<any> {
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + '/getTPSubmittedDates?params=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers });
  }

  getTPExistOfDay(params: any) {
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + '?filter=' + JSON.stringify(params);
    return this.http.get(url, { headers: this.headers });
  }

  updateTP(params: any) {
    let updateData = {};
    let filter = {
      companyId: params["companyId"],
      createdBy: params["userId"],
      month: parseInt(params["month"]),
      year: parseInt(params["year"])
    };
    //check if date is exists then update the particular date & update the whole month tp
    if(params.hasOwnProperty("date")===true){
      filter["date"]=params["date"];
      updateData = params;
    }else{
      updateData = {
        sendForApproval: true,
        updatedAt: new Date(),
        tpSubmissionDate: new Date()
      }
    }


    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + '/update?where=' + JSON.stringify(filter);
    return this.http.post(url, updateData, { headers: this.headers });
  }

  deleteTP(params:any):Observable<any>{

    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + '/'+params.id;
    console.log(url)
    return this.http.delete(url, { headers: this.headers });
  }


  getTPDayPlan(formObject) {

    let paramObj = JSON.parse(JSON.stringify(formObject));
    let selectedMonth = paramObj.month;
    if (selectedMonth < 10) {
      selectedMonth = "0" + selectedMonth;
    }

    let lastDay = new Date(paramObj.year, paramObj.month, 0).getDate();
    let startDate = paramObj.year + "-" + selectedMonth + "-01";
    let endDate = paramObj.year + "-" + selectedMonth + "-" + lastDay;

    let obj = {
      "companyId": formObject.companyId,
      "userId": formObject.employeeId,
      "month": formObject.month,
      "year": formObject.year,
      "fromDate": startDate,
      "toDate": endDate,
      "type": formObject.type,
      "stateIds": formObject.stateInfo,
      "districtIds": formObject.districtInfo,
      "totalWorkingTypes": formObject.totalWorkingTypes,
      "userIds": formObject.employeeId,
      "isDivisionExist":formObject.isDivisionExist,
      "division":formObject.divisionId
	}
	console.log("object...123",obj)
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM + "/getTPDayPlan";
    return this.http.post<TourProgram>(url,obj);

  }
  
  getUserTpPendingdata(params:object):Observable <any>{
    let url = this._urlService.API_ENDPOINT_TOURPROGRAM  +
			"/reminderVisit?params=" +
			JSON.stringify(params);
		return this.http.get<any>(url, { headers: this.headers });
	}
}
