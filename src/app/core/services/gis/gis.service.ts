import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GisService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  getDistrictCordinate(): Observable<any> {
    
    let url = this._urlService.API_ENDPOINT_Gis ;
    return this.http.get(url, { headers: this.headers });
  }
}
