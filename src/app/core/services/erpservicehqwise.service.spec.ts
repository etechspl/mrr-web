import { TestBed } from '@angular/core/testing';

import { ErpservicehqwiseService } from './erpservicehqwise.service';

describe('ErpservicehqwiseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ErpservicehqwiseService = TestBed.get(ErpservicehqwiseService);
    expect(service).toBeTruthy();
  });
});
