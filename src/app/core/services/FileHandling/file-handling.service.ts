import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileHandlingService {

  constructor(private _urlService: URLService,
    private http: HttpClient,
    private _authService: AuthenticationService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this._authService.getAccessTokenNew()
  });

  uploadUserImage(companyId: string, userId: string, containerName: string, fileUpload: File) {

    const formData = new FormData();
    formData.append('image', fileUpload, fileUpload.name);
    formData.append('companyId', companyId);
    formData.append('userId', userId);
    formData.append('uploadingImageFor', "userImage");

    let url = this._urlService.API_ENDPOINT_CONTAINER + '/' + containerName + '/upload';
    return this.http.post(url, formData, {
      reportProgress: true,
      observe: 'events'
    });

  }


  getDocument(containerName: string, documentName: string) {
    let url = this._urlService.API_ENDPOINT_CONTAINER + '/' + containerName + '/download/' + documentName;
    return this.http.get(url, { headers: this.headers, responseType: 'blob' });

  }

  removeImage(companyId: string, userId: string) {

    let whereObj = {
      companyId: companyId,
      id: userId
    }

    let updateObject = {
      imageId: "5ca72ea2ae04e602af8bfa58",  //Default Image Id
      imageUploaded: false,
      updatedAt: new Date()
    }
    let url = this._urlService.API_ENDPOINT_USERLOGINS + '/update?where=' + JSON.stringify(whereObj)


    return this.http.post(url, JSON.stringify(updateObject), { headers: this.headers })
  }

  getImageDetailBasedOnId(fileId: String): Observable<any> {
    let url = this._urlService.API_ENDPOINT_FileLiberary + '?filter={"where":{"_id":"' + fileId + '"}}';
    return this.http.get(url, { headers: this.headers });
  }

  //---------------------Praveen Kumar(03-04-2020)----------------------------
  removeDocument(containerName: string, documentName: string): Observable<any> {
    const url = `${this._urlService.API_ENDPOINT_CONTAINER}/${containerName}/files/${documentName}`;
    return this.http.delete(url, { headers: this.headers });
  }
  removeFileLibrary(id: string): Observable<any> {
    const url = `${this._urlService.API_ENDPOINT_FileLiberary}/${id}`;
    return this.http.delete(url, { headers: this.headers });
  }
}
