import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';
import { Observable } from 'rxjs';
import { URLService } from './URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class DataTransferHistoryService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  transferRecord(companyId: string,dataIds:any,fromUserId:string,toUserId:string,type:string,transferId:string): Observable<any> {
    let updateData={};
    let updateDataArr=[];
    if(type=="Area"){
      for(var i=0;i<dataIds.length;i++){
        updateData={
        companyId: companyId,
        newUser:toUserId,
        oldUser:fromUserId.toString(),
        dataId:dataIds[i].id,
        transferBy:transferId,
        datatype:type,
        updatedAt:new Date(),
      }
      updateDataArr.push(updateData);
      }
    }else{
      for(var i=0;i<dataIds.length;i++){
        updateData={
        companyId: companyId,
        newUser:toUserId,
        oldUser:fromUserId.toString(),
        dataId:dataIds[i],
        transferBy:transferId,
        datatype:type,
        updatedAt:new Date(),
      }
      updateDataArr.push(updateData);
      }
    }
 
    let url = this._urlService.API_ENDPOINT_DataTransferHistory; 
    return this.http.post(url, updateDataArr, { headers: this.headers });
  }
}
