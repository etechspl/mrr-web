import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';
import { URLService } from './URL/url.service';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErpservicehqwiseService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService) { }

    headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': this.authservice.getAccessTokenNew()
    });

    getPrimaryCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getPrimaryCount?param=' + JSON.stringify(data);
      console.log("url :11 ",url);
    return this.http.get(url, { headers: this.headers })
    }
    getCollectionCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getCollectionCount?param=' + JSON.stringify(data);
      console.log("url : 22",url);
      return this.http.get(url, { headers: this.headers })
    }
    //getOutstandingCount
    getOutstandingCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getOutstandingCount?param=' + JSON.stringify(data);
      console.log("url : 33",url);
      return this.http.get(url, { headers: this.headers })
    }
    getSalesReturnCount(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getSalesReturnCount?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    //done by ravi on 15-10-2020
    
    getStateWiseDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getStateWiseDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }
    getStateWiseProductDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getStateWiseProductDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }

    getHQWiseDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getHQWiseDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }
    getHQWiseProductDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getHQWiseProductDetails?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }

    getPartyWiseDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getPartyWiseDetails?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    getPartyWiseProductDetails(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getPartyWiseProductDetails?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    getERPStateData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getERPStateData?param=' + JSON.stringify(data);
       return this.http.get(url, { headers: this.headers })
    }

    getERPHeadquarterData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getERPHeadquarterData?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    getDesignationsData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getDesignationData?param=' + JSON.stringify(data);
     
    return this.http.get(url, { headers: this.headers })
    }
    getManagerData(data: any) :Observable<any>{
      let url = this._urlService.API_ENDPOINT_ERP + '/getManagerData?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
    }

    //==============Reports APIs Starts
    getTargetVsAchievementVsPrimaryVsClosingHqWise(data : any) : Observable<any>{      
      let url = this._urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getTargetVsAchievementVsPrimaryVsClosingHqWise?params=' + JSON.stringify(data);
      return this.http.get(url, { headers: this.headers})
  }
}
