import { Injectable } from '@angular/core';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  getActivities(companyId: string) {
    let obj = {
      where: {
        companyId: companyId
      }
    }
    let url = this._urlService.API_ENDPOINT_Activity + '?filter=' + JSON.stringify(obj);
    console.log(url)
    return this.http.get<any>(url, { headers: this.headers })
    //  return this.http.get<District>(url)
  }
}
