import { Observable, throwError } from "rxjs";
import { AuthenticationService } from "./../../auth/authentication.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { URLService } from "./../URL/url.service";
import { Injectable } from "@angular/core";
import { catchError, map, tap } from "rxjs/operators";

@Injectable({
	providedIn: "root",
})
export class CrmService {
	constructor(
		private _urlService: URLService,
		private http: HttpClient,
		private authservice: AuthenticationService
	) { }
	headers = new HttpHeaders({
		"Content-Type": "application/json; charset=utf-8",
		Authorization: this.authservice.getAccessTokenNew(),
	});

	serverUrl = this._urlService.API_ENDPOINT;
	acessToken = "&access_token=" + this.authservice.getAccessTokenNew();

	submitCrmSponsorship(data: any): Observable<any> {
		return this.http.post(
			this._urlService.API_ENDPOINT_CRM_SPONSORSHIP,
			data,
			{ headers: this.headers }
		);
	}

	getRequestCountManagerHeirarchy(query: any) {
		let url =
			this._urlService.API_ENDPOINT_CRM_SPONSORSHIP +
			"/count?where=" +
			JSON.stringify(query);
		return this.http.get(url, { headers: this.headers });
	}

	getRequestUserwise(query: any) {
		let url =
			this._urlService.API_ENDPOINT_CRM_SPONSORSHIP +
			"/getCrmRequestUserwise?params=" +
			JSON.stringify(query);
		return this.http.get(url, { headers: this.headers });
	}

	getCRMSponsorshipData(query: any) {
		let url =
			this._urlService.API_ENDPOINT_CRM_SPONSORSHIP +
			"?filter=" +
			JSON.stringify(query);
		return this.http.get(url, { headers: this.headers });
	}

	getRequestDataManagerHeirarchy(query: any) {
		return this.http.get(
			this._urlService.API_ENDPOINT_CRM_SPONSORSHIP +
			"?filter=" +
			JSON.stringify(query),
			{ headers: this.headers }
		);
	}

	getApproveAndDisapproveCrmRequest(query: any) {
		let url =
			this._urlService.API_ENDPOINT_CRM_SPONSORSHIP +
			"/getApproveAndDisapproveCrmRequest?params=" +
			JSON.stringify(query);
		return this.http.get(url, { headers: this.headers });
	}

	getCRMYearlyInvestmentWithROIDoctorWiseReport(params: any) {
		let url = this._urlService.API_ENDPOINT_CRM_SPONSORSHIP + "/getCRMYearlyInvestmentWithROIDoctorWiseReport?params=" + JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}
ipAddress:any;
	getIpAddress() {

		this.http.get("http://api.ipify.org/?format=json").subscribe((res:any)=>{
     	 this.ipAddress = res.ip;
    	});
		console.log(this.ipAddress);
	}
}
