import { TestBed } from '@angular/core/testing';

import { DcrProviderVisitDetailsService } from './dcr-provider-visit-details.service';

describe('DcrProviderVisitDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DcrProviderVisitDetailsService = TestBed.get(DcrProviderVisitDetailsService);
    expect(service).toBeTruthy();
  });
});
