import { Observable } from "rxjs";
import { AuthenticationService } from "./../../auth/authentication.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { URLService } from "./../URL/url.service";
import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root",
})
export class DcrProviderVisitDetailsService {
	constructor(
		private _urlService: URLService,
		private http: HttpClient,
		private authservice: AuthenticationService
	) {}
	headers = new HttpHeaders({
		"Content-Type": "application/json; charset=utf-8",
		Authorization: this.authservice.getAccessTokenNew(),
	});
	getReminderProviderVisitDetails(
		companyId: string,
		submitted:string,
		type: string,
		stateIds: any,
		districtIds: any,
		userId: any,
		fromDate: string,
		toDate: string,
		
		providerType: any,
		unlistedStatus: boolean,
		isDivisionExist: boolean,
		divisionIds: any,
		category: any,
	): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRProviderVisitDetails;
		var obj = {};
		if (type == "State") {
			obj = {
				companyId: companyId,
				stateIds: stateIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				submitted:submitted,
				category: category,
			};
		} else if (type == "Headquarter") {
			obj = {
				companyId: companyId,
				districtIds: districtIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				submitted:submitted,
				category: category,
			};
		} else {
			obj = {
				companyId: companyId,
				userIds: userId,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				category: category,
				submitted:submitted
			};
		}

		console.log("obj+++++++",obj);
		
		url = baseUrl + "/getReminderProviderVisitDetails?params=" + JSON.stringify(obj);
		url = url.replace(/[+]/g, "%2B");
		url = url.replace(/[*]/g, "%2A");
		url = url.replace(/&/g, "%26");
		url = url.replace(/[-]/g, "%2D");
		url = url.replace(/[.]/g, "%2E");
		
		return this.http.get(url, { headers: this.headers });
	

	
		//  return this.http.get(url);
	}

	getProviderVisitDetails(
		companyId: string,
		type: string,
		stateIds: any,
		districtIds: any,
		userId: any,
		fromDate: string,
		toDate: string,
		providerType: any,
		unlistedStatus: boolean,
		isDivisionExist: boolean,
		divisionIds: any,
		category: any,
	): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRProviderVisitDetails;
		var obj = {};
		if (type == "State") {
			obj = {
				companyId: companyId,
				stateIds: stateIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				category: category,
			};
		} else if (type == "Headquarter") {
			obj = {
				companyId: companyId,
				districtIds: districtIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				category: category,
			};
		} else {
			obj = {
				companyId: companyId,
				userIds: userId,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				category: category,
			};
		}

		console.log("obj+++++++++++++rahul+++++++",obj);
		url = baseUrl + "/getProviderVisitDetail?params=" + JSON.stringify(obj);
		url = url.replace(/[+]/g, "%2B");
		url = url.replace(/[*]/g, "%2A");
		url = url.replace(/&/g, "%26");
		url = url.replace(/[-]/g, "%2D");
		url = url.replace(/[.]/g, "%2E"); 
		console.log("url+++++++++++++rahul+++++++",url);
		return this.http.get(url, { headers: this.headers });

		

		//  return this.http.get(url);
	}

	getAreaVisitDetails(
		companyId: string,
		type: string,
		stateIds: any,
		districtIds: any,
		userId: any,
		fromDate: string,
		toDate: string,
		providerType: any,
		unlistedStatus: boolean,
		isDivisionExist: boolean,
		divisionIds: any,
		category: any,
	): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRProviderVisitDetails;
		var obj = {};
		if (type == "State") {
			obj = {
				companyId: companyId,
				stateIds: stateIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				category: category,
			};
		} else if (type == "Headquarter") {
			obj = {
				companyId: companyId,
				districtIds: districtIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				category: category,
			};
		} else {
			obj = {
				companyId: companyId,
				userIds: userId,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
				category: category,
			};
		}

		console.log("obj+++++++++++++rahul+++++++",obj);
		url = baseUrl + "/getAreaVisitDetail?params=" + JSON.stringify(obj);
		url = url.replace(/[+]/g, "%2B");
		url = url.replace(/[*]/g, "%2A");
		url = url.replace(/&/g, "%26");
		url = url.replace(/[-]/g, "%2D");
		url = url.replace(/[.]/g, "%2E"); 
		console.log("url+++++++++++++rahul+++++++",url);
		return this.http.get(url, { headers: this.headers });

		

		//  return this.http.get(url);
	}

	MEFTrackerReport(
		companyId: string,
		type: string,
		stateIds: any,
		districtIds: any,
		userId: any,
		fromDate: string,
		toDate: string,
		providerType: any,
		unlistedStatus: boolean,
		isDivisionExist: boolean,
		divisionIds: any
	): Observable<any> {
		let url;
		let baseUrl = this._urlService.API_ENDPOINT_DCRProviderVisitDetails;
		var obj = {};
		if (type == "State") {
			obj = {
				companyId: companyId,
				stateIds: stateIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
			};
		} else if (type == "Headquarter") {
			obj = {
				companyId: companyId,
				districtIds: districtIds,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
			};
		} else {
			obj = {
				companyId: companyId,
				userIds: userId,
				type: type,
				fromDate: fromDate,
				toDate: toDate,
				providerType: providerType,
				unlistedInclude: unlistedStatus,
				isDivisionExist: isDivisionExist,
				division: divisionIds,
			};
		}

		url = baseUrl + "/MEFTrackerReport?params=" + JSON.stringify(obj);
		url = url.replace(/[+]/g, "%2B");
		url = url.replace(/[*]/g, "%2A");
		url = url.replace(/&/g, "%26");
		url = url.replace(/[-]/g, "%2D");
		url = url.replace(/[.]/g, "%2E");
		return this.http.get(url, { headers: this.headers });

		//  return this.http.get(url);
	}

	//------------------------------------------------------------

	getVisitedDetails(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/mgrWorkedWithVisitDetails?params=" +
			JSON.stringify(params);

		return this.http.get<any>(url, { headers: this.headers });
	}

	getSampleIssueDistributedBalanceDetails(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/getSampleIssueDistributedBalanceDetails?params=" +
			JSON.stringify(params);

		return this.http.get<any>(url, { headers: this.headers });
	}
	getProviderSampleAndGiftDetails(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/getProviderSampleAndGiftDetails?params=" +
			JSON.stringify(params);

		return this.http.get<any>(url, { headers: this.headers });
	}
	getUserInfo(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_USERINFO +
			`?filter=` +
			JSON.stringify(params);
		return this.http.get(url, { headers: this.headers });
	}
	//-----------------PREETI 13-09-2019------------
	getLatLongOnAddress(address: string): Observable<any> {
		let url =
			this._urlService.API_GEOCODE +
			"json?address=" +
			address +
			"+CA&key=AIzaSyCKCxPLVooXnPEFe17z4a65p-7DpJmsA2o";
		return this.http.get<any>(url, { headers: this.headers });
	}
	//-----------------------------------------------
	// Anjana-- 11.06.2019
	getEmployeeLocationOnMap(params: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/getGeoCoordinates?params=" +
			JSON.stringify(params);
		return this.http.get<any>(url, { headers: this.headers });
	}

	getProviderVisitDetailsUsingDcrId(dcrId): Observable<any> {
		let where = {
			dcrId: dcrId,
		};
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			`?filter=` +
			JSON.stringify({ where });
		return this.http.get(url, { headers: this.headers });
	}
	// ----------------------------------mahender-------------------------------------------
	getRemenderCalls(params:object):Observable <any>{
		let url =
			this._urlService.API_ENDPOINT_DCRProviderVisitDetails +
			"/reminderVisit?params=" +
			JSON.stringify(params);
		return this.http.get<any>(url, { headers: this.headers });
	}

	
}
