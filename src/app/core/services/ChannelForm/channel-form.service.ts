import { string } from '@amcharts/amcharts4/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { stringify } from 'querystring';
import { Observable } from 'rxjs';
import { URLService } from '../URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class ChannelFormService {
 
  HttpClient: any;


  constructor(private _urlservice:URLService,
    private _http:HttpClient) { }

  get(filter):Observable<any>{
    const url=this._urlservice.API_ENDPOINT_CHANNELFORMS +'?filter='+JSON.stringify(filter)
    console.log("athila",filter)
    return this._http.get(url);
  }
  formDetails(employee:any):Observable<any>{

    let url = this._urlservice.API_ENDPOINT_CHANNELFORMS;
    return this.HttpClient.post(url,employee);
  }
  

}
