import { TestBed } from '@angular/core/testing';

import { ChannelFormService } from './channel-form.service';

describe('ChannelFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChannelFormService = TestBed.get(ChannelFormService);
    expect(service).toBeTruthy();
  });
});
