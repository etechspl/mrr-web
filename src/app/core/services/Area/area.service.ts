import { AuthenticationService } from './../../auth/authentication.service';
import { AreaModel } from './../../../content/pages/_core/models/area.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { AreaEditDialogComponent } from '../../../content/pages/components/master/area-edit-dialog/area-edit-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class AreaService {


  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });


  createArea(currentUser: any, areaObject: Object): Observable<AreaModel> {

    areaObject['companyId'] = currentUser['companyId'];
    areaObject['areaName'] = areaObject['areaName'].toString().toUpperCase();
    areaObject['status'] = true;
    areaObject['appStatus'] = 'approved';
    areaObject['delStatus'] = '';
    areaObject['mgrAppDate'] = new Date();
    areaObject['finalAppDate'] = new Date();
    areaObject['mgrDelDate'] = new Date('1900-01-02T00:00:00');
    areaObject['finalDelDate'] = new Date('1900-01-02T00:00:00');
    areaObject['appByMgr'] = currentUser['id'];
    areaObject['finalAppBy'] = currentUser['id'];
    areaObject['delByMgr'] = '';
    areaObject['finalDelBy'] = '';
    areaObject['createdAt'] = new Date();
    areaObject['updatedAt'] = new Date();
    if (currentUser['userInfo'][0].rL === 0) {
      areaObject['stateId'] = areaObject['stateId'];
      areaObject['districtId'] = areaObject['districtId'];
    } else if (currentUser['userInfo'][0].rL === 1 || currentUser['userInfo'][0].rL > 1) {
      areaObject['stateId'] = currentUser['userInfo'][0].stateId;
      areaObject['districtId'] = currentUser['userInfo'][0].districtId;
      if (currentUser.company.validation.approvalUpto > 0) {
        areaObject['status'] = false;
        areaObject['appStatus'] = 'pending';
        areaObject['delStatus'] = '';
        areaObject['mgrAppDate'] = new Date('1900-01-02T00:00:00');
        areaObject['finalAppDate'] = new Date('1900-01-02T00:00:00');
        areaObject['mgrDelDate'] = new Date('1900-01-02T00:00:00');
        areaObject['finalDelDate'] = new Date('1900-01-02T00:00:00');
        areaObject['appByMgr'] = '';
        areaObject['finalAppBy'] = '';
      }
    }
    let url = this._urlService.API_ENDPOINT_AREAS;
    return this.http.post<AreaModel>(url, JSON.stringify(areaObject), { headers: this.headers })
    //return null;
  }

  getAreas(companyId: string, districtIds: any): Observable<AreaModel> {
    let obj;
    if (typeof districtIds == "string") { //For Single District
      obj = {
        "where": {
          "companyId": companyId,
          "districtId": districtIds
        },
        "order": "areaName ASC"
      }
    } else { //For Multiple District
      obj = {
        "where": {
          "companyId": companyId,
          "districtId": {
            "inq": districtIds
          }
        },
        "order": "areaName ASC"
      }
    }
    let url = this._urlService.API_ENDPOINT_AREAS + '?filter=' + JSON.stringify(obj);
    return this.http.get<AreaModel>(url, { headers: this.headers })
    //  return this.http.get<District>(url)
  }



  getAreaList(companyId: string, districtIds: any, designationLevel: number, userIds: any): Observable<AreaModel> {
    /*let obj = {
      "companyId": "5b2e22083225df01ba7d6059",
      "designationLevel": 2,
      "districtIds": ["585e3befa7cc34147822688f", "585e3befa7cc341478226881"],
      "userIds": ["5bce1cc8d0a43a0b1e4e23c4"]
    }*/
    let obj = {
      "companyId": companyId,
      "designationLevel": designationLevel,
      "districtIds": [districtIds],
      "userIds": userIds
    }
    console.log("Object---->",obj);
    
    let url = this._urlService.API_ENDPOINT_AREAS + '/getAreaList?parameters=' + JSON.stringify(obj);
    return this.http.get<AreaModel>(url, { headers: this.headers });
  }




	getAreaNameBasedOnId(params:any): Observable<any> {
		console.log("params+++++++++",params);
		let url =this._urlService.API_ENDPOINT_AREAS +'?filter=' + JSON.stringify(params)
		return this.http.get(url, { headers: this.headers });
	}

  checkAreaAlreadyExist(companyId: string, formInfo: any) {
   console.log('userfrominfi',formInfo);
    var checkObj={};
    if(formInfo.hasOwnProperty("areaCode")===true){
      checkObj = {
        "where": {
          "companyId": companyId,
          "areaName": formInfo.areaName.toUpperCase(),
          "areaCode": formInfo.areaCode
        }
      }
    }else{
      // Anjana Rana- 09-07-2019 -- For Upload data module.
      checkObj = {
        "where": {
          "companyId": companyId,
          "userId":formInfo.userId,
          "stateId": formInfo.stateId,
          "districtId": formInfo.districtId,
          "areaName": formInfo.areaName.toUpperCase(),
          "status":true
        }
      }
    }
    console.log('This is checkObj',checkObj);
    let url = this._urlService.API_ENDPOINT_AREAS + '?filter=' + JSON.stringify(checkObj);
    return this.http.get<any>(url, { headers: this.headers });
  }

  editArea(editAreaVal: Object, companyId: string, _id: string): Observable<AreaEditDialogComponent> {
    editAreaVal['companyId'] = companyId;
    editAreaVal['areaName'] = editAreaVal['areaName'].toUpperCase();
    editAreaVal['areaCode'] = editAreaVal['areaCode'].toString();
    editAreaVal['type'] = editAreaVal['type'];
    editAreaVal['updatedBy'] = _id;
    editAreaVal['updatedAt'] = new Date();
    let url = this._urlService.API_ENDPOINT_AREAS + '/editArea?params=' + JSON.stringify(editAreaVal);
    return this.http.get<AreaEditDialogComponent>(url);
  }

  
}
