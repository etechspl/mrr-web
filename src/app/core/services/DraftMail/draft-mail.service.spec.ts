import { TestBed } from '@angular/core/testing';

import { DraftMailService } from './draft-mail.service';

describe('DraftMailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DraftMailService = TestBed.get(DraftMailService);
    expect(service).toBeTruthy();
  });
});
