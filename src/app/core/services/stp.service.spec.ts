import { TestBed } from '@angular/core/testing';

import { STPService } from './stp.service';

describe('STPService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: STPService = TestBed.get(STPService);
    expect(service).toBeTruthy();
  });
});
