import { TestBed } from '@angular/core/testing';

import { ERPServiceService } from './erpservice.service';

describe('ERPServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ERPServiceService = TestBed.get(ERPServiceService);
    expect(service).toBeTruthy();
  });
});
