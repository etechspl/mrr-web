import { TestBed } from '@angular/core/testing';

import { ExpenseClaimedService } from './expense-claimed.service';

describe('ExpenseClaimedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExpenseClaimedService = TestBed.get(ExpenseClaimedService);
    expect(service).toBeTruthy();
  });
});
