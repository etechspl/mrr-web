import { TestBed } from '@angular/core/testing';

import { StpApprovalService } from './stp-approval.service';

describe('StpApprovalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StpApprovalService = TestBed.get(StpApprovalService);
    expect(service).toBeTruthy();
  });
});
