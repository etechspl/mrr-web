import { Injectable } from '@angular/core';
import { URLService } from './URL/url.service';
import { AuthenticationService } from '../auth/authentication.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as _moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ExpenseClaimedService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  })

  getExpenseDetails(params: object): Observable<any> {
    let filter = {
      where: {
        userId: params["userId"],
        companyId: params["companyId"],
        dcrDate: {
          between: [new Date(params["startDate"]), new Date(params["endDate"])]
        }
      },
       "include": [
        {
         "relation": "userLogin"
       },{
       "relation": "userInfo"
     },{
      "relation": "district"
    },{
      "relation": "dCRMaster"
    }
     ],
     order: "dcrDate ASC"
    }
    let url = this._urlService.API_ENDPOINT_EXPENSE_CLAIMED + '/?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
  }

  getVisitedAreaAsPerProviderVisit(params: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_DCRProviderVisitDetails + '/getVisitedAreaAsPerProviderVisit?params=' + JSON.stringify(params);
    return this.http.get(url, { headers: this.headers })
  }

  getMiscExpeFileId(params: object): Observable<any> {
    let filter = {
      where: {
        submitBy: params["userId"],
        companyId: params["companyId"],
        dcrDate: {
          between: [new Date(params["startDate"]), new Date(params["endDate"])]
        },
        id:params["dcrId"],
      }
    }
    let url = this._urlService.API_ENDPOINT_DCRMASTER + '/?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
  }
  getDADetails(params: object): Observable<any> {
    let filter = {
      where: params
    }
    
    let url = this._urlService.API_ENDPOINT_DAILY_ALLOWANCES + '/?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
  }

  getUserWiseExpense(params: object): Observable<any> {

    let url = this._urlService.API_ENDPOINT_EXPENSE_CLAIMED + '/getUserWiseExpense?params=' + JSON.stringify(params);
    return this.http.get(url, { headers: this.headers })
  }
  getConsolidatedExpense(params: any): Observable<any> {

    let url = this._urlService.API_ENDPOINT_EXPENSE_CLAIMED + '/getConsolidatedExpense?params=' + JSON.stringify(params);
    return this.http.get(url, { headers: this.headers })
  }


  // modifyExpense(params: object) {

  //   let where = {}
  //   let updateValue = {};
  //   if (params["rL"] > 1) {
  //     if (params["finalize"] === false) {
  //       where = {
  //         id: params["id"]
  //       }
  //       updateValue = {
  //         daMgr: params["daMgr"],
  //         daAdmin: params["daMgr"],
  //         fareMgr: params["fareMgr"],
  //         fareAdmin: params["fareMgr"],
  //         distanceMgr: params["distanceMgr"],
  //         distanceAdmin: params["distanceMgr"],
  //         miscExpenseMgr: params["miscExpenseMgr"],
  //         miscExpenseAdmin: params["miscExpenseMgr"],
  //         sumExpAdm: params["sumExpAdm"],
  //         subtExpeAdm: params["subtExpeAdm"],
  //         remarksMgr: params["remarksMgr"],
  //         remarksAdmin: params["remarksMgr"],
  //         totalExpenseMgr: (params["daMgr"] + params["fareMgr"] + params["miscExpenseMgr"] + params["sumExpMgr"] - params["subtExpeMgr"]),
  //         totalExpenseAdmin: (params["daMgr"] + params["fareMgr"] + params["miscExpenseMgr"] + params["sumExpMgr"] - params["subtExpeMgr"]),
  //         modifiedByMgr: true,
  //       }
  //     } else if (params["finalize"] === true) {
  //       where = {
  //         userId: params["userId"],
  //         dcrDate: { between: [new Date(params["fromDate"]), new Date(params["endDate"])] }
  //       }
  //       updateValue = {
  //         appByMgr: params["appBy"],
  //         appMgrDate: new Date()
  //       }
  //     }

  //   } else if (params["rL"] === 0) {
  //     if (params["finalize"] === false) {
  //       where = {
  //         id: params["id"]
  //       }

  //       updateValue = {
  //         daAdmin: params["daMgr"],
  //         fareAdmin: params["fareMgr"],
  //         distanceAdmin: params["distanceMgr"],
  //         miscExpenseAdmin: params["miscExpenseMgr"],
  //         sumExpAdm: params["sumExpAdm"],
  //         subtExpeAdm: params["subtExpeAdm"],
  //         remarksAdmin: params["remarksMgr"],
  //         totalExpenseAdmin: (params["daMgr"] + params["fareMgr"] + params["miscExpenseMgr"] + params["sumExpAdm"] - params["subtExpeAdm"]),
  //         modifiedByAdm: true,
  //       }
  //     } else if (params["finalize"] === true) {
  //       where = {
  //         userId: params["userId"],
  //         dcrDate: { between: [new Date(params["fromDate"]), new Date(params["endDate"])]}
  //       }

  //       updateValue = {
  //         appByAdm: params["appBy"],
  //         appAdmDate: new Date()
  //       }
  //     }
  //   }

  //   console.log("where",where);
  //   let url = this._urlService.API_ENDPOINT_EXPENSE_CLAIMED + '/update?where=' + JSON.stringify(where);
  //   return this.http.post(url, updateValue, { headers: this.headers });
  // }


  //--------------------PK(2019-12-18)---------------------------
  
  modifyExpense(params: object) {
 
    let where = {}
    let updateValue = {};
    if (params["rL"] > 1) {
      if (params["finalize"] === false) {
        where = {
          id: params["id"]
        }
        updateValue = {
          daMgr: params["daMgr"],
          daAdmin: params["daMgr"],
          fareMgr: params["fareMgr"],
          fareAdmin: params["fareMgr"],
          distanceMgr: params["distanceMgr"],
          distanceAdmin: params["distanceMgr"],
          miscExpenseMgr: params["miscExpenseMgr"],
          miscExpenseAdmin: params["miscExpenseMgr"],
          sumExpAdm: params["sumExpAdm"],
          subtExpeAdm: params["subtExpeAdm"],
          remarksMgr: params["remarksMgr"],
          appByAdm: params["appBy"],
          appAdmDate: new Date(),
          remarksAdmin: params["remarksMgr"],
          totalExpenseMgr: (params["daMgr"] + params["fareMgr"] + params["miscExpenseMgr"] + params["sumExpMgr"] - params["subtExpeMgr"] ),
          totalExpenseAdmin: (params["daMgr"] + params["fareMgr"] + params["miscExpenseMgr"] + params["sumExpMgr"] - params["subtExpeMgr"] ),
          modifiedByMgr: true,
        }
      } else if (params["finalize"] === true) {
        where = {
          userId: params["userId"],
          dcrDate: { between: [new Date(params["fromDate"]), new Date(params["endDate"])] }
        }
      }
 
    } else if (params["rL"] === 0) {
      if (params["finalize"] === false) {
        where = {
          id: params["id"]
        }
 
        updateValue = {
          daAdmin: params["daMgr"],
          fareAdmin: params["fareMgr"],
          distanceAdmin: params["distanceMgr"],
          miscExpenseAdmin: params["miscExpenseMgr"] ,
          sumExpAdm: params["sumExpAdm"],
          subtExpeAdm: params["subtExpeAdm"],
          remarksAdmin: params["remarksMgr"],
          appByAdm: params["appBy"],
          appAdmDate: new Date(),
          totalExpenseAdmin: (params["daMgr"] + params["fareMgr"] + params["miscExpenseMgr"]+params["sumExpAdm"] - params["subtExpeAdm"]),
          modifiedByAdm: true,
        }
      } else if (params["finalize"] === true) {
        console.log("pk sir",params["userId"],params["appBy"])
        where = {
          userId: params["userId"],
          dcrDate: { between: [new Date(params["fromDate"]), new Date(params["endDate"])]}
        }
 
        updateValue = {
          appByAdm: params["appBy"],
          appAdmDate: new Date()
        }
      }
    }
 
    console.log("where",updateValue);
    let url = this._urlService.API_ENDPOINT_EXPENSE_CLAIMED + '/update?where=' + JSON.stringify(where);
    return this.http.post(url, updateValue, { headers: this.headers });
  }

  getConsolidatedExpenseNew(params: object): Observable<any> {
    console.log("params :  ",params);
    let url = this._urlService.API_ENDPOINT_EXPENSE_CLAIMED + '/getConsolidatedExpenseNew?params=' + JSON.stringify(params);
    return this.http.get(url, { headers: this.headers })
  }
  
  
  //--------------------Aakash (22-07-2020)---------------------------
  getExpenseClaimedByDcrId(where: object): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_EXPENSE_CLAIMED +
			"?filter=" +
			JSON.stringify({ where });
		return this.http.get(url, { headers: this.headers });
  }
  


  getProviderVisit(params: object): Observable<any> {
    let filter = {
    where: {
    submitBy: params["submitBy"],
    companyId: params["companyId"],
    dcrId:params["dcrId"]
    }
    }
    let url = this._urlService.API_ENDPOINT_DCRProviderVisitDetails + '/?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
    }

	deleteExpenseClaimed(id): Observable<any> {
		let url = this._urlService.API_ENDPOINT_EXPENSE_CLAIMED + `/${id}`;
		return this.http.delete(url, { headers: this.headers });
	}
  
  getExpenseDump(param: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_DCRMASTER + '/getExpenseDump?params=' + JSON.stringify(param);
     return this.http.post<any>(url, JSON.stringify(param), { headers: this.headers });
  }
}
