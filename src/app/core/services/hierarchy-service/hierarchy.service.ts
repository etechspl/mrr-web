import { HierarchyModel } from '../../../content/pages/_core/models/hierarchy.model';
import { UserLogin } from '../../../content/pages/_core/models/userlogin.model';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class HierarchyService {
  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }
  serverUrl = this._urlService.API_ENDPOINT;
  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

// ********** Assigning lower managers of Reporting manager to Supervisor *******  By Aakash on (12-03-2020) *************
  createHierarchyOnEdit(userEditForm: any, companyId: any) {
    let obj = {
      supervisorId: userEditForm.id,
      companyId: companyId,
      status: true,
      type: "lower"
    }
    // to get all the lower managers
    this.getManagerHierarchy(obj).subscribe(res => {
      let url = this.serverUrl + '/Hierarchies/';
      let juniorMgrs = res as [];
      let data = [];
      for (let mgr of juniorMgrs) {
        if(userEditForm.immediateSupervisorName !== null){
          userEditForm.immediateSupervisorName.forEach(item =>{
            let obj = {
              "companyId": companyId,
              "userId": mgr['userId'],
              "userName": mgr['name'],
              "userDesignation": mgr['designation'],
              "userDesignationLevel": mgr['designationLevel'],
              "supervisorId": item.id,
              "supervisorName": item.name,
              "supervisorDesignation": item.designation,
              "immediateSupervisorId": item.id,
              "immediateSupervisorName": item.name,
              "status": true
            }
            data.push(obj);
          })
        }
      }
      this.http.post(url, data, { headers: this.headers }).subscribe(res => {
      }, err => {
      });
    })
  }
// ******************** End createHierarchyOnEdit *********************
  createHierarchy(userModel: UserLogin, createdUserLoginResponses: object, createdUserInfoResponses: object) {
    let url = this.serverUrl + '/Hierarchies/';
    let data = [];
    let userHierarchy = {
      "companyId": createdUserLoginResponses['companyId'],
      "userId": createdUserLoginResponses['id'],
      "userName": createdUserLoginResponses['name'],
      "userDesignation": createdUserInfoResponses['designation'],
      "userDesignationLevel": createdUserInfoResponses['designationLevel'],
      "supervisorId": userModel.reportingManager['id'],
      "supervisorName": userModel.reportingManager['name'],
      "supervisorDesignation": userModel.reportingManager['designation'],
      "immediateSupervisorId": userModel.reportingManager['id'],
      "immediateSupervisorName": userModel.reportingManager['name'],
      "status": true
    }


    //update the user to all the sr manager
    let query = {
      where: {
        status: true,
        companyId: createdUserLoginResponses['companyId'],
        userId: userModel.reportingManager['id'],
      }
    }
    let url1 = this.serverUrl + '/Hierarchies?filter=' + JSON.stringify(query);
    this.http.get<HierarchyModel[]>(url1, { headers: this.headers }).subscribe(res => {
      let userSrMgrs = res as HierarchyModel[];
      // while adding the user

      for (let userSrMgr of userSrMgrs) {
        let obj = {
          "companyId": userSrMgr['companyId'],
          "userId": createdUserLoginResponses['id'],
          "userName": createdUserLoginResponses['name'],
          "userDesignation": createdUserInfoResponses['designation'],
          "userDesignationLevel": createdUserInfoResponses['designationLevel'],
          "supervisorId": userSrMgr['supervisorId'],
          "supervisorName": userSrMgr['supervisorName'],
          "supervisorDesignation": userSrMgr['supervisorDesignation'],
          "immediateSupervisorId": userSrMgr['supervisorId'],
          "immediateSupervisorName": userSrMgr['supervisorName'],
          "status": true
        }
        data.push(obj);
      }
      //data.push(userHierarchy);
      this.http.post(url, data, { headers: this.headers }).subscribe(res => {
      }, err => {

      });
    }, err => {

    })
    return this.http.post(url, userHierarchy, { headers: this.headers });

  }

  getManagerHierarchy(params: any): Observable<any> {
    console.log("params->",params);
    
    let url = this._urlService.API_ENDPOINT_HIERARCHY + '/getManagerHierarchy?params=' + JSON.stringify(params);
    console.log("url=>",url);
    return this.http.get(url, { headers: this.headers })

  }
  getManagerHierarchyInArray(params: Object) {
    let url = this._urlService.API_ENDPOINT_HIERARCHY + '/getManagerHierarchyInArray?params=' + JSON.stringify(params);
    return this.http.get(url, { headers: this.headers })

  }

  deleteHierarchys(Hierarchies: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_HIERARCHY + '/deleteHierarchys?param=' + JSON.stringify(Hierarchies);
    return this.http.get(url, { headers: this.headers })
  }


  gettingUserReportingManager(Hierarchies: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_HIERARCHY + '/gettingUserReportingManager?params=' + JSON.stringify(Hierarchies);
    return this.http.get(url, { headers: this.headers })
  }


  //-----------------PK(2019-12-09)------------------
  createHierarchyNew(object: any): Observable<any> {
    const url = this._urlService.API_ENDPOINT_HIERARCHY;
    return this.http.post(url, object, { headers: this.headers });
  }
  updateHierarchy(whereObject: object, updateObject: object): Observable<any> {
    const url = this._urlService.API_ENDPOINT_HIERARCHY + '/update?where=' + JSON.stringify(whereObject);
    return this.http.post(url, updateObject, { headers: this.headers });
  }
  //-----------------------------------------------------------


  hierarchyMappingWithTeam(params: any): Observable<any> {
		// let url =`${this._urlService.API_ENDPOINT_HIERARCHY}/hierarchyMappingWithTeam?params=${JSON.stringify(params)}`;
		let url =`${this._urlService.API_ENDPOINT_HIERARCHY}/hierarchyMappingWithTeam`;
		return this.http.post(url, params);
	}

}
