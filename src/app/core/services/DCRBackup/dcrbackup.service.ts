import { Observable } from 'rxjs';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLService } from '../URL/url.service';
import { Injectable } from '@angular/core';
import { formatDate } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class DCRBackupService {

  constructor(private _urlService: URLService,
    private http: HttpClient,
    private authservice: AuthenticationService) { }
    headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': this.authservice.getAccessTokenNew()
    });
    submitDCRCopy(dcrObj:any): Observable<any>{
      let url = this._urlService.API_ENDPOINT_DCRMASTERBACKUP;
      return this.http.post(url, JSON.stringify(dcrObj), { headers: this.headers });
    }
}
