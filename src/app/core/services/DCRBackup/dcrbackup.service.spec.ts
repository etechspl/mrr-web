import { TestBed } from '@angular/core/testing';

import { DCRBackupService } from './dcrbackup.service';

describe('DCRBackupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DCRBackupService = TestBed.get(DCRBackupService);
    expect(service).toBeTruthy();
  });
});
