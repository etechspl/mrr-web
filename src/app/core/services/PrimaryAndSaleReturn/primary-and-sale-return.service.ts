import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class PrimaryAndSaleReturnService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private urlService: URLService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  getTargetVsAchievementData(data: object): Observable<any> {
    let incomingData = JSON.parse(JSON.stringify(data));
    
    let obj = {};
    if (incomingData.type == "state") {
    if (incomingData.lookingFor == "all") {
    obj = {
    "type": "state",
    "lookingFor": "all",
    "companyId": incomingData.companyId,
    "month": incomingData.month,
    "year": incomingData.year,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist
    }
    } else if (incomingData.lookingFor == "selected") {
    obj = {
    "type": "state",
    "lookingFor": "selected",
    "companyId": incomingData.companyId,
    "month": incomingData.month,
    "year": incomingData.year,
    "stateIds": incomingData.stateIds,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist
    }
    }
    } else if (incomingData.type == "headquarter") {
    if (incomingData.lookingFor == "all") {
    obj = {
    "type": "headquarter",
    "lookingFor": "all",
    "companyId": incomingData.companyId,
    "stateIds": [incomingData.stateId],
    "stateCode":incomingData.stateCode,
    "month": incomingData.month,
    "year": incomingData.year,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist,
    "APIEndPoint": incomingData.APIEndPoint
    }
    } else if (incomingData.lookingFor == "selected") {
    obj = {
    "type": "headquarter",
    "lookingFor": "selected",
    "companyId": incomingData.companyId,
    "month": incomingData.month,
    "year": incomingData.year,
    "stateCode":incomingData.stateCode,
    "headquarterIds": incomingData.headquarterId,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist,
    "APIEndPoint": incomingData.APIEndPoint 
    }
    }
    }
    console.log("obj----------------->",obj)
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/targetVsAchievementGraph?param=' + JSON.stringify(obj);
    
    return this.http.get<any>(url, { headers: this.headers })
    }

  getMRQtySecondarySale(param: object): Observable<any> {
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/MRQtySecondarySale?param=' + JSON.stringify(param);
    return this.http.get<any>(url,{ headers: this.headers })
  }

  getSalesStatus(params: any): Observable<any> {
    var obj = {
      companyId: params.companyId,
      stateId: params.stateId,
      districtId: params.districtId,
      userId: params.userId,
      month: params.month,
      year: params.year,
	  type: params.type,
	  designation : params.designation
    };
	console.log("obj....",obj)
	let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getSecondarySalesStatus?param=' + JSON.stringify(obj);
	console.log("url....",url)
    return this.http.get(url, { headers: this.headers })
  }
  managerwiseSSS(params: any): Observable<any> {
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/managerwiseSSS?param=' + JSON.stringify(params);
    return this.http.get<any>(url, { headers: this.headers })
  }
  getProductCategroryOrBrandWiseSale(param:any){
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getProductCategroryOrBrandWiseSale?param=' + JSON.stringify(param);
    return this.http.get(url, { headers: this.headers })
  }
  stockistwiseSales(param:any): Observable<any>{
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/stockistwiseSales?param=' + JSON.stringify(param);
    return this.http.get(url, { headers: this.headers })
  }
  getStockists(param:any): Observable<any>{
    let url = this.urlService.API_ENDPOINT_PROVIDERS + '?filter='+JSON.stringify(param);
    return this.http.get(url, { headers: this.headers })
  }
  //------------By Preeti Arora 31-12-2019------------------//

  getTragetVsAchievementData(params:any): Observable<any>{
    console.log("params: ------ Services---",params);
    var obj = {};
    if (params.type == "State") {
      obj = {
        "companyId": params.companyId,
        "stateIds": params.stateIds,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist
      }
    } else if (params.type == "Headquarter") {
      obj = {
        "companyId": params.companyId,
        "districtIds": params.districtIds,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist
      }
    } else {
      obj = {
        "companyId": params.companyId,
        "userIds": params.employeeId,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist
      }
    }

    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn+ '/getTargetVsAchievementData?params=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  //-------------------END ----------------------------
  getTargetVsAchievementDataForHq(data: object): Observable<any> {
    let incomingData = JSON.parse(JSON.stringify(data));
    
    let obj = {};
    if (incomingData.type == "state") {
    if (incomingData.lookingFor == "all") {
    obj = {
    "type": "state",
    "lookingFor": "all",
    "companyId": incomingData.companyId,
    "month": incomingData.month,
    "year": incomingData.year,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist
    }
    } else if (incomingData.lookingFor == "selected") {
    obj = {
    "type": "state",
    "lookingFor": "selected",
    "companyId": incomingData.companyId,
    "month": incomingData.month,
    "year": incomingData.year,
    "stateIds": incomingData.stateIds,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist
    }
    }
    } else if (incomingData.type == "headquarter") {
    if (incomingData.lookingFor == "all") {
    obj = {
    "type": "headquarter",
    "lookingFor": "all",
    "companyId": incomingData.companyId,
    "stateIds": [incomingData.stateId],
    "stateCode":incomingData.stateCode,
    "month": incomingData.month,
    "year": incomingData.year,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist,
    "APIEndPoint": incomingData.APIEndPoint
    }
    } else if (incomingData.lookingFor == "selected") {
    obj = {
    "type": "headquarter",
    "lookingFor": "selected",
    "companyId": incomingData.companyId,
    "month": incomingData.month,
    "year": incomingData.year,
    "stateCode":incomingData.stateCode,
    "headquarterIds": incomingData.headquarterId,
    "designationLevel": incomingData.designationLevel,
    "userId": incomingData.userId,
    "isDivisionExist": incomingData.isDivisonExist,
    "APIEndPoint": incomingData.APIEndPoint 
    }
    }
    }
    
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getTargetVsAchPrimaryERPData?param=' + JSON.stringify(obj);
    
    return this.http.get<any>(url, { headers: this.headers })
    }
//--------------------------
}
