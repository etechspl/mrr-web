import { TestBed } from '@angular/core/testing';

import { PrimaryAndSaleReturnService } from './primary-and-sale-return.service';

describe('PrimaryAndSaleReturnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrimaryAndSaleReturnService = TestBed.get(PrimaryAndSaleReturnService);
    expect(service).toBeTruthy();
  });
});
