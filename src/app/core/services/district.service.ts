import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';
import { District } from '../../content/pages/_core/models/district.model';
import { URLService } from './URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {
  constructor(private http: HttpClient, private authservice: AuthenticationService, private urlService: URLService) {
  }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });


  serverUrl = this.urlService.API_ENDPOINT;
  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();
  //Get State List Based on Company.
  getDistricts(companyId: string, stateId: string): Observable<District> {
    var obj = {
      "where": {
        "stateId": stateId,
        "assignedTo": companyId,
        "status": true
      },
      "order": "districtName ASC",
      "include": {
        "relation": "state",
        "scope": {
          "fields": [
            "id",
            "stateName"
          ]
        }
      }
    }


    let url = this.serverUrl + '/Districts?filter=' + JSON.stringify(obj);
    return this.http.get<District>(url, { headers: this.headers })
  }
  getDistrict(companyId: string, stateId?: any): Observable<any> {
    let url: string;
    let obj: object;
    if (typeof stateId == "string") { //For Single State
      obj = {
        "assignedTo": companyId,
        "stateId": [stateId],
        isDivisionExist: false
      }
    } else { //For Multiple State
      obj = {
        "assignedTo": companyId,
        "stateId": stateId,
        isDivisionExist: false
      }
    }


    url = this.urlService.API_ENDPOINT_DISTRICTS + '/getDistrict?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }



  //-------------------------------PK(2019-06-2019)---------------------------------------------
  getDistrictsBasedOnDivision(object: any): Observable<any> {
    let url: string;
    let obj: object;
    if (typeof object.stateId == "string") { //For Single State
      obj = {
        "assignedTo": object.companyId,
        "stateId": [object.stateId],
        isDivisionExist: true,
        division: object.division
      }
    } else { //For Multiple State
      obj = {
        "assignedTo": object.companyId,
        "stateId": object.stateId,
        isDivisionExist: true,
        division: object.division
      }
    }
    url = this.urlService.API_ENDPOINT_DISTRICTS + '/getDistrict?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getUniqueHqOnManagerBasedOnDivision(object: any): Observable<any> {
    let obj = {}

    if (typeof (object.stateIds) == "string") {
      obj = {
        companyId: object.companyId,
        supervisorId: object.managerId,
        stateIds: [object.stateId],
        isDivisionExist: true,
        division: object.division,
        isContainStateFilter: true
      };
    } else {

      obj = {
        companyId: object.companyId,
        supervisorId: object.managerId,
        stateIds: object.stateId,
        isDivisionExist: true,
        division: object.division,
        isContainStateFilter: true
      };
    }

    let url = this.urlService.API_ENDPOINT_USERINFO + '/getUniqueHeadquarterOnManager?parameters=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });

  }
  //------------------------------------END-----------------------------------------------------


  getDistrictToBeMapped(companyId: string, stateId: string) {
    var obj = {
      companyId: companyId,
      stateId: stateId
    }
    let url = this.urlService.API_ENDPOINT_DISTRICTS + '/getDistrictToBeMapped?params=' + JSON.stringify(obj)
    return this.http.get(url, { headers: this.headers })
  }

  mapDistricts(companyId, stateId, selectedDistricts) {

    var obj = {
      stateId: stateId,
      companyId: companyId,
      selectedDistricts: selectedDistricts
    }
    let url = this.urlService.API_ENDPOINT_DISTRICTS + '/mapDistricts?params=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  removeMappedDistricts(companyId, stateId, selectedDistricts) {

    var obj = {
      stateId: stateId,
      companyId: companyId,
      selectedDistricts: selectedDistricts
    }
    let url = this.urlService.API_ENDPOINT_DISTRICTS + '/removeMappedDistricts?params=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  createDistrict(companyId: string, districtForm: Object) {
    districtForm['companyId'] = companyId;
    let url = this.urlService.API_ENDPOINT_DISTRICTS + '/createDistrict?params=' + JSON.stringify(districtForm);
    return this.http.post(url, { headers: this.headers })
  }
  // Check Exist State Status - Anjana Rana for Data Upload
  checkDistrictStatus(param: any) {
    var obj = {
      "where": param
    }
    let url = this.urlService.API_ENDPOINT_DISTRICTS + '?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });
  }


  //------------------PK(26-12-2019)-------------------
  getDistrictNew(whereObject: object): Observable<any> {
    let url = this.urlService.API_ENDPOINT_DISTRICTS + '?filter=' + JSON.stringify(whereObject)
    return this.http.get(url, { headers: this.headers });
  }


  updateDistrictDetail(whereObject: object, dataToBeUpdate: object): Observable<any> {
    const url = this.urlService.API_ENDPOINT_DISTRICTS + "/update?where=" + JSON.stringify(whereObject);
    return this.http.post(url, dataToBeUpdate, { headers: this.headers });
  }
  //---------------------------END----------------------------------------

  getDistrictsForTarget(companyId: string, stateId?: any): Observable<any> {
    let url: string;
    let obj: object;
    if (typeof stateId == "string") { //For Single State
      obj = {
        "assignedTo": companyId,
        "stateId": [stateId],
        isDivisionExist: false
      }
    } else { //For Multiple State
      obj = {
        "assignedTo": companyId,
        "stateId": stateId,
        isDivisionExist: false
      }
    }
    url = this.urlService.API_ENDPOINT_DISTRICTS + '/getDistricts?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

}
