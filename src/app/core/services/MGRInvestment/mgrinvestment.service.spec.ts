import { TestBed } from '@angular/core/testing';

import { MGRInvestmentService } from './mgrinvestment.service';

describe('MGRInvestmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MGRInvestmentService = TestBed.get(MGRInvestmentService);
    expect(service).toBeTruthy();
  });
});
