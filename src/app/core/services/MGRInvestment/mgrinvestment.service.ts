import { AuthenticationService } from './../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLService } from './../URL/url.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MGRInvestmentService {

  
  constructor(private _urlService: URLService,
    private http: HttpClient,
    private authservice: AuthenticationService) { }

    headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': this.authservice.getAccessTokenNew()
    });

  submitInvestment(investmentObj: any, step: number, investmentId: string): Observable<any> {


    if (step == 0) {
      if (investmentId == "0") {
        let url = this._urlService.API_ENDPOINT_MGRInvestment;
        return this.http.post(url, investmentObj, { headers: this.headers })
      } else {
        let whereObj = {
          id: investmentId
        }
        let url = this._urlService.API_ENDPOINT_MGRInvestment + "/update?where=" + JSON.stringify(whereObj);
        return this.http.post(url, JSON.stringify(investmentObj), { headers: this.headers });
      }
    } else if (step == 1 || step == 2) {

      let url = this._urlService.API_ENDPOINT_MGRInvestmentVisitDetails;
      let finalObj = [];
      finalObj.push(investmentObj);
      console.log("finalObj : ",finalObj);
      return this.http.post(url, investmentObj, { headers: this.headers });

    }else if (step == 11) {
      let whereObj = {
        id: investmentId
      }

      let url = this._urlService.API_ENDPOINT_MGRInvestment + "/update?where=" + JSON.stringify(whereObj);
      return this.http.post(url, JSON.stringify(investmentObj), { headers: this.headers });
    }

  }

  deleteInvestmentVisitDetail(investmentId: string, providerType: any) {
    let whereObj = {
      "investmentId": investmentId,
      "providerType": providerType
    }
    let url = this._urlService.API_ENDPOINT_MGRInvestmentVisitDetails + '/deleteMatchedRecord?params=' + JSON.stringify(whereObj);
    return this.http.get(url, { headers: this.headers })

  }

  getUserCompleteInvestmentDetails(investmentId: string): Observable<any>{
    let url = this._urlService.API_ENDPOINT_MGRInvestment + '?filter={"where": {"id":"' + investmentId + '"} ,"include": [{"relation": "info","scope": { "order":"totalSession.timeIn ASC","include":[{"relation":"providerinfo","scope":{"fields":["providerName","address","category"] ,"order":"totalSession.timeIn ASC"}},{"relation":"block","scope":{"fields":["areaName"]}}]}},{"relation":"userInfo","scope":{"fields":["name","districtName"]}}]}';
   return this.http.get(url, { headers: this.headers })
  }

  getUserInvestmentDetailsOnDate(submissionDate: string, userId: string) {
    let url = this._urlService.API_ENDPOINT_MGRInvestment + '?filter={"where": {"submissionDate":"' + submissionDate + '","submitBy":"' + userId + '"},"include": [{"relation": "info","scope": { "order":"totalSession.timeIn ASC","include":[{"relation":"providerinfo","scope":{"fields":["providerName","address","category"] ,"order":"totalSession.timeIn ASC"}},{"relation":"block","scope":{"fields":["areaName"]}}]}},{"relation":"userInfo","scope":{"fields":["name","districtName"]}}]}';
    return this.http.get(url, { headers: this.headers })
  }

  getInvestmentDetailsDoneOrNot(submissionDate: string, userId: string) {
    let url = this._urlService.API_ENDPOINT_MGRInvestment + '?filter={"where": {"submissionDate":"' + submissionDate + '","submitBy":"' + userId + '", "InvestmentVersion" : "complete"}}';
    return this.http.get(url, { headers: this.headers })
  }

  getInvestmentDetails(params): Observable<any> {
    let url;
    let baseUrl = this._urlService.API_ENDPOINT_MGRInvestment;
    var obj = {};
    if (params.type == "State") {
      obj = {
        "companyId": params.companyId,
        "stateIds": params.stateIds,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist,
        "status": params.status
      }
    } else if (params.type == "Headquarter") {
      obj = {
        "companyId": params.companyId,
        "districtIds": params.districtIds,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist,
        "status": params.status
      }
    } else {
      obj = {
        "companyId": params.companyId,
        "userIds": params.employeeId,
        "type": params.type,
        "fromDate": params.fromDate,
        "toDate": params.toDate,
        "division": params.division,
        "isDivisionExist": params.isDivisionExist,
        "status": params.status
      }
    }
    url = baseUrl + '/getInvestmentDetails?params=' + JSON.stringify(obj);
    console.log("url : ",url);
    return this.http.get(url, { headers: this.headers })

  }

  getUserDateWiseInvestmentDetail(userId: string, fromDate: string, toDate: string): Observable<any> {
    var obj = {};
    obj = {
      "userId": userId,
      "fromDate": fromDate,
      "toDate": toDate
    }
    let url = this._urlService.API_ENDPOINT_MGRInvestment + '/getUserDateWiseInvestmentDetail?userId=' + userId + '&fromDate=' + fromDate + '&toDate=' + toDate;
    return this.http.get(url, { headers: this.headers })

  }

  getUserInvestmentDetail(userId: string, fromDate: string, toDate: string): Observable<any> {
    var obj = {};
    obj = {
      "userId": userId,
      "fromDate": fromDate,
      "toDate": toDate
    }
    let url = this._urlService.API_ENDPOINT_MGRInvestment + '/getUserInvestmentDetail?userId=' + userId + '&fromDate=' + fromDate + '&toDate=' + toDate;
    return this.http.get(url, { headers: this.headers })

  }

  getUserJointworkDetails(userId: string, fromDate: string, toDate: string) {
    var obj = {};
    obj = {
      "userId": userId,
      "fromDate": fromDate,
      "toDate": toDate
    }
    let url = this._urlService.API_ENDPOINT_MGRInvestmentVisitDetails + '/getUserJointworkDetails?userId=' + userId + '&fromDate=' + fromDate + '&toDate=' + toDate;

    return this.http.get(url, { headers: this.headers })

  }
}
