import { filter } from 'rxjs/operators';
import { AuthenticationService } from './../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URLService } from './URL/url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DesignationService {

  constructor(private httpClient: HttpClient,private authservice : AuthenticationService, private _urlService : URLService ) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  acessToken = "&access_token="+this.authservice.getAccessTokenNew();
  //Get State List Based on Company.
  getDesignations(companyId : string){  
    let url = this._urlService.API_ENDPOINT+"/designations?filter={\"where\":{\"companyId\":\""+companyId+"\"}}"+this.acessToken;
    return this.httpClient.get(url)
  }
  getDesignationId(param : any): Observable<any>{  
    let filters={
      "where":param
    }
    let url = this._urlService.API_ENDPOINT+"/designations?filter="+JSON.stringify(filters);
    return this.httpClient.get(url,{ headers: this.headers })

  }
}
