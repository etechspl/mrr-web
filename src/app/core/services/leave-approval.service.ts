import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';
import { URLService } from './URL/url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LeaveApprovalService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  getLeavesForApproval(data: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_leaveApproval + '/getLeaveApprovalRequest' + '?params=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
  }

  approveLeave(data: object) {
    let leaveApproveData = data;
    let filter = {
      id: data['id'],
      companyId: data['companyId']
    }

    let updateData = {};
    if (data['rl'] === 0) {
      updateData = {
        "leaveType": data['leaveType'],
        "appStatus": 'approved',
        status:true,
        "finalAppDate": new Date()
      }
    }
    let url = this._urlService.API_ENDPOINT_leaveApproval + '/update?where=' + JSON.stringify(filter);
    //this.updateLeaveInDCRMaster(leaveApproveData);
    return this.http.post(url, updateData, { headers: this.headers });
  }
  updateLeaveInDCRMaster(leaveApproveData) {
    let filter = {
      companyId: leaveApproveData['companyId'],
      submitBy: leaveApproveData['userId'],
      dcrDate:leaveApproveData['requestDate'],
      'workingStatus': 'Leave',
      "stateId" : leaveApproveData['stateId'],
      "districtId" : leaveApproveData['districtId'],
      "visitedBlock" : [], 
      "jointWork" : [], 
      "remarks" : "", 
      "timeIn" : new Date(), 
      "timeOut" : new Date(), 
      "createdAt" : new Date(), 
      "updatedAt" : new Date(), 
      "reportedFrom" : "web preeti", 
      "geoLocation" : [], 
      "dcrSubmissionDate" : new Date(), 
      "punchDate" : new Date(), 
      "callModifiedDate" :new Date(), 
      "DCRStatus" : 0, 
      "dcrVersion" : "complete", 
    }
    let url = this._urlService.API_ENDPOINT_DCRMASTER;
    return this.http.post(url, filter, { headers: this.headers })

  }
}
