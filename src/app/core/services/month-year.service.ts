import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MonthYearService {

  currentUser = JSON.parse(sessionStorage.currentUser);

  constructor() { }

  public getMonthAndCurrentYearList(fromWhichYear: number) {
    let retunObject = [];
    let yearList = [];
    let counter = 0;
    let currentYear = new Date().getFullYear();
    let currenMonth = new Date().getMonth();

    if (currenMonth == 11) {
      currentYear = currentYear + 1;
    }

    for (let i = fromWhichYear; i <= currentYear; i++) {
      yearList[counter] = i;
      counter++;
    }
    retunObject = [{
      "month": [{
        "monthValue": 1,
        "monthName": "January"
      }, {
        "monthValue": 2,
        "monthName": "February"
      }, {
        "monthValue": 3,
        "monthName": "March"
      }, {
        "monthValue": 4,
        "monthName": "April"
      }, {
        "monthValue": 5,
        "monthName": "May"
      }, {
        "monthValue": 6,
        "monthName": "June"
      }, {
        "monthValue": 7,
        "monthName": "July"
      }, {
        "monthValue": 8,
        "monthName": "August"
      }, {
        "monthValue": 9,
        "monthName": "September"
      }, {
        "monthValue": 10,
        "monthName": "October"
      }, {
        "monthValue": 11,
        "monthName": "November"
      }, {
        "monthValue": 12,
        "monthName": "December"
      }]
    }, {
      "year": yearList
    }
    ]
    return retunObject
  }

  getFinancialYear() {
    let companyStartDate = new Date(this.currentUser.company.createdAt);
    let companyStartMonth = companyStartDate.getMonth() + 1;
    let companyStartYear = companyStartDate.getFullYear();
    let currentYear = new Date().getFullYear();

    let finalObject = [];
    let view = "";
    let value = [];
    let startYear = 0;
    let endYear = 0;
    if (companyStartMonth >= 1 && companyStartMonth <= 3) {
      startYear = companyStartYear - 1;
      endYear = companyStartYear;
      view = startYear + "-" + endYear;
      value = [startYear, endYear];
      finalObject.push({
        view: view,
        value: value
      })
    } else {
      startYear = companyStartYear;
      endYear = companyStartYear + 1;
     /* view = startYear + "-" + endYear;
      value = [startYear, endYear];
      finalObject.push({
        view: view,
        value: value
      })*/
    }

    let diff = currentYear - companyStartYear;
    if (diff > 0) {
      for (let i = 0; i <= diff; i++) {
        if (i == 0) {
          startYear = companyStartYear;
          endYear = companyStartYear + 1;
        } else {
          startYear = startYear + 1;
          endYear = endYear + 1;
        }

        view = startYear + "-" + endYear;
        value = [startYear, endYear];
        finalObject.push({
          view: view,
          value: value
        })
      }
    }


    //console.log("companyStartMonth : " + companyStartMonth);
    //console.log("companyStartYear : " + companyStartYear);
    //console.log(finalObject);
    // console.log("companyStartYear : " + companyStartYear);

    return finalObject;
  }

}
