import { TestBed } from '@angular/core/testing';

import { StockiestService } from './stockiest.service';

describe('StockiestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockiestService = TestBed.get(StockiestService);
    expect(service).toBeTruthy();
  });
});
