import { Observable } from 'rxjs';
import { Injectable, ChangeDetectorRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class StockiestService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private urlService: URLService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  currentUser = JSON.parse(sessionStorage.currentUser);
  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();

  getStockiest(companyId: string, districts: any): Observable<any> {
    let url;
    var obj;
    obj = {
      filter: {
        "where": {
          "companyId": companyId,
          "districtId": { $in: districts }
        },
        "order": "providerName ASC"
      }
    }
    url = this.urlService.API_ENDPOINT_PROVIDERS + '?' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  getStockiestToBeMapped(companyId: string, districts: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "districts": districts,
      "providerType": 'Stockist'
    }
    let url = this.urlService.API_ENDPOINT_PROVIDERS + '/getProviderToBeMapped?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  mapStockiests(companyId: string, stateId: string, selectedEmployee: any, selectedStockist: any, loggedUserId: string): Observable<any> {
    var data = [];
    let stkId = selectedStockist._id;
    let stkDistrictId = selectedStockist.districtId;
    let stkBlockId = selectedStockist.blockId;
    let stkName = selectedStockist.stockiestName;
    for (let i = 0; i < selectedEmployee.length; i++) {
      let obj = {
        "companyId": companyId,
        "stkId": stkId,
        "stkStateId": stateId,
        "stkDistrictId": stkDistrictId,
        "stkBlockId": stkBlockId,
        "stkName": stkName,
        "userId": selectedEmployee[0].userId,
        "status": true,
        "createdAt": new Date(),
        "updatedAt": new Date(),
        "updatedBy": "",
        "createdBy": loggedUserId
      }
      data.push(obj);
    }
    let url = this.urlService.API_ENDPOINT_UserStockiestMapping;
    return this.http.post(url, JSON.stringify(data), { headers: this.headers });
  }

  getUserStockistMappedDetail(companyId: string, districts: any, employeeIds: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "districts": districts,
      "employeeIds": employeeIds
    }
    let url = this.urlService.API_ENDPOINT_UserStockiestMapping + '/getUserStockistDetail?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  getMappedStockist(companyId: string, userId: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "userId": userId,
      status: true
    }
    let url = this.urlService.API_ENDPOINT_UserStockiestMapping + '/getMappedStockistForFilter?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  getMappedStockists(companyId: string, userId: any): Observable<any> {
    var obj;
    obj = {
        "where": {
          companyId: companyId,
          userId: {inq:userId},
          status: true,
          appStatus: 'approved',
          providerType: "Stockist"
        }
    } 
    let url = this.urlService.API_ENDPOINT_PROVIDERS+'?filter='+JSON.stringify(obj);
    return this.http.get(url,{headers:this.headers})
  }

  getHQWiseStockist(obj: any): Observable<any> {
    var obj;
    let url = this.urlService.API_ENDPOINT_USERINFO + '/getAllStockistHqWise?params=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  deleteStockists(stockist: any): Observable<any> {
    var obj;
    obj = {
      "stockist": stockist
    }
    let url = this.urlService.API_ENDPOINT_UserStockiestMapping + '/deleteMappedStockists?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  deleteProducts(productInfo: any, formInfo: any): Observable<any> {
    var obj;
    obj = {
      "productInfo": productInfo,
      "formInfo": formInfo
    }
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/deleteSubmittedProducts?param=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  submitSSS(companyId: string, sssFormInfo: any, selectedProductInfo: any, loggedUserId: string): Observable<any> {
    var data = [];
    for (let i = 0; i < selectedProductInfo.sssSubmitFormDetail.length; i++) {
      let dataQty;
      let dataAmt;
      let totalPrimaryQty = 0;
      let totalPrimaryAmt = 0;
      let totalReturnQty = 0;
      let totalReturnAmt = 0;
      let obj1 = {};
      if (sssFormInfo.saleType === 'primarySale') {
        if (selectedProductInfo.sssSubmitFormDetail[i].splRate === undefined || selectedProductInfo.sssSubmitFormDetail[i].splRate === '') {
          dataQty = selectedProductInfo.sssSubmitFormDetail[i].primary;
          dataAmt = (selectedProductInfo.sssSubmitFormDetail[i].primary) * (selectedProductInfo.sssSubmitFormDetail[i].netRate);
          totalPrimaryQty = selectedProductInfo.sssSubmitFormDetail[i].primary;
          totalPrimaryAmt = (selectedProductInfo.sssSubmitFormDetail[i].primary) * (selectedProductInfo.sssSubmitFormDetail[i].netRate);
        } else {
          dataQty = selectedProductInfo.sssSubmitFormDetail[i].primary;
          dataAmt = (selectedProductInfo.sssSubmitFormDetail[i].primary) * (selectedProductInfo.sssSubmitFormDetail[i].splRate);
          totalPrimaryQty = selectedProductInfo.sssSubmitFormDetail[i].primary;
          totalPrimaryAmt = (selectedProductInfo.sssSubmitFormDetail[i].primary) * (selectedProductInfo.sssSubmitFormDetail[i].splRate);
        }
        obj1 = {
          "type": sssFormInfo.saleType,
          "invoiceNo": sssFormInfo.invoiceNo,
          "invoiceDate": new Date(sssFormInfo.invoiceDate),
          "userId": sssFormInfo.userId,
          "netRate": parseInt(selectedProductInfo.sssSubmitFormDetail[i].netRate),
          "splRate": selectedProductInfo.sssSubmitFormDetail[i].splRate,
          "qty": parseInt(dataQty),
          "amount": parseInt(dataAmt),
          "createdAt": new Date()
        }
      } else if (sssFormInfo.saleType === 'returnSale') {
        if (selectedProductInfo.sssSubmitFormDetail[i].splRate === undefined || selectedProductInfo.sssSubmitFormDetail[i].splRate === '') {
          dataQty = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other));
          dataAmt = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other)) * (selectedProductInfo.sssSubmitFormDetail[i].netRate);
          totalReturnQty = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other));
          totalReturnAmt = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other)) * (selectedProductInfo.sssSubmitFormDetail[i].netRate);
        } else {
          dataQty = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other));
          dataAmt = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other)) * (selectedProductInfo.sssSubmitFormDetail[i].splRate);
          totalReturnQty = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other));
          totalReturnAmt = (parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable) + parseInt(selectedProductInfo.sssSubmitFormDetail[i].other)) * (selectedProductInfo.sssSubmitFormDetail[i].splRate);
        }
        obj1 = {
          "type": sssFormInfo.saleType,
          "invoiceNo": sssFormInfo.invoiceNo,
          "invoiceDate": new Date(sssFormInfo.invoiceDate),
          "userId": sssFormInfo.userId,
          "netRate": parseInt(selectedProductInfo.sssSubmitFormDetail[i].netRate),
          "splRate": parseInt(selectedProductInfo.sssSubmitFormDetail[i].splRate),
          "qty": parseInt(dataQty),
          "breakage": parseInt(selectedProductInfo.sssSubmitFormDetail[i].breakage),
          "saleable": parseInt(selectedProductInfo.sssSubmitFormDetail[i].saleable),
          "other": parseInt(selectedProductInfo.sssSubmitFormDetail[i].other),
          "amount": parseInt(dataAmt),
          "createdAt": new Date()
        }
      }
      var responses = [];
      responses.push(obj1);
      let proInfoArr = selectedProductInfo.sssSubmitFormDetail[i].product.split(",");
      let obj2 = {
        "companyId": companyId,
        "productId": proInfoArr[0],
        "productName": proInfoArr[1],
        "productCode": proInfoArr[1],
        "partyName": sssFormInfo.stockistId.stkName,
        "partyId": sssFormInfo.stockistId.stkId,
        "partyCode": sssFormInfo.stockistId.stkId,
        "stateId": sssFormInfo.stateId,
        "districtId": sssFormInfo.districtId,
        //"blockId": sssFormInfo.districtId,
        "data": responses,
        "month": parseInt(sssFormInfo.month),
        "year": parseInt(sssFormInfo.year),
        "opening": parseInt(selectedProductInfo.sssSubmitFormDetail[i].opening),
        "closing": 0,
        "totalPrimaryQty": totalPrimaryQty,
        "totalPrimaryAmt": totalPrimaryAmt,
        "totalSaleReturnQty": totalReturnQty,
        "totalSaleReturnAmt": totalReturnAmt,
        "createdAt": new Date(),
        "updatedAt": new Date(),
        "createdBy": loggedUserId
      }
      data.push(obj2);
    }

    let status = this.createPrimaryWithValidationCheck(data);
    return status;
  }
  createPrimaryWithValidationCheck(data: any): Observable<any> {
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/createPrimaryInfoWithValidationCheck?param=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
  }

  getProductBasedOnState(companyId: string, stateId: any): Observable<any> {
    let stateArr = [];
    stateArr.push(stateId);
    var obj;
    obj = {
      "where": {
        "companyId": companyId,
        "assignedStates": {
          "inq": stateArr
        },
        status: true
      },
      "order": "productName ASC"
    }
    let url = this.urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getProductBasedOnStateForTargetModule(companyId: string, stateId: any): Observable<any> {
    //let stateArr = [];
    //stateArr.push(stateId);
    var obj;
    obj = {
      "where": {
        "companyId": companyId,
        "assignedStates": {
          "inq": stateId
        }
      },
      "order": "productName ASC",
      "limit": 50
    }
    let url = this.urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }


  setProductRateFromMaster(companyId: string, productInfo: any, stateId: string): Observable<any> {
    let proInfoArr = productInfo.model.product.split(",");
    let stateArr = [];
    stateArr.push(stateId);
    var obj;
    obj = {
      "where": {
        "_id": proInfoArr[0],
        "companyId": companyId,
        "assignedStates": {
          "inq": stateArr
        }
      }
    }
    let url = this.urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  checkProductAlraedyExistOrNot(companyId: string, productInfo: any, formInfo: any): Observable<any> {
    let proInfoArr = productInfo.model.product.split(",");
    var obj;
    obj = {
      "where": {
        "companyId": companyId,
        "productId": proInfoArr[0],
        "data": {
          elemMatch: {
            "type": formInfo.saleType,
            "userId": formInfo.userId
          }
        },
        "month": formInfo.month,
        "year": formInfo.year
      },
      "order": "productName ASC"
    }
    let url = this.urlService.API_ENDPOINT + '/PrimaryAndSalereturns?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getSubmittedSSSDetail(companyId: string, formInfo: any): Observable<any> {
    var obj;
    obj = {
      "where": {
        "companyId": companyId,
        "partyName": formInfo.stockistId.stkName,
        "data": {
          elemMatch: {
            "type": formInfo.saleType,
            "userId": formInfo.userId
          }
        },
        "month": formInfo.month,
        "year": formInfo.year
      },
      "order": "productName ASC"
    }
    let url = this.urlService.API_ENDPOINT + '/PrimaryAndSalereturns?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getSubmittedSSSDetailForEditModule(companyId: string, formInfo: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "formInfo": formInfo,
    }
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getProductInfoAsUnwindUserData?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  editSubmittedSSSDetails(companyId: string, formInfo: any, sssInfo: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "formInfo": formInfo,
      "sssInfo": sssInfo,
    }
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/editSubmittedSSSDetail?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getSubmittedSSSForSSSClosing(companyId: string, sssClosingInfo: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "sssClosingInfo": sssClosingInfo,
    }
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getSubmittedSSSDetailForSSSClosing?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  submitClosingDetails(currentUser: any, formInfo: any, sssClosingInfo: any): Observable<any> {
    var obj;
    obj = {
      "companyId": currentUser.companyId,
      "formInfo": formInfo,
      "sssClosingInfo": sssClosingInfo,
    }

    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/submitClosingDetail?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  checkForClosingSubmitted(companyId: string, formInfo: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "partyId": formInfo.stockistId.stkId,
      "month": formInfo.month,
      "year": formInfo.year
    }
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/checkClosingSubmittedOrNot?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getSubmittedSaleStateAndHQwise(companyId: string, sssInfo: any, sssType: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "sssInfo": sssInfo,
      "sssType": sssType,
      "stkAmtCal": this.currentUser.company.validation.stkAmtCal
    }

    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getSubmittedSaleStateAndHQwise?param=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getSubmittedSSSPerformanceWise(companyId: string, sssInfo: any, sssType: any): Observable<any> {
    let obj = {
      "companyId": companyId,
      "sssInfo": sssInfo,
      "sssType": sssType,
      "stkAmtCal": this.currentUser.company.validation.stkAmtCal,
      "SSSViewFormula": this.currentUser.company.SSSViewFormula
    }

    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getSubmittedSSSSalePerformanceWise';
    return this.http.post<any>(url, JSON.stringify(obj), { headers: this.headers })
  }

  getSingleUserSSS(companyId: string, sssClosingInfo: any, stateId: string): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "sssClosingInfo": sssClosingInfo,
      "stkAmtCal": this.currentUser.company.validation.stkAmtCal,
      "prodTypeStatus": this.currentUser.company.validation.prodTypeStatus,
      "stateId": stateId,
    }

    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getProductAndSSSDetailForSingleUser?param=' + JSON.stringify(obj);

    return this.http.get<any>(url, { headers: this.headers })
  }

  submitSingleUserSSSDetails(companyId: string, formInfo: any, sssClosingInfo: any, userDetails: any): Observable<any> {
    var obj;
    obj = {
      "companyId": companyId,
      "formInfo": formInfo,
      "sssClosingInfo": sssClosingInfo,
      "stateId": userDetails.stateId,
      "districtId": userDetails.districtId,
      "userId": userDetails.userId,
    }

    if (userDetails.hasOwnProperty('sssSchemeFormula')) {
      obj["sssSchemeFormula"] = userDetails.sssSchemeFormula
    }

    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/submitSingleUserSSSDetail';

    return this.http.post<any>(url, JSON.stringify(obj), { headers: this.headers });
  }


  getSingleUserSSSERP(companyId: string, sssClosingInfo: any, stateId: string): Observable<any> {
    var obj;

    obj = {
      "companyId": companyId,
      "PartyCode": sssClosingInfo.stockistId.stkCode,
      "partyId": sssClosingInfo.stockistId.stkId,
      "employeeCode": sssClosingInfo.employeeCode,
      "fromDate": sssClosingInfo.fromDate,
      "month": sssClosingInfo.month,
      "year": sssClosingInfo.year,
      "toDate": sssClosingInfo.toDate,
      "APIEndPoint": sssClosingInfo.APIEndPoint,
      //"fromData":sssClosingInfo,
      "userId": sssClosingInfo.userId,
      "rL": sssClosingInfo.rL
    }

    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getProductListForSSS?params=';
    return this.http.post<any>(url, JSON.stringify(obj), { headers: this.headers });
  }

  getSSSDump(param: any): Observable<any> {
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getPrimaryAndSceondaryHQWiseDump';
    return this.http.post<any>(url, JSON.stringify(param), { headers: this.headers });
  }

  getSSSDumpMultiMonth(param: any): Observable<any> {
    let url = this.urlService.API_ENDPOINT_PrimaryAndSaleReturn + '/getPrimaryAndSceondaryHQWiseDumpMultiMonth';
    return this.http.post<any>(url, JSON.stringify(param), { headers: this.headers });
  }

  getManagerHierarchyWithUniqueHQs(obj: any): Observable<any> {
    let url = this.urlService.API_ENDPOINT_HIERARCHY + '/getManagerHierarchyWithUniqueHQs?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers });
  }

}
