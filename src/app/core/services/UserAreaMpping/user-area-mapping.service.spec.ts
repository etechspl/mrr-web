import { TestBed } from '@angular/core/testing';

import { UserAreaMappingService } from './user-area-mapping.service';

describe('UserAreaMappingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserAreaMappingService = TestBed.get(UserAreaMappingService);
    expect(service).toBeTruthy();
  });
});
