import { Observable } from 'rxjs';
import { URLService } from './../URL/url.service';
import { AuthenticationService } from './../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserAreaMappingService {

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  createUserAreaMapping(data: Object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS;
    return this.http.post<any>(url, JSON.stringify(data), { headers: this.headers });
  }

  getMappedAreas(companyId: string, userId: string): Observable<any> {
    console.log(companyId);
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getUserMappedBlocks?companyId=' + companyId + '&userId=' + userId;
    return this.http.get<any>(url, { headers: this.headers })
  }

  getMappedAreasForApproval(userInfo: any, userIds: any, requestType: string): Observable<any> {
    let Obj = {
      "companyId": userInfo.companyId,
      "rL": userInfo.userInfo[0].rL,
      "approvalUpto": userInfo.company.validation.approvalUpto,
      "userInfo": userIds,
      requestType: requestType
    };
    console.log("Obj+++++++++++++",Obj);
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getUserMappedBlocksForApprovalDeletionRequest?params=' + JSON.stringify(Obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getMappedAreasOfEmployees(companyId: string, userIds: any): Observable<any> {
    // let obj={"where":{"companyId":"5b2e22083225df01ba7d6059","userId":{"inq":["5b33ca57f87a57439085de2a"]}}};
    let obj = {
      "where": {
        "companyId": companyId,
        "userId": { inq: userIds },
        "status": true
      },
      "fields": { areaId: true, id: false }
    }
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '?filter=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers })

  }

  checkMappingAlreadyExist(areaInfo: any, userId: string) {
    let checkObj;
    checkObj = {
      "where": {
        "companyId": areaInfo.companyId,
        "areaId": areaInfo.id,
        "userId": userId
      }
    }
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '?filter=' + JSON.stringify(checkObj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers });
  }
  

  updateAreaMappingStatus(filter, obj):Observable<any>{
    console.log(filter,'------------------}}}}}}}}}}}',obj)
    const url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/update?where='+ JSON.stringify(filter)
    return this.http.post(url,obj)
  }

  updateAreaStatusInArea(filter, obj):Observable<any>{
    const url = this._urlService.API_ENDPOINT_AREAS + '/update?where='+ JSON.stringify(filter)
    return this.http.post(url,obj)
  }

  deleteAreaMapping(delDetails: Object, companyId: string, _id: string, validationRole: number, rL: number,deleteReason:string): Observable<any> {
    delDetails['companyId'] = companyId;
    delDetails['validationRole'] = validationRole;
    delDetails['rL'] = rL;
    delDetails['updatedBy'] = _id;
    delDetails['updatedAt'] = new Date();
    delDetails['deleteReason']=deleteReason;
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/deleteAreaAndMapping?params=' + JSON.stringify(delDetails).replace(/&/g,"%26");
    return this.http.get<any>(url);
  }

  approveDeleteAreaAndMapping(approveDetails: Object, requestType: string, companyId: string, _id: string, validationRole: number, rL: number): Observable<any> {
    let obj = {
      approveDetails: approveDetails,
      companyId: companyId,
      updatedBy: _id,
      validationRole: validationRole,
      rL: rL,
      requestType: requestType
    }
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/approveAreaAndMapping?params=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url);
  }

  getProviderForApprovalDeletion(userInfo: any, userIds: any, requestType: string, proiderType: any): Observable<any> {
    let Obj = {
      "companyId": userInfo.companyId,
      "rL": userInfo.userInfo[0].rL,
      "approvalUpto": userInfo.company.validation.approvalUpto,
      "userInfo": userIds,
      requestType: requestType,
      providerType: proiderType
    };
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getProviderOnUserBasis?param=' + JSON.stringify(Obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers })
  }

  getProvidersList(companyId: string, type: string, stateIds: any, districtIds: any, areaIds: any, userId: any, providerType: any, status: any,designation:string,category:any,division:any,isDivisionExist:any): Observable<any> {
    let url: string;
    if (type === null) {
      type = "Area";
    }
    
    let obj = {
      companyId: companyId,
      type: type,
      stateId: stateIds,
      districtId: districtIds,
      areaId: areaIds,
      userId: userId,
      provoderType: providerType,
      status: status,
      designation:designation,
      category : category,
			division: division,
			isDivisionExist: isDivisionExist,
    };
   
    url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getProviderListReplica';
    url=url.replace(/[+]/g,"%2B");
   url=url.replace(/[*]/g,"%2A");
   url=url.replace(/&/g,"%26");
   url=url.replace(/[-]/g,"%2D");
   url=url.replace(/[.]/g,"%2E");
    return this.http.post(url, obj,{ headers: this.headers })
    // return this.http.post(url, updateData, { headers: this.headers });

    //  return this.http.get(url); 
  }
  getProvidersListWithPatchName(companyId: string, type: string, stateIds: any, districtIds: any, areaIds: any, userId: any, providerType: any, status: any,designation:string,category:any): Observable<any> {
    let url: string;
    if (type === null) {
      type = "Area";
    }
    
    let obj = {
      companyId: companyId,
      type: type,
      stateId: stateIds,
      districtId: districtIds,
      areaId: areaIds,
      userId: userId,
      provoderType: providerType,
      status: status,
      designation:designation,
      category : category
    };
   
    url = this._urlService.API_ENDPOINT_PROVIDERS + '/getProvidersListWithPatchName';
    url=url.replace(/[+]/g,"%2B");
   url=url.replace(/[*]/g,"%2A");
   url=url.replace(/&/g,"%26");
   url=url.replace(/[-]/g,"%2D");
   url=url.replace(/[.]/g,"%2E");
    return this.http.post(url, obj,{ headers: this.headers })
    // return this.http.post(url, updateData, { headers: this.headers });

    //  return this.http.get(url); 
  }
	rejectAreaRequest(
		approveDetails: Object,
   // areaId:Object,
		requestType: string,
		companyId: string,
		_id: string,
		validationRole: number,
		rL: number,
		rejectionMessage: string
	): Observable<any> {
		let obj = {
			approveDetails: approveDetails,
			companyId: companyId,
      areaId: approveDetails[0].areaId,
			updatedBy: _id,
			validationRole: validationRole,
			rL: rL,
			requestType: requestType,
			rejectionMessage: rejectionMessage,
      rejectedBy : _id,
      rejectionAddDate : new Date(),
      "rejectDelStatus" : false,
      "rejectAddStatus" : false
		};
		//console.log(Obj);
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getAreaRequestsEmployeewise?param=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers })
     

	}
  getEmployeewiseAreaCount(companyId: string,approvalUpto:number, loginFrom: string, requsetType: string,userId:string): Observable<any> {
    let Obj = {
      "companyId": companyId,
      "approvalUpto":approvalUpto,
      "designationLevel": loginFrom,
      "requsetType":requsetType,
      "supervisorId":[userId]
    };
    //console.log(Obj);
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getAreaRequestsEmployeewise?param=' + JSON.stringify(Obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers })
  }
  transferAreaToNewUser(companyId: string,Areaids:any,fromUserId:string,toUserId:string): Observable<any> {
   
    let AreaIds=[];
    for(var i=0;i<Areaids.length;i++){
      AreaIds.push(Areaids[i].id);
    }

    let filter = {
      companyId: companyId,
       areaId:{ inq: AreaIds},
    };
      
    let updateData={
      userId:toUserId,
      updatedAt:new Date(),
    }
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/update?where=' + JSON.stringify(filter);
    return this.http.post(url, updateData, { headers: this.headers });
  }

  //get all users area under manager hierarchy 
  getUserMappedBlocksForMobile(companyId:string,userId:string) {
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getUserMappedBlocksForMobile?companyId=' + companyId+"&userId="+userId;
    
    return this.http.get(url,{headers:this.headers});
  }

  getProviderLocationTagged(obj: any,) {
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '/getProviderLocationTagged?param=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers });
  }

  unTaggedProvider(obj: any,) {
    let url = this._urlService.API_ENDPOINT_PROVIDERS + '/unTaggedProvider?param=' + JSON.stringify(obj);
    // let url = this._urlService.API_ENDPOINT_PROVIDERS + '/update?where=' + JSON.stringify(obj);
    return this.http.get(url,{ headers: this.headers });
  }
  getLasrDCR(obj: any) {
    let url = this._urlService.API_ENDPOINT_DCRProviderVisitDetails + '?filter=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers });
  }
  getPatchDetail(obj: any) {
    let url = this._urlService.API_ENDPOINT_PatchMasters + '/getPatchDetail?param=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers });
  }
  getPatchDetailsUserWise(obj:any){
    let url = this._urlService.API_ENDPOINT_PatchMasters + '/getPatchDetailsUserWise?param=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers });
  }
  getPatchAllDetails(obj: any) {
    let url = this._urlService.API_ENDPOINT_PatchMasters + '?filter=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers });
  }
  removePatches(obj: any) {
    let url = this._urlService.API_ENDPOINT_PatchMasters + '/removePatches?param=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.get<any>(url, { headers: this.headers });
  }
  updatePatcheName(obj: any,updateData:any) {
    let url = this._urlService.API_ENDPOINT_PatchMasters + '/update?where=' + JSON.stringify(obj).replace(/&/g,"%26");
    return this.http.post(url, updateData, { headers: this.headers });
  }

  updateProductGroupNames(obj: any,updateData:any) {
         let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/update?where=' + JSON.stringify(obj).replace(/&/g,"%26");
     return this.http.post(url, updateData, { headers: this.headers });
  }
    
  getMappedAreasOfEmployeesForFileUpload(where): Observable<any> {
    let url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS + '?filter=' + JSON.stringify({where});
    return this.http.get<any>(url, { headers: this.headers })
  }

  unTaggedMultipleProviders(obj: any,) {
    let url = this._urlService.API_ENDPOINT_PROVIDERS + '/unTaggedMultipleProviders?param=' + JSON.stringify(obj);
    // let url = this._urlService.API_ENDPOINT_PROVIDERS + '/update?where=' + JSON.stringify(obj);
    return this.http.get(url,{ headers: this.headers });
  }

  updateAreadetails(where,data): Observable<any> {
    const url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS+ '/update?where='+ JSON.stringify(where);
    return this.http.post(url,data)
  }

  changeAreaStatus(object):Observable<any>{
    console.log("object: ------- ",object);
    const url = this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS+'/changeAreaStatus?param='+JSON.stringify(object);
    return this.http.get(url);
  }
}
