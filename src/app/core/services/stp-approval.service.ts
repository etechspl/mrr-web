import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';
import { URLService } from './URL/url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StpApprovalService {

  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  //companyId: string, userId: string, level: number,type:string
  getSTPForApprovalDivisionWise(data: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_STP + '/getStpApprovalRequestBasedOnDivision?params=' + JSON.stringify(data);
    return this.http.get(url, { headers: this.headers })
  }

  getSTPForApproval(data: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_STP + '/getStpApprovalRequest?params=' + JSON.stringify(data);
     return this.http.get(url, { headers: this.headers })
  }

 
  getSTPApprovalForUser(data: object): Observable<any> {
    let obj = {};
    if (data['rl'] === 0) {
      obj = {
        where: {
          companyId: data['companyId'],
          userId: data['userId'],
          appByAdm: '',
          delStatus: { neq: 'approved' },
          //appStatus:'inprocess',
          status: false
        },
        include: [{
          relation: "fromArea"
        }, {
          relation: "toArea"
        },
        //------Nipun (09-01-2020)-------
        {
          relation: "userInfo"
        }]
        //------end (09-01-2020)-------
      }
    } else {
      obj = {
        where: {
          companyId: data['companyId'],
          userId: data['userId'],
          appByMgr: '',
          appByAdm: '',
          delStatus: { neq: 'approved' },
          status: false
        },
        include: [
          {
          relation: "fromArea"
        }, {
          relation: "toArea"
        },
        {
          relation: "userInfo"
        }
      ]
      }
    }


    let url = this._urlService.API_ENDPOINT_STP + '?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  updateRequestSTP(data: object) {
    let filter = {
      id: { inq: data['ids'] }
    }

    let updateData = {};
    if (data['rl'] === 0) {
      updateData = {
        "appByAdm": data['userId'],
        "appAdmDate": new Date()
      }
    } else {
      updateData = {
        "appByMgr": data['userId'],
        "appMgrDate": new Date()
      }
    }
    let url = this._urlService.API_ENDPOINT_STP + '/update?where=' + JSON.stringify(filter);
    return this.http.post(url, updateData, { headers: this.headers });
  }

  approveSTP(data: object) {
    let filter = {
      id: data['id']
    }

    

    let updateData = {};
    if (data['rl'] === 0) {
      updateData = {
        "appByAdm": data['userId'],
        "distance": data['distance'],
        "type": data['type'],
        "frcId": data['frcId'],
        "status": true,
        "appStatus": 'approved',
        "finalAppDate": new Date()

      }
    } else {
      updateData = {
        "appByMgr": data['userId'],
        "distance": data['distance'],
        "type": data['type'],
        "frcId": data['frcId'],
        "status": data['status'],
        "appStatus": "inprocess",
        "mgrAppDate": new Date()
      }
    }
   
    console.log("Update ",updateData);

    let url = this._urlService.API_ENDPOINT_STP + '/update?where=' + JSON.stringify(filter);
    return this.http.post(url, updateData, { headers: this.headers });
  }

  deleteSTPApprovalReq(obj: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_STP + '/removestpApproval?params=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });
  }
}