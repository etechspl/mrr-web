import { TestBed } from '@angular/core/testing';

import { DataTransferHistoryService } from './data-transfer-history.service';

describe('DataTransferHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataTransferHistoryService = TestBed.get(DataTransferHistoryService);
    expect(service).toBeTruthy();
  });
});
