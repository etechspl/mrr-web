import { TestBed, inject } from '@angular/core/testing';

import { FixExpenseService } from './fix-expense.service';

describe('FixExpenseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FixExpenseService]
    });
  });

  it('should be created', inject([FixExpenseService], (service: FixExpenseService) => {
    expect(service).toBeTruthy();
  }));
});
