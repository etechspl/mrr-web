import { Injectable } from '@angular/core';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FixExpenseService {

  constructor(private _httpClient:HttpClient,private _authService:AuthenticationService,private _urlService:URLService) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this._authService.getAccessTokenNew()
  })

  getFixExpense(params:any):Observable<any>{
   console.log("params in fix--",params);
   

    let url = this._urlService.API_ENDPOINT_FIX_EXPENSE + '/getFixExpense?params=' + JSON.stringify(params);
    return this._httpClient.get(url,{headers:this.headers});
  
  }
}
