import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from 'ngx-auth';
import { AuthenticationService } from '../auth/authentication.service';
import { State } from '../../content/pages/_core/models/state.model';
import { URLService } from './URL/url.service';

@Injectable()
export class StateService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }

  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();

  //Get State List Based on Company.

  getStates(companyId: string): Observable<any> {
    let obj = {
      companyId: companyId,
      isDivisionExist: false
    };
     
    let url = this._urlService.API_ENDPOINT_STATE + '/getState?parameters=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }

  getUniqueStateOnManager(companyId: string, managerId: string): Observable<any> {
    let obj = {
      companyId: companyId,
      supervisorId: managerId,
      isDivisionExist: false
    };
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getUniqueStateOnManager?parameters=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });

  }

  getUniqueHqOnManager(companyId: string, managerId: string, stateIds: any, isContainStateFilterValue: boolean): Observable<any> {
    let obj = {}

    if (isContainStateFilterValue == false) {
      obj = {
        companyId: companyId,
        supervisorId: managerId,
        stateIds: [],
        isDivisionExist: false,
        isContainStateFilter: isContainStateFilterValue
      };

    } else if (isContainStateFilterValue == true) {
      if (typeof (stateIds) == "string") {
        obj = {
          companyId: companyId,
          supervisorId: managerId,
          stateIds: [stateIds],
          isDivisionExist: false,
          isContainStateFilter: isContainStateFilterValue
        };
      } else {

        obj = {
          companyId: companyId,
          supervisorId: managerId,
          stateIds: stateIds,
          isDivisionExist: false,
          isContainStateFilter: isContainStateFilterValue

        };
      }
    }


    let url = this._urlService.API_ENDPOINT_USERINFO + '/getUniqueHeadquarterOnManager?parameters=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });

  }


  getOnlyFirstState(companyId: string): Observable<any> {
    let obj = {
      where: {
        assignedTo: companyId
      }
    }
    let url = this._urlService.API_ENDPOINT_STATE + '/findOne?filter=' + JSON.stringify(obj) + this.acessToken;
    return this.http.get(url)

  }


  //-------------------------------PK(2019-06-2019)---------------------------------------------


  getStatesCode(companyId: string,stateId:string): Observable<any> {
    let obj = {
    where: {
    assignedTo: companyId,
    id:stateId
    }
    }
    
    let url = this._urlService.API_ENDPOINT_STATE + '?filter=' + JSON.stringify(obj);
    
    return this.http.get(url, { headers: this.headers });
    }

    
  getStatesBasedOnDivision(object: any): Observable<any> {
    let obj = {
      companyId: object.companyId,
      isDivisionExist: object.isDivisionExist,
      division: object.division
    };
    let url = this._urlService.API_ENDPOINT_STATE + '/getState?parameters=' + JSON.stringify(obj);
    
    return this.http.get(url, { headers: this.headers })
  }

  getStatesOnDivision(object: any): Observable<any> {
    let obj = {
      companyId: object.companyId,
      isDivisionExist: object.isDivisionExist,
      divisionId: object.divisionId
    };
    let url = this._urlService.API_ENDPOINT_STATE + '/getState?parameters=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }


  getUniqueStateOnManagerBasedOnDivision(object: any): Observable<any> {
    let obj = {
      companyId: object.companyId,
      isDivisionExist: object.isDivisionExist,
      division: object.division,
      supervisorId: object.supervisorId
    };
    let url = this._urlService.API_ENDPOINT_USERINFO + '/getUniqueStateOnManager?parameters=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });

  }
  //------------------------------------END----------------------------------------------------------------
  // Check Exist State Status - Anjana Rana for Data Upload
  checkStateStatus(param: any) {
    var obj = {
      "where": param
    }
    let url = this._urlService.API_ENDPOINT_STATE + '?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers });
  }

  //-------------------Praveen Kumar(27-12-2019)--------------------------
  getStateDetail(whereObject: object): Observable<any> {
    let url = this._urlService.API_ENDPOINT_STATE + '?filter=' + JSON.stringify(whereObject);
    return this.http.get(url, { headers: this.headers });
  }
  updateStateDetail(whereObject: object, dataToBeUpdate: object): Observable<any> {
    const url = this._urlService.API_ENDPOINT_STATE + "/update?where=" + JSON.stringify(whereObject);
    return this.http.post(url, dataToBeUpdate, { headers: this.headers });
  }
  //---------------------------END----------------------------------------


  //------------------------ERP STATES-------------------------------------
  getERPStateDetail(whereObject: object): Observable<any> {
    const header = new HttpHeaders({
     'Content-Type': 'application/json',
      // 'Access-Control-Allow-Origin': '*',
      //'http://103.11.85.61/PharmaMRApi/api/'
      //'Authorization': this.authservice.getAccessTokenNew()


      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
     // 'Content-Type': 'application/json; charset=utf-8'
    });
    //let url = this._urlService.ERP_API_ENDPOINT_STATE + '?filter=' + JSON.stringify(whereObject);
    let url = this._urlService.ERP_API_ENDPOINT_STATE;
    return this.http.post(url, whereObject, { headers: header });
    
  }
  // --------------------nipun (11-03-2020)--
  assignDivisionIfNotAssigned(query, ids): Observable<any>{
    let data = {
      assignedDivision: ids
    };
    return this.http.post(this._urlService.API_ENDPOINT_STATE+'/update?where='+JSON.stringify(query), data);
  }
  // --------------------nipun (11-03-2020)--
}



