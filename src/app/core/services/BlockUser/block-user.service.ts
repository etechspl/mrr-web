import { Injectable } from '@angular/core';
import { UserLogin } from '../../../content/pages/_core/models/userlogin.model';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLService } from '../URL/url.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class BlockUserService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  constructor(private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService) { }

  createBlockUser(createObject: object): Observable<any> {
    const url = this._urlService.API_ENDPOINT_BlockUser;
    return this.http.post(url, createObject, { headers: this.headers })
  }
  updateBlockUserInfo(whereObject: object, updateObject: object): Observable<any> {
    console.log("updateObject++++++++++",updateObject);
    const url = this._urlService.API_ENDPOINT_BlockUser + '/update?where=' + JSON.stringify(whereObject);
    return this.http.post(url, updateObject, { headers: this.headers });
  }
  getBlockUserDetails(where: Object) {
    let url = this._urlService.API_ENDPOINT_BlockUser + '?filter=' + JSON.stringify(where);
    return this.http.get(url, { headers: this.headers })
  }
}
