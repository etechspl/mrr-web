import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DoctorChemistService {

  constructor(
    private authservice: AuthenticationService,
    private _urlService: URLService,
    private http: HttpClient,
  ) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });

  addDoctorChemistPair(data): Observable<any>{
    return this.http.post(this._urlService.API_ENDPOINT_DOCTOR_CHEMIST_LINKING, data);
  }
  
  getDoctorChemistPair(userId, companyId):Observable<any> {
    let query = {
      where: { 
        status: true,
        userId: userId,
        companyId: companyId
      }
    }
    return this.http.get(this._urlService.API_ENDPOINT_DOCTOR_CHEMIST_LINKING+'?filter='+JSON.stringify(query) );

  }

  getDoctorwiseChemistPair(providerId, companyId):Observable<any> {
    let query = {
      where: { 
        status: true,
        providerId: providerId,
        companyId: companyId
      }
    }
    return this.http.get(this._urlService.API_ENDPOINT_DOCTOR_CHEMIST_LINKING+'?filter='+JSON.stringify(query) );

  }

  deleteDoctorChemistPair(id): Observable<any>{
    return this.http.delete(this._urlService.API_ENDPOINT_DOCTOR_CHEMIST_LINKING+'/'+id);
    // return null;
  }
}
