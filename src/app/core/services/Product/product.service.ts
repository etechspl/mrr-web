import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';
import { Product } from '../../../content/pages/_core/models/productCreation';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  dataChange: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient, private authservice: AuthenticationService, private _urlService: URLService) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  acessToken = "&access_token=" + this.authservice.getAccessTokenNew();
  // ---------------product creation method by preeti arora 30-11-2018.-----------

  createProduct(productCreationForm: Object, companyId: string) {
    productCreationForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Products + '/createProduct?params=' + JSON.stringify(productCreationForm);
    return this.http.post(url, { eaders: this.headers })
  } h

  //-------------------------------------------------------//
  //------product view by preeti arora-4-12-2018----------------------//

  getProducts(productForm: Object, companyId: string): Observable<any> {
    productForm['companyId'] = companyId;

    let url = this._urlService.API_ENDPOINT_Products + '/getProductDetail?params=' + JSON.stringify(productForm);

    return this.http.get<Product>(url);
  }
  //-------------------------------------------------------------------//
  //----------------delete selected product by preeti arora 11-12-2018---//

  deleteProduct(productForm: Object, companyId: string) {
    productForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Products + '/deleteProduct?params=' + JSON.stringify(productForm);
    return this.http.post(url, { headers: this.headers })

  }
  //---------------------------------------------------------//
  //----------------edit product by preeti 14-12-2018-------------//
  getEditedData(productEditForm: object, companyId: string) {
    productEditForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Products + '/editProduct?params=' + JSON.stringify(productEditForm);
    return this.http.post(url, { headers: this.headers })
  }

  //-------------------------------------------------------//
  getFocusProductBasedOnProvider(companyId: string, productIds: any) {
    let filter = {
      where: {
        id: { inq: productIds[0].focusProduct },
        companyId: companyId,
        status: true,
      },
      fields: {
        id: true,
        productName: true,
      }
    }
    let url = this._urlService.API_ENDPOINT_Products + '?filter=' + JSON.stringify(filter);
    return this.http.get<any>(url, { headers: this.headers })
  }

  getProductDetailOnStateBasis(stateId: string, companyId: string): Observable<any> {

    let url = this._urlService.API_ENDPOINT_Products + '/getProductDetailOnStateBasis?params=' + JSON.stringify({
      "assignedStates": stateId,
      "companyId": companyId,
      "status": true
    });

    return this.http.get<any>(url, { headers: this.headers })
  }

  getProductList(stateId: string, companyId: string): Observable<any> {
    var obj = {
      "where": {
        "assignedStates": stateId,
        "companyId": companyId,
        "status": true
      },
      "order": "productName ASC"

    }
    let url = this._urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }
  getCompanyLables(companyId: string): Observable<any> {

    var obj = {
      "where": {
        "companyId": companyId,
      }
    }
    let url = this._urlService.API_ENDPOINT_LABELS + '?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })

  }
  getProductInfoOnProductIdBasis(productId: string) {
    let filter = {
      where: {
        id: productId,
      },
      fields: {
        id: true,
        productName: true,
        ptr: true
      }
    }
    let url = this._urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(filter);
    return this.http.get<any>(url, { headers: this.headers })
  }
  checkProductExistStatus(productInfo: object) {
    let filter = {
      where: productInfo
    }
    let url = this._urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(filter);
    return this.http.get<any>(url, { headers: this.headers })

  }

  // by nipun 
  getAllProducts(query): Observable<any> {
    return this.http.get(this._urlService.API_ENDPOINT_Products + '?filter=' + JSON.stringify(query));
  }



  getProductsDataForGroup(object: any): Observable<any> {
    var obj = {};
    if (object.IS_DIVISION_EXIST == true) {
      obj = {
        "where": {
          "companyId": object.companyId,
          "assignedDivision": { inq: object.divisionId },
          "status": true
        },
        fields: {
          id: true,
          productName: true,
        },
        "order": "productName ASC"
      }
    } else {
      obj = {
        "where": {
          "companyId": object.companyId,
          "status": true
        }, fields: {
          id: true,
          productName: true,
        },
        "order": "productName ASC"
      }
    }

    let url = this._urlService.API_ENDPOINT_Products + '?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })

  }


  createProductGroups(ProductgroupForm: Object, companyId: string) {
    ProductgroupForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/createAssignProductGroups?params=' + JSON.stringify(ProductgroupForm);
    return this.http.post(url, { headers: this.headers })
  }

  ProductGroupDetails(companyId: string): Observable<any> {
    var obj = {
      "where": {
        "companyId": companyId
      }
    }
    return this.http.get(this._urlService.API_ENDPOINT_Assignproducttogroup + '?filter=' + JSON.stringify(obj));
  }

  // ProductGroupName(companyId: string,groupId : string): Observable<any> {
  //   var obj = {
  //     "where": {
  //       "companyId": companyId,
  //       "id":groupId,
  //       "status":true 
  //     }
  //   }
  //   return this.http.get(this._urlService.API_ENDPOINT_ProductGroups+'?filter='+JSON.stringify(obj));
  // }

  assignProductToGroup(assignProductForm: Object, companyId: string) {
    assignProductForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/assignProduct?params=' + JSON.stringify(assignProductForm);
    return this.http.post(url, { headers: this.headers })
  }


  getProductGroup(companyId: string, isDivisionExist: any, obj: any): Observable<any> {
    var objData = {
      "companyId": companyId,
      "stateIds": obj.stateInfo,
      "divisionId": obj.divisionId,
      "isDivisionExist": isDivisionExist
    }
    let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/getProductGroupBasedOnStates?params=' + JSON.stringify(objData);
    return this.http.get<any>(url, { headers: this.headers })
  }


  getProductDetails(companyId: string, ProductgroupForm: Object): Observable<any> {
    ProductgroupForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/getAssignedProductDetails?params=' + JSON.stringify(ProductgroupForm);
    return this.http.post(url, { headers: this.headers })
  }

  removeProductFromGroup(EditAssignedProduct: Object) {
    let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/removeProductFromGroup?params=' + JSON.stringify(EditAssignedProduct);
    return this.http.post(url, { headers: this.headers })
  }

  deleteProductGroup(obj: any, companyId: string): Observable<any> {
    obj['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/deleteProductGroup?params=' + JSON.stringify(obj);
    return this.http.post(url, { headers: this.headers })
  }
  getProductsForEditForm(obj: any, companyId: string) {
    obj['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_Products + '/getProductsForEditForm?params=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }

  EditProductGroups(EditAssignedProduct: Object, companyId: string, data: Object) {
    EditAssignedProduct['companyId'] = companyId;
    EditAssignedProduct['data'] = data;
    let url = this._urlService.API_ENDPOINT_Assignproducttogroup + '/editProductGroup?params=' + JSON.stringify(EditAssignedProduct);
    return this.http.post(url, { headers: this.headers })
  }


  createProductSubGroups(ProductgroupForm: Object, companyId: string) {
    ProductgroupForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_ProductSubGroup + '/createAssignProductGroups?params=' + JSON.stringify(ProductgroupForm);
    return this.http.post(url, { headers: this.headers })
  }

  getProductSubGroupDetails(companyId: string, ProductgroupForm: Object): Observable<any> {
    ProductgroupForm['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_ProductSubGroup + '/getProductSubGroupDetails?params=' + JSON.stringify(ProductgroupForm);
    return this.http.post(url, { headers: this.headers })
  }



  deleteProductSubGroup(obj: any, companyId: string): Observable<any> {
    obj['companyId'] = companyId;
    let url = this._urlService.API_ENDPOINT_ProductSubGroup + '/deleteProductSubGroup?params=' + JSON.stringify(obj);
    return this.http.post(url, { headers: this.headers })
  }

  removeProductFromSubGroup(EditProductSubGroup: Object) {
    let url = this._urlService.API_ENDPOINT_ProductSubGroup + '/removeProductFromSubGroup?params=' + JSON.stringify(EditProductSubGroup);
    return this.http.post(url, { headers: this.headers })
  }

  updateProductSubGroupNames(obj: any, updateData: any) {
    let url = this._urlService.API_ENDPOINT_ProductSubGroup + '/update?where=' + JSON.stringify(obj).replace(/&/g, "%26");
    return this.http.post(url, updateData, { headers: this.headers });
  }


  EditProductSubGroups(EditProductSubGroup: Object, companyId: string, data: Object) {
    EditProductSubGroup['companyId'] = companyId;
    EditProductSubGroup['data'] = data;
    let url = this._urlService.API_ENDPOINT_ProductSubGroup + '/editProductSubGroup?params=' + JSON.stringify(EditProductSubGroup);
    return this.http.post(url, { headers: this.headers })
  }


  getProductsBasedOnDivision(companyId: string, divisionIds: any): Observable<any> {
    var obj = {
      "where": {
        "assignedDivision": { inq: divisionIds },
        "companyId": companyId,
        "status": true
      },
      "order": "productName ASC"

    }
    let url = this._urlService.API_ENDPOINT + '/Products?filter=' + JSON.stringify(obj);
    return this.http.get<any>(url, { headers: this.headers })
  }



}
