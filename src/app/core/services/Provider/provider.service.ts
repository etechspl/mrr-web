import { Observable } from "rxjs";
import { AuthenticationService } from "./../../auth/authentication.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { URLService } from "./../URL/url.service";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";

@Injectable({
	providedIn: "root",
})
export class ProviderService {
	constructor(
		private _urlService: URLService,
		private http: HttpClient,
		private authservice: AuthenticationService
	) {}
	headers = new HttpHeaders({
		"Content-Type": "application/json; charset=utf-8",
		Authorization: this.authservice.getAccessTokenNew(),
	});

	serverUrl = this._urlService.API_ENDPOINT;
	acessToken = "&access_token=" + this.authservice.getAccessTokenNew();
	//commit this method because this method is re create under user-area-mapping service
	/*getProvidersList(companyId: string, type: string, stateIds: any, districtIds: any, areaIds: any, userId: any, providerType: any, status: any): Observable<any> {
    let url, getMappedBlockUrl;
    let obj = {
      companyId:companyId,
      type:type,
      stateId:stateIds,
      districtId:districtIds,
      areaId:areaIds,
      userId:userId,
      provoderType:providerType,
      status:status
    };

    if (type == "State") {
      obj = {
        "companyId": companyId,
        "stateId": { inq: stateIds },
        "providerType": { inq: providerType },
        "status": { inq: status },
        "type": type
      }
    } else if (type == "Headquarter") {
      obj = {
        "companyId": companyId,
        "districtId": { inq: districtIds },
        "providerType": { inq: providerType },
        "status": { inq: status },
        "type": type
      }
    } else if (type == "Area" || type == "Employee Wise") {

      obj = {

        "companyId": companyId,
        "blockId": { inq: areaIds },
        "providerType": { inq: providerType },
        "status": { inq: status },
        "type": type
      }


    }

    url = this.serverUrl + '/Providers?filter=' + JSON.stringify(obj);
    return this.http.get(url, { headers: this.headers })
  }*/

	getProviderVisitAnalysis(
		companyId: string,
		userId: string,
		type: any,
		fiscalType: string,
		yearOrFinancialYear: any,
		designationLevel: number,
		category: string,
		team: string
	): Observable<any> {
		let obj2 = {
			companyId: companyId,
			userId: userId,
			type: type,
			fiscalYear: fiscalType,
			year: yearOrFinancialYear,
			designationLevel: designationLevel,
			category: category,
			team:team
		};

		console.log("obj2",obj2);

		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/providerVisitAnalysis";

		return this.http.post(url,obj2, { headers: this.headers });
	}

	approveDeleteProviders(
		approveDetails: Object,
		requestType: string,
		companyId: string,
		_id: string,
		validationRole: number,
		rL: number
	): Observable<any> {
		let obj = {
			approveDetails: approveDetails,
			companyId: companyId,
			updatedBy: _id,
			validationRole: validationRole,
			rL: rL,
			requestType: requestType,
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/approveAndDeleteProviderRequestReplica";
		return this.http.post(url, obj, { headers: this.headers });

		// let url = this._urlService.API_ENDPOINT_PROVIDERS + '/approveAndDeleteProviderRequest?params='+ JSON.stringify(obj).replace(/&/g, "%26");
		// return this.http.get(url,{headers:this.headers});
	}


	rejectProviderRequest(
		approveDetails: Object,
		requestType: string,
		companyId: string,
		_id: string,
		validationRole: number,
		rL: number,
		rejectionMessage: string
	): Observable<any> {
		let obj = {
			approveDetails: approveDetails,
			companyId: companyId,
			updatedBy: _id,
			validationRole: validationRole,
			rL: rL,
			requestType: requestType,
			rejectionMessage: rejectionMessage,
		};
		return this.http.post(
			this._urlService.API_ENDPOINT +
				"/Providers/rejectApproveAndDeleteProviderRequest",
			obj
		);
	}

	apprAndDelAreaDocVenCount(
		companyId: string,
		validationRole: number,
		rL: number,
		userId: string
	): Observable<any> {
		let obj = {
			companyId: companyId,
			validationRole: validationRole,
			rL: rL,
			designationLevel: rL,
			supervisorId: [userId],
		};
		let url = this._urlService.API_ENDPOINT_PROVIDERS +
			"/apprAndDelAreaDocVenCount?param=" +
			JSON.stringify(obj).replace(/&/g, "%26");
		return this.http.get<any>(url);
	}

	getEmployeewiseProviderCount(
		companyId: string,
		approvalUpto: number,
		loginFrom: string,
		requsetType: string,
		providerType: any,
		userId: string
	): Observable<any> {
		let Obj = {
			companyId: companyId,
			approvalUpto: approvalUpto,
			designationLevel: loginFrom,
			requsetType: requsetType,
			providerType: providerType,
			supervisorId: [userId],
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getDocVenRequestsEmployeewise?param=" +
			JSON.stringify(Obj).replace(/&/g, "%26");
		return this.http.get<any>(url, { headers: this.headers });
	}

	getMEFProviderList(companyId: string, blockId: any) {
		let filter = {
			where: {
				companyId: companyId,
				blockId: { inq: blockId },
				category: "MEF",
				appStatus: "approved",
				status: true,
			},
			fields: {
				id: true,
				providerName: true,
			},
			order: "providerName ASC",
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(filter);
		return this.http.get<any>(url, { headers: this.headers });
	}

	getFocusProductList(companyId: string, providerId: string) {
		let filter = {
			where: {
				id: providerId,
				companyId: companyId,
				category: "MEF",
				appStatus: "approved",
				status: true,
			},
			fields: {
				focusProduct: true,
				category: true,
			},
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(filter);
		return this.http.get<any>(url, { headers: this.headers });
	}
	getProviderListBasedOnArea(
		companyId: string,
		areaIds: any,
		providerType: any
	) {
		var checkObj = {};
		checkObj = {
			companyId: companyId,
			blockId: areaIds,
			providerType: providerType,
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getProviderListBasedOnAreaInDCR?param=" +
			JSON.stringify(checkObj);
		return this.http.get<any[]>(url, { headers: this.headers });
	}

	getProvidersBlockId(providerId: string) {
		let filter = {
			where: {
				id: providerId,
			},
			fields: {
				blockId: true,
			},
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(filter);
		return this.http.get(url, { headers: this.headers });
	}
	getSponserDoctorsROI(params: object) {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getSponserDoctorsRoi?param=" +
			JSON.stringify(params);
		return this.http.get<any[]>(url, { headers: this.headers });
	}

	providerCreation(resArrObj: any) {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/providerCreation?param=" +
			JSON.stringify(resArrObj);
		url = url.replace(/[+]/g, "%2B");
		url = url.replace(/[*]/g, "%2A");
		url = url.replace(/&/g, "%26");
		url = url.replace(/[-]/g, "%2D");
		url = url.replace(/[.]/g, "%2E");
		url = url.replace(/[#]/g, "%23");
		url = url.replace(/[,]/g, "%2C");
		url = url.replace(/[']/g, "%27");
		url = url.replace(/[(]/g, "%28");
		url = url.replace(/[)]/g, "%29");
		url = url.replace(/\\/g, "%5C");
		
		return this.http.get(url, { headers: this.headers });
	}

	getProviderDetails(whereObj: object) {
console.log("whereObj",whereObj)
		

		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getProviderDetails?param=" +
			JSON.stringify(whereObj);
			console.log("url",url)
		return this.http.get<any>(url, { headers: this.headers });

	}

	approveDeleteProvidersForMrMgr(
		approveDetails: any,
		requestType: string,
		companyId: string,
		_id: string,
		validationRole: number,
		rL: number,
		deleteReason: string
	): Observable<any> {
		let obj = {
			approveDetails: approveDetails,
			companyId: companyId,
			updatedBy: _id,
			validationRole: validationRole,
			rL: rL,
			requestType: requestType,
			deleteReason: deleteReason,
		};
	let url =`${this._urlService.API_ENDPOINT_PROVIDERS}/approveAndDeleteProviderForMrMgrHimsef`;
// this._urlService.API_ENDPOINT_PROVIDERS +"/approveAndDeleteProviderForMrMgrHimsef?params=" +JSON.stringify(obj).replace(/&/g, "%26");


return this.http.post(url,obj);
	}

	getProviderListForSelectedBlock(companyId: string, blockId: any) {
		let filter = {
			where: {
				companyId: companyId,
				blockId: { inq: blockId },
			},
			order: "providerName ASC",
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(filter);
		return this.http.get<any>(url, { headers: this.headers });
	}

	setProviderDetailForEditInfo(providerId: string): Observable<any> {
		var obj = {
			where: {
				id: providerId,
			},
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(obj);
		return this.http.get<any>(url, { headers: this.headers });
	}

	modifyProvider(object: any): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/modifyProvider?param=" +
			JSON.stringify(object);
		url = url.replace(/[+]/g, "%2B");
		url = url.replace(/[*]/g, "%2A");
		url = url.replace(/&/g, "%26");
		url = url.replace(/[-]/g, "%2D");
		url = url.replace(/[.]/g, "%2E");
		url = url.replace(/[#]/g, "%23");
		url = url.replace(/[,]/g, "%2C");
		url = url.replace(/[']/g, "%27");
		url = url.replace(/[(]/g, "%28");
		url = url.replace(/[)]/g, "%29");
		url = url.replace(/\\/g, "%5C");
		return this.http.get(url, { headers: this.headers });
	}
	// anjana rana- 09-07-2019 for data Uploading module
	checkProviderExistStatus(params: any) {
		let filter = {
			where: {
				companyId: params.companyId,
				blockId: params.blockId,
				providerName: params.providerName,
				providerType: params.providerType,
				status: true,
			},
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(filter);
		return this.http.get<any>(url, { headers: this.headers });
	}
	//
	uploadProviderData(data: Object): Observable<any> {
		let url = this._urlService.API_ENDPOINT_PROVIDERS;
		return this.http.post<any>(url, JSON.stringify(data), {
			headers: this.headers,
		});
	}

	//-------------------------Preeti(01-08-2019 03:22PM)------------------------
	transferProviderToNewUser(
		companyId: string,
		NewuserAreaid: any,
		oldUserProvidersId: any,
		fromUserId: string,
		toUserId: string
	): Observable<any> {
		let oldUserProvidersIds = [];

		for (var i = 0; i < oldUserProvidersId.length; i++) {
			oldUserProvidersIds.push(oldUserProvidersId[i].providerId);
		}

		let filter = {
			companyId: companyId,
			_id: { inq: oldUserProvidersIds },
			userId: fromUserId,
		};
		let updateData = {
			blockId: NewuserAreaid,
			userId: toUserId,
			updatedAt: new Date(),
		};

		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/update?where=" +
			JSON.stringify(filter);
		return this.http.post(url, updateData, { headers: this.headers });
	}

	updateUserIdInProviders(
		companyId: string,
		Areaids: any,
		fromUserId: string,
		toUserId: string
	): Observable<any> {
		let AreaIds = [];
		for (var i = 0; i < Areaids.length; i++) {
			AreaIds.push(Areaids[i].id);
		}

		let filter = {
			companyId: companyId,
			blockId: { inq: AreaIds },
			userId: fromUserId,
		};

		let updateData = {
			userId: toUserId,
			updatedAt: new Date(),
		};
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/update?where=" +
			JSON.stringify(filter);
		return this.http.post(url, updateData, { headers: this.headers });
	}
	//---------------------------------END--------------------------------------

	//----------------__Preeti------------------------------
	getProviderCategory(filter: any) {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(filter);
		return this.http.get(url, { headers: this.headers });
	}
	getProductDetails(filter: any) {
		let url =
			this._urlService.API_ENDPOINT_Products +
			"?filter=" +
			JSON.stringify(filter);
		return this.http.get(url, { headers: this.headers });
	}
	//-------------------END----------------------------------

	getDoctorDisparityDetails(
		companyId: string,
		userId: string,
		type: any,
		fiscalType: string,
		yearOrFinancialYear: any,
		designationLevel: number,
		category: any,
		fromDate: any,
		toDate: any
	): Observable<any> {
		let obj2 = {
			companyId: companyId,
			userId: userId,
			type: type,
			fiscalYear: fiscalType,
			designationLevel: designationLevel,
			category: category,
			fromDate: fromDate,
			toDate: toDate,
		};

console.log("obj2",obj2);

		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getdoctorDisparity?param=" +
			JSON.stringify(obj2).replace(/&/g, "%26");
		return this.http.get(url, { headers: this.headers });
	}
	// *********************  SpecialtyWiseDoctorCountComponent
	getSpecialityWiseDoctorCount(param: any): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getProviderCountOnTypeBasis?param=" +
			JSON.stringify(param).replace(/&/g, "%26");
		return this.http.get(url, { headers: this.headers });
	}
	///update  by umesh 21-12-2019
	getProviderUpdateDetail(where: object, data: object) {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/update?where=" +
			JSON.stringify(where);
		return this.http.post(url, data, { headers: this.headers });
	}
	//END
	//make by umesh on 16-12-2019
	getProviderDetail(param: any): Observable<any> {
		console.log('service',param)
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"?filter=" +
			JSON.stringify(param);
		return this.http.get(url, { headers: this.headers });
	}

	//------------Preeti Arora 1-02-2020-----------------
	getFieldForceStatus(params) {
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				fromDate: params.fromDate,
				toDate: params.toDate,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getFieldForceStatus?params=" +
			JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}
	//-------------End------------------------------------

	//-----------------------------nipun (29-01-2020)
	getProvidersList(object: any): Observable<any> {
		return this.http.get(
			this._urlService.API_ENDPOINT_PROVIDERS +
				"?filter=" +
				JSON.stringify(object)
		);
	}

	getAreaIdsAsPerEmployeeId(obj: any): Observable<any> {
		return this.http.get(
			this._urlService.API_ENDPOINT_USER_AREA_MAPPINGS +
				"?filter=" +
				JSON.stringify(obj)
		);
	}
	//-----------------------------nipun (29-01-2020)

	//-----------------------------nipun (03-02-2020)
	updateDoctorVendorLinking(query: any, data: any): Observable<any> {
		return this.http.post(
			this._urlService.API_ENDPOINT_DOCTORVENDORLINKING +
				"/upsertWithWhere?where=" +
				JSON.stringify(query),
			data,
			{ headers: this.headers }
		);
	}
	//-----------------------------nipun (03-02-2020)

	//-----------------------------nipun (04-02-2020)
	getAssignedVendors(query): Observable<any> {
		return this.http.get(
			this._urlService.API_ENDPOINT_DOCTORVENDORLINKING +
				"?where=" +
				JSON.stringify(query),
			{ headers: this.headers }
		);
	}
	//-----------------------------nipun (04-02-2020)

	//provider count api done by ravindra 18-06-2020
	getCrmProvidersCount(query): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/count?where=" +
			JSON.stringify(query);
		return this.http.get(url, { headers: this.headers });
	}

	updateTaggedLocation(where: any, data: any): Observable<any> {
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/update?where=" +
			JSON.stringify(where);
		return this.http.post(url, data, { headers: this.headers });
	}


	getMasterData(params: any): Observable<any> {
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				division: params.division,
				status:params.status,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				status:params.status,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				status:params.status,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}
		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getMasterData?params=" +
			JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}

	getStationDetails(params: any): Observable<any> {
		var obj = {};
		if (params.type == "State") {
			obj = {
				companyId: params.companyId,
				stateIds: params.stateIds,
				type: params.type,
				division: params.division,
				status:params.status,
				isDivisionExist: params.isDivisionExist,
			};
		} else if (params.type == "Headquarter") {
			obj = {
				companyId: params.companyId,
				districtIds: params.districtIds,
				type: params.type,
				status:params.status,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		} else {
			obj = {
				companyId: params.companyId,
				userIds: params.employeeId,
				type: params.type,
				status:params.status,
				division: params.division,
				isDivisionExist: params.isDivisionExist,
			};
		}


		let url =
			this._urlService.API_ENDPOINT_PROVIDERS +
			"/getStationData?params=" +
			JSON.stringify(obj);
		return this.http.get(url, { headers: this.headers });
	}

	createPatch(params:any): Observable<any> {
		let url =this._urlService.API_ENDPOINT_PatchMasters +"/createPatch";
		return this.http.post(url, params, { headers: this.headers });
	}
	updatePatch(params:any): Observable<any> {
		let url =this._urlService.API_ENDPOINT_PatchMasters +"/updatePatch";
		return this.http.post(url, params, { headers: this.headers });
	}
	findAlreadyInsertedPatch(params:any): Observable<any> {
		let url =this._urlService.API_ENDPOINT_PatchMasters +'?filter=' + JSON.stringify(params)
		return this.http.get(url, { headers: this.headers });
	}

	getStockistDetails(params:any): Observable<any> {
		let url =this._urlService.API_ENDPOINT_PROVIDERS +'?filter=' + JSON.stringify(params)
		return this.http.get(url, { headers: this.headers });
	}

	getProvider(params:any): Observable<any>{
		let paramObj = {
			where:params,
			include: [
				{
				  relation:'district',
				  scope:{
					fields:{
					  'districtName':true,
					  'id':true
					}
				  }
				},
				{
					relation:'user',
					scope:{
					  fields:{
						'name':true,
						'id':true
					  }
					}
				  }
			  ]
		}
		console.log('service',paramObj);
		let url =this._urlService.API_ENDPOINT_PROVIDERS +'?filter=' + JSON.stringify(paramObj)
         return this.http.get(url, { headers: this.headers });
	}

	updategeoLocationApproval(params:object, where : any):Observable<any>{
		const url = this._urlService.API_ENDPOINT_PROVIDERS+'/update?where='+JSON.stringify(where)
		return this.http.post<any>(url, params, {
			headers: this.headers
		});
	}


	// new api 
	userversioncheck(params): Observable<any>{
		// let paramObj = {
		// 	where:params,
		// 	// include: [
		// 	// 	{
		// 	// 	  relation:'user',
		// 	// 	  scope:{
		// 	// 		fields:{
		// 	// 		  'companyName':true,
		// 	// 		  'name':true,
		// 	// 		  'designation':true,
		// 	// 		  'id':true,
		// 	// 		  'employeeCode':true,
		// 	// 		  'divisionName':true,
		// 	// 		  'username':true
		// 	// 		}
		// 	// 	  }
		// 	// 	}
		// 	//   ]
		// }
		console.log('service userversioncheck',JSON.stringify(params));

		let url =this._urlService.API_ENDPOINT_DEVICES +'/deviceVersion?params=' + JSON.stringify(params)
         return this.http.get(
			 url, { headers: this.headers });
	}
	

}

// let url =
// 			this._urlService.API_ENDPOINT_PROVIDERS +
// 			"/getFieldForceStatus?params=" +
// 			JSON.stringify(obj);
// 		return this.http.get(url, { headers: this.headers });