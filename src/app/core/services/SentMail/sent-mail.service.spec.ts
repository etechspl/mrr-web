import { TestBed } from '@angular/core/testing';

import { SentMailService } from './sent-mail.service';

describe('SentMailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SentMailService = TestBed.get(SentMailService);
    expect(service).toBeTruthy();
  });
});
