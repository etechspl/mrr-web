import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { URLService } from '../URL/url.service';

@Injectable({
  providedIn: 'root'
})
export class SentMailService {

  constructor(
    private http: HttpClient,
    private authservice: AuthenticationService,
    private _urlService: URLService
  ) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  createSentItem(data: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_SENTITEM;
    return this.http.post(url, JSON.stringify(data), { headers: this.headers });
  }

  getMails(object: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_SENTITEM + '/getMails?param=' + JSON.stringify(object);
    return this.http.get(url, { headers: this.headers });
  }

  updateSentMail(whereObject: any, data: any): Observable<any> {
    let url = this._urlService.API_ENDPOINT_SENTITEM + '/update?where=' + JSON.stringify(whereObject);
    return this.http.post(url, data, { headers: this.headers });
  }
}
