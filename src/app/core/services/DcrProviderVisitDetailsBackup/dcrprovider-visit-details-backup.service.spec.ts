import { TestBed } from '@angular/core/testing';

import { DCRProviderVisitDetailsBackupService } from './dcrprovider-visit-details-backup.service';

describe('DCRProviderVisitDetailsBackupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DCRProviderVisitDetailsBackupService = TestBed.get(DCRProviderVisitDetailsBackupService);
    expect(service).toBeTruthy();
  });
});
