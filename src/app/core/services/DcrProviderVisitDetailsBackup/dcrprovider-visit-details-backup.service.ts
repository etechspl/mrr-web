import { Observable } from 'rxjs';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLService } from '../URL/url.service';
import { Injectable } from '@angular/core';
import { formatDate } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class DCRProviderVisitDetailsBackupService {

  constructor(private _urlService: URLService,
    private http: HttpClient,
    private authservice: AuthenticationService) { }
    headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': this.authservice.getAccessTokenNew()
    });
   
getDCRProviderVisitCopy(dcrId:any): Observable<any>{
  let url = this._urlService.API_ENDPOINT_DCRProviderVisitDetails + '?filter={"where": {"dcrId":"' + dcrId + '"}}';
  return this.http.get(url, { headers: this.headers })
}
submitDCRProviderVisitCopy(dcrObj:any): Observable<any>{
  let DCRObj=[];
  let obj={};
  
  for(var i=0 ;i<dcrObj.length;i++){
    console.log(dcrObj[i].companyId)
    obj={   
    "companyId": dcrObj[i].companyId,
    "dcrDate": dcrObj[i].dcrDate,
    "dcrId": dcrObj[i].dcrId,
    "providerType": dcrObj[i].providerType,
    "productDetails": dcrObj[i].productDetails,
    "giftDetails": dcrObj[i].giftDetails,
    "geoLocation": dcrObj[i].geoLocation,
    "windowDisplay":dcrObj[i].windowDisplay,
    "stateId": dcrObj[i].stateId,
    "districtId":dcrObj[i].districtId,
    "blockId": dcrObj[i].blockId,
    "providerId": dcrObj[i].providerId,
    "remarks":dcrObj[i].remarks,
    "jointWorkWithId":dcrObj[i].jointWorkWithId,
    "isOutsideTp": dcrObj[i].isOutsideTp,
    "detailingDone":dcrObj[i].detailingDone,
    "sampleQty":dcrObj[i].sampleQty,
    "punchDate": dcrObj[i].punchDate,
    "callModifiedDate": dcrObj[i].callModifiedDate,
    "totalSession":dcrObj[i].totalSession,
    "createdAt": dcrObj[i].createdAt,
    "updatedAt": dcrObj[i].updatedAt,
    "purchasedQty":dcrObj[i].purchasedQty,
    "productPob":dcrObj[i].productPob,
    "ipAddress": dcrObj[i].ipAddress,
    "submitBy": dcrObj[i].submitBy,
    "deletedAt":new Date(),
    }
    DCRObj.push(obj);
  }



  let url = this._urlService.API_ENDPOINT_DCRProviderVisitBackup;
  return this.http.post(url, DCRObj, { headers: this.headers });
}
}
