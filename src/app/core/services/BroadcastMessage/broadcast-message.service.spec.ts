import { TestBed } from '@angular/core/testing';

import { BroadcastMessageService } from './broadcast-message.service';

describe('BroadcastMessageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BroadcastMessageService = TestBed.get(BroadcastMessageService);
    expect(service).toBeTruthy();
  });
});
