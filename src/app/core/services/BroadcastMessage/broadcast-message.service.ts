import { Injectable } from '@angular/core';
import { URLService } from '../URL/url.service';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BroadcastMessageService {

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.authservice.getAccessTokenNew()
  });
  constructor(private _urlService:URLService,private http: HttpClient,private authservice: AuthenticationService) { }
  
  baseUrl=this._urlService.API_ENDPOINT_BroadcastMessages;

  createBroadcaseMessgae(obj:any){
    // let url = this._urlService.API_ENDPOINT_BroadcastMessages + "/createBroadcastMessage?params=" + JSON.stringify(obj) + this.authservice.ACCESSTOKEN;
    // return this.http.get(url)
    //alert(obj)
    return this.http.post(this.baseUrl, obj, { headers: this.headers });
  }
  getBroadcastMessageDetail(companyId:string){
    let filter={
      companyId:companyId
    };
    let url = this.baseUrl  + '?filter={"where": {"companyId":"' + companyId + '","status":true},"include": [{"relation": "state","scope": {"fields":["stateName"]}},{"relation":"district","scope":{"fields":["districtName"]}},{"relation":"userInfo","scope":{"fields":["name"]}}]}';

  //  let url = this.baseUrl + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
  }
  deleteBroadcastMessages(messageIds:any){
    let whereObj={
       id:{inq:messageIds}
    }
    let url = this.baseUrl + "/update?where=" + JSON.stringify(whereObj);
    return this.http.post(url, {status:false}, { headers: this.headers });
  }
  getUserBroadcastMessage(companyId:string,userId:string){
    let filter={
      companyId:companyId,
      userId:userId
    };
    let url = this.baseUrl  + '?filter={"where": {"companyId":"' + companyId + '","userId":"'+userId+'","validity": {"gte":"2019-08-28T00:00:00.000Z"},"status":true},"include": [{"relation": "state","scope": {"fields":["stateName"]}},{"relation":"district","scope":{"fields":["districtName"]}},{"relation":"userInfo","scope":{"fields":["name"]}}]}';

  //  let url = this.baseUrl + '?filter=' + JSON.stringify(filter);
    return this.http.get(url, { headers: this.headers })
  }
}
