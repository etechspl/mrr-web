import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class TokenStorage {
	/**
	 * Get access token
	 * @returns {Observable<string>}
	 */
	public getAccessToken(): Observable<string> {
		const token: string = <string>sessionStorage.getItem('accessToken');
		return of(token);
	}

	//PK----
	public getAccessTokenNew() {
		const token: string = <string>sessionStorage.getItem('accessToken');
		return token;
	}

	/**
	 * Get refresh token
	 * @returns {Observable<string>}
	 */
	public getRefreshToken(): Observable<string> {
		const token: string = <string>sessionStorage.getItem('refreshToken');
		return of(token);
	}

	/**
	 * Get user roles in JSON string
	 * @returns {Observable<any>}
	 */
	public getUserRoles(): Observable<any> {
		const roles: any = sessionStorage.getItem('userRoles');
		try {
			return of(JSON.parse(roles));
		} catch (e) { }
	}

	/**
	 * Set access token
	 * @returns {TokenStorage}
	 */
	public setAccessToken(token: string): TokenStorage {
		sessionStorage.setItem('accessToken', token);

		return this;
	}
	//PK---
	public setAccessTokenNew(token: string) {
		sessionStorage.setItem('accessToken', token);
	}
	/**
	 * Set refresh token
	 * @returns {TokenStorage}
	 */
	public setRefreshToken(token: string): TokenStorage {
		sessionStorage.setItem('refreshToken', token);

		return this;
	}

	/**
	 * Set user roles
	 * @param roles
	 * @returns {TokenStorage}
	 */
	public setUserRoles(roles: any): any {
		if (roles != null) {
			sessionStorage.setItem('userRoles', JSON.stringify(roles));
		}

		return this;
	}

	/**
	 * Remove tokens
	 */
	public clear() {
		sessionStorage.removeItem('accessToken');
		sessionStorage.removeItem('refreshToken');
		sessionStorage.removeItem('userRoles');
		sessionStorage.removeItem('currentUser');
		sessionStorage.removeItem('userAccess');
	}
}
