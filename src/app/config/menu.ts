// tslint:disable-next-line:no-shadowed-variable
import { ConfigModel } from '../core/interfaces/config';

// tslint:disable-next-line:no-shadowed-variable
export class MenuConfig implements ConfigModel {
	public config: any = {};
	currentUser = JSON.parse(sessionStorage.currentUser);

	constructor() {
		this.config = {
			header: {
				self: {},
				 items: this.currentUser.userAccess[0].topItem
				 //[
				// 	//REPORTS
				// 	{
				// 		title: 'Reports',
				// 		root: true,
				// 		icon: 'flaticon-line-graph',
				// 		toggle: 'click',
				// 		translate: 'MENU.REPORTS',
				// 		submenu: {
				// 			type: 'mega',
				// 			width: '1000px',
				// 			alignment: 'left',
				// 			columns: [
				// 				{
				// 					heading: {
				// 						heading: true,
				// 						title: 'DCR Report',
				// 					},
				// 					items: [
				// 						{
				// 							title: 'DCR Consolidated',
				// 							tooltip: 'Complete DCR Details with multiple option',
				// 							icon: 'flaticon-map',
				// 							page: '/reports/dcrsummary'
				// 						},
				// 						{
				// 							title: 'Manager Joint Report',
				// 							tooltip: 'Analysis of Manager Working Status',
				// 							icon: 'flaticon-user',
				// 							page: '/reports/mgrJointWork'
				// 						},
				// 						{
				// 							title: 'Manager Team Performance',
				// 							tooltip: 'Analysis of Complete Summary of Selected manager\'s Team',
				// 							icon: 'flaticon-user',
				// 							page: '/reports/mgrTeamPerformance'
				// 						}
				// 						/*{
				// 							title: 'IPO Reports',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: 'flaticon-clipboard',
				// 						},
				// 						{
				// 							title: 'Finance Margins',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: 'flaticon-graphic-1',
				// 						},
				// 						{
				// 							title: 'Revenue Reports',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: 'flaticon-graphic-2',
				// 						}*/
				// 					]
				// 				},
				// 				{
				// 					bullet: 'line',
				// 					heading: {
				// 						heading: true,
				// 						title: 'Tour Program',

				// 					},
				// 					items: [
				// 						{
				// 							title: 'TP Status',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/tpstatus'
				// 						},
				// 						{
				// 							title:
				// 								'Team\'s TP',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/viewteamstp'

				// 						},
				// 						{
				// 							title: 'Approve Tour Program',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/tpApproval'
				// 						},
				// 						{
				// 							title: 'Tour Program Deviation',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/tpDeviation'
				// 						}
				// 						/*{
				// 							title: 'Malibu Accounting',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						{
				// 							title: 'Vineseed Website Rewamp',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						{
				// 							title: 'Zircon Mobile App',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						{
				// 							title: 'Mercury CMS',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						}*/
				// 					]
				// 				},
				// 				{
				// 					bullet: 'dot',
				// 					heading: {
				// 						heading: true,
				// 						title: 'Providers Report',
				// 					},
				// 					items: [
				// 						{
				// 							title: 'View Provider',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/viewproviderlist'
				// 						},
				// 						{
				// 							title: this.currentUser.company.lables.doctorLabel + ' Visit',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/providervisitdetail'

				// 						},
				// 						{
				// 							title: this.currentUser.company.lables.vendorLabel + '/' + this.currentUser.company.lables.stockistLabel + ' Visit Report',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/vendorvisitdetail'
				// 						},
				// 						{
				// 							title: 'Un-touched Provider',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/untouchedProvider'

				// 						},
				// 						{
				// 							title: 'Provider Visit Analysis',
				// 							tooltip: '',
				// 							icon: '',
				// 							page: '/reports/providerVisitAnalysis'
				// 						},
				// 						{
				// 							title: 'Dr. Call Avg. Yearly',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						{
				// 							title: 'Dr. Call Yearly',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						/*{
				// 							title: 'Staff Payslips',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						{
				// 							title: 'Corporate Expenses',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						{
				// 							title: 'Project Expenses',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						}*/
				// 					]
				// 				},
				// 				{
				// 					heading: {
				// 						heading: true,
				// 						title: 'Sales',
				// 						icon: '',
				// 					},
				// 					items: [
				// 						{
				// 							title: 'Target Vs Achievement',
				// 							tooltip: 'Non functional dummy link',
				// 							icon: '',
				// 						},
				// 						/*  {
				// 								title: 'Sources & Mediums',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Reporting Settings',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Conversions',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Report Flows',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Audit & Logs',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							}*/
				// 					]
				// 				}
				// 			]
				// 		}
				// 	},
				// 	//MASTERS ACTIONS
				// 	{
				// 		title: 'Actions',
				// 		root: true,
				// 		icon: 'flaticon-add',
				// 		toggle: 'click',
				// 		translate: 'MENU.ACTIONS',
				// 		submenu: {
				// 			type: 'classic',
				// 			alignment: 'left',
				// 			items: [
				// 				{
				// 					title: 'Master',
				// 					icon: 'flaticon-business',
				// 					submenu: {
				// 						type: 'classic',
				// 						alignment: 'right',
				// 						bullet: 'line',
				// 						items: [
				// 							{
				// 								title: 'State',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'District',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Area',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},

				// 							{
				// 								title: 'Division',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Product',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Designation',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Gift',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Sample',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Master Data Transfer',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							}
				// 						]
				// 					}
				// 				},
				// 				{
				// 					title: 'Customer Feedbacks',
				// 					tooltip: 'Non functional dummy link',
				// 					icon: 'flaticon-chat-1',
				// 					submenu: {
				// 						type: 'classic',
				// 						alignment: 'right',
				// 						bullet: 'dot',
				// 						items: [
				// 							{
				// 								title: 'Customer Feedbacks',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Supplier Feedbacks',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Reviewed Feedbacks',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Resolved Feedbacks',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							},
				// 							{
				// 								title: 'Feedback Reports',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: '',
				// 							}
				// 						]
				// 					}
				// 				},
				// 				{
				// 					title: 'Create New User',
				// 					tooltip: 'Non functional dummy link',
				// 					icon: 'flaticon-users',
				// 				}
				// 			]
				// 		}
				// 	},
				// 	//REQUESTS
				// 	{
				// 		title: 'Requests',
				// 		root: true,
				// 		icon: 'flaticon-paper-plane',
				// 		toggle: 'click',
				// 		translate: 'MENU.REQUESTS',
				// 		/*badge: {
				// 			type: 'm-badge--brand m-badge--wide',
				// 			value: 'new',
				// 			translate: 'MENU.NEW',
				// 		},*/
				// 		submenu: {
				// 			type: 'classic',
				// 			alignment: 'left',
				// 			items: [
				// 				{
				// 					title: this.currentUser.company.lables.areaLabel,
				// 					icon: 'flaticon-business',
				// 					submenu: {
				// 						type: 'classic',
				// 						alignment: 'right',
				// 						items: [
				// 							{
				// 								title: this.currentUser.company.lables.areaLabel + ' Approval',
				// 								tooltip: 'Approval Requests',
				// 								icon: 'flaticon-users',
				// 								page: '/master/approvalMaster/' + this.currentUser.company.lables.areaLabel + 'Approval',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							},
				// 							{
				// 								title: this.currentUser.company.lables.areaLabel + ' Deletion',
				// 								tooltip: 'Deletion Requests',
				// 								icon: 'flaticon-interface-1',
				// 								page: '/master/approvalMaster/' + this.currentUser.company.lables.areaLabel + 'Deletion',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							}
				// 						]
				// 					}
				// 				},
				// 				{
				// 					title: this.currentUser.company.lables.doctorLabel,
				// 					icon: 'flaticon-business',
				// 					submenu: {
				// 						type: 'classic',
				// 						alignment: 'right',
				// 						items: [
				// 							{
				// 								title: this.currentUser.company.lables.doctorLabel + ' Approval',
				// 								tooltip: 'Approval Requests',
				// 								icon: 'flaticon-users',
				// 								page: '/master/approvalMaster/' + this.currentUser.company.lables.doctorLabel + 'Approval',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							},
				// 							{
				// 								title: this.currentUser.company.lables.doctorLabel + ' Deletion',
				// 								tooltip: 'Deletion Requests',
				// 								icon: 'flaticon-interface-1',
				// 								page: '/master/approvalMaster/' + this.currentUser.company.lables.doctorLabel + 'Deletion',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							}
				// 						]
				// 					}
				// 				},
				// 				{
				// 					title: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel,
				// 					icon: 'flaticon-business',
				// 					submenu: {
				// 						type: 'classic',
				// 						alignment: 'right',
				// 						items: [
				// 							{
				// 								title: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel + ' Approval',
				// 								tooltip: 'Approval Requests',
				// 								icon: 'flaticon-users',
				// 								page: '/master/approvalMaster/' + this.currentUser.company.lables.vendorLabel + 'Approval',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							},
				// 							{
				// 								title: this.currentUser.company.lables.vendorLabel + ' / ' + this.currentUser.company.lables.stockistLabel + ' Deletion',
				// 								tooltip: 'Deletion Requests',
				// 								icon: 'flaticon-interface-1',
				// 								page: '/master/approvalMaster/' + this.currentUser.company.lables.vendorLabel + 'Deletion',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							}
				// 						]
				// 					}
				// 				},
				// 				/*{
				// 					title: 'Marketing',
				// 					tooltip: 'Non functional dummy link',
				// 					icon: 'flaticon-map',
				// 				},
				// 				{
				// 					title: 'Campaigns',
				// 					tooltip: 'Non functional dummy link',
				// 					icon: 'flaticon-graphic-2',
				// 					badge: {
				// 						type: 'm-badge--success',
				// 						value: '3'
				// 					}
				// 				},*/
				// 				/*{
				// 					title: 'Tour Program',
				// 					tooltip: 'Non functional dummy link',
				// 					icon: 'flaticon-infinity',
				// 					submenu: {
				// 						type: 'classic',
				// 						alignment: 'right',
				// 						items: [
				// 							{
				// 								title: 'TP Approval',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: 'flaticon-add',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							},
				// 							{
				// 								title: 'TP Deletion',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: 'flaticon-signs-1',
				// 							}
				// 						]
				// 					}
				// 				},
				// 				{
				// 					title: 'Expense',
				// 					tooltip: 'Non functional dummy link',
				// 					icon: 'flaticon-infinity',
				// 					submenu: {
				// 						type: 'classic',
				// 						alignment: 'right',
				// 						items: [
				// 							{
				// 								title: 'Finalize Expense',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: 'flaticon-add',
				// 								badge: {
				// 									type: 'm-badge--danger',
				// 									value: '3'
				// 								}
				// 							},
				// 							{
				// 								title: 'Approve Manager Expense',
				// 								tooltip: 'Non functional dummy link',
				// 								icon: 'flaticon-signs-1',
				// 							}
				// 						]
				// 					}
				// 				}*/
				// 			]
				// 		}
				// 	}
				// ]
			},
			aside: {
				self: {},
				items: this.currentUser.userAccess[0].items

			}
		};
	}
}
