'use strict';
var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');
var async = require('async');

module.exports = function(Tourprogram) {
    // update API for Sync TP
    Tourprogram.updateAllTPRecord = function(records, cb) {
        

        var self = this;
        var response = [];
        var ids = [];
        var TourprogramCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        if (records.length === 0 || records === "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }

        let returnData = records;
		let returnInfo = [];
        for (var w = 0; w < returnData.length; w++) {           

           	let set={};
            set = returnData[w];		
			
			set["stateId"] = ObjectId(set["stateId"]);
			set["districtId"] = ObjectId(set["districtId"]);
			set["createdBy"] = ObjectId(set["createdBy"]);
			
			set["companyId"] = ObjectId(set["companyId"]);
			set["createdBy"] = ObjectId(set["createdBy"]);
			
			set["date"] = new Date(set["date"]);
			if(set.hasOwnProperty("tpSubmissionDate")===true){
				set["tpSubmissionDate"] = new Date(set["tpSubmissionDate"]);
			}
			
			if(set.hasOwnProperty("tpModificationDate")===true){
				set["tpModificationDate"] = new Date(set["tpModificationDate"]);
			}
			
			
			if(set.hasOwnProperty("planningSubmissionDate")===true){
				set["planningSubmissionDate"] = new Date(set["planningSubmissionDate"]);
			}
			if(set.hasOwnProperty("planningModificationDate")===true){
				set["planningModificationDate"] = new Date(set["planningModificationDate"]);
			}
			
			if(set.hasOwnProperty("createdAt")===true){
				set["createdAt"] = new Date(set["createdAt"]);
			}
			
			if(set.hasOwnProperty("updatedAt")===true){
				set["updatedAt"] = new Date(set["updatedAt"]);
			}
			
			if(set.hasOwnProperty("approvedAt")===true){
				set["approvedAt"] = new Date(set["approvedAt"]);
			}
			
			returnInfo.push(set);
			TourprogramCollection.update(
					{ "_id" : ObjectId(records[w].id)},
					{ $set: set },
					function(err,result){
					//// console.log("res");// console.log(result);
					if(err){
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: records, methodName: 'updateAllTPRecord' });
						// console.log("err");// console.log(err);
					}
				
			      });
        }

        return cb(false, returnInfo);

    };
        // Update All Data for sync

    Tourprogram.remoteMethod(
        'updateAllTPRecord', {
            description: 'update and send response all data',
            accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'post'
            }
        }
    );
    
    
    // End TP Record

    
    // Delete API for TP
    Tourprogram.deleteAllRecord = function(records, cb) {
        var self = this;
        var response = [];
        var ids = [];
        var TourprogramCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        if (records.length === 0 || records === "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }

        for (var w = 0; w < records.length; w++) {

            Tourprogram.destroyById(
                records[w].id,
                function(err) {
                    if (err) {
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: records, methodName: 'deleteAllRecord' });
                        // console.log(err);
                    }
                }
            );


        }

        return cb(false, "Record has been Deleted Sucessfully");

    };
        // Delete All Data

    Tourprogram.remoteMethod(
        'deleteAllRecord', {
            description: 'Delete and send response all data',
            accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'post'
            }
        }
    );


    
    //---------------------------------------Praveen Kumar(17-10-2018)-----------------------------------------
    Tourprogram.getTPDetail = function(param, cb) {
        var self = this;
        var TourprogramCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        //// console.log(param)
        TourprogramCollection.aggregate(
            // Stage 1
            {
                $match: {
                    //"_id" : ObjectId("5c24d16db5156b0944933a9d"),
                    companyId: ObjectId(param.companyId),
                    createdBy: ObjectId(param.userId),
                    month: param.month,
                    year: param.year,
                    //sendForApproval: true,
                    //"date": new Date("2018-09-02")

                }
            },

            // Stage 2
            {
                $group: {
                    _id: {
                        id: "$_id",
                        date: "$date"
                    },
                    submittedActivity: { $addToSet: "$activity" },
                    apprvActivity: {
                        $addToSet: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$approvalStatus", true] }
                                ]
                            }, "$approvedActivity", "-------"]
                        }
                    },
                    submittedArea: { $addToSet: "$area" },
                    approvedArea: {
                        $addToSet: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$approvalStatus", true] }
                                ]
                            }, "$approvedArea", "-------"]
                        }
                    },
                    submittedDoctor: { $addToSet: "$doctors" },
                    apprvDoctor: {
                        $addToSet: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$approvalStatus", true] }
                                ]
                            }, "$approvedDoctors", "-------"]
                        }
                    },
                    submittedChemist: { $addToSet: "$chemists" },
                    apprvChemist: {
                        $addToSet: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$approvalStatus", true] }
                                ]
                            }, "$approvedChemists", "-------"]
                        }
                    },
                    submittedRemark: { $addToSet: "$remarks" },
                    apprvRemark: {
                        $addToSet: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$approvalStatus", true] }
                                ]
                            }, "$approvedRemarks", "-------"]
                        }
                    },
                    sendForApprovalDate: { $addToSet: "$tpSubmissionDate" },
                    apporvedById: {
                        $addToSet: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$approvalStatus", true] }
                                ]
                            }, "$approvedById", "-------"]
                        }
                    },
                    apporvedByName: {
                        $addToSet: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$approvalStatus", true] }
                                ]
                            }, "$approvedByName", "-------"]
                        }
                    },
                    approvedAt: {
                        $addToSet: "$approvedAt"
                    },
                    createdBy: {
                        $addToSet: "$createdBy"
                    },
                    TPStatus: {
                        "$addToSet": "$TPStatus"
                    },
                    isDayPlan: {
                        "$addToSet": "$isDayPlan"
                    },
                    plannedWorkWith: {
                        "$addToSet": "$tpPlannedWithUserId"
                    }
                }
            },
            // Stage 3

            {
                $unwind: { "path": "$submittedActivity", "preserveNullAndEmptyArrays": true }
            },

            // Stage 4
            {
                $unwind: { "path": "$apprvActivity", "preserveNullAndEmptyArrays": true }
            },

            // Stage 5
            {
                $unwind: { "path": "$submittedArea", "preserveNullAndEmptyArrays": true }
            },

            // Stage 6
            {
                $unwind: { "path": "$approvedArea", "preserveNullAndEmptyArrays": true }
            },

            // Stage 7
            {
                $unwind: { "path": "$submittedDoctor", "preserveNullAndEmptyArrays": true }
            },

            // Stage 8
            {
                $unwind: { "path": "$apprvDoctor", "preserveNullAndEmptyArrays": true }
            },

            // Stage 9
            {
                $unwind: { "path": "$submittedChemist", "preserveNullAndEmptyArrays": true }
            },

            // Stage 10
            {
                $unwind: { "path": "$apprvChemist", "preserveNullAndEmptyArrays": true }
            },

            // Stage 11
            {
                $unwind: { "path": "$submittedRemark", "preserveNullAndEmptyArrays": true }
            },

            // Stage 12
            {
                $unwind: { "path": "$apprvRemark", "preserveNullAndEmptyArrays": true }
            }, {
                $unwind: { "path": "$sendForApprovalDate", "preserveNullAndEmptyArrays": true }
            }, {
                $unwind: { "path": "$apporvedById", "preserveNullAndEmptyArrays": true }
            }, {
                $unwind: { "path": "$apporvedByName", "preserveNullAndEmptyArrays": true }
            }, {
                $unwind: { "path": "$approvedAt", "preserveNullAndEmptyArrays": true }
            }, {
                $unwind: { "path": "$createdBy", "preserveNullAndEmptyArrays": true }
            }, {
                $unwind: { "path": "$TPStatus", "preserveNullAndEmptyArrays": true }
            }, {
                $unwind: { "path": "$isDayPlan", "preserveNullAndEmptyArrays": true }
            },{
                $unwind: { "path": "$plannedWorkWith", "preserveNullAndEmptyArrays": true }
            },
            {
          $lookup:
            {
                "from" : "UserInfo",
                "localField" : "plannedWorkWith",
                "foreignField" : "userId",
                "as" : "data"
            }},
            // Stage 13
            {
                $project: {
                    _id: 1,
                    date: { "$dateToString": { "format": "%Y-%m-%d", "date": "$_id.date" } },
                    submittedActivity: 1,
                    apprvActivity: 1,
                    submittedArea: 1,
                    approvedArea: 1,
                    submittedDoctor: 1,
                    apprvDoctor: 1,
                    submittedChemist: 1,
                    apprvChemist: 1,
                    submittedRemark: 1,
                    apprvRemark: 1,
                    sendForApprovalDate: { "$dateToString": { "format": "%Y-%m-%d", "date": "$sendForApprovalDate" } },
                    apporvedById: 1,
                    apporvedByName: 1,
                    approvedAt: {
                        // $cond: { if: { $eq: ["$apporvedAt", "-------"] }, then: "-------", else: { "$dateToString": { "format": "%Y-%m-%d", "date": "$apporvedAt" } } }
                        "$dateToString": { "format": "%Y-%m-%d", "date": "$approvedAt" }
                    },
                    createdBy: 1,
                    TPStatus: 1,
                    isDayPlan: 1,
					workWith: { $arrayElemAt: ["$data.name", 0] },


                }
            },

            // Stage 14
            {
                $sort: {
                    date: 1
                }
            },
            function(err, result) {
                if (err) {
                    sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'getTPDetail' });
                    // console.log(err);
                    return cb(err)
                }
                //// console.log(result)
                if (result.length > 0) {
                    let finalData = [];
                    for (let i = 0; i < result.length; i++) {
                        let tempObj = {};

                        let dcrDateTemp = moment(new Date(result[i].date), 'yyyy-mm-dd');
                        let formattedDcrDate = dcrDateTemp.format('DD-MMM-YYYY');

                        tempObj.date = formattedDcrDate;
                        tempObj.submittedActivity = result[i].submittedActivity;
                        tempObj.apprvActivity = result[i].apprvActivity;
                        tempObj.createdBy = result[i].createdBy;
                        tempObj._id = result[i]._id.id;
                        tempObj.TPStatus = result[i].TPStatus;
                        tempObj.isDayPlan = result[i].isDayPlan;
						tempObj.workWithPlan = result[i].workWith;
                        let tempSubmittedAreaForBefore2018Year = ["--------"];
                        let submittedArea = "--------";
                        if (typeof result[i].submittedArea == "object") {
                            for (let sa = 0; sa < result[i].submittedArea.length; sa++) {
                                tempSubmittedAreaForBefore2018Year[sa] = result[i].submittedArea[sa].from;
                                //As TP was not submitted on the basis of "From" to "To" station
                                if (param.year >= 2019) {
                                    if (sa == 0) {
                                        submittedArea = "From : " + result[i].submittedArea[sa].from + " -- To : " + result[i].submittedArea[sa].to;
                                    } else {
                                        submittedArea = submittedArea + ", From : " + result[i].submittedArea[sa].from + " -- To : " + result[i].submittedArea[sa].to;
                                    }
                                }
                            }
                        }

                        // As TP was not submitted on the basis of "From" to "To" station
                        if (param.year >= 2019) {
                            tempObj.submittedArea = submittedArea;
                        } else {
                            tempObj.submittedArea = tempSubmittedAreaForBefore2018Year;
                        }

                        let tempApprovedAreaForBefore2018Year = ["--------"];
                        let approvedArea = "--------";
                        if (typeof result[i].approvedArea == "object") {
                            for (let aa = 0; aa < result[i].approvedArea.length; aa++) {

                                tempApprovedAreaForBefore2018Year[aa] = result[i].approvedArea[aa].from;
                                //As TP was not submitted on the basis of "From" to "To" station
                                if (param.year >= 2019) {
                                    if (aa == 0) {
                                        approvedArea = "From : " + result[i].approvedArea[aa].from + " -- To : " + result[i].approvedArea[aa].to;
                                    } else {
                                        approvedArea = approvedArea + ", From : " + result[i].approvedArea[aa].from + " -- To : " + result[i].approvedArea[aa].to;
                                    }
                                }
                            }
                        }
                        tempObj.approvedArea = approvedArea;

                        //As TP was not submitted on the basis of "From" to "To" station
                        if (param.year >= 2019) {
                            tempObj.approvedArea = approvedArea;
                        } else {
                            tempObj.approvedArea = tempApprovedAreaForBefore2018Year;
                        }


                        let submittedDoctor = "--------";
                        if (typeof result[i].submittedDoctor == "object") {
                            for (let sd = 0; sd < result[i].submittedDoctor.length; sd++) {
                                if (sd == 0) {
                                    submittedDoctor = result[i].submittedDoctor[sd].Name;
                                } else {
                                    submittedDoctor = submittedDoctor + ", " + result[i].submittedDoctor[sd].Name;
                                }
                            }
                        }
                        tempObj.submittedDoctor = submittedDoctor;


                        let apprvDoctor = "--------";
                        if (typeof result[i].apprvDoctor == "object") {
                            for (let ad = 0; ad < result[i].apprvDoctor.length; ad++) {
                                if (ad == 0) {
                                    apprvDoctor = result[i].apprvDoctor[ad].Name;
                                } else {
                                    apprvDoctor = apprvDoctor + ", " + result[i].apprvDoctor[ad].Name;
                                }
                            }
                        }
                        tempObj.apprvDoctor = apprvDoctor;

                        let submittedChemist = "--------";
                        if (typeof result[i].submittedChemist == "object") {
                            for (let sc = 0; sc < result[i].submittedChemist.length; sc++) {
                                if (sc == 0) {
                                    submittedChemist = result[i].submittedChemist[sc].Name;
                                } else {
                                    submittedChemist = submittedChemist + ", " + result[i].submittedChemist[sc].Name;
                                }
                            }
                        }
                        tempObj.submittedChemist = submittedChemist;

                        let approvedChemists = "--------";
                        if (typeof result[i].approvedChemists == "object") {
                            for (let ac = 0; ac < result[i].approvedChemists.length; ac++) {
                                if (ac == 0) {
                                    approvedChemists = result[i].approvedChemists[ac].Name;
                                } else {
                                    approvedChemists = approvedChemists + ", " + result[i].approvedChemists[ac].Name;
                                }
                            }
                        }
                        tempObj.approvedChemists = approvedChemists;
                        tempObj.submittedRemark = result[i].submittedRemark;
                        tempObj.apprvRemark = result[i].apprvRemark;
                        let sendForApprovalDateTemp = moment(new Date(result[i].sendForApprovalDate), 'yyyy-mm-dd');
                        let sendForApprovalDate = sendForApprovalDateTemp.format('DD-MMM-YYYY');
                        tempObj.sendForApprovalDate = sendForApprovalDate;
                        tempObj.apporvedById = result[i].apporvedById;
                        tempObj.apporvedByName = result[i].apporvedByName;
                        if (result[i].approvedAt == "1900-01-01") {
                            tempObj.approvedAt = "-------";
                        } else {
                            let approvedDateTemp = moment(new Date(result[i].approvedAt), 'yyyy-mm-dd');
                            let approvedDate = approvedDateTemp.format('DD-MMM-YYYY');
                            tempObj.approvedAt = approvedDate;
                        }
                        finalData.push(tempObj);
                    }


                    //// console.log(finalData)
                    cb(false, finalData);
                } else {
                    cb(false, []);
                }



            });
    };

    Tourprogram.remoteMethod(
        'getTPDetail', {
            description: 'Get Tour Program Details',
            accepts: [{ arg: 'param', type: 'object' }],
            returns: {
                root: true,
                type: 'array'
            },
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //--------------------------------------------END----------------------------------------------------------
    
    //--------------------------Praveen Kumar 23-10-2018-------------------------------------------------------
    Tourprogram.getDCHeirarchyTPData = function(userIdArr, cb) {
        var self = this;
        var TPCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        var ids = [];
        for (var w in userIdArr) {
            ids.push(ObjectId(userIdArr[w]));
        }
        TPCollection.aggregate({
                $match: {
                    createdBy: { $in: ids }
                }
            },
            // Stage 2
            {
                $project: {
                    id: "$_id",
                    _id: 0,
                    date: 1,
                    month: 1,
                    year: 1,
                    activity: 1,
                    activityApproved: 1,
                    doctors: 1,
                    approvedDoctors: 1,
                    chemists: 1,
                    approvedChemists: 1,
                    area: 1,
                    approvedArea: 1,
                    remarks: 1,
                    //month:1,
                    //year:1,
                    sendForApproval: 1,
                    approvalStatus: 1,
                    //ipAddress:1,
                    //geoLocation:1,
                    //rejectionMessage:1,
                    //tpSubAppVersion:1,
                    //tpApprvAppVersion:1,
                    createdAt: 1,
                    updatedAt: 1,
                    createdBy: 1,
                    approvedRemarks: 1,
					TPStatus : 1
                }
            },
            function(err, DCHeirarchyTP) {
                if (err) {
                    sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: userIdArr, methodName: 'getDCHeirarchyTPData' });
                    // console.log(err);
                    return cb(err);
                }
                if (!DCHeirarchyTP) {
                    var err = new Error('no records found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                return cb(false, DCHeirarchyTP);
            });

    };

    Tourprogram.remoteMethod(
        'getDCHeirarchyTPData', {
            description: 'getDCHeirarchyTPData for selected details',
            accepts: [{
                arg: 'userId',
                type: 'array'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //------------------------------END----------------------------------------------------------------
    //--------------------------Ravindra Singh 24-10-2018------------------------------
    
    Tourprogram.updateTPStatus = function(object, cb) {
        var self = this;
        var TPCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
		//------As Per Vrushali Instruction on 11-04-2019
		//let tomorrowDate =  new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
		let tomorrowDate = new Date(moment.utc().add('1070', 'minutes'));
        //------END As Per Vrushali Instruction on 11-04-2019
		let updateObj = {};
        if (object.approvalStatus == true) {
            updateObj = {
                "approvalStatus": object.approvalStatus,
                "approvedAt": new Date(object.approvedAt),
				"approvedById" : ObjectId(object.approvedById),
				"approvedByName" : object.approvedByName,
				"rejectionMessage": "",
                //"updatedAt": new Date()
				"updatedAt": tomorrowDate
            }
        } else if (object.sendForApproval == true && object.approvalStatus == false) {
            updateObj = {
                "sendForApproval": object.sendForApproval,
                "tpSubmissionDate": new Date(object.tpSubmissionDate),
                //"updatedAt": new Date()
				"updatedAt": tomorrowDate
            }
        } else if (object.rejectionMessage != "" || object.rejectionMessage != undefined || object.rejectionMessage != null) {
            updateObj = {
                "rejectionMessage": object.rejectionMessage,
				"approvedById" : ObjectId(object.approvedById),
				"approvedByName" : object.approvedByName,
                //"updatedAt": new Date()
				"updatedAt": tomorrowDate
            }
        }

        TPCollection.update({
                "month": object.month,
                "year": object.year,
                "createdBy": ObjectId(object.createdBy)
            }, {
                $set: updateObj
            }, {
                multi: true
            },
            function(err, res) {
                if (err) {
                    sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: object, methodName: 'updateTPStatus' });
                    return cb(false, err);
                }
                if (res) {

                    //  // console.log(res.nModified);
                    var lastResult = [];
                    lastResult.push({
                        "count": res.result.nModified
                    });
                    //// console.log(lastResult);
                    return cb(false, lastResult);
                }
            })
    };
    Tourprogram.remoteMethod(
        'updateTPStatus', {
            description: 'Update TP  Send For Approval Status',
            accepts: [{
                arg: 'object',
                type: 'Object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    //-------------------------------------Rahul Choudhary-24-10-2018----------getTPSubmittedDetails----------------------------------------------------
    Tourprogram.getTPSubmittedDetails = function(month, year, userId, cb) {

        var self = this;
        var TPCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        //user details
        var users = [];
        Tourprogram.app.models.Hierarchy.getTCs(userId, function(err, results) {
            if(err){
                sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: [month, year, userId], methodName: 'getTPSubmittedDetails' });
            }
            var ids = [];
            users = results;
            for (var w in results) {
                ids.push(results[w].userId);
            }
            TPCollection.aggregate({
                    $match: {
                        month: month,
                        year: year,
                        createdBy: {
                            $in: ids
                        }
                    }
                }, {
                    $group: {
                        _id: {
                            "createdBy": "$createdBy",
                            "approvalStatus": "$approvalStatus",
                            "sendForApproval": "$sendForApproval"
                        }
                    }
                },

                function(err, records) {
                    if (err) {
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: [month, year, userId], methodName: 'getTPSubmittedDetails' });
                        // console.log(err);
                        return cb(err);
                    }
                    // // console.log(records);

                    if (!records) {
                        var err = new Error('no records found');
                        err.statusCode = 404;
                        err.code = 'NOT FOUND';
                        return cb(err);
                    }

                    var response = [];
                    for (var index in users) {

                        var tpSubmittedStatus = false;
                        var tpApprovedStatus = false;
                        var tpSendForApproval = false;
                        var uid = '' + users[index].userId + '';
                        for (var tpIndex in records) {
                            if (uid.indexOf(records[tpIndex]._id.createdBy) > -1) {
                                tpSubmittedStatus = true;
                                if (records[tpIndex]._id.approvalStatus === true) {
                                    tpApprovedStatus = true;
                                }
                                if (records[tpIndex]._id.sendForApproval === true) {
                                    tpSendForApproval = true;
                                }
                                break;
                            }

                        }
                        if (tpSubmittedStatus === true && tpApprovedStatus === true) {
                            //// console.log("TP Submitted And Approved");
                            response.push({ id: '' + uid + '', name: users[index].userName, tpSubmitted: 'Yes', tpApproved: 'Yes', tpSendForApproval: 'Yes' });
                        } else if (tpSubmittedStatus === true && tpApprovedStatus == false && tpSendForApproval == true) {
                            //// console.log("TP Submitted");
                            response.push({ id: '' + uid + '', name: users[index].userName, tpSubmitted: 'Yes', tpApproved: 'No', tpSendForApproval: 'Yes' });
                        } else if (tpSubmittedStatus === true && tpApprovedStatus == false && tpSendForApproval == false) {
                            //// console.log("TP Submitted");
                            response.push({ id: '' + uid + '', name: users[index].userName, tpSubmitted: 'Yes', tpApproved: 'No', tpSendForApproval: 'No' });
                        } else if (tpSubmittedStatus === false) {
                            //// console.log("TP Not Submitted");
                            response.push({ id: '' + uid + '', name: users[index].userName, tpSubmitted: 'No', tpApproved: 'No', tpSendForApproval: 'No' });
                        }
                    }
                    return cb(false, response);
                });
        })



    };

    Tourprogram.remoteMethod(
        'getTPSubmittedDetails', {
            description: 'Get the list of employe based on month and year tp submitted and approved.',
            accepts: [{
                arg: 'month',
                type: 'number'
            }, {
                arg: 'year',
                type: 'number'
            }, {
                arg: 'userId',
                type: 'string'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );



    //----------------------------------------------------------------------------------------------------
    
    //-----------------------------------------Ravindra(24-10-2018)---------------------------------------------
    Tourprogram.usersTPStatusAPI = function(params, cb) {

        var self = this;
        var TPCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        //user details
        // // console.log("usersTPStatusAPI",params)
        var usersArr = [];
        if (params.level == 0) {

            Tourprogram.app.models.UserInfo.getAllUserIdsOnCompanyBasis(params.companyId, params.status, [], function(err, results) {
                TPCollection.aggregate({
                        $match: {
                            month: params.month,
                            year: params.year,
                            createdBy: { $in: results[0].userIds }
                        }
                    }, {
                        $group: {
                            _id: {
                                "createdBy": "$createdBy",
                                "approvalStatus": "$approvalStatus",
                                "sendForApproval": "$sendForApproval"
                            }
                        }
                    },

                    function(err, records) {
                        if (err) {
                            sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'usersTPStatusAPI' });
                            // console.log(err);
                            return cb(err);
                        }
                        // // console.log(records);

                        if (!records) {
                            var err = new Error('no records found');
                            err.statusCode = 404;
                            err.code = 'NOT FOUND';
                            return cb(err);
                        }
                        var response = [];
                        var responseObj = {};
                        var totalActiveUser = results[0].userIds.length;
                        var totalUserSubmittedTP = 0;
                        var totalUserApprovedTP = 0;
                        var totalUserNotApprovedTP = 0;
                        if (records.length > 0) {

                            for (var i = 0; i < records.length; i++) {
                                if (records[i]._id.sendForApproval === true) {
                                    totalUserSubmittedTP++;
                                }
                                if (records[i]._id.approvalStatus === true) {
                                    totalUserApprovedTP++;
                                }
                                if ((records[i]._id.approvalStatus === false) && (records[i]._id.sendForApproval === true)) {
                                    totalUserNotApprovedTP++;
                                }
                            }
                            responseObj = {
                                totalActiveUser: totalActiveUser,
                                totalUserSubmittedTP: totalUserSubmittedTP,
                                totalUserApprovedTP: totalUserApprovedTP,
                                totalUserNotApprovedTP: totalUserNotApprovedTP
                            };
                            response.push(responseObj);
                        } else {
                            responseObj = {
                                totalActiveUser: totalActiveUser,
                                totalUserSubmittedTP: totalUserSubmittedTP,
                                totalUserApprovedTP: totalUserApprovedTP,
                                totalUserNotApprovedTP: totalUserNotApprovedTP
                            };
                            response.push(responseObj);
                        }
                        return cb(false, response);
                    });
            })
        } else if (params.level >= 2) {
            Tourprogram.app.models.Hierarchy.find({
                where: {
                    companyId: ObjectId(params.companyId),
                    supervisorId: ObjectId(params.userId),
                    status: true
                }
            }, function(err, hrcyResult) {
                if(err){
                    sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'usersTPStatusAPI' });
                }

                let userIdArr = [];
                if (hrcyResult.length > 0) {
                    for (let i = 0; i < hrcyResult.length; i++) {
                        userIdArr.push(ObjectId(hrcyResult[i].userId));
                    }
                }
                TPCollection.aggregate({
                        $match: {
                            month: params.month,
                            year: params.year,
                            createdBy: { $in: userIdArr }
                        }
                    }, {
                        $group: {
                            _id: {
                                "createdBy": "$createdBy",
                                "approvalStatus": "$approvalStatus",
                                "sendForApproval": "$sendForApproval"
                            }
                        }
                    },

                    function(err, records) {
                        if (err) {
                            sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'usersTPStatusAPI' });
                            // console.log(err);
                            return cb(err);
                        }
                        // // console.log(records);

                        if (!records) {
                            var err = new Error('no records found');
                            err.statusCode = 404;
                            err.code = 'NOT FOUND';
                            return cb(err);
                        }
                        var response = [];
                        var responseObj = {};
                        var totalActiveUser = userIdArr.length;
                        var totalUserSubmittedTP = 0;
                        var totalUserApprovedTP = 0;
                        var totalUserNotApprovedTP = 0;
                        if (records.length > 0) {

                            for (var i = 0; i < records.length; i++) {
                                if (records[i]._id.sendForApproval === true) {
                                    totalUserSubmittedTP++;
                                }
                                if (records[i]._id.approvalStatus === true) {
                                    totalUserApprovedTP++;
                                }
                                if ((records[i]._id.approvalStatus === false) && (records[i]._id.sendForApproval === true)) {
                                    totalUserNotApprovedTP++;
                                }
                            }
                            responseObj = {
                                totalActiveUser: totalActiveUser,
                                totalUserSubmittedTP: totalUserSubmittedTP,
                                totalUserApprovedTP: totalUserApprovedTP,
                                totalUserNotApprovedTP: totalUserNotApprovedTP
                            };
                            response.push(responseObj);
                        } else {
                            responseObj = {
                                totalActiveUser: totalActiveUser,
                                totalUserSubmittedTP: totalUserSubmittedTP,
                                totalUserApprovedTP: totalUserApprovedTP,
                                totalUserNotApprovedTP: totalUserNotApprovedTP
                            };
                            response.push(responseObj);
                        }
                        return cb(false, response);
                    });
            })
        }else if (params.level == 1) {
            
                TPCollection.aggregate({
                        $match: {
                            month: params.month,
                            year: params.year,
                            createdBy: ObjectId(params.userId)
                        }
                    }, {
                        $group: {
                            _id: {
                                "createdBy": "$createdBy",
                                "approvalStatus": "$approvalStatus",
                                "sendForApproval": "$sendForApproval"
                            }
                        }
                    },

                    function(err, records) {
                        if (err) {
                            sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'usersTPStatusAPI' });
                            // console.log(err);
                            return cb(err);
                        }
                        // // console.log(records);

                        if (!records) {
                            var err = new Error('no records found');
                            err.statusCode = 404;
                            err.code = 'NOT FOUND';
                            return cb(err);
                        }
                        var response = [];
                        var responseObj = {};
                        var totalActiveUser = 1;
                        var totalUserSubmittedTP = 0;
                        var totalUserApprovedTP = 0;
                        var totalUserNotApprovedTP = 0;
                        if (records.length > 0) {

                            for (var i = 0; i < records.length; i++) {
                                if (records[i]._id.sendForApproval === true) {
                                    totalUserSubmittedTP++;
                                }
                                if (records[i]._id.approvalStatus === true) {
                                    totalUserApprovedTP++;
                                }
                                if ((records[i]._id.approvalStatus === false) && (records[i]._id.sendForApproval === true)) {
                                    totalUserNotApprovedTP++;
                                }
                            }
                            responseObj = {
                                totalActiveUser: totalActiveUser,
                                totalUserSubmittedTP: totalUserSubmittedTP,
                                totalUserApprovedTP: totalUserApprovedTP,
                                totalUserNotApprovedTP: totalUserNotApprovedTP
                            };
                            response.push(responseObj);
                        } else {
                            responseObj = {
                                totalActiveUser: totalActiveUser,
                                totalUserSubmittedTP: totalUserSubmittedTP,
                                totalUserApprovedTP: totalUserApprovedTP,
                                totalUserNotApprovedTP: totalUserNotApprovedTP
                            };
                            response.push(responseObj);
                        }
                        return cb(false, response);
                    });
            
        }

    };

    Tourprogram.remoteMethod(
        'usersTPStatusAPI', {
            description: 'Get the list of employe based on month and year tp submitted and approved.',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Tourprogram.getTotalTPStatus = function(month, year, companyId, userIds, cb) {

        var self = this;
        var TPCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
		
		//// console.log("TPPPPPPP , "+userIds)
        //user details
        var usersArr = [];
        Tourprogram.app.models.UserInfo.getAllUserIdsOnCompanyBasis(companyId, [true], userIds, function(err, results) {
			    TPCollection.aggregate({
                    $match: {
                        companyId: ObjectId(companyId),
                        month: month,
                        year: year,
                        createdBy: { $in: results[0].userIds }
                    }
                }, {
                    $group: {
                        _id: {
                            "createdBy": "$createdBy",
                            "approvalStatus": "$approvalStatus",
                            "sendForApproval": "$sendForApproval"
                        }
                    }
                },

                function(err, records) {
                    if (err) {
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: [month, year, companyId, userIds], methodName: 'getTotalTPStatus' });
                        // console.log(err);
                        return cb(err);
                    }
                    //// console.log(records);

                    if (!records) {
                        var err = new Error('no records found');
                        err.statusCode = 404;
                        err.code = 'NOT FOUND';
                        return cb(err);
                    }
                    var response = [];
                    var responseObj = {};
                    var totalActiveUser = results[0].userIds.length;
                    var totalUserSubmittedTP = 0;
                    var totalUserApprovedTP = 0;
                    var totalUserNotApprovedTP = 0;
                    if (records.length > 0) {

                        for (var i = 0; i < records.length; i++) {
                            
							if (records[i]._id.sendForApproval === true) {
                                    totalUserSubmittedTP++;
                                }
                            if (records[i]._id.approvalStatus === true) {
                                totalUserApprovedTP++;
                            }
							if ((records[i]._id.approvalStatus === false) && (records[i]._id.sendForApproval === true)) {
							totalUserNotApprovedTP++;
							}
                           
                        }
                        responseObj = {
                            totalActiveUser: totalActiveUser,
                            totalUserSubmittedTP: totalUserSubmittedTP,
                            totalUserApprovedTP: totalUserApprovedTP,
                            totalUserNotApprovedTP: totalUserNotApprovedTP
                        };
                        response.push(responseObj);
                    } else {
                        responseObj = {
                            totalActiveUser: totalActiveUser,
                            totalUserSubmittedTP: totalUserSubmittedTP,
                            totalUserApprovedTP: totalUserApprovedTP,
                            totalUserNotApprovedTP: totalUserNotApprovedTP
                        };
                        response.push(responseObj);
                    }

                    //// console.log("TPStatusss222222",response)
                    return cb(false, response);
                });
        })



    };

    Tourprogram.remoteMethod(
        'getTotalTPStatus', {
            description: 'Get the list of employe based on month and year tp submitted and approved.',
            accepts: [{
                arg: 'month',
                type: 'number'
            }, {
                arg: 'year',
                type: 'number'
            }, {
                arg: 'companyId',
                type: 'string'
            }, {
                arg: 'userIds',
                type: 'array'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //------------------------------END----------------------------------------------------------------
    
    //----------------------------Praveen Kumar(24-12-2018)---------------------------------
    Tourprogram.getTPStatus = function(params, cb) {
        var self = this;
       // // console.log(params);
        //  // console.log("stateId : " + params.stateIds);
       // // console.log("districtId : " + params.districtIds);
       // // console.log("fromDate : " + params.type);


        var TourprogramCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        async.parallel({
            userResult: function(cb) {
                if (params.type == "State" || params.type == "Headquarter") {
                    Tourprogram.app.models.UserInfo.getAllUserIdsBasedOnType(
                        params,
                        function(err, userRecords) {
                            cb(false, userRecords);
                        });
                } else if (params.type == "Employee Wise") {                    
                    Tourprogram.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                        params.companyId,
                        params.status,
                        params.userIds,
                        function(err, userRecords) {
                            if(err){
                                sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'getTPStatus' });
                            }
                            cb(false, userRecords);
                        });
                }
            }
        }, function(err, asyncResult) {
            if (err) {
                sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'getTPStatus' });
                // console.log(err);
            }
			var match={
				createdBy: { $in: asyncResult.userResult[0].userIds },
                            month: params.month,
                            year: params.year
			}
			console.log("match- in tp",match)
            if (asyncResult.userResult.length > 0) {
                TourprogramCollection.aggregate(
                    // Stage 1
                    {
                        $match: {
                            createdBy: { $in: asyncResult.userResult[0].userIds },
                            month: params.month,
                            year: params.year
                        }
                    },
                    // Stage 2
                    {
                        $group: {
                            _id: {
                                createdBy: "$createdBy"
                            },
                            sendForApproval: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [
                                            { $eq: ["$sendForApproval", true] }
                                        ]
                                    }, true, false]
                                }
                            },
                            sendForApprovalDate: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [
                                            { $eq: ["$sendForApproval", true] }
                                        ]
                                    }, "$tpSubmissionDate", "-------"]
                                }
                            },
                            approvalStatus: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [
                                            { $eq: ["$approvalStatus", true] }
                                        ]
                                    }, true, false]
                                }
                            },
                            approvedDate: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [
                                            { $eq: ["$approvalStatus", true] }
                                        ]
                                    }, "$approvedAt", "-------"]
                                }
                            },
                            rejectionMsg:{
                                $addToSet:"$rejectionMessage"
                               }


                        }
                    },
                    // Stage 3
                    {
                        $unwind: "$sendForApproval"
                    }, {
                        $unwind: "$sendForApprovalDate"
                    },
                    // Stage 4
                    {
                        $unwind: "$approvalStatus"
                    },
                    // Stage 5
                    {
                        $unwind: "$approvedDate"
                    },
                   {
                   $unwind: {
                    "path": "$rejectionMsg",
                    "preserveNullAndEmptyArrays": true
                              }
                       },
                    // Stage 6
                    {
                        $project: {
                            _id: 0,
                            userId: "$_id.createdBy",
                            sendForApproval: 1,
                            sendForApprovalDate: {
                                $cond: { if: { $eq: ["$sendForApprovalDate", "-------"] }, then: "-------", else: { "$dateToString": { "format": "%Y-%m-%d", "date": "$sendForApprovalDate" } } }
                            },
                            approvalStatus: 1,
                            approvedDate: 1,
                            rejectionMsg:1
                        }
                    },
                    function(err, result) {
                        console.log("result=>",result);
                        

                        if(err){
                            sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'getTPStatus' });
                        }
                        //// console.log(result)
                        let rejectionMsg="";

                        if (result.length > 0) { // If Data exist for at least 1 employee
                            let lastResult = [];
                            for (let i = 0; i < asyncResult.userResult[0].obj.length; i++) {

                                let filterObj1 = result.filter(function(obj) {

                                    return obj.userId.toString() == asyncResult.userResult[0].obj[i].userId.toString();
                                });
                                if (filterObj1.length > 0) {
                                    //// console.log(filterObj1)
                                    let submittedDateFinal = "-------";
                                    let submittedStatus = "No";
                                    let approvedDateFinal = "-------";
                                    let approvedStatus = "No";
                                    if (filterObj1[0].sendForApproval == true) {
                                        submittedStatus = "Yes";
                                        let submittedDate1 = moment(new Date(filterObj1[0].sendForApprovalDate), 'YYYY-mm-dd');
                                        submittedDateFinal = submittedDate1.format('DD-MMM-YYYY');
                                    }
                                    if (filterObj1[0].approvalStatus == true) {
                                        approvedStatus = "Yes"
                                        let approvedDate1 = moment(new Date(filterObj1[0].approvedDate), 'YYYY-mm-dd');
                                        approvedDateFinal = approvedDate1.format('DD-MMM-YYYY');
                                    }
                                    if(filterObj1[0].hasOwnProperty('rejectionMsg')){
                                        if(filterObj1[0].rejectionMsg==""){
                                            rejectionMsg="---------";
                                        }else{
                                            rejectionMsg=filterObj1[0].rejectionMsg
                                        }
                                    }     
                                                
                                    lastResult.push({
                                        name: asyncResult.userResult[0].obj[i].name,
                                        userId: asyncResult.userResult[0].obj[i].userId,
                                        designation: asyncResult.userResult[0].obj[i].designation,
                                        stateId: asyncResult.userResult[0].obj[i].stateId,
                                        stateName: asyncResult.userResult[0].obj[i].stateName,
                                        districtId: asyncResult.userResult[0].obj[i].districtId,
                                        districtName: asyncResult.userResult[0].obj[i].districtName,
                                        submitted: submittedStatus,
                                        submittedDate: submittedDateFinal,
                                        approved: approvedStatus,
                                        approvedDate: approvedDateFinal,
                                        divisionName: asyncResult.userResult[0].obj[i].divisionName,
                                        rejectionMsg:rejectionMsg
                                    })

                                } else {
                                    lastResult.push({
                                        name: asyncResult.userResult[0].obj[i].name,
                                        userId: asyncResult.userResult[0].obj[i].userId,
                                        designation: asyncResult.userResult[0].obj[i].designation,
                                        stateId: asyncResult.userResult[0].obj[i].stateId,
                                        stateName: asyncResult.userResult[0].obj[i].stateName,
                                        districtId: asyncResult.userResult[0].obj[i].districtId,
                                        districtName: asyncResult.userResult[0].obj[i].districtName,
                                        submitted: "No",
                                        submittedDate: "---------",
                                        approved: "No",
                                        approvedDate: "---------",
                                        divisionName: asyncResult.userResult[0].obj[i].divisionName,
                                        rejectionMsg:"---------",

                                    })
                                }
                            }
                            cb(false, lastResult);

                        } else { //If Data does not exist for even 1 employee
                            let lastResult = [];
                            for (let i = 0; i < asyncResult.userResult[0].obj.length; i++) {
                                lastResult.push({
                                    name: asyncResult.userResult[0].obj[i].name,
                                    userId: asyncResult.userResult[0].obj[i].userId,
                                    designation: asyncResult.userResult[0].obj[i].designation,
                                    stateId: asyncResult.userResult[0].obj[i].stateId,
                                    stateName: asyncResult.userResult[0].obj[i].stateName,
                                    districtId: asyncResult.userResult[0].obj[i].districtId,
                                    districtName: asyncResult.userResult[0].obj[i].districtName,
                                    submitted: "No",
                                    submittedDate: "---------",
                                    approved: "No",
                                    approvedDate: "---------",
                                    divisionName: asyncResult.userResult[0].obj[i].divisionName,
                                    rejectionMsg:"---------",

                                })
                            }
                            cb(false, lastResult);
                        }

                    })
            } else {
                cb(false, []);
            }
        });
    };

    Tourprogram.remoteMethod(
        'getTPStatus', {
            description: 'Get Tour Program Status',
            accepts: [{ arg: 'params', type: 'object' }],
            returns: {
                root: true,
                type: 'array'
            },
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //-----------------------------------END------------------------------------------------
    
    //---------------------------------Praveen Kumar(29-12-2018)----------------------------
    Tourprogram.TPDeviationDetails = function(param, cb) {

        var self = this;
        //// console.log(param)
        var TourprogramCollection = self.getDataSource().connector.collection(Tourprogram.modelName);
        let DCRMasterCollection = self.getDataSource().connector.collection(Tourprogram.app.models.DCRMaster.modelName);

        if (param.month < 10) {
            param.month = "0" + param.month;
        }
        let lastDay = new Date(param.year, param.month, 0).getDate();
        let fromDate = param.year + "-" + param.month + "-01";
        let toDate = param.year + "-" + param.month + "-" + lastDay;
        // // console.log("fromDate : " + fromDate + " --- " + "toDate : " + toDate);
		

        async.parallel({
            TPResult: function(cb) {
                TourprogramCollection.aggregate({
                    $match: {
                        // "_id": ObjectId("5c24cee4b5156b0944933087"),
                        companyId: ObjectId(param.companyId),
                        createdBy: ObjectId(param.userId),
                        month: parseInt(param.month),
                        year: parseInt(param.year)
                    }
                }, {
                    $unwind: {
                        "path": "$approvedArea",
                        "preserveNullAndEmptyArrays": true
                    }
                }, {
                    $unwind: {
                        "path": "$approvedDoctors",
                        "preserveNullAndEmptyArrays": true
                    }
                }, {
                    $group: {
                        _id: {
                            date: "$date"
                        },
                        activity: {
                            $addToSet: "$activity"
                        },
                        TPStatus: {
                            $addToSet: "$TPStatus"
                        },
                        approvedArea: {
                            $addToSet: "$approvedArea"
                        }
                    }
                }, function(err, TPResult) {
                    if(err){
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'TPDeviationDetails' });
                    }
                    return cb(false, TPResult);
                });

            },
            DCRResult: function(cb) {
                DCRMasterCollection.aggregate({
                    $match: {
                        // "_id": ObjectId("5c25e8e3b5156b26a87a7e3d"),
                        "companyId": ObjectId(param.companyId),
                        "submitBy": ObjectId(param.userId),
                        "dcrDate": {
                            $gte: new Date(fromDate),
                            $lte: new Date(toDate)
                        }
                    }
                }, {
                    $lookup: {
                        "from": "DCRProviderVisitDetails",
                        "localField": "_id",
                        "foreignField": "dcrId",
                        "as": "dcrDetails"
                    }
                }, {
                    $unwind: {
                        "path": "$dcrDetails",
                        "preserveNullAndEmptyArrays": true
                    }
                }, {
                    $unwind: {
                        "path": "$visitedBlock",
                        "preserveNullAndEmptyArrays": true
                    }
                }, {
                    $unwind: {
                        "path": "$jointWork",
                        "preserveNullAndEmptyArrays": true
                    }
                }, {
                    $group: {
                        _id: {
                            dcrDate: "$dcrDate"
                        },
                        dcrSubmissionDate: {
                            $addToSet: "$dcrSubmissionDate"
                        },
                        workingStatus: {
                            $addToSet: "$workingStatus"
                        },
                        DCRStatus: {
                            $addToSet: "$DCRStatus"
                        },
                        visitedArea: {
                            $addToSet: "$visitedBlock"
                        },
                        jointWork: {
                            $addToSet: "$jointWork"
                        },
                        totalVisitedDoctor: {
                            $addToSet: {
                                $cond: [{
                                    $eq: ["$dcrDetails.providerType", "RMP"]
                                }, "$dcrDetails.providerId", "$anything_NotToInsertElseValue"]
                            }
                        },
                        totalVisitedVendor: {
                            $addToSet: {
                                $cond: [{
                                    $eq: ["$dcrDetails.providerType", "Drug"]
                                }, "$dcrDetails.providerId", "$anything_NotToInsertElseValue"]
                            }
                        },
                        totalVisitedStockist: {
                            $addToSet: {
                                $cond: [{
                                    $eq: ["$dcrDetails.providerType", "Stockist"]
                                }, "$dcrDetails.providerId", "$anything_NotToInsertElseValue"]
                            }
                        }
                    }
                }, function(err, dcrResult) {
                    if (err) {
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'TPDeviationDetails' });
                        // console.log(err)
                    }
                    //// console.log(dcrResult)
                    return cb(false, dcrResult);
                });

            }
        }, function(err, result) {
            if(err){
                sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'TPDeviationDetails' });
            }

            let allDates = AllDatesBetweenTwoDates(new Date(fromDate), new Date(toDate))
                //// console.log(allDates)
            let finalData = [];
            //for (let i = 0; i < 1; i++) {
            for (let i = 0; i < allDates.length; i++) {
                let reportDate = moment(allDates[i]).format('DD-MMM-YYYY');
                //// console.log(reportDate)
                let dcrSubmissionDate = "-------";
                let plannedActivity = "-------";
                let actualActivity = "--------";
                let plannedArea = "--------";
                let actualVisitedArea = "------";
                let workWith = "--------";
                let visitedDoctor = 0;
                let visitedChemist = 0;
                let visitedStockist = 0;

                let TPApprovedAreas = [];
                let DCRVisitiedAreas = [];

                let TPMatchedObject = result.TPResult.filter(function(obj) {
                    return obj._id.date.getTime() == allDates[i].getTime(); // we used getTime() function to compare two dates
                });

                if (TPMatchedObject.length > 0) {

                    plannedActivity = TPMatchedObject[0].activity[0];
                    if (plannedActivity == "Working" || TPMatchedObject[0].TPStatus[0] == 1) { //'TPStatus' : One key will be added in TP model for working + meeting
                        for (let i = 0; i < TPMatchedObject[0].approvedArea.length; i++) {
                            TPApprovedAreas.push(TPMatchedObject[0].approvedArea[i].from);
                            TPApprovedAreas.push(TPMatchedObject[0].approvedArea[i].to);

                            //As TP was not submitted on the basis of "From" to "To" station
                            if (param.year >= 2019) {
                                if (i == 0) {
                                    plannedArea = "From : " + TPMatchedObject[0].approvedArea[i].from + " -- To : " + TPMatchedObject[0].approvedArea[i].to
                                } else {
                                    plannedArea = plannedArea + ", From : " + TPMatchedObject[0].approvedArea[i].from + " -- To : " + TPMatchedObject[0].approvedArea[i].to
                                }
                            }
                        }

                        //As TP was not submitted on the basis of "From" to "To" station
                        if (param.year <= 2018) {
                            plannedArea = TPApprovedAreas;
                        }
                    }
                }

                let DCRMatchedObject = result.DCRResult.filter(function(obj) {
                    return obj._id.dcrDate.getTime() == allDates[i].getTime(); // we used getTime() function to compare two dates
                });

                if (DCRMatchedObject.length > 0) {
                    //// console.log(DCRMatchedObject[0])
                    dcrSubmissionDate = moment(DCRMatchedObject[0].dcrSubmissionDate[0]).format('DD-MMM-YYYY');

                    //// console.log("Punch Date : "+dcrSubmissionDate)
                    actualActivity = DCRMatchedObject[0].workingStatus[0];
                    //// console.log(DCRMatchedObject[0].workingStatus[0])
                    if (actualActivity == "Working" || DCRMatchedObject[0].DCRStatus[0] == 1) { //'DCRStatus' : One key will be added in DCRMaster model for working + meeting

                        //  // console.log("Len : " + DCRMatchedObject[0])
                        for (let i = 0; i < DCRMatchedObject[0].visitedArea.length; i++) {
                            DCRVisitiedAreas.push(DCRMatchedObject[0].visitedArea[i].from)
                            DCRVisitiedAreas.push(DCRMatchedObject[0].visitedArea[i].to)
                            if (i == 0) {
                                actualVisitedArea = "From : " + DCRMatchedObject[0].visitedArea[i].from + " -- To : " + DCRMatchedObject[0].visitedArea[i].to
                            } else {
                                actualVisitedArea = actualVisitedArea + ", From : " + DCRMatchedObject[0].visitedArea[i].from + " -- To : " + DCRMatchedObject[0].visitedArea[i].to
                            }
                        }

                        for (let i = 0; i < DCRMatchedObject[0].jointWork.length; i++) {
                            if (i == 0) {
                                workWith = DCRMatchedObject[0].jointWork[i].Name;
                            } else {
                                workWith = workWith + ", " + DCRMatchedObject[0].jointWork[i].Name;
                            }
                        }

                        visitedDoctor = DCRMatchedObject[0].totalVisitedDoctor.length;
                        visitedChemist = DCRMatchedObject[0].totalVisitedVendor.length;
                        visitedStockist = DCRMatchedObject[0].totalVisitedStockist.length;
                    }
                }

                // // console.log("TP Len : " + TPApprovedAreas.length)
                //// console.log("DCR Len : " + DCRVisitiedAreas.length)
                let color = "";
				let deviationStatus = "";
                let list1Counter = 0;
                let list1Size = TPApprovedAreas.length;
                let list2Counter = DCRVisitiedAreas.length;
                for (let z = 0; z < TPApprovedAreas.length; z++) {

                    let flag = false;
                    for (let x = 0; x < DCRVisitiedAreas.length; x++) {

                        if (TPApprovedAreas[z] == DCRVisitiedAreas[x]) {

                            flag = true;
                            break;
                        }
                    }
                    if (flag == false) {
                        //Station Not Found

                    } else {

                        list1Counter++;
                    }
                }
                if (list1Counter == list2Counter) {
                    if (list1Size > list2Counter) {
                        color = "ffcccc"; //Red
						deviationStatus = "Deviated";
                    } else {

                        color = "ebebe0"; // grey
						deviationStatus = "No Deviation";
                    }
                    //planned matched

                } else if (list1Counter == 0 && list2Counter > 0) {
                    color = "ffcccc";
					deviationStatus = "Deviated";
                } else if (list1Counter > 0 && list2Counter == 0) {
                    color = "ffcccc";
					deviationStatus = "Deviated";
                } else if (list1Counter > list2Counter) {
                    color = "ffcccc";
					deviationStatus = "Deviated";
                } else if (list1Counter < list2Counter) {

                    if (list1Size == list1Counter) {
                        //blue when planned stations matched with visited station and also he works on extra stations
                        color = "cce6ff"; //Blue
						deviationStatus = "Additional Visited";
                    } else {
                        color = "ffcccc";
						deviationStatus = "Deviated";
                    }
                }
                // // console.log("Color : " + color)
                finalData.push({
                    reportDate: reportDate,
                    dcrSubmissionDate: dcrSubmissionDate,
                    plannedActivity: plannedActivity,
                    actualActivity: actualActivity,
                    plannedArea: plannedArea,
                    actualVisitedArea: actualVisitedArea,
                    workWith: workWith,
                    visitedDoctor: visitedDoctor,
                    visitedChemist: visitedChemist,
                    visitedStockist: visitedStockist,
                    tpDeviationColor: color,
					tpDeviationStatus: deviationStatus
                })
            }
            return cb(false, finalData);
        });
    };

    Tourprogram.remoteMethod(
        'TPDeviationDetails', {
            description: 'Get TP Deviation details on User',
            accepts: [{ arg: 'param', type: 'object' }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );

    function AllDatesBetweenTwoDates(startDate, endDate) {
        for (var a = [], d = startDate; d <= endDate; d.setDate(d.getDate() + 1)) {
            a.push(new Date(d));
        }
        return a;
    };

    //--------------------------------------END---------------------------------------------
	
	// create by praveen singh for getting the tp submitted dates for the selected month
	 Tourprogram.getTPSubmittedDates = function(params, cb) {
        var self = this;
        var TourprogramCollection = self.getDataSource().connector.collection(Tourprogram.modelName);

        TourprogramCollection.aggregate({
                $match: {
                    "companyId": ObjectId(params["companyId"]),
                    "createdBy": ObjectId(params["createdBy"]),
                    month: parseInt(params["month"]),
                    year: parseInt(params["year"])
                }
            }, {
                $group: {
                    _id: null,
                    dates: {
                        $addToSet: "$date"
                    }
                }
            },
            function(err, result) {
                if(err) {
                    sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: params, methodName: 'getTPSubmittedDates' });
					// console.log(err)
                }
                return cb(false, result);
            }
        );

    };
 
    Tourprogram.remoteMethod(
        'getTPSubmittedDates', {
            description: 'Get TP submitted dates which will use to disable dates on TP calendar ',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //----------divya------------
    
	Tourprogram.getTPDayPlan = function(param, cb) {	
        var self = this;
        var TourprogramCollection = self.getDataSource().connector.collection(Tourprogram.modelName);

        if (param.type == "State" || param.type == "Headquarter") {
            Tourprogram.app.models.UserInfo.getAllUserIdsBasedOnType(
                param,
                function(err, userRecords) {
                    if (err) {
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'getTPDayPlan' });
                        // console.log(err)
                    }
                    if (userRecords.length > 0) {
                        TourprogramCollection.aggregate({
                                $match: {
                                    "companyId": ObjectId(param.companyId),
                                    "createdBy": { $in: userRecords[0].userIds },
                                    "month":param.month,
                                    "year":param.year
                                }
                            }, {
                                $sort:{
                                    "date":1
                                }
                            }, {
                                $group: {
                                    _id:{ "userId": "$createdBy"},
                                    data: {
                                        $push: {
                                            date: { $dayOfMonth: "$date" },
                                            dayPlan: "$isDayPlan",
                                        }
                                    }
                                }
                            }, {
                                $lookup: {
                                    "from": "UserInfo",
                                    "localField": "_id.userId",
                                    "foreignField": "userId",
                                    "as": "userData"
                                }
                            }, {
                                $unwind: "$userData"
                            }, {
                                $lookup: {
                                    "from": "State",
                                    "localField": "userData.stateId",
                                    "foreignField": "_id",
                                    "as": "stateData"
                                }
                            }, {
                                $lookup: {
                                    "from": "District",
                                    "localField": "userData.districtId",
                                    "foreignField": "_id",
                                    "as": "districtData"
                                }
                            }, {
                                $project: {
                                    _id: 1,
                                    "userData.name": 1 ,
									divisionName : { $arrayElemAt: ["$userData.divisionName", 0] },
                                    stateId: { $arrayElemAt: ["$stateData._id", 0] },
                                    stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                                    districtId: { $arrayElemAt: ["$districtData._id", 0] },
                                    districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                                    data:1
                                }
                            }, {
                                $sort: {
                                    stateName: 1,
                                    districtName: 1,
                                    userName: 1
                                }
                            },
                            function(err, tpResult) {
                                
                                if (err) {
                                    sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'getTPDayPlan' });
                                    // console.log(err);
                                    return cb(err);
                                }
                                if (!tpResult) {
                                    var err = new Error('no records found');
                                    err.statusCode = 404;
                                    err.code = 'NOT FOUND';
                                    return cb(err);
                                }



                                let lastDayOfMonth = getNumberOfDaysInAMonth(param.month, param.year);
                                let finalResult = [];
                                for (let i = 0; i < userRecords[0].obj.length; i++) {
                                    let dateWiseDataArray = [];

                                    let filterObj = tpResult.filter(function(obj) {
                                        return obj._id.userId.toString() == userRecords[0].obj[i].userId.toString();
                                    });

                                    if (filterObj.length > 0) {
                                        //TP Is Submitted
                                        for (let d = 1; d <= lastDayOfMonth; d++) {
                                            // dateWiseDataArray
                                            let isDateSunday = false;
                                            let myDate = new Date();
                                            myDate.setFullYear(param.year);
                                            myDate.setMonth(param.month - 1);
                                            myDate.setDate(d);
                                            if (myDate.getDay() == 0) {
                                                isDateSunday = true;
                                            }


                                            let tpFound = filterObj[0].data.filter(function(obj) {
                                                return obj.date == d;
                                            });
                                            if (tpFound.length > 0) {
                                                    dateWiseDataArray.push({
                                                       dayPlan: tpFound[0].dayPlan,
                                                       sunday: isDateSunday,
                                                       day: d
                                                    });
                                            } else {
                                                //TP Not Found for particular date
                                                dateWiseDataArray.push({ dayPlan: "--", sunday: isDateSunday, day: "--" });
                                             
                                            }
                                              
                                        }

                                    } else {
                                        //TP Is Not Submitted
                                        for (let d = 1; d <= lastDayOfMonth; d++) {
                                            let isDateSunday = false;
                                            let myDate = new Date();
                                            myDate.setFullYear(param.year);
                                            myDate.setMonth(param.month - 1);
                                            myDate.setDate(d);
                                            if (myDate.getDay() == 0) {
                                                isDateSunday = true;
                                            }
                                            dateWiseDataArray.push({ dayPlan: "--", sunday: isDateSunday, day: "--" });
                                        }
                                    }

                                    finalResult.push({
										divisionName :userRecords[0].obj[i].divisionName,
                                        stateName: userRecords[0].obj[i].stateName,
                                        districtName: userRecords[0].obj[i].districtName,
                                        name: userRecords[0].obj[i].name,
                                        dateArray: dateWiseDataArray
                                    });
                                }
                                return cb(false, finalResult)

                            });
                    } else {
                        return cb(false, []);

                    }
                });
        } else {
            var userIds = [];
            for (var i = 0; i < param.userIds.length; i++) {
                userIds.push(ObjectId(param.userIds[i]));
            }

            Tourprogram.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                param.companyId, [true],
                userIds,
                function(err, userRecords) {
                    if (err) {
                        sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'getTPDayPlan' });
                        return err;
                    }
                    TourprogramCollection.aggregate({
                            $match: {
                                "companyId": ObjectId(param.companyId),
                                "submitBy": { $in: userRecords[0].userIds },
                                "month":param.month,
                                "year":param.year
                            }
                        }, {
                            $group: {
                                _id:{ "userId": "$createdBy"},
                                data: {
                                    $push: {
                                        date: { $dayOfMonth: "$date" },
                                        dayPlan: "$isDayPlan"
                                    }
                                }
                            }
                        }, {
                            $lookup: {
                                "from": "UserInfo",
                                "localField": "_id.userId",
                                "foreignField": "userId",
                                "as": "userData"
                            }
                        }, {
                            $unwind: "$userData"
                        }, {
                            $lookup: {
                                "from": "State",
                                "localField": "userData.stateId",
                                "foreignField": "_id",
                                "as": "stateData"
                            }
                        }, {
                            $lookup: {
                                "from": "District",
                                "localField": "userData.districtId",
                                "foreignField": "_id",
                                "as": "districtData"
                            }
                        }, {
                            $project: {
                                 _id: 1,
                                    "userData.name": 1 ,
									divisionName : { $arrayElemAt: ["$userData.divisionName", 0] },
                                    stateId: { $arrayElemAt: ["$stateData._id", 0] },
                                    stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                                    districtId: { $arrayElemAt: ["$districtData._id", 0] },
                                    districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                                    data:1
                            }
                        }, {
                            $sort: {
                                stateName: 1,
                                districtName: 1,
                                userName: 1
                            }
                        },
                        function(err, tpResult) {
                            if (err) {
                                sendMail.sendMail({ collectionName: 'Tourprogram', errorObject: err, paramsObject: param, methodName: 'getTPDayPlan' });
                                // console.log(err);
                                return cb(err);
                            }
                            if (!tpResult) {
                                var err = new Error('no records found');
                                err.statusCode = 404;
                                err.code = 'NOT FOUND';
                                return cb(err);
                            }

                            let lastDayOfMonth = getNumberOfDaysInAMonth(param.month, param.year);
                            let finalResult = [];
                            for (let i = 0; i < userRecords[0].obj.length; i++) {
                                let dateWiseDataArray = [];

                                let filterObj = tpResult.filter(function(obj) {
                                        return obj._id.userId.toString() == userRecords[0].obj[i].userId.toString();
                                });

                                if (filterObj.length > 0) {
                                    //TP Is Submitted
                                     for (let d = 1; d <= lastDayOfMonth; d++) {
                                        let isDateSunday = false;
                                        let myDate = new Date();
                                        myDate.setFullYear(param.year);
                                        myDate.setMonth(param.month - 1);
                                        myDate.setDate(d);
                                        if (myDate.getDay() == 0) {
                                            isDateSunday = true;
                                        }


                                        let tpFound = filterObj[0].data.filter(function(obj) {
                                                return obj.date == d;
                                        });
                                        if (tpFound.length > 0) {
                                            //TP Found for particular date

                                            dateWiseDataArray.push({
                                                       dayPlan: tpFound[0].dayPlan,
                                                       sunday: isDateSunday,
                                                       day: d
                                                    });
                                        } else {
                                            //TP Not Found for particular date
                                            dateWiseDataArray.push({ dayPlan: "--", sunday: isDateSunday, day: "--" });
                                        }
                                    }

                                } else {
                                    //TP Is Not Submitted
                                    for (let d = 1; d <= lastDayOfMonth; d++) {
                                        let isDateSunday = false;
                                        let myDate = new Date();
                                        myDate.setFullYear(param.year);
                                        myDate.setMonth(param.month - 1);
                                        myDate.setDate(d);
                                        if (myDate.getDay() == 0) {
                                            isDateSunday = true;
                                        }
                                        dateWiseDataArray.push({ dayPlan: "--", sunday: isDateSunday, day: "--" });
                                    }
                                }

                                finalResult.push({
									division:userRecords[0].obj[i].divisionName,
                                    stateName: userRecords[0].obj[i].stateName,
                                    districtName: userRecords[0].obj[i].districtName,
                                    name: userRecords[0].obj[i].name,
                                    dateArray: dateWiseDataArray
                                });
								


                            }
							
                            return cb(false, finalResult)

                        });


                });

        }

    }

    Tourprogram.remoteMethod(

        'getTPDayPlan', {

            description: 'Get Tour Program Details',

            accepts: [{ arg: 'param', type: 'object',http:{ source: 'body' }}],
            
            returns: {

                root: true,

                type: 'array'

            },


            http: {

                verb: 'post'

            }

        }

    );

    function getNumberOfDaysInAMonth(month, year) {

        return new Date(year, month, 0).getDate();

    };

};